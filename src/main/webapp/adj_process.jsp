<%-- 
    Document   : adj_process
    Created on : 2017. 5. 30, 오후 2:47:45
    Author     : MoonBong
--%>
<%@page import="com.onoffkorea.system.common.util.Util"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.sql.*" %>
<%@page import="javax.naming.*"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="com.onoffkorea.system.app.Vo.KiccPgVo"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%!
    /**
     * 파라미터 체크 메소드
     */
    public String getNullToSpace(String param) {
        return (param == null) ? "" : param.trim();
    }

    public String getNullToZero(String param) {
        return (param == null) ? "0" : param.trim();
    }
        
    
    public void closeResultSet(ResultSet rs)
    {
        try
        {
            if ( rs != null ) rs.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    
    public void closePstmt(PreparedStatement psmt)
    {
        try
        {
            if ( psmt != null ) psmt.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }    
    
    public void closeConnection(Connection con)
    {
        try
        {
            if ( con != null ) con.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    
    public void closeCallableStatement(CallableStatement cs)
    {
        try
        {
            if ( cs != null ) cs.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }    
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>정산내역삭제</title>
    </head>
    <body>
<%
    final Logger logger = Logger.getLogger("transdelete_process.jsp");
    
    Connection conn = null;
    PreparedStatement pstmt = null;
    PreparedStatement pstmt2 = null;
    ResultSet rs = null;
    ResultSet rs2 = null;
    CallableStatement cs1=null;
    CallableStatement cs2=null;
   
    String filename=null;
   
    try
    {
       //DB Connection을 가져온다.
        /*
		Context initContext = new InitialContext();                                            //2
        DataSource ds = (DataSource) initContext.lookup("java:comp/env/jdbc/takeonoracle");     //3
        conn = ds.getConnection();
		*/
		Class.forName("com.mysql.jdbc.Driver");
		//DB에 접속을 한다.
//                conn = DriverManager.getConnection("jdbc:mysql://10.100.50.32:3306/okpay","okpay","okpay!");
//              conn = DriverManager.getConnection("jdbc:mysql://10.100.50.54:3306/okpay","okpay","okpay!");
		conn = DriverManager.getConnection("jdbc:mysql://192.168.0.241:3306/okpay","okpay","okpay!");


        conn.setAutoCommit(false);
		System.out.println("-------------4-------------");
        conn.setAutoCommit(false);
        
        String strreqTranDate = request.getParameter("adjdate");
        
        
        cs1 = conn.prepareCall("{call PRC_CREATE_ONFFMERCH_ADJUST ( ?, ?, ?, ? )}");
        cs1.setString(1, strreqTranDate); 
        cs1.registerOutParameter(2, java.sql.Types.VARCHAR); 
        cs1.registerOutParameter(3, java.sql.Types.INTEGER); 
        cs1.registerOutParameter(4, java.sql.Types.VARCHAR); 
        cs1.execute(); 
        
        out.println("정산날자 처리결과 : " + "[" + cs1.getString(2) + "]["+ cs1.getString(4) +"]" + "</br>");
        
        
        cs2 = conn.prepareCall("{call PRC_ONFFMERCH_ADJUST_TRAN ( ?, ?, ?, ? )}");
        cs2.setString(1, strreqTranDate); 
        cs2.registerOutParameter(2, java.sql.Types.VARCHAR); 
        cs2.registerOutParameter(3, java.sql.Types.INTEGER); 
        cs2.registerOutParameter(4, java.sql.Types.VARCHAR); 
        cs2.execute(); 
        
        out.println("거래내역 정산 처리결과 : " + "[" + cs2.getString(2) + "]["+ cs2.getString(4) +"]" + "</br>");
        
        conn.commit();
        out.println("<b>정산처리완료!!!</b><br/>");
    }
    catch(Exception ex)
    {
        ex.printStackTrace();
        conn.rollback();
        
        out.println("<b>정산내역처리오류!!!</b><br/>");
    }
    finally
    {
       
        closeResultSet(rs);
        closeResultSet(rs2);
        closePstmt(pstmt);
        closePstmt(pstmt2);
        
        closeCallableStatement(cs1);
        closeCallableStatement(cs2);
        
        closeConnection(conn);
    }

%>        
    </body>
</html>


