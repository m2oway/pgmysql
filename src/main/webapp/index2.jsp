<%-- 
    Document   : test.jsp
    Created on : 2015. 1. 7, 오후 4:53:48
    Author     : Administrator
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>okpay</title>
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dhx/dhtmlxwindows.css" />"  />
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dhx/dhtmlxwindows_dhx_skyblue.css" />"  />
        <script src="<c:url value="/resources/javascript/dhx/dhtmlxcommon.js" />"></script>
        <script src="<c:url value="/resources/javascript/dhx/dhtmlxwindows.js" />"></script>
        <script src="<c:url value="/resources/javascript/dhx/dhtmlxcontainer.js" />"></script>        
        <script type="text/javascript" src="<c:url value="/resources/javascript/jquery-1.6.2.min.js" />"></script>
        <script type="text/javascript" src="<c:url value="/resources/javascript/jquery-ui-1.8.17.custom.min.js" />"></script>
        <script type="text/javascript" src="<c:url value="/resources/javascript/jqueryform/2.8/jquery.form.js" />"></script>
        <style>
            table.logintable {
                font-family: verdana,arial,sans-serif;
                font-size:11px;
                color:#FFF;
                background-color:#666;

                border: solid 1px;
                border-color: #CCC;
                border-collapse: collapse;
            }	
            table.logintable td
            {
                border: solid 1px;
                border-color: #CCC;
            }

            table.titletable {
                font-family: verdana,arial,sans-serif;
                font-size:13px;
                color:#ff2005;
            }							

            table.gridtable {
                font-family: verdana,arial,sans-serif;
                font-size:13px;
                color:#333333;
                border: solid 1px;
                border-color: #CCC;
                border-collapse: collapse;
            }	

            table.gridtable td {
                border-left:solid 1px;
                border-left-color: #CCC;
                padding: 8px;
                background-color: #ffffff;
            }	

            table.gridtable td.headcol {
                border-width: 1px;
                padding: 8px;
                border-style: solid;
                border-color: #CCC;
                text-align:center;

            }		

            table.bbs {
                font-family: verdana,arial,sans-serif;
                font-size:11px;
                color:#333333;
                border-top:solid 1px;
                border-top-color: #CCC;
                border-collapse: collapse;
            }	

            table.maintable
            {
                margin:0;
                padding:0;

            }

            table.maintable td.leftcol
            {
                /*
                padding-left:30px;
                */
                padding-left:0px;
            }

            #divpop{position:absolute;left:10px;top:10px;width:430px;height:538px;z-index:10;background-color:#eee;}        
        </style>

        <script type="text/javascript">
            $(document).ready(function () {

                $("#loginForm").submit(function () {

                    $.post($(this).attr("action"), $(this).serialize(), function (html) {

                        if (html == "success")
                        {
                            location.href = "${pageContext.request.contextPath}/common/main";
                        }
                        else
                        {
                            alert('로그인 정보 확인해 주세요.');
                            $("input[name = user_id]").focus();
                        }

                    });
                    return false;
                });

            });

            function login()
            {
                $("#loginForm").submit();
            }
     

        </script>
    </head>
    <body style="margin:0px;padding:0px;" onload="doOnLoad();" >
<div id="winVP">        
        <table width='1200px' height='100%' border="0" bordercolor="red" cellpadding="0" class="maintable" align="center">
            <tr>
                <!--
                <td ><img width="1200" height="481" border="0" style="border:hidden;" src="<c:url value="/resources/images/onofftop3.jpg"/>" ></td>
                -->
                <!--
                <td ><img width="1200" height="350" border="0" style="border:hidden;" src="<c:url value="/resources/images/onofftop6.jpg"/>" ></td>
                -->
                
                <td ><img width="1200" height="381" border="0" style="border:hidden;" src="<c:url value="/resources/images/onofftop7.jpg"/>" ></td>
            </tr>
            <tr>
                <td class="leftcol" style="padding-top:5px; padding-bottom:20px;">
                    <table width="100%" border="0" bordercolor='red' cellpadding="0">
                        <tr>
                            <td  width="312" style="padding-top:10px;">
                                <table width="312" hetight="140" border="0" bordercolor="red">
                                    <tr align="left" valign="bottom">
                                        <td  width="312" height="126" valign="top" align="left" style="BACKGROUND:url('<c:url value="/resources/images/login_main.jpg"/>') no-repeat" >
                                            <table width="312" hetight="100%" border="0" bordercolor="red">
                                                <tr align="left">
                                                    <td width="135" style="padding-left:75px;padding-top:40px;" align="left"><form id="loginForm" method="post" modelAttribute="loginFormBean" action="${pageContext.request.contextPath}/login/loginForm2">
                                                            <table width="100%" height="100%" border="0" bordercolor="blue" >
                                                                <tr>
                                                                    <td><input type="text" name="user_id"  onkeydown="javascript:if (event.keyCode==13) { login();};"  style="WIDTH: 120px; IME-MODE: disabled"/></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="password" name="user_pwd" onkeydown="javascript:if (event.keyCode==13) { login();};"  style="WIDTH: 120px; IME-MODE: disabled" /></td>
                                                                </tr>
                                                            </table></form>
                                                    </td>
                                                    <td style="PADDING-BOTTOM:0px; PADDING-TOP: 45px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px" align="left"><A href="javascript:login();"><IMG src="<c:url value="/resources/images/login_btn.png"/>"></A> </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>                                    
                            </td>
                            <td valign="top" align="left" style="padding-left:20px;padding-right:0px;" rowspan="2" >
                                <table width="100%" class="titletable">
                                    <tr>
                                        <td valign="bottom" align="left"><img src="resources/images/muiza_tile.jpg" width="240" height="32"></td>
                                        <td  valign="bottom" align="right">[01/10~01/31일까지 무이자 안내]</td>
                                    </tr>
                                </table>                        
                                <table width="100%" class="gridtable">
                                    <tr>
                                        <td class="headcol">슬림할부</td>
                                        <td class="headcol">슬림할부개월</td>
                                        <td class="headcol">이벤트 비고 참조 공지</td>
                                    </tr>
                                    <tr>
                                        <td>삼성(2~5개월)</td>
                                        <td>삼성 6,10,12개월</td>
                                        <td>6개월 - 1회차 고객부담</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>법인체크,선불,기프트카드는 제외</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                            <td width="280" rowspan="2" valign="middle" align="center" style="padding-top:10px;"><img src="<c:url value="/resources/images/terminal.jpg"/>" width="200" height="180"></td>
                        </tr>
                        <tr>
                            <td  width="312" valign="middle" align="left" >
                                        <img src="<c:url value="/resources/images/insue2.jpg"/>" width="310" height="85">
                            </td>                            
                        </tr>                        
                        <tr>
                            <td  width="312" valign="top" align="left" style="padding-top:10px;">
                                <img src="<c:url value="/resources/images/helptel2.jpg"/>" width="309" height="166">
                            </td>
                            <td valign="top" align="left" style="padding-left:20px;padding-right:38px;padding-top:10px;">
                                <table width="100%" class="titletable" border="0" bordercolor="red">
                                    <tr>
                                        <td valign="bottom" align="left"><img src="resources/images/gonggi_title.jpg" width="95" height="28"></td>
                                    </tr>
                                </table>      
                                <table width="100%" class="bbs" id="bbsList" border="0" bordercolor="red">
                                </table>                               
                            </td>
                            <td width="280" valign="top" style="padding-top:10px;"><img src="<c:url value="/resources/images/smslink2.jpg"/>" width="280" height="183"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td ><img src="<c:url value="/resources/images/onoffbottom.jpg"/>" width="1200" height="81"></td>
            </tr>
        </table></div>
<!--
            <div id="divpop">
            <form name="divpop_frm" method="post" action="<c:url value="/bbs/bbsMasterPopUpList"/>" modelAttribute="bbsFormBean"  >
                <table id="popupList">
                    <tr>
                        <td>제목</td>
                        <td>내용</td>
                    </tr>
                </table>
                <br/>
                <input type="checkbox" name="chkbox" value="" checked onClick="close_divpop('divpop_frm', 'divpop');" />오늘 하루 이 창을 열지 않음
                <a href="javascript:;" onclick="close_divpop('divpop_frm', 'divpop')"><b>[닫기]</b></a>
            </form>
        </div>            
--><!--<div id="winVP" style="position: relative; height: 500px; margin: 10px;"></div>-->
        <script type="text/javascript">
         
          
            $(document).ready(function () {
                
                $.ajax({
                    url :"<c:url value="/bbs/bbsMasterPopUpList" />",
                    type : "POST",
                    async : true,
                    dataType : "json",
                    success : function(data) {
                        //$("#popupList").append(data);
                        //alert(data.length);
                        //alert(data[0].title);
                            $.each(data,function(index,bbsdata)
                            {
                                createWindow(bbsdata);
                                //alert(bbsdata.title);
                                //alert(bbsdata.content);
                            }      
                        );
                    },
                    error : function() { 
                        alert("조회 실패");
                    }
                });         

                $.ajax({
                    url : "<c:url value="/bbs/bbsMasterMainList" />",
                    type : "POST",
                    async : true,
                    dataType : "json",
                    success : function(data) {
                        $("#bbsList").append(data);
                    },
                    error : function() { 
                        alert("조회 실패");
                    }
                });      

            }); 
            
            var initx = 0;
            var inity = 30;
                
                var dhxWins;
                function doOnLoad() {
                    dhxWins = new dhtmlXWindows();
                    dhxWins.enableAutoViewport(false);
                    dhxWins.attachViewportTo("winVP");
                }
		
		var idPrefix = 1;
		function createWindow(bbsdata) {
			var p = 0;
			dhxWins.forEachWindow(function(){p++;});
			/*
                        if (p>5) {
				alert("Too many windows");
				return;
			}
                        */
			var id = "userWin"+(idPrefix++);

			var w = 300;
			var h = 200;
                        var x;
                        if(idPrefix == 2)
                        {
                            x = initx+10;
                        }
                        else
                        {
                            x = initx+300;
                        }
			
			var y = inity+0;
                        
                        initx = x;
			inity = y;
                        dhxWins.createWindow(id, x, y, w, h);
			dhxWins.window(id).setText(bbsdata.title);
			//dhxWins.window(id).attachHTMLString("<span style='font-size:11px;font-family:tahoma;padding:4px;'>"+bbsdata.content+"</span>");
                        //dhxWins.window(id).attachHTMLString("<div style='font-family: Tahoma; font-size: 11px; height: 100%; overflow: auto;'>"+bbsdata.content+"</div>");
                        dhxWins.window(id).attachHTMLString("<textarea readonly style='font-family: Tahoma; font-size: 11px; height: 100%;width: 100%; overflow: auto;'>"+bbsdata.content+"</textarea>");
		}
		

            function set_cookie(name, value, expiredays) {
                var todayDate = new Date();
                todayDate.setDate(todayDate.getDate() + expiredays);
                document.cookie = name + "=" + escape(value) + "; path=/; expires=" + todayDate.toGMTString() + ";"
            }

            function close_divpop(frm, pop) {
                var f = eval("document." + frm);
                if (f.chkbox.checked) {
                    set_cookie(pop, "Y", 1);
                }
                document.getElementById(pop).style.display = "none";
            }
/*
            cookiedata = document.cookie;
            if (cookiedata.indexOf("divpop=Y") < 0) {
                document.getElementById('divpop').style.display = "block";
            } else {
                document.getElementById('divpop').style.display = "none";
            }
*/
        </script>            

    </body>
</html>