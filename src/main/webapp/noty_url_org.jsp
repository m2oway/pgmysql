<%@ page contentType="text/html; charset=euc-kr" 
	import="java.util.*, java.net.*, java.io.*, java.text.SimpleDateFormat,java.sql.*,javax.sql.*,javax.naming.*,org.apache.log4j.Logger,java.text.SimpleDateFormat,java.util.Date,java.io.*,java.math.BigDecimal" 
	buffer="1kb"
	autoFlush="false"
%><%!
    /**
     * 파라미터 체크 메소드
     */
    public String getNullToSpace(String param) {
        return (param == null) ? "" : param.trim();
    }

    public String getNullToZero(String param) {
        return (param == null) ? "0" : param.trim();
    }
    
    public String getNullToZero2(String param) {
        return (param == null || param.equals("")) ? "0" : param.trim();
    }
    
    public String getNullToTrim(String param) {
        return (param == null || param.equals("")) ? "0" : param.trim();
    }
           
    
    public void closeResultSet(ResultSet rs)
    {
        try
        {
            if ( rs != null ) rs.close();
        }
        catch(Exception ex)
        {
        }
    }
    
    public void closePstmt(PreparedStatement psmt)
    {
        try
        {
            if ( psmt != null ) psmt.close();
        }
        catch(Exception ex)
        {
        }
    }    
    
        public void closeStmt(Statement stmt)
    {
        try
        {
            if ( stmt != null ) stmt.close();
        }
        catch(Exception ex)
        {
        }
    }    
    
    public void closeConnection(Connection con)
    {
        try
        {
            if ( con != null ) con.close();
        }
        catch(Exception ex)
        {
        }
    }       
%><%
        //request.setCharacterEncoding("EUC-KR");
        final Logger logger = Logger.getLogger("easypay_noti_onoff.jsp");

        Connection conn = null;
        PreparedStatement pstmt = null;
        PreparedStatement pstmt2 = null;
        Statement stmt = null;
        ResultSet rs = null;
        ResultSet rs2 = null;
    
//------------------------------------------------------------------------------
// FILE NAME : noty_url.jsp
// DATE : 2005-10-04
// 이페이지는 noty_url.jsp로 지정된 페이지로 오는 결과전문을 받고 수신응답을 하기위한 샘플페이지입니다.
//-------------------------------------------------------------------------------

	int    ipos = 0;
	String ret = "false";
	String data = null, data1 = null; 
	
	 //String File_Path = "C:/log/"; 
	/*String File_Path = "/home/log/";*/
        String File_Path = "/home/okwas/okpayedi/log/";
	byte[] byte_data = null;
	
        //String strTpDate = new String(request.getParameter("data").getBytes("UTF-8"),"euc-kr");
        //data = URLDecoder.decode(strTpDate);
	data = URLDecoder.decode(request.getParameter("data"));
        out.print("["+data+"]");
	//data = "05850000101020171020130101256720104520171020                                          김현주                                                         Email                                             0테스트상품                                        1001011111111 135                                 1001148540031106O2017091315583301000401000432348509    승인성공                        40111111xxxx9991000 00000012000                1000000000000000000000000000000000000000000000000123456891111                                                                                   KN     ";
        //data = "05850000101020171020130101256720104520171020                                          김현주                                                         Email                                             0테스트상품                                        1001011111111 135                                 1011148540031106O2017091315583301000401000432348509    승인성공                        40111111xxxx9991000 00000012000                1000000000000000000000000000000000000000000000000123456891111                                                                                   KN     ";
	
        String CurDay = new SimpleDateFormat("yyyyMMddHHmmss").format(new java.util.Date());
	System.out.println("CurDay : " + CurDay);
	File_Path = File_Path + CurDay.substring(0,8) + ".txt";
	
	data1 = "[" 
			+ CurDay.substring(0,4) + "/" + CurDay.substring(4,6)
			+ "/" + CurDay.substring(6,8) + "-" + CurDay.substring(8,10)
			+ ":" + CurDay.substring(10,12) + ":" + CurDay.substring(12)
			+ "]:" + data;
	
	FileWriter file = new FileWriter(File_Path, true);
	
	file.write(data1);
	file.write("\n");
	file.close();
	
	byte_data = data.getBytes("euc-kr");
	if  (byte_data[0] >= '0' && byte_data[0] <= '9' && byte_data[1] >= '0' && byte_data[1] <= '9' &&
		 byte_data[2] >= '0' && byte_data[2] <= '9' && byte_data[3] >= '0' && byte_data[3] <= '9')         // 신PG 4byte길이
	{
		// 신PG 헤더
		String	RecvLen                = getNullToTrim(new String(byte_data, ipos,  4)); ipos +=  4;
		String	EncType                = getNullToTrim(new String(byte_data, ipos,  1)); ipos +=  1; // 0: 암화안함, 1:openssl, 2: seed
		String	Version                = getNullToTrim(new String(byte_data, ipos,  4)); ipos +=  4; // 전문버전
		String	VersionType            = getNullToTrim(new String(byte_data, ipos,  2)); ipos +=  2; // 구분
		String	Resend                 = getNullToTrim(new String(byte_data, ipos,  1)); ipos +=  1; // 전송구분 : 0 : 처음,  2: 재전송
		String	RequestDate            = getNullToTrim(new String(byte_data, ipos, 14)); ipos += 14; // 요청일자 : yyyymmddhhmmss
		String	StoreId                = getNullToTrim(new String(byte_data, ipos, 10)); ipos += 10; // 상점아이디
		String	OrderNumber            = getNullToTrim(new String(byte_data, ipos, 50)); ipos += 50; // 주문번호
		String	UserName               = getNullToTrim(new String(byte_data, ipos, 50)); ipos += 50; // 주문자명
		String	IdNum                  = getNullToTrim(new String(byte_data, ipos, 13)); ipos += 13; // 주민번호 or 사업자번호
		String	Email                  = getNullToTrim(new String(byte_data, ipos, 50)); ipos += 50; // email
		String	GoodType               = getNullToTrim(new String(byte_data, ipos,  1)); ipos +=  1; // 제품구분 0 : 실물, 1 : 디지털
		String	GoodName               = getNullToTrim(new String(byte_data, ipos, 50)); ipos += 50; // 제품명
		String	KeyInType              = getNullToTrim(new String(byte_data, ipos,  1)); ipos +=  1; // KeyInType 여부 : 1 : Swap, 2: KeyIn
		String	LineType               = getNullToTrim(new String(byte_data, ipos,  1)); ipos +=  1; // lineType 0 : offline, 1:internet, 2:Mobile
		String	PhoneNo                = getNullToTrim(new String(byte_data, ipos, 12)); ipos += 12; // 휴대폰번호
		String	ApprovalCount          = getNullToTrim(new String(byte_data, ipos,  1)); ipos +=  1; // 승인갯수
		String	HeadFiller             = getNullToTrim(new String(byte_data, ipos, 35)); ipos += 35; // 예비
		
		System.out.println("Header->RecvLen                =["  +  RecvLen        +  "]");
		System.out.println("Header->EncType                =["  +  EncType        +  "]");
		System.out.println("Header->Version                =["  +  Version        +  "]");
		System.out.println("Header->VersionType            =["  +  VersionType    +  "]");
		System.out.println("Header->Resend                 =["  +  Resend         +  "]");
		System.out.println("Header->RequestDate            =["  +  RequestDate    +  "]");
		System.out.println("Header->StoreId                =["  +  StoreId        +  "]");
		System.out.println("Header->OrderNumber            =["  +  OrderNumber    +  "]");
		System.out.println("Header->UserName               =["  +  UserName       +  "]");
		System.out.println("Header->IdNum                  =["  +  IdNum          +  "]");
		System.out.println("Header->Email                  =["  +  Email          +  "]");
		System.out.println("Header->GoodType               =["  +  GoodType       +  "]");
		System.out.println("Header->GoodName               =["  +  GoodName       +  "]");
		System.out.println("Header->KeyInType              =["  +  KeyInType      +  "]");
		System.out.println("Header->LineType               =["  +  LineType       +  "]");
		System.out.println("Header->PhoneNo                =["  +  PhoneNo        +  "]");
		System.out.println("Header->ApprovalCount          =["  +  ApprovalCount  +  "]");
		System.out.println("Header->HeadFiller             =["  +  HeadFiller     +  "]");
		
		if (byte_data[ipos] == '1' || byte_data[ipos] == 'I')                                   // 신용카드 승인/취소
		{
                    System.out.println("신용카드 시작");
			//rStatus = O : 성공, X : 실패 
			//rApprovalType.charAt(2) = '0' : 승인, '1' : 취소
			
			String	rApprovalType          = getNullToTrim(new String(byte_data, ipos,  4                    )); ipos +=  4;
			String	rTransactionNo         = getNullToTrim(new String(byte_data, ipos, 12                    )); ipos += 12; // 거래번호
			String	rStatus                = getNullToTrim(new String(byte_data, ipos,  1                    )); ipos +=  1; // 상태    O : 승인, X : 거절
			String	rTradeDate             = getNullToTrim(new String(byte_data, ipos,  8                    )); ipos +=  8; // 거래일자
			String	rTradeTime             = getNullToTrim(new String(byte_data, ipos,  6                    )); ipos +=  6; // 거래시간
			String	rIssCode               = getNullToTrim(new String(byte_data, ipos,  6                    )); ipos +=  6; // 발급사코드
			String	rAquCode               = getNullToTrim(new String(byte_data, ipos,  6                    )); ipos +=  6; // 매입사코드
			String	rAuthNo                = getNullToTrim(new String(byte_data, ipos, 12                    )); ipos += 12; // 승인번호    or 거절시 오류코드
			String	rMessage1              = getNullToTrim(new String(byte_data, ipos, 16                    )); ipos += 16; // 메시지1
			String	rMessage2              = getNullToTrim(new String(byte_data, ipos, 16                    )); ipos += 16; // 메시지2
			String	rCardNo                = getNullToTrim(new String(byte_data, ipos, 16                    )); ipos += 16; // 카드번호
			String	rExpDate               = getNullToTrim(new String(byte_data, ipos,  4                    )); ipos +=  4; // 유효기간
			String	rInstallment           = getNullToTrim(new String(byte_data, ipos,  2                    )); ipos +=  2; // 할부
			String	rAmount                = getNullToTrim(new String(byte_data, ipos,  9                    )); ipos +=  9; // 금액
			String	rMerchantNo            = getNullToTrim(new String(byte_data, ipos, 15                    )); ipos += 15; // 가맹점번호
			String	rAuthSendType          = getNullToTrim(new String(byte_data, ipos,  1                    )); ipos +=  1; // 전송구분
			String	rApprovalSendType      = getNullToTrim(new String(byte_data, ipos,  1                    )); ipos +=  1; // 전송구분(0 :    거절, 1 : 승인, 2: 원카드)
			String	rPoint1                = getNullToTrim(new String(byte_data, ipos, 12                    )); ipos += 12; // Point1
			String	rPoint2                = getNullToTrim(new String(byte_data, ipos, 12                    )); ipos += 12; // Point2
			String	rPoint3                = getNullToTrim(new String(byte_data, ipos, 12                    )); ipos += 12; // Point3
			String	rPoint4                = getNullToTrim(new String(byte_data, ipos, 12                    )); ipos += 12; // Point4
			String	rVanTransactionNo      = getNullToTrim(new String(byte_data, ipos, 12                    )); ipos += 12; // Van거래번호
			String	rFiller                = getNullToTrim(new String(byte_data, ipos, 82                    )); ipos += 82; // 예비
			String	rAuthType              = getNullToTrim(new String(byte_data, ipos,  1                    )); ipos +=  1; // ISP : ISP거래, MP1, MP2 : MPI거래, SPACE    : 일반거래
			String	rMPIPositionType       = getNullToTrim(new String(byte_data, ipos,  1                    )); ipos +=  1; // K : KSNET, R    : Remote, C : 제3기관, SPACE : 일반거래
			String	rMPIReUseType          = getNullToTrim(new String(byte_data, ipos,  1                    )); ipos +=  1; // Y : 재사용, N : 재사용아님
			String	rFiller_1              = getNullToTrim(new String(byte_data, ipos, byte_data.length-ipos ));             // 예비
			
			System.out.println("Credit->rApprovalType          =["  +  rApprovalType       +  "]");
			System.out.println("Credit->rTransactionNo         =["  +  rTransactionNo      +  "]");
			System.out.println("Credit->rStatus                =["  +  rStatus             +  "]");
			System.out.println("Credit->rTradeDate             =["  +  rTradeDate          +  "]");
			System.out.println("Credit->rTradeTime             =["  +  rTradeTime          +  "]");
			System.out.println("Credit->rIssCode               =["  +  rIssCode            +  "]");
			System.out.println("Credit->rAquCode               =["  +  rAquCode            +  "]");
			System.out.println("Credit->rAuthNo                =["  +  rAuthNo             +  "]");
			System.out.println("Credit->rMessage1              =["  +  rMessage1           +  "]");
			System.out.println("Credit->rMessage2              =["  +  rMessage2           +  "]");
			System.out.println("Credit->rCardNo                =["  +  rCardNo             +  "]");
			System.out.println("Credit->rExpDate               =["  +  rExpDate            +  "]");
			System.out.println("Credit->rInstallment           =["  +  rInstallment        +  "]");
			System.out.println("Credit->rAmount                =["  +  rAmount             +  "]");
			System.out.println("Credit->rMerchantNo            =["  +  rMerchantNo         +  "]");
			System.out.println("Credit->rAuthSendType          =["  +  rAuthSendType       +  "]");
			System.out.println("Credit->rApprovalSendType      =["  +  rApprovalSendType   +  "]");
			System.out.println("Credit->rPoint1                =["  +  rPoint1             +  "]");
			System.out.println("Credit->rPoint2                =["  +  rPoint2             +  "]");
			System.out.println("Credit->rPoint3                =["  +  rPoint3             +  "]");
			System.out.println("Credit->rPoint4                =["  +  rPoint4             +  "]");
			System.out.println("Credit->rVanTransactionNo      =["  +  rVanTransactionNo   +  "]");
			System.out.println("Credit->rFiller                =["  +  rFiller             +  "]");
			System.out.println("Credit->rAuthType              =["  +  rAuthType           +  "]");
			System.out.println("Credit->rMPIPositionType       =["  +  rMPIPositionType    +  "]");
			System.out.println("Credit->rMPIReUseType          =["  +  rMPIReUseType       +  "]");
			System.out.println("Credit->rFiller_1              =["  +  rFiller_1           +  "]");      
			
			ret = "OK";
                        
                        String strTpReustCheck = rStatus;// O : 성공, X : 실패 
                                
                        String strTpAppCheck = String.valueOf(rApprovalType.charAt(2));//rApprovalType.charAt(2) = '0' : 승인, '1' : 취소
                        
                        
                       System.out.println("strTpAppCheck : " + strTpAppCheck);
                        System.out.println("strTpReustCheck : " + strTpReustCheck);
                        
                        //정보를 입력한다.
                       try
                       {
                           System.out.println("\n\n\n conn start");
                           Class.forName("org.gjt.mm.mysql.Driver");
                           //conn = DriverManager.getConnection("jdbc:mysql://211.178.37.78:3306/okpay", "okpay", "okpay!");
                           conn = DriverManager.getConnection("jdbc:mysql://10.100.50.32:3306/okpay", "okpay", "okpay!");
                            System.out.println("\n\n\n conn end");
                           conn.setAutoCommit(false);
                            System.out.println("\n\n\n auto commmit  end");
                           
                           String strTpNotiseq = "";
                           
                           //String strmkprimeryquery = "SELECT concat(date_format(now(), '%Y%m%d'),LPAD(get_seq('KSNETTRANSEQ'),10,'0')) AS seq";
                           
                           String strmkprimeryquery = "SELECT concat(date_format(now(), '%Y%m%d'),LPAD(f_get_sequence('KSNETTRANSEQ'),10,'0')) AS seq";
                           
                           stmt = conn.createStatement();
                           
                           rs = stmt.executeQuery(strmkprimeryquery);
                           
                           rs.next();
                           
                          strTpNotiseq = rs.getString(1);
                          
                          System.out.println("strTpNotiseq : " + strTpNotiseq);
                           
                           conn.commit();
                           
                           closeResultSet(rs);
                           closeStmt(stmt);
                           
                            //noti수신정보를 무조건 저장한다.
                            StringBuffer sb_notiinf_ins_sql = new StringBuffer();
                            sb_notiinf_ins_sql.append(" insert into tb_ksnet_noti ");
                            sb_notiinf_ins_sql.append(" ( ");
                            sb_notiinf_ins_sql.append(" 	noti_seq ");
                            sb_notiinf_ins_sql.append(" 	,enctype ");
                            sb_notiinf_ins_sql.append(" 	,version ");
                            sb_notiinf_ins_sql.append(" 	,versiontype ");
                            sb_notiinf_ins_sql.append(" 	,resend ");
                            sb_notiinf_ins_sql.append(" 	,requestdate ");
                            sb_notiinf_ins_sql.append(" 	,storeid ");
                            sb_notiinf_ins_sql.append(" 	,ordernumber ");
                            sb_notiinf_ins_sql.append(" 	,username ");
                            sb_notiinf_ins_sql.append(" 	,idnum ");
                            sb_notiinf_ins_sql.append(" 	,email ");
                            sb_notiinf_ins_sql.append(" 	,goodtype ");
                            sb_notiinf_ins_sql.append(" 	,goodname ");
                            sb_notiinf_ins_sql.append(" 	,keyintype ");
                            sb_notiinf_ins_sql.append(" 	,linetype ");
                            sb_notiinf_ins_sql.append(" 	,phoneno ");
                            sb_notiinf_ins_sql.append(" 	,approvalcount ");
                            sb_notiinf_ins_sql.append(" 	,headfiller ");
                            sb_notiinf_ins_sql.append(" 	,approvaltype ");
                            sb_notiinf_ins_sql.append(" 	,transactionno ");
                            sb_notiinf_ins_sql.append(" 	,status ");
                            sb_notiinf_ins_sql.append(" 	,tradedate ");
                            sb_notiinf_ins_sql.append(" 	,tradetime ");
                            sb_notiinf_ins_sql.append(" 	,isscode ");
                            sb_notiinf_ins_sql.append(" 	,aqucode ");
                            sb_notiinf_ins_sql.append(" 	,authno ");
                            sb_notiinf_ins_sql.append(" 	,message1 ");
                            sb_notiinf_ins_sql.append(" 	,message2 ");
                            sb_notiinf_ins_sql.append(" 	,cardno ");
                            sb_notiinf_ins_sql.append(" 	,expdate ");
                            sb_notiinf_ins_sql.append(" 	,installment ");
                            sb_notiinf_ins_sql.append(" 	,amount ");
                            sb_notiinf_ins_sql.append(" 	,merchantno ");
                            sb_notiinf_ins_sql.append(" 	,authsendtype ");
                            sb_notiinf_ins_sql.append(" 	,approvalsendtype ");
                            sb_notiinf_ins_sql.append(" 	,point1 ");
                            sb_notiinf_ins_sql.append(" 	,point2 ");
                            sb_notiinf_ins_sql.append(" 	,point3 ");
                            sb_notiinf_ins_sql.append(" 	,point4 ");
                            sb_notiinf_ins_sql.append(" 	,vantransactionno ");
                            sb_notiinf_ins_sql.append(" 	,filler ");
                            sb_notiinf_ins_sql.append(" 	,authtype ");
                            sb_notiinf_ins_sql.append(" 	,mpipositiontype ");
                            sb_notiinf_ins_sql.append(" 	,mpireusetype ");
                            sb_notiinf_ins_sql.append(" 	,filler_1 ");
                            sb_notiinf_ins_sql.append(" 	,proc_flag ");
                            sb_notiinf_ins_sql.append(" 	,ins_dt ");
                            sb_notiinf_ins_sql.append(" 	,mod_dt ");
                            sb_notiinf_ins_sql.append(" 	,ins_user ");
                            sb_notiinf_ins_sql.append(" 	,mod_user ");
                            sb_notiinf_ins_sql.append(" 	,notiretry_dt ");
                            sb_notiinf_ins_sql.append(" 	,notitety_msg ");    
                            sb_notiinf_ins_sql.append(" ) values ( ");
                            sb_notiinf_ins_sql.append(" 	?, ");//noti_seq                            
                            sb_notiinf_ins_sql.append(" 	?, ");//enctype
                            sb_notiinf_ins_sql.append(" 	?, ");//version
                            sb_notiinf_ins_sql.append(" 	?, ");//versiontype
                            sb_notiinf_ins_sql.append(" 	?, ");//resend
                            sb_notiinf_ins_sql.append(" 	?, ");//requestdate
                            sb_notiinf_ins_sql.append(" 	?, ");//storeid
                            sb_notiinf_ins_sql.append(" 	?, ");//ordernumber
                            sb_notiinf_ins_sql.append(" 	?, ");//username
                            sb_notiinf_ins_sql.append(" 	?, ");//idnum
                            sb_notiinf_ins_sql.append(" 	?, ");//email
                            sb_notiinf_ins_sql.append(" 	?, ");//goodtype
                            sb_notiinf_ins_sql.append(" 	?, ");//goodname
                            sb_notiinf_ins_sql.append(" 	?, ");//keyintype
                            sb_notiinf_ins_sql.append(" 	?, ");//linetype
                            sb_notiinf_ins_sql.append(" 	?, ");//phoneno
                            sb_notiinf_ins_sql.append(" 	?, ");//approvalcount
                            sb_notiinf_ins_sql.append(" 	?, ");//headfiller
                            sb_notiinf_ins_sql.append(" 	?, ");//approvaltype
                            sb_notiinf_ins_sql.append(" 	?, ");//transactionno
                            sb_notiinf_ins_sql.append(" 	?, ");//status
                            sb_notiinf_ins_sql.append(" 	?, ");//tradedate
                            sb_notiinf_ins_sql.append(" 	?, ");//tradetime
                            sb_notiinf_ins_sql.append(" 	?, ");//isscode
                            sb_notiinf_ins_sql.append(" 	?, ");//aqucode
                            sb_notiinf_ins_sql.append(" 	?, ");//authno 
                            sb_notiinf_ins_sql.append(" 	?, ");//message1
                            sb_notiinf_ins_sql.append(" 	?, ");//message2
                            sb_notiinf_ins_sql.append(" 	?, ");//cardno
                            sb_notiinf_ins_sql.append(" 	?, ");//expdate
                            sb_notiinf_ins_sql.append(" 	?, ");//installment
                            sb_notiinf_ins_sql.append(" 	?, ");//amount
                            sb_notiinf_ins_sql.append(" 	?, ");//merchantno
                            sb_notiinf_ins_sql.append(" 	?, ");//authsendtype
                            sb_notiinf_ins_sql.append(" 	?, ");//approvalsendtype
                            sb_notiinf_ins_sql.append(" 	?, ");//point1
                            sb_notiinf_ins_sql.append(" 	?, ");//point2
                            sb_notiinf_ins_sql.append(" 	?, ");//point3
                            sb_notiinf_ins_sql.append(" 	?, ");//point4
                            sb_notiinf_ins_sql.append(" 	?, ");//vantransactionno
                            sb_notiinf_ins_sql.append(" 	?, ");//filler
                            sb_notiinf_ins_sql.append(" 	?, ");//authtype
                            sb_notiinf_ins_sql.append(" 	?, ");//mpipositiontype
                            sb_notiinf_ins_sql.append(" 	?, ");//mpireusetype
                            sb_notiinf_ins_sql.append(" 	?, ");//filler_1
                            sb_notiinf_ins_sql.append(" 	?, ");//proc_flag
                            sb_notiinf_ins_sql.append(" 	date_format(now(), '%Y%m%d%H%i%s'), ");//ins_dt
                            sb_notiinf_ins_sql.append(" 	date_format(now(), '%Y%m%d%H%i%s'), ");//mod_dt
                            sb_notiinf_ins_sql.append(" 	'system', ");//ins_user
                            sb_notiinf_ins_sql.append(" 	'system', ");//mod_user
                            sb_notiinf_ins_sql.append(" 	null, ");//notiretry_dt
                            sb_notiinf_ins_sql.append(" 	null ");//notitety_msg
                            sb_notiinf_ins_sql.append(" ) ");

                            pstmt = conn.prepareStatement(sb_notiinf_ins_sql.toString());
                            pstmt.setString(1	,strTpNotiseq);//enctype
                            pstmt.setString(2	,EncType);//enctype
                            pstmt.setString(3	,Version);//version
                            pstmt.setString(4	,VersionType);//versiontype
                            pstmt.setString(5	,Resend);//resend
                            pstmt.setString(6	,RequestDate);//requestdate
                            pstmt.setString(7	,StoreId);//storeid
                            pstmt.setString(8	,OrderNumber);//ordernumber
                            pstmt.setString(9	,UserName);//username
                            pstmt.setString(10	,IdNum);//idnum
                            pstmt.setString(11	,Email);//email
                            pstmt.setString(12	,GoodType);//goodtype
                            pstmt.setString(13	,GoodName);//goodname
                            pstmt.setString(14	,KeyInType);//keyintype
                            pstmt.setString(15	,LineType);//linetype
                            pstmt.setString(16	,PhoneNo);//phoneno
                            pstmt.setString(17	,ApprovalCount);//approvalcount
                            pstmt.setString(18	,HeadFiller);//headfiller
                            pstmt.setString(19	,rApprovalType);//approvaltype
                            pstmt.setString(20	,rTransactionNo);//transactionno
                            pstmt.setString(21	,rStatus);//status
                            pstmt.setString(22	,rTradeDate);//tradedate
                            pstmt.setString(23	,rTradeTime);//tradetime
                            pstmt.setString(24	,rIssCode);//isscode
                            pstmt.setString(25	,rAquCode);//aqucode
                            pstmt.setString(26	,rAuthNo);//authno 
                            pstmt.setString(27	,rMessage1);//message1
                            pstmt.setString(28	,rMessage2);//message2
                            pstmt.setString(29	,rCardNo);//cardno
                            pstmt.setString(30	,rExpDate);//expdate
                            pstmt.setString(31	,rInstallment);//installment
                            pstmt.setString(32	,rAmount);//amount
                            pstmt.setString(33	,rMerchantNo);//merchantno
                            pstmt.setString(34	,rAuthSendType);//authsendtype
                            pstmt.setString(35	,rApprovalSendType);//approvalsendtype
                            pstmt.setString(36	,rPoint1);//point1
                            pstmt.setString(37	,rPoint2);//point2
                            pstmt.setString(38	,rPoint3);//point3
                            pstmt.setString(39	,rPoint4);//point4
                            pstmt.setString(40	,rVanTransactionNo);//vantransactionno
                            pstmt.setString(41	,rFiller);//filler
                            pstmt.setString(42	,rAuthType);//authtype
                            pstmt.setString(43	,rMPIPositionType);//mpipositiontype
                            pstmt.setString(44	,rMPIReUseType);//mpireusetype
                            pstmt.setString(45	,rFiller_1);//filler_1
                            pstmt.setString(46	,"00");//proc_flag

                            int intNotiIns = pstmt.executeUpdate();

                            conn.commit();    

                            closePstmt(pstmt);

                            logger.debug("-- onoffmerchinfo search --");                           

                            //수신된 정보로 가맹점 정보를 검색
                            StringBuffer sb_onffTidInfo = new StringBuffer();

                            sb_onffTidInfo.append(" select m.onfftid, m.onffmerch_no, m.pay_chn_cate, n.terminal_no, n.merch_no ");
                            sb_onffTidInfo.append(" from TB_ONFFTID_MST m, ");
                            sb_onffTidInfo.append("       ( ");
                            sb_onffTidInfo.append("           SELECT b.pay_mtd_seq, a.start_dt, a.end_dt, b.APP_ISS_CD, b.TID_MTD, b.TERMINAL_NO,b.PAY_MTD,b.MERCH_NO  ");
                            sb_onffTidInfo.append("            FROM TB_PAY_MTD a ");
                            sb_onffTidInfo.append("                      ,TB_PAY_MTD_DETAIL b ");
                            sb_onffTidInfo.append("            WHERE  ");
                            sb_onffTidInfo.append("            a.PAY_MTD_SEQ = b.PAY_MTD_SEQ ");
                            sb_onffTidInfo.append("            and b.DEL_FLAG  = 'N' ");
                            sb_onffTidInfo.append("            and b.USE_FLAG = 'Y' ");
                            sb_onffTidInfo.append("            and b.DEL_FLAG  = 'N' ");
                            sb_onffTidInfo.append("            and b.USE_FLAG = 'Y' ");
                            sb_onffTidInfo.append("            and a.start_dt <= date_format(now(), '%Y%m%d') ");
                            sb_onffTidInfo.append("            and a.end_dt  >= date_format(now(), '%Y%m%d') ");
                            sb_onffTidInfo.append("       ) n ");
                            sb_onffTidInfo.append(" where  ");
                            sb_onffTidInfo.append(" m.PAY_MTD_SEQ = n.pay_mtd_seq        ");
                            sb_onffTidInfo.append(" and m.del_flag = 'N'  ");
                            sb_onffTidInfo.append(" and n.MERCH_NO = ? ");

                            pstmt = conn.prepareStatement(sb_onffTidInfo.toString());

                            System.out.println("StoreId : ["+ StoreId +"]");
                            pstmt.setString(1, StoreId);

                            rs = pstmt.executeQuery();

                            String strOnffTid = "";
                            String strOnffMerchNo = "";
                            String strPayChnCate = "";
							String strTpTermNo = "";
							String strTpMerchNo = "";


                            if(rs != null && rs.next())
                            {
                                strOnffTid = getNullToSpace(rs.getString(1));
                                strOnffMerchNo = getNullToSpace(rs.getString(2));
                                strPayChnCate =  getNullToSpace(rs.getString(3));

								strTpTermNo =  getNullToSpace(rs.getString(4));
								strTpMerchNo =  getNullToSpace(rs.getString(5));
                            }

                            closeResultSet(rs);
                            closePstmt(pstmt);

                            logger.trace("strOnffTid : "+ strOnffTid);
                            logger.trace("strOnffMerchNo : "+ strOnffMerchNo);
                            logger.trace("strPayChnCate : " + strPayChnCate);

                            String strTpCms = "";
                            String strTpCompCate = null;
                            //승인이 가맹점정보가 있을시 수수료 정보를 가져온다.
                            if(!strOnffTid.equals("") && !strOnffMerchNo.equals("") && strTpAppCheck.equals("0"))
                            {
                            
                                //가맹점 사업자의 과세 종류를 가져온다.
                                StringBuffer sb_onffcompcate = new StringBuffer();
                                sb_onffcompcate.append(" select B.COMP_CATE ");
                                sb_onffcompcate.append(" from TB_ONFFMERCH_MST A, TB_COMPANY B  ");
                                sb_onffcompcate.append(" where A.COMP_SEQ = B.COMP_SEQ  ");
                                sb_onffcompcate.append(" and A.ONFFMERCH_NO = ?  ");

                                pstmt = conn.prepareStatement(sb_onffcompcate.toString());
                                pstmt.setString(1, strOnffMerchNo);
                                rs = pstmt.executeQuery();  

                                if(rs != null && rs.next())
                                {
                                    strTpCompCate = rs.getString(1);
                                }

                                System.out.println("strTpCompCate : " + strTpCompCate);

                                closeResultSet(rs);
                                closePstmt(pstmt);
                            
                                logger.debug("-- onofftidinfo commisioninfo search --");
                                //가맹점 정보에 대한 수수료 정보를 가져온다.
                                StringBuffer sb_onffcms = new StringBuffer();
                                sb_onffcms.append(" SELECT ONFFTID_CMS_SEQ, ONFFTID, COMMISSION, START_DT, END_DT ");
                                sb_onffcms.append("  FROM TB_ONFFTID_COMMISSION ");
                                sb_onffcms.append("  WHERE ONFFTID = ? ");
                                sb_onffcms.append("  and DEL_FLAG  = 'N' ");
                                sb_onffcms.append("  and USE_FLAG = 'Y' ");
                                sb_onffcms.append("  and START_DT <= ? ");
                                sb_onffcms.append("  and END_DT >= ? ");

                                String strTpAppDt = rTradeDate;

                                pstmt = conn.prepareStatement(sb_onffcms.toString());

                                pstmt.setString(1, strOnffTid);
                                pstmt.setString(2, strTpAppDt);
                                pstmt.setString(3, strTpAppDt);
                                rs = pstmt.executeQuery();  

                                if(rs != null && rs.next())
                                {
                                    strTpCms = rs.getString(3);
                                }
                                closeResultSet(rs);
                                closePstmt(pstmt);

                                logger.trace("strTpCms : " + strTpCms);
                            }            

                            //가맹점번호가 없을경우
                            if(strOnffTid.equals("") || strOnffMerchNo.equals(""))
                            {
                                logger.debug("-- onoff merch, tild info null --");
                                //noti처리상태 update
                                StringBuffer sb_noti_mod_sql = new StringBuffer();
                                sb_noti_mod_sql.append(" UPDATE tb_ksnet_noti SET ");
                                sb_noti_mod_sql.append(" proc_flag=? ");
                                sb_noti_mod_sql.append(" ,mod_dt = date_format(now(), '%Y%m%d%H%i%s') ");
                                sb_noti_mod_sql.append(" ,mod_user= '0' ");                
                                sb_noti_mod_sql.append(" WHERE noti_seq = ? ");
                                pstmt = conn.prepareStatement(sb_noti_mod_sql.toString());
                                pstmt.setString(1, "50");
                                pstmt.setString(2, strTpNotiseq);
                                int int_mod_notiinfo = pstmt.executeUpdate();  

                                closeResultSet(rs);
                                closePstmt(pstmt);
                            }
                            //승인이면서 수수료 정보가 없는경우
                            else if( strTpAppCheck.equals("O") && strTpCms.equals(""))
                            {
                                logger.debug("-- onoff tid cms info null --");
                                //noti처리상태 update
                                StringBuffer sb_noti_mod_sql = new StringBuffer();
                                sb_noti_mod_sql.append(" UPDATE tb_ksnet_noti SET ");
                                sb_noti_mod_sql.append(" proc_flag=? ");
                                sb_noti_mod_sql.append(" ,mod_dt = date_format(now(), '%Y%m%d%H%i%s') ");
                                sb_noti_mod_sql.append(" ,mod_user= '0' ");                
                                sb_noti_mod_sql.append(" WHERE noti_seq = ? ");
                                pstmt = conn.prepareStatement(sb_noti_mod_sql.toString());
                                pstmt.setString(1, "50");
                                pstmt.setString(2, strTpNotiseq);
                                int int_mod_notiinfo = pstmt.executeUpdate();  

                                closeResultSet(rs);
                                closePstmt(pstmt);
                            }
                            //정상건
                            else
                            {   
                                    //StoreId pg상점ID에 해당하는 수수료율을 가져온다.
                                    StringBuffer sb_merchInfo = new StringBuffer();

                                    sb_merchInfo.append(" select m.MERCH_NO, m.COMMISION ");
                                    sb_merchInfo.append(" from TB_MID_INFO m ");
                                    sb_merchInfo.append(" where  ");
                                    sb_merchInfo.append(" m.del_flag = 'N'  ");
                                    sb_merchInfo.append(" and m.MERCH_NO = ? ");

                                    pstmt = conn.prepareStatement(sb_merchInfo.toString());

                                    pstmt.setString(1, StoreId);

                                    rs = pstmt.executeQuery();
                                    
                                    String strTpVanCms = "0";

                                    if(rs != null && rs.next())
                                    {
                                        strTpVanCms = getNullToZero2(rs.getString(2));
                                    }
                                    
                                    closeResultSet(rs);
                                    closePstmt(pstmt);
                                    
                                    //Van수수료율을 계산한다.
                                    //수수료율 
                                    double dbl_TpVanCms = Double.parseDouble(strTpVanCms)/Double.parseDouble("100");
                                    logger.debug("dbl_TpCms : " +dbl_TpVanCms );

                                    double dbl_TpApp_mat_forvancms = Double.parseDouble(rAmount);
                                    logger.debug("dbl_TpApp_mat_forvancms : " +dbl_TpApp_mat_forvancms );

                                    //수수료 계산
                                    //double dbl_onfftid_cms = Math.floor(dbl_TpCms * dbl_TpApp_mat);
                                    double dbl_van_cms = Math.round(dbl_TpVanCms * dbl_TpApp_mat_forvancms);
                                    logger.debug("dbl_onfftid_cms : " +dbl_van_cms );

                                    //수수료 부가세 계산
                                    //double dbl_onfftid_cmsvat = Math.floor(dbl_onfftid_cms * 0.1);
                                    double dbl_van_cmsvat = Math.round(dbl_van_cms * 0.1);
                                    logger.debug("dbl_van_cmsvat : " +dbl_van_cmsvat );
                                    
                                    //van입금액 게산
                                    double dbl_van_payamt;
                                    //입금액 계산
                                    dbl_van_payamt = dbl_TpApp_mat_forvancms - dbl_van_cms - dbl_van_cmsvat;
                                    
                                    System.out.println("dbl_van_payamt : " + dbl_van_payamt);
                                    
                                    //van입금일을 계산한다.
                                     
                                    StringBuffer sb_vanpaydtInfo = new StringBuffer();
                                    sb_vanpaydtInfo.append(" select f_get_workday(?,5) from dual ");

                                    pstmt = conn.prepareStatement(sb_vanpaydtInfo.toString());

                                    pstmt.setString(1, rTradeDate);

                                    rs = pstmt.executeQuery();
                                    String strTpPaydt = "";
                                    
                                    if(rs != null && rs.next())
                                    {
                                        strTpPaydt = getNullToZero2(rs.getString(1));
                                    }
                                    
                                    closeResultSet(rs);
                                    closePstmt(pstmt);
                                    
                                    System.out.println("strTpPaydt : " + strTpPaydt);
                                    
                                    
                                    
                                    logger.debug("-- onoff info check sucess --");
                                    System.out.println("\n\n\n----------------------");
                                    System.out.println("strTpReustCheck : [" + strTpReustCheck + "]");
                                    System.out.println("strTpAppCheck : [" + strTpAppCheck + "]");
                                    System.out.println("----------------------\n\n\n");
                                    
                                     System.out.println("\n\n\n----------------------");
                                    System.out.println("tttttttttttttttt");
                                    System.out.println("----------------------\n\n\n");
                                    
                                    
                                    //승인 경우
                                    if(strTpReustCheck.equals("O") && strTpAppCheck.equals("0"))
                                    {
                                        logger.debug("-- notiinfo app --");
                                        //신용카드일 경우
                                        //if(r_pay_type.equals("11"))
                                        //{
                                            logger.debug("-- notiinfo app card --");

                                            //이미 입력거래가 있는경우
                                            StringBuffer sbTranChkSql = new StringBuffer();
                                            sbTranChkSql.append(" select count(1) cnt ");
                                            sbTranChkSql.append(" from VW_TRAN_CARDPG ");
                                            sbTranChkSql.append(" where pg_seq = ? ");
                                            //sbTranChkSql.append(" and TID = ? ");
											sbTranChkSql.append(" and merch_no = ? ");
                                            sbTranChkSql.append(" and massagetype='10' ");

                                            pstmt = conn.prepareStatement(sbTranChkSql.toString());

                                            pstmt.setString(1, rTransactionNo);
                                            pstmt.setString(2, StoreId);

                                            rs = pstmt.executeQuery();

                                            int int_appChk_cnt = 0;

                                            if(rs != null && rs.next())
                                            {
                                                int_appChk_cnt = rs.getInt("cnt");
                                            }

                                            closeResultSet(rs);
                                            closePstmt(pstmt);                    
                                            logger.debug("-- tran cnt chkeck --");
                                            String strNotResult = null;
                                            //접수된 승인건이 없는 경우
                                            if(int_appChk_cnt == 0)
                                            {
                                                logger.debug("-- tran cnt chkeck ok  --");
                                                logger.debug("-- isscd transger  --");
                                                //발급사코드 변환
                                                StringBuffer sb_isscd = new StringBuffer();
                                                sb_isscd.append(" select main_code, detail_code, code_nm detailcode_nm ");
                                                sb_isscd.append(" from TB_CODE_DETAIL ");
                                                sb_isscd.append(" where main_code = 'ISS_CD' ");
                                                sb_isscd.append(" and use_flag = 'Y' ");
                                                sb_isscd.append(" and del_flag='N' ");
                                                sb_isscd.append(" and detail_code = f_get_iss_cd('KSNETPG', ?) ");
                                                //xet.substring(xet.length()-2, xet.length())
                                                pstmt = conn.prepareStatement(sb_isscd.toString());
                                                pstmt.setString(1, rIssCode.substring(rIssCode.length()-2, rIssCode.length()));

                                                rs = pstmt.executeQuery();                            

                                                String strOnffIssCd = null;
                                                String strOnffIssNm = null;

                                                if(rs != null && rs.next())
                                                {
                                                    strOnffIssCd = rs.getString(2);
                                                    strOnffIssNm = rs.getString(3);
                                                }    
                                                else
                                                {
                                                    strOnffIssCd = "091";
                                                    strOnffIssNm = "기타카드";
                                                }
                                                closeResultSet(rs);
                                                closePstmt(pstmt);

                                                logger.debug("-- acqcd transger  --");
                                                //매입사코드변환
                                                StringBuffer sb_acqcd = new StringBuffer();
                                                sb_acqcd.append(" select main_code, detail_code, code_nm detailcode_nm ");
                                                sb_acqcd.append(" from TB_CODE_DETAIL ");
                                                sb_acqcd.append(" where main_code = 'ISS_CD' ");
                                                sb_acqcd.append(" and use_flag = 'Y' ");
                                                sb_acqcd.append(" and del_flag='N' ");
                                                sb_acqcd.append(" and detail_code = f_get_iss_cd('KSNETPG', ?) ");

                                                pstmt = conn.prepareStatement(sb_acqcd.toString());
                                                pstmt.setString(1, rAquCode.substring(rAquCode.length()-2, rAquCode.length()));

                                                rs = pstmt.executeQuery(); 

                                                String strOnffAcqCd = null;
                                                String strOnffAcqNm = null;

                                                if(rs != null && rs.next())
                                                {
                                                    strOnffAcqCd = rs.getString(2);
                                                    strOnffAcqNm = rs.getString(3);
                                                }
                                                else
                                                {
                                                    strOnffAcqCd = "091";
                                                    strOnffAcqNm = "기타카드";
                                                }
                                                closeResultSet(rs);
                                                closePstmt(pstmt);

                                                logger.debug("-- isscd transger  --");
                                                //수수료를 계산한다.
                                                //수수료율 
                                                double dbl_TpCms = Double.parseDouble(strTpCms)/Double.parseDouble("100");
                                                logger.debug("dbl_TpCms : " +dbl_TpCms );

                                                double dbl_TpApp_mat = Double.parseDouble(rAmount);
                                                logger.debug("dbl_TpApp_mat : " +dbl_TpApp_mat );

                                                //수수료 계산
                                                //double dbl_onfftid_cms = Math.floor(dbl_TpCms * dbl_TpApp_mat);
                                                double dbl_onfftid_cms = Math.round(dbl_TpCms * dbl_TpApp_mat);
                                                logger.debug("dbl_onfftid_cms : " +dbl_onfftid_cms );

                                                //수수료 부가세 계산
                                                //double dbl_onfftid_cmsvat = Math.floor(dbl_onfftid_cms * 0.1);
                                                double dbl_onfftid_cmsvat = Math.round(dbl_onfftid_cms * 0.1);
                                                logger.debug("dbl_onfftid_cmsvat : " +dbl_onfftid_cmsvat );

                                                //원천징수 세액 계산
                                                //double dbl_withholdtax = Math.floor(dbl_TpApp_mat * 0.033);
                                                double dbl_withholdtax = Math.round(dbl_TpApp_mat * 0.033);
                                                logger.debug("dbl_withholdtax : " + dbl_withholdtax );


                                                double dbl_onfftid_payamt;
                                                //지급액 계산
                                                if(strTpCompCate.equals("3"))
                                                {
                                                    dbl_onfftid_payamt = dbl_TpApp_mat - dbl_onfftid_cms - dbl_onfftid_cmsvat - dbl_withholdtax;
                                                }
                                                else
                                                {
                                                    dbl_onfftid_payamt = dbl_TpApp_mat - dbl_onfftid_cms - dbl_onfftid_cmsvat;

                                                    dbl_withholdtax = 0;
                                                }


                                                logger.debug("-- strTpTranseq create  --");
                                                //거래일련번호를 생성한다.
                                                String strTpTranseq = null;
                                                String sb_TpTranseqSql = "SELECT concat(date_format(now(), '%Y%m%d%H%i%s'),'I',LPAD(f_get_sequence('CARDTRANSEQ'),10,'0')) AS seq";
                                                pstmt = conn.prepareStatement(sb_TpTranseqSql);
                                                rs = pstmt.executeQuery();
                                                if(rs != null && rs.next())
                                                {
                                                    strTpTranseq = rs.getString(1);
                                                }            
                                                
                                                conn.commit();

                                                closeResultSet(rs);
                                                closePstmt(pstmt);
                                                logger.debug("strTpTranseq : "+strTpTranseq);
                                                logger.debug("-- traninfo insert  --");
                                                //해당거래정보를 insert 한다.
                                                StringBuffer sb_instraninfo = new StringBuffer();
                                                sb_instraninfo.append(" insert ");
                                                sb_instraninfo.append(" into TB_TRAN_CARDPG_"+rTradeDate.substring(2, 6) + " ( ");
                                                sb_instraninfo.append(" TRAN_SEQ			 ");
                                                sb_instraninfo.append(" ,MASSAGETYPE		 ");
                                                sb_instraninfo.append(" ,CARD_NUM			 ");
                                                sb_instraninfo.append(" ,APP_DT				 ");
                                                sb_instraninfo.append(" ,APP_TM				 ");
                                                sb_instraninfo.append(" ,APP_NO				 ");
                                                sb_instraninfo.append(" ,TOT_AMT			 ");	
                                                sb_instraninfo.append(" ,PG_SEQ				 ");
                                                sb_instraninfo.append(" ,PAY_CHN_CATE		 ");	
                                                sb_instraninfo.append(" ,TID				 ");	
                                                sb_instraninfo.append(" ,ONFFTID			 ");	
                                                sb_instraninfo.append(" ,WCC				 ");	
                                                sb_instraninfo.append(" ,EXPIRE_DT			 ");
                                                sb_instraninfo.append(" ,CARD_CATE			 ");
                                                sb_instraninfo.append(" ,ORDER_SEQ			 ");
                                                sb_instraninfo.append(" ,TRAN_DT			 ");
                                                sb_instraninfo.append(" ,TRAN_TM			 ");	
                                                sb_instraninfo.append(" ,TRAN_CATE			 ");
                                                sb_instraninfo.append(" ,FREE_INST_FLAG		 ");
                                                sb_instraninfo.append(" ,TAX_AMT			 ");	
                                                sb_instraninfo.append(" ,SVC_AMT			 ");	
                                                sb_instraninfo.append(" ,CURRENCY			 ");
                                                sb_instraninfo.append(" ,INSTALLMENT		 ");	
                                                sb_instraninfo.append(" ,ISS_CD				 ");
                                                sb_instraninfo.append(" ,ISS_NM				 ");
                                                sb_instraninfo.append(" ,APP_ISS_CD			 ");
                                                sb_instraninfo.append(" ,APP_ISS_NM			 ");
                                                sb_instraninfo.append(" ,ACQ_CD				 ");
                                                sb_instraninfo.append(" ,ACQ_NM			 ");
                                                sb_instraninfo.append(" ,APP_ACQ_CD			 ");
                                                sb_instraninfo.append(" ,APP_ACQ_NM			 ");
                                                sb_instraninfo.append(" ,MERCH_NO			 ");
                                                sb_instraninfo.append(" ,ORG_MERCH_NO		 ");
                                                sb_instraninfo.append(" ,RESULT_CD			 ");
                                                sb_instraninfo.append(" ,RESULT_MSG			 ");
                                                sb_instraninfo.append(" ,ORG_APP_DD			 ");
                                                sb_instraninfo.append(" ,ORG_APP_NO			 ");
                                                sb_instraninfo.append(" ,CNCL_REASON		 ");	
                                                sb_instraninfo.append(" ,TRAN_STATUS		 ");	
                                                sb_instraninfo.append(" ,RESULT_STATUS		 ");
                                                sb_instraninfo.append(" ,TRAN_STEP			 ");
                                                sb_instraninfo.append(" ,CNCL_DT			 ");	
                                                sb_instraninfo.append(" ,ACQ_DT				 ");
                                                sb_instraninfo.append(" ,ACQ_RESULT_CD		 ");
                                                sb_instraninfo.append(" ,HOLDOFF_DT			 ");
                                                sb_instraninfo.append(" ,HOLDON_DT			 ");
                                                sb_instraninfo.append(" ,PAY_DT				 ");
                                                sb_instraninfo.append(" ,PAY_AMT			 ");	
                                                sb_instraninfo.append(" ,COMMISION			 ");
                                                sb_instraninfo.append(" ,ONOFFTID_PAY_AMT	 ");	
                                                sb_instraninfo.append(" ,ONOFFTID_COMMISION	 ");
                                                sb_instraninfo.append(" ,CLIENT_IP			 ");
                                                sb_instraninfo.append(" ,USER_TYPE			 ");
                                                sb_instraninfo.append(" ,MEMB_USER_NO		 ");
                                                sb_instraninfo.append(" ,USER_ID				 ");
                                                sb_instraninfo.append(" ,USER_NM			 ");
                                                sb_instraninfo.append(" ,USER_MAIL			 ");
                                                sb_instraninfo.append(" ,USER_PHONE1		 ");
                                                sb_instraninfo.append(" ,USER_PHONE2		 ");
                                                sb_instraninfo.append(" ,USER_ADDR			 ");
                                                sb_instraninfo.append(" ,PRODUCT_TYPE		 ");
                                                sb_instraninfo.append(" ,PRODUCT_NM		 ");
                                                sb_instraninfo.append(" ,FILLER				 ");
                                                sb_instraninfo.append(" ,FILLER2				 ");
                                                sb_instraninfo.append(" ,TERM_FILLER1		 ");
                                                sb_instraninfo.append(" ,TERM_FILLER2		 ");	
                                                sb_instraninfo.append(" ,TERM_FILLER3		 ");	
                                                sb_instraninfo.append(" ,TERM_FILLER4		 ");	
                                                sb_instraninfo.append(" ,TERM_FILLER5		 ");	
                                                sb_instraninfo.append(" ,TERM_FILLER6		 ");	
                                                sb_instraninfo.append(" ,TERM_FILLER7		 ");	
                                                sb_instraninfo.append(" ,TERM_FILLER8		 ");	
                                                sb_instraninfo.append(" ,TERM_FILLER9		 ");	
                                                sb_instraninfo.append(" ,TERM_FILLER10		 ");
                                                sb_instraninfo.append(" ,TRAD_CHK_FLAG		 ");
                                                sb_instraninfo.append(" ,ACC_CHK_FLAG		 ");
                                                sb_instraninfo.append(" ,INS_DT				 ");
                                                sb_instraninfo.append(" ,MOD_DT				 ");
                                                sb_instraninfo.append(" ,INS_USER			 ");	
                                                sb_instraninfo.append(" ,MOD_USER			 ");
                                                sb_instraninfo.append(" ,ORG_PG_SEQ			 ");
                                                sb_instraninfo.append(" ,ONFFMERCH_NO	 ");
                                                sb_instraninfo.append(" ,auth_pw ");
                                                sb_instraninfo.append(" ,auth_value ");
                                                sb_instraninfo.append(" ,app_card_num ");
                                                sb_instraninfo.append(" ,ONOFFTID_VAT ");
                                                sb_instraninfo.append(" ,WITHHOLD_TAX ");
                                                sb_instraninfo.append(" )  ");
                                                sb_instraninfo.append(" values ( ");
                                                sb_instraninfo.append("  ? ");//tran_seq			
                                                sb_instraninfo.append(" ,? ");//massagetype			
                                                sb_instraninfo.append(" ,? ");//card_num			
                                                sb_instraninfo.append(" ,? ");//app_dt			
                                                sb_instraninfo.append(" ,? ");//app_tm			
                                                sb_instraninfo.append(" ,? ");//app_no			
                                                sb_instraninfo.append(" ,? ");//tot_amt			
                                                sb_instraninfo.append(" ,? ");//pg_seq			
                                                sb_instraninfo.append(" ,? ");//pay_chn_cate		
                                                sb_instraninfo.append(" ,? ");//tid				
                                                sb_instraninfo.append(" ,? ");//onfftid			
                                                sb_instraninfo.append(" ,? ");//wcc				
                                                sb_instraninfo.append(" ,? ");//expire_dt			
                                                sb_instraninfo.append(" ,? ");//card_cate			
                                                sb_instraninfo.append(" ,? ");//order_seq			
                                                sb_instraninfo.append(" ,? ");//tran_dt			
                                                sb_instraninfo.append(" ,? ");//tran_tm			
                                                sb_instraninfo.append(" ,? ");//tran_cate			
                                                sb_instraninfo.append(" ,? ");//free_inst_flag		
                                                sb_instraninfo.append(" ,? ");//tax_amt			
                                                sb_instraninfo.append(" ,? ");//svc_amt			
                                                sb_instraninfo.append(" ,? ");//currency			
                                                sb_instraninfo.append(" ,? ");//installment			
                                                sb_instraninfo.append(" ,? ");//iss_cd			
                                                sb_instraninfo.append(" ,? ");//iss_nm			
                                                sb_instraninfo.append(" ,? ");//app_iss_cd			
                                                sb_instraninfo.append(" ,? ");//app_iss_nm			
                                                sb_instraninfo.append(" ,? ");//acq_cd			
                                                sb_instraninfo.append(" ,? ");//acq_nm			
                                                sb_instraninfo.append(" ,? ");//app_acq_cd			
                                                sb_instraninfo.append(" ,? ");//app_acq_nm			
                                                sb_instraninfo.append(" ,? ");//merch_no			
                                                sb_instraninfo.append(" ,? ");//org_merch_no		
                                                sb_instraninfo.append(" ,? ");//result_cd			
                                                sb_instraninfo.append(" ,? ");//result_msg			
                                                sb_instraninfo.append(" ,? ");//org_app_dd			
                                                sb_instraninfo.append(" ,? ");//org_app_no			
                                                sb_instraninfo.append(" ,? ");//cncl_reason			
                                                sb_instraninfo.append(" ,? ");//tran_status			
                                                sb_instraninfo.append(" ,? ");//result_status		
                                                sb_instraninfo.append(" ,? ");//tran_step			
                                                sb_instraninfo.append(" ,? ");//cncl_dt			
                                                sb_instraninfo.append(" ,? ");//acq_dt			
                                                sb_instraninfo.append(" ,? ");//acq_result_cd
                                                sb_instraninfo.append(" ,? ");//holdoff_dt
                                                sb_instraninfo.append(" ,? ");//holdon_dt
                                                sb_instraninfo.append(" ,? ");//pay_dt
                                                sb_instraninfo.append(" ,? ");//pay_amt
                                                sb_instraninfo.append(" ,? ");//commision
                                                sb_instraninfo.append(" ,? ");//onofftid_pay_amt
                                                sb_instraninfo.append(" ,? ");//onofftid_commision
                                                sb_instraninfo.append(" ,? ");//client_ip
                                                sb_instraninfo.append(" ,? ");//user_type
                                                sb_instraninfo.append(" ,? ");//memb_user_no
                                                sb_instraninfo.append(" ,? ");//user_id
                                                sb_instraninfo.append(" ,? ");//user_nm
                                                sb_instraninfo.append(" ,? ");//user_mail
                                                sb_instraninfo.append(" ,? ");//user_phone1
                                                sb_instraninfo.append(" ,? ");//user_phone2
                                                sb_instraninfo.append(" ,? ");//user_addr
                                                sb_instraninfo.append(" ,? ");//product_type
                                                sb_instraninfo.append(" ,? ");//product_nm
                                                sb_instraninfo.append(" ,? ");//filler
                                                sb_instraninfo.append(" ,? ");//filler2
                                                sb_instraninfo.append(" ,? ");//term_filler1
                                                sb_instraninfo.append(" ,? ");//term_filler2
                                                sb_instraninfo.append(" ,? ");//term_filler3
                                                sb_instraninfo.append(" ,? ");//term_filler4
                                                sb_instraninfo.append(" ,? ");//term_filler5
                                                sb_instraninfo.append(" ,? ");//term_filler6
                                                sb_instraninfo.append(" ,? ");//term_filler7
                                                sb_instraninfo.append(" ,? ");//term_filler8
                                                sb_instraninfo.append(" ,? ");//term_filler9
                                                sb_instraninfo.append(" ,? ");//term_filler10
                                                sb_instraninfo.append(" ,? ");//trad_chk_flag
                                                sb_instraninfo.append(" ,? ");//acc_chk_flag
                                                sb_instraninfo.append("  , date_format(now(), '%Y%m%d%H%i%s') ");
                                                sb_instraninfo.append("  , date_format(now(), '%Y%m%d%H%i%s') ");
                                                sb_instraninfo.append(" ,? ");//ins_user			
                                                sb_instraninfo.append(" ,? ");//mod_user			
                                                sb_instraninfo.append(" ,? ");//org_pg_seq			
                                                sb_instraninfo.append(" ,? ");//onffmerch_no		
                                                sb_instraninfo.append(" ,? ");//auth_pw		  
                                                sb_instraninfo.append(" ,? ");//auth_value		  
                                                sb_instraninfo.append(" ,? ");//app_card_num	
                                                sb_instraninfo.append(" ,? ");//ONOFFTID_VAT		  
                                                sb_instraninfo.append(" ,? ");//WITHHOLD_TAX
                                                sb_instraninfo.append(" ) ");          

                                                pstmt = conn.prepareStatement(sb_instraninfo.toString());

                                                pstmt.setString(1,strTpTranseq);//tran_seq				, jdbcType=VARCHAR} 
                                                pstmt.setString(2,"10");//massagetype			, jdbcType=VARCHAR} 
                                                pstmt.setString(3,  rCardNo);//card_num			, jdbcType=VARCHAR} 
                                                pstmt.setString(4,  rTradeDate);//app_dt				, jdbcType=VARCHAR}
                                                pstmt.setString(5,  rTradeTime);//app_tm				 , jdbcType=VARCHAR}
                                                pstmt.setString(6,  rAuthNo);//app_no				, jdbcType=VARCHAR}
                                                pstmt.setString(7,  rAmount);//tot_amt				 , jdbcType=INTEGER }
                                                pstmt.setString(8,  rTransactionNo);//pg_seq				, jdbcType=VARCHAR}
                                                pstmt.setString(9,  strPayChnCate);//pay_chn_cate			 , jdbcType=VARCHAR}
                                                //pstmt.setString(10, strCol2);//tid					, jdbcType=VARCHAR}
                                                //pstmt.setString(10, StoreId);//tid					, jdbcType=VARCHAR}
												pstmt.setString(10, strTpTermNo);//tid					, jdbcType=VARCHAR}
                                                pstmt.setString(11, strOnffTid);//onfftid				, jdbcType=VARCHAR}
                                                pstmt.setString(12, null);//wcc					, jdbcType=VARCHAR}
                                                pstmt.setString(13,"0000");//expire_dt			, jdbcType=VARCHAR}
                                                pstmt.setString(14, null);//card_cate			  , jdbcType=VARCHAR}
                                                pstmt.setString(15, null);//order_seq			  , jdbcType=VARCHAR}
                                                pstmt.setString(16, rTradeDate);//tran_dt				, jdbcType=VARCHAR}
                                                pstmt.setString(17, rTradeTime);//tran_tm				, jdbcType=VARCHAR}
                                                //pstmt.setString(18,r_noti_type);//tran_cate			  , jdbcType=VARCHAR}
                                                pstmt.setString(18,"20");//tran_cate			  , jdbcType=VARCHAR}
                                                pstmt.setString(19, null);//free_inst_flag		    , jdbcType=VARCHAR}
                                                pstmt.setDouble(20,0);//tax_amt				, jdbcType=INTEGER }
                                                pstmt.setDouble(21,0);//svc_amt				, jdbcType=INTEGER }
                                                pstmt.setString(22,null);//currency			 , jdbcType=VARCHAR}
                                                pstmt.setString(23,rInstallment);//installment			   , jdbcType=VARCHAR}
                                                pstmt.setString(24, strOnffIssCd);//iss_cd				, jdbcType=VARCHAR}
                                                pstmt.setString(25, strOnffIssNm);//iss_nm				 , jdbcType=VARCHAR}
                                                pstmt.setString(26, rIssCode);//app_iss_cd			 , jdbcType=VARCHAR}
                                                pstmt.setString(27, null);//app_iss_nm			 , jdbcType=VARCHAR}
                                                pstmt.setString(28, strOnffAcqCd);//acq_cd				, jdbcType=VARCHAR}
                                                pstmt.setString(29, strOnffAcqNm);//acq_nm				, jdbcType=VARCHAR}
                                                pstmt.setString(30, rAquCode);//app_acq_cd			  , jdbcType=VARCHAR}
                                                pstmt.setString(31, null);//app_acq_nm			   , jdbcType=VARCHAR}
                                                pstmt.setString(32, StoreId);//merch_no			  , jdbcType=VARCHAR}
                                                pstmt.setString(33, rMerchantNo);//org_merch_no			  , jdbcType=VARCHAR}
                                                pstmt.setString(34, "0000");//result_cd			 , jdbcType=VARCHAR}
                                                pstmt.setString(35, "성공");//result_msg			   , jdbcType=VARCHAR}
                                                pstmt.setString(36,null);//org_app_dd			  , jdbcType=VARCHAR}
                                                pstmt.setString(37,null);//org_app_no			   , jdbcType=VARCHAR}
                                                pstmt.setString(38,null);//cncl_reason			, jdbcType=VARCHAR}
                                                pstmt.setString(39,"00");//tran_status			   , jdbcType=VARCHAR}
                                                pstmt.setString(40,"00");//result_status		     , jdbcType=VARCHAR}
                                                pstmt.setString(41,"00");//tran_step			 , jdbcType=VARCHAR}
                                                pstmt.setString(42,null);//cncl_dt				, jdbcType=VARCHAR}
                                                pstmt.setString(43,null);//acq_dt				, jdbcType=VARCHAR}
                                                pstmt.setString(44,null);//acq_result_cd		     , jdbcType=VARCHAR}
                                                pstmt.setString(45,null);//holdoff_dt			  , jdbcType=VARCHAR}
                                                pstmt.setString(46,null);//holdon_dt			  , jdbcType=VARCHAR}
                                                
												/*
                                                pstmt.setString(47,strTpPaydt);//pay_dt				, jdbcType=VARCHAR}
                                                pstmt.setDouble(48, dbl_van_payamt);//pay_amt				, jdbcType=INTEGER }
                                                pstmt.setDouble(49, dbl_van_cms+dbl_van_cmsvat);//commision	, jdbcType=INTEGER }
                                                */

												pstmt.setString(47, null);//pay_dt				, jdbcType=VARCHAR}
                                                pstmt.setString(48, null);//pay_amt				, jdbcType=INTEGER }
                                                pstmt.setString(49, null);//commision			  , jdbcType=INTEGER }
                                                
                                                pstmt.setDouble(50,dbl_onfftid_payamt);//onofftid_pay_amt		, jdbcType=INTEGER }
                                                pstmt.setDouble(51,dbl_onfftid_cms);//onofftid_commision		, jdbcType=INTEGER }
                                                pstmt.setString(52,null);//client_ip				, jdbcType=VARCHAR}
                                                pstmt.setString(53,null);//user_type			, jdbcType=VARCHAR}
                                                pstmt.setString(54,null);//memb_user_no		, jdbcType=VARCHAR}
                                                pstmt.setString(55,null);//user_id				, jdbcType=VARCHAR}
                                                pstmt.setString(56,null);//user_nm				, jdbcType=VARCHAR}
                                                pstmt.setString(57,null);//user_mail			    , jdbcType=VARCHAR}
                                                pstmt.setString(58,null);//user_phone1			 , jdbcType=VARCHAR}
                                                pstmt.setString(59,null);//user_phone2			 , jdbcType=VARCHAR}
                                                pstmt.setString(60,null);//user_addr			, jdbcType=VARCHAR}
                                                pstmt.setString(61,null);//product_type		  , jdbcType=VARCHAR}
                                                pstmt.setString(62,null);//product_nm			 , jdbcType=VARCHAR}
                                                pstmt.setString(63,null);//filler				   , jdbcType=VARCHAR}
                                                pstmt.setString(64,null);//filler2				   , jdbcType=VARCHAR}
                                                pstmt.setString(65,null);//term_filler1			, jdbcType=VARCHAR}
                                                pstmt.setString(66,null);//term_filler2			, jdbcType=VARCHAR}
                                                pstmt.setString(67,null);//term_filler3			, jdbcType=VARCHAR}
                                                pstmt.setString(68,null);//term_filler4			, jdbcType=VARCHAR}
                                                pstmt.setString(69,null);//term_filler5			, jdbcType=VARCHAR}
                                                pstmt.setString(70,null);//term_filler6				, jdbcType=VARCHAR}
                                                pstmt.setString(71,null);//term_filler7			  , jdbcType=VARCHAR}
                                                pstmt.setString(72,null);//term_filler8			, jdbcType=VARCHAR}
                                                pstmt.setString(73,null);//term_filler9			, jdbcType=VARCHAR}
                                                pstmt.setString(74,null);//term_filler10			, jdbcType=VARCHAR}
                                                pstmt.setString(75,"0");//trad_chk_flag		  , jdbcType=VARCHAR}
                                                pstmt.setString(76,"0");//acc_chk_flag			, jdbcType=VARCHAR}
                                                pstmt.setString(77,"0");//ins_user				     , jdbcType=VARCHAR}
                                                pstmt.setString(78,"0");//mod_user			, jdbcType=VARCHAR}
                                                pstmt.setString(79,null);//org_pg_seq			, jdbcType=VARCHAR}
                                                pstmt.setString(80,strOnffMerchNo);//onffmerch_no		  , jdbcType=VARCHAR}
                                                pstmt.setString(81,null);//auth_pw		  , jdbcType=VARCHAR}
                                                pstmt.setString(82,null);//auth_value		  , jdbcType=VARCHAR}   
                                                pstmt.setString(83,rCardNo);//app_card_num		  , jdbcType=VARCHAR}                
                                                pstmt.setDouble(84,dbl_onfftid_cmsvat);//ONOFFTID_VAT		  , jdbcType=VARCHAR}     
                                                pstmt.setDouble(85,dbl_withholdtax);//WITHHOLD_TAX		  , jdbcType=VARCHAR}            

                                                int int_insResult = pstmt.executeUpdate();                               
                                                logger.debug("-- traninfo insert end  --");
                                                closeResultSet(rs);
                                                closePstmt(pstmt);

                                                strNotResult = "10";//처리완료
                                            }
                                            //접수된 승인건이 있는 경우
                                            else
                                            {
                                                logger.debug("-- traninfo dual!!  --");
                                                strNotResult = "20";//기존재건 존재
                                            }

                                            logger.debug("-- notiinfo update!!  --");
                                            //noti처리상태 update
                                            StringBuffer sb_noti_mod_sql = new StringBuffer();
                                            sb_noti_mod_sql.append(" UPDATE tb_ksnet_noti SET ");
                                            sb_noti_mod_sql.append(" proc_flag=? ");
                                            sb_noti_mod_sql.append(" ,mod_dt = date_format(now(), '%Y%m%d%H%i%s') ");
                                            sb_noti_mod_sql.append(" ,mod_user= '0' ");                
                                            sb_noti_mod_sql.append(" WHERE noti_seq = ? ");

                                            pstmt = conn.prepareStatement(sb_noti_mod_sql.toString());

                                            pstmt.setString(1, strNotResult);
                                            pstmt.setString(2, strTpNotiseq);

                                            int int_mod_notiinfo = pstmt.executeUpdate();
                                        //}
                                    }
                                    //취소일경우
                                    else if(strTpReustCheck.equals("O") && strTpAppCheck.equals("1"))
                                    {
                                         logger.debug("-- noti cncl  --");
                                        //카드취소일 경우
                                        //if(r_pay_type.equals("11"))
                               
                                            logger.debug("-- noti cncl card  --");

                                            //이미 입력거래가 있는경우
                                            StringBuffer sbTranChkSql = new StringBuffer();
                                            sbTranChkSql.append(" select count(1) cnt ");
                                            sbTranChkSql.append(" from VW_TRAN_CARDPG ");
                                            sbTranChkSql.append(" where pg_seq = ? ");
                                            //sbTranChkSql.append(" and TID = ? ");
											sbTranChkSql.append(" and merch_no = ? ");
                                            sbTranChkSql.append(" and massagetype='40' ");
                                            sbTranChkSql.append(" and RESULT_STATUS='00' ");

                                            pstmt = conn.prepareStatement(sbTranChkSql.toString());

                                            pstmt.setString(1, rTransactionNo);
                                            pstmt.setString(2, StoreId);

                                            rs = pstmt.executeQuery();

                                            int int_CnclChk_cnt = 0;

                                            if(rs != null && rs.next())
                                            {
                                                int_CnclChk_cnt = rs.getInt("cnt");
                                            }

                                            closeResultSet(rs);
                                            closePstmt(pstmt);                    
                                           logger.debug("-- tran cnt chkeck --");
                                            String strNotResult = null;
                                            //접수된 승인건이 없는 경우
                                            if(int_CnclChk_cnt == 0)
                                            {
                                                logger.debug("-- tran cnt chkeck ok  --");
                                                //원거래를 가져온다.
                                                StringBuffer sb_orgtraninfoSql = new StringBuffer();

                                                sb_orgtraninfoSql.append(" select  b.comp_nm, b.biz_no, b.corp_no, b.comp_ceo_nm, b.comp_tel1, b.tax_flag ,b.merch_nm,  b.app_chk_amt, b.addr_1, b.addr_2 ");
                                                sb_orgtraninfoSql.append("         ,a.tran_seq,a.massagetype,a.card_num ");
                                                sb_orgtraninfoSql.append("         ,a.app_dt ");
                                                sb_orgtraninfoSql.append("         ,a.app_tm         ");
                                                sb_orgtraninfoSql.append("         ,a.app_no,a.tot_amt,a.pg_seq,a.tid ");
                                                sb_orgtraninfoSql.append("         ,a.pay_chn_cate,a.onfftid,a.onffmerch_no,a.wcc ");
                                                sb_orgtraninfoSql.append("         ,a.expire_dt,a.card_cate,a.order_seq ");
                                                sb_orgtraninfoSql.append("         ,a.tran_dt,a.tran_tm,a.tran_cate,a.free_inst_flag ");
                                                sb_orgtraninfoSql.append("         ,a.tax_amt,a.svc_amt,a.currency ");
                                                sb_orgtraninfoSql.append("         ,a.installment,a.iss_cd,a.iss_nm ");
                                                sb_orgtraninfoSql.append("         ,a.app_iss_cd,a.app_iss_nm,a.acq_cd ");
                                                sb_orgtraninfoSql.append("         ,a.acq_nm,a.app_acq_cd,a.app_acq_nm ");
                                                sb_orgtraninfoSql.append("         ,a.merch_no,a.org_merch_no,a.result_cd,a.result_msg ");
                                                sb_orgtraninfoSql.append("         ,a.tran_status,a.result_status ");
                                                sb_orgtraninfoSql.append("         ,a.org_app_dd,a.org_app_no,a.cncl_reason ");
                                                sb_orgtraninfoSql.append("         ,a.tran_step,a.cncl_dt,a.acq_dt,a.acq_result_cd ");
                                                sb_orgtraninfoSql.append("         ,a.holdoff_dt,a.holdon_dt,a.pay_dt,a.pay_amt ");
                                                sb_orgtraninfoSql.append("         ,a.commision,a.client_ip,a.user_type ");
                                                sb_orgtraninfoSql.append("         ,a.memb_user_no,a.user_id,a.user_nm ");
                                                sb_orgtraninfoSql.append("         ,a.user_mail,a.user_phone1,a.user_phone2 ");
                                                sb_orgtraninfoSql.append("         ,a.user_addr,a.product_type,a.product_nm ");
                                                sb_orgtraninfoSql.append("         ,a.filler,a.filler2,a.term_filler1,a.term_filler2 ");
                                                sb_orgtraninfoSql.append("         ,a.term_filler3,a.term_filler4,a.term_filler5 ");
                                                sb_orgtraninfoSql.append("         ,a.term_filler6,a.term_filler7,a.term_filler8 ");
                                                sb_orgtraninfoSql.append("         ,a.term_filler9,a.term_filler10,a.trad_chk_flag ");
                                                sb_orgtraninfoSql.append("         ,a.acc_chk_flag,a.ins_dt,a.mod_dt,a.ins_user,a.mod_user,a.onofftid_pay_amt,a.onofftid_commision,a.tb_nm, a.app_card_num,a.ONOFFTID_VAT,a.WITHHOLD_TAX	 	 ");
                                                sb_orgtraninfoSql.append(" from VW_TRAN_CARDPG a ");
                                                sb_orgtraninfoSql.append("          ,( ");
                                                sb_orgtraninfoSql.append("               SELECT x1.comp_nm, x1.BIZ_NO, x1.CORP_NO, x1.COMP_CEO_NM, x1.COMP_TEL1, x1.addr_1, x1.addr_2, x1.TAX_FLAG , x.ONFFMERCH_NO, x.MERCH_NM,  x.APP_CHK_AMT ");
                                                sb_orgtraninfoSql.append("                FROM  TB_ONFFMERCH_MST x, ");
                                                sb_orgtraninfoSql.append("                          TB_COMPANY x1 ");
                                                sb_orgtraninfoSql.append("                WHERE                  ");
                                                sb_orgtraninfoSql.append("                          x.del_flag = 'N' ");
                                                sb_orgtraninfoSql.append("                        AND x.SVC_STAT = '00' ");
                                                sb_orgtraninfoSql.append("                        AND x1.del_flag = 'N' ");
                                                sb_orgtraninfoSql.append("                        AND x1.use_flag = 'Y'                 ");
                                                sb_orgtraninfoSql.append("                        AND  X.COMP_SEQ = x1.comp_seq ");
                                                sb_orgtraninfoSql.append("          ) b ");
                                                sb_orgtraninfoSql.append(" where ");
                                                sb_orgtraninfoSql.append("      a.ONFFMERCH_NO = b.ONFFMERCH_NO ");
                                                sb_orgtraninfoSql.append(" and       ");
                                                sb_orgtraninfoSql.append("     a.pg_seq =? ");
                                                //sb_orgtraninfoSql.append(" and a.tid = ? ");
												sb_orgtraninfoSql.append(" and a.merch_no = ? ");
                                                sb_orgtraninfoSql.append(" and    a.massagetype='10' ");
                                                sb_orgtraninfoSql.append(" and    a.result_status='00' ");

                                                pstmt2 = conn.prepareStatement(sb_orgtraninfoSql.toString());
                                                pstmt2.setString(1, rTransactionNo);
                                                pstmt2.setString(2, StoreId);
                                                rs2 = pstmt2.executeQuery();

                                                //원거래정보
                                                //원거래정보가 있는 경우
                                                if(rs2 != null && rs2.next())
                                                {
                                                    logger.debug("-- org tran  exist  --");
                                                    logger.debug("-- strTpTranseq create  --");
                                                    //거래일련번호를 생성한다.
                                                    String strTpTranseq = null;
                                                    String sb_TpTranseqSql = "SELECT concat(date_format(now(), '%Y%m%d%H%i%s'),'I',LPAD(f_get_sequence('CARDTRANSEQ'),10,'0')) AS seq";
                                                    pstmt = conn.prepareStatement(sb_TpTranseqSql);
                                                    rs = pstmt.executeQuery();
                                                    if(rs != null && rs.next())
                                                    {
                                                        strTpTranseq = rs.getString(1);
                                                    }                
                                                    conn.commit();
                                                    closeResultSet(rs);
                                                    closePstmt(pstmt);
                                                    logger.debug("strTpTranseq : "+strTpTranseq);
                                                    logger.debug("-- traninfo insert  --");
                                                    //취소정보를 insert한다.
                                                    StringBuffer sb_instraninfo = new StringBuffer();
                                                    sb_instraninfo.append(" insert ");
                                                    sb_instraninfo.append(" into TB_TRAN_CARDPG_"+rTradeDate.substring(2, 6) + " ( ");
                                                        sb_instraninfo.append(" TRAN_SEQ			 ");
                                                        sb_instraninfo.append(" ,MASSAGETYPE		 ");
                                                        sb_instraninfo.append(" ,CARD_NUM			 ");
                                                        sb_instraninfo.append(" ,APP_DT				 ");
                                                        sb_instraninfo.append(" ,APP_TM				 ");
                                                        sb_instraninfo.append(" ,APP_NO				 ");
                                                        sb_instraninfo.append(" ,TOT_AMT			 ");	
                                                        sb_instraninfo.append(" ,PG_SEQ				 ");
                                                        sb_instraninfo.append(" ,PAY_CHN_CATE		 ");	
                                                        sb_instraninfo.append(" ,TID				 ");	
                                                        sb_instraninfo.append(" ,ONFFTID			 ");	
                                                        sb_instraninfo.append(" ,WCC				 ");	
                                                        sb_instraninfo.append(" ,EXPIRE_DT			 ");
                                                        sb_instraninfo.append(" ,CARD_CATE			 ");
                                                        sb_instraninfo.append(" ,ORDER_SEQ			 ");
                                                        sb_instraninfo.append(" ,TRAN_DT			 ");
                                                        sb_instraninfo.append(" ,TRAN_TM			 ");	
                                                        sb_instraninfo.append(" ,TRAN_CATE			 ");
                                                        sb_instraninfo.append(" ,FREE_INST_FLAG		 ");
                                                        sb_instraninfo.append(" ,TAX_AMT			 ");	
                                                        sb_instraninfo.append(" ,SVC_AMT			 ");	
                                                        sb_instraninfo.append(" ,CURRENCY			 ");
                                                        sb_instraninfo.append(" ,INSTALLMENT		 ");	
                                                        sb_instraninfo.append(" ,ISS_CD				 ");
                                                        sb_instraninfo.append(" ,ISS_NM				 ");
                                                        sb_instraninfo.append(" ,APP_ISS_CD			 ");
                                                        sb_instraninfo.append(" ,APP_ISS_NM			 ");
                                                        sb_instraninfo.append(" ,ACQ_CD				 ");
                                                        sb_instraninfo.append(" ,ACQ_NM			 ");
                                                        sb_instraninfo.append(" ,APP_ACQ_CD			 ");
                                                        sb_instraninfo.append(" ,APP_ACQ_NM			 ");
                                                        sb_instraninfo.append(" ,MERCH_NO			 ");
                                                        sb_instraninfo.append(" ,ORG_MERCH_NO		 ");
                                                        sb_instraninfo.append(" ,RESULT_CD			 ");
                                                        sb_instraninfo.append(" ,RESULT_MSG			 ");
                                                        sb_instraninfo.append(" ,ORG_APP_DD			 ");
                                                        sb_instraninfo.append(" ,ORG_APP_NO			 ");
                                                        sb_instraninfo.append(" ,CNCL_REASON		 ");	
                                                        sb_instraninfo.append(" ,TRAN_STATUS		 ");	
                                                        sb_instraninfo.append(" ,RESULT_STATUS		 ");
                                                        sb_instraninfo.append(" ,TRAN_STEP			 ");
                                                        sb_instraninfo.append(" ,CNCL_DT			 ");	
                                                        sb_instraninfo.append(" ,ACQ_DT				 ");
                                                        sb_instraninfo.append(" ,ACQ_RESULT_CD		 ");
                                                        sb_instraninfo.append(" ,HOLDOFF_DT			 ");
                                                        sb_instraninfo.append(" ,HOLDON_DT			 ");
                                                        sb_instraninfo.append(" ,PAY_DT				 ");
                                                        sb_instraninfo.append(" ,PAY_AMT			 ");	
                                                        sb_instraninfo.append(" ,COMMISION			 ");
                                                        sb_instraninfo.append(" ,ONOFFTID_PAY_AMT	 ");	
                                                        sb_instraninfo.append(" ,ONOFFTID_COMMISION	 ");
                                                        sb_instraninfo.append(" ,CLIENT_IP			 ");
                                                        sb_instraninfo.append(" ,USER_TYPE			 ");
                                                        sb_instraninfo.append(" ,MEMB_USER_NO		 ");
                                                        sb_instraninfo.append(" ,USER_ID				 ");
                                                        sb_instraninfo.append(" ,USER_NM			 ");
                                                        sb_instraninfo.append(" ,USER_MAIL			 ");
                                                        sb_instraninfo.append(" ,USER_PHONE1		 ");
                                                        sb_instraninfo.append(" ,USER_PHONE2		 ");
                                                        sb_instraninfo.append(" ,USER_ADDR			 ");
                                                        sb_instraninfo.append(" ,PRODUCT_TYPE		 ");
                                                        sb_instraninfo.append(" ,PRODUCT_NM		 ");
                                                        sb_instraninfo.append(" ,FILLER				 ");
                                                        sb_instraninfo.append(" ,FILLER2				 ");
                                                        sb_instraninfo.append(" ,TERM_FILLER1		 ");
                                                        sb_instraninfo.append(" ,TERM_FILLER2		 ");	
                                                        sb_instraninfo.append(" ,TERM_FILLER3		 ");	
                                                        sb_instraninfo.append(" ,TERM_FILLER4		 ");	
                                                        sb_instraninfo.append(" ,TERM_FILLER5		 ");	
                                                        sb_instraninfo.append(" ,TERM_FILLER6		 ");	
                                                        sb_instraninfo.append(" ,TERM_FILLER7		 ");	
                                                        sb_instraninfo.append(" ,TERM_FILLER8		 ");	
                                                        sb_instraninfo.append(" ,TERM_FILLER9		 ");	
                                                        sb_instraninfo.append(" ,TERM_FILLER10		 ");
                                                        sb_instraninfo.append(" ,TRAD_CHK_FLAG		 ");
                                                        sb_instraninfo.append(" ,ACC_CHK_FLAG		 ");
                                                        sb_instraninfo.append(" ,INS_DT				 ");
                                                        sb_instraninfo.append(" ,MOD_DT				 ");
                                                        sb_instraninfo.append(" ,INS_USER			 ");	
                                                        sb_instraninfo.append(" ,MOD_USER			 ");
                                                        sb_instraninfo.append(" ,ORG_PG_SEQ			 ");
                                                        sb_instraninfo.append(" ,ONFFMERCH_NO	 ");
                                                        sb_instraninfo.append(" ,auth_pw ");
                                                        sb_instraninfo.append(" ,auth_value ");
                                                        sb_instraninfo.append(" ,app_card_num ");
                                                        sb_instraninfo.append(" ,ONOFFTID_VAT ");
                                                        sb_instraninfo.append(" ,WITHHOLD_TAX ");
                                                        sb_instraninfo.append(" )  ");
                                                        sb_instraninfo.append(" values ( ");
                                                        sb_instraninfo.append("  ? ");//tran_seq			
                                                        sb_instraninfo.append(" ,? ");//massagetype			
                                                        sb_instraninfo.append(" ,? ");//card_num			
                                                        sb_instraninfo.append(" ,? ");//app_dt			
                                                        sb_instraninfo.append(" ,? ");//app_tm			
                                                        sb_instraninfo.append(" ,? ");//app_no			
                                                        sb_instraninfo.append(" ,? ");//tot_amt			
                                                        sb_instraninfo.append(" ,? ");//pg_seq			
                                                        sb_instraninfo.append(" ,? ");//pay_chn_cate		
                                                        sb_instraninfo.append(" ,? ");//tid				
                                                        sb_instraninfo.append(" ,? ");//onfftid			
                                                        sb_instraninfo.append(" ,? ");//wcc				
                                                        sb_instraninfo.append(" ,? ");//expire_dt			
                                                        sb_instraninfo.append(" ,? ");//card_cate			
                                                        sb_instraninfo.append(" ,? ");//order_seq			
                                                        sb_instraninfo.append(" ,? ");//tran_dt			
                                                        sb_instraninfo.append(" ,? ");//tran_tm			
                                                        sb_instraninfo.append(" ,? ");//tran_cate			
                                                        sb_instraninfo.append(" ,? ");//free_inst_flag		
                                                        sb_instraninfo.append(" ,? ");//tax_amt			
                                                        sb_instraninfo.append(" ,? ");//svc_amt			
                                                        sb_instraninfo.append(" ,? ");//currency			
                                                        sb_instraninfo.append(" ,? ");//installment			
                                                        sb_instraninfo.append(" ,? ");//iss_cd			
                                                        sb_instraninfo.append(" ,? ");//iss_nm			
                                                        sb_instraninfo.append(" ,? ");//app_iss_cd			
                                                        sb_instraninfo.append(" ,? ");//app_iss_nm			
                                                        sb_instraninfo.append(" ,? ");//acq_cd			
                                                        sb_instraninfo.append(" ,? ");//acq_nm			
                                                        sb_instraninfo.append(" ,? ");//app_acq_cd			
                                                        sb_instraninfo.append(" ,? ");//app_acq_nm			
                                                        sb_instraninfo.append(" ,? ");//merch_no			
                                                        sb_instraninfo.append(" ,? ");//org_merch_no		
                                                        sb_instraninfo.append(" ,? ");//result_cd			
                                                        sb_instraninfo.append(" ,? ");//result_msg			
                                                        sb_instraninfo.append(" ,? ");//org_app_dd			
                                                        sb_instraninfo.append(" ,? ");//org_app_no			
                                                        sb_instraninfo.append(" ,? ");//cncl_reason			
                                                        sb_instraninfo.append(" ,? ");//tran_status			
                                                        sb_instraninfo.append(" ,? ");//result_status		
                                                        sb_instraninfo.append(" ,? ");//tran_step			
                                                        sb_instraninfo.append(" ,? ");//cncl_dt			
                                                        sb_instraninfo.append(" ,? ");//acq_dt			
                                                        sb_instraninfo.append(" ,? ");//acq_result_cd
                                                        sb_instraninfo.append(" ,? ");//holdoff_dt
                                                        sb_instraninfo.append(" ,? ");//holdon_dt
                                                        sb_instraninfo.append(" ,? ");//pay_dt
                                                        sb_instraninfo.append(" ,? ");//pay_amt
                                                        sb_instraninfo.append(" ,? ");//commision
                                                        sb_instraninfo.append(" ,? ");//onofftid_pay_amt
                                                        sb_instraninfo.append(" ,? ");//onofftid_commision
                                                        sb_instraninfo.append(" ,? ");//client_ip
                                                        sb_instraninfo.append(" ,? ");//user_type
                                                        sb_instraninfo.append(" ,? ");//memb_user_no
                                                        sb_instraninfo.append(" ,? ");//user_id
                                                        sb_instraninfo.append(" ,? ");//user_nm
                                                        sb_instraninfo.append(" ,? ");//user_mail
                                                        sb_instraninfo.append(" ,? ");//user_phone1
                                                        sb_instraninfo.append(" ,? ");//user_phone2
                                                        sb_instraninfo.append(" ,? ");//user_addr
                                                        sb_instraninfo.append(" ,? ");//product_type
                                                        sb_instraninfo.append(" ,? ");//product_nm
                                                        sb_instraninfo.append(" ,? ");//filler
                                                        sb_instraninfo.append(" ,? ");//filler2
                                                        sb_instraninfo.append(" ,? ");//term_filler1
                                                        sb_instraninfo.append(" ,? ");//term_filler2
                                                        sb_instraninfo.append(" ,? ");//term_filler3
                                                        sb_instraninfo.append(" ,? ");//term_filler4
                                                        sb_instraninfo.append(" ,? ");//term_filler5
                                                        sb_instraninfo.append(" ,? ");//term_filler6
                                                        sb_instraninfo.append(" ,? ");//term_filler7
                                                        sb_instraninfo.append(" ,? ");//term_filler8
                                                        sb_instraninfo.append(" ,? ");//term_filler9
                                                        sb_instraninfo.append(" ,? ");//term_filler10
                                                        sb_instraninfo.append(" ,? ");//trad_chk_flag
                                                        sb_instraninfo.append(" ,? ");//acc_chk_flag
                                                        sb_instraninfo.append("  , date_format(now(), '%Y%m%d%H%i%s') ");
                                                        sb_instraninfo.append("  , date_format(now(), '%Y%m%d%H%i%s') ");
                                                        sb_instraninfo.append(" ,? ");//ins_user			
                                                        sb_instraninfo.append(" ,? ");//mod_user			
                                                        sb_instraninfo.append(" ,? ");//org_pg_seq			
                                                        sb_instraninfo.append(" ,? ");//onffmerch_no		
                                                        sb_instraninfo.append(" ,? ");//auth_pw		  
                                                        sb_instraninfo.append(" ,? ");//auth_value		  
                                                        sb_instraninfo.append(" ,? ");//app_card_num	
                                                        sb_instraninfo.append(" ,? ");//ONOFFTID_VAT		  
                                                        sb_instraninfo.append(" ,? ");//WITHHOLD_TAX
                                                    sb_instraninfo.append(" ) ");          

                                                    //System.out.println("sb_instraninfo.toString() : " + sb_instraninfo.toString());

                                                    pstmt = conn.prepareStatement(sb_instraninfo.toString());

                                                    //당일취소,익일취소 구분
                                                    String strTpOrgAppDt = rs2.getString("app_dt");
                                                    String strTpCnclAppDt = rTradeDate.substring(0, 8);

                                                    //당일취소
                                                    if(strTpCnclAppDt.equals(strTpOrgAppDt))
                                                    {                            
                                                        pstmt.setString(4,rTradeDate);//app_dt				, jdbcType=VARCHAR}
                                                        pstmt.setString(5,rTradeTime);//app_tm				 , jdbcType=VARCHAR}

                                                        pstmt.setString(39,"10");//tran_status 당일취소 셋팅                            
                                                        pstmt.setString(76,"3");//acc_chk_flag 매입비대상 셋팅
                                                    }
                                                    //익일취소
                                                    else
                                                    {
                                                        pstmt.setString(4,rTradeDate);//app_dt				, jdbcType=VARCHAR}
                                                        pstmt.setString(5,rTradeTime);//app_tm				 , jdbcType=VARCHAR}

                                                        pstmt.setString(39,"20");//tran_status 익일취소(매입취소) 셋팅
                                                        pstmt.setString(76,"0");//acc_chk_flag 매입비대상 셋팅
                                                    }
                                                    pstmt.setString(1,strTpTranseq);//tran_seq				, jdbcType=VARCHAR} 
                                                    pstmt.setString(2,"40");//massagetype			, jdbcType=VARCHAR} 
                                                    pstmt.setString(3,rs2.getString("card_num"));//card_num			, jdbcType=VARCHAR} 

                                                    pstmt.setString(6,rAuthNo);//app_no				, jdbcType=VARCHAR}
                                                    pstmt.setString(7,rs2.getString("tot_amt"));//tot_amt				 , jdbcType=INTEGER }
                                                    pstmt.setString(8,rTransactionNo);//pg_seq				, jdbcType=VARCHAR}
                                                    pstmt.setString(9,rs2.getString("pay_chn_cate"));//pay_chn_cate			 , jdbcType=VARCHAR}
                                                    //pstmt.setString(10,StoreId);//tid					, jdbcType=VARCHAR}
													pstmt.setString(10,strTpTermNo);//tid					, jdbcType=VARCHAR}
                                                    pstmt.setString(11,rs2.getString("onfftid"));//onfftid				, jdbcType=VARCHAR}
                                                    pstmt.setString(12,rs2.getString("wcc"));//wcc					, jdbcType=VARCHAR}
                                                    pstmt.setString(13,rs2.getString("expire_dt"));//expire_dt			, jdbcType=VARCHAR}
                                                    pstmt.setString(14,rs2.getString("card_cate"));//card_cate			  , jdbcType=VARCHAR}
                                                    pstmt.setString(15,OrderNumber);//order_seq			  , jdbcType=VARCHAR}
                                                    pstmt.setString(16,rTradeDate);//tran_dt				, jdbcType=VARCHAR}
                                                    pstmt.setString(17,rTradeTime);//tran_tm				, jdbcType=VARCHAR}
                                                    pstmt.setString(18,"40");//tran_cate			  , jdbcType=VARCHAR}
                                                    pstmt.setString(19,rs2.getString("free_inst_flag"));//free_inst_flag		    , jdbcType=VARCHAR}
                                                    pstmt.setDouble(20,rs2.getDouble("tax_amt"));//tax_amt				, jdbcType=INTEGER }
                                                    pstmt.setDouble(21,rs2.getDouble("svc_amt"));//svc_amt				, jdbcType=INTEGER }
                                                    pstmt.setString(22,rs2.getString("currency"));//currency			 , jdbcType=VARCHAR}
                                                    pstmt.setString(23,rs2.getString("installment"));//installment			   , jdbcType=VARCHAR}
                                                    
                                                    pstmt.setString(24,rs2.getString("iss_cd"));//iss_cd				, jdbcType=VARCHAR}
                                                    pstmt.setString(25,rs2.getString("iss_nm"));//iss_nm				 , jdbcType=VARCHAR}
                                                    pstmt.setString(26,rs2.getString("app_iss_cd"));//app_iss_cd			 , jdbcType=VARCHAR}
                                                    pstmt.setString(27,rs2.getString("app_iss_nm"));//app_iss_nm			 , jdbcType=VARCHAR}
                                                    pstmt.setString(28,rs2.getString("acq_cd"));//acq_cd				, jdbcType=VARCHAR}
                                                    pstmt.setString(29,rs2.getString("acq_nm"));//acq_nm				, jdbcType=VARCHAR}
                                                    pstmt.setString(30,rs2.getString("app_acq_cd"));//app_acq_cd			  , jdbcType=VARCHAR}
                                                    pstmt.setString(31,rs2.getString("app_acq_nm"));//app_acq_nm			   , jdbcType=VARCHAR}
                                                    
                                                    /*
                                                    pstmt.setString(24,null);//iss_cd				, jdbcType=VARCHAR}
                                                    pstmt.setString(25,null);//iss_nm				 , jdbcType=VARCHAR}
                                                    pstmt.setString(26,null);//app_iss_cd			 , jdbcType=VARCHAR}
                                                    pstmt.setString(27,null);//app_iss_nm			 , jdbcType=VARCHAR}
                                                    pstmt.setString(28,null);//acq_cd				, jdbcType=VARCHAR}
                                                    pstmt.setString(29,null);//acq_nm				, jdbcType=VARCHAR}
                                                    pstmt.setString(30,null);//app_acq_cd			  , jdbcType=VARCHAR}
                                                    pstmt.setString(31,null);//app_acq_nm			   , jdbcType=VARCHAR}
                                                    */
                                                    pstmt.setString(32,rs2.getString("merch_no"));//merch_no			  , jdbcType=VARCHAR}
                                                    pstmt.setString(33,rs2.getString("org_merch_no"));//org_merch_no			  , jdbcType=VARCHAR}
                                                    pstmt.setString(34,"0000");//result_cd			 , jdbcType=VARCHAR}
                                                    pstmt.setString(35,"성공(noti수신)");//result_msg			   , jdbcType=VARCHAR}
                                                    pstmt.setString(36,rs2.getString("app_dt"));//org_app_dd			  , jdbcType=VARCHAR}
                                                    pstmt.setString(37,rs2.getString("app_no"));//org_app_no			   , jdbcType=VARCHAR}
                                                    pstmt.setString(38,"noti취소통지");//cncl_reason			, jdbcType=VARCHAR}
                                                    pstmt.setString(40,"00");//result_status		     , jdbcType=VARCHAR}
                                                    pstmt.setString(41,"00");//tran_step			 , jdbcType=VARCHAR}
                                                    pstmt.setString(42,rTradeDate);//cncl_dt				, jdbcType=VARCHAR}
                                                    pstmt.setString(43,null);//acq_dt				, jdbcType=VARCHAR}
                                                    pstmt.setString(44,null);//acq_result_cd		     , jdbcType=VARCHAR}
                                                    pstmt.setString(45,null);//holdoff_dt			  , jdbcType=VARCHAR}
                                                    pstmt.setString(46,null);//holdon_dt			  , jdbcType=VARCHAR}
                                                    
                                                    /*
                                                    pstmt.setString(47,null);//pay_dt				, jdbcType=VARCHAR}
                                                    pstmt.setString(48,null);//pay_amt				, jdbcType=INTEGER }
                                                    pstmt.setString(49,null);//commision			  , jdbcType=INTEGER }
                                                    */
													/*
                                                    pstmt.setString(47, strTpPaydt);//pay_dt				, jdbcType=VARCHAR}
                                                    pstmt.setDouble(48, 0-dbl_van_payamt);//pay_amt				, jdbcType=INTEGER }
                                                    pstmt.setDouble(49, 0-(dbl_van_cms+dbl_van_cmsvat));//commision			  , jdbcType=INTEGER }
                                                    */
													pstmt.setString(47, null);//pay_dt				, jdbcType=VARCHAR}
                                                    pstmt.setString(48, null);//pay_amt				, jdbcType=INTEGER }
                                                    pstmt.setString(49, null);//commision			  , jdbcType=INTEGER }
                                                    
                                                    pstmt.setString(50,rs2.getString("onofftid_pay_amt"));//onofftid_pay_amt		, jdbcType=INTEGER }
                                                    pstmt.setString(51,rs2.getString("onofftid_commision"));//onofftid_commision		, jdbcType=INTEGER }
                                                    pstmt.setString(52,null);//client_ip				, jdbcType=VARCHAR}
                                                    pstmt.setString(53,rs2.getString("user_type"));//user_type			, jdbcType=VARCHAR}
                                                    pstmt.setString(54,rs2.getString("memb_user_no"));//memb_user_no		, jdbcType=VARCHAR}
                                                    pstmt.setString(55,rs2.getString("user_id"));//user_id				, jdbcType=VARCHAR}
                                                    pstmt.setString(56,rs2.getString("user_nm"));//user_nm				, jdbcType=VARCHAR}
                                                    pstmt.setString(57,rs2.getString("user_mail"));//user_mail			    , jdbcType=VARCHAR}
                                                    pstmt.setString(58,rs2.getString("user_phone1"));//user_phone1			 , jdbcType=VARCHAR}
                                                    pstmt.setString(59,rs2.getString("user_phone2"));//user_phone2			 , jdbcType=VARCHAR}
                                                    pstmt.setString(60,rs2.getString("user_addr"));//user_addr			, jdbcType=VARCHAR}
                                                    pstmt.setString(61,rs2.getString("product_type"));//product_type		  , jdbcType=VARCHAR}
                                                    pstmt.setString(62,rs2.getString("product_nm"));//product_nm			 , jdbcType=VARCHAR}
                                                    pstmt.setString(63,null);//filler				   , jdbcType=VARCHAR}
                                                    pstmt.setString(64,null);//filler2				   , jdbcType=VARCHAR}
                                                    pstmt.setString(65,null);//term_filler1			, jdbcType=VARCHAR}
                                                    pstmt.setString(66,null);//term_filler2			, jdbcType=VARCHAR}
                                                    pstmt.setString(67,null);//term_filler3			, jdbcType=VARCHAR}
                                                    pstmt.setString(68,null);//term_filler4			, jdbcType=VARCHAR}
                                                    pstmt.setString(69,null);//term_filler5			, jdbcType=VARCHAR}
                                                    pstmt.setString(70,null);//term_filler6				, jdbcType=VARCHAR}
                                                    pstmt.setString(71,null);//term_filler7			  , jdbcType=VARCHAR}
                                                    pstmt.setString(72,null);//term_filler8			, jdbcType=VARCHAR}
                                                    pstmt.setString(73,null);//term_filler9			, jdbcType=VARCHAR}
                                                    pstmt.setString(74,null);//term_filler10			, jdbcType=VARCHAR}
                                                    pstmt.setString(75,"0");//trad_chk_flag		  , jdbcType=VARCHAR}
                                                    pstmt.setString(77,"0");//ins_user				     , jdbcType=VARCHAR}
                                                    pstmt.setString(78,"0");//mod_user			, jdbcType=VARCHAR}
                                                    pstmt.setString(79,rs2.getString("pg_seq"));//org_pg_seq			, jdbcType=VARCHAR}
                                                    pstmt.setString(80,rs2.getString("onffmerch_no"));//onffmerch_no		  , jdbcType=VARCHAR}
                                                    pstmt.setString(81,null);//auth_pw		  , jdbcType=VARCHAR}
                                                    pstmt.setString(82,null);//auth_value		  , jdbcType=VARCHAR}   
                                                    pstmt.setString(83,rs2.getString("app_card_num"));//app_card_num		  , jdbcType=VARCHAR} 
                                                    pstmt.setDouble(84,rs2.getDouble("ONOFFTID_VAT"));//ONOFFTID_VAT		  , jdbcType=VARCHAR}     
                                                    pstmt.setDouble(85,rs2.getDouble("WITHHOLD_TAX"));//WITHHOLD_TAX		  , jdbcType=VARCHAR}      
                                                    int int_insResult = pstmt.executeUpdate();                               

                                                    closeResultSet(rs);
                                                    closePstmt(pstmt);
                                                    logger.debug("--org traninfo update  --");
                                                    //원거래를 update한다.
                                                    StringBuffer sb_orgtran_mod_sql = new StringBuffer();
                                                    sb_orgtran_mod_sql.append(" update " + rs2.getString("tb_nm") + " ");
                                                    sb_orgtran_mod_sql.append(" set      cncl_reason = ? ");
                                                    sb_orgtran_mod_sql.append("              ,tran_status= ? ");
                                                    sb_orgtran_mod_sql.append("              ,cncl_dt = ? ");
                                                    sb_orgtran_mod_sql.append("              ,acc_chk_flag = ? ");
                                                    sb_orgtran_mod_sql.append("               ,mod_dt =  date_format(now(), '%Y%m%d%H%i%s') ");
                                                    sb_orgtran_mod_sql.append("               ,mod_user= '0' ");
                                                    sb_orgtran_mod_sql.append("          where 1=1 ");
                                                    sb_orgtran_mod_sql.append("            and tran_seq = ? ");

                                                    pstmt = conn.prepareStatement(sb_orgtran_mod_sql.toString());

                                                    pstmt.setString(1, "noti취소통지");

                                                    //당일취소
                                                    if(strTpCnclAppDt.equals(strTpOrgAppDt))
                                                    {
                                                       pstmt.setString(2, "10");//당일취소
                                                       pstmt.setString(4, "3");//매입비대상   
                                                    }
                                                    else
                                                    {
                                                        pstmt.setString(2, "20");//매입취소
                                                        pstmt.setString(4, rs2.getString("acc_chk_flag"));
                                                    }                        
                                                    pstmt.setString(3, rTradeDate);                        
                                                    pstmt.setString(5, rs2.getString("tran_seq"));

                                                    int int_mod_orginfo = pstmt.executeUpdate();  

                                                    closeResultSet(rs);
                                                    closePstmt(pstmt);

                                                    strNotResult = "10";

                                                } 
                                                //원거래정보가 없는 경우
                                                else
                                                {
                                                    logger.debug("--org traninfo not exist  --");
                                                    //원거래가 없어 insert안함.
                                                    strNotResult = "30";
                                                }
                                            }
                                            //이미 취소거래가 있는경우
                                            else
                                            {
                                                logger.debug("-- cncl traninfo exist  --");
                                                strNotResult = "20";
                                            }
                                            //noti처리상태 update
                                            StringBuffer sb_noti_mod_sql = new StringBuffer();
                                            sb_noti_mod_sql.append(" UPDATE tb_ksnet_noti SET ");
                                            sb_noti_mod_sql.append(" proc_flag=? ");
                                            sb_noti_mod_sql.append(" ,mod_dt =  date_format(now(), '%Y%m%d%H%i%s') ");
                                            sb_noti_mod_sql.append(" ,mod_user= '0' ");                
                                            sb_noti_mod_sql.append(" WHERE noti_seq = ? ");
                                            pstmt = conn.prepareStatement(sb_noti_mod_sql.toString());
                                            pstmt.setString(1, strNotResult);
                                            pstmt.setString(2, strTpNotiseq);
                                            int int_mod_notiinfo = pstmt.executeUpdate();                                         
                                    }//승인취소구분 end
                                    else
                                    {
                                        System.out.println("etcxxxx");
                                    }
                            }//정상건end
                     
                            conn.commit();
                       } 
                       catch ( SQLException e ) 
                       {
                          e.printStackTrace();
                          if(conn !=null)
                          {
                               conn.rollback();
                          }
                          logger.error(e);						  
                        } 
                        catch ( Exception e ) 
                        {
                          e.printStackTrace();
                          if(conn !=null)
                          {
                               conn.rollback();
                          }
                          logger.error(e);
                        } 
                        finally 
                        {
                            closeResultSet(rs);
                            closeResultSet(rs2);
                            closePstmt(pstmt);
                            closePstmt(pstmt2);
                            closeConnection(conn);
                        }
                        
                }
        }
	
	response.reset();
	
	//System.out.println("msg.length() : " + msg.length());

	String msg = "result=" + ret;

	System.out.println("\n\n\n result : ["+msg+"]");
	out.print(msg);
	
	//명시적으로 읽어들일 길이를 header에 설정한다.
	response.setContentLength(msg.length());
	out.close();
	System.out.println("\n\n\n result : ["+msg+"]");
%>