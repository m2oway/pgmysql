<%-- 
    Document   : adjdelete_process
    Created on : 2017. 5. 30, 오후 2:17:54
    Author     : MoonBong
--%>
<%@page import="com.onoffkorea.system.common.util.Util"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.sql.*" %>
<%@page import="javax.naming.*"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="com.onoffkorea.system.app.Vo.KiccPgVo"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%!
    /**
     * 파라미터 체크 메소드
     */
    public String getNullToSpace(String param) {
        return (param == null) ? "" : param.trim();
    }

    public String getNullToZero(String param) {
        return (param == null) ? "0" : param.trim();
    }
        
    
    public void closeResultSet(ResultSet rs)
    {
        try
        {
            if ( rs != null ) rs.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    
    public void closePstmt(PreparedStatement psmt)
    {
        try
        {
            if ( psmt != null ) psmt.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }    
    
    public void closeConnection(Connection con)
    {
        try
        {
            if ( con != null ) con.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>정산내역삭제</title>
    </head>
    <body>
<%
    final Logger logger = Logger.getLogger("transdelete_process.jsp");
    
    Connection conn = null;
    PreparedStatement pstmt = null;
    PreparedStatement pstmt2 = null;
    ResultSet rs = null;
    ResultSet rs2 = null;
   
    String filename=null;
   
    try
    {
       //DB Connection을 가져온다.
        Context initContext = new InitialContext();                                            //2
        DataSource ds = (DataSource) initContext.lookup("java:comp/env/jdbc/okpay");     //3
        conn = ds.getConnection();                                                             //4
        conn.setAutoCommit(false);
        
        String strreqTranDate = request.getParameter("GEN_DT");
        
        StringBuffer sb_orgtran_mod_sql = new StringBuffer();
        sb_orgtran_mod_sql.append(" delete from TB_MERCH_PAY_SCHE_DETAIL ");
        sb_orgtran_mod_sql.append(" where GEN_DT = ? ");
                                        
        pstmt = conn.prepareStatement(sb_orgtran_mod_sql.toString());

        pstmt.setString(1, strreqTranDate);
        int int_mod_orginfo = pstmt.executeUpdate();  
        
        closeResultSet(rs);
        closePstmt(pstmt);
        
        
        StringBuffer sb_orgtran_mod_sql2 = new StringBuffer();
        sb_orgtran_mod_sql2.append(" delete from TB_MERCH_PAY_SCHE ");
        sb_orgtran_mod_sql2.append(" where GEN_DT = ? ");
                                        
        pstmt = conn.prepareStatement(sb_orgtran_mod_sql2.toString());

        pstmt.setString(1, strreqTranDate);
        int int_mod_orginfo2 = pstmt.executeUpdate();  
        
        closeResultSet(rs);
        closePstmt(pstmt);
        
        conn.commit();
        out.println("<b>정산내역삭제완료!!!</b><br/>");
    }
    catch(Exception ex)
    {
        ex.printStackTrace();
        conn.rollback();
        
        out.println("<b>정산내역삭제오류!!!</b><br/>");
    }
    finally
    {
       
        closeResultSet(rs);
        closeResultSet(rs2);
        closePstmt(pstmt);
        closePstmt(pstmt2);
        closeConnection(conn);
    }

%>        
    </body>
</html>

