<%-- 
    Document   : uid_input_sample.jsp
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>가맹점등록 정보 등록</title>
        </head>
        <body>
</c:if>
<script type="text/javascript">
                var regobj = new RegExp('-',"gi");
                var dhxSelPopupWins;
		dhxSelPopupWins = new dhtmlXWindows();
                
                               
                $(document).ready(function () {
                    terminal_layout = new dhtmlXLayoutObject("onfftid_cmsVP2", "1C", "dhx_skyblue");
                    terminal_layout.cells('a').hideHeader();

                    terminal_mygrid = terminal_layout.cells('a').attachGrid();
                    terminal_mygrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var mheaders = "";
                    mheaders += "<center>ID</center>,<center>관리자구분</center>,<center>그룹ID</center>,<center>선택버튼</center>";
                    terminal_mygrid.setHeader(mheaders);
                    terminal_mygrid.setColAlign("center,center,center,center");
                    terminal_mygrid.setColTypes("txt,txt,txt,txt");
                    terminal_mygrid.setInitWidths("150,150,150,150");
                    terminal_mygrid.setColSorting("str,str,str,str");
                    terminal_mygrid.enableColumnMove(false);
                    terminal_mygrid.setSkin("dhx_skyblue");
                    terminal_mygrid.init();
        
                    myCalendar = new dhtmlXCalendarObject(["view_onfftid_cms_start_dt","view_onfftid_cms_end_dt"]);
                                        
                    //terminal 정보 등록 이벤트
                    $("#terminalFormInsert input[name=insert]").click(function(){
                       doInsertTerminalMaster(); 
                    });              
                    
                    //terminal 정보 등록 닫기
                    $("#terminalFormInsert input[name=close]").click(function(){
                         terminalMasterWindow.window("w1").close();
                    });                            
                    
                });
</script>
        <div style='width:100%;height:100%;overflow:auto;'>            
                <table class="gridtable" height="100%" width="100%">   
                    <tr>
                        <td align='left' colspan='4'>서비스관리 > 가맹점 관리 > 가맹점 등록</td>                        
                    </tr>
                    <tr>
                        <td align='left' colspan='4'>- 사업자조회</td>                        
                    </tr>
                    <tr>                        
                        <td colspan='4'>
                            <table class="gridtable" height="100%" width="100%">  
                                 <tr>
                                <td class="headcol">사업자번호</td>
                                <td><input name="biz_no"  readonly /></td>
                                <td class="headcol">UID</tD>
                                <td><input name="corp_no"  readonly/></td>
                                <td class="headcol">지급ID</td>
                                <td><input name="corp_no"  readonly /></td>
                                <td><button>검색</button></td>
                                 </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align='left' colspan='4'>- 신규등록</td>                        
                    </tr>
                    <tr>                        
                        <td colspan='4'>
                            <table class="gridtable" height="100%" width="100%">   
                                <tr>
                                    <td  align='left' colspan='6'>사업자 정보</td>
                                </tr>
                                <tr>
                                    <td class="headcol">사업자번호</td>
                                    <td><input name="biz_no"  readonly />&nbsp;<button>검색</button></td>
                                    <td class="headcol">상호</tD>
                                    <td><input name="corp_no"  readonly /></td>
                                    <td class="headcol">대표자명</td>
                                    <td><input name="corp_no"  readonly /></td>
                                </tr>
                                <tr>
                                    <td class="headcol">담보금액</td>
                                    <td><input name="biz_no"  readonly /></td>
                                    <td class="headcol">승인한도</tD>
                                    <td><input name="corp_no"  readonly /></td>
                                    <td class="headcol">건별한도</td>
                                    <td><input name="corp_no"  readonly /></td>
                                </tr>                                
                            </table>
                        </td>
                    </tr>            
                    <tr>                        
                        <td colspan='4'>
                            <table class="gridtable" height="100%" width="100%">   
                                <tr>
                                    <td  align='left' colspan='6'>UID 정보</td>
                                </tr>
                                <tr>
                                    <td class="headcol">mall아이디</td>
                                    <td><input name="biz_no"  readonly />&nbsp;<button>생성</button></td>
                                    <td class="headcol">가맹점명</tD>
                                    <td><input name="corp_no"  readonly /></td>
                                    <td class="headcol">가맹점URL</td>
                                    <td><input name="corp_no"  readonly /></td>
                                </tr>
                                <tr>
                                    <td class="headcol">채널구분</td>
                                    <td>
                                        <select name="bal_period"  >
                                            <option value="">--선택--</option>                                
                                        </select>
                                    </td>
                                    <td class="headcol">취소권한</tD>
                                    <td>                                        
                                        <select name="bal_period"  >
                                            <option value="">--선택--</option>                                
                                        </select></td>
                                    <td class="headcol">상태</td>
                                    <td>
                                        <select name="bal_period"  >
                                            <option value="">--선택--</option>                                
                                        </select>
                                    </td>
                                </tr>    
                                <tr>
                                    <td class="headcol">주소</td>
                                    <td colspan="5">
                                        <input name="biz_no" size="6"  />&nbsp;-&nbsp;<input name="biz_no" size="6" />&nbsp;&nbsp;<input name="biz_no" size="40" />&nbsp;<input name="biz_no" size="40" />
                                    </td>                    
                                </tr>   
                                <tr>
                                    <td class="headcol">결제수단</td>
                                    <td colspan="3">
                                        <select name="bal_period"  >
                                            <option value="">--선택--</option>                                
                                        </select>
                                    </td>
                                    <td class="headcol">CID 선택</td>
                                    <td><input name="biz_no"  readonly />&nbsp;<button>검색</button></td>           
                                </tr>
                                <tr>
                                    <td class="headcol">계좌이체수수료</td>
                                    <td>
                                        <input name="biz_no"  readonly />
                                    </td>
                                    <td class="headcol">가상계좌수수료</tD>
                                    <td>                                        
                                        <input name="biz_no"  readonly />
                                    </td>
                                    <td class="headcol">휴대폰수수료</td>
                                    <td>
                                        <input name="biz_no"  readonly />
                                    </td>
                                </tr>    
                                <tr>
                                    <td class="headcol">신용카드 통합수수료</td>
                                    <td>
                                        <input name="biz_no"  readonly />
                                    </td>
                                    <td class="headcol">영업대행사</tD>
                                    <td>                                        
                                        <input name="biz_no"  readonly />&nbsp;<button>검색</button>
                                    </td>
                                    <td class="headcol">PG선택</td>
                                    <td>
                                        <select name="bal_period"  >
                                            <option value="">--대행--</option>                                
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="headcol">신용카드 개별수수료</td>
                                    <td colspan="3">&nbsp;BC &nbsp;<input name="biz_no" size="3" />&nbsp;신한 &nbsp;<input name="biz_no" size="3" />&nbsp;국민 &nbsp;<input name="biz_no" size="3" />&nbsp;삼성 &nbsp;<input name="biz_no" size="3" />&nbsp;현대 &nbsp;<input name="biz_no" size="3" />&nbsp;하나 &nbsp;<input name="biz_no" size="3" />&nbsp;롯데 &nbsp;<input name="biz_no" size="3" />&nbsp;농협 &nbsp;<input name="biz_no" size="3" /></td>
                                    <td class="headcol">부가세여부</td>
                                    <td><select name="bal_period"  >
                                            <option value="">--과세--</option>                                
                                        </select></td>           
                                </tr>
                                <tr>
                                    <td class="headcol">최대할부개월</td>
                                    <td colspan="5">
                                       <select name="bal_period"  >
                                            <option value="">--미사용--</option>                                
                                       </select>&nbsp;&nbsp;
                                        <select name="bal_period"  >
                                            <option value="">--선택--</option>                                
                                        </select>
                                    </td>                    
                                </tr>
                                <tr>
                                    <td class="headcol">정산방식</td>
                                    <td>
                                          <select name="bal_period"  >
                                            <option value="">--선택--</option>                                
                                        </select>
                                    </td>
                                    <td class="headcol">정산주기</tD>
                                    <td>                                        
                                          <select name="bal_period"  >
                                            <option value="">--선택--</option>                                
                                        </select>
                                    </td>
                                    <td class="headcol">인증구분</td>
                                    <td>
                                        <select name="bal_period"  >
                                            <option value="">--비인증--</option>                                
                                        </select>
                                    </td>
                                </tr> 
                                <tr>
                                    <td class="headcol">시작일</td>
                                    <td>
                                          <select name="bal_period"  >
                                            <option value="">--선택--</option>                                
                                        </select>
                                    </td>
                                    <td class="headcol">종료일</tD>
                                    <td>                                        
                                          <select name="bal_period"  >
                                            <option value="">--선택--</option>                                
                                        </select>
                                    </td>
                                    <td class="headcol">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>       
                            </table>
                        </td>
                    </tr>    
                    <tr>                        
                        <td colspan='4'>
                            <table class="gridtable" height="100%" width="100%">   
                                <tr>
                                    <td  align='left' colspan='8'>지급ID 정보</td>
                                </tr>
                                <tr>
                                    <td class="headcol">지급아이디</td>
                                    <td><input name="biz_no"  readonly />&nbsp;<button>생성</button></td>
                                    <td class="headcol">입금은행</tD>
                                    <td> <select name="bal_period"  >
                                            <option value="">--비인증--</option>                                
                                        </select></td>
                                    <td class="headcol">입금계좌</td>
                                    <td><input name="corp_no"  readonly /></td>
                                    <td class="headcol">입금주</td>
                                    <td><input name="corp_no"  readonly /></td>
                                </tr> 
                            </table>
                        </td>
                    </tr>
                    <tr>                        
                        <td colspan='4'>
                            <table class="gridtable" height="100%" width="100%">   
                                <tr>
                                    <td  align='left' colspan='8'>로그인ID 정보</td>
                                </tr>
                                <tr>
                                    <td class="headcol">아이디</td>
                                    <td><input name="biz_no"  readonly />&nbsp;<button>중복첵크</button></td>
                                    <td class="headcol">비밀번호</td>
                                    <td><input name="corp_no"  readonly /></td>
                                    <td class="headcol">관리자구분</tD>
                                    <td> <select name="bal_period"  >
                                            <option value="">--선택--</option>                                
                                        </select>
                                    </td>
                                </tr> 
                                <tr>
                                    <td class="headcol">접속시 암호변경</td>
                                    <td><input name="biz_no"  readonly /></td>
                                    <td class="headcol">그룹정보 표기 유형</td>
                                    <td><input name="corp_no"  readonly /></td>
                                    <td class="headcol">그룹 아이디</tD>
                                    <td><input name="corp_no"  readonly /></td>                                    
                                </tr>                                 
                                <tr>
                                     <td colspan="6" height="110px">
                                        <div id="onfftid_cmsVP2" style="position: relative; width:100%;height:100px;background-color:white;"></div>
                                    </td>    
                                </tr>
                            </table>
                        </td>
                    </tr>                             
                    <tr>
                        <td colspan="4" align="center"><input type="button" name="insert" value="등록"/></td>
                    </tr>       
                </table>
                 <input type="hidden" name="commissioninfo" />
            </form>
        </div>
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>