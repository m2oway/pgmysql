<%-- 
    Document   : uid_input_sample.jsp
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>UID 정보 등록</title>
        </head>
        <body>
</c:if>
<%
    String strCurDate = com.onoffkorea.system.common.util.Util.getTodayDate();
    String strTpDefaultEndDt = "20991231";
%>
            <script type="text/javascript">
                var regobj = new RegExp('-',"gi");
                var dhxSelPopupWins;
		dhxSelPopupWins = new dhtmlXWindows();
                
                $(document).ready(function () {
                    

                    onffmerchant_layout = new dhtmlXLayoutObject("onffmerchantVP2", "1C", "dhx_skyblue");
                    onffmerchant_layout.cells('a').hideHeader();

                    onffmerchant_mygrid = onffmerchant_layout.cells('a').attachGrid();
                    onffmerchant_mygrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var mheaders = "";
                    mheaders += "<center>담보종류코드</center>,<center>담보종류</center>,<center>담보액</center>,<center>적용시작일</center>,<center>적용종료일</center>,<center>MEMO</center>";
                    onffmerchant_mygrid.setHeader(mheaders);
                    onffmerchant_mygrid.setColAlign("center,center,center,center,center,left");
                    onffmerchant_mygrid.setColTypes("txt,txt,txt,txt,txt,txt");
                    onffmerchant_mygrid.setInitWidths("10,100,100,80,80,200");
                    onffmerchant_mygrid.setColSorting("str,str,str,str,str,str");
                    onffmerchant_mygrid.enableColumnMove(false);
                    onffmerchant_mygrid.setSkin("dhx_skyblue");
                    onffmerchant_mygrid.init();
                    //onffmerchant_mygrid.parse(${termMasterList}, "json");
                    //onffmerchant_mygrid.attachEvent("onRowDblClicked", doInsertMerchantTidInsert);
 
                   onffmerchant_mygrid.setColumnHidden(0,true);   
                   //onffmerchant_mygrid.setColumnHidden(3,true);                    
                    
                    terminal_layout = new dhtmlXLayoutObject("onfftid_cmsVP2", "1C", "dhx_skyblue");
                    terminal_layout.cells('a').hideHeader();

                    terminal_mygrid = terminal_layout.cells('a').attachGrid();
                    terminal_mygrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var mheaders = "";
                    mheaders += "<center>수수료</center>,<center>적용시작일</center>,<center>적용종료일</center>,<center>MEMO</center>";
                    terminal_mygrid.setHeader(mheaders);
                    terminal_mygrid.setColAlign("center,center,center,center");
                    terminal_mygrid.setColTypes("txt,txt,txt,txt");
                    terminal_mygrid.setInitWidths("100,100,100,200");
                    terminal_mygrid.setColSorting("str,str,str,str");
                    terminal_mygrid.enableColumnMove(false);
                    terminal_mygrid.setSkin("dhx_skyblue");
                    terminal_mygrid.init();
        
                    myCalendar = new dhtmlXCalendarObject(["view_onfftid_cms_start_dt","view_onfftid_cms_end_dt"]);
                    
                    
                    
                    termuser_layout = new dhtmlXLayoutObject("onff_userVP2", "1C", "dhx_skyblue");
                    termuser_layout.cells('a').hideHeader();

                    termuser_mygrid = termuser_layout.cells('a').attachGrid();
                    termuser_mygrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var mheaders = "";
                    mheaders += "<center>ID</center>,<center>비밀번호</center>,<center>이름</center>,<center>권한</center>";
                    termuser_mygrid.setHeader(mheaders);
                    termuser_mygrid.setColAlign("center,center,center,center");
                    termuser_mygrid.setColTypes("txt,txt,txt,txt");
                    termuser_mygrid.setInitWidths("100,100,100,200");
                    termuser_mygrid.setColSorting("str,str,str,str");
                    termuser_mygrid.enableColumnMove(false);
                    termuser_mygrid.setSkin("dhx_skyblue");
                    termuser_mygrid.init();
       
                });
            </script>
        <div style='width:100%;height:100%;overflow:auto;'>            
            <form id="terminalFormInsert" method="POST" >
                <table class="gridtable" height="100%" width="100%">   
                    <tr>
                        <td align='left' colspan='4'>서비스관리 > 가맹점 관리 > 가맹점 등록</td>                        
                    </tr>
                    <tr>
                        <td align='left' colspan='4'>- 사업자 정보</td>                        
                    </tr>
                    <tr>
                        <td align='left' colspan='4'>
                            <table class="gridtable" height="100%" width="100%">  
                                <tr>                        
                                    <td class="headcol">사업자명*</td>
                                    <td><input name="comp_nm" id="comp_nm"  readonly  /><input type="hidden" name="comp_seq" id="comp_seq"/>&nbsp;<button>검색</button></td>
                                    <td class="headcol">개인/법인구분*</tD>
                                    <td><input name="comp_cate_nm" id="comp_cate_nm"  readonly /></td>
                                    <td class="headcol">사업자번호*</td>
                                    <td><input name="biz_no"  readonly /></td>                                    
                                </tr>
                                 <tr>
                                    <td class="headcol">법인번호*</tD>
                                    <td><input name="corp_no"  readonly /></td>
                                    <td class="headcol">업태*</td>
                                    <td><input name="comp_biz_type"  readonly /></td>
                                    <td class="headcol">업종*</tD>
                                    <td><input name="comp_biz_cate"  readonly /></td>                                    
                                </tr>             
                                 <tr>
                                    <td class="headcol">과세구분*</tD>
                                    <td ><input name="comp_tax_flag"  /></td>                        
                                    <td class="headcol">대표자명*</tD>
                                    <td ><input name="comp_ceo_nm"  /></td>
                                    <td class="headcol">대표번호*</td>
                                    <td><input name="comp_tel1"  /></td>                                    
                                </tr>     
                                <tr>
                                    <td class="headcol">FAX</tD>
                                    <td><input name="comp_tel2"  /></td>
                                    <td class="headcol">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td class="headcol">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>                                
                            </table>
                        </td>                        
                    </tr>
                    <tr>
                        <td align='left' colspan='4'>- UID 정보</td>                        
                    </tr>
                    <tr>
                        <td align='left' colspan='4'>
                            <table class="gridtable" height="100%" width="100%">  
                                <tr>
                                    <td align='left' colspan='4'>UID 기본정보</td>                        
                                </tr>                                
                                <tr>                        
                                    <td class="headcol" width="20%">UID(자동생성)</td>
                                    <td ><input name="onfftid" size="25" readonly /></td>
                                    <td class="headcol" width="20%">UID명*</td>
                                    <td ><input name="onfftid_nm" size="40" maxlength="50" onblur="javascript:checkLength(this,50);" /></td>                        
                                </tr>
                                <tr>
                                   <td class="headcol">결제채널*</td>
                                   <td><select name="pay_chn_cate" >
                                           <option value="">온라인카드결제</option> 
                                           <option value="">ARS</option> 
                                           <option value="">단말기결제</option> 
                                       </select>                         
                                   </td>
                                   <td class="headcol">가맹점상태*</td>
                                   <td ><select name="svc_stat" >
                                           <option value="">사용</option> 
                                           <option value="">일시중지</option> 
                                           <option value="">사용중지</option> 
                                           <option value="">해지</option>                                            
                                       </select>                         
                                   </td>                        
                               </tr>                                     
                                <tr>
                                    <td class="headcol">결제인증구분*</td>
                                    <td ><select name="cert_type" >
                                           <option value="">비인증</option> 
                                           <option value="">인증</option> 
                                        </select> 
                                    </td>                        
                                    <td class="headcol">결제취소권한*</td>
                                    <td ><select name="cncl_auth" >
                                           <option value="">취소가능</option> 
                                           <option value="">취소불가</option> 
                                    </td>                        
                                </tr>                     
                                <tr>
                                    <td class="headcol">Memo</td>
                                    <td colspan="3"><textarea name="memo" cols="60" rows="3"></textarea></td>                        
                                </tr>                          
                                <tr>                        
                                    <td class="headcol">CID*선택</td>
                                    <td colspan='3'><input name="pay_mtd_nm" id="pay_mtd_nm" size='30' readonly onclick='javascript:PaymtdSelectWin();'/><input type='hidden' name="pay_mtd_seq" id='pay_mtd_seq'>&nbsp;<button>검색</button></td>
                                </tr>
                                <tr>
                                    <td align='left' colspan='4'>UID 수수료 정보</td>                        
                                </tr>
                                <tr>                        
                                    <td class="headcol" >수수료율*</tD>
                                    <td colspan="3"><input name="onfftid_cms_commission"  onkeyPress="javascript:InpuOnlyNumber(this);"  /></td>    
                                </tr>     
                                <tr>
                                    <td class="headcol">시작일*</td>
                                    <td><input name="view_onfftid_cms_start_dt" id="view_onfftid_cms_start_dt" maxlength="10" value="<%=strCurDate%>" readonly/><input type="hidden" name="onfftid_cms_start_dt"/></td>
                                    <td class="headcol">종료일*</tD>
                                    <td><input name="view_onfftid_cms_end_dt" id="view_onfftid_cms_end_dt" maxlength="10" value="<%=strTpDefaultEndDt%>" readonly/><input type="hidden" name="onfftid_cms_end_dt"/></td>
                                </tr> 
                                <tr>
                                    <td class="headcol">Memo</td>
                                    <td colspan="3"><textarea name="onfftid_cms_memo" cols="60" rows="2"></textarea></td>                        
                                </tr>                     
                                <tr>
                                    <td align='left' colspan='4' valign="top">
                                        <div id = "common_btn" align = "right" style="height:30px;" >
                                        <input type="button" name="onfftid_cms_addbt" value="수수료 추가" onclick="javascript:addRow();"/>
                                        <input type="button" name="onfftid_cms_delbt" value="수수료 삭제" onclick="javascript:delRow();"/>
                                        </div>
                                        <div id="onfftid_cmsVP2" style="position: relative; width:100%;height:100px;background-color:white;"></div>
                                    </td>                        
                                </tr>                                
                            </table>
                        </td>                        
                    </tr>                    
                    <tr>
                        <td align='left' colspan='4'>- 지급ID 정보</td>                        
                    </tr>
                    <tr>
                        <td align='left' colspan='4'>
                            <table class="gridtable" height="100%" width="100%">  
                                <tr>
                                    <td align='left' colspan='4'>지급ID 기본정보</td>                        
                                </tr>    
                                <tr>                        
                                    <td class="headcol" width="20%">지급ID(자동생성)</td>
                                    <td ><input name="onffmerch_no" size="25" readonly /></td>
                                    <td class="headcol" width="20%">지급ID명*</td>
                                    <td ><input name="merch_nm" size="40" maxlength="50" onblur="javascript:checkLength(this,50);" /></td>                        
                                </tr>
                                <tr>                        
                                    <td class="headcol" width="20%">연락처1*</td>
                                    <td ><input name="tel_1" size="20" maxlength="20"/></td>
                                    <td class="headcol" width="20%">연락처2</td>
                                    <td ><input name="tel_2" size="20" maxlength="20" /></td>                        
                                </tr>
                                <tr>                        
                                    <td class="headcol" width="20%">E-MAIL</td>
                                    <td ><input name="email" size="25" maxlength="100" /></td>
                                    <td class="headcol">우편번호</td>
                                    <td><input name="zip_cd" maxlength="6" /></td>
                                </tr>             
                                <tr>
                                    <td class="headcol">주소</tD>
                                    <td colspan="3"><input name="addr_1" size="40" />&nbsp;<input name="addr_2" size="40" /></td>
                                </tr> 
                                 <tr>
                                    <td class="headcol">결제한도*</td>
                                    <td ><input name="app_chk_amt"  onkeyPress="javascript:InpuOnlyNumber2(this);"  maxlength="15" /><span name="insert_amt_han"></span><span>원</span></td>     
                                    <td class="headcol">건별결제한도*</td>
                                    <td ><input name="appreq_chk_amt"  onkeyPress="javascript:InpuOnlyNumber2(this);"  maxlength="15" value="0" />(미사용시 0원 입력)</td>                            
                                </tr> 
                                <tr>
                                    <td class="headcol">취소권한*</td>
                                    <td ><select name="cncl_auth" >
                                           <option value="">취소가능</option> 
                                           <option value="">취소불가</option> 
                                        </select></td>  
                                    <td class="headcol">지급ID상태*</td>
                                    <td ><select name="svc_stat" >
                                           <option value="">사용</option> 
                                           <option value="">일시중지</option> 
                                           <option value="">사용중지</option> 
                                           <option value="">해지</option>  
                                        </select>                         
                                    </td>
                                </tr>                           
                                <tr>
                                    <td class="headcol">해지일</td>
                                    <td><input name="view_close_dt" id="view_close_dt" maxlength="10" /><input type="hidden" name="close_dt"/></td>
                                    <td class="headcol">해지사유</tD>
                                    <td><input name="close_reson" size="50" /></td>
                                </tr>                    
                                <tr>
                                    <td class="headcol">Memo</td>
                                    <td colspan="3"><textarea name="memo" cols="60" rows="5"></textarea></td>                        
                                </tr>                          
                                <tr>
                                    <td align='left' colspan='4'>정산정보</td>                        
                                </tr>
                                 <tr>
                                    <td class="headcol">정산주기*</td>
                                    <td colspan="3"><select name="bal_period" id="bal_period" onchange="javascript:selChangeBalPeriod();" >
                                            <option value="">일정산</option>
                                            <option value="">주1회정산</option>
                                            <option value="">월1회정산</option>
                                            <option value="">월2회정산</option>
                                        </select>                         
                                    </td>
                                </tr>
                                 <tr>                        
                                    <td class="headcol">정산기준일*</td>
                                    <td colspan="3"><select name="pay_dt_cd_1" id="pay_dt_cd_1" >
                                            <option value="">--선택하세요.--</option>
                                        </select>&nbsp;<select name="pay_dt_cd_2" id="pay_dt_cd_2" >
                                            <option value="">--선택하세요.--</option>
                                        </select>   
                                    </td>                        
                                </tr>
                                <tr>
                                    <td class="headcol">지급일*</td>
                                    <td >정산대상 최종승인일+<input name="pay_dt" size="3"  />(영업일)</td>                         
                                    <td class="headcol">입금은행*</td>
                                    <td><select name="bank_cd" >
                                            <option value="">--선택하세요.--</option>
                                        </select></td> 
                                <tr>
                                    <td class="headcol">입금계좌*</td>
                                    <td><input name="acc_no" maxlength="30" size="30" onkeyPress="javascript:InpuOnlyNumber(this);" /></td>     
                                    <td class="headcol">계좌주*</tD>
                                    <td ><input name="acc_nm" size="30"  /></td>                        
                                </tr>  
                                <tr>
                                    <td align='left' colspan='4'>대리점 정보</td>                        
                                </tr>
                                <tr>                        
                                    <td class="headcol" width="30%">대리점명</td>
                                    <td><input name="agent_nm" id="agent_nm"  readonly /><input type="hidden" name="agent_seq" id="agent_seq"/></td>
                                    <td class="headcol">대리점수수료</tD>
                                    <td ><input name="agent_commission"  onkeyPress="javascript:InpuOnlyNumber(this);"  /></td>    
                                </tr>
                                <tr>
                                    <td align='left' colspan='4'>담보 정보</td>                        
                                </tr>
                                <tr>                        
                                    <td class="headcol" width="30%">담보종류*</td>
                                    <td><select name="security_cate" >
                                            <option value="">--선택하세요.--</option>
                                        </select></td>
                                    <td class="headcol">담보액*</tD>
                                    <td ><input name="security_amt"  onkeyPress="javascript:InpuOnlyNumber2(this);"   maxlength="15" /><span name="insert_security_amt_han"></span><span>원</span></td>    
                                </tr>     
                                <tr>
                                    <td class="headcol">시작일*</td>
                                    <td><input name="view_security_start_dt" id="view_security_start_dt" maxlength="10" value="<%=strCurDate%>" readonly/><input type="hidden" name="security_start_dt"/></td>
                                    <td class="headcol">종료일*</tD>
                                    <td><input name="view_security_end_dt" id="view_security_end_dt" maxlength="10" value="<%=strTpDefaultEndDt%>" readonly/><input type="hidden" name="security_end_dt"/></td>
                                </tr>  
                                <tr>
                                    <td class="headcol">Memo</td>
                                    <td colspan="3"><textarea name="security_memo" cols="60" rows="3"></textarea></td>                        
                                </tr>                              
                                <tr>
                                    <td align='left' colspan='4' valign="top">
                                        <div id = "common_btn" align = "right" style="height:30px;" >
                                        <input type="button" name="security_addbt" value="담보추가" onclick="javascript:addRow();"/>
                                        <input type="button" name="security_delbt" value="담보삭제" onclick="javascript:delRow();"/>
                                        </div>
                                        <div id="onffmerchantVP2" style="position: relative; width:100%;height:200px;background-color:white;"></div>
                                    </td>                        
                                </tr>                                                   
                            </table>
                        </td>                        
                    </tr>   
                    <tr>                        
                        <td colspan='4'>
                            <table class="gridtable" height="100%" width="100%">   
                                <tr>
                                    <td  align='left' colspan='8'>로그인ID 정보</td>
                                </tr>
                                <tr>
                                    <td class="headcol">아이디</td>
                                    <td><input name="biz_no"  readonly />&nbsp;<button>중복첵크</button></td>
                                    <td class="headcol">비밀번호</td>
                                    <td><input name="corp_no"  readonly /></td>
                                    <td class="headcol">이름</td>
                                    <td><input name="corp_no"  readonly /></td>
                                    <td class="headcol">권한</tD>
                                    <td> <select name="bal_period"  >
                                            <option value="">시스템관리자</option>                                
                                            <option value="">정산관리자</option>
                                            <option value="">결제관리자</option>
                                        </select>
                                    </td>
                                </tr>         
                                <tr>
                                     <td colspan="8" height="110px">
                                        <div id="onff_userVP2" style="position: relative; width:100%;height:100px;background-color:white;"></div>
                                    </td>    
                                </tr>
                            </table>
                        </td>
                    </tr>                             
                    <tr>
                        <td colspan="4" align="center"><input type="button" name="sample" value="등록"/></td>
                    </tr>       
                </table>
                 <input type="hidden" name="commissioninfo" />
            </form>
        </div>
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>