<%@ page language="java" contentType="text/html; charset=euc-kr" %>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="javax.sql.*" %>
<%@ page import="javax.naming.*"%>
<%@ page import="org.apache.log4j.Logger"%>
<%@ page import="com.onoffkorea.system.app.Vo.KiccPgVo"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.io.*" %>
<%@ page import="java.math.BigDecimal"%>;
<%!
    /**
     * 파라미터 체크 메소드
     */
    public String getNullToSpace(String param) {
        return (param == null) ? "" : param.trim();
    }

    public String getNullToZero(String param) {
        return (param == null) ? "0" : param.trim();
    }
        
    
    public void closeResultSet(ResultSet rs)
    {
        try
        {
            if ( rs != null ) rs.close();
        }
        catch(Exception ex)
        {
        }
    }
    
    public void closePstmt(PreparedStatement psmt)
    {
        try
        {
            if ( psmt != null ) psmt.close();
        }
        catch(Exception ex)
        {
        }
    }    
    
    public void closeConnection(Connection con)
    {
        try
        {
            if ( con != null ) con.close();
        }
        catch(Exception ex)
        {
        }
    }      
    
    /* ============================================================================== */
    /* =   null 값을 처리하는 메소드                                                = */
    /* = -------------------------------------------------------------------------- = */
        public String f_get_parm( String val )
        {
          if ( val == null ) val = "";
          return  val;
        }
    /* ============================================================================== */    
%>
<%
    /* ============================================================================== */
    /* =   01. 공통 통보 페이지 설명(필독!!)                                        = */
    /* = -------------------------------------------------------------------------- = */
    /* =   공통 통보 페이지에서는,                                                  = */
    /* =   가상계좌 입금 통보 데이터와 모바일안심결제 통보 데이터 등을 KCP를 통해   = */
    /* =   실시간으로 통보 받을 수 있습니다.                                        = */
    /* =                                                                            = */
    /* =   common_return 페이지는 이러한 통보 데이터를 받기 위한 샘플 페이지        = */
    /* =   입니다. 현재의 페이지를 업체에 맞게 수정하신 후, 아래 사항을 참고하셔서  = */
    /* =   KCP 관리자 페이지에 등록해 주시기 바랍니다.                              = */
    /* =                                                                            = */
    /* =   등록 방법은 다음과 같습니다.                                             = */
    /* =  - KCP 관리자페이지(admin.kcp.co.kr)에 로그인 합니다.                      = */
    /* =  - [쇼핑몰 관리] -> [정보변경] -> [공통 URL 정보] -> 공통 URL 변경 후에    = */
    /* =    가맹점 URL을 입력합니다.                                                = */
    /* ============================================================================== */

	request.setCharacterEncoding ( "euc-kr" ) ;
        
        final String RESULT_SUCCESS = "0000";
        final String RESULT_FAIL   = "5001";
        
        final Logger logger = Logger.getLogger("kacppay_noti_onoff.jsp");

        Connection conn = null;
        PreparedStatement pstmt = null;
        PreparedStatement pstmt2 = null;
        ResultSet rs = null;
        ResultSet rs2 = null;        
        
        //noti 성공여부
        String result_msg = "";
        
    /* ============================================================================== */
    /* =   02. 공통 통보 데이터 받기                                                = */
    /* = -------------------------------------------------------------------------- = */
	String site_cd		= f_get_parm( request.getParameter( "site_cd"		   ) );    // 사이트 코드
	String tno              = f_get_parm( request.getParameter( "tno"              ) );    // KCP 거래번호
	String ordr_idxx        = f_get_parm( request.getParameter( "ordr_idxx"        ) );    // 주문번호
        String tx_cd            = f_get_parm( request.getParameter( "tx_cd"            ) );    // 업무처리 구분 코드
	String tx_tm            = f_get_parm( request.getParameter( "tx_tm"            ) );    // 업무처리 완료 시간
    /* = -------------------------------------------------------------------------- = */
	String cat_id           = "";                                                          // 단말기 번호
	String app_time         = "";                                                          // 승인시각
	String card_mny         = "";                                                          // 결제금액
	String card_quota       = "";                                                          // 할부개월
	String card_no          = "";                                                          // 카드번호(마스킹)
	String card_cd          = "";                                                          // 발급사 코드
	String card_name        = "";                                                          // 발급사 이름
	String acqu_cd          = "";                                                          // 매입사 코드
	String acqu_name        = "";                                                          // 매입사 이름
	String app_no           = "";                                                          // 승인번호
	String bizx_numb        = "";                                                          // 가맹점번호
	String noinf            = "";                                                          // 무이자구분
	String can_time         = "";                                                          // 취소시각

    /* ============================================================================== */
    /* =   03-1. OFFPG 승인 통보 노티 받기                                          = */
    /* = -------------------------------------------------------------------------- = */
    if ( tx_cd.equals("NXC1") )
    {
        cat_id      = f_get_parm( request.getParameter( "cat_id"     ) );           // 단말기 번호
        app_time    = f_get_parm( request.getParameter( "app_time"   ) );           // 승인시각
        card_mny    = f_get_parm( request.getParameter( "card_mny"   ) );           // 결제금액
        card_quota  = f_get_parm( request.getParameter( "card_quota" ) );           // 할부개월
        card_no     = f_get_parm( request.getParameter( "card_no"    ) );           // 카드번호(마스킹)
        card_cd     = f_get_parm( request.getParameter( "card_cd"    ) );           // 발급사 코드
        card_name   = f_get_parm( request.getParameter( "card_name"  ) );           // 발급사 이름
        acqu_cd     = f_get_parm( request.getParameter( "acqu_cd"    ) );           // 매입사 코드
        acqu_name   = f_get_parm( request.getParameter( "acqu_name"  ) );           // 매입사 이름
        app_no      = f_get_parm( request.getParameter( "app_no"     ) );           // 승인번호
        bizx_numb   = f_get_parm( request.getParameter( "bizx_numb"  ) );           // 가맹점번호
        noinf       = f_get_parm( request.getParameter( "noinf"      ) );           // 무이자구분
    }
    /* = -------------------------------------------------------------------------- = */
    /* =   03-2  . OFFPG 취소 통보 노티 받기                                        = */
    /* = -------------------------------------------------------------------------- = */
    else if ( tx_cd.equals("NXC2") )
    {
        cat_id      = f_get_parm( request.getParameter( "cat_id"     ) );           // 단말기 번호
        app_time    = f_get_parm( request.getParameter( "app_time"   ) );           // 승인시각
        card_mny    = f_get_parm( request.getParameter( "card_mny"   ) );           // 결제금액
        card_quota  = f_get_parm( request.getParameter( "card_quota" ) );           // 할부개월
        card_no     = f_get_parm( request.getParameter( "card_no"    ) );           // 카드번호(마스킹)
        card_cd     = f_get_parm( request.getParameter( "card_cd"    ) );           // 발급사 코드
        card_name   = f_get_parm( request.getParameter( "card_name"  ) );           // 발급사 이름
        acqu_cd     = f_get_parm( request.getParameter( "acqu_cd"    ) );           // 매입사 코드
        acqu_name   = f_get_parm( request.getParameter( "acqu_name"  ) );           // 매입사 이름
        app_no      = f_get_parm( request.getParameter( "app_no"     ) );           // 승인번호
        bizx_numb   = f_get_parm( request.getParameter( "bizx_numb"  ) );           // 가맹점번호
        noinf       = f_get_parm( request.getParameter( "noinf"      ) );           // 무이자구분
        can_time    = f_get_parm( request.getParameter( "can_time"   ) );           // 취소시각
    }
    /* ============================================================================== */

    StringBuffer sb_param = new StringBuffer();
        //승인노티
        if ( tx_cd.equals("NXC1") )
        {
            sb_param.append("==========================[cncl noti]=================================\n");
            sb_param.append("IP   	    : " + request.getRemoteAddr()  + "\n");
            sb_param.append("site_cd      : " + site_cd      + "\n");
            sb_param.append("tx_cd        : " + tx_cd        + "\n");
            sb_param.append("tx_tm        : " + tx_tm        + "\n");
            sb_param.append("cat_id       : " + cat_id       + "\n");
            sb_param.append("tno          : " + tno          + "\n");
            sb_param.append("app_time     : " + app_time     + "\n");
            sb_param.append("card_mny     : " + card_mny     + "\n");
            sb_param.append("card_quota   : " + card_quota   + "\n");
            sb_param.append("card_no      : " + card_no      + "\n");
            sb_param.append("card_cd      : " + card_cd      + "\n");
            sb_param.append("card_name    : " + card_name    + "\n");
            sb_param.append("acqu_cd      : " + acqu_cd      + "\n");
            sb_param.append("acqu_name    : " + acqu_name    + "\n");
            sb_param.append("app_no       : " + app_no       + "\n");
            sb_param.append("bizx_numb    : " + bizx_numb    + "\n");
            sb_param.append("noinf        : " + noinf        + "\n");
            sb_param.append("======================================================================\n");

        }
        //취소노티
        else if ( tx_cd.equals("NXC2") )
        {
            sb_param.append("=============================[cncl noti]==============================\n");
            sb_param.append("IP   	    : " + request.getRemoteAddr()  + "\n");
            sb_param.append("site_cd      : " + site_cd      + "\n");
            sb_param.append("tx_cd        : " + tx_cd        + "\n");
            sb_param.append("tx_tm        : " + tx_tm        + "\n");
            sb_param.append("cat_id       : " + cat_id       + "\n");
            sb_param.append("tno          : " + tno          + "\n");
            sb_param.append("app_time     : " + app_time     + "\n");
            sb_param.append("card_mny     : " + card_mny     + "\n");
            sb_param.append("card_quota   : " + card_quota   + "\n");
            sb_param.append("card_no      : " + card_no      + "\n");
            sb_param.append("card_cd      : " + card_cd      + "\n");
            sb_param.append("card_name    : " + card_name    + "\n");
            sb_param.append("acqu_cd      : " + acqu_cd      + "\n");
            sb_param.append("acqu_name    : " + acqu_name    + "\n");
            sb_param.append("app_no       : " + app_no       + "\n");
            sb_param.append("bizx_numb    : " + bizx_numb    + "\n");
            sb_param.append("noinf        : " + noinf        + "\n");
            sb_param.append("can_time     : " + can_time     + "\n");
            sb_param.append("======================================================================\n");
        }

    logger.info("----------noti param---------------");
    logger.info(sb_param.toString());
    logger.info("-------------------------");


    /* ============================================================================== */
    /* =   03. 공통 통보 결과를 업체 자체적으로 DB 처리 작업하시는 부분입니다.      = */
    /* = -------------------------------------------------------------------------- = */
    /* =   통보 결과를 DB 작업 하는 과정에서 정상적으로 통보된 건에 대해 DB 작업을  = */
    /* =   실패하여 DB update 가 완료되지 않은 경우, 결과를 재통보 받을 수 있는     = */
    /* =   프로세스가 구성되어 있습니다. 소스에서 result 라는 Form 값을 생성 하신   = */
    /* =   후, DB 작업이 성공 한 경우, result 의 값을 "0000" 로 세팅해 주시고,      = */
    /* =   DB 작업이 실패 한 경우, result 의 값을 "0000" 이외의 값으로 세팅해 주시  = */
    /* =   기 바랍니다. result 값이 "0000" 이 아닌 경우에는 재통보를 받게 됩니다.   = */
    /* = -------------------------------------------------------------------------- = */


        Date today=new Date();
        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmss");       
        String strCurYear = formater.format(today);
        
        try 
        {
            //DB Connection을 가져온다.
            Context initContext = new InitialContext();                                            //2
            DataSource ds = (DataSource) initContext.lookup("java:comp/env/jdbc/okpay");     //3
            conn = ds.getConnection();                                                             //4
            conn.setAutoCommit(false);
            
            logger.debug("-- strTpNotiseq gen--");
            
            //noti수신일련번호를 만든다..
            String strTpNotiseq = null;
            String sb_TpNotiSql = "select concat(DATE_FORMAT(now(),'%Y%m%d%H%i%S'), LPAD(f_get_sequence('TB_KCP_NOTI'),8,'0'))";
            pstmt = conn.prepareStatement(sb_TpNotiSql);
            rs = pstmt.executeQuery();
            if(rs != null && rs.next())
            {
               strTpNotiseq = rs.getString(1);
            }
            
            closeResultSet(rs);
            closePstmt(pstmt);
            
            logger.debug("-- noti info insert start--");
            
            //noti수신정보를 무조건 저장한다.
            StringBuffer sb_notiinf_ins_sql = new StringBuffer();
            sb_notiinf_ins_sql.append(" insert into TB_KCP_NOTI ");
            sb_notiinf_ins_sql.append(" ( ");
            sb_notiinf_ins_sql.append(" 	noti_seq, ");			
            sb_notiinf_ins_sql.append(" 	res_cd, ");			
            sb_notiinf_ins_sql.append(" 	res_msg, ");		
            sb_notiinf_ins_sql.append(" 	cno, ");			
            sb_notiinf_ins_sql.append(" 	memb_id, ");		
            sb_notiinf_ins_sql.append(" 	amount, ");			
            sb_notiinf_ins_sql.append(" 	order_no, ");		
            sb_notiinf_ins_sql.append(" 	noti_type, ");		
            sb_notiinf_ins_sql.append(" 	auth_no, ");			
            sb_notiinf_ins_sql.append(" 	tran_date, ");		
            sb_notiinf_ins_sql.append(" 	card_no, ");			
            sb_notiinf_ins_sql.append(" 	issuer_cd, ");		
            sb_notiinf_ins_sql.append(" 	issuer_nm, ");		
            sb_notiinf_ins_sql.append(" 	acquirer_cd, ");		
            sb_notiinf_ins_sql.append(" 	acquirer_nm, ");		
            sb_notiinf_ins_sql.append(" 	install_period, ");		
            sb_notiinf_ins_sql.append(" 	noint, ");			
            sb_notiinf_ins_sql.append(" 	canc_date, ");		
            sb_notiinf_ins_sql.append(" 	merch_no, ");		
            sb_notiinf_ins_sql.append(" 	cat_id, ");		
            sb_notiinf_ins_sql.append(" 	reserve1, ");		
            sb_notiinf_ins_sql.append(" 	reserve2, ");		
            sb_notiinf_ins_sql.append(" 	reserve3, ");		
            sb_notiinf_ins_sql.append(" 	reserve4, ");		
            sb_notiinf_ins_sql.append(" 	reserve5, ");		
            sb_notiinf_ins_sql.append(" 	reserve6, ");		
            sb_notiinf_ins_sql.append(" 	user_id, ");			
            sb_notiinf_ins_sql.append(" 	user_nm, ");		
            sb_notiinf_ins_sql.append(" 	INS_USER, ");             
            sb_notiinf_ins_sql.append(" 	MOD_USER ");        
            sb_notiinf_ins_sql.append(" ) values ( ");
            sb_notiinf_ins_sql.append(" 	?, ");//noti_seq
            sb_notiinf_ins_sql.append(" 	?, ");//res_cd
            sb_notiinf_ins_sql.append(" 	?, ");//res_msg
            sb_notiinf_ins_sql.append(" 	?, ");//cno
            sb_notiinf_ins_sql.append(" 	?, ");//memb_id
            sb_notiinf_ins_sql.append(" 	?, ");//amount
            sb_notiinf_ins_sql.append(" 	?, ");//order_no
            sb_notiinf_ins_sql.append(" 	?, ");//noti_type
            sb_notiinf_ins_sql.append(" 	?, ");//auth_no
            sb_notiinf_ins_sql.append(" 	?, ");//tran_date
            sb_notiinf_ins_sql.append(" 	?, ");//card_no
            sb_notiinf_ins_sql.append(" 	?, ");//issuer_cd
            sb_notiinf_ins_sql.append(" 	?, ");//issuer_nm
            sb_notiinf_ins_sql.append(" 	?, ");//acquirer_cd
            sb_notiinf_ins_sql.append(" 	?, ");//acquirer_nm
            sb_notiinf_ins_sql.append(" 	?, ");//install_period
            sb_notiinf_ins_sql.append(" 	?, ");//noint
            sb_notiinf_ins_sql.append(" 	?, ");//canc_date
            sb_notiinf_ins_sql.append(" 	?, ");//merch_no
            sb_notiinf_ins_sql.append(" 	?, ");//cat_id
            sb_notiinf_ins_sql.append(" 	?, ");//reserve1
            sb_notiinf_ins_sql.append(" 	?, ");//reserve2
            sb_notiinf_ins_sql.append(" 	?, ");//reserve3
            sb_notiinf_ins_sql.append(" 	?, ");//reserve4
            sb_notiinf_ins_sql.append(" 	?, ");//reserve5
            sb_notiinf_ins_sql.append(" 	?, ");//reserve6
            sb_notiinf_ins_sql.append(" 	?, ");//user_id
            sb_notiinf_ins_sql.append(" 	?, ");//user_nm
            sb_notiinf_ins_sql.append(" 	?, ");//INS_USER
            sb_notiinf_ins_sql.append(" 	? ");//MOD_USER
            sb_notiinf_ins_sql.append(" ) ");
            
            pstmt = conn.prepareStatement(sb_notiinf_ins_sql.toString());
            pstmt.setString(1	,strTpNotiseq); //noti_seq		
            pstmt.setString(2	,"0000"		); //res_cd			
            pstmt.setString(3	,"sucess"	); //res_msg		
            pstmt.setString(4	,tno		); //cno			
            pstmt.setString(5	,site_cd	); //memb_id		
            pstmt.setString(6	,card_mny		); //amount			
            pstmt.setString(7	,ordr_idxx		); //order_no		
            pstmt.setString(8	,tx_cd		); //noti_type		
            pstmt.setString(9	,app_no		); //auth_no			
            pstmt.setString(10	, app_time);//tran_date
            pstmt.setString(11	, card_no);//card_no
            pstmt.setString(12	, card_cd);//issuer_cd
            pstmt.setString(13	, card_name);//issuer_nm
            pstmt.setString(14	, acqu_cd);//acquirer_cd
            pstmt.setString(15	, acqu_name);//acquirer_nm
            pstmt.setString(16	, card_quota);//install_period
            pstmt.setString(17	, noinf);//noint
            pstmt.setString(18	, can_time);//canc_date
            pstmt.setString(19	, bizx_numb);//merch_no
            pstmt.setString(20	, cat_id);//cat_id
            pstmt.setString(21	, null);//reserve1
            pstmt.setString(22	, null);//reserve2
            pstmt.setString(23	, null);//reserve3
            pstmt.setString(24	, null);//reserve4
            pstmt.setString(25	, null);//reserve5
            pstmt.setString(26	, null);//reserve6
            pstmt.setString(27	, null);//user_id
            pstmt.setString(28	, null);//user_nm
            pstmt.setString(29	,"0"); //INS_USER             
            pstmt.setString(30	,"0"); //MOD_USER   
            
            int intNotiIns = pstmt.executeUpdate();
            
            conn.commit();    
            
            closePstmt(pstmt);

            logger.debug("-- onoffmerchinfo search --");
            
            //수신된 정보로 가맹점 정보를 검색
            StringBuffer sb_onffTidInfo = new StringBuffer();

            sb_onffTidInfo.append(" select m.onfftid, m.onffmerch_no, m.pay_chn_cate, n.terminal_no, n.merch_no ");
            sb_onffTidInfo.append(" from TB_ONFFTID_MST m, ");
            sb_onffTidInfo.append("       ( ");
            sb_onffTidInfo.append("           SELECT b.pay_mtd_seq, a.start_dt, a.end_dt, b.APP_ISS_CD, b.TID_MTD, b.TERMINAL_NO,b.PAY_MTD,b.MERCH_NO  ");
            sb_onffTidInfo.append("            FROM TB_PAY_MTD a ");
            sb_onffTidInfo.append("                      ,TB_PAY_MTD_DETAIL b ");
            sb_onffTidInfo.append("            WHERE  ");
            sb_onffTidInfo.append("            a.PAY_MTD_SEQ = b.PAY_MTD_SEQ ");
            sb_onffTidInfo.append("            and b.DEL_FLAG  = 'N' ");
            sb_onffTidInfo.append("            and b.USE_FLAG = 'Y' ");
            sb_onffTidInfo.append("            and b.DEL_FLAG  = 'N' ");
            sb_onffTidInfo.append("            and b.USE_FLAG = 'Y' ");
            sb_onffTidInfo.append("            and a.start_dt <= DATE_FORMAT(now(),'%Y%m%d') ");
            sb_onffTidInfo.append("            and a.end_dt  >= DATE_FORMAT(now(),'%Y%m%d') ");
            sb_onffTidInfo.append("       ) n ");
            sb_onffTidInfo.append(" where  ");
            sb_onffTidInfo.append(" m.PAY_MTD_SEQ = n.pay_mtd_seq        ");
            sb_onffTidInfo.append(" and m.del_flag = 'N'  ");
            sb_onffTidInfo.append(" and n.TERMINAL_NO = ? ");

            pstmt = conn.prepareStatement(sb_onffTidInfo.toString());

            pstmt.setString(1, site_cd);

            rs = pstmt.executeQuery();

            String strOnffTid = "";
            String strOnffMerchNo = "";
            String strPayChnCate = "";

            if(rs != null && rs.next())
            {
                strOnffTid = getNullToSpace(rs.getString(1));
                strOnffMerchNo = getNullToSpace(rs.getString(2));
                strPayChnCate =  getNullToSpace(rs.getString(3));
            }
            
            closeResultSet(rs);
            closePstmt(pstmt);
           
            logger.trace("strOnffTid : "+ strOnffTid);
            logger.trace("strOnffMerchNo : "+ strOnffMerchNo);
            logger.trace("strPayChnCate : " + strPayChnCate);
            
            String strTpCms = "";
            
            //승인이 가맹점정보가 있을시 수수료 정보를 가져온다.
            if(!strOnffTid.equals("") && !strOnffMerchNo.equals("") && tx_cd.equals("NXC1"))
            {
                logger.debug("-- onofftidinfo commisioninfo search --");
                //가맹점 정보에 대한 수수료 정보를 가져온다.
                StringBuffer sb_onffcms = new StringBuffer();
                sb_onffcms.append(" SELECT ONFFTID_CMS_SEQ, ONFFTID, COMMISSION, START_DT, END_DT ");
                sb_onffcms.append("  FROM TB_ONFFTID_COMMISSION ");
                sb_onffcms.append("  WHERE ONFFTID = ? ");
                sb_onffcms.append("  and DEL_FLAG  = 'N' ");
                sb_onffcms.append("  and USE_FLAG = 'Y' ");
                sb_onffcms.append("  and START_DT <= ? ");
                sb_onffcms.append("  and END_DT >= ? ");

                String strTpAppDt = app_time.substring(0,8);

                pstmt = conn.prepareStatement(sb_onffcms.toString());

                pstmt.setString(1, strOnffTid);
                pstmt.setString(2, strTpAppDt);
                pstmt.setString(3, strTpAppDt);
                rs = pstmt.executeQuery();  

                if(rs != null && rs.next())
                {
                    strTpCms = rs.getString(3);
                }
                closeResultSet(rs);
                closePstmt(pstmt);
                
                logger.trace("strTpCms : " + strTpCms);
            }            
            
            //가맹점번호가 없을경우
            if(strOnffTid.equals("") || strOnffMerchNo.equals(""))
            {
                logger.debug("-- onoff merch, tild info null --");
                //noti처리상태 update
                StringBuffer sb_noti_mod_sql = new StringBuffer();
                sb_noti_mod_sql.append(" UPDATE TB_KCP_NOTI SET ");
                sb_noti_mod_sql.append(" proc_flag=? ");
                sb_noti_mod_sql.append(" ,mod_dt = DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                sb_noti_mod_sql.append(" ,mod_user= '0' ");                
                sb_noti_mod_sql.append(" WHERE noti_seq = ? ");
                pstmt = conn.prepareStatement(sb_noti_mod_sql.toString());
                pstmt.setString(1, "50");
                pstmt.setString(2, strTpNotiseq);
                int int_mod_notiinfo = pstmt.executeUpdate();  
                
                closeResultSet(rs);
                closePstmt(pstmt);
            }
            //승인이면서 수수료 정보가 없는경우
            else if( tx_cd.equals("NXC1") && strTpCms.equals(""))
            {
                logger.debug("-- onoff tid cms info null --");
                //noti처리상태 update
                StringBuffer sb_noti_mod_sql = new StringBuffer();
                sb_noti_mod_sql.append(" UPDATE TB_KCP_NOTI SET ");
                sb_noti_mod_sql.append(" proc_flag=? ");
                sb_noti_mod_sql.append(" ,mod_dt = DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                sb_noti_mod_sql.append(" ,mod_user= '0' ");                
                sb_noti_mod_sql.append(" WHERE noti_seq = ? ");
                pstmt = conn.prepareStatement(sb_noti_mod_sql.toString());
                pstmt.setString(1, "50");
                pstmt.setString(2, strTpNotiseq);
                int int_mod_notiinfo = pstmt.executeUpdate();  
                
                closeResultSet(rs);
                closePstmt(pstmt);
            }
            else
            {
                 logger.debug("-- onoff info check sucess --");
                //승인 경우
                if(tx_cd.equals("NXC1"))
                {
                    logger.debug("-- notiinfo app --");
                    //신용카드일 경우
                    //if(r_pay_type.equals("11"))
                    {
                        logger.debug("-- notiinfo app card --");
                        
                        //이미 입력거래가 있는경우
                        StringBuffer sbTranChkSql = new StringBuffer();
                        sbTranChkSql.append(" select count(1) cnt ");
                        sbTranChkSql.append(" from VW_TRAN_CARDPG ");
                        sbTranChkSql.append(" where pg_seq = ? ");
                        sbTranChkSql.append(" and TID = ? ");
                        sbTranChkSql.append(" and massagetype='10' ");

                        pstmt = conn.prepareStatement(sbTranChkSql.toString());

                        pstmt.setString(1, tno);
                        pstmt.setString(2, site_cd);

                        rs = pstmt.executeQuery();

                        int int_appChk_cnt = 0;

                        if(rs != null && rs.next())
                        {
                            int_appChk_cnt = rs.getInt("cnt");
                        }

                        closeResultSet(rs);
                        closePstmt(pstmt);                    
                        logger.debug("-- tran cnt chkeck --");
                        String strNotResult = null;
                        //접수된 승인건이 없는 경우
                        if(int_appChk_cnt == 0)
                        {
                            logger.debug("-- tran cnt chkeck ok  --");
                            logger.debug("-- isscd transger  --");
                            //발급사코드 변환
                            StringBuffer sb_isscd = new StringBuffer();
                            sb_isscd.append(" select main_code, detail_code, code_nm detailcode_nm ");
                            sb_isscd.append(" from TB_CODE_DETAIL ");
                            sb_isscd.append(" where main_code = 'ISS_CD' ");
                            sb_isscd.append(" and use_flag = 'Y' ");
                            sb_isscd.append(" and del_flag='N' ");
                            sb_isscd.append(" and detail_code = f_get_iss_cd('KCPPG', ?) ");
                            
                            pstmt = conn.prepareStatement(sb_isscd.toString());
                            pstmt.setString(1, card_cd);

                            rs = pstmt.executeQuery();                            

                            String strOnffIssCd = null;
                            String strOnffIssNm = null;

                            if(rs != null && rs.next())
                            {
                                strOnffIssCd = rs.getString(2);
                                strOnffIssNm = rs.getString(3);
                            }    
                            else
                            {
                                strOnffIssCd = "091";
                                strOnffIssNm = "기타카드";
                            }
                            closeResultSet(rs);
                            closePstmt(pstmt);
                            
                            logger.debug("-- acqcd transger  --");
                            //매입사코드변환
                            StringBuffer sb_acqcd = new StringBuffer();
                            sb_acqcd.append(" select main_code, detail_code, code_nm detailcode_nm ");
                            sb_acqcd.append(" from TB_CODE_DETAIL ");
                            sb_acqcd.append(" where main_code = 'ISS_CD' ");
                            sb_acqcd.append(" and use_flag = 'Y' ");
                            sb_acqcd.append(" and del_flag='N' ");
                            sb_acqcd.append(" and detail_code = f_get_iss_cd('KCPPG', ?) ");
                            
                            pstmt = conn.prepareStatement(sb_acqcd.toString());
                            pstmt.setString(1, acqu_cd);
                            
                            rs = pstmt.executeQuery(); 
                            
                            String strOnffAcqCd = null;
                            String strOnffAcqNm = null;

                            if(rs != null && rs.next())
                            {
                                strOnffAcqCd = rs.getString(2);
                                strOnffAcqNm = rs.getString(3);
                            }
                            else
                            {
                                strOnffAcqCd = "091";
                                strOnffAcqNm = "기타카드";
                            }
                            closeResultSet(rs);
                            closePstmt(pstmt);

                            logger.debug("-- isscd transger  --");
                            //수수료를 계산한다.
                            //double dbl_TpCms = Double.parseDouble(strTpCms)/Double.parseDouble("100");
                            //double dbl_TpApp_mat = Double.parseDouble(card_mny);
                            //double dbl_onfftid_cms = Math.floor(dbl_TpCms * dbl_TpApp_mat);
                            //double dbl_onfftid_payamt = dbl_TpApp_mat - dbl_onfftid_cms;

						    BigDecimal bigdec_TpCms = new BigDecimal(strTpCms).divide(new BigDecimal("100"));
                            BigDecimal bigdec_TpApp_mat = new BigDecimal(card_mny);
                            BigDecimal bigdec_onfftid_cmd = bigdec_TpApp_mat.multiply(bigdec_TpCms);
                                
                            double dbl_onfftid_cms = Math.floor( bigdec_onfftid_cmd.doubleValue() );                                
                            BigDecimal bigdec_onfftid_payamt = bigdec_TpApp_mat.subtract(new BigDecimal(String.valueOf(dbl_onfftid_cms))); 


                            logger.debug("-- strTpTranseq create  --");
                            //거래일련번호를 생성한다.
                            String strTpTranseq = null;
                            String sb_TpTranseqSql = "select concat(DATE_FORMAT(now(),'%Y%m%d%H%i%S'), LPAD(f_get_sequence('TRANSEQ'),8,'0'))";
                            pstmt = conn.prepareStatement(sb_TpTranseqSql);
                            rs = pstmt.executeQuery();
                            if(rs != null && rs.next())
                            {
                                strTpTranseq = rs.getString(1);
                            }                

                            closeResultSet(rs);
                            closePstmt(pstmt);
                            logger.debug("strTpTranseq : "+strTpTranseq);
                            logger.debug("-- traninfo insert  --");
                            //해당거래정보를 insert 한다.
                            StringBuffer sb_instraninfo = new StringBuffer();
                            sb_instraninfo.append(" insert ");
                            sb_instraninfo.append(" into TB_TRAN_CARDPG_"+app_time.substring(2, 6) + " ( ");
                            sb_instraninfo.append(" TRAN_SEQ			 ");
                            sb_instraninfo.append(" ,MASSAGETYPE		 ");
                            sb_instraninfo.append(" ,CARD_NUM			 ");
                            sb_instraninfo.append(" ,APP_DT				 ");
                            sb_instraninfo.append(" ,APP_TM				 ");
                            sb_instraninfo.append(" ,APP_NO				 ");
                            sb_instraninfo.append(" ,TOT_AMT			 ");	
                            sb_instraninfo.append(" ,PG_SEQ				 ");
                            sb_instraninfo.append(" ,PAY_CHN_CATE		 ");	
                            sb_instraninfo.append(" ,TID				 ");	
                            sb_instraninfo.append(" ,ONFFTID			 ");	
                            sb_instraninfo.append(" ,WCC				 ");	
                            sb_instraninfo.append(" ,EXPIRE_DT			 ");
                            sb_instraninfo.append(" ,CARD_CATE			 ");
                            sb_instraninfo.append(" ,ORDER_SEQ			 ");
                            sb_instraninfo.append(" ,TRAN_DT			 ");
                            sb_instraninfo.append(" ,TRAN_TM			 ");	
                            sb_instraninfo.append(" ,TRAN_CATE			 ");
                            sb_instraninfo.append(" ,FREE_INST_FLAG		 ");
                            sb_instraninfo.append(" ,TAX_AMT			 ");	
                            sb_instraninfo.append(" ,SVC_AMT			 ");	
                            sb_instraninfo.append(" ,CURRENCY			 ");
                            sb_instraninfo.append(" ,INSTALLMENT		 ");	
                            sb_instraninfo.append(" ,ISS_CD				 ");
                            sb_instraninfo.append(" ,ISS_NM				 ");
                            sb_instraninfo.append(" ,APP_ISS_CD			 ");
                            sb_instraninfo.append(" ,APP_ISS_NM			 ");
                            sb_instraninfo.append(" ,ACQ_CD				 ");
                            sb_instraninfo.append(" ,ACQ_NM			 ");
                            sb_instraninfo.append(" ,APP_ACQ_CD			 ");
                            sb_instraninfo.append(" ,APP_ACQ_NM			 ");
                            sb_instraninfo.append(" ,MERCH_NO			 ");
                            sb_instraninfo.append(" ,ORG_MERCH_NO		 ");
                            sb_instraninfo.append(" ,RESULT_CD			 ");
                            sb_instraninfo.append(" ,RESULT_MSG			 ");
                            sb_instraninfo.append(" ,ORG_APP_DD			 ");
                            sb_instraninfo.append(" ,ORG_APP_NO			 ");
                            sb_instraninfo.append(" ,CNCL_REASON		 ");	
                            sb_instraninfo.append(" ,TRAN_STATUS		 ");	
                            sb_instraninfo.append(" ,RESULT_STATUS		 ");
                            sb_instraninfo.append(" ,TRAN_STEP			 ");
                            sb_instraninfo.append(" ,CNCL_DT			 ");	
                            sb_instraninfo.append(" ,ACQ_DT				 ");
                            sb_instraninfo.append(" ,ACQ_RESULT_CD		 ");
                            sb_instraninfo.append(" ,HOLDOFF_DT			 ");
                            sb_instraninfo.append(" ,HOLDON_DT			 ");
                            sb_instraninfo.append(" ,PAY_DT				 ");
                            sb_instraninfo.append(" ,PAY_AMT			 ");	
                            sb_instraninfo.append(" ,COMMISION			 ");
                            sb_instraninfo.append(" ,ONOFFTID_PAY_AMT	 ");	
                            sb_instraninfo.append(" ,ONOFFTID_COMMISION	 ");
                            sb_instraninfo.append(" ,CLIENT_IP			 ");
                            sb_instraninfo.append(" ,USER_TYPE			 ");
                            sb_instraninfo.append(" ,MEMB_USER_NO		 ");
                            sb_instraninfo.append(" ,USER_ID				 ");
                            sb_instraninfo.append(" ,USER_NM			 ");
                            sb_instraninfo.append(" ,USER_MAIL			 ");
                            sb_instraninfo.append(" ,USER_PHONE1		 ");
                            sb_instraninfo.append(" ,USER_PHONE2		 ");
                            sb_instraninfo.append(" ,USER_ADDR			 ");
                            sb_instraninfo.append(" ,PRODUCT_TYPE		 ");
                            sb_instraninfo.append(" ,PRODUCT_NM		 ");
                            sb_instraninfo.append(" ,FILLER				 ");
                            sb_instraninfo.append(" ,FILLER2				 ");
                            sb_instraninfo.append(" ,TERM_FILLER1		 ");
                            sb_instraninfo.append(" ,TERM_FILLER2		 ");	
                            sb_instraninfo.append(" ,TERM_FILLER3		 ");	
                            sb_instraninfo.append(" ,TERM_FILLER4		 ");	
                            sb_instraninfo.append(" ,TERM_FILLER5		 ");	
                            sb_instraninfo.append(" ,TERM_FILLER6		 ");	
                            sb_instraninfo.append(" ,TERM_FILLER7		 ");	
                            sb_instraninfo.append(" ,TERM_FILLER8		 ");	
                            sb_instraninfo.append(" ,TERM_FILLER9		 ");	
                            sb_instraninfo.append(" ,TERM_FILLER10		 ");
                            sb_instraninfo.append(" ,TRAD_CHK_FLAG		 ");
                            sb_instraninfo.append(" ,ACC_CHK_FLAG		 ");
                            sb_instraninfo.append(" ,INS_DT				 ");
                            sb_instraninfo.append(" ,MOD_DT				 ");
                            sb_instraninfo.append(" ,INS_USER			 ");	
                            sb_instraninfo.append(" ,MOD_USER			 ");
                            sb_instraninfo.append(" ,ORG_PG_SEQ			 ");
                            sb_instraninfo.append(" ,ONFFMERCH_NO	 ");
                            sb_instraninfo.append(" ,auth_pw ");
                            sb_instraninfo.append(" ,auth_value ");
                            sb_instraninfo.append(" ,app_card_num ");
                            sb_instraninfo.append(" )  ");
                            sb_instraninfo.append(" values ( ");
                            sb_instraninfo.append("  ? ");//tran_seq			
                            sb_instraninfo.append(" ,? ");//massagetype			
                            sb_instraninfo.append(" ,? ");//card_num			
                            sb_instraninfo.append(" ,? ");//app_dt			
                            sb_instraninfo.append(" ,? ");//app_tm			
                            sb_instraninfo.append(" ,? ");//app_no			
                            sb_instraninfo.append(" ,? ");//tot_amt			
                            sb_instraninfo.append(" ,? ");//pg_seq			
                            sb_instraninfo.append(" ,? ");//pay_chn_cate		
                            sb_instraninfo.append(" ,? ");//tid				
                            sb_instraninfo.append(" ,? ");//onfftid			
                            sb_instraninfo.append(" ,? ");//wcc				
                            sb_instraninfo.append(" ,? ");//expire_dt			
                            sb_instraninfo.append(" ,? ");//card_cate			
                            sb_instraninfo.append(" ,? ");//order_seq			
                            sb_instraninfo.append(" ,? ");//tran_dt			
                            sb_instraninfo.append(" ,? ");//tran_tm			
                            sb_instraninfo.append(" ,? ");//tran_cate			
                            sb_instraninfo.append(" ,? ");//free_inst_flag		
                            sb_instraninfo.append(" ,? ");//tax_amt			
                            sb_instraninfo.append(" ,? ");//svc_amt			
                            sb_instraninfo.append(" ,? ");//currency			
                            sb_instraninfo.append(" ,? ");//installment			
                            sb_instraninfo.append(" ,? ");//iss_cd			
                            sb_instraninfo.append(" ,? ");//iss_nm			
                            sb_instraninfo.append(" ,? ");//app_iss_cd			
                            sb_instraninfo.append(" ,? ");//app_iss_nm			
                            sb_instraninfo.append(" ,? ");//acq_cd			
                            sb_instraninfo.append(" ,? ");//acq_nm			
                            sb_instraninfo.append(" ,? ");//app_acq_cd			
                            sb_instraninfo.append(" ,? ");//app_acq_nm			
                            sb_instraninfo.append(" ,? ");//merch_no			
                            sb_instraninfo.append(" ,? ");//org_merch_no		
                            sb_instraninfo.append(" ,? ");//result_cd			
                            sb_instraninfo.append(" ,? ");//result_msg			
                            sb_instraninfo.append(" ,? ");//org_app_dd			
                            sb_instraninfo.append(" ,? ");//org_app_no			
                            sb_instraninfo.append(" ,? ");//cncl_reason			
                            sb_instraninfo.append(" ,? ");//tran_status			
                            sb_instraninfo.append(" ,? ");//result_status		
                            sb_instraninfo.append(" ,? ");//tran_step			
                            sb_instraninfo.append(" ,? ");//cncl_dt			
                            sb_instraninfo.append(" ,? ");//acq_dt			
                            sb_instraninfo.append(" ,? ");//acq_result_cd
                            sb_instraninfo.append(" ,? ");//holdoff_dt
                            sb_instraninfo.append(" ,? ");//holdon_dt
                            sb_instraninfo.append(" ,? ");//pay_dt
                            sb_instraninfo.append(" ,? ");//pay_amt
                            sb_instraninfo.append(" ,? ");//commision
                            sb_instraninfo.append(" ,? ");//onofftid_pay_amt
                            sb_instraninfo.append(" ,? ");//onofftid_commision
                            sb_instraninfo.append(" ,? ");//client_ip
                            sb_instraninfo.append(" ,? ");//user_type
                            sb_instraninfo.append(" ,? ");//memb_user_no
                            sb_instraninfo.append(" ,? ");//user_id
                            sb_instraninfo.append(" ,? ");//user_nm
                            sb_instraninfo.append(" ,? ");//user_mail
                            sb_instraninfo.append(" ,? ");//user_phone1
                            sb_instraninfo.append(" ,? ");//user_phone2
                            sb_instraninfo.append(" ,? ");//user_addr
                            sb_instraninfo.append(" ,? ");//product_type
                            sb_instraninfo.append(" ,? ");//product_nm
                            sb_instraninfo.append(" ,? ");//filler
                            sb_instraninfo.append(" ,? ");//filler2
                            sb_instraninfo.append(" ,? ");//term_filler1
                            sb_instraninfo.append(" ,? ");//term_filler2
                            sb_instraninfo.append(" ,? ");//term_filler3
                            sb_instraninfo.append(" ,? ");//term_filler4
                            sb_instraninfo.append(" ,? ");//term_filler5
                            sb_instraninfo.append(" ,? ");//term_filler6
                            sb_instraninfo.append(" ,? ");//term_filler7
                            sb_instraninfo.append(" ,? ");//term_filler8
                            sb_instraninfo.append(" ,? ");//term_filler9
                            sb_instraninfo.append(" ,? ");//term_filler10
                            sb_instraninfo.append(" ,? ");//trad_chk_flag
                            sb_instraninfo.append(" ,? ");//acc_chk_flag
                            sb_instraninfo.append("  , DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                            sb_instraninfo.append("  , DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                            sb_instraninfo.append(" ,? ");//ins_user			
                            sb_instraninfo.append(" ,? ");//mod_user			
                            sb_instraninfo.append(" ,? ");//org_pg_seq			
                            sb_instraninfo.append(" ,? ");//onffmerch_no		
                            sb_instraninfo.append(" ,? ");//auth_pw		  
                            sb_instraninfo.append(" ,? ");//auth_value		  
                            sb_instraninfo.append(" ,? ");//app_card_num	
                            sb_instraninfo.append(" ) ");          

                            pstmt = conn.prepareStatement(sb_instraninfo.toString());

                            pstmt.setString(1,strTpTranseq);//tran_seq				, jdbcType=VARCHAR} 
                            pstmt.setString(2,"10");//massagetype			, jdbcType=VARCHAR} 
                            pstmt.setString(3,card_no);//card_num			, jdbcType=VARCHAR} 
                            pstmt.setString(4,app_time.substring(0, 8));//app_dt				, jdbcType=VARCHAR}
                            pstmt.setString(5,app_time.substring(8, 14));//app_tm				 , jdbcType=VARCHAR}
                            pstmt.setString(6,app_no);//app_no				, jdbcType=VARCHAR}
                            pstmt.setString(7,card_mny);//tot_amt				 , jdbcType=INTEGER }
                            pstmt.setString(8,tno);//pg_seq				, jdbcType=VARCHAR}
                            pstmt.setString(9,strPayChnCate);//pay_chn_cate			 , jdbcType=VARCHAR}
                            pstmt.setString(10,site_cd);//tid					, jdbcType=VARCHAR}
                            pstmt.setString(11,strOnffTid);//onfftid				, jdbcType=VARCHAR}
                            pstmt.setString(12,null);//wcc					, jdbcType=VARCHAR}
                            pstmt.setString(13,"0000");//expire_dt			, jdbcType=VARCHAR}
                            pstmt.setString(14,null);//card_cate			  , jdbcType=VARCHAR}
                            pstmt.setString(15,ordr_idxx);//order_seq			  , jdbcType=VARCHAR}
                            pstmt.setString(16,app_time.substring(0, 8));//tran_dt				, jdbcType=VARCHAR}
                            pstmt.setString(17,app_time.substring(8, 14));//tran_tm				, jdbcType=VARCHAR}
                            //pstmt.setString(18,r_noti_type);//tran_cate			  , jdbcType=VARCHAR}
                            pstmt.setString(18,"20");//tran_cate			  , jdbcType=VARCHAR}
                            
                            String r_noint = null;
                            //무이자일경우
                            if(noinf.equals("Y"))
                            {
                                r_noint = "02";
                            }
                            //일반일경우
                            else
                            {
                                r_noint = "00";
                            }
                            
                            pstmt.setString(19,r_noint);//free_inst_flag		    , jdbcType=VARCHAR}
                            pstmt.setDouble(20,0);//tax_amt				, jdbcType=INTEGER }
                            pstmt.setDouble(21,0);//svc_amt				, jdbcType=INTEGER }
                            pstmt.setString(22,null);//currency			 , jdbcType=VARCHAR}
                            pstmt.setString(23,card_quota);//installment			   , jdbcType=VARCHAR}
                            pstmt.setString(24,strOnffIssCd);//iss_cd				, jdbcType=VARCHAR}
                            pstmt.setString(25,strOnffIssNm);//iss_nm				 , jdbcType=VARCHAR}
                            pstmt.setString(26,card_cd);//app_iss_cd			 , jdbcType=VARCHAR}
                            pstmt.setString(27,card_name);//app_iss_nm			 , jdbcType=VARCHAR}
                            pstmt.setString(28,strOnffAcqCd);//acq_cd				, jdbcType=VARCHAR}
                            pstmt.setString(29,strOnffAcqNm);//acq_nm				, jdbcType=VARCHAR}
                            pstmt.setString(30,acqu_cd);//app_acq_cd			  , jdbcType=VARCHAR}
                            pstmt.setString(31,acqu_name);//app_acq_nm			   , jdbcType=VARCHAR}
                            pstmt.setString(32,site_cd);//merch_no			  , jdbcType=VARCHAR}
                            pstmt.setString(33,bizx_numb);//org_merch_no			  , jdbcType=VARCHAR}
                            pstmt.setString(34,"0000");//result_cd			 , jdbcType=VARCHAR}
                            pstmt.setString(35, "성공(noti수신)");//result_msg			   , jdbcType=VARCHAR}
                            pstmt.setString(36,null);//org_app_dd			  , jdbcType=VARCHAR}
                            pstmt.setString(37,null);//org_app_no			   , jdbcType=VARCHAR}
                            pstmt.setString(38,null);//cncl_reason			, jdbcType=VARCHAR}
                            pstmt.setString(39,"00");//tran_status			   , jdbcType=VARCHAR}
                            pstmt.setString(40,"00");//result_status		     , jdbcType=VARCHAR}
                            pstmt.setString(41,"00");//tran_step			 , jdbcType=VARCHAR}
                            pstmt.setString(42,null);//cncl_dt				, jdbcType=VARCHAR}
                            pstmt.setString(43,null);//acq_dt				, jdbcType=VARCHAR}
                            pstmt.setString(44,null);//acq_result_cd		     , jdbcType=VARCHAR}
                            pstmt.setString(45,null);//holdoff_dt			  , jdbcType=VARCHAR}
                            pstmt.setString(46,null);//holdon_dt			  , jdbcType=VARCHAR}
                            pstmt.setString(47,null);//pay_dt				, jdbcType=VARCHAR}
                            pstmt.setString(48,null);//pay_amt				, jdbcType=INTEGER }
                            pstmt.setString(49,null);//commision			  , jdbcType=INTEGER }
                            //pstmt.setDouble(50,dbl_onfftid_payamt);//onofftid_pay_amt		, jdbcType=INTEGER }
                            //pstmt.setDouble(51,dbl_onfftid_cms);//onofftid_commision		, jdbcType=INTEGER }
							pstmt.setString(50,String.valueOf((long)bigdec_onfftid_payamt.doubleValue()) );//onofftid_pay_amt		, jdbcType=INTEGER }
                            pstmt.setString(51,String.valueOf((long)dbl_onfftid_cms));//onofftid_commision		, jdbcType=INTEGER }
                            pstmt.setString(52,null);//client_ip				, jdbcType=VARCHAR}
                            pstmt.setString(53,null);//user_type			, jdbcType=VARCHAR}
                            pstmt.setString(54,null);//memb_user_no		, jdbcType=VARCHAR}
                            pstmt.setString(55,null);//user_id				, jdbcType=VARCHAR}
                            pstmt.setString(56,null);//user_nm				, jdbcType=VARCHAR}
                            pstmt.setString(57,null);//user_mail			    , jdbcType=VARCHAR}
                            pstmt.setString(58,null);//user_phone1			 , jdbcType=VARCHAR}
                            pstmt.setString(59,null);//user_phone2			 , jdbcType=VARCHAR}
                            pstmt.setString(60,null);//user_addr			, jdbcType=VARCHAR}
                            pstmt.setString(61,null);//product_type		  , jdbcType=VARCHAR}
                            pstmt.setString(62,null);//product_nm			 , jdbcType=VARCHAR}
                            pstmt.setString(63,null);//filler				   , jdbcType=VARCHAR}
                            pstmt.setString(64,null);//filler2				   , jdbcType=VARCHAR}
                            pstmt.setString(65,null);//term_filler1			, jdbcType=VARCHAR}
                            pstmt.setString(66,null);//term_filler2			, jdbcType=VARCHAR}
                            pstmt.setString(67,null);//term_filler3			, jdbcType=VARCHAR}
                            pstmt.setString(68,null);//term_filler4			, jdbcType=VARCHAR}
                            pstmt.setString(69,null);//term_filler5			, jdbcType=VARCHAR}
                            pstmt.setString(70,null);//term_filler6				, jdbcType=VARCHAR}
                            pstmt.setString(71,null);//term_filler7			  , jdbcType=VARCHAR}
                            pstmt.setString(72,null);//term_filler8			, jdbcType=VARCHAR}
                            pstmt.setString(73,null);//term_filler9			, jdbcType=VARCHAR}
                            pstmt.setString(74,null);//term_filler10			, jdbcType=VARCHAR}
                            pstmt.setString(75,"0");//trad_chk_flag		  , jdbcType=VARCHAR}
                            pstmt.setString(76,"0");//acc_chk_flag			, jdbcType=VARCHAR}
                            pstmt.setString(77,"0");//ins_user				     , jdbcType=VARCHAR}
                            pstmt.setString(78,"0");//mod_user			, jdbcType=VARCHAR}
                            pstmt.setString(79,null);//org_pg_seq			, jdbcType=VARCHAR}
                            pstmt.setString(80,strOnffMerchNo);//onffmerch_no		  , jdbcType=VARCHAR}
                            pstmt.setString(81,null);//auth_pw		  , jdbcType=VARCHAR}
                            pstmt.setString(82,null);//auth_value		  , jdbcType=VARCHAR}   
                            pstmt.setString(83,card_no);//app_card_num		  , jdbcType=VARCHAR}                

                            int int_insResult = pstmt.executeUpdate();                               
                            logger.debug("-- traninfo insert end  --");
                            closeResultSet(rs);
                            closePstmt(pstmt);

                            strNotResult = "10";//처리완료
                        }
                        //접수된 승인건이 있는 경우
                        else
                        {
                            logger.debug("-- traninfo dual!!  --");
                            strNotResult = "20";//기존재건 존재
                        }

                        logger.debug("-- notiinfo update!!  --");
                        //noti처리상태 update
                        StringBuffer sb_noti_mod_sql = new StringBuffer();
                        sb_noti_mod_sql.append(" UPDATE TB_KCP_NOTI SET ");
                        sb_noti_mod_sql.append(" proc_flag=? ");
                        sb_noti_mod_sql.append(" ,mod_dt = DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                        sb_noti_mod_sql.append(" ,mod_user= '0' ");                
                        sb_noti_mod_sql.append(" WHERE noti_seq = ? ");

                        pstmt = conn.prepareStatement(sb_noti_mod_sql.toString());

                        pstmt.setString(1, strNotResult);
                        pstmt.setString(2, strTpNotiseq);

                        int int_mod_notiinfo = pstmt.executeUpdate();
                    }
                }
                //취소일경우
                else if(tx_cd.equals("NXC2"))
                {
                     logger.debug("-- noti cncl  --");
                    //카드취소일 경우
                    //if(r_pay_type.equals("11"))
                    {
                        logger.debug("-- noti cncl card  --");
                        
                        //이미 입력거래가 있는경우
                        StringBuffer sbTranChkSql = new StringBuffer();
                        sbTranChkSql.append(" select count(1) cnt ");
                        sbTranChkSql.append(" from VW_TRAN_CARDPG ");
                        sbTranChkSql.append(" where pg_seq = ? ");
                        sbTranChkSql.append(" and TID = ? ");
                        sbTranChkSql.append(" and massagetype='40' ");
                        sbTranChkSql.append(" and RESULT_STATUS='00' ");

                        pstmt = conn.prepareStatement(sbTranChkSql.toString());

                        pstmt.setString(1, tno);
                        pstmt.setString(2, site_cd);

                        rs = pstmt.executeQuery();

                        int int_CnclChk_cnt = 0;

                        if(rs != null && rs.next())
                        {
                            int_CnclChk_cnt = rs.getInt("cnt");
                        }

                        closeResultSet(rs);
                        closePstmt(pstmt);                    
                       logger.debug("-- tran cnt chkeck --");
                        String strNotResult = null;
                        //접수된 승인건이 없는 경우
                        if(int_CnclChk_cnt == 0)
                        {
                            logger.debug("-- tran cnt chkeck ok  --");
                            //원거래를 가져온다.
                            StringBuffer sb_orgtraninfoSql = new StringBuffer();

                            sb_orgtraninfoSql.append(" select  b.comp_nm, b.biz_no, b.corp_no, b.comp_ceo_nm, b.comp_tel1, b.tax_flag ,b.merch_nm,  b.app_chk_amt, b.addr_1, b.addr_2 ");
                            sb_orgtraninfoSql.append("         ,a.tran_seq,a.massagetype,a.card_num ");
                            sb_orgtraninfoSql.append("         ,a.app_dt ");
                            sb_orgtraninfoSql.append("         ,a.app_tm         ");
                            sb_orgtraninfoSql.append("         ,a.app_no,a.tot_amt,a.pg_seq,a.tid ");
                            sb_orgtraninfoSql.append("         ,a.pay_chn_cate,a.onfftid,a.onffmerch_no,a.wcc ");
                            sb_orgtraninfoSql.append("         ,a.expire_dt,a.card_cate,a.order_seq ");
                            sb_orgtraninfoSql.append("         ,a.tran_dt,a.tran_tm,a.tran_cate,a.free_inst_flag ");
                            sb_orgtraninfoSql.append("         ,a.tax_amt,a.svc_amt,a.currency ");
                            sb_orgtraninfoSql.append("         ,a.installment,a.iss_cd,a.iss_nm ");
                            sb_orgtraninfoSql.append("         ,a.app_iss_cd,a.app_iss_nm,a.acq_cd ");
                            sb_orgtraninfoSql.append("         ,a.acq_nm,a.app_acq_cd,a.app_acq_nm ");
                            sb_orgtraninfoSql.append("         ,a.merch_no,a.org_merch_no,a.result_cd,a.result_msg ");
                            sb_orgtraninfoSql.append("         ,a.tran_status,a.result_status ");
                            sb_orgtraninfoSql.append("         ,a.org_app_dd,a.org_app_no,a.cncl_reason ");
                            sb_orgtraninfoSql.append("         ,a.tran_step,a.cncl_dt,a.acq_dt,a.acq_result_cd ");
                            sb_orgtraninfoSql.append("         ,a.holdoff_dt,a.holdon_dt,a.pay_dt,a.pay_amt ");
                            sb_orgtraninfoSql.append("         ,a.commision,a.client_ip,a.user_type ");
                            sb_orgtraninfoSql.append("         ,a.memb_user_no,a.user_id,a.user_nm ");
                            sb_orgtraninfoSql.append("         ,a.user_mail,a.user_phone1,a.user_phone2 ");
                            sb_orgtraninfoSql.append("         ,a.user_addr,a.product_type,a.product_nm ");
                            sb_orgtraninfoSql.append("         ,a.filler,a.filler2,a.term_filler1,a.term_filler2 ");
                            sb_orgtraninfoSql.append("         ,a.term_filler3,a.term_filler4,a.term_filler5 ");
                            sb_orgtraninfoSql.append("         ,a.term_filler6,a.term_filler7,a.term_filler8 ");
                            sb_orgtraninfoSql.append("         ,a.term_filler9,a.term_filler10,a.trad_chk_flag ");
                            sb_orgtraninfoSql.append("         ,a.acc_chk_flag,a.ins_dt,a.mod_dt,a.ins_user,a.mod_user,a.onofftid_pay_amt,a.onofftid_commision,a.tb_nm, a.app_card_num	 ");
                            sb_orgtraninfoSql.append(" from VW_TRAN_CARDPG a ");
                            sb_orgtraninfoSql.append("          ,( ");
                            sb_orgtraninfoSql.append("               SELECT x1.comp_nm, x1.BIZ_NO, x1.CORP_NO, x1.COMP_CEO_NM, x1.COMP_TEL1, x1.addr_1, x1.addr_2, x1.TAX_FLAG , x.ONFFMERCH_NO, x.MERCH_NM,  x.APP_CHK_AMT ");
                            sb_orgtraninfoSql.append("                FROM  TB_ONFFMERCH_MST x, ");
                            sb_orgtraninfoSql.append("                          TB_COMPANY x1 ");
                            sb_orgtraninfoSql.append("                WHERE                  ");
                            sb_orgtraninfoSql.append("                          x.del_flag = 'N' ");
                            sb_orgtraninfoSql.append("                        AND x.SVC_STAT = '00' ");
                            sb_orgtraninfoSql.append("                        AND x1.del_flag = 'N' ");
                            sb_orgtraninfoSql.append("                        AND x1.use_flag = 'Y'                 ");
                            sb_orgtraninfoSql.append("                        AND  X.COMP_SEQ = x1.comp_seq ");
                            sb_orgtraninfoSql.append("          ) b ");
                            sb_orgtraninfoSql.append(" where ");
                            sb_orgtraninfoSql.append("      a.ONFFMERCH_NO = b.ONFFMERCH_NO ");
                            sb_orgtraninfoSql.append(" and       ");
                            sb_orgtraninfoSql.append("     a.pg_seq =? ");
                            sb_orgtraninfoSql.append(" and    a.massagetype='10' ");
                            sb_orgtraninfoSql.append(" and    a.result_status='00' ");

                            pstmt2 = conn.prepareStatement(sb_orgtraninfoSql.toString());
                            pstmt2.setString(1, tno);
                            rs2 = pstmt2.executeQuery();
                            
                            //원거래정보
                            //원거래정보가 있는 경우
                            if(rs2 != null && rs2.next())
                            {
                                logger.debug("-- org tran  exist  --");
                                logger.debug("-- strTpTranseq create  --");
                                //거래일련번호를 생성한다.
                                String strTpTranseq = null;
                                String sb_TpTranseqSql = "select concat(DATE_FORMAT(now(),'%Y%m%d%H%i%S'), LPAD(f_get_sequence('TRANSEQ'),8,'0'))";
                                pstmt = conn.prepareStatement(sb_TpTranseqSql);
                                rs = pstmt.executeQuery();
                                if(rs != null && rs.next())
                                {
                                    strTpTranseq = rs.getString(1);
                                }                

                                closeResultSet(rs);
                                closePstmt(pstmt);
                                logger.debug("strTpTranseq : "+strTpTranseq);
                                logger.debug("-- traninfo insert  --");
                                //취소정보를 insert한다.
                                StringBuffer sb_instraninfo = new StringBuffer();
                                sb_instraninfo.append(" insert ");
                                sb_instraninfo.append(" into TB_TRAN_CARDPG_"+can_time.substring(2, 6) + " ( ");
                                sb_instraninfo.append(" TRAN_SEQ			 ");
                                sb_instraninfo.append(" ,MASSAGETYPE		 ");
                                sb_instraninfo.append(" ,CARD_NUM			 ");
                                sb_instraninfo.append(" ,APP_DT				 ");
                                sb_instraninfo.append(" ,APP_TM				 ");
                                sb_instraninfo.append(" ,APP_NO				 ");
                                sb_instraninfo.append(" ,TOT_AMT			 ");	
                                sb_instraninfo.append(" ,PG_SEQ				 ");
                                sb_instraninfo.append(" ,PAY_CHN_CATE		 ");	
                                sb_instraninfo.append(" ,TID				 ");	
                                sb_instraninfo.append(" ,ONFFTID			 ");	
                                sb_instraninfo.append(" ,WCC				 ");	
                                sb_instraninfo.append(" ,EXPIRE_DT			 ");
                                sb_instraninfo.append(" ,CARD_CATE			 ");
                                sb_instraninfo.append(" ,ORDER_SEQ			 ");
                                sb_instraninfo.append(" ,TRAN_DT			 ");
                                sb_instraninfo.append(" ,TRAN_TM			 ");	
                                sb_instraninfo.append(" ,TRAN_CATE			 ");
                                sb_instraninfo.append(" ,FREE_INST_FLAG		 ");
                                sb_instraninfo.append(" ,TAX_AMT			 ");	
                                sb_instraninfo.append(" ,SVC_AMT			 ");	
                                sb_instraninfo.append(" ,CURRENCY			 ");
                                sb_instraninfo.append(" ,INSTALLMENT		 ");	
                                sb_instraninfo.append(" ,ISS_CD				 ");
                                sb_instraninfo.append(" ,ISS_NM				 ");
                                sb_instraninfo.append(" ,APP_ISS_CD			 ");
                                sb_instraninfo.append(" ,APP_ISS_NM			 ");
                                sb_instraninfo.append(" ,ACQ_CD				 ");
                                sb_instraninfo.append(" ,ACQ_NM			 ");
                                sb_instraninfo.append(" ,APP_ACQ_CD			 ");
                                sb_instraninfo.append(" ,APP_ACQ_NM			 ");
                                sb_instraninfo.append(" ,MERCH_NO			 ");
                                sb_instraninfo.append(" ,ORG_MERCH_NO		 ");
                                sb_instraninfo.append(" ,RESULT_CD			 ");
                                sb_instraninfo.append(" ,RESULT_MSG			 ");
                                sb_instraninfo.append(" ,ORG_APP_DD			 ");
                                sb_instraninfo.append(" ,ORG_APP_NO			 ");
                                sb_instraninfo.append(" ,CNCL_REASON		 ");	
                                sb_instraninfo.append(" ,TRAN_STATUS		 ");	
                                sb_instraninfo.append(" ,RESULT_STATUS		 ");
                                sb_instraninfo.append(" ,TRAN_STEP			 ");
                                sb_instraninfo.append(" ,CNCL_DT			 ");	
                                sb_instraninfo.append(" ,ACQ_DT				 ");
                                sb_instraninfo.append(" ,ACQ_RESULT_CD		 ");
                                sb_instraninfo.append(" ,HOLDOFF_DT			 ");
                                sb_instraninfo.append(" ,HOLDON_DT			 ");
                                sb_instraninfo.append(" ,PAY_DT				 ");
                                sb_instraninfo.append(" ,PAY_AMT			 ");	
                                sb_instraninfo.append(" ,COMMISION			 ");
                                sb_instraninfo.append(" ,ONOFFTID_PAY_AMT	 ");	
                                sb_instraninfo.append(" ,ONOFFTID_COMMISION	 ");
                                sb_instraninfo.append(" ,CLIENT_IP			 ");
                                sb_instraninfo.append(" ,USER_TYPE			 ");
                                sb_instraninfo.append(" ,MEMB_USER_NO		 ");
                                sb_instraninfo.append(" ,USER_ID				 ");
                                sb_instraninfo.append(" ,USER_NM			 ");
                                sb_instraninfo.append(" ,USER_MAIL			 ");
                                sb_instraninfo.append(" ,USER_PHONE1		 ");
                                sb_instraninfo.append(" ,USER_PHONE2		 ");
                                sb_instraninfo.append(" ,USER_ADDR			 ");
                                sb_instraninfo.append(" ,PRODUCT_TYPE		 ");
                                sb_instraninfo.append(" ,PRODUCT_NM		 ");
                                sb_instraninfo.append(" ,FILLER				 ");
                                sb_instraninfo.append(" ,FILLER2				 ");
                                sb_instraninfo.append(" ,TERM_FILLER1		 ");
                                sb_instraninfo.append(" ,TERM_FILLER2		 ");	
                                sb_instraninfo.append(" ,TERM_FILLER3		 ");	
                                sb_instraninfo.append(" ,TERM_FILLER4		 ");	
                                sb_instraninfo.append(" ,TERM_FILLER5		 ");	
                                sb_instraninfo.append(" ,TERM_FILLER6		 ");	
                                sb_instraninfo.append(" ,TERM_FILLER7		 ");	
                                sb_instraninfo.append(" ,TERM_FILLER8		 ");	
                                sb_instraninfo.append(" ,TERM_FILLER9		 ");	
                                sb_instraninfo.append(" ,TERM_FILLER10		 ");
                                sb_instraninfo.append(" ,TRAD_CHK_FLAG		 ");
                                sb_instraninfo.append(" ,ACC_CHK_FLAG		 ");
                                sb_instraninfo.append(" ,INS_DT				 ");
                                sb_instraninfo.append(" ,MOD_DT				 ");
                                sb_instraninfo.append(" ,INS_USER			 ");	
                                sb_instraninfo.append(" ,MOD_USER			 ");
                                sb_instraninfo.append(" ,ORG_PG_SEQ			 ");
                                sb_instraninfo.append(" ,ONFFMERCH_NO	 ");
                                sb_instraninfo.append(" ,auth_pw ");
                                sb_instraninfo.append(" ,auth_value ");
                                sb_instraninfo.append(" ,app_card_num ");
                                sb_instraninfo.append(" )  ");
                                sb_instraninfo.append(" values ( ");
                                sb_instraninfo.append("  ? ");//tran_seq			
                                sb_instraninfo.append(" ,? ");//massagetype			
                                sb_instraninfo.append(" ,? ");//card_num			
                                sb_instraninfo.append(" ,? ");//app_dt			
                                sb_instraninfo.append(" ,? ");//app_tm			
                                sb_instraninfo.append(" ,? ");//app_no			
                                sb_instraninfo.append(" ,? ");//tot_amt			
                                sb_instraninfo.append(" ,? ");//pg_seq			
                                sb_instraninfo.append(" ,? ");//pay_chn_cate		
                                sb_instraninfo.append(" ,? ");//tid				
                                sb_instraninfo.append(" ,? ");//onfftid			
                                sb_instraninfo.append(" ,? ");//wcc				
                                sb_instraninfo.append(" ,? ");//expire_dt			
                                sb_instraninfo.append(" ,? ");//card_cate			
                                sb_instraninfo.append(" ,? ");//order_seq			
                                sb_instraninfo.append(" ,? ");//tran_dt			
                                sb_instraninfo.append(" ,? ");//tran_tm			
                                sb_instraninfo.append(" ,? ");//tran_cate			
                                sb_instraninfo.append(" ,? ");//free_inst_flag		
                                sb_instraninfo.append(" ,? ");//tax_amt			
                                sb_instraninfo.append(" ,? ");//svc_amt			
                                sb_instraninfo.append(" ,? ");//currency			
                                sb_instraninfo.append(" ,? ");//installment			
                                sb_instraninfo.append(" ,? ");//iss_cd			
                                sb_instraninfo.append(" ,? ");//iss_nm			
                                sb_instraninfo.append(" ,? ");//app_iss_cd			
                                sb_instraninfo.append(" ,? ");//app_iss_nm			
                                sb_instraninfo.append(" ,? ");//acq_cd			
                                sb_instraninfo.append(" ,? ");//acq_nm			
                                sb_instraninfo.append(" ,? ");//app_acq_cd			
                                sb_instraninfo.append(" ,? ");//app_acq_nm			
                                sb_instraninfo.append(" ,? ");//merch_no			
                                sb_instraninfo.append(" ,? ");//org_merch_no		
                                sb_instraninfo.append(" ,? ");//result_cd			
                                sb_instraninfo.append(" ,? ");//result_msg			
                                sb_instraninfo.append(" ,? ");//org_app_dd			
                                sb_instraninfo.append(" ,? ");//org_app_no			
                                sb_instraninfo.append(" ,? ");//cncl_reason			
                                sb_instraninfo.append(" ,? ");//tran_status			
                                sb_instraninfo.append(" ,? ");//result_status		
                                sb_instraninfo.append(" ,? ");//tran_step			
                                sb_instraninfo.append(" ,? ");//cncl_dt			
                                sb_instraninfo.append(" ,? ");//acq_dt			
                                sb_instraninfo.append(" ,? ");//acq_result_cd
                                sb_instraninfo.append(" ,? ");//holdoff_dt
                                sb_instraninfo.append(" ,? ");//holdon_dt
                                sb_instraninfo.append(" ,? ");//pay_dt
                                sb_instraninfo.append(" ,? ");//pay_amt
                                sb_instraninfo.append(" ,? ");//commision
                                sb_instraninfo.append(" ,? ");//onofftid_pay_amt
                                sb_instraninfo.append(" ,? ");//onofftid_commision
                                sb_instraninfo.append(" ,? ");//client_ip
                                sb_instraninfo.append(" ,? ");//user_type
                                sb_instraninfo.append(" ,? ");//memb_user_no
                                sb_instraninfo.append(" ,? ");//user_id
                                sb_instraninfo.append(" ,? ");//user_nm
                                sb_instraninfo.append(" ,? ");//user_mail
                                sb_instraninfo.append(" ,? ");//user_phone1
                                sb_instraninfo.append(" ,? ");//user_phone2
                                sb_instraninfo.append(" ,? ");//user_addr
                                sb_instraninfo.append(" ,? ");//product_type
                                sb_instraninfo.append(" ,? ");//product_nm
                                sb_instraninfo.append(" ,? ");//filler
                                sb_instraninfo.append(" ,? ");//filler2
                                sb_instraninfo.append(" ,? ");//term_filler1
                                sb_instraninfo.append(" ,? ");//term_filler2
                                sb_instraninfo.append(" ,? ");//term_filler3
                                sb_instraninfo.append(" ,? ");//term_filler4
                                sb_instraninfo.append(" ,? ");//term_filler5
                                sb_instraninfo.append(" ,? ");//term_filler6
                                sb_instraninfo.append(" ,? ");//term_filler7
                                sb_instraninfo.append(" ,? ");//term_filler8
                                sb_instraninfo.append(" ,? ");//term_filler9
                                sb_instraninfo.append(" ,? ");//term_filler10
                                sb_instraninfo.append(" ,? ");//trad_chk_flag
                                sb_instraninfo.append(" ,? ");//acc_chk_flag
                                sb_instraninfo.append("  , DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                                sb_instraninfo.append("  , DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                                sb_instraninfo.append(" ,? ");//ins_user			
                                sb_instraninfo.append(" ,? ");//mod_user			
                                sb_instraninfo.append(" ,? ");//org_pg_seq			
                                sb_instraninfo.append(" ,? ");//onffmerch_no		
                                sb_instraninfo.append(" ,? ");//auth_pw		  
                                sb_instraninfo.append(" ,? ");//auth_value		  
                                sb_instraninfo.append(" ,? ");//app_card_num	
                                sb_instraninfo.append(" ) ");          
                                
                                //System.out.println("sb_instraninfo.toString() : " + sb_instraninfo.toString());

                                pstmt = conn.prepareStatement(sb_instraninfo.toString());

                                //당일취소,익일취소 구분
                                String strTpOrgAppDt = rs2.getString("app_dt");
                                String strTpCnclAppDt = can_time.substring(0, 8);

                                //당일취소
                                if(strTpCnclAppDt.equals(strTpOrgAppDt))
                                {                            
                                    pstmt.setString(4,can_time.substring(0, 8));//app_dt				, jdbcType=VARCHAR}
                                    pstmt.setString(5,can_time.substring(8, 14));//app_tm				 , jdbcType=VARCHAR}

                                    pstmt.setString(39,"10");//tran_status 당일취소 셋팅                            
                                    pstmt.setString(76,"3");//acc_chk_flag 매입비대상 셋팅
                                }
                                //익일취소
                                else
                                {
                                    pstmt.setString(4,can_time.substring(0, 8));//app_dt				, jdbcType=VARCHAR}
                                    pstmt.setString(5,can_time.substring(8, 14));//app_tm				 , jdbcType=VARCHAR}

                                    pstmt.setString(39,"20");//tran_status 익일취소(매입취소) 셋팅
                                    pstmt.setString(76,"0");//acc_chk_flag 매입비대상 셋팅
                                }
                                pstmt.setString(1,strTpTranseq);//tran_seq				, jdbcType=VARCHAR} 
                                pstmt.setString(2,"40");//massagetype			, jdbcType=VARCHAR} 
                                pstmt.setString(3,rs2.getString("card_num"));//card_num			, jdbcType=VARCHAR} 

                                pstmt.setString(6,null);//app_no				, jdbcType=VARCHAR}
                                pstmt.setString(7,rs2.getString("tot_amt"));//tot_amt				 , jdbcType=INTEGER }
                                pstmt.setString(8,tno);//pg_seq				, jdbcType=VARCHAR}
                                pstmt.setString(9,rs2.getString("pay_chn_cate"));//pay_chn_cate			 , jdbcType=VARCHAR}
                                pstmt.setString(10,site_cd);//tid					, jdbcType=VARCHAR}
                                pstmt.setString(11,rs2.getString("onfftid"));//onfftid				, jdbcType=VARCHAR}
                                pstmt.setString(12,rs2.getString("wcc"));//wcc					, jdbcType=VARCHAR}
                                pstmt.setString(13,rs2.getString("expire_dt"));//expire_dt			, jdbcType=VARCHAR}
                                pstmt.setString(14,rs2.getString("card_cate"));//card_cate			  , jdbcType=VARCHAR}
                                pstmt.setString(15,ordr_idxx);//order_seq			  , jdbcType=VARCHAR}
                                pstmt.setString(16,can_time.substring(0, 8));//tran_dt				, jdbcType=VARCHAR}
                                pstmt.setString(17,can_time.substring(8, 14));//tran_tm				, jdbcType=VARCHAR}
                                pstmt.setString(18,"40");//tran_cate			  , jdbcType=VARCHAR}
                                pstmt.setString(19,rs2.getString("free_inst_flag"));//free_inst_flag		    , jdbcType=VARCHAR}
                                pstmt.setDouble(20,rs2.getDouble("tax_amt"));//tax_amt				, jdbcType=INTEGER }
                                pstmt.setDouble(21,rs2.getDouble("svc_amt"));//svc_amt				, jdbcType=INTEGER }
                                pstmt.setString(22,rs2.getString("currency"));//currency			 , jdbcType=VARCHAR}
                                pstmt.setString(23,rs2.getString("installment"));//installment			   , jdbcType=VARCHAR}
                                /*
                                pstmt.setString(24,rs2.getString("iss_cd"));//iss_cd				, jdbcType=VARCHAR}
                                pstmt.setString(25,rs2.getString("iss_nm"));//iss_nm				 , jdbcType=VARCHAR}
                                pstmt.setString(26,rs2.getString("app_iss_cd"));//app_iss_cd			 , jdbcType=VARCHAR}
                                pstmt.setString(27,rs2.getString("app_iss_nm"));//app_iss_nm			 , jdbcType=VARCHAR}
                                pstmt.setString(28,rs2.getString("acq_cd"));//acq_cd				, jdbcType=VARCHAR}
                                pstmt.setString(29,rs2.getString("acq_nm"));//acq_nm				, jdbcType=VARCHAR}
                                pstmt.setString(30,rs2.getString("app_acq_cd"));//app_acq_cd			  , jdbcType=VARCHAR}
                                pstmt.setString(31,rs2.getString("app_acq_nm"));//app_acq_nm			   , jdbcType=VARCHAR}
                                */
                                pstmt.setString(24,null);//iss_cd				, jdbcType=VARCHAR}
                                pstmt.setString(25,null);//iss_nm				 , jdbcType=VARCHAR}
                                pstmt.setString(26,null);//app_iss_cd			 , jdbcType=VARCHAR}
                                pstmt.setString(27,null);//app_iss_nm			 , jdbcType=VARCHAR}
                                pstmt.setString(28,null);//acq_cd				, jdbcType=VARCHAR}
                                pstmt.setString(29,null);//acq_nm				, jdbcType=VARCHAR}
                                pstmt.setString(30,null);//app_acq_cd			  , jdbcType=VARCHAR}
                                pstmt.setString(31,null);//app_acq_nm			   , jdbcType=VARCHAR}
                                
                                pstmt.setString(32,rs2.getString("merch_no"));//merch_no			  , jdbcType=VARCHAR}
                                pstmt.setString(33,rs2.getString("org_merch_no"));//org_merch_no			  , jdbcType=VARCHAR}
                                pstmt.setString(34,"0000");//result_cd			 , jdbcType=VARCHAR}
                                pstmt.setString(35,"성공(noti수신)");//result_msg			   , jdbcType=VARCHAR}
                                pstmt.setString(36,rs2.getString("app_dt"));//org_app_dd			  , jdbcType=VARCHAR}
                                pstmt.setString(37,rs2.getString("app_no"));//org_app_no			   , jdbcType=VARCHAR}
                                pstmt.setString(38,"noti취소통지");//cncl_reason			, jdbcType=VARCHAR}
                                pstmt.setString(40,"00");//result_status		     , jdbcType=VARCHAR}
                                pstmt.setString(41,"00");//tran_step			 , jdbcType=VARCHAR}
                                pstmt.setString(42,can_time.substring(0, 8));//cncl_dt				, jdbcType=VARCHAR}
                                pstmt.setString(43,null);//acq_dt				, jdbcType=VARCHAR}
                                pstmt.setString(44,null);//acq_result_cd		     , jdbcType=VARCHAR}
                                pstmt.setString(45,null);//holdoff_dt			  , jdbcType=VARCHAR}
                                pstmt.setString(46,null);//holdon_dt			  , jdbcType=VARCHAR}
                                pstmt.setString(47,null);//pay_dt				, jdbcType=VARCHAR}
                                pstmt.setString(48,null);//pay_amt				, jdbcType=INTEGER }
                                pstmt.setString(49,null);//commision			  , jdbcType=INTEGER }
                                pstmt.setString(50,rs2.getString("onofftid_pay_amt"));//onofftid_pay_amt		, jdbcType=INTEGER }
                                pstmt.setString(51,rs2.getString("onofftid_commision"));//onofftid_commision		, jdbcType=INTEGER }
                                pstmt.setString(52,null);//client_ip				, jdbcType=VARCHAR}
                                pstmt.setString(53,rs2.getString("user_type"));//user_type			, jdbcType=VARCHAR}
                                pstmt.setString(54,rs2.getString("memb_user_no"));//memb_user_no		, jdbcType=VARCHAR}
                                pstmt.setString(55,rs2.getString("user_id"));//user_id				, jdbcType=VARCHAR}
                                pstmt.setString(56,rs2.getString("user_nm"));//user_nm				, jdbcType=VARCHAR}
                                pstmt.setString(57,rs2.getString("user_mail"));//user_mail			    , jdbcType=VARCHAR}
                                pstmt.setString(58,rs2.getString("user_phone1"));//user_phone1			 , jdbcType=VARCHAR}
                                pstmt.setString(59,rs2.getString("user_phone2"));//user_phone2			 , jdbcType=VARCHAR}
                                pstmt.setString(60,rs2.getString("user_addr"));//user_addr			, jdbcType=VARCHAR}
                                pstmt.setString(61,rs2.getString("product_type"));//product_type		  , jdbcType=VARCHAR}
                                pstmt.setString(62,rs2.getString("product_nm"));//product_nm			 , jdbcType=VARCHAR}
                                pstmt.setString(63,null);//filler				   , jdbcType=VARCHAR}
                                pstmt.setString(64,null);//filler2				   , jdbcType=VARCHAR}
                                pstmt.setString(65,null);//term_filler1			, jdbcType=VARCHAR}
                                pstmt.setString(66,null);//term_filler2			, jdbcType=VARCHAR}
                                pstmt.setString(67,null);//term_filler3			, jdbcType=VARCHAR}
                                pstmt.setString(68,null);//term_filler4			, jdbcType=VARCHAR}
                                pstmt.setString(69,null);//term_filler5			, jdbcType=VARCHAR}
                                pstmt.setString(70,null);//term_filler6				, jdbcType=VARCHAR}
                                pstmt.setString(71,null);//term_filler7			  , jdbcType=VARCHAR}
                                pstmt.setString(72,null);//term_filler8			, jdbcType=VARCHAR}
                                pstmt.setString(73,null);//term_filler9			, jdbcType=VARCHAR}
                                pstmt.setString(74,null);//term_filler10			, jdbcType=VARCHAR}
                                pstmt.setString(75,"0");//trad_chk_flag		  , jdbcType=VARCHAR}
                                pstmt.setString(77,"0");//ins_user				     , jdbcType=VARCHAR}
                                pstmt.setString(78,"0");//mod_user			, jdbcType=VARCHAR}
                                pstmt.setString(79,rs2.getString("pg_seq"));//org_pg_seq			, jdbcType=VARCHAR}
                                pstmt.setString(80,rs2.getString("onffmerch_no"));//onffmerch_no		  , jdbcType=VARCHAR}
                                pstmt.setString(81,null);//auth_pw		  , jdbcType=VARCHAR}
                                pstmt.setString(82,null);//auth_value		  , jdbcType=VARCHAR}   
                                pstmt.setString(83,rs2.getString("app_card_num"));//app_card_num		  , jdbcType=VARCHAR}                
                                int int_insResult = pstmt.executeUpdate();                               

                                closeResultSet(rs);
                                closePstmt(pstmt);
                                logger.debug("--org traninfo update  --");
                                //원거래를 update한다.
                                StringBuffer sb_orgtran_mod_sql = new StringBuffer();
                                sb_orgtran_mod_sql.append(" update " + rs2.getString("tb_nm") + " ");
                                sb_orgtran_mod_sql.append(" set      cncl_reason = ? ");
                                sb_orgtran_mod_sql.append("              ,tran_status= ? ");
                                sb_orgtran_mod_sql.append("              ,cncl_dt = ? ");
                                sb_orgtran_mod_sql.append("              ,acc_chk_flag = ? ");
                                sb_orgtran_mod_sql.append("               ,mod_dt = DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                                sb_orgtran_mod_sql.append("               ,mod_user= '0' ");
                                sb_orgtran_mod_sql.append("          where 1=1 ");
                                sb_orgtran_mod_sql.append("            and tran_seq = ? ");

                                pstmt = conn.prepareStatement(sb_orgtran_mod_sql.toString());

                                pstmt.setString(1, "noti취소통지");

                                //당일취소
                                if(strTpCnclAppDt.equals(strTpOrgAppDt))
                                {
                                   pstmt.setString(2, "10");//당일취소
                                   pstmt.setString(4, "3");//매입비대상   
                                }
                                else
                                {
                                    pstmt.setString(2, "20");//매입취소
                                    pstmt.setString(4, rs2.getString("acc_chk_flag"));
                                }                        
                                pstmt.setString(3, can_time.substring(0, 8));                        
                                pstmt.setString(5, rs2.getString("tran_seq"));

                                int int_mod_orginfo = pstmt.executeUpdate();  

                                closeResultSet(rs);
                                closePstmt(pstmt);
                                
                                strNotResult = "10";

                            } 
                            //원거래정보가 없는 경우
                            else
                            {
                                logger.debug("--org traninfo not exist  --");
                                //원거래가 없어 insert안함.
                                strNotResult = "30";
                            }
                        }
                        //이미 취소거래가 있는경우
                        else
                        {
                            logger.debug("-- cncl traninfo exist  --");
                            strNotResult = "20";
                        }
                        //noti처리상태 update
                        StringBuffer sb_noti_mod_sql = new StringBuffer();
                        sb_noti_mod_sql.append(" UPDATE TB_KCP_NOTI SET ");
                        sb_noti_mod_sql.append(" proc_flag=? ");
                        sb_noti_mod_sql.append(" ,mod_dt = DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                        sb_noti_mod_sql.append(" ,mod_user= '0' ");                
                        sb_noti_mod_sql.append(" WHERE noti_seq = ? ");
                        pstmt = conn.prepareStatement(sb_noti_mod_sql.toString());
                        pstmt.setString(1, strNotResult);
                        pstmt.setString(2, strTpNotiseq);
                        int int_mod_notiinfo = pstmt.executeUpdate();
                    }
                }
            }
            result_msg =  RESULT_SUCCESS;
            conn.commit();

          } catch ( SQLException e ) {
            e.printStackTrace();
			if(conn !=null)
			{
				conn.rollback();
			}
            logger.error(e);
            
            result_msg = RESULT_FAIL;

          } catch ( Exception e ) {
            e.printStackTrace();
			if(conn !=null)
			{
				conn.rollback();
			}
            logger.error(e);
            result_msg = RESULT_FAIL;
          } 
          finally 
          {
              closeResultSet(rs);
              closeResultSet(rs2);
              closePstmt(pstmt);
              closePstmt(pstmt2);
              closeConnection(conn);
          }
    
    /* ============================================================================== */
    /* =   04. result 값 세팅 하기                                                  = */
    /* ============================================================================== */
%>
<html><body><form><input type="hidden" name="result" value="<%=result_msg%>"></form></body></html>