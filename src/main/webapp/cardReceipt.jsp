<%-- 
    Document   : cardReceipt.jsp
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="javax.sql.*" %>
<%@ page import="javax.naming.*"%>
<%@ page import="org.apache.log4j.Logger"%>
<%@ page import="com.onoffkorea.system.app.Vo.KiccPgVo"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%!
    /**
     * 파라미터 체크 메소드
     */
    public String getNullToSpace(String param) {
        return (param == null) ? "" : param.trim();
    }

    public String getNullToZero(String param) {
        return (param == null) ? "0" : param.trim();
    }
        
    
    public void closeResultSet(ResultSet rs)
    {
        try
        {
            if ( rs != null ) rs.close();
        }
        catch(Exception ex)
        {
        }
    }
    
    public void closePstmt(PreparedStatement psmt)
    {
        try
        {
            if ( psmt != null ) psmt.close();
        }
        catch(Exception ex)
        {
        }
    }    
    
    public void closeConnection(Connection con)
    {
        try
        {
            if ( con != null ) con.close();
        }
        catch(Exception ex)
        {
        }
    }       
%>
<%
    final Logger logger = Logger.getLogger("cardReceipt.jsp");

    Connection conn = null;
    PreparedStatement pstmt = null;
    PreparedStatement pstmt2 = null;
    ResultSet rs = null;
    ResultSet rs2 = null;
    
    String strTpTranSeq = getNullToSpace(request.getParameter("tran_seq"));
    String strTpOnffTid = getNullToSpace(request.getParameter("onofftid"));
    
    System.out.println("strTpTranSeq : " + strTpTranSeq);
    System.out.println("strTpOnffTid : " + strTpOnffTid);
        
    String strbill_tran_seq=null;
    String strbill_iss_nm=null;
    String strbill_installment=null;
    String strbill_ViewCardNo=null;
    String strbill_massagetype=null;
    String strbill_app_dt=null;
    String strbill_app_tm=null;
    String strbill_org_app_dd=null;
    String strbill_cncl_dt=null;
    String strbill_product_nm=null;
    String strbill_ViewAmt=null;
    String strbill_ViewSvcAmt=null;
    String strbill_ViewTaxAmt=null;
    String strbill_ViewTotAmt=null;
    String strbill_user_nm=null;
    String strbill_app_no=null;
    String strbill_org_app_no=null;
    String strbill_comp_nm=null;
    String strbill_biz_no=null;
    String strbill_comp_ceo_nm=null;
    String strbill_comp_tel1=null;
    String strbill_addr_1=null;
    String strbill_addr_2=null;    
    
    String onffCompInfo_comp_nm = null;
    String onffCompInfo_ViewOnffBizNo = null;
    String onffCompInfo_comp_ceo_nm = null;
    String onffCompInfo_comp_tel1 = null;
    String onffCompInfo_addr_1 = null;
    String onffCompInfo_addr_2 = null;

    try 
    {   
        //DB Connection을 가져온다.
        Context initContext = new InitialContext();                                            //2
        DataSource ds = (DataSource) initContext.lookup("java:comp/env/jdbc/okpay");     //3
        conn = ds.getConnection();                                                             //4
        conn.setAutoCommit(false);
        
        StringBuffer sb_traninfo_sql = new StringBuffer();
        sb_traninfo_sql.append(" select  b.comp_nm, b.biz_no, b.corp_no, b.comp_ceo_nm, b.comp_tel1, b.tax_flag ,b.merch_nm,  b.app_chk_amt, b.addr_1, b.addr_2 ");
        sb_traninfo_sql.append("        ,a.tran_seq,a.massagetype,a.card_num ");
        sb_traninfo_sql.append("        ,a.app_dt ");
        sb_traninfo_sql.append("        ,a.app_tm        ");
        sb_traninfo_sql.append("        ,a.app_no,a.tot_amt,a.pg_seq,a.tid ");
        sb_traninfo_sql.append("        ,a.pay_chn_cate,a.onfftid,a.onffmerch_no,a.wcc ");
        sb_traninfo_sql.append("        ,a.expire_dt,a.card_cate,a.order_seq ");
        sb_traninfo_sql.append("        ,a.tran_dt,a.tran_tm,a.tran_cate,a.free_inst_flag ");
        sb_traninfo_sql.append("        ,a.tax_amt,a.svc_amt,a.currency ");
        sb_traninfo_sql.append("        ,a.installment,a.iss_cd,a.iss_nm ");
        sb_traninfo_sql.append("        ,a.app_iss_cd,a.app_iss_nm,a.acq_cd ");
        sb_traninfo_sql.append("        ,a.acq_nm,a.app_acq_cd,a.app_acq_nm ");
        sb_traninfo_sql.append("        ,a.merch_no,a.org_merch_no,a.result_cd,a.result_msg ");
        sb_traninfo_sql.append("        ,a.tran_status,a.result_status ");
        sb_traninfo_sql.append("        ,a.org_app_dd,a.org_app_no,a.cncl_reason ");
        sb_traninfo_sql.append("        ,a.tran_step,a.cncl_dt,a.acq_dt,a.acq_result_cd ");
        sb_traninfo_sql.append("        ,a.holdoff_dt,a.holdon_dt,a.pay_dt,a.pay_amt ");
        sb_traninfo_sql.append("        ,a.commision,a.client_ip,a.user_type ");
        sb_traninfo_sql.append("        ,a.memb_user_no,a.user_id,a.user_nm ");
        sb_traninfo_sql.append("        ,a.user_mail,a.user_phone1,a.user_phone2 ");
        sb_traninfo_sql.append("        ,a.user_addr,a.product_type,a.product_nm ");
        sb_traninfo_sql.append("        ,a.filler,a.filler2,a.term_filler1,a.term_filler2 ");
        sb_traninfo_sql.append("        ,a.term_filler3,a.term_filler4,a.term_filler5 ");
        sb_traninfo_sql.append("        ,a.term_filler6,a.term_filler7,a.term_filler8 ");
        sb_traninfo_sql.append("        ,a.term_filler9,a.term_filler10,a.trad_chk_flag ");
        sb_traninfo_sql.append("        ,a.acc_chk_flag,a.ins_dt,a.mod_dt,a.ins_user,a.mod_user,a.onofftid_pay_amt,a.onofftid_commision,a.tb_nm, a.app_card_num	");
        sb_traninfo_sql.append(" from VW_TRAN_CARDPG a ");
        sb_traninfo_sql.append("         ,( ");
        sb_traninfo_sql.append("              SELECT x1.comp_nm, x1.BIZ_NO, x1.CORP_NO, x1.COMP_CEO_NM, x1.COMP_TEL1, x1.addr_1, x1.addr_2, x1.TAX_FLAG , x.ONFFMERCH_NO, x.MERCH_NM,  x.APP_CHK_AMT");
        sb_traninfo_sql.append("               FROM  TB_ONFFMERCH_MST x, ");
        sb_traninfo_sql.append("                         TB_COMPANY x1 ");
        sb_traninfo_sql.append("               WHERE                 ");
        sb_traninfo_sql.append("                         x.del_flag = 'N' ");
        sb_traninfo_sql.append("                       AND x.SVC_STAT = '00' ");
        sb_traninfo_sql.append("                       AND x1.del_flag = 'N' ");
        sb_traninfo_sql.append("                       AND x1.use_flag = 'Y'                ");
        sb_traninfo_sql.append("                       AND  X.COMP_SEQ = x1.comp_seq ");
        sb_traninfo_sql.append("         ) b");
        sb_traninfo_sql.append(" where");
        sb_traninfo_sql.append("     a.ONFFMERCH_NO = b.ONFFMERCH_NO ");
        sb_traninfo_sql.append(" and      ");
        sb_traninfo_sql.append("    a.tran_seq = ? ");
        sb_traninfo_sql.append(" and      ");
        sb_traninfo_sql.append("    a.onfftid = ? ");
        
        System.out.println(sb_traninfo_sql.toString());
            
        pstmt = conn.prepareStatement(sb_traninfo_sql.toString());
        pstmt.setString(1		,strTpTranSeq); //noti_seq		
        pstmt.setString(2		,strTpOnffTid		); //res_cd
        
        rs = pstmt.executeQuery();
        
        System.out.println("step_1");
     
        rs.next();
        
        System.out.println("step_2");
        
        strbill_tran_seq = getNullToSpace(rs.getString("tran_seq"));
        
        strbill_iss_nm = getNullToSpace(rs.getString("iss_nm"));
        strbill_installment = getNullToSpace(rs.getString("installment"));
        
        String strTpCardNo = getNullToSpace(rs.getString("card_num"));
        
        strbill_ViewCardNo = strTpCardNo.substring(0, 8) + "****" + strTpCardNo.substring(12 , strTpCardNo.length()); 
        
        strbill_massagetype = getNullToSpace(rs.getString("massagetype"));
        strbill_app_dt = getNullToSpace(rs.getString("app_dt"));
        strbill_app_tm = getNullToSpace(rs.getString("app_tm"));
        strbill_org_app_dd = getNullToSpace(rs.getString("org_app_dd"));
        strbill_cncl_dt = getNullToSpace(rs.getString("cncl_dt"));
        strbill_product_nm = getNullToSpace(rs.getString("product_nm"));

        NumberFormat nf = NumberFormat.getInstance();
             
        String strTaxAmt = com.onoffkorea.system.common.util.Util.nullToInt(rs.getString("tax_amt"));
        String strTotAmt = com.onoffkorea.system.common.util.Util.nullToInt(rs.getString("tot_amt"));
        String strSvcAmt = com.onoffkorea.system.common.util.Util.nullToInt(rs.getString("svc_amt"));
        
        strbill_ViewAmt = nf.format( Integer.parseInt(strTotAmt) - Integer.parseInt(strSvcAmt) - Integer.parseInt(strTaxAmt));//과세승인금액
        strbill_ViewTaxAmt = nf.format( Integer.parseInt(strTaxAmt) );//부가세
        strbill_ViewTotAmt = nf.format( Integer.parseInt(strTotAmt) );//총금액
        strbill_ViewSvcAmt = nf.format( Integer.parseInt(strSvcAmt) );//비과세금액
        
        strbill_user_nm = getNullToSpace(rs.getString("user_nm"));
        strbill_app_no = getNullToSpace(rs.getString("app_no"));
        strbill_org_app_no = getNullToSpace(rs.getString("org_app_no"));
        strbill_comp_nm = getNullToSpace(rs.getString("comp_nm"));
        strbill_biz_no = getNullToSpace(rs.getString("biz_no"));
        strbill_comp_ceo_nm = getNullToSpace(rs.getString("comp_ceo_nm"));
        strbill_comp_tel1 = getNullToSpace(rs.getString("comp_tel1"));
        strbill_addr_1 = getNullToSpace(rs.getString("addr_1"));
        strbill_addr_2 = getNullToSpace(rs.getString("addr_2"));
        
        System.out.println("step_3");
        
        StringBuffer sb_compinfo_sql = new StringBuffer();
        
        sb_compinfo_sql.append(" select a.comp_seq    ");
        sb_compinfo_sql.append("                             , a.comp_nm     ");
        sb_compinfo_sql.append("                             , a.biz_no      ");
        sb_compinfo_sql.append("                             , a.corp_no  ");
        sb_compinfo_sql.append("                             , a.comp_ceo_nm ");
        sb_compinfo_sql.append("                             , a.comp_tel1   ");
        sb_compinfo_sql.append("                             , a.comp_tel2 ");
        sb_compinfo_sql.append("                             , a.biz_cate    ");
        sb_compinfo_sql.append("                             , a.biz_type    ");
        sb_compinfo_sql.append("                             , a.tax_flag    ");
        sb_compinfo_sql.append("                             , d.code_nm tax_flag_nm                   ");
        sb_compinfo_sql.append("                             , a.use_flag    ");
        sb_compinfo_sql.append("                             , c.code_nm use_flag_nm    ");
        sb_compinfo_sql.append("                             , DATE_FORMAT(STR_TO_DATE(a.ins_dt, '%Y%m%d%H%i%S'), '%Y-%m-%d') ins_dt      ");
        sb_compinfo_sql.append("                             , DATE_FORMAT(STR_TO_DATE(a.mod_dt, '%Y%m%d%H%i%S'), '%Y-%m-%d') mod_dt      ");
        sb_compinfo_sql.append("                             , a.ins_user    ");
        sb_compinfo_sql.append("                             , a.mod_user    ");
        sb_compinfo_sql.append("                             , a.comp_cate   ");
        sb_compinfo_sql.append("                             , b.code_nm cate_comp_nm ");
        sb_compinfo_sql.append("                             , a.zip_cd      ");
        sb_compinfo_sql.append("                             , a.addr_1      ");
        sb_compinfo_sql.append("                             , a.addr_2 ");
        sb_compinfo_sql.append("                             , a.memo ");
        sb_compinfo_sql.append("                             , a.del_flag ");
        sb_compinfo_sql.append("                             , a.comp_email ");
        sb_compinfo_sql.append("        from tb_company a left outer join ");
        sb_compinfo_sql.append("             (select detail_code, code_nm from TB_CODE_DETAIL where main_code='COMP_CATE' and USE_flag='Y' and del_flag='N') b on 1=1 and a.comp_cate = b.detail_code left outer join ");
        sb_compinfo_sql.append("             (select detail_code, code_nm from TB_CODE_DETAIL where main_code='USE_FLAG' and USE_flag='Y' and del_flag='N') c on 1=1 and a.use_flag = c.detail_code left outer join ");
        sb_compinfo_sql.append("             (select detail_code, code_nm from TB_CODE_DETAIL where main_code='TAX_FLAG' and USE_flag='Y' and del_flag='N') d on 1=1 and a.tax_flag = d.detail_code ");
        sb_compinfo_sql.append("        where 1=1 ");
        sb_compinfo_sql.append("            and a.del_flag = 'N' ");
        sb_compinfo_sql.append("            and comp_seq = '2015010100000001' ");        
              
        System.out.println(sb_compinfo_sql.toString());
        
        System.out.println("step_4");
        
        pstmt2 = conn.prepareStatement(sb_compinfo_sql.toString());
        
        System.out.println("step_5");
        
        rs2 = pstmt2.executeQuery();
        
        System.out.println("step_6");
        
        rs2.next();
        
        System.out.println("step_7");
        
        onffCompInfo_comp_nm = getNullToSpace(rs2.getString("comp_nm"));
        
        String strTpOnffBizNo = getNullToSpace(rs2.getString("biz_no"));
        onffCompInfo_ViewOnffBizNo = strTpOnffBizNo.substring(0, 6) +  "****" ;
        
        onffCompInfo_comp_ceo_nm = getNullToSpace(rs2.getString("comp_ceo_nm"));
        onffCompInfo_comp_tel1 = getNullToSpace(rs2.getString("comp_tel1"));
        onffCompInfo_addr_1 = getNullToSpace(rs2.getString("addr_1"));
        onffCompInfo_addr_2 = getNullToSpace(rs2.getString("addr_2"));
        
        System.out.println("step_8");
    } 
    catch ( SQLException exsql ) 
    {
        exsql.printStackTrace();
        logger.error(exsql);
    } 
    catch ( Exception e ) 
    {
        logger.error(e);
    } 
    finally 
    {
       closeResultSet(rs);
       closeResultSet(rs2);
       closePstmt(pstmt);
       closePstmt(pstmt2);
       closeConnection(conn);
    }             
%>
<html>
<head>
<title>okpay 카드영수증</title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/main/settlementPaymentCss.css" />"  />
<script type="text/javascript" src="<c:url value="/resources/javascript/jquery-1.6.2.min.js" />"></script>
<script type="text/javascript">
    window.resizeTo(400,720);
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%
    if(strbill_tran_seq != null && !strbill_tran_seq.equals(""))
    {
%>
<form name="frm_card_reuslt_pop" id="frm_card_reuslt_pop" method="post" >
<div id="divMsg" ondragstart="return false" onselectstart="return false" oncontextmenu="return false">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td height="5"></td>
	</tr>
	<tr>
		<td width="100%" valign="middle" align="center" height="30">
		<div id="divAD" style="width:330" align="left" style="font-size:8pt">
			배경까지 인쇄하셔야 아래의 영수증을 보실 수 있습니다.</br>
			■ 설정 : 도구&gt인터넷옵션&gt고급&gt인쇄(배경 및 이미지인쇄 선택)
		</div>
		</td>
	</tr>
	<tr>
		<td width="100%" valign="middle" align="center">
		<!--
	    /***********************************************************************
	            영수증
	    ************************************************************************/
		-->
		<table width="330" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td height="5" colspan="3"></td>
			</tr>
			<tr>
                            <td width="25"></td>
				<td width="280" align="center" valign="middle">
				<table border="0" width="280" align="center" cellpadding="0" cellspacing="1" bgcolor="#7893C8">
					<tr>
						<td width="100%" align="center" valign="middle" colspan="11" bgcolor="#FFFFFF">
						<table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#FFFFFF">
							<tr>
								<td align="center" height="35" colspan=2><u><font size="2">카 드 매 출 전 표</font></u></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="50%" bgcolor="#FFFFFF" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">일련번호</td>
							</tr>
							<tr>
								<td align="center" height="17"><%=strbill_tran_seq%></td>
							</tr>
						</table>
						</td>
						<td width="50%" bgcolor="#FFFFFF" >
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">카드종류</td>
							</tr>
							<tr>
								<td align="center" height="17"><%=strbill_iss_nm%></td>
							</tr>
						</table>
						</td>
						<td width="50%" align="center" valign="middle"></td>
					</tr>
					<tr>
						<td width="50%" bgcolor="#FFFFFF" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">결제방법</td>
							</tr>
                                                        <%
                                                            if(strbill_installment.equals("00"))
                                                            {
                                                        %>
							<tr>
								<td align="center" height="17">일시불</td>
							</tr>
                                                        <%
                                                            }
                                                            else
                                                            {
                                                        %>
							<tr>
								<td align="center" height="17"><%=strbill_installment%></td>
							</tr>
                                                        <%
                                                            }
                                                        %>
						</table>
						</td>
						<td width="50%" bgcolor="#FFFFFF" >
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">카드번호</td>
							</tr>
							<tr>
								<td align="center" height="17"><%=strbill_ViewCardNo%></td>
							</tr>
						</table>
						</td>
						<td width="50%" align="center" valign="middle"></td>
					</tr>
					<tr>
						<td width="50%" bgcolor="#FFFFFF" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">유효기간</td>
							</tr>
							<tr>
								<td align="center" height="17">**/**</td>
							</tr>
						</table>
						</td>
						<td width="50%" bgcolor="#FFFFFF" >
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">거래유형</td>
							</tr>
                                                        <%
                                                           if(strbill_massagetype.equals("10"))
                                                           {
                                                        %>
							<tr>
								<td align="center" height="17">승인</td>
							</tr>
                                                        <%
                                                           }
                                                           else
                                                           {
                                                        %>
							<tr>
								<td align="center" height="17">취소</td>
							</tr>
                                                        <%
                                                           }
                                                        %>
						</table>
						</td>
						<td width="50%" align="center" valign="middle"></td>
					</tr>
					<tr>
						<td width="50%" bgcolor="#FFFFFF" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">거래일시</td>
							</tr>
                                                        <%
                                                           if(strbill_massagetype.equals("10"))
                                                           {
                                                        %>
							<tr>
                                                            <td align="center" height="17"><%=strbill_app_dt%>&nbsp;<%=strbill_app_tm%></td>
							</tr>
                                                        <%
                                                           }
                                                           else
                                                           {
                                                        %>
                                                            <td align="center" height="17"><%=strbill_org_app_dd%></td>
                                                        <%
                                                           }
                                                        %>
						</table>
						</td>
						<td width="50%" bgcolor="#FFFFFF" >
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">취소일시</td>
							</tr>
                                                        <%
                                                           if(strbill_massagetype.equals("10"))
                                                           {
                                                        %>                                                        
							<tr>
                                                            <td align="center" height="17"><%=strbill_cncl_dt%></td>
							</tr>
                                                        <%
                                                           }
                                                           else
                                                           {
                                                        %>
                                                            <td align="center" height="17"><%=strbill_app_dt%>&nbsp;<%=strbill_app_tm%></td>
                                                        <%
                                                           }
                                                        %>
						</table>
						</td>
                                                <td width="50%" align="center" valign="middle"></td>
					</tr>
					
					<tr>
						<td width="30%" bgcolor="#DBE4F7">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left">품명</td>
							</tr>
						</table>
						</td>
						<td width="20%" bgcolor="#DBE4F7">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>&nbsp;</td>
							</tr>
						</table>
						</td>
						<td width="20%" bgcolor="#DBE4F7">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>&nbsp;</td>
							</tr>
						</table>
						</td>            
					</tr>
					<tr>
						<!--상품명-->
						<td width="30%" bgcolor="#FFFFFF" rowspan="4">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left"><%=strbill_product_nm%></td>
							</tr>
						</table>
						</td>
						<td width="20%" bgcolor="#FFFFFF">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							bgcolor="#DBE4F7">
							<tr>
								<td align="left">과세금액 <br>
								<font style='font-size:7pt;font-family:돋움;'>(AMOUNT)</font></td>
							</tr>
						</table>
						</td>

						<td width="6%" bgcolor="#FFFFFF">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="right" width="6%"><%=strbill_ViewAmt%></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="20%" bgcolor="#FFFFFF">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							bgcolor="#DBE4F7">
							<tr>
								<td align="left">비과세금액 <br>
								<font style='font-size:7pt;font-family:돋움;'>(SERVICE)</font></td>
							</tr>
						</table>
						</td>

						<td width="6%" bgcolor="#FFFFFF">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="right" width="6%"><%=strbill_ViewSvcAmt%></td>
							</tr>
						</table>
						</td>
					</tr>                                        
					<tr>
						<td width="20%" bgcolor="#FFFFFF">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							bgcolor="#DBE4F7">
							<tr>
								<td align="left">세금 <br>
								<font style='font-size:7pt;font-family:돋움;'>(TAXES)</font></td>
							</tr>
						</table>
						</td>

						<td width="6%" bgcolor="#FFFFFF">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="right" width="6%"><%=strbill_ViewTaxAmt%></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="20%" bgcolor="#FFFFFF">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							bgcolor="#DBE4F7">
							<tr>
								<td align="left">합계 <br>
								<font style='font-size:7pt;font-family:돋움;'>(TOTALS)</font></td>
							</tr>
						</table>
						</td>

						<td width="6%" bgcolor="#FFFFFF">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="right" width="6%"><%=strbill_ViewTotAmt%></td>
							</tr>
						</table>
						</td>
					</tr>

					<!-- CTL 관련 추가 -->

					<tr>
                                            <td width="50%" bgcolor="#FFFFFF" colspan="2" >
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">고객명</td>
							</tr>
							<tr>
								<td align="center" height="17"><%=strbill_user_nm%></td>
							</tr>
						</table>
						</td>						
						<td width="50%" bgcolor="#FFFFFF" >
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">승인번호/고유번호</td>
							</tr>
                                                        <%
                                                           if(strbill_massagetype.equals("10"))
                                                           {
                                                        %>       
							<tr>
								<td align="center" height="17"><%=strbill_app_no%></td>
							</tr>
                                                        <%
                                                           }
                                                           else
                                                           {
                                                        %>
							<tr>
								<td align="center" height="17"><%=strbill_org_app_no%></td>
							</tr>
                                                        <%
                                                           }
                                                        %>
						</table>
						</td>
					</tr>
                                        <tr>
                                            <td width="50%" bgcolor="#9bcdff" colspan="3" align="center"><b>공급자 정보</b></td>
					<tr>
						<td width="50%" bgcolor="#FFFFFF" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">공급자 상호<br>
								</td>
							</tr>
							<tr>
								<td align="center" height="17"><%=strbill_comp_nm%></td>
							</tr>
						</table>
						</td>
						<td width="50%" bgcolor="#FFFFFF" >
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">사업자등록번호</td>
							</tr>
							<tr>
								<td align="center" height="17"><%=strbill_biz_no%></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="50%" bgcolor="#FFFFFF" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">대표자명<br>
								</td>
							</tr>
							<tr>
								<td align="center" height="17"><%=strbill_comp_ceo_nm%></td>
							</tr>
						</table>
						</td>
						<td width="50%" bgcolor="#FFFFFF" >
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">이용문의(구매,취소,환불)</td>
							</tr>
							<tr>
								<td align="center" height="17"><%=strbill_comp_tel1%></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="100%" bgcolor="#FFFFFF" colspan="9">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">사업장주소</td>
							</tr>
							<tr>
                                                            <td align="center" height="17"><%=strbill_addr_1%>&nbsp;<%=strbill_addr_2%></td>
							</tr>
						</table>
						</td>
					</tr>	 
                                        <tr>
                                            <td width="50%" bgcolor="#9bcdff" colspan="3" align="center"><b>가맹점 정보</b></td>
					<tr>                                        
					<tr>
						<td width="50%" bgcolor="#FFFFFF" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">가맹점명</td>
							</tr>
							<tr>
	
								<td align="center" height="17"><%=onffCompInfo_comp_nm%></td>
					
							</tr>
						</table>
						</td>
						<td width="50%" bgcolor="#FFFFFF" >
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">사업자등록번호</td>
							</tr>
							<tr>
	
								<td align="center" height="17"><%=onffCompInfo_ViewOnffBizNo%></td>
					
							</tr>
						</table>
						</td>
					</tr>
					<tr>
                                            <td width="50%" bgcolor="#FFFFFF" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">대표자</td>
							</tr>
							<tr>
	
								<td align="center" height="17"><%=onffCompInfo_comp_ceo_nm%></td>
					
							</tr>
						</table>
						</td>
						<td width="50%" bgcolor="#FFFFFF" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">승인관련문의</td>
							</tr>
							<tr>
								<td align="center" height="17"><%=onffCompInfo_comp_tel1%></td>
							</tr>
						</table>
						</td>
						
					</tr>                                        
					<tr>
						<td width="100%" bgcolor="#FFFFFF" colspan="3">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">가맹점주소</td>
							</tr>
							<tr>
								<td align="center" height="17"><%=onffCompInfo_addr_1%><br><%=onffCompInfo_addr_2%></td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				<!--border end--></td>
				<td width="25"></td>
			</tr>
			<tr>
				<td width="330" height="5" align="center" valign="middle" colspan="3"></td>
			</tr>      
			<tr>
                            <td width="330" height="18" align="center" valign="middle" colspan="3"><input type="button" value="인쇄" onclick="window.print();" />&nbsp;<input type="button" value="닫기" onclick="window.close()" /></td>                        
                        </tr>                     
		</table>
		<!--영수증 end-->
		</td>
	</tr>
</table>
</div>
</form>
<%
  }
  else
  {
%>
<form name="frm_pay" method="post" >
<div id="divMsg" ondragstart="return false" onselectstart="return false" oncontextmenu="return false">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td height="5"></td>
	</tr>
	<tr>
		<td width="100%" valign="middle" align="center">
		<!--
	    /***********************************************************************
	            영수증
	    ************************************************************************/
		-->
		<table width="330" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td height="5" colspan="3"></td>
			</tr>
			<tr>
                            <td width="25"></td>
				<td width="280" align="center" valign="middle">
				<table border="0" width="280" align="center" cellpadding="0" cellspacing="1" bgcolor="#7893C8">
					<tr>
						<td width="100%" align="center" valign="middle" colspan="11" bgcolor="#FFFFFF">
						<table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#FFFFFF">
							<tr>
								<td align="center" height="35" colspan=2><u><font size="2">카 드 승 인 결 과</font></u></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="100%" bgcolor="#FFFFFF" >
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7" align="center">오류코드</td>
							</tr>
							<tr>
								<td align="center" height="17">${tran_result_cd}</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="100%" bgcolor="#FFFFFF" >
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7" align="center">오류내용</td>
							</tr>
							<tr>
								<td align="center" height="17">${tran_result_msg}</td>
							</tr>
						</table>
						</td>
						<td width="50%" align="center" valign="middle"></td>
					</tr>
				</table>
				<!--border end--></td>
				<td width="25"></td>
			</tr>
			<tr>
				<td width="330" height="5" align="center" valign="middle" colspan="3"></td>
			</tr>                        
			<tr>
                            <td width="330" height="18" align="center" valign="middle" colspan="3"><input type="button" value="close" onclick="window.close()" /></td>
			</tr>
		</table>
		<!--영수증 end-->
		</td>
	</tr>
</table>
</div>
</form>    
<%
    }
%>
</body>
</html>
