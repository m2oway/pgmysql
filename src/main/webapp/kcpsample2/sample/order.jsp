<%@ page language="java" contentType="text/html;charset=euc-kr"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>*** KCP Online Payment System Ver 7.0[HUB Version] ***</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
    <link href="css/style.css" rel="stylesheet" type="text/css" id="cssLink"/>
    <script type="text/javascript">

        // 주문번호 생성 예제
        function init_orderid()
        {	
            var today = new Date();
            var year  = today.getFullYear();
            var month = today.getMonth()+ 1;
            var date  = today.getDate();
            var time  = today.getTime();

            if(parseInt(month) < 10)
            {
                month = "0" + month;
            }

            var vOrderID = year + "" + month + "" + date + "" + time;

            document.forms[0].ordr_idxx.value = vOrderID;
        }

    </script>
</head>

<body onload="init_orderid();" oncontextmenu="return false;" ondragstart="return false;" onselectstart="return false;">

<div id="sample_wrap">

<form name="formOrder" action="order_pay_ssl.jsp" method="post">

                    <h1>[결제요청] <span> 이 페이지는 결제를 요청하는 샘플(예시) 페이지입니다.</span></h1>
                    <!-- 상단 문구 -->
                    <div class="sample">
                            <p>소스 수정 시 가맹점의 상황에 맞게 적절히 수정 적용하시길 바랍니다.<br />
                            결제에 필요한 정보를 정확하게 입력하시어 결제를 진행하시기 바랍니다.</p>
                    <!-- 상단 테이블 End -->

                <!-- 주문정보 타이틀 -->
                    <h2>&sdot; 주문 정보</h2>
                    <table class="tbl" cellpadding="0" cellspacing="0">

                    <!-- 지불 방법 -->
                    <tr>
                        <th>지불 방법</th>
                        <td><input type="text" name="pay_method" value="CARD" size="13" class="w100" readonly/></td>
                    </tr>
                    <!-- 주문 번호 -->
                    <tr>
                        <th>주문 번호</th>
                        <td><input type="text" name="ordr_idxx" class="w200" value="" maxlength="40"/></td>
                    </tr>
                    <!-- 상품명 -->
                    <tr>
                        <th>상품명</th>
                        <td><input type="text" name="good_name" class="w100" value="운동화"/></td>
                    </tr>
                    <!-- 결제 금액 -->
                    <tr>
                        <th>결제 금액</th>
                        <td><input type="text" name="good_mny" class="w100" value="1004" maxlength="9"/>원(숫자만 입력)</td>
                    </tr>
                    <!-- 주문자 이름 -->
                    <tr>
                        <th>주문자명</th>
                        <td><input type="text" name="buyr_name" class="w100" value="홍길동"/></td>
                    </tr>
                    <!-- 주문자 E-Mail -->
                    <tr>
                        <th>E-mail</th>
                        <td><input type="text" name="buyr_mail" class="w200" value="test@test.co.kr" maxlength="30" /></td>
                    </tr>
                    <!-- 주문자 전화번호 -->
                    <tr>
                        <th>전화번호</th>
                        <td><input type="text" name="buyr_tel1" class="w100" value="02-2108-1000"/></td>
                    </tr>
                    <!-- 주문자 휴대폰번호 -->
                    <tr>
                        <th>휴대폰번호</th>
                        <td><input type="text" name="buyr_tel2" class="w100" value="010-0000-0000"/></td>
                    </tr>
                    </table>
                    <!-- 주문 정보 출력 테이블 End -->

                    <!-- 결제 버튼 테이블 Start -->
                        <!-- 결제 요청/처음으로 이미지 버튼 -->
                    <div class="btnset" id="display_pay_button" style="display:block">
                      <input name="" type="submit" class="submit" value="결제요청" onclick="return card_type_check();"/>
                      <a href="index.html" class="home">처음으로</a>
                    </div>
                </div>
                    <!-- 결제 버튼 테이블 End -->
                  <div class="footer">
                    Copyright (c) KCP INC. All Rights reserved.
                  </div>

<!-- 요청종류 승인(pay)/취소,매입(mod) 요청시 사용 -->
<input type="hidden" name="req_tx"    value="pay">
<!-- 필수 항목 : 결제 금액/화폐단위 -->
<input type="hidden" name="currency" value="410">
<!-- 필수 항목 : 할부개월수 옵션 (고객이 선택할 수 있는 할부개월수의 최대값을 세팅, 세팅하지 않을 경우 일시불로 자동 세팅됨) -->
<input type="hidden" name="quotaopt" value="12">

    </form>
</div>
</body>
</html>