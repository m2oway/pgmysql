<%@ page language="java" contentType="text/html;charset=euc-kr"%>
<%
    /* ============================================================================== */
    /* =   PAGE : 일반결제 지불 처리 PAGE                                           = */
    /* = -------------------------------------------------------------------------- = */
    /* =   연동시 오류가 발생하는 경우 아래의 주소로 접속하셔서 확인하시기 바랍니다.= */
    /* =   접속 주소 : http://kcp.co.kr/technique.requestcode.do			        = */
    /* = -------------------------------------------------------------------------- = */
    /* =   Copyright (c)  2013   KCP Inc.   All Rights Reserverd.                   = */
    /* ============================================================================== */


    /* ============================================================================== */
    /* =   환경 설정 파일 Include                                                   = */
    /* = -------------------------------------------------------------------------- = */
    /* =   ※ 주의 ※                                                               = */
    /* =   테스트 및 실결제 연동시 site_conf_inc.jsp파일을 수정하시기 바랍니다.     = */
    /* = -------------------------------------------------------------------------- = */
%>

<%@ include file = "./../cfg/site_conf_inc.jsp"%>

<%
    /* = -------------------------------------------------------------------------- = */
    /* =   환경 설정 파일 Include End                                               = */
    /* ============================================================================== */
%>
<%!
    /* ============================================================================== */
    /* =   null 값을 처리하는 메소드                                                = */
    /* = -------------------------------------------------------------------------- = */
        public String f_get_parm( String val )
        {
          if ( val == null ) val = "";
          return  val;
        }
    /* ============================================================================== */
%>
<%
    request.setCharacterEncoding ( "euc-kr" ) ;
    /* ============================================================================== */
    /* =   01. 지불 요청 정보 설정                                                  = */
    /* = -------------------------------------------------------------------------- = */
    String pay_method = f_get_parm( request.getParameter( "pay_method" ) );  // 결제 방법
    String ordr_idxx  = f_get_parm( request.getParameter( "ordr_idxx"  ) );  // 주문 번호
    String good_name  = f_get_parm( request.getParameter( "good_name"  ) );  // 상품 정보
    String good_mny   = f_get_parm( request.getParameter( "good_mny"   ) );  // 결제 금액
    String buyr_name  = f_get_parm( request.getParameter( "buyr_name"  ) );  // 주문자 이름
    String buyr_mail  = f_get_parm( request.getParameter( "buyr_mail"  ) );  // 주문자 E-Mail
    String buyr_tel1  = f_get_parm( request.getParameter( "buyr_tel1"  ) );  // 주문자 전화번호
    String buyr_tel2  = f_get_parm( request.getParameter( "buyr_tel2"  ) );  // 주문자 휴대폰번호
    String req_tx     = f_get_parm( request.getParameter( "req_tx"     ) );  // 요청 종류
    String currency   = f_get_parm( request.getParameter( "currency"   ) );  // 화폐단위 (WON/USD)
    String quotaopt   = f_get_parm( request.getParameter( "quotaopt"   ) );  // 할부개월수 옵션
    /* ============================================================================== */


    /* ============================================================================== */
    /* =   02. HTML 페이지 관련 처리                                                = */
    /* = -------------------------------------------------------------------------- = */
    
	StringBuffer quota_list        = new StringBuffer() ;  // 할부 개월 리스트 표시
	StringBuffer expire_year_list  = new StringBuffer() ;  // 유효기간(년) 리스트 표시
  StringBuffer expire_month_list = new StringBuffer() ;  // 유효기간(월) 리스트 표시

  quota_list.append ( "<select name=\"quota\" class=\"frmselect\">" ) ;
  quota_list.append ( "<option value=\"00\" selected>일시불</option>" ) ;

  // 할부개월 리스트 표시
  if ( Integer.parseInt( good_mny ) >= 50000 )
  {
      if( quotaopt.equals("") ) { quotaopt = "0"; }

      for( int index = 2 ; index <= Integer.parseInt(quotaopt) ; index++ )
      {
          if( index < 10  )
              quota_list.append ( "<option value=\"0" + index + "\">" + index + "개월</option>" ) ;
          else
              quota_list.append ( "<option value=\""  + index + "\">" + index + "개월</option>" ) ;
      }
  }
  quota_list.append ( "</select>" ) ;

  // 유효기간(년) 리스트 표시
  final int CURR_YEAR = 13 ;
  final int MAX_YEAR  = 25 ;

  expire_year_list.append ( "<select name=\"expiry_yy\" class=\"frmselect\">\n" )
                  .append ( "    <option value=\"xx\" selected>선택</option>\n" ) ;

  for ( int index = CURR_YEAR ; index <= MAX_YEAR ; index++ )
  {
      if ( index < 10 )
          expire_year_list.append ( "<option value=\"0" + index + "\" >0" + index + "</option>\n" ) ;
      else
          expire_year_list.append ( "<option value=\"" + index + "\" >" + index + "</option>\n" ) ;
  }
  expire_year_list.append ( "</select>" ) ;


  // 유효기간(월) 리스트 표시
  expire_month_list.append ( "<select name=\"expiry_mm\"  class=\"frmselect\">\n" )
                   .append ( "<option value=\"xx\" selected>선택</option>\n"      ) ;

  for ( int index = 1 ; index < 13 ; index++ )
  {
      if ( index < 10 )
          expire_month_list.append ( "<option value=\"0" + index + "\" >0" + index + "</option>\n" ) ;
      else
          expire_month_list.append ( "<option value=\"" + index + "\" >" + index + "</option>\n" ) ;
  }
  expire_month_list.append ( "</select>\n" ) ;

    /* ============================================================================== */


    /* ============================================================================== */
    /* =   03. 일반 KEY-IN 결제 페이지 구현                                         = */
    /* ============================================================================== */
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>*** KCP Online Payment System Ver 7.0[HUB Version] ***</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
    <link href="css/style.css" rel="stylesheet" type="text/css" id="cssLink"/>
    <script type="text/javascript">

    function jsf__show_progress( show )
    {
        if ( show == true )
        {
            window.show_pay_btn.style.display  = "none";
            window.show_progress.style.display = "inline";
        }
        else
        {
            window.show_pay_btn.style.display  = "inline";
            window.show_progress.style.display = "none";
        }
    }

    function  jsf__chk_ssl_card( form )
    {
        cardno    = form.card_no.value;
        cardnolen = form.card_no.value.length;

        if ( cardnolen < 13 || cardnolen > 16 )
        {
            alert( "카드번호의 길이는 최소 13자리에서 최대 16자리입니다." );
            form.card_no.focus();
            form.card_no.select();
            return  false;
        }

        for ( inx = 0 ; inx < cardnolen ; inx ++ )
        {
            if ( cardno.charAt( inx ) > "9" || cardno.charAt( inx ) < "0" )
            {
                alert( "카드번호는 \'-\' 없이 숫자만 입력하여 주십시오." );
                form.card_no.focus();
                form.card_no.select();
                return  false;
            }
        }
    }

    function  jsf__pay_card( form )
    {
        var return_Val = false ;

        jsf__show_progress(true);

        if ( jsf__chk_ssl_card( form ) == false )
        {
            jsf__show_progress(false);
            return false ;
        }

        if ( form.cardtype.value == "3" )
        {
            form.cardauth.value = form.cardauth2.value;
        }
        else if ( form.cardtype.value == "4" )
        {
            form.quota.value    = "00";
            form.cardauth.value = "";
            form.cardpwd.value  = "";
        }

        return_Val = true ;

        return return_Val ;
    }

    </script>
</head>

<body>

<div id="sample_wrap">

    <form name="ssl_form" action="./pp_cli_hub.jsp" method="post">

                    <h1>[결제요청] <span> 이 페이지는 결제를 요청하는 샘플(예시) 페이지입니다.</span></h1>
                    <!-- 상단 문구 -->
                    <div class="sample">
                            <p>소스 수정시 소스 안에 <span>※ 주의 ※</span>표시가 포함된 문장은
                            가맹점의 상황에 맞게 적절히 수정 적용하시길 바랍니다.<br />
                            이 페이지는 신용카드 결제를 요청하는 페이지입니다.</p>
                    <!-- 상단 문구 End -->

                <!-- 상단 테이블 End -->

                <!-- 주문 정보 출력 테이블 Start -->
                    <h2>&sdot; 주문 정보</h2>
                    <table class="tbl" cellpadding="0" cellspacing="0">
                    <!-- 주문번호 -->
                    <tr>
                        <th>주문번호</th>
                        <td><%= ordr_idxx %></td>
                    </tr>
                    <!-- 상품명 -->
                    <tr>
                        <th>상품명</th>
                        <td><%= good_name %></td>
                    </tr>
                    <!-- 결제 금액 -->
                    <tr>
                        <th>결제 금액</th>
                        <td><%= good_mny %> 원</td>
                    </tr>
                    <!-- 주문자 이름 -->
                    <tr>
                        <th>주문자 이름</th>
                        <td><%= buyr_name %></td>
                    </tr>
                    <!-- 주문자 E-Mail -->
                    <tr>
                        <th>주문자 E-Mail</th>
                        <td><%= buyr_mail %></td>
                    </tr>
                    <!-- 주문자 전화번호 -->
                    <tr>
                        <th>주문자 전화번호</th>
                        <td><%= buyr_tel1 %></td>
                    </tr>
                    <!-- 주문자 휴대폰번호 -->
                    <tr>
                        <th>주문자 휴대폰번호</th>
                        <td><%= buyr_tel2 %></td>
                    </tr>
                </table>
                <!-- 주문 정보 출력 테이블 End -->

                <!-- 결제 정보 출력 테이블 Start -->
                <h2>&sdot; 결제 정보</h2>
                <table class="tbl" cellpadding="0" cellspacing="0">
                    <!-- 요청 방법 : 신용카드 일반 -->
                    <tr>
                        <th>결제 방법</th>
                        <td>신용카드 - 일반</td>
                    </tr>
                    <!-- 결제 금액 -->
                    <tr>
                        <th>결제 금액</th>
                        <td><%= good_mny %> 원</td>
                    </tr>
                    <!-- 카드 번호 -->
                    <tr>
                        <th>카드 번호</th>
                        <td><input type="text" name="card_no" value="" size="20" maxlength="16" class="frminput">('-'없이 숫자만, 14~16자리)</td>
                    </tr>
                    <!-- 할부 개월 -->
                    <tr id="show_quota">
                        <th>할부 개월</th>
                        <td><%= quota_list.toString() %></td>
                    </tr>
                    <!-- 유효 기간 -->
                    <tr>
                        <th>유효 기간</th>
                        <td><%= expire_year_list.toString() %>년 <%= expire_month_list.toString() %>월</td>
                    </tr>
                    <!-- 인증 정보 -->
                    <tr>
                        <th>인증 정보</th>
                        <td>
                            <input type='password' name='cardauth' value="" size="14" maxlength='10' class="frminput"/> ( 주민번호 뒤 7자리, 사업자번호 10자리 )
                        </td>
                    </tr>
                    <!-- 비밀번호 -->
                    <tr>
                        <th>비밀 번호</th>
                        <td>
                            <input type="password" name="cardpwd" value="" size="3" maxlength="2" class="frminput">(카드비밀번호 앞2자리)
                        </td>
                    </tr>
                </table>
                <!-- 결제 정보 출력 테이블 End -->

                <!-- 결제 버튼 테이블 Start -->
                    <!-- 결제 요청/처음으로 버튼 -->
                    <div class="btnset" id="show_pay_btn" style="display:block">
                      <input name="" type="submit" class="submit" value="결제요청" onclick="return jsf__pay_card(this.form);"/>
                      <a href="index.html" class="home">처음으로</a>
                    </div>
                    <!-- 결제 진행 중입니다. 메시지 -->
                    <div id="show_progress" style="display:none">
                        <td colspan="2" class="center red" >결제 진행 중입니다. 잠시만 기다려 주십시오...</td>
                    </div>
                </div>
                <!-- 결제 버튼 테이블 End -->
                  <div class="footer">
                    Copyright (c) KCP INC. All Rights reserved.
                  </div>

    <!-- KCP 관련 셋팅 -->
    <input type="hidden" name="card_pay_method" value="SSL">
    <input type="hidden" name="pay_method" value="<%= pay_method %>">
    <input type="hidden" name="ordr_idxx"  value="<%= ordr_idxx  %>">
    <input type="hidden" name="good_name"  value="<%= good_name  %>">
    <input type="hidden" name="good_mny"   value="<%= good_mny   %>">
    <input type="hidden" name="buyr_name"  value="<%= buyr_name  %>">
    <input type="hidden" name="buyr_mail"  value="<%= buyr_mail  %>">
    <input type="hidden" name="buyr_tel1"  value="<%= buyr_tel1  %>">
    <input type="hidden" name="buyr_tel2"  value="<%= buyr_tel2  %>">
    <input type="hidden" name="req_tx"     value="<%= req_tx     %>">
    <input type="hidden" name="currency"   value="<%= currency   %>">
    <input type="hidden" name="quotaopt"   value="<%= quotaopt   %>">

    </form>
</div>
</body>
</html>