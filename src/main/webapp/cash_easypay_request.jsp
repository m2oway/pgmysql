<%@ page contentType="text/html; charset=euc-kr" %>
<%@page import="java.security.Security"%>
<%@ include file="./inc/easypay_config.jsp" %>
<%--
    /*****************************************************************************
     * Easypay 기본 설정정보를 include한다
     ****************************************************************************/
--%>
<%@ page import="com.kicc.*" %>
<%@ page import="java.util.Enumeration"%>
<%!
    /**
     * 파라미터 체크 메소드
     */
    public String getNullToSpace(String param) {
        return (param == null) ? "" : param.trim();
    }
%>
<%
  /* -------------------------------------------------------------------------- */
  /* ::: 처리구분 설정                                                          */
  /* -------------------------------------------------------------------------- */
  final String ISSUE    = "issue";   // 발행
  final String CANCL    = "cancel";  // 취소

  String tr_cd          = getNullToSpace(request.getParameter("EP_tr_cd"));            // [필수]요청구분
  String pay_type       = getNullToSpace(request.getParameter("EP_pay_type"));         // [필수]결제수단
  String req_type       = getNullToSpace(request.getParameter("EP_req_type"));         // [필수]요청타입

  /* -------------------------------------------------------------------------- */
  /* ::: 현금영수증 발행정보 설정                                               */
  /* -------------------------------------------------------------------------- */
  String order_no       = getNullToSpace(request.getParameter("EP_order_no"));         // [필수]주문번호
  String user_id        = getNullToSpace(request.getParameter("EP_user_id"));          // [선택]고객 ID
  String user_nm        = getNullToSpace(request.getParameter("EP_user_nm"));          // [선택]고객명
  String issue_type     = getNullToSpace(request.getParameter("EP_issue_type"));       // [필수]현금영수증발행용도
  String auth_type      = getNullToSpace(request.getParameter("EP_auth_type"));        // [필수]인증구분
  String auth_value     = getNullToSpace(request.getParameter("EP_auth_value"));       // [필수]인증번호
  String sub_mall_yn    = getNullToSpace(request.getParameter("EP_sub_mall_yn"));      // [필수]하위가맹점사용여부
  String sub_mall_buss  = getNullToSpace(request.getParameter("EP_sub_mall_buss"));    // [선택]하위가맹점사업자번호
  String tot_amt        = getNullToSpace(request.getParameter("EP_tot_amt"));          // [필수]총거래금액
  String service_amt    = getNullToSpace(request.getParameter("EP_service_amt"));      // [필수]봉사료
  String vat            = getNullToSpace(request.getParameter("EP_vat"));              // [필수]부가세

  /* -------------------------------------------------------------------------- */
  /* ::: 현금영수증 취소정보 설정                                               */
  /* -------------------------------------------------------------------------- */
  String mgr_txtype     = getNullToSpace(request.getParameter("mgr_txtype"));          // [필수]거래구분
  String org_cno        = getNullToSpace(request.getParameter("org_cno"));             // [필수]원거래고유번호
  String req_id         = getNullToSpace(request.getParameter("req_id"));              // [필수]가맹점 관리자 로그인 아이디
  String mgr_msg        = getNullToSpace(request.getParameter("mgr_msg"));             // [선택]변경 사유

  /* -------------------------------------------------------------------------- */ 
  /* ::: IP 정보 설정                                                           */ 
  /* -------------------------------------------------------------------------- */ 
  String client_ip      = request.getRemoteAddr();                                     // [필수]결제고객 IP

  /* -------------------------------------------------------------------------- */
  /* ::: 결제 결과                                                              */
  /* -------------------------------------------------------------------------- */
  String res_cd         = "";
  String res_msg        = "";

  /* -------------------------------------------------------------------------- */
  /* ::: 결제 결과                                                              */
  /* -------------------------------------------------------------------------- */
  String r_cno             = "";	//PG거래번호 
  String r_amount          = "";	//총 결제금액
  String r_auth_no         = "";	//승인번호
  String r_tran_date       = "";	//승인일시
  String r_pnt_auth_no     = "";	//포인트승인번호
  String r_pnt_tran_date   = "";	//포인트승인일시
  String r_cpon_auth_no    = "";	//쿠폰승인번호
  String r_cpon_tran_date  = "";	//쿠폰승인일시
  String r_card_no         = "";	//카드번호
  String r_issuer_cd       = "";	//발급사코드
  String r_issuer_nm       = "";	//발급사명
  String r_acquirer_cd     = "";	//매입사코드
  String r_acquirer_nm     = "";	//매입사명
  String r_install_period  = "";	//할부개월
  String r_noint           = "";	//무이자여부
  String r_bank_cd         = "";	//은행코드
  String r_bank_nm         = "";	//은행명
  String r_account_no      = "";	//계좌번호
  String r_deposit_nm      = "";	//입금자명
  String r_expire_date     = "";	//계좌사용만료일
  String r_cash_res_cd     = "";	//현금영수증 결과코드
  String r_cash_res_msg    = "";	//현금영수증 결과메세지
  String r_cash_auth_no    = "";	//현금영수증 승인번호
  String r_cash_tran_date  = "";	//현금영수증 승인일시
  String r_auth_id         = "";	//PhoneID
  String r_billid          = "";	//인증번호
  String r_mobile_no       = "";	//휴대폰번호
  String r_ars_no          = "";	//전화번호
  String r_cp_cd           = "";	//포인트사/쿠폰사
  String r_used_pnt        = "";	//사용포인트
  String r_remain_pnt      = "";	//잔여한도
  String r_pay_pnt         = "";	//할인/발생포인트
  String r_accrue_pnt      = "";	//누적포인트
  String r_remain_cpon     = "";	//쿠폰잔액
  String r_used_cpon       = "";	//쿠폰 사용금액
  String r_mall_nm         = "";	//제휴사명칭
  String r_escrow_yn       = "";	//에스크로 사용유무
  String r_complex_yn      = "";	//복합결제 유무
  String r_canc_acq_date   = "";	//매입취소일시
  String r_canc_date       = "";	//취소일시
  String r_refund_date     = "";	//환불예정일시

  /* -------------------------------------------------------------------------- */
  /* ::: EasyPayClient 인스턴스 생성 [변경불가 !!].                             */
  /* -------------------------------------------------------------------------- */
  EasyPayClient easyPay = new EasyPayClient();
  easyPay.easypay_setenv_init( g_gw_url, g_gw_port, g_cert_file, g_log_dir );
  easyPay.easypay_reqdata_init();

  if( ISSUE.equals(req_type) )
  {
	/* ---------------------------------------------------------------------- */
    /* ::: 발행요청                                                           */
    /* ---------------------------------------------------------------------- */
    int cash_data = easyPay.easypay_item("cash_data");
    easyPay.easypay_deli_us( cash_data, "order_no"      , order_no     );
    easyPay.easypay_deli_us( cash_data, "user_id"       , user_id      );    
    easyPay.easypay_deli_us( cash_data, "user_nm"       , user_nm      );
    easyPay.easypay_deli_us( cash_data, "issue_type"    , issue_type   );
    easyPay.easypay_deli_us( cash_data, "auth_type"     , auth_type    );
    easyPay.easypay_deli_us( cash_data, "auth_value"    , auth_value   );	
    easyPay.easypay_deli_us( cash_data, "sub_mall_yn"   , sub_mall_yn  );	
    
    if( sub_mall_yn =="1" )
        easyPay.easypay_deli_us( cash_data, "sub_mall_buss", sub_mall_buss   );	

    easyPay.easypay_deli_us( cash_data, "tot_amt"      , tot_amt      );	
    easyPay.easypay_deli_us( cash_data, "service_amt"  , service_amt  );	
    easyPay.easypay_deli_us( cash_data, "vat"          , vat          );
  }
  else if( CANCL.equals(req_type) )
  {
  	/* ---------------------------------------------------------------------- */
    /* ::: 취소 요청                                                          */
    /* ---------------------------------------------------------------------- */
    int mgr_data = easyPay.easypay_item("mgr_data");
    easyPay.easypay_deli_us( mgr_data, "mgr_txtype"    , mgr_txtype   );
    easyPay.easypay_deli_us( mgr_data, "org_cno"       , org_cno      );    
    easyPay.easypay_deli_us( mgr_data, "req_ip"        , client_ip    );
    easyPay.easypay_deli_us( mgr_data, "req_id"        , req_id       );
    easyPay.easypay_deli_us( mgr_data, "mgr_msg"       , mgr_msg      );
  }
  
  /* -------------------------------------------------------------------------- */
  /* ::: 실행                                                                   */
  /* -------------------------------------------------------------------------- */ 
  if ( tr_cd.length() > 0 )
  {
    easyPay.easypay_run( g_mall_id, tr_cd, order_no );
    
    res_cd = easyPay.res_cd;
    res_msg = easyPay.res_msg;
  } 
  else
  {
    res_cd  = "M114";
    res_msg = "연동 오류|tr_cd값이 설정되지 않았습니다.";
  }

  /* -------------------------------------------------------------------------- */
  /* ::: 결과 처리                                                              */
  /* -------------------------------------------------------------------------- */
  r_cno             = easyPay.easypay_get_res( "cno"             );    //PG거래번호 
  r_amount          = easyPay.easypay_get_res( "amount"          );    //총 결제금액
  r_auth_no         = easyPay.easypay_get_res( "auth_no"         );    //승인번호
  r_tran_date       = easyPay.easypay_get_res( "tran_date"       );    //승인일시
  r_pnt_auth_no     = easyPay.easypay_get_res( "pnt_auth_no"     );    //포인트승인번호
  r_pnt_tran_date   = easyPay.easypay_get_res( "pnt_tran_date"   );    //포인트승인일시
  r_cpon_auth_no    = easyPay.easypay_get_res( "cpon_auth_no"    );    //쿠폰승인번호
  r_cpon_tran_date  = easyPay.easypay_get_res( "cpon_tran_date"  );    //쿠폰승인일시
  r_card_no         = easyPay.easypay_get_res( "card_no"         );    //카드번호
  r_issuer_cd       = easyPay.easypay_get_res( "issuer_cd"       );    //발급사코드
  r_issuer_nm       = easyPay.easypay_get_res( "issuer_nm"       );    //발급사명
  r_acquirer_cd     = easyPay.easypay_get_res( "acquirer_cd"     );    //매입사코드
  r_acquirer_nm     = easyPay.easypay_get_res( "acquirer_nm"     );    //매입사명
  r_install_period  = easyPay.easypay_get_res( "install_period"  );    //할부개월
  r_noint           = easyPay.easypay_get_res( "noint"           );    //무이자여부
  r_bank_cd         = easyPay.easypay_get_res( "bank_cd"         );    //은행코드
  r_bank_nm         = easyPay.easypay_get_res( "bank_nm"         );    //은행명
  r_account_no      = easyPay.easypay_get_res( "account_no"      );    //계좌번호
  r_deposit_nm      = easyPay.easypay_get_res( "deposit_nm"      );    //입금자명
  r_expire_date     = easyPay.easypay_get_res( "expire_date"     );    //계좌사용만료일
  r_cash_res_cd     = easyPay.easypay_get_res( "cash_res_cd"     );    //현금영수증 결과코드
  r_cash_res_msg    = easyPay.easypay_get_res( "cash_res_msg"    );    //현금영수증 결과메세지
  r_cash_auth_no    = easyPay.easypay_get_res( "cash_auth_no"    );    //현금영수증 승인번호
  r_cash_tran_date  = easyPay.easypay_get_res( "cash_tran_date"  );    //현금영수증 승인일시
  r_auth_id         = easyPay.easypay_get_res( "auth_id"         );    //PhoneID
  r_billid          = easyPay.easypay_get_res( "billid"          );    //인증번호
  r_mobile_no       = easyPay.easypay_get_res( "mobile_no"       );    //휴대폰번호
  r_ars_no          = easyPay.easypay_get_res( "ars_no"          );    //전화번호
  r_cp_cd           = easyPay.easypay_get_res( "cp_cd"           );    //포인트사/쿠폰사
  r_used_pnt        = easyPay.easypay_get_res( "used_pnt"        );    //사용포인트
  r_remain_pnt      = easyPay.easypay_get_res( "remain_pnt"      );    //잔여한도
  r_pay_pnt         = easyPay.easypay_get_res( "pay_pnt"         );    //할인/발생포인트
  r_accrue_pnt      = easyPay.easypay_get_res( "accrue_pnt"      );    //누적포인트
  r_remain_cpon     = easyPay.easypay_get_res( "remain_cpon"     );    //쿠폰잔액
  r_used_cpon       = easyPay.easypay_get_res( "used_cpon"       );    //쿠폰 사용금액
  r_mall_nm         = easyPay.easypay_get_res( "mall_nm"         );    //제휴사명칭
  r_escrow_yn       = easyPay.easypay_get_res( "escrow_yn"       );    //에스크로 사용유무
  r_complex_yn      = easyPay.easypay_get_res( "complex_yn"      );    //복합결제 유무
  r_canc_acq_date   = easyPay.easypay_get_res( "canc_acq_date"   );    //매입취소일시
  r_canc_date       = easyPay.easypay_get_res( "canc_date"       );    //취소일시
  r_refund_date     = easyPay.easypay_get_res( "refund_date"     );    //환불예정일시
%>

<html>
<meta name="robots" content="noindex, nofollow"> 
<script type="text/javascript">
    function f_submit(){
        document.frm.submit();
    }
</script>

<body onload="f_submit();">
<form name="frm" method="post" action="./result.jsp">
    <input type="hidden" name="res_cd"          value="<%=res_cd%>">            <!-- 결과코드 //-->
    <input type="hidden" name="res_msg"         value="<%=res_msg%>">           <!-- 결과메시지 //-->
    <input type="hidden" name="order_no"        value="<%=order_no%>">          <!-- 주문번호 //-->
    <input type="hidden" name="cno"             value="<%=r_cno%>">             <!-- PG거래번호 //-->
    
    <input type="hidden" name="amount"          value="<%=r_amount%>">          <!-- 총 결제금액 //-->
    <input type="hidden" name="auth_no"         value="<%=r_auth_no%>">         <!-- 승인번호 //-->
    <input type="hidden" name="tran_date"       value="<%=r_tran_date%>">       <!-- 거래일시 //-->
    <input type="hidden" name="pnt_auth_no"     value="<%=r_pnt_auth_no%>">     <!-- 포인트 승인 번호 //-->
    <input type="hidden" name="pnt_tran_date"   value="<%=r_pnt_tran_date%>">   <!-- 포인트 승인 일시 //-->
    <input type="hidden" name="cpon_auth_no"    value="<%=r_cpon_auth_no%>">    <!-- 쿠폰 승인 번호 //-->
    <input type="hidden" name="cpon_tran_date"  value="<%=r_cpon_tran_date%>">  <!-- 쿠폰 승인 일시 //-->
    <input type="hidden" name="card_no"         value="<%=r_card_no%>">         <!-- 카드번호 //-->
    <input type="hidden" name="issuer_cd"       value="<%=r_issuer_cd%>">       <!-- 발급사코드 //-->
    <input type="hidden" name="issuer_nm"       value="<%=r_issuer_nm%>">       <!-- 발급사명 //-->
    <input type="hidden" name="acquirer_cd"     value="<%=r_acquirer_cd%>">     <!-- 매입사코드 //-->
    <input type="hidden" name="acquirer_nm"     value="<%=r_acquirer_nm%>">     <!-- 매입사명 //-->
    <input type="hidden" name="install_period"  value="<%=r_install_period%>">  <!-- 할부개월 //-->
    <input type="hidden" name="noint"           value="<%=r_noint%>">           <!-- 무이자여부 //-->
    <input type="hidden" name="bank_cd"         value="<%=r_bank_cd%>">         <!-- 은행코드 //-->
    <input type="hidden" name="bank_nm"         value="<%=r_bank_nm%>">         <!-- 은행명 //-->
    <input type="hidden" name="account_no"      value="<%=r_account_no%>">      <!-- 계좌번호 //-->
    <input type="hidden" name="deposit_nm"      value="<%=r_deposit_nm%>">      <!-- 입금자명 //-->
    <input type="hidden" name="expire_date"     value="<%=r_expire_date%>">     <!-- 계좌사용만료일시 //-->
    <input type="hidden" name="cash_res_cd"     value="<%=r_cash_res_cd%>">     <!-- 현금영수증 결과코드 //-->
    <input type="hidden" name="cash_res_msg"    value="<%=r_cash_res_msg%>">    <!-- 현금영수증 결과메세지 //-->
    <input type="hidden" name="cash_auth_no"    value="<%=r_cash_auth_no%>">    <!-- 현금영수증 승인번호 //-->
    <input type="hidden" name="cash_tran_date"  value="<%=r_cash_tran_date%>">  <!-- 현금영수증 승인일시 //-->
    <input type="hidden" name="auth_id"         value="<%=r_auth_id%>">         <!-- PhoneID //-->
    <input type="hidden" name="billid"          value="<%=r_billid%>">          <!-- 인증번호 //-->
    <input type="hidden" name="mobile_no"       value="<%=r_mobile_no%>">       <!-- 휴대폰번호 //-->
    <input type="hidden" name="ars_no"          value="<%=r_ars_no%>">          <!-- ARS 전화번호 //-->
    <input type="hidden" name="cp_cd"           value="<%=r_cp_cd%>">           <!-- 포인트사 //-->
    <input type="hidden" name="used_pnt"        value="<%=r_used_pnt%>">        <!-- 사용포인트 //-->
    <input type="hidden" name="remain_pnt"      value="<%=r_remain_pnt%>">      <!-- 잔여한도 //-->
    <input type="hidden" name="pay_pnt"         value="<%=r_pay_pnt%>">         <!-- 할인/발생포인트 //-->
    <input type="hidden" name="accrue_pnt"      value="<%=r_accrue_pnt%>">      <!-- 누적포인트 //-->
    <input type="hidden" name="remain_cpon"     value="<%=r_remain_cpon%>">     <!-- 쿠폰잔액 //-->
    <input type="hidden" name="used_cpon"       value="<%=r_used_cpon%>">       <!-- 쿠폰 사용금액 //-->
    <input type="hidden" name="mall_nm"         value="<%=r_mall_nm%>">         <!-- 제휴사명칭 //-->
    <input type="hidden" name="escrow_yn"       value="<%=r_escrow_yn%>">       <!-- 에스크로 사용유무 //-->
    <input type="hidden" name="complex_yn"      value="<%=r_complex_yn%>">      <!-- 복합결제 유무 //-->
    <input type="hidden" name="canc_acq_data"   value="<%=r_canc_acq_date%>">   <!-- 매입취소일시 //-->
    <input type="hidden" name="canc_date"       value="<%=r_canc_date%>">       <!-- 취소일시 //-->
    <input type="hidden" name="refund_date"     value="<%=r_refund_date%>">     <!-- 환불예정일시 //-->
    <input type="hidden" name="pay_type"        value="<%=pay_type%>">          <!-- 결제수단 //-->
    
    <input type="hidden" name="gw_url"          value="<%=g_gw_url%>">          <!--  //-->
    <input type="hidden" name="gw_port"         value="<%=g_gw_port%>">         <!--  //-->
</form>
</body>
</html>
