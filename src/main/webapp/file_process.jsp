<%-- 
    Document   : file_process
    Created on : 2017. 5. 29, 오후 7:20:44
    Author     : MoonBong
--%>
<%@page import="com.onoffkorea.system.common.util.Util"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.commons.fileupload.FileItem"%>
<%@page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.sql.*" %>
<%@page import="javax.naming.*"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="com.onoffkorea.system.app.Vo.KiccPgVo"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%!
    /**
     * 파라미터 체크 메소드
     */
    public String getNullToSpace(String param) {
        return (param == null) ? "" : param.trim();
    }

    public String getNullToZero(String param) {
        return (param == null) ? "0" : param.trim();
    }
        
    
    public void closeResultSet(ResultSet rs)
    {
        try
        {
            if ( rs != null ) rs.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    
    public void closePstmt(PreparedStatement psmt)
    {
        try
        {
            if ( psmt != null ) psmt.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }    
    
    public void closeConnection(Connection con)
    {
        try
        {
            if ( con != null ) con.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }       
    
    public void closeFileOutputStream(FileOutputStream os)
    {
        try
        {
            if ( os != null ) os.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }        
    
    public void closeInputStream(InputStream in)
    {
        try
        {
            if ( in != null ) in.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }         
    
    
    public void closeFileInputStream(FileInputStream in)
    {
        try
        {
            if ( in != null ) in.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }       
        
        
    public void closeBufferedReader(BufferedReader in)
    {
        try
        {
            if ( in != null ) in.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }           
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>거래내역파일처리</title>
    </head>
    <body>
<%
    final Logger logger = Logger.getLogger("file_process.jsp");
    Date today=new Date();
    SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmss"); 
    
    String strCurDate = formater.format(today);
        
    FileOutputStream os=null;
    InputStream in=null;
            
    FileInputStream fis = null;
    BufferedReader br = null;
    
    Connection conn = null;
    PreparedStatement pstmt = null;
    PreparedStatement pstmt2 = null;
    ResultSet rs = null;
    ResultSet rs2 = null;
   
    String filename=null;
   
    try
    {
       //DB Connection을 가져온다.
        //Context initContext = new InitialContext();                                            //2
		//System.out.println("-------------1-------------");
        //DataSource ds = (DataSource) initContext.lookup("java:comp/env/jdbc/takeonoracle");     //3
		///		System.out.println("-------------2-------------");

		/*
		 Context initContext = new InitialContext();
				System.out.println("-------------A1-------------");
		 Context envContext  = (Context) initContext.lookup("java:comp/env");
				System.out.println("-------------2-------------");
		 DataSource ds = (DataSource) envContext.lookup("jdbc/takeonoracle");


        conn = ds.getConnection();                                                             //4
						System.out.println("-------------3-------------");
        */
				//JDBC 드라이버를 가져옵니다.
		Class.forName("com.mysql.jdbc.Driver");
		//DB에 접속을 한다.
//                conn = DriverManager.getConnection("jdbc:mysql://10.100.50.32:3306/okpay","okpay","okpay!");
//                conn = DriverManager.getConnection("jdbc:mysql://10.100.50.54:3306/okpay","okpay","okpay!");
  		  conn = DriverManager.getConnection("jdbc:mysql://192.168.0.241:3306/okpay","okpay","okpay!");


        conn.setAutoCommit(false);
		//System.out.println("-------------4-------------");


        
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        String strTpMerchCms = "";
        String strTpMerchDay = "";
        String strTpVanCms = "";
        String strTpVanDay = "";
        //file을 save한다.
        if(isMultipart)
        {
            DiskFileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);

            List<FileItem> items = upload.parseRequest(request);

            for( FileItem item : items)
            {
                if(item.isFormField())
                {
                    String name=item.getFieldName();

                    String value=item.getString("utf-8");
                    out.println("요청파라미터:"+name+"="+value+"<br/>");
                    
                    if(name.equals("merch_cms"))
                    {
                        strTpMerchCms = Util.nullToString(value).trim();
                    }
                    else if(name.equals("merch_day"))
                    {
                        strTpMerchDay = Util.nullToString(value).trim();
                    }
                    else if(name.equals("van_cms"))
                    {
                        strTpVanCms = Util.nullToString(value).trim();
                    }
                    else if(name.equals("van_day"))
                    {
                        strTpVanDay = Util.nullToString(value).trim();
                    }
                        
                }
                else
                {
                    String name=item.getFieldName();
                    String fileName=item.getName();
                    String contentType=item.getContentType();
                    long size=item.getSize();
                    
                    out.println("요청파라미터 : " + name +"<br/>" );
                    out.println("파일명 : "+ fileName +"<br/>");
                    out.println("내용형식 : "+ contentType +"<br/>");
                    out.println("파일크기 : "+ size +"<br/>");

                    //filesave
                    filename="/usr/local/tomcat6/uploadfile/okpay/" + strCurDate +fileName;
//                    filename="c:\\uploadfile\\okpay\\" + strCurDate +fileName;
                    //out.println("file저장 : "+filename+"<br/>");
                    request.setAttribute("filename", filename);
                    os=new FileOutputStream(filename);
                    in=item.getInputStream();
                    byte[] buffer=new byte[512];
                    
                    int len=-1;
                    
                    while((len=in.read(buffer)) !=-1)
                    {
                        os.write(buffer,0,len);
                    }
                }            
            }

        }   

        closeFileOutputStream(os);
        closeInputStream(in);
        
        
        //저장된file을 읽어 처리한다.
            fis = new FileInputStream(filename);
            String sLine = "";
            
            int readCount =0 ;
            
            br = new BufferedReader(new InputStreamReader(fis));
            while( (sLine = br.readLine()) != null )
            {
                readCount++;
                System.out.println("SLINE : ["+sLine+"]");
                
                if(readCount > 1)
                {
                    String[] arrStrLineInfo = sLine.split(",", -1);                    
                    
                    String strCol1	=	(Util.nullToString(arrStrLineInfo[0]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//온오프구분
                    String strCol2	=	(Util.nullToString(arrStrLineInfo[1]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//상점ID
                    String strCol3	=	(Util.nullToString(arrStrLineInfo[2]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//거래번호
                    String strCol4	=	(Util.nullToString(arrStrLineInfo[3]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//주문번호
                    String strCol5	=	(Util.nullToString(arrStrLineInfo[4]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//승인구분
                    String strCol6	=	(Util.nullToString(arrStrLineInfo[5]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//매입사
                    String strCol7	=	(Util.nullToString(arrStrLineInfo[6]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//카드번호
                    String strCol8	=	(Util.nullToString(arrStrLineInfo[7]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//금액
                    //String strCol9	=	(Util.nullToString(arrStrLineInfo[8]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//할부
					String strCol9	=	(Util.nullToString(arrStrLineInfo[8]).trim()).replaceAll("=\"", "").replaceAll("\"", "").trim();	//할부
                    
                    if(strCol9.equals(""))
                    {
                        strCol9="00";
                    }
                    else
                    {
                        if(strCol9.length() < 2)
                        {
                            strCol9 =  "0" + strCol9;
                        }
                    }
                    String strCol10	=	(Util.nullToString(arrStrLineInfo[9]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//승인번호
                    String strCol11	=	(Util.nullToString(arrStrLineInfo[10]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//승인일자
                    String strCol12	=	(Util.nullToString(arrStrLineInfo[11]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//승인시간
                    String strCol13	=	(Util.nullToString(arrStrLineInfo[12]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//취소일자
                    String strCol14	=	(Util.nullToString(arrStrLineInfo[13]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//취소시간
                    String strCol15	=	(Util.nullToString(arrStrLineInfo[14]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//매입요청일
                    String strCol16	=	(Util.nullToString(arrStrLineInfo[15]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//매입완료일
                    String strCol17	=	(Util.nullToString(arrStrLineInfo[16]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//매입상태
                    String strCol18	=	(Util.nullToString(arrStrLineInfo[17]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//반송사유
                    String strCol19	=	(Util.nullToString(arrStrLineInfo[18]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//주문자명
                    String strCol20	=	(Util.nullToString(arrStrLineInfo[19]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//이자구분
                    String strCol21	=	(Util.nullToString(arrStrLineInfo[20]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//발급사
                    String strCol22	=	(Util.nullToString(arrStrLineInfo[21]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//제품명
                    String strCol23	=	(Util.nullToString(arrStrLineInfo[22]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//카드구분
                    String strCol24	=	(Util.nullToString(arrStrLineInfo[23]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//통화구분
                    String strCol25	=	(Util.nullToString(arrStrLineInfo[24]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//간편결제
                    
                    logger.debug("strCol1  :["+strCol1 +"]");
                    logger.debug("strCol2  :["+strCol2 +"]");
                    logger.debug("strCol3  :["+strCol3 +"]");
                    logger.debug("strCol4  :["+strCol4 +"]");
                    logger.debug("strCol5  :["+strCol5 +"]");
                    logger.debug("strCol6  :["+strCol6 +"]");
                    logger.debug("strCol7  :["+strCol7 +"]");
                    logger.debug("strCol8  :["+strCol8 +"]");
                    logger.debug("strCol9  :["+strCol9 +"]");
                    logger.debug("strCol10 :["+strCol10+"]");
                    logger.debug("strCol11 :["+strCol11+"]");
                    logger.debug("strCol12 :["+strCol12+"]");
                    logger.debug("strCol13 :["+strCol13+"]");
                    logger.debug("strCol14 :["+strCol14+"]");
                    logger.debug("strCol15 :["+strCol15+"]");
                    logger.debug("strCol16 :["+strCol16+"]");
                    logger.debug("strCol17 :["+strCol17+"]");
                    logger.debug("strCol18 :["+strCol18+"]");
                    logger.debug("strCol19 :["+strCol19+"]");
                    logger.debug("strCol20 :["+strCol20+"]");
                    logger.debug("strCol21 :["+strCol21+"]");
                    logger.debug("strCol22 :["+strCol22+"]");
                    logger.debug("strCol23 :["+strCol23+"]");
                    logger.debug("strCol24 :["+strCol24+"]");
                    logger.debug("strCol25 :["+strCol25+"]");
                    
                    //수신된 정보로 가맹점 정보를 검색
                    StringBuffer sb_onffTidInfo = new StringBuffer();

                    sb_onffTidInfo.append(" select m.onfftid, m.onffmerch_no, m.pay_chn_cate, n.terminal_no, n.merch_no ");
                    sb_onffTidInfo.append(" from TB_ONFFTID_MST m, ");
                    sb_onffTidInfo.append("       ( ");
                    sb_onffTidInfo.append("           SELECT b.pay_mtd_seq, a.start_dt, a.end_dt, b.APP_ISS_CD, b.TID_MTD, b.TERMINAL_NO,b.PAY_MTD,b.MERCH_NO  ");
                    sb_onffTidInfo.append("            FROM TB_PAY_MTD a ");
                    sb_onffTidInfo.append("                      ,TB_PAY_MTD_DETAIL b ");
                    sb_onffTidInfo.append("            WHERE  ");
                    sb_onffTidInfo.append("            a.PAY_MTD_SEQ = b.PAY_MTD_SEQ ");
                    sb_onffTidInfo.append("            and b.DEL_FLAG  = 'N' ");
                    sb_onffTidInfo.append("            and b.USE_FLAG = 'Y' ");
                    sb_onffTidInfo.append("            and b.DEL_FLAG  = 'N' ");
                    sb_onffTidInfo.append("            and b.USE_FLAG = 'Y' ");
                    sb_onffTidInfo.append("            and a.start_dt <= DATE_FORMAT(now(),'%Y%m%d') ");
                    sb_onffTidInfo.append("            and a.end_dt  >= DATE_FORMAT(now(),'%Y%m%d') ");
                    sb_onffTidInfo.append("       ) n ");
                    sb_onffTidInfo.append(" where  ");
                    sb_onffTidInfo.append(" m.PAY_MTD_SEQ = n.pay_mtd_seq        ");
                    sb_onffTidInfo.append(" and m.del_flag = 'N'  ");
                    sb_onffTidInfo.append(" and n.merch_no = ? ");

                    pstmt = conn.prepareStatement(sb_onffTidInfo.toString());

                    pstmt.setString(1, strCol2);

                    rs = pstmt.executeQuery();

                    String strOnffTid = "";
                    String strOnffMerchNo = "";
                    String strPayChnCate = "";
                    String strVanTid = "";

                    if(rs != null && rs.next())
                    {
                        strOnffTid = getNullToSpace(rs.getString(1));
                        strOnffMerchNo = getNullToSpace(rs.getString(2));
                        strPayChnCate =  getNullToSpace(rs.getString(3));
                        strVanTid =  getNullToSpace(rs.getString(4));
                    }
                    
                    logger.debug("strOnffTid : " + strOnffTid);
                    logger.debug("strOnffMerchNo : " + strOnffMerchNo);
                    logger.debug("strPayChnCate : " + strPayChnCate);

                    closeResultSet(rs);
                    closePstmt(pstmt);
                    
                    String strTpCms="";
                    //승인이 가맹점정보가 있을시 수수료 정보를 가져온다.
                    //if(!strOnffTid.equals("") && !strOnffMerchNo.equals("") && r_noti_type.equals("10"))
                    if(!strOnffTid.equals("") && !strOnffMerchNo.equals(""))
                    {
                        //가맹점 사업자의 과세 종류를 가져온다.
                        String strTpCompCate = null;
                        StringBuffer sb_onffcompcate = new StringBuffer();
                        sb_onffcompcate.append(" select B.COMP_CATE ");
                        sb_onffcompcate.append(" from TB_ONFFMERCH_MST A, TB_COMPANY B  ");
                        sb_onffcompcate.append(" where A.COMP_SEQ = B.COMP_SEQ  ");
                        sb_onffcompcate.append(" and A.ONFFMERCH_NO = ?  ");
                        
                        pstmt = conn.prepareStatement(sb_onffcompcate.toString());
                        pstmt.setString(1, strOnffMerchNo);
                        rs = pstmt.executeQuery();  
                        
                        if(rs != null && rs.next())
                        {
                            strTpCompCate = rs.getString(1);
                        }
                        
                         closeResultSet(rs);
                         closePstmt(pstmt);
                        
                         
                         if(strTpMerchCms.equals(""))
                         {
                             //가맹점 정보에 대한 수수료 정보를 가져온다.
                            StringBuffer sb_onffcms = new StringBuffer();
                            sb_onffcms.append(" SELECT ONFFTID_CMS_SEQ, ONFFTID, COMMISSION, START_DT, END_DT ");
                            sb_onffcms.append("  FROM TB_ONFFTID_COMMISSION ");
                            sb_onffcms.append("  WHERE ONFFTID = ? ");
                            sb_onffcms.append("  and DEL_FLAG  = 'N' ");
                            sb_onffcms.append("  and USE_FLAG = 'Y' ");
                            sb_onffcms.append("  and START_DT <= ? ");
                            sb_onffcms.append("  and END_DT >= ? ");

                            String strTpAppDt = strCol11;
                            logger.debug("cms prepare create start");
                            pstmt = conn.prepareStatement(sb_onffcms.toString());
                            logger.debug("cms prepare 0");
                            pstmt.setString(1, strOnffTid);
                            pstmt.setString(2, strTpAppDt);
                            pstmt.setString(3, strTpAppDt);

                            logger.debug("cms prepare 1");
                            rs = pstmt.executeQuery();  

                            logger.debug("cms prepare create end");

                            if(rs != null && rs.next())
                            {
                                strTpCms = rs.getString(3);
                            }

                            logger.debug("strTpCms :" + strTpCms);
                            closeResultSet(rs);
                            closePstmt(pstmt);
                         }
                         else
                         {
                             strTpCms = strTpMerchCms;
                         }
                        
                                                
                        //승인 경우
                        if(strCol5.equals("승인"))
                        {
                            //신용카드일 경우
                            //if(r_pay_type.equals("11"))
                            {
                                //이미 입력거래가 있는경우
                                StringBuffer sbTranChkSql = new StringBuffer();
                                sbTranChkSql.append(" select count(1) cnt ");
                                sbTranChkSql.append(" from VW_TRAN_CARDPG ");
                                sbTranChkSql.append(" where pg_seq = ? ");
                                sbTranChkSql.append(" and TID = ? ");
                                sbTranChkSql.append(" and CARD_NUM = ? ");
                                sbTranChkSql.append(" and APP_DT = ? ");
                                sbTranChkSql.append(" and APP_NO = ? ");
                                sbTranChkSql.append(" and TOT_AMT = ? ");
                                sbTranChkSql.append(" and massagetype='10' ");
                                
                                logger.debug("app org tran start");
                                pstmt = conn.prepareStatement(sbTranChkSql.toString());

                                pstmt.setString(1, strCol3);
                                pstmt.setString(2, strCol2);
                                pstmt.setString(3, strCol7);
                                pstmt.setString(4, strCol11);
                                pstmt.setString(5, strCol10);
                                pstmt.setString(6, strCol8);                            

                                rs = pstmt.executeQuery();
                                logger.debug("app org tran end");

                                int int_appChk_cnt = 0;

                                if(rs != null && rs.next())
                                {
                                    int_appChk_cnt = rs.getInt("cnt");
                                }

                                closeResultSet(rs);
                                closePstmt(pstmt);                    

                                String strNotResult = null;
                                //접수된 승인건이 없는 경우
                                if(int_appChk_cnt == 0)
                                {
                                    logger.debug("app iss cd start");
                                    //발급사코드 변환
                                    StringBuffer sb_isscd = new StringBuffer();
                                    sb_isscd.append(" select main_code, detail_code, code_nm detailcode_nm ");
                                    sb_isscd.append(" from TB_CODE_DETAIL ");
                                    sb_isscd.append(" where main_code = 'ISS_CD' ");
                                    sb_isscd.append(" and use_flag = 'Y' ");
                                    sb_isscd.append(" and del_flag='N' ");
                                    sb_isscd.append(" and detail_code = f_get_iss_cd('KSNETPG', ?) ");

                                    pstmt = conn.prepareStatement(sb_isscd.toString());
                                    pstmt.setString(1, null);

                                    rs = pstmt.executeQuery();      
                                    
                                    logger.debug("app iss cd end");

                                    String strOnffIssCd = null;
                                    String strOnffIssNm = null;

                                    if(rs != null && rs.next())
                                    {
                                        strOnffIssCd = rs.getString(2);
                                        strOnffIssNm = rs.getString(3);
                                    }    
                                    else
                                    {
                                        strOnffIssCd = "091";
                                        strOnffIssNm = "기타카드";
                                    }
                                    
                                    logger.debug("strOnffIssCd : " + strOnffIssCd);
                                    logger.debug("strOnffIssNm : " + strOnffIssNm);
                                    
                                    closeResultSet(rs);
                                    closePstmt(pstmt);

                                    //매입사코드변환
                                    StringBuffer sb_acqcd = new StringBuffer();
                                    sb_acqcd.append(" select main_code, detail_code, code_nm detailcode_nm ");
                                    sb_acqcd.append(" from TB_CODE_DETAIL ");
                                    sb_acqcd.append(" where main_code = 'ISS_CD' ");
                                    sb_acqcd.append(" and use_flag = 'Y' ");
                                    sb_acqcd.append(" and del_flag='N' ");
                                    sb_acqcd.append(" and detail_code = f_get_iss_cd('KSNETPG', ?) ");
                                    
                                    logger.debug("app acc cd start");
                                    
                                    pstmt = conn.prepareStatement(sb_acqcd.toString());
                                    pstmt.setString(1, null);

                                    rs = pstmt.executeQuery(); 
                                    
                                    
                                    logger.debug("app acc cd end");

                                    String strOnffAcqCd = null;
                                    String strOnffAcqNm = null;

                                    if(rs != null && rs.next())
                                    {
                                        strOnffAcqCd = rs.getString(2);
                                        strOnffAcqNm = rs.getString(3);
                                    }
                                    else
                                    {
                                        strOnffAcqCd = "091";
                                        strOnffAcqNm = "기타카드";
                                    }
                                    
                                    logger.debug("app strOnffAcqCd : " +strOnffAcqCd );
                                    logger.debug("app strOnffAcqNm : " +strOnffAcqNm );
                                    
                                    closeResultSet(rs);
                                    closePstmt(pstmt);

                                    logger.debug("strTpCms : " + strTpCms);

                                    //수수료를 계산한다.
                                    //수수료율 
                                    double dbl_TpCms = Double.parseDouble(strTpCms)/Double.parseDouble("100");
                                    logger.debug("dbl_TpCms : " +dbl_TpCms );
                                    
                                    double dbl_TpApp_mat = Double.parseDouble(strCol8);
                                    logger.debug("dbl_TpApp_mat : " +dbl_TpApp_mat );
                                    
                                    //수수료 계산
                                    //double dbl_onfftid_cms = Math.floor(dbl_TpCms * dbl_TpApp_mat);
									double dbl_onfftid_cms = Math.round(dbl_TpCms * dbl_TpApp_mat);
                                    logger.debug("dbl_onfftid_cms : " +dbl_onfftid_cms );
                                    
                                    //수수료 부가세 계산
                                    //double dbl_onfftid_cmsvat = Math.floor(dbl_onfftid_cms * 0.1);
									double dbl_onfftid_cmsvat = Math.round(dbl_onfftid_cms * 0.1);
                                    logger.debug("dbl_onfftid_cmsvat : " +dbl_onfftid_cmsvat );
                                    
                                    //원천징수 세액 계산
                                    //double dbl_withholdtax = Math.floor(dbl_TpApp_mat * 0.033);
									double dbl_withholdtax = Math.round(dbl_TpApp_mat * 0.033);
                                    logger.debug("dbl_withholdtax : " + dbl_withholdtax );
                                    
                                    
                                    double dbl_onfftid_payamt;
                                    //지급액 계산
                                    if(strTpCompCate.equals("3"))
                                    {
                                        dbl_onfftid_payamt = dbl_TpApp_mat - dbl_onfftid_cms - dbl_onfftid_cmsvat - dbl_withholdtax;
                                    }
                                    else
                                    {
                                        dbl_onfftid_payamt = dbl_TpApp_mat - dbl_onfftid_cms - dbl_onfftid_cmsvat;
                                        
                                        dbl_withholdtax = 0;
                                    }
                                    
                                    double dbl_van_payamt;
                                    double dbl_van_paycms;
                                    double dbl_van_payvat;
                                    
                                    String strTpVanPayDt= null;
                                    if(!strTpVanCms.equals(""))
                                    {
                                        
                                        if( !strCol11.equals(strCol13))
                                        {
                                            double dbl_TpVanCms = Double.parseDouble(strTpVanCms)/Double.parseDouble("100");
                                            dbl_van_paycms = Math.floor(dbl_TpVanCms * dbl_TpApp_mat);
                                            dbl_van_payvat = Math.floor(dbl_van_paycms * 0.1);
                                            dbl_van_payamt = dbl_TpApp_mat - dbl_van_paycms - dbl_van_payvat;

                                            if(!strTpVanDay.equals(""))
                                            {
                                                String strquery = "select f_get_workday(?,?)";

                                                pstmt = conn.prepareStatement(strquery);
                                                pstmt.setString(1, strCol15);
                                                pstmt.setString(2, strTpVanDay);

                                               rs = pstmt.executeQuery();
                                               if(rs != null && rs.next())
                                               {
                                                   strTpVanPayDt = rs.getString(1);
                                               }
                                               //logger.debug("strTpVanPayDt : " + strTpVanPayDt);
                                               closeResultSet(rs);
                                               closePstmt(pstmt);                                         
                                            }
                                        }
                                        //당일 취소인 경우
                                        else
                                        {
                                            dbl_van_payamt = 0;
                                            dbl_van_paycms = 0;
                                            dbl_van_payvat = 0;
                                            strTpVanPayDt = null;
                                        }
                                        
                                    }
                                    else
                                    {
                                        dbl_van_payamt = 0;
                                        dbl_van_paycms = 0;
                                        dbl_van_payvat = 0;
                                        
                                        strTpVanPayDt = null;
                                    }
                                    
                                    
                                    logger.debug("dbl_onfftid_payamt : " +dbl_onfftid_payamt );

                                    logger.debug("app transeqsql start");
                                    //거래일련번호를 생성한다.
                                    String strTpTranseq = null;
                                    String sb_TpTranseqSql = "select concat(DATE_FORMAT(now(),'%y%m%d%H%i%S'), LPAD(f_get_sequence('TRANSEQ'),8,'0'))";
                                    pstmt = conn.prepareStatement(sb_TpTranseqSql);
                                    logger.debug("app transeqsql end");
                                    rs = pstmt.executeQuery();
                                    if(rs != null && rs.next())
                                    {
                                        strTpTranseq = rs.getString(1);
                                    }                
                                    
                                    logger.debug("strTpTranseq : " + strTpTranseq);
                                    
                                    closeResultSet(rs);
                                    closePstmt(pstmt);

                                    //해당거래정보를 insert 한다.
                                    StringBuffer sb_instraninfo = new StringBuffer();
                                    sb_instraninfo.append(" insert ");
                                    sb_instraninfo.append(" into TB_TRAN_CARDPG_"+strCol11.substring(2, 6) + " ( ");
                                    sb_instraninfo.append(" TRAN_SEQ			 ");
                                    sb_instraninfo.append(" ,MASSAGETYPE		 ");
                                    sb_instraninfo.append(" ,CARD_NUM			 ");
                                    sb_instraninfo.append(" ,APP_DT				 ");
                                    sb_instraninfo.append(" ,APP_TM				 ");
                                    sb_instraninfo.append(" ,APP_NO				 ");
                                    sb_instraninfo.append(" ,TOT_AMT			 ");	
                                    sb_instraninfo.append(" ,PG_SEQ				 ");
                                    sb_instraninfo.append(" ,PAY_CHN_CATE		 ");	
                                    sb_instraninfo.append(" ,TID				 ");	
                                    sb_instraninfo.append(" ,ONFFTID			 ");	
                                    sb_instraninfo.append(" ,WCC				 ");	
                                    sb_instraninfo.append(" ,EXPIRE_DT			 ");
                                    sb_instraninfo.append(" ,CARD_CATE			 ");
                                    sb_instraninfo.append(" ,ORDER_SEQ			 ");
                                    sb_instraninfo.append(" ,TRAN_DT			 ");
                                    sb_instraninfo.append(" ,TRAN_TM			 ");	
                                    sb_instraninfo.append(" ,TRAN_CATE			 ");
                                    sb_instraninfo.append(" ,FREE_INST_FLAG		 ");
                                    sb_instraninfo.append(" ,TAX_AMT			 ");	
                                    sb_instraninfo.append(" ,SVC_AMT			 ");	
                                    sb_instraninfo.append(" ,CURRENCY			 ");
                                    sb_instraninfo.append(" ,INSTALLMENT		 ");	
                                    sb_instraninfo.append(" ,ISS_CD				 ");
                                    sb_instraninfo.append(" ,ISS_NM				 ");
                                    sb_instraninfo.append(" ,APP_ISS_CD			 ");
                                    sb_instraninfo.append(" ,APP_ISS_NM			 ");
                                    sb_instraninfo.append(" ,ACQ_CD				 ");
                                    sb_instraninfo.append(" ,ACQ_NM			 ");
                                    sb_instraninfo.append(" ,APP_ACQ_CD			 ");
                                    sb_instraninfo.append(" ,APP_ACQ_NM			 ");
                                    sb_instraninfo.append(" ,MERCH_NO			 ");
                                    sb_instraninfo.append(" ,ORG_MERCH_NO		 ");
                                    sb_instraninfo.append(" ,RESULT_CD			 ");
                                    sb_instraninfo.append(" ,RESULT_MSG			 ");
                                    sb_instraninfo.append(" ,ORG_APP_DD			 ");
                                    sb_instraninfo.append(" ,ORG_APP_NO			 ");
                                    sb_instraninfo.append(" ,CNCL_REASON		 ");	
                                    sb_instraninfo.append(" ,TRAN_STATUS		 ");	
                                    sb_instraninfo.append(" ,RESULT_STATUS		 ");
                                    sb_instraninfo.append(" ,TRAN_STEP			 ");
                                    sb_instraninfo.append(" ,CNCL_DT			 ");	
                                    sb_instraninfo.append(" ,ACQ_DT				 ");
                                    sb_instraninfo.append(" ,ACQ_RESULT_CD		 ");
                                    sb_instraninfo.append(" ,HOLDOFF_DT			 ");
                                    sb_instraninfo.append(" ,HOLDON_DT			 ");
                                    sb_instraninfo.append(" ,PAY_DT				 ");
                                    sb_instraninfo.append(" ,PAY_AMT			 ");	
                                    sb_instraninfo.append(" ,COMMISION			 ");
                                    sb_instraninfo.append(" ,ONOFFTID_PAY_AMT	 ");	
                                    sb_instraninfo.append(" ,ONOFFTID_COMMISION	 ");
                                    sb_instraninfo.append(" ,CLIENT_IP			 ");
                                    sb_instraninfo.append(" ,USER_TYPE			 ");
                                    sb_instraninfo.append(" ,MEMB_USER_NO		 ");
                                    sb_instraninfo.append(" ,USER_ID				 ");
                                    sb_instraninfo.append(" ,USER_NM			 ");
                                    sb_instraninfo.append(" ,USER_MAIL			 ");
                                    sb_instraninfo.append(" ,USER_PHONE1		 ");
                                    sb_instraninfo.append(" ,USER_PHONE2		 ");
                                    sb_instraninfo.append(" ,USER_ADDR			 ");
                                    sb_instraninfo.append(" ,PRODUCT_TYPE		 ");
                                    sb_instraninfo.append(" ,PRODUCT_NM		 ");
                                    sb_instraninfo.append(" ,FILLER				 ");
                                    sb_instraninfo.append(" ,FILLER2				 ");
                                    sb_instraninfo.append(" ,TERM_FILLER1		 ");
                                    sb_instraninfo.append(" ,TERM_FILLER2		 ");	
                                    sb_instraninfo.append(" ,TERM_FILLER3		 ");	
                                    sb_instraninfo.append(" ,TERM_FILLER4		 ");	
                                    sb_instraninfo.append(" ,TERM_FILLER5		 ");	
                                    sb_instraninfo.append(" ,TERM_FILLER6		 ");	
                                    sb_instraninfo.append(" ,TERM_FILLER7		 ");	
                                    sb_instraninfo.append(" ,TERM_FILLER8		 ");	
                                    sb_instraninfo.append(" ,TERM_FILLER9		 ");	
                                    sb_instraninfo.append(" ,TERM_FILLER10		 ");
                                    sb_instraninfo.append(" ,TRAD_CHK_FLAG		 ");
                                    sb_instraninfo.append(" ,ACC_CHK_FLAG		 ");
                                    sb_instraninfo.append(" ,INS_DT				 ");
                                    sb_instraninfo.append(" ,MOD_DT				 ");
                                    sb_instraninfo.append(" ,INS_USER			 ");	
                                    sb_instraninfo.append(" ,MOD_USER			 ");
                                    sb_instraninfo.append(" ,ORG_PG_SEQ			 ");
                                    sb_instraninfo.append(" ,ONFFMERCH_NO	 ");
                                    sb_instraninfo.append(" ,auth_pw ");
                                    sb_instraninfo.append(" ,auth_value ");
                                    sb_instraninfo.append(" ,app_card_num ");
                                    sb_instraninfo.append(" ,ONOFFTID_VAT ");
                                    sb_instraninfo.append(" ,WITHHOLD_TAX ");
                                    sb_instraninfo.append(" )  ");
                                    sb_instraninfo.append(" values ( ");
                                    sb_instraninfo.append("  ? ");//tran_seq			
                                    sb_instraninfo.append(" ,? ");//massagetype			
                                    sb_instraninfo.append(" ,? ");//card_num			
                                    sb_instraninfo.append(" ,? ");//app_dt			
                                    sb_instraninfo.append(" ,? ");//app_tm			
                                    sb_instraninfo.append(" ,? ");//app_no			
                                    sb_instraninfo.append(" ,? ");//tot_amt			
                                    sb_instraninfo.append(" ,? ");//pg_seq			
                                    sb_instraninfo.append(" ,? ");//pay_chn_cate		
                                    sb_instraninfo.append(" ,? ");//tid				
                                    sb_instraninfo.append(" ,? ");//onfftid			
                                    sb_instraninfo.append(" ,? ");//wcc				
                                    sb_instraninfo.append(" ,? ");//expire_dt			
                                    sb_instraninfo.append(" ,? ");//card_cate			
                                    sb_instraninfo.append(" ,? ");//order_seq			
                                    sb_instraninfo.append(" ,? ");//tran_dt			
                                    sb_instraninfo.append(" ,? ");//tran_tm			
                                    sb_instraninfo.append(" ,? ");//tran_cate			
                                    sb_instraninfo.append(" ,? ");//free_inst_flag		
                                    sb_instraninfo.append(" ,? ");//tax_amt			
                                    sb_instraninfo.append(" ,? ");//svc_amt			
                                    sb_instraninfo.append(" ,? ");//currency			
                                    sb_instraninfo.append(" ,? ");//installment			
                                    sb_instraninfo.append(" ,? ");//iss_cd			
                                    sb_instraninfo.append(" ,? ");//iss_nm			
                                    sb_instraninfo.append(" ,? ");//app_iss_cd			
                                    sb_instraninfo.append(" ,? ");//app_iss_nm			
                                    sb_instraninfo.append(" ,? ");//acq_cd			
                                    sb_instraninfo.append(" ,? ");//acq_nm			
                                    sb_instraninfo.append(" ,? ");//app_acq_cd			
                                    sb_instraninfo.append(" ,? ");//app_acq_nm			
                                    sb_instraninfo.append(" ,? ");//merch_no			
                                    sb_instraninfo.append(" ,? ");//org_merch_no		
                                    sb_instraninfo.append(" ,? ");//result_cd			
                                    sb_instraninfo.append(" ,? ");//result_msg			
                                    sb_instraninfo.append(" ,? ");//org_app_dd			
                                    sb_instraninfo.append(" ,? ");//org_app_no			
                                    sb_instraninfo.append(" ,? ");//cncl_reason			
                                    sb_instraninfo.append(" ,? ");//tran_status			
                                    sb_instraninfo.append(" ,? ");//result_status		
                                    sb_instraninfo.append(" ,? ");//tran_step			
                                    sb_instraninfo.append(" ,? ");//cncl_dt			
                                    sb_instraninfo.append(" ,? ");//acq_dt			
                                    sb_instraninfo.append(" ,? ");//acq_result_cd
                                    sb_instraninfo.append(" ,? ");//holdoff_dt
                                    sb_instraninfo.append(" ,? ");//holdon_dt
                                    sb_instraninfo.append(" ,? ");//pay_dt
                                    sb_instraninfo.append(" ,? ");//pay_amt
                                    sb_instraninfo.append(" ,? ");//commision
                                    sb_instraninfo.append(" ,? ");//onofftid_pay_amt
                                    sb_instraninfo.append(" ,? ");//onofftid_commision
                                    sb_instraninfo.append(" ,? ");//client_ip
                                    sb_instraninfo.append(" ,? ");//user_type
                                    sb_instraninfo.append(" ,? ");//memb_user_no
                                    sb_instraninfo.append(" ,? ");//user_id
                                    sb_instraninfo.append(" ,? ");//user_nm
                                    sb_instraninfo.append(" ,? ");//user_mail
                                    sb_instraninfo.append(" ,? ");//user_phone1
                                    sb_instraninfo.append(" ,? ");//user_phone2
                                    sb_instraninfo.append(" ,? ");//user_addr
                                    sb_instraninfo.append(" ,? ");//product_type
                                    sb_instraninfo.append(" ,? ");//product_nm
                                    sb_instraninfo.append(" ,? ");//filler
                                    sb_instraninfo.append(" ,? ");//filler2
                                    sb_instraninfo.append(" ,? ");//term_filler1
                                    sb_instraninfo.append(" ,? ");//term_filler2
                                    sb_instraninfo.append(" ,? ");//term_filler3
                                    sb_instraninfo.append(" ,? ");//term_filler4
                                    sb_instraninfo.append(" ,? ");//term_filler5
                                    sb_instraninfo.append(" ,? ");//term_filler6
                                    sb_instraninfo.append(" ,? ");//term_filler7
                                    sb_instraninfo.append(" ,? ");//term_filler8
                                    sb_instraninfo.append(" ,? ");//term_filler9
                                    sb_instraninfo.append(" ,? ");//term_filler10
                                    sb_instraninfo.append(" ,? ");//trad_chk_flag
                                    sb_instraninfo.append(" ,? ");//acc_chk_flag
                                    sb_instraninfo.append("  , DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                                    sb_instraninfo.append("  , DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                                    sb_instraninfo.append(" ,? ");//ins_user			
                                    sb_instraninfo.append(" ,? ");//mod_user			
                                    sb_instraninfo.append(" ,? ");//org_pg_seq			
                                    sb_instraninfo.append(" ,? ");//onffmerch_no		
                                    sb_instraninfo.append(" ,? ");//auth_pw		  
                                    sb_instraninfo.append(" ,? ");//auth_value		  
                                    sb_instraninfo.append(" ,? ");//app_card_num	
                                    sb_instraninfo.append(" ,? ");//ONOFFTID_VAT		  
                                    sb_instraninfo.append(" ,? ");//WITHHOLD_TAX	                                    
                                    sb_instraninfo.append(" ) ");          

                                   logger.debug("app ins start");
                                    pstmt = conn.prepareStatement(sb_instraninfo.toString());
                                    logger.debug("prepareStatement create");

                                    pstmt.setString(1,strTpTranseq);//tran_seq				, jdbcType=VARCHAR} 
                                    pstmt.setString(2,"10");//massagetype			, jdbcType=VARCHAR} 
                                    pstmt.setString(3,  strCol7);//card_num			, jdbcType=VARCHAR} 
                                    pstmt.setString(4,  strCol11);//app_dt				, jdbcType=VARCHAR}
                                    pstmt.setString(5,  strCol12);//app_tm				 , jdbcType=VARCHAR}
                                    pstmt.setString(6,  strCol10);//app_no				, jdbcType=VARCHAR}
                                    pstmt.setString(7,  strCol8);//tot_amt				 , jdbcType=INTEGER }
                                    pstmt.setString(8,  strCol3);//pg_seq				, jdbcType=VARCHAR}
                                    pstmt.setString(9,  strPayChnCate);//pay_chn_cate			 , jdbcType=VARCHAR}
                                    //pstmt.setString(10, strCol2);//tid					, jdbcType=VARCHAR}
                                    pstmt.setString(10, strVanTid);//tid					, jdbcType=VARCHAR}
                                    pstmt.setString(11, strOnffTid);//onfftid				, jdbcType=VARCHAR}
                                    pstmt.setString(12, null);//wcc					, jdbcType=VARCHAR}
                                    pstmt.setString(13,"0000");//expire_dt			, jdbcType=VARCHAR}
                                    pstmt.setString(14, null);//card_cate			  , jdbcType=VARCHAR}
                                    pstmt.setString(15, null);//order_seq			  , jdbcType=VARCHAR}
                                    pstmt.setString(16, strCol11);//tran_dt				, jdbcType=VARCHAR}
                                    pstmt.setString(17, strCol12);//tran_tm				, jdbcType=VARCHAR}
                                    //pstmt.setString(18,r_noti_type);//tran_cate			  , jdbcType=VARCHAR}
                                    pstmt.setString(18,"20");//tran_cate			  , jdbcType=VARCHAR}
                                    pstmt.setString(19, null);//free_inst_flag		    , jdbcType=VARCHAR}
                                    pstmt.setDouble(20,0);//tax_amt				, jdbcType=INTEGER }
                                    pstmt.setDouble(21,0);//svc_amt				, jdbcType=INTEGER }
                                    pstmt.setString(22,null);//currency			 , jdbcType=VARCHAR}
                                    pstmt.setString(23,strCol9);//installment			   , jdbcType=VARCHAR}
                                    pstmt.setString(24, strOnffIssCd);//iss_cd				, jdbcType=VARCHAR}
                                    pstmt.setString(25, strOnffIssNm);//iss_nm				 , jdbcType=VARCHAR}
                                    pstmt.setString(26, null);//app_iss_cd			 , jdbcType=VARCHAR}
                                    pstmt.setString(27, strCol21);//app_iss_nm			 , jdbcType=VARCHAR}
                                    pstmt.setString(28, null);//acq_cd				, jdbcType=VARCHAR}
                                    pstmt.setString(29, strOnffAcqNm);//acq_nm				, jdbcType=VARCHAR}
                                    pstmt.setString(30, null);//app_acq_cd			  , jdbcType=VARCHAR}
                                    pstmt.setString(31, strCol6);//app_acq_nm			   , jdbcType=VARCHAR}
                                    pstmt.setString(32, strCol2);//merch_no			  , jdbcType=VARCHAR}
                                    pstmt.setString(33, strCol2);//org_merch_no			  , jdbcType=VARCHAR}
                                    pstmt.setString(34, "0000");//result_cd			 , jdbcType=VARCHAR}
                                    pstmt.setString(35, "성공");//result_msg			   , jdbcType=VARCHAR}
                                    pstmt.setString(36,null);//org_app_dd			  , jdbcType=VARCHAR}
                                    pstmt.setString(37,null);//org_app_no			   , jdbcType=VARCHAR}
                                    pstmt.setString(38,null);//cncl_reason			, jdbcType=VARCHAR}
                                    pstmt.setString(39,"00");//tran_status			   , jdbcType=VARCHAR}
                                    pstmt.setString(40,"00");//result_status		     , jdbcType=VARCHAR}
                                    pstmt.setString(41,"00");//tran_step			 , jdbcType=VARCHAR}
                                    pstmt.setString(42,null);//cncl_dt				, jdbcType=VARCHAR}
                                    pstmt.setString(43,null);//acq_dt				, jdbcType=VARCHAR}
                                    pstmt.setString(44,null);//acq_result_cd		     , jdbcType=VARCHAR}
                                    pstmt.setString(45,null);//holdoff_dt			  , jdbcType=VARCHAR}
                                    pstmt.setString(46,null);//holdon_dt			  , jdbcType=VARCHAR}
                                    pstmt.setString(47,strTpVanPayDt);//pay_dt				, jdbcType=VARCHAR}
                                    pstmt.setDouble(48,dbl_van_payamt);//pay_amt				, jdbcType=INTEGER }
                                    pstmt.setDouble(49,dbl_van_paycms+dbl_van_payvat);//commision			  , jdbcType=INTEGER }
                                    pstmt.setDouble(50,dbl_onfftid_payamt);//onofftid_pay_amt		, jdbcType=INTEGER }
                                    pstmt.setDouble(51,dbl_onfftid_cms);//onofftid_commision		, jdbcType=INTEGER }
                                    pstmt.setString(52,null);//client_ip				, jdbcType=VARCHAR}
                                    pstmt.setString(53,null);//user_type			, jdbcType=VARCHAR}
                                    pstmt.setString(54,null);//memb_user_no		, jdbcType=VARCHAR}
                                    pstmt.setString(55,null);//user_id				, jdbcType=VARCHAR}
                                    pstmt.setString(56,null);//user_nm				, jdbcType=VARCHAR}
                                    pstmt.setString(57,null);//user_mail			    , jdbcType=VARCHAR}
                                    pstmt.setString(58,null);//user_phone1			 , jdbcType=VARCHAR}
                                    pstmt.setString(59,null);//user_phone2			 , jdbcType=VARCHAR}
                                    pstmt.setString(60,null);//user_addr			, jdbcType=VARCHAR}
                                    pstmt.setString(61,null);//product_type		  , jdbcType=VARCHAR}
                                    pstmt.setString(62,null);//product_nm			 , jdbcType=VARCHAR}
                                    pstmt.setString(63,null);//filler				   , jdbcType=VARCHAR}
                                    pstmt.setString(64,null);//filler2				   , jdbcType=VARCHAR}
                                    pstmt.setString(65,null);//term_filler1			, jdbcType=VARCHAR}
                                    pstmt.setString(66,null);//term_filler2			, jdbcType=VARCHAR}
                                    pstmt.setString(67,null);//term_filler3			, jdbcType=VARCHAR}
                                    pstmt.setString(68,null);//term_filler4			, jdbcType=VARCHAR}
                                    pstmt.setString(69,null);//term_filler5			, jdbcType=VARCHAR}
                                    pstmt.setString(70,null);//term_filler6				, jdbcType=VARCHAR}
                                    pstmt.setString(71,null);//term_filler7			  , jdbcType=VARCHAR}
                                    pstmt.setString(72,null);//term_filler8			, jdbcType=VARCHAR}
                                    pstmt.setString(73,null);//term_filler9			, jdbcType=VARCHAR}
                                    pstmt.setString(74,null);//term_filler10			, jdbcType=VARCHAR}
                                    pstmt.setString(75,"0");//trad_chk_flag		  , jdbcType=VARCHAR}
                                    pstmt.setString(76,"0");//acc_chk_flag			, jdbcType=VARCHAR}
                                    pstmt.setString(77,"0");//ins_user				     , jdbcType=VARCHAR}
                                    pstmt.setString(78,"0");//mod_user			, jdbcType=VARCHAR}
                                    pstmt.setString(79,null);//org_pg_seq			, jdbcType=VARCHAR}
                                    pstmt.setString(80,strOnffMerchNo);//onffmerch_no		  , jdbcType=VARCHAR}
                                    pstmt.setString(81,null);//auth_pw		  , jdbcType=VARCHAR}
                                    pstmt.setString(82,null);//auth_value		  , jdbcType=VARCHAR}   
                                    pstmt.setString(83,strCol7);//app_card_num		  , jdbcType=VARCHAR}                
                                    pstmt.setDouble(84,dbl_onfftid_cmsvat);//ONOFFTID_VAT		  , jdbcType=VARCHAR}     
                                    pstmt.setDouble(85,dbl_withholdtax);//WITHHOLD_TAX		  , jdbcType=VARCHAR}     

                                    int int_insResult = pstmt.executeUpdate(); 
                                    logger.debug("prepareStatement excute");
                                    logger.debug("app ins end");
                                    
                                    closeResultSet(rs);
                                    closePstmt(pstmt);
                                }
								else
								{
									//out.println("alreay app !!");
								}
                            }
                        }
                        //취소일경우
                        else if(strCol5.equals("취소"))
                        {
                             //카드취소일 경우
                            //if(r_pay_type.equals("11"))
                            {
                                //이미 입력거래가 있는경우
                                StringBuffer sbTranChkSql = new StringBuffer();
                                sbTranChkSql.append(" select count(1) cnt ");
                                sbTranChkSql.append(" from VW_TRAN_CARDPG ");
                                sbTranChkSql.append(" where pg_seq = ? ");
                                sbTranChkSql.append(" and TID = ? ");
                                sbTranChkSql.append(" and CARD_NUM = ? ");
                                sbTranChkSql.append(" and APP_DT = ? ");
                                sbTranChkSql.append(" and APP_NO = ? ");
                                sbTranChkSql.append(" and TOT_AMT = ? ");
                                sbTranChkSql.append(" and massagetype='40' ");
                                sbTranChkSql.append(" and RESULT_STATUS='00' ");
                                logger.debug("cncl dup check start!!");
                                pstmt = conn.prepareStatement(sbTranChkSql.toString());
                                logger.debug("cncl dup check end!!");
                                pstmt.setString(1, strCol3);//pg번호
                                pstmt.setString(2, strCol2);//tid
                                pstmt.setString(3, strCol7);//카드번호
                                pstmt.setString(4, strCol13);//승인일
                                pstmt.setString(5, strCol10);//승인번호
                                pstmt.setString(6, strCol8);//총금액           

                                rs = pstmt.executeQuery();

                                int int_CnclChk_cnt = 0;

                                if(rs != null && rs.next())
                                {
                                    int_CnclChk_cnt = rs.getInt("cnt");
                                }

                                logger.debug("cncl int_CnclChk_cnt : " + int_CnclChk_cnt);
                                
                                closeResultSet(rs);
                                closePstmt(pstmt);                    

                                String strNotResult = null;
                                //접수된 승인건이 없는 경우
                                if(int_CnclChk_cnt == 0)
                                {
                                    //원거래를 가져온다.
                                    StringBuffer sb_orgtraninfoSql = new StringBuffer();

                                    sb_orgtraninfoSql.append(" select  b.comp_nm, b.biz_no, b.corp_no, b.comp_ceo_nm, b.comp_tel1, b.tax_flag ,b.merch_nm,  b.app_chk_amt, b.addr_1, b.addr_2 ");
                                    sb_orgtraninfoSql.append("         ,a.tran_seq,a.massagetype,a.card_num ");
                                    sb_orgtraninfoSql.append("         ,a.app_dt ");
                                    sb_orgtraninfoSql.append("         ,a.app_tm         ");
                                    sb_orgtraninfoSql.append("         ,a.app_no,a.tot_amt,a.pg_seq,a.tid ");
                                    sb_orgtraninfoSql.append("         ,a.pay_chn_cate,a.onfftid,a.onffmerch_no,a.wcc ");
                                    sb_orgtraninfoSql.append("         ,a.expire_dt,a.card_cate,a.order_seq ");
                                    sb_orgtraninfoSql.append("         ,a.tran_dt,a.tran_tm,a.tran_cate,a.free_inst_flag ");
                                    sb_orgtraninfoSql.append("         ,a.tax_amt,a.svc_amt,a.currency ");
                                    sb_orgtraninfoSql.append("         ,a.installment,a.iss_cd,a.iss_nm ");
                                    sb_orgtraninfoSql.append("         ,a.app_iss_cd,a.app_iss_nm,a.acq_cd ");
                                    sb_orgtraninfoSql.append("         ,a.acq_nm,a.app_acq_cd,a.app_acq_nm ");
                                    sb_orgtraninfoSql.append("         ,a.merch_no,a.org_merch_no,a.result_cd,a.result_msg ");
                                    sb_orgtraninfoSql.append("         ,a.tran_status,a.result_status ");
                                    sb_orgtraninfoSql.append("         ,a.org_app_dd,a.org_app_no,a.cncl_reason ");
                                    sb_orgtraninfoSql.append("         ,a.tran_step,a.cncl_dt,a.acq_dt,a.acq_result_cd ");
                                    sb_orgtraninfoSql.append("         ,a.holdoff_dt,a.holdon_dt,a.pay_dt,a.pay_amt ");
                                    sb_orgtraninfoSql.append("         ,a.commision,a.client_ip,a.user_type ");
                                    sb_orgtraninfoSql.append("         ,a.memb_user_no,a.user_id,a.user_nm ");
                                    sb_orgtraninfoSql.append("         ,a.user_mail,a.user_phone1,a.user_phone2 ");
                                    sb_orgtraninfoSql.append("         ,a.user_addr,a.product_type,a.product_nm ");
                                    sb_orgtraninfoSql.append("         ,a.filler,a.filler2,a.term_filler1,a.term_filler2 ");
                                    sb_orgtraninfoSql.append("         ,a.term_filler3,a.term_filler4,a.term_filler5 ");
                                    sb_orgtraninfoSql.append("         ,a.term_filler6,a.term_filler7,a.term_filler8 ");
                                    sb_orgtraninfoSql.append("         ,a.term_filler9,a.term_filler10,a.trad_chk_flag ");
                                    sb_orgtraninfoSql.append("         ,a.acc_chk_flag,a.ins_dt,a.mod_dt,a.ins_user,a.mod_user,a.onofftid_pay_amt,a.onofftid_commision,a.tb_nm, a.app_card_num, a.ONOFFTID_VAT, a.WITHHOLD_TAX	 ");
                                    sb_orgtraninfoSql.append(" from VW_TRAN_CARDPG a ");
                                    sb_orgtraninfoSql.append("          ,( ");
                                    sb_orgtraninfoSql.append("               SELECT x1.comp_nm, x1.BIZ_NO, x1.CORP_NO, x1.COMP_CEO_NM, x1.COMP_TEL1, x1.addr_1, x1.addr_2, x1.TAX_FLAG , x.ONFFMERCH_NO, x.MERCH_NM,  x.APP_CHK_AMT ");
                                    sb_orgtraninfoSql.append("                FROM  TB_ONFFMERCH_MST x, ");
                                    sb_orgtraninfoSql.append("                          TB_COMPANY x1 ");
                                    sb_orgtraninfoSql.append("                WHERE                  ");
                                    sb_orgtraninfoSql.append("                          x.del_flag = 'N' ");
                                    sb_orgtraninfoSql.append("                        AND x.SVC_STAT = '00' ");
                                    sb_orgtraninfoSql.append("                        AND x1.del_flag = 'N' ");
                                    sb_orgtraninfoSql.append("                        AND x1.use_flag = 'Y'                 ");
                                    sb_orgtraninfoSql.append("                        AND  X.COMP_SEQ = x1.comp_seq ");
                                    sb_orgtraninfoSql.append("          ) b ");
                                    sb_orgtraninfoSql.append(" where ");
                                    sb_orgtraninfoSql.append("      a.ONFFMERCH_NO = b.ONFFMERCH_NO ");
                                    sb_orgtraninfoSql.append(" and       ");
                                    sb_orgtraninfoSql.append("     a.pg_seq =? ");
                                    sb_orgtraninfoSql.append(" and a.TID = ? ");
                                    sb_orgtraninfoSql.append(" and a.CARD_NUM = ? ");
                                    sb_orgtraninfoSql.append(" and a.APP_DT = ? ");
                                    sb_orgtraninfoSql.append(" and a.APP_NO = ? ");
                                    sb_orgtraninfoSql.append(" and a.TOT_AMT = ? ");                                
                                    sb_orgtraninfoSql.append(" and    a.massagetype='10' ");
                                    sb_orgtraninfoSql.append(" and    a.result_status='00' ");
                                    
                                    logger.debug("cncl org sel start!!");
                                    
                                    pstmt2 = conn.prepareStatement(sb_orgtraninfoSql.toString());

                                    pstmt2.setString(1, strCol3);//pg번호
                                    pstmt2.setString(2, strCol2);//tid
                                    pstmt2.setString(3, strCol7);//카드번호
                                    pstmt2.setString(4, strCol11);//승인일
                                    pstmt2.setString(5, strCol10);//승인번호
                                    pstmt2.setString(6, strCol8);//총금액       

                                    rs2 = pstmt2.executeQuery();
                                    
                                    logger.debug("cncl org sel end!!");
                                    
                                     //원거래정보
                                    //원거래정보가 있는 경우
                                    if(rs2 != null && rs2.next())
                                    {
                                        String strTpVanPayDt= null;
                                        if(!strTpVanCms.equals(""))
                                        {
                                            if( !strCol11.equals(strCol13))
                                            {

                                                if(!strTpVanDay.equals(""))
                                                {
                                                    String strquery = "select f_get_workday(?,?)";

                                                    pstmt = conn.prepareStatement(strquery);
                                                    pstmt.setString(1, strCol15);
                                                    pstmt.setString(2, strTpVanDay);

                                                   rs = pstmt.executeQuery();
                                                   if(rs != null && rs.next())
                                                   {
                                                       strTpVanPayDt = rs.getString(1);
                                                   }
                                                   //logger.debug("strTpVanPayDt : " + strTpVanPayDt);
                                                   closeResultSet(rs);
                                                   closePstmt(pstmt);                                         
                                                }
                                            }
                                            //당일 취소인 경우
                                            else
                                            {
                                                strTpVanPayDt = null;
                                            }

                                        }
                                        else
                                        {
                                            strTpVanPayDt = null;
                                        }

                                        
                                        
                                         logger.debug(" ------------------------- cncl OrgTran OK start!! ----------------------------------");
                                        //거래일련번호를 생성한다.
                                        String strTpTranseq = null;
                                        String sb_TpTranseqSql = "select concat(DATE_FORMAT(now(),'%y%m%d%H%i%S'), LPAD(f_get_sequence('TRANSEQ'),8,'0'))";
                                        
                                        logger.debug("cncl create seq start!!");
                                        
                                        pstmt = conn.prepareStatement(sb_TpTranseqSql);
                                        
                                        logger.debug("cncl create pstmt!!");
                                        
                                        rs = pstmt.executeQuery();
                                        
                                        logger.debug("cncl create exe!!");
                                        if(rs != null && rs.next())
                                        {
                                            strTpTranseq = rs.getString(1);
                                        }                
                                        logger.debug("cstrTpTranseq : " + strTpTranseq);
                                        logger.debug("cncl create seq end!!");
                                        closeResultSet(rs);
                                        closePstmt(pstmt);

                                        //취소정보를 insert한다.
                                        StringBuffer sb_instraninfo = new StringBuffer();
                                        sb_instraninfo.append(" insert ");
                                        //sb_instraninfo.append(" into TB_TRAN_CARDPG_"+strCol13.substring(2, 6) + " ( ");
										sb_instraninfo.append(" into TB_TRAN_CARDPG_"+strCol11.substring(2, 6) + " ( ");
                                        sb_instraninfo.append(" TRAN_SEQ			 ");
                                        sb_instraninfo.append(" ,MASSAGETYPE		 ");
                                        sb_instraninfo.append(" ,CARD_NUM			 ");
                                        sb_instraninfo.append(" ,APP_DT				 ");
                                        sb_instraninfo.append(" ,APP_TM				 ");
                                        sb_instraninfo.append(" ,APP_NO				 ");
                                        sb_instraninfo.append(" ,TOT_AMT			 ");	
                                        sb_instraninfo.append(" ,PG_SEQ				 ");
                                        sb_instraninfo.append(" ,PAY_CHN_CATE		 ");	
                                        sb_instraninfo.append(" ,TID				 ");	
                                        sb_instraninfo.append(" ,ONFFTID			 ");	
                                        sb_instraninfo.append(" ,WCC				 ");	
                                        sb_instraninfo.append(" ,EXPIRE_DT			 ");
                                        sb_instraninfo.append(" ,CARD_CATE			 ");
                                        sb_instraninfo.append(" ,ORDER_SEQ			 ");
                                        sb_instraninfo.append(" ,TRAN_DT			 ");
                                        sb_instraninfo.append(" ,TRAN_TM			 ");	
                                        sb_instraninfo.append(" ,TRAN_CATE			 ");
                                        sb_instraninfo.append(" ,FREE_INST_FLAG		 ");
                                        sb_instraninfo.append(" ,TAX_AMT			 ");	
                                        sb_instraninfo.append(" ,SVC_AMT			 ");	
                                        sb_instraninfo.append(" ,CURRENCY			 ");
                                        sb_instraninfo.append(" ,INSTALLMENT		 ");	
                                        sb_instraninfo.append(" ,ISS_CD				 ");
                                        sb_instraninfo.append(" ,ISS_NM				 ");
                                        sb_instraninfo.append(" ,APP_ISS_CD			 ");
                                        sb_instraninfo.append(" ,APP_ISS_NM			 ");
                                        sb_instraninfo.append(" ,ACQ_CD				 ");
                                        sb_instraninfo.append(" ,ACQ_NM			 ");
                                        sb_instraninfo.append(" ,APP_ACQ_CD			 ");
                                        sb_instraninfo.append(" ,APP_ACQ_NM			 ");
                                        sb_instraninfo.append(" ,MERCH_NO			 ");
                                        sb_instraninfo.append(" ,ORG_MERCH_NO		 ");
                                        sb_instraninfo.append(" ,RESULT_CD			 ");
                                        sb_instraninfo.append(" ,RESULT_MSG			 ");
                                        sb_instraninfo.append(" ,ORG_APP_DD			 ");
                                        sb_instraninfo.append(" ,ORG_APP_NO			 ");
                                        sb_instraninfo.append(" ,CNCL_REASON		 ");	
                                        sb_instraninfo.append(" ,TRAN_STATUS		 ");	
                                        sb_instraninfo.append(" ,RESULT_STATUS		 ");
                                        sb_instraninfo.append(" ,TRAN_STEP			 ");
                                        sb_instraninfo.append(" ,CNCL_DT			 ");	
                                        sb_instraninfo.append(" ,ACQ_DT				 ");
                                        sb_instraninfo.append(" ,ACQ_RESULT_CD		 ");
                                        sb_instraninfo.append(" ,HOLDOFF_DT			 ");
                                        sb_instraninfo.append(" ,HOLDON_DT			 ");
                                        sb_instraninfo.append(" ,PAY_DT				 ");
                                        sb_instraninfo.append(" ,PAY_AMT			 ");	
                                        sb_instraninfo.append(" ,COMMISION			 ");
                                        sb_instraninfo.append(" ,ONOFFTID_PAY_AMT	 ");	
                                        sb_instraninfo.append(" ,ONOFFTID_COMMISION	 ");
                                        sb_instraninfo.append(" ,CLIENT_IP			 ");
                                        sb_instraninfo.append(" ,USER_TYPE			 ");
                                        sb_instraninfo.append(" ,MEMB_USER_NO		 ");
                                        sb_instraninfo.append(" ,USER_ID				 ");
                                        sb_instraninfo.append(" ,USER_NM			 ");
                                        sb_instraninfo.append(" ,USER_MAIL			 ");
                                        sb_instraninfo.append(" ,USER_PHONE1		 ");
                                        sb_instraninfo.append(" ,USER_PHONE2		 ");
                                        sb_instraninfo.append(" ,USER_ADDR			 ");
                                        sb_instraninfo.append(" ,PRODUCT_TYPE		 ");
                                        sb_instraninfo.append(" ,PRODUCT_NM		 ");
                                        sb_instraninfo.append(" ,FILLER				 ");
                                        sb_instraninfo.append(" ,FILLER2				 ");
                                        sb_instraninfo.append(" ,TERM_FILLER1		 ");
                                        sb_instraninfo.append(" ,TERM_FILLER2		 ");	
                                        sb_instraninfo.append(" ,TERM_FILLER3		 ");	
                                        sb_instraninfo.append(" ,TERM_FILLER4		 ");	
                                        sb_instraninfo.append(" ,TERM_FILLER5		 ");	
                                        sb_instraninfo.append(" ,TERM_FILLER6		 ");	
                                        sb_instraninfo.append(" ,TERM_FILLER7		 ");	
                                        sb_instraninfo.append(" ,TERM_FILLER8		 ");	
                                        sb_instraninfo.append(" ,TERM_FILLER9		 ");	
                                        sb_instraninfo.append(" ,TERM_FILLER10		 ");
                                        sb_instraninfo.append(" ,TRAD_CHK_FLAG		 ");
                                        sb_instraninfo.append(" ,ACC_CHK_FLAG		 ");
                                        sb_instraninfo.append(" ,INS_DT				 ");
                                        sb_instraninfo.append(" ,MOD_DT				 ");
                                        sb_instraninfo.append(" ,INS_USER			 ");	
                                        sb_instraninfo.append(" ,MOD_USER			 ");
                                        sb_instraninfo.append(" ,ORG_PG_SEQ			 ");
                                        sb_instraninfo.append(" ,ONFFMERCH_NO	 ");
                                        sb_instraninfo.append(" ,auth_pw ");
                                        sb_instraninfo.append(" ,auth_value ");
                                        sb_instraninfo.append(" ,app_card_num ");
                                        sb_instraninfo.append(" ,ONOFFTID_VAT ");
                                        sb_instraninfo.append(" ,WITHHOLD_TAX ");                                        
                                        sb_instraninfo.append(" )  ");
                                        sb_instraninfo.append(" values ( ");
                                        sb_instraninfo.append("  ? ");//tran_seq			
                                        sb_instraninfo.append(" ,? ");//massagetype			
                                        sb_instraninfo.append(" ,? ");//card_num			
                                        sb_instraninfo.append(" ,? ");//app_dt			
                                        sb_instraninfo.append(" ,? ");//app_tm			
                                        sb_instraninfo.append(" ,? ");//app_no			
                                        sb_instraninfo.append(" ,? ");//tot_amt			
                                        sb_instraninfo.append(" ,? ");//pg_seq			
                                        sb_instraninfo.append(" ,? ");//pay_chn_cate		
                                        sb_instraninfo.append(" ,? ");//tid				
                                        sb_instraninfo.append(" ,? ");//onfftid			
                                        sb_instraninfo.append(" ,? ");//wcc				
                                        sb_instraninfo.append(" ,? ");//expire_dt			
                                        sb_instraninfo.append(" ,? ");//card_cate			
                                        sb_instraninfo.append(" ,? ");//order_seq			
                                        sb_instraninfo.append(" ,? ");//tran_dt			
                                        sb_instraninfo.append(" ,? ");//tran_tm			
                                        sb_instraninfo.append(" ,? ");//tran_cate			
                                        sb_instraninfo.append(" ,? ");//free_inst_flag		
                                        sb_instraninfo.append(" ,? ");//tax_amt			
                                        sb_instraninfo.append(" ,? ");//svc_amt			
                                        sb_instraninfo.append(" ,? ");//currency			
                                        sb_instraninfo.append(" ,? ");//installment			
                                        sb_instraninfo.append(" ,? ");//iss_cd			
                                        sb_instraninfo.append(" ,? ");//iss_nm			
                                        sb_instraninfo.append(" ,? ");//app_iss_cd			
                                        sb_instraninfo.append(" ,? ");//app_iss_nm			
                                        sb_instraninfo.append(" ,? ");//acq_cd			
                                        sb_instraninfo.append(" ,? ");//acq_nm			
                                        sb_instraninfo.append(" ,? ");//app_acq_cd			
                                        sb_instraninfo.append(" ,? ");//app_acq_nm			
                                        sb_instraninfo.append(" ,? ");//merch_no			
                                        sb_instraninfo.append(" ,? ");//org_merch_no		
                                        sb_instraninfo.append(" ,? ");//result_cd			
                                        sb_instraninfo.append(" ,? ");//result_msg			
                                        sb_instraninfo.append(" ,? ");//org_app_dd			
                                        sb_instraninfo.append(" ,? ");//org_app_no			
                                        sb_instraninfo.append(" ,? ");//cncl_reason			
                                        sb_instraninfo.append(" ,? ");//tran_status			
                                        sb_instraninfo.append(" ,? ");//result_status		
                                        sb_instraninfo.append(" ,? ");//tran_step			
                                        sb_instraninfo.append(" ,? ");//cncl_dt			
                                        sb_instraninfo.append(" ,? ");//acq_dt			
                                        sb_instraninfo.append(" ,? ");//acq_result_cd
                                        sb_instraninfo.append(" ,? ");//holdoff_dt
                                        sb_instraninfo.append(" ,? ");//holdon_dt
                                        sb_instraninfo.append(" ,? ");//pay_dt
                                        sb_instraninfo.append(" ,? ");//pay_amt
                                        sb_instraninfo.append(" ,? ");//commision
                                        sb_instraninfo.append(" ,? ");//onofftid_pay_amt
                                        sb_instraninfo.append(" ,? ");//onofftid_commision
                                        sb_instraninfo.append(" ,? ");//client_ip
                                        sb_instraninfo.append(" ,? ");//user_type
                                        sb_instraninfo.append(" ,? ");//memb_user_no
                                        sb_instraninfo.append(" ,? ");//user_id
                                        sb_instraninfo.append(" ,? ");//user_nm
                                        sb_instraninfo.append(" ,? ");//user_mail
                                        sb_instraninfo.append(" ,? ");//user_phone1
                                        sb_instraninfo.append(" ,? ");//user_phone2
                                        sb_instraninfo.append(" ,? ");//user_addr
                                        sb_instraninfo.append(" ,? ");//product_type
                                        sb_instraninfo.append(" ,? ");//product_nm
                                        sb_instraninfo.append(" ,? ");//filler
                                        sb_instraninfo.append(" ,? ");//filler2
                                        sb_instraninfo.append(" ,? ");//term_filler1
                                        sb_instraninfo.append(" ,? ");//term_filler2
                                        sb_instraninfo.append(" ,? ");//term_filler3
                                        sb_instraninfo.append(" ,? ");//term_filler4
                                        sb_instraninfo.append(" ,? ");//term_filler5
                                        sb_instraninfo.append(" ,? ");//term_filler6
                                        sb_instraninfo.append(" ,? ");//term_filler7
                                        sb_instraninfo.append(" ,? ");//term_filler8
                                        sb_instraninfo.append(" ,? ");//term_filler9
                                        sb_instraninfo.append(" ,? ");//term_filler10
                                        sb_instraninfo.append(" ,? ");//trad_chk_flag
                                        sb_instraninfo.append(" ,? ");//acc_chk_flag
                                        sb_instraninfo.append("  , DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                                        sb_instraninfo.append("  , DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                                        sb_instraninfo.append(" ,? ");//ins_user			
                                        sb_instraninfo.append(" ,? ");//mod_user			
                                        sb_instraninfo.append(" ,? ");//org_pg_seq			
                                        sb_instraninfo.append(" ,? ");//onffmerch_no		
                                        sb_instraninfo.append(" ,? ");//auth_pw		  
                                        sb_instraninfo.append(" ,? ");//auth_value		  
                                        sb_instraninfo.append(" ,? ");//app_card_num	
                                        sb_instraninfo.append(" ,? ");//ONOFFTID_VAT		  
                                        sb_instraninfo.append(" ,? ");//WITHHOLD_TAX	                                        
                                        sb_instraninfo.append(" ) ");          

                                        //System.out.println("sb_instraninfo.toString() : " + sb_instraninfo.toString());
                                        logger.debug("cncl ins start!!");
                                        pstmt = conn.prepareStatement(sb_instraninfo.toString());
                                        logger.debug("cncl create!!");

                                        //당일취소,익일취소 구분
                                        String strTpOrgAppDt = rs2.getString("app_dt");
                                        String strTpCnclAppDt = strCol13;

                                        //당일취소
                                        if(strTpCnclAppDt.equals(strTpOrgAppDt))
                                        {                            
                                            pstmt.setString(4,strCol11);//app_dt				, jdbcType=VARCHAR}
                                            pstmt.setString(5,strCol12);//app_tm				 , jdbcType=VARCHAR}

                                            pstmt.setString(39,"10");//tran_status 당일취소 셋팅                            
                                            pstmt.setString(76,"3");//acc_chk_flag 매입비대상 셋팅
                                        }
                                        //익일취소
                                        else
                                        {
                                            pstmt.setString(4,strCol11);//app_dt				, jdbcType=VARCHAR}
                                            pstmt.setString(5,strCol12);//app_tm				 , jdbcType=VARCHAR}

                                            pstmt.setString(39,"20");//tran_status 익일취소(매입취소) 셋팅
                                            pstmt.setString(76,"0");//acc_chk_flag 매입비대상 셋팅
                                        }
                                        pstmt.setString(1,strTpTranseq);//tran_seq				, jdbcType=VARCHAR} 
                                        pstmt.setString(2,"40");//massagetype			, jdbcType=VARCHAR} 
                                        pstmt.setString(3,rs2.getString("card_num"));//card_num			, jdbcType=VARCHAR} 

                                        pstmt.setString(6,strCol10);//app_no				, jdbcType=VARCHAR}
                                        pstmt.setString(7,rs2.getString("tot_amt"));//tot_amt				 , jdbcType=INTEGER }
                                        pstmt.setString(8,strCol3);//pg_seq				, jdbcType=VARCHAR}
                                        pstmt.setString(9,rs2.getString("pay_chn_cate"));//pay_chn_cate			 , jdbcType=VARCHAR}
                                        //pstmt.setString(10,strCol2);//tid					, jdbcType=VARCHAR}
                                        pstmt.setString(10, strVanTid);//tid					, jdbcType=VARCHAR}
                                        pstmt.setString(11,rs2.getString("onfftid"));//onfftid				, jdbcType=VARCHAR}
                                        pstmt.setString(12,rs2.getString("wcc"));//wcc					, jdbcType=VARCHAR}
                                        pstmt.setString(13,rs2.getString("expire_dt"));//expire_dt			, jdbcType=VARCHAR}
                                        pstmt.setString(14,rs2.getString("card_cate"));//card_cate			  , jdbcType=VARCHAR}
                                        pstmt.setString(15, null);//order_seq			  , jdbcType=VARCHAR}
                                        pstmt.setString(16,strCol11);//tran_dt				, jdbcType=VARCHAR}
                                        pstmt.setString(17,strCol12);//tran_tm				, jdbcType=VARCHAR}
                                        pstmt.setString(18,"40");//tran_cate			  , jdbcType=VARCHAR}
                                        pstmt.setString(19,rs2.getString("free_inst_flag"));//free_inst_flag		    , jdbcType=VARCHAR}
                                        pstmt.setDouble(20,rs2.getDouble("tax_amt"));//tax_amt				, jdbcType=INTEGER }
                                        pstmt.setDouble(21,rs2.getDouble("svc_amt"));//svc_amt				, jdbcType=INTEGER }
                                        pstmt.setString(22,rs2.getString("currency"));//currency			 , jdbcType=VARCHAR}
                                        pstmt.setString(23,rs2.getString("installment"));//installment			   , jdbcType=VARCHAR}
                                        /*
                                        pstmt.setString(24,rs2.getString("iss_cd"));//iss_cd				, jdbcType=VARCHAR}
                                        pstmt.setString(25,rs2.getString("iss_nm"));//iss_nm				 , jdbcType=VARCHAR}
                                        pstmt.setString(26,rs2.getString("app_iss_cd"));//app_iss_cd			 , jdbcType=VARCHAR}
                                        pstmt.setString(27,rs2.getString("app_iss_nm"));//app_iss_nm			 , jdbcType=VARCHAR}
                                        pstmt.setString(28,rs2.getString("acq_cd"));//acq_cd				, jdbcType=VARCHAR}
                                        pstmt.setString(29,rs2.getString("acq_nm"));//acq_nm				, jdbcType=VARCHAR}
                                        pstmt.setString(30,rs2.getString("app_acq_cd"));//app_acq_cd			  , jdbcType=VARCHAR}
                                        pstmt.setString(31,rs2.getString("app_acq_nm"));//app_acq_nm			   , jdbcType=VARCHAR}
                                        */
                                        pstmt.setString(24,null);//iss_cd				, jdbcType=VARCHAR}
                                        pstmt.setString(25,null);//iss_nm				 , jdbcType=VARCHAR}
                                        pstmt.setString(26,null);//app_iss_cd			 , jdbcType=VARCHAR}
                                        pstmt.setString(27,null);//app_iss_nm			 , jdbcType=VARCHAR}
                                        pstmt.setString(28,null);//acq_cd				, jdbcType=VARCHAR}
                                        pstmt.setString(29,null);//acq_nm				, jdbcType=VARCHAR}
                                        pstmt.setString(30,null);//app_acq_cd			  , jdbcType=VARCHAR}
                                        pstmt.setString(31,null);//app_acq_nm			   , jdbcType=VARCHAR}

                                        pstmt.setString(32,rs2.getString("merch_no"));//merch_no			  , jdbcType=VARCHAR}
                                        pstmt.setString(33,rs2.getString("org_merch_no"));//org_merch_no			  , jdbcType=VARCHAR}
                                        pstmt.setString(34,"0000");//result_cd			 , jdbcType=VARCHAR}
                                        pstmt.setString(35,"취소성공");//result_msg			   , jdbcType=VARCHAR}
                                        pstmt.setString(36,rs2.getString("app_dt"));//org_app_dd			  , jdbcType=VARCHAR}
                                        pstmt.setString(37,rs2.getString("app_no"));//org_app_no			   , jdbcType=VARCHAR}
                                        pstmt.setString(38,"noti취소통지");//cncl_reason			, jdbcType=VARCHAR}
                                        pstmt.setString(40,"00");//result_status		     , jdbcType=VARCHAR}
                                        pstmt.setString(41,"00");//tran_step			 , jdbcType=VARCHAR}
                                        pstmt.setString(42, strCol13);//cncl_dt				, jdbcType=VARCHAR}
                                        pstmt.setString(43,null);//acq_dt				, jdbcType=VARCHAR}
                                        pstmt.setString(44,null);//acq_result_cd		     , jdbcType=VARCHAR}
                                        pstmt.setString(45,null);//holdoff_dt			  , jdbcType=VARCHAR}
                                        pstmt.setString(46,null);//holdon_dt			  , jdbcType=VARCHAR}
                                        pstmt.setString(47,strTpVanPayDt);//pay_dt				, jdbcType=VARCHAR}
                                        pstmt.setString(48,rs2.getString("pay_amt"));//pay_amt				, jdbcType=INTEGER }
                                        pstmt.setString(49,rs2.getString("commision"));//commision			  , jdbcType=INTEGER }
                                        pstmt.setString(50,rs2.getString("onofftid_pay_amt"));//onofftid_pay_amt		, jdbcType=INTEGER }
                                        pstmt.setString(51,rs2.getString("onofftid_commision"));//onofftid_commision		, jdbcType=INTEGER }
                                        pstmt.setString(52,null);//client_ip				, jdbcType=VARCHAR}
                                        pstmt.setString(53,rs2.getString("user_type"));//user_type			, jdbcType=VARCHAR}
                                        pstmt.setString(54,rs2.getString("memb_user_no"));//memb_user_no		, jdbcType=VARCHAR}
                                        pstmt.setString(55,rs2.getString("user_id"));//user_id				, jdbcType=VARCHAR}
                                        pstmt.setString(56,rs2.getString("user_nm"));//user_nm				, jdbcType=VARCHAR}
                                        pstmt.setString(57,rs2.getString("user_mail"));//user_mail			    , jdbcType=VARCHAR}
                                        pstmt.setString(58,rs2.getString("user_phone1"));//user_phone1			 , jdbcType=VARCHAR}
                                        pstmt.setString(59,rs2.getString("user_phone2"));//user_phone2			 , jdbcType=VARCHAR}
                                        pstmt.setString(60,rs2.getString("user_addr"));//user_addr			, jdbcType=VARCHAR}
                                        pstmt.setString(61,rs2.getString("product_type"));//product_type		  , jdbcType=VARCHAR}
                                        pstmt.setString(62,rs2.getString("product_nm"));//product_nm			 , jdbcType=VARCHAR}
                                        pstmt.setString(63,null);//filler				   , jdbcType=VARCHAR}
                                        pstmt.setString(64,null);//filler2				   , jdbcType=VARCHAR}
                                        pstmt.setString(65, null);//term_filler1			, jdbcType=VARCHAR}
                                        pstmt.setString(66, null);//term_filler2			, jdbcType=VARCHAR}
                                        pstmt.setString(67, null);//term_filler3			, jdbcType=VARCHAR}
                                        pstmt.setString(68, null);//term_filler4			, jdbcType=VARCHAR}
                                        pstmt.setString(69, null);//term_filler5			, jdbcType=VARCHAR}
                                        pstmt.setString(70, null);//term_filler6				, jdbcType=VARCHAR}
                                        pstmt.setString(71,null);//term_filler7			  , jdbcType=VARCHAR}
                                        pstmt.setString(72,null);//term_filler8			, jdbcType=VARCHAR}
                                        pstmt.setString(73,null);//term_filler9			, jdbcType=VARCHAR}
                                        pstmt.setString(74,null);//term_filler10			, jdbcType=VARCHAR}
                                        pstmt.setString(75,"0");//trad_chk_flag		  , jdbcType=VARCHAR}
                                        pstmt.setString(77,"0");//ins_user				     , jdbcType=VARCHAR}
                                        pstmt.setString(78,"0");//mod_user			, jdbcType=VARCHAR}
                                        pstmt.setString(79,rs2.getString("pg_seq"));//org_pg_seq			, jdbcType=VARCHAR}
                                        pstmt.setString(80,rs2.getString("onffmerch_no"));//onffmerch_no		  , jdbcType=VARCHAR}
                                        pstmt.setString(81,null);//auth_pw		  , jdbcType=VARCHAR}
                                        pstmt.setString(82,null);//auth_value		  , jdbcType=VARCHAR}   
                                        pstmt.setString(83,rs2.getString("app_card_num"));//app_card_num		  , jdbcType=VARCHAR}         
                                        
                                        pstmt.setDouble(84,rs2.getDouble("ONOFFTID_VAT"));//ONOFFTID_VAT		  , jdbcType=VARCHAR}     
                                        pstmt.setDouble(85,rs2.getDouble("WITHHOLD_TAX"));//WITHHOLD_TAX		  , jdbcType=VARCHAR}    
                                        int int_insResult = pstmt.executeUpdate();                               
                                        logger.debug("cncl exe!!");
                                        logger.debug("cncl ins end!!");
                                        
                                        closeResultSet(rs);
                                        closePstmt(pstmt);

                                        //원거래를 update한다.
                                         logger.debug("cncl org update start!!");
                                        StringBuffer sb_orgtran_mod_sql = new StringBuffer();
                                        sb_orgtran_mod_sql.append(" update " + rs2.getString("tb_nm") + " ");
                                        sb_orgtran_mod_sql.append(" set      cncl_reason = ? ");
                                        sb_orgtran_mod_sql.append("              ,tran_status= ? ");
                                        sb_orgtran_mod_sql.append("              ,cncl_dt = ? ");
                                        sb_orgtran_mod_sql.append("              ,acc_chk_flag = ? ");
                                        sb_orgtran_mod_sql.append("               ,mod_dt = DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                                        sb_orgtran_mod_sql.append("               ,mod_user= '0' ");
                                        sb_orgtran_mod_sql.append("          where 1=1 ");
                                        sb_orgtran_mod_sql.append("            and tran_seq = ? ");
                                        
                                        pstmt = conn.prepareStatement(sb_orgtran_mod_sql.toString());

                                        logger.debug("cncl pstmt create!!");
                                        pstmt.setString(1, "fileupload");
                                        pstmt.setString(3, strCol13);

                                        //당일취소
                                        if(strTpCnclAppDt.equals(strTpOrgAppDt))
                                        {
                                           pstmt.setString(2, "10");//당일취소
                                           pstmt.setString(4, "3");//매입비대상   
                                        }
                                        else
                                        {
                                            pstmt.setString(2, "20");//매입취소
                                            pstmt.setString(4, rs2.getString("acc_chk_flag"));
                                        }                        
                                        
                                        logger.debug("org trnaseq : " + rs2.getString("tran_seq"));
                                        
                                        pstmt.setString(5, rs2.getString("tran_seq"));

                                        int int_mod_orginfo = pstmt.executeUpdate();  
                                        logger.debug("cncl excute!!");
                                        
                                        logger.debug("cncl org update end!!");
                                        
                                        closeResultSet(rs);
                                        closePstmt(pstmt);

                                    } 
                                    //원거래정보가 없는 경우
                                    else
                                    {
                                        logger.debug(" ------------------------- cncl OrgTran NOT start!! ----------------------------------");
                                        //수수료를 계산한다.
                                        double dbl_TpCms = Double.parseDouble(strTpCms)/Double.parseDouble("100");
                                        double dbl_TpApp_mat = Double.parseDouble(strCol8);
                                        //double dbl_onfftid_cms = Math.floor(dbl_TpCms * dbl_TpApp_mat);
										double dbl_onfftid_cms = Math.round(dbl_TpCms * dbl_TpApp_mat);
                                        
                                        //수수료 부가세 계산
                                        //double dbl_onfftid_cmsvat = Math.floor(dbl_onfftid_cms * 0.1);
										double dbl_onfftid_cmsvat = Math.round(dbl_onfftid_cms * 0.1);
                                        logger.debug("dbl_onfftid_cmsvat : " +dbl_onfftid_cmsvat );

                                        //원천징수 세액 계산
                                        //double dbl_withholdtax = Math.floor(dbl_TpApp_mat * 0.033);
										double dbl_withholdtax = Math.round(dbl_TpApp_mat * 0.033);
                                        logger.debug("dbl_withholdtax : " + dbl_withholdtax );

                                        double dbl_onfftid_payamt;
                                        //지급액 계산
                                        if(strTpCompCate.equals("3"))
                                        {
                                            dbl_onfftid_payamt = dbl_TpApp_mat - dbl_onfftid_cms - dbl_onfftid_cmsvat - dbl_withholdtax;
                                        }
                                        else
                                        {
                                            dbl_onfftid_payamt = dbl_TpApp_mat - dbl_onfftid_cms - dbl_onfftid_cmsvat;

                                            dbl_withholdtax = 0;
                                        }
                                        
                                        
                                        double dbl_van_payamt;
                                        double dbl_van_paycms;
                                        double dbl_van_payvat;

                                        String strTpVanPayDt= null;
                                        if(!strTpVanCms.equals(""))
                                        {
                                            if( !strCol11.equals(strCol13))
                                            {
                                                double dbl_TpVanCms = Double.parseDouble(strTpVanCms)/Double.parseDouble("100");
                                                dbl_van_paycms = Math.floor(dbl_TpVanCms * dbl_TpApp_mat);
                                                dbl_van_payvat = Math.floor(dbl_van_paycms * 0.1);
                                                dbl_van_payamt = dbl_TpApp_mat - dbl_van_paycms - dbl_van_payvat;

                                                if(!strTpVanDay.equals(""))
                                                {
                                                    String strquery = "select f_get_workday(?,?)";

                                                    pstmt = conn.prepareStatement(strquery);
                                                    pstmt.setString(1, strCol15);
                                                    pstmt.setString(2, "1");

                                                   rs = pstmt.executeQuery();
                                                   if(rs != null && rs.next())
                                                   {
                                                       strTpVanPayDt = rs.getString(1);
                                                   }
                                                   //logger.debug("strTpVanPayDt : " + strTpVanPayDt);
                                                   closeResultSet(rs);
                                                   closePstmt(pstmt);                                         
                                                }
                                            }
                                            //당일 취소인 경우
                                            else
                                            {
                                                dbl_van_payamt = 0;
                                                dbl_van_paycms = 0;
                                                dbl_van_payvat = 0;
                                                strTpVanPayDt = null;
                                            }

                                        }
                                        else
                                        {
                                            dbl_van_payamt = 0;
                                            dbl_van_paycms = 0;
                                            dbl_van_payvat = 0;

                                            strTpVanPayDt = null;
                                        }                                        
                                        
                                        
                                        //거래일련번호를 생성한다.
                                        String strTpTranseq = null;
                                        String sb_TpTranseqSql = "select concat(DATE_FORMAT(now(),'%y%m%d%H%i%S'), LPAD(f_get_sequence('TRANSEQ'),8,'0'))";
                                        
                                        logger.debug("cncl create seq start!!");
                                        
                                        pstmt = conn.prepareStatement(sb_TpTranseqSql);
                                        
                                        logger.debug("cncl create pstmt!!");
                                        
                                        rs = pstmt.executeQuery();
                                        
                                        logger.debug("cncl create exe!!");
                                        if(rs != null && rs.next())
                                        {
                                            strTpTranseq = rs.getString(1);
                                        }                
                                        logger.debug("cstrTpTranseq : " + strTpTranseq);
                                        logger.debug("cncl create seq end!!");
                                        closeResultSet(rs);
                                        closePstmt(pstmt);

                                        //취소정보를 insert한다.
                                        StringBuffer sb_instraninfo = new StringBuffer();
                                        sb_instraninfo.append(" insert ");
                                        sb_instraninfo.append(" into TB_TRAN_CARDPG_"+strCol11.substring(2, 6) + " ( ");
                                        sb_instraninfo.append(" TRAN_SEQ			 ");
                                        sb_instraninfo.append(" ,MASSAGETYPE		 ");
                                        sb_instraninfo.append(" ,CARD_NUM			 ");
                                        sb_instraninfo.append(" ,APP_DT				 ");
                                        sb_instraninfo.append(" ,APP_TM				 ");
                                        sb_instraninfo.append(" ,APP_NO				 ");
                                        sb_instraninfo.append(" ,TOT_AMT			 ");	
                                        sb_instraninfo.append(" ,PG_SEQ				 ");
                                        sb_instraninfo.append(" ,PAY_CHN_CATE		 ");	
                                        sb_instraninfo.append(" ,TID				 ");	
                                        sb_instraninfo.append(" ,ONFFTID			 ");	
                                        sb_instraninfo.append(" ,WCC				 ");	
                                        sb_instraninfo.append(" ,EXPIRE_DT			 ");
                                        sb_instraninfo.append(" ,CARD_CATE			 ");
                                        sb_instraninfo.append(" ,ORDER_SEQ			 ");
                                        sb_instraninfo.append(" ,TRAN_DT			 ");
                                        sb_instraninfo.append(" ,TRAN_TM			 ");	
                                        sb_instraninfo.append(" ,TRAN_CATE			 ");
                                        sb_instraninfo.append(" ,FREE_INST_FLAG		 ");
                                        sb_instraninfo.append(" ,TAX_AMT			 ");	
                                        sb_instraninfo.append(" ,SVC_AMT			 ");	
                                        sb_instraninfo.append(" ,CURRENCY			 ");
                                        sb_instraninfo.append(" ,INSTALLMENT		 ");	
                                        sb_instraninfo.append(" ,ISS_CD				 ");
                                        sb_instraninfo.append(" ,ISS_NM				 ");
                                        sb_instraninfo.append(" ,APP_ISS_CD			 ");
                                        sb_instraninfo.append(" ,APP_ISS_NM			 ");
                                        sb_instraninfo.append(" ,ACQ_CD				 ");
                                        sb_instraninfo.append(" ,ACQ_NM			 ");
                                        sb_instraninfo.append(" ,APP_ACQ_CD			 ");
                                        sb_instraninfo.append(" ,APP_ACQ_NM			 ");
                                        sb_instraninfo.append(" ,MERCH_NO			 ");
                                        sb_instraninfo.append(" ,ORG_MERCH_NO		 ");
                                        sb_instraninfo.append(" ,RESULT_CD			 ");
                                        sb_instraninfo.append(" ,RESULT_MSG			 ");
                                        sb_instraninfo.append(" ,ORG_APP_DD			 ");
                                        sb_instraninfo.append(" ,ORG_APP_NO			 ");
                                        sb_instraninfo.append(" ,CNCL_REASON		 ");	
                                        sb_instraninfo.append(" ,TRAN_STATUS		 ");	
                                        sb_instraninfo.append(" ,RESULT_STATUS		 ");
                                        sb_instraninfo.append(" ,TRAN_STEP			 ");
                                        sb_instraninfo.append(" ,CNCL_DT			 ");	
                                        sb_instraninfo.append(" ,ACQ_DT				 ");
                                        sb_instraninfo.append(" ,ACQ_RESULT_CD		 ");
                                        sb_instraninfo.append(" ,HOLDOFF_DT			 ");
                                        sb_instraninfo.append(" ,HOLDON_DT			 ");
                                        sb_instraninfo.append(" ,PAY_DT				 ");
                                        sb_instraninfo.append(" ,PAY_AMT			 ");	
                                        sb_instraninfo.append(" ,COMMISION			 ");
                                        sb_instraninfo.append(" ,ONOFFTID_PAY_AMT	 ");	
                                        sb_instraninfo.append(" ,ONOFFTID_COMMISION	 ");
                                        sb_instraninfo.append(" ,CLIENT_IP			 ");
                                        sb_instraninfo.append(" ,USER_TYPE			 ");
                                        sb_instraninfo.append(" ,MEMB_USER_NO		 ");
                                        sb_instraninfo.append(" ,USER_ID				 ");
                                        sb_instraninfo.append(" ,USER_NM			 ");
                                        sb_instraninfo.append(" ,USER_MAIL			 ");
                                        sb_instraninfo.append(" ,USER_PHONE1		 ");
                                        sb_instraninfo.append(" ,USER_PHONE2		 ");
                                        sb_instraninfo.append(" ,USER_ADDR			 ");
                                        sb_instraninfo.append(" ,PRODUCT_TYPE		 ");
                                        sb_instraninfo.append(" ,PRODUCT_NM		 ");
                                        sb_instraninfo.append(" ,FILLER				 ");
                                        sb_instraninfo.append(" ,FILLER2				 ");
                                        sb_instraninfo.append(" ,TERM_FILLER1		 ");
                                        sb_instraninfo.append(" ,TERM_FILLER2		 ");	
                                        sb_instraninfo.append(" ,TERM_FILLER3		 ");	
                                        sb_instraninfo.append(" ,TERM_FILLER4		 ");	
                                        sb_instraninfo.append(" ,TERM_FILLER5		 ");	
                                        sb_instraninfo.append(" ,TERM_FILLER6		 ");	
                                        sb_instraninfo.append(" ,TERM_FILLER7		 ");	
                                        sb_instraninfo.append(" ,TERM_FILLER8		 ");	
                                        sb_instraninfo.append(" ,TERM_FILLER9		 ");	
                                        sb_instraninfo.append(" ,TERM_FILLER10		 ");
                                        sb_instraninfo.append(" ,TRAD_CHK_FLAG		 ");
                                        sb_instraninfo.append(" ,ACC_CHK_FLAG		 ");
                                        sb_instraninfo.append(" ,INS_DT				 ");
                                        sb_instraninfo.append(" ,MOD_DT				 ");
                                        sb_instraninfo.append(" ,INS_USER			 ");	
                                        sb_instraninfo.append(" ,MOD_USER			 ");
                                        sb_instraninfo.append(" ,ORG_PG_SEQ			 ");
                                        sb_instraninfo.append(" ,ONFFMERCH_NO	 ");
                                        sb_instraninfo.append(" ,auth_pw ");
                                        sb_instraninfo.append(" ,auth_value ");
                                        sb_instraninfo.append(" ,app_card_num ");
                                        sb_instraninfo.append(" ,ONOFFTID_VAT ");
                                        sb_instraninfo.append(" ,WITHHOLD_TAX ");                                           
                                        sb_instraninfo.append(" )  ");
                                        sb_instraninfo.append(" values ( ");
                                        sb_instraninfo.append("  ? ");//tran_seq			
                                        sb_instraninfo.append(" ,? ");//massagetype			
                                        sb_instraninfo.append(" ,? ");//card_num			
                                        sb_instraninfo.append(" ,? ");//app_dt			
                                        sb_instraninfo.append(" ,? ");//app_tm			
                                        sb_instraninfo.append(" ,? ");//app_no			
                                        sb_instraninfo.append(" ,? ");//tot_amt			
                                        sb_instraninfo.append(" ,? ");//pg_seq			
                                        sb_instraninfo.append(" ,? ");//pay_chn_cate		
                                        sb_instraninfo.append(" ,? ");//tid				
                                        sb_instraninfo.append(" ,? ");//onfftid			
                                        sb_instraninfo.append(" ,? ");//wcc				
                                        sb_instraninfo.append(" ,? ");//expire_dt			
                                        sb_instraninfo.append(" ,? ");//card_cate			
                                        sb_instraninfo.append(" ,? ");//order_seq			
                                        sb_instraninfo.append(" ,? ");//tran_dt			
                                        sb_instraninfo.append(" ,? ");//tran_tm			
                                        sb_instraninfo.append(" ,? ");//tran_cate			
                                        sb_instraninfo.append(" ,? ");//free_inst_flag		
                                        sb_instraninfo.append(" ,? ");//tax_amt			
                                        sb_instraninfo.append(" ,? ");//svc_amt			
                                        sb_instraninfo.append(" ,? ");//currency			
                                        sb_instraninfo.append(" ,? ");//installment			
                                        sb_instraninfo.append(" ,? ");//iss_cd			
                                        sb_instraninfo.append(" ,? ");//iss_nm			
                                        sb_instraninfo.append(" ,? ");//app_iss_cd			
                                        sb_instraninfo.append(" ,? ");//app_iss_nm			
                                        sb_instraninfo.append(" ,? ");//acq_cd			
                                        sb_instraninfo.append(" ,? ");//acq_nm			
                                        sb_instraninfo.append(" ,? ");//app_acq_cd			
                                        sb_instraninfo.append(" ,? ");//app_acq_nm			
                                        sb_instraninfo.append(" ,? ");//merch_no			
                                        sb_instraninfo.append(" ,? ");//org_merch_no		
                                        sb_instraninfo.append(" ,? ");//result_cd			
                                        sb_instraninfo.append(" ,? ");//result_msg			
                                        sb_instraninfo.append(" ,? ");//org_app_dd			
                                        sb_instraninfo.append(" ,? ");//org_app_no			
                                        sb_instraninfo.append(" ,? ");//cncl_reason			
                                        sb_instraninfo.append(" ,? ");//tran_status			
                                        sb_instraninfo.append(" ,? ");//result_status		
                                        sb_instraninfo.append(" ,? ");//tran_step			
                                        sb_instraninfo.append(" ,? ");//cncl_dt			
                                        sb_instraninfo.append(" ,? ");//acq_dt			
                                        sb_instraninfo.append(" ,? ");//acq_result_cd
                                        sb_instraninfo.append(" ,? ");//holdoff_dt
                                        sb_instraninfo.append(" ,? ");//holdon_dt
                                        sb_instraninfo.append(" ,? ");//pay_dt
                                        sb_instraninfo.append(" ,? ");//pay_amt
                                        sb_instraninfo.append(" ,? ");//commision
                                        sb_instraninfo.append(" ,? ");//onofftid_pay_amt
                                        sb_instraninfo.append(" ,? ");//onofftid_commision
                                        sb_instraninfo.append(" ,? ");//client_ip
                                        sb_instraninfo.append(" ,? ");//user_type
                                        sb_instraninfo.append(" ,? ");//memb_user_no
                                        sb_instraninfo.append(" ,? ");//user_id
                                        sb_instraninfo.append(" ,? ");//user_nm
                                        sb_instraninfo.append(" ,? ");//user_mail
                                        sb_instraninfo.append(" ,? ");//user_phone1
                                        sb_instraninfo.append(" ,? ");//user_phone2
                                        sb_instraninfo.append(" ,? ");//user_addr
                                        sb_instraninfo.append(" ,? ");//product_type
                                        sb_instraninfo.append(" ,? ");//product_nm
                                        sb_instraninfo.append(" ,? ");//filler
                                        sb_instraninfo.append(" ,? ");//filler2
                                        sb_instraninfo.append(" ,? ");//term_filler1
                                        sb_instraninfo.append(" ,? ");//term_filler2
                                        sb_instraninfo.append(" ,? ");//term_filler3
                                        sb_instraninfo.append(" ,? ");//term_filler4
                                        sb_instraninfo.append(" ,? ");//term_filler5
                                        sb_instraninfo.append(" ,? ");//term_filler6
                                        sb_instraninfo.append(" ,? ");//term_filler7
                                        sb_instraninfo.append(" ,? ");//term_filler8
                                        sb_instraninfo.append(" ,? ");//term_filler9
                                        sb_instraninfo.append(" ,? ");//term_filler10
                                        sb_instraninfo.append(" ,? ");//trad_chk_flag
                                        sb_instraninfo.append(" ,? ");//acc_chk_flag
                                        sb_instraninfo.append("  , DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                                        sb_instraninfo.append("  , DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                                        sb_instraninfo.append(" ,? ");//ins_user			
                                        sb_instraninfo.append(" ,? ");//mod_user			
                                        sb_instraninfo.append(" ,? ");//org_pg_seq			
                                        sb_instraninfo.append(" ,? ");//onffmerch_no		
                                        sb_instraninfo.append(" ,? ");//auth_pw		  
                                        sb_instraninfo.append(" ,? ");//auth_value		  
                                        sb_instraninfo.append(" ,? ");//app_card_num	
                                        sb_instraninfo.append(" ,? ");//ONOFFTID_VAT		  
                                        sb_instraninfo.append(" ,? ");//WITHHOLD_TAX	                                        
                                        sb_instraninfo.append(" ) ");          

                                        //System.out.println("sb_instraninfo.toString() : " + sb_instraninfo.toString());
                                        logger.debug("cncl ins start!!");
                                        pstmt = conn.prepareStatement(sb_instraninfo.toString());
                                        logger.debug("cncl create!!");

                                        //당일취소,익일취소 구분
                                        String strTpOrgAppDt = strCol11;
                                        String strTpCnclAppDt = strCol13;

                                        //당일취소
                                        if(strTpCnclAppDt.equals(strTpOrgAppDt))
                                        {                            
                                            pstmt.setString(4,strCol11);//app_dt				, jdbcType=VARCHAR}
                                            pstmt.setString(5,strCol12);//app_tm				 , jdbcType=VARCHAR}

                                            pstmt.setString(39,"10");//tran_status 당일취소 셋팅                            
                                            pstmt.setString(76,"3");//acc_chk_flag 매입비대상 셋팅
                                        }
                                        //익일취소
                                        else
                                        {
                                            pstmt.setString(4,strCol11);//app_dt				, jdbcType=VARCHAR}
                                            pstmt.setString(5,strCol12);//app_tm				 , jdbcType=VARCHAR}

                                            pstmt.setString(39,"20");//tran_status 익일취소(매입취소) 셋팅
                                            pstmt.setString(76,"0");//acc_chk_flag 매입비대상 셋팅
                                        }
                                        pstmt.setString(1,strTpTranseq);//tran_seq				, jdbcType=VARCHAR} 
                                        pstmt.setString(2,"40");//massagetype			, jdbcType=VARCHAR} 
                                        pstmt.setString(3, strCol7);//card_num			, jdbcType=VARCHAR} 

                                        pstmt.setString(6,strCol10);//app_no				, jdbcType=VARCHAR}
                                        pstmt.setString(7,strCol8);//tot_amt				 , jdbcType=INTEGER }
                                        pstmt.setString(8,strCol3);//pg_seq				, jdbcType=VARCHAR}
                                        pstmt.setString(9,strPayChnCate);//pay_chn_cate			 , jdbcType=VARCHAR}
                                        //pstmt.setString(10,strCol2);//tid					, jdbcType=VARCHAR}
                                        pstmt.setString(10, strVanTid);//tid					, jdbcType=VARCHAR}
                                        pstmt.setString(11,strOnffTid);//onfftid				, jdbcType=VARCHAR}
                                        pstmt.setString(12,null);//wcc					, jdbcType=VARCHAR}
                                        pstmt.setString(13,"0000");//expire_dt			, jdbcType=VARCHAR}
                                        pstmt.setString(14, null);//card_cate			  , jdbcType=VARCHAR}
                                        pstmt.setString(15, null);//order_seq			  , jdbcType=VARCHAR}
                                        pstmt.setString(16,strCol11);//tran_dt				, jdbcType=VARCHAR}
                                        pstmt.setString(17,strCol12);//tran_tm				, jdbcType=VARCHAR}
                                        pstmt.setString(18,"40");//tran_cate			  , jdbcType=VARCHAR}
                                        pstmt.setString(19,null);//free_inst_flag		    , jdbcType=VARCHAR}
                                        pstmt.setDouble(20,0);//tax_amt				, jdbcType=INTEGER }
                                        pstmt.setDouble(21,0);//svc_amt				, jdbcType=INTEGER }
                                        pstmt.setString(22,null);//currency			 , jdbcType=VARCHAR}
                                        pstmt.setString(23,strCol9);//installment			   , jdbcType=VARCHAR}
                                        /*
                                        pstmt.setString(24,rs2.getString("iss_cd"));//iss_cd				, jdbcType=VARCHAR}
                                        pstmt.setString(25,rs2.getString("iss_nm"));//iss_nm				 , jdbcType=VARCHAR}
                                        pstmt.setString(26,rs2.getString("app_iss_cd"));//app_iss_cd			 , jdbcType=VARCHAR}
                                        pstmt.setString(27,rs2.getString("app_iss_nm"));//app_iss_nm			 , jdbcType=VARCHAR}
                                        pstmt.setString(28,rs2.getString("acq_cd"));//acq_cd				, jdbcType=VARCHAR}
                                        pstmt.setString(29,rs2.getString("acq_nm"));//acq_nm				, jdbcType=VARCHAR}
                                        pstmt.setString(30,rs2.getString("app_acq_cd"));//app_acq_cd			  , jdbcType=VARCHAR}
                                        pstmt.setString(31,rs2.getString("app_acq_nm"));//app_acq_nm			   , jdbcType=VARCHAR}
                                        */
                                        pstmt.setString(24,null);//iss_cd				, jdbcType=VARCHAR}
                                        pstmt.setString(25,null);//iss_nm				 , jdbcType=VARCHAR}
                                        pstmt.setString(26,null);//app_iss_cd			 , jdbcType=VARCHAR}
                                        pstmt.setString(27,null);//app_iss_nm			 , jdbcType=VARCHAR}
                                        pstmt.setString(28,null);//acq_cd				, jdbcType=VARCHAR}
                                        pstmt.setString(29,null);//acq_nm				, jdbcType=VARCHAR}
                                        pstmt.setString(30,null);//app_acq_cd			  , jdbcType=VARCHAR}
                                        pstmt.setString(31,null);//app_acq_nm			   , jdbcType=VARCHAR}

                                        pstmt.setString(32,strCol2);//merch_no			  , jdbcType=VARCHAR}
                                        pstmt.setString(33,strCol2);//org_merch_no			  , jdbcType=VARCHAR}
                                        pstmt.setString(34,"0000");//result_cd			 , jdbcType=VARCHAR}
                                        pstmt.setString(35,"취소성공");//result_msg			   , jdbcType=VARCHAR}
                                        pstmt.setString(36,null);//org_app_dd			  , jdbcType=VARCHAR}
                                        pstmt.setString(37,strCol10);//org_app_no			   , jdbcType=VARCHAR}
                                        pstmt.setString(38,"fileupload");//cncl_reason			, jdbcType=VARCHAR}
                                        pstmt.setString(40,"00");//result_status		     , jdbcType=VARCHAR}
                                        pstmt.setString(41,"00");//tran_step			 , jdbcType=VARCHAR}
                                        pstmt.setString(42, strCol13);//cncl_dt				, jdbcType=VARCHAR}
                                        pstmt.setString(43,null);//acq_dt				, jdbcType=VARCHAR}
                                        pstmt.setString(44,null);//acq_result_cd		     , jdbcType=VARCHAR}
                                        pstmt.setString(45,null);//holdoff_dt			  , jdbcType=VARCHAR}
                                        pstmt.setString(46,null);//holdon_dt			  , jdbcType=VARCHAR}
                                        pstmt.setString(47,strTpVanPayDt);//pay_dt				, jdbcType=VARCHAR}
                                        pstmt.setDouble(48,dbl_van_payamt);//pay_amt				, jdbcType=INTEGER }
                                        pstmt.setDouble(49,dbl_van_paycms+dbl_van_payvat);//commision			  , jdbcType=INTEGER }
                                        pstmt.setDouble(50,dbl_onfftid_payamt);//onofftid_pay_amt		, jdbcType=INTEGER }
                                        pstmt.setDouble(51,dbl_onfftid_cms);//onofftid_commision		, jdbcType=INTEGER }
                                        pstmt.setString(52,null);//client_ip				, jdbcType=VARCHAR}
                                        pstmt.setString(53,null);//user_type			, jdbcType=VARCHAR}
                                        pstmt.setString(54,null);//memb_user_no		, jdbcType=VARCHAR}
                                        pstmt.setString(55,null);//user_id				, jdbcType=VARCHAR}
                                        pstmt.setString(56,null);//user_nm				, jdbcType=VARCHAR}
                                        pstmt.setString(57,null);//user_mail			    , jdbcType=VARCHAR}
                                        pstmt.setString(58,null);//user_phone1			 , jdbcType=VARCHAR}
                                        pstmt.setString(59,null);//user_phone2			 , jdbcType=VARCHAR}
                                        pstmt.setString(60,null);//user_addr			, jdbcType=VARCHAR}
                                        pstmt.setString(61,null);//product_type		  , jdbcType=VARCHAR}
                                        pstmt.setString(62,null);//product_nm			 , jdbcType=VARCHAR}
                                        pstmt.setString(63,null);//filler				   , jdbcType=VARCHAR}
                                        pstmt.setString(64,null);//filler2				   , jdbcType=VARCHAR}
                                        pstmt.setString(65, null);//term_filler1			, jdbcType=VARCHAR}
                                        pstmt.setString(66, null);//term_filler2			, jdbcType=VARCHAR}
                                        pstmt.setString(67, null);//term_filler3			, jdbcType=VARCHAR}
                                        pstmt.setString(68, null);//term_filler4			, jdbcType=VARCHAR}
                                        pstmt.setString(69, null);//term_filler5			, jdbcType=VARCHAR}
                                        pstmt.setString(70, null);//term_filler6				, jdbcType=VARCHAR}
                                        pstmt.setString(71,null);//term_filler7			  , jdbcType=VARCHAR}
                                        pstmt.setString(72,null);//term_filler8			, jdbcType=VARCHAR}
                                        pstmt.setString(73,null);//term_filler9			, jdbcType=VARCHAR}
                                        pstmt.setString(74,null);//term_filler10			, jdbcType=VARCHAR}
                                        pstmt.setString(75,"0");//trad_chk_flag		  , jdbcType=VARCHAR}
                                        pstmt.setString(77,"0");//ins_user				     , jdbcType=VARCHAR}
                                        pstmt.setString(78,"0");//mod_user			, jdbcType=VARCHAR}
                                        pstmt.setString(79,null);//org_pg_seq			, jdbcType=VARCHAR}
                                        pstmt.setString(80,strOnffMerchNo);//onffmerch_no		  , jdbcType=VARCHAR}
                                        pstmt.setString(81,null);//auth_pw		  , jdbcType=VARCHAR}
                                        pstmt.setString(82,null);//auth_value		  , jdbcType=VARCHAR}   
                                        pstmt.setString(83,strCol7);//app_card_num		  , jdbcType=VARCHAR}       
                                        pstmt.setDouble(84,dbl_onfftid_cmsvat);//ONOFFTID_VAT		  , jdbcType=VARCHAR}     
                                        pstmt.setDouble(85,dbl_withholdtax);//WITHHOLD_TAX		  , jdbcType=VARCHAR}                                             
                                        int int_insResult = pstmt.executeUpdate();                               
                                        logger.debug("cncl exe!!");
                                        logger.debug("cncl ins end!!");
                                        
                                        closeResultSet(rs);
                                        closePstmt(pstmt);

                                    }
                                    
                                    closeResultSet(rs2);
                                    closePstmt(pstmt2);
                                }
                            }
                        }                            
                    } 
                    //가맹점정보 미등록
                    else
                    {
                        out.println("pg가맹점 미등록 : " + strCol2 + "<br/>");
                    }
                 
                }
            }
            
            conn.commit();
            
            
            //정산 프로시져 돌림
            
             out.println("<b>파일처리완료!!!</b><br/>");
    }
    catch(Exception ex)
    {
        ex.printStackTrace();
        conn.rollback();
        
        out.println("<b>파일처리오류!!!</b><br/>");
    }
    finally
    {
        closeFileOutputStream(os);
        closeInputStream(in);
        
        closeFileInputStream(fis);
        closeBufferedReader(br);
        
        closeResultSet(rs);
        closeResultSet(rs2);
        closePstmt(pstmt);
        closePstmt(pstmt2);
        closeConnection(conn);
    }

%>        
    </body>
</html>
