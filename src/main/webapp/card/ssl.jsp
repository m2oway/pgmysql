<%@ page contentType="text/html; charset=euc-kr" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../inc/easypay_config.jsp" %><!-- ' 환경설정 파일 include -->

<html>
<head>
<title>KICC EASYPAY7.0 SAMPLE</title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="content-type" content="text/html; charset=euc-kr">
<link href="../css/style.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../js/default.js" type="text/javascript"></script>
<script type="text/javascript">

    /* 입력 자동 Setting */
    function f_init(){
        var frm_pay = document.frm_pay;

        var today = new Date();
        var year  = today.getFullYear();
        var month = today.getMonth() + 1;
        var date  = today.getDate();
        var time  = today.getTime();

        if(parseInt(month) < 10) {
            month = "0" + month;
        }

        if(parseInt(date) < 10) {
            date = "0" + date;
        }

        frm_pay.EP_order_no.value = "ORDER_" + year + month + date + time;   //가맹점주문번호
        frm_pay.EP_user_id.value = "USER_" + time;                           //고객ID
        frm_pay.EP_user_nm.value = "길라임";
        frm_pay.EP_user_mail.value = "test@kicc.co.kr";
        frm_pay.EP_user_phone1.value = "0212344567";
        frm_pay.EP_user_phone2.value = "01012344567";
        frm_pay.EP_user_addr.value = "서울 금천구 가산동 459-9 ";
        frm_pay.EP_product_nm.value = "☆테스트상품☆";
        frm_pay.EP_product_amt.value = "1004";

    }

    function f_submit() {
        var frm_pay = document.frm_pay;

        var bRetVal = false;

        /*  주문정보 확인 */
        if( !frm_pay.EP_order_no.value ) {
            alert("가맹점주문번호를 입력하세요!!");
            frm_pay.EP_order_no.focus();
            return;
        }

        if( !frm_pay.EP_product_amt.value ) {
            alert("상품금액을 입력하세요!!");
            frm_pay.EP_product_amt.focus();
            return;
        }

        if( !frm_pay.EP_user_mail.value ) {
            alert("고객Email을 입력하세요!!");
            frm_pay.EP_user_mail.focus();
            return;
        }
        /* 결제금액 설정 */
        frm_pay.EP_tot_amt.value = frm_pay.EP_card_amt.value = frm_pay.EP_product_amt.value;

        /* 결제 정보 확인 */
        if( !frm_pay.EP_card_txtype.value ) {
            alert("신용카드 처리종류를 선택하세요.!!");
            frm_pay.EP_card_txtype.focus();
            return;
        }

        frm_pay.EP_card_no.value = frm_pay.card_no1.value + frm_pay.card_no2.value + frm_pay.card_no3.value + frm_pay.card_no4.value;
        if ( frm_pay.EP_card_no.value.length < 13 )
         {
            alert("신용카드번호를 입력하세요.!!");
            frm_pay.card_no1.focus();
            return;
        }
        if( !frm_pay.expire_yy.value ) {
            alert("유효기간(년) 선택하세요.!!");
            frm_pay.expire_yy.focus();
            return;
        }

        if( !frm_pay.expire_mm.value ) {
            alert("유효기간(월) 선택하세요.!!");
            frm_pay.expire_mm.focus();
            return;
        }
        frm_pay.EP_expire_date.value = frm_pay.expire_yy.value.substring(2, 4) + frm_pay.expire_mm.value;

        if( frm_pay.EP_card_amt.value < 50000 ) {
            frm_pay.EP_install_period.value = "00";
            frm_pay.EP_noint.value = "00";
        }

        /* 인증구분에 따라 처리 */
        if( frm_pay.EP_cert_type.value == "0" ) {

            /* 카드구분에 따라 처리 */
            if( frm_pay.EP_card_user_type.value == "0" ) {
            	/* 개인 */
        	    if( frm_pay.EP_password.value.length != 2 ) {
        	        alert("비밀번호를 입력하세요.!!");
                    frm_pay.EP_password.focus();
                    return;
                }
                if( frm_pay.EP_auth_value.value.length != 7 ) {
        	        alert("주민등록번호를 하세요.!!");
                    frm_pay.EP_auth_value.focus();
                    return;
                }
            }
            else {
            	/* 법인 */
            	frm_pay.EP_password.value = "  ";
            	if( frm_pay.EP_auth_value.value.length != 10 ) {
        	        alert("사업자등록번호를 입력하세요.!!");
                    frm_pay.EP_auth_value.focus();
                    return;
                }
            }
        }
        if( frm_pay.EP_tax_flg.value == "TG01" )
        {
			if( !frm_pay.EP_com_tax_amt.value ) {
	            alert("과세 승인 금액을 입력하세요.!!");
	            frm_pay.EP_com_tax_amt.focus();
	            return;
	        }

	        if( !frm_pay.EP_com_free_amt.value ) {
	            alert("비과세 승인 금액을 입력하세요.!!");
	            frm_pay.EP_com_free_amt.focus();
	            return;
	        }

	        if( !frm_pay.EP_com_vat_amt.value ) {
	            alert("부가세 금액을 입력하세요.!!");
	            frm_pay.EP_com_vat_amt.focus();
	            return;
	        }
        }
        
        bRetVal = true;
        if ( bRetVal ) frm_pay.submit();
    }
</script>
</head>
<body onload="f_init();">
<form name="frm_pay" method="post" action="../easypay_request.jsp">

<!-- 거래구분(수정불가) -->
<input type="hidden" name="EP_tr_cd" value="00101000">
<!-- 결제수단(수정불가) -->
<input type="hidden" name="EP_pay_type" value="card">

<!-- 결제총금액 -->
<input type="hidden" name="EP_tot_amt" value="">
<!-- 통화코드 : 00(원), 01(달러)-->
<input type="hidden" name="EP_currency" value="00">
<!-- 에스크로여부(수정불가) -->
<input type="hidden" name="EP_escrow_yn" value="N">
<!-- 복합결제여부(수정불가) -->
<input type="hidden" name="EP_complex_yn" value="N">

<!-- 카드결제종류(수정불가) -->
<input type="hidden" name="EP_req_type" value="0">
<!-- 신용카드 결제금액 -->
<input type="hidden" name="EP_card_amt" value="">
<!-- 신용카드 WCC(수정불가) -->
<input type="hidden" name="EP_wcc" value="@">
<!-- 신용카드번호 -->
<input type="hidden" name="EP_card_no" value="">
<!-- 유효기간 -->
<input type="hidden" name="EP_expire_date" value="">

<!-- 가맹점 필드 -->
<input type="hidden" name="EP_user_define1" value="">
<input type="hidden" name="EP_user_define2" value="">
<input type="hidden" name="EP_user_define3" value="">
<input type="hidden" name="EP_user_define4" value="">
<input type="hidden" name="EP_user_define5" value="">
<input type="hidden" name="EP_user_define6" value="">

<table border="0" width="910" cellpadding="10" cellspacing="0">
<tr>
    <td>
    <!-- title start -->
	<table border="0" width="900" cellpadding="0" cellspacing="0">
	<tr>
		<td height="30" bgcolor="#FFFFFF" align="left">&nbsp;<img src="../img/arow3.gif" border="0" align="absmiddle">&nbsp;일반 > <b>KEY-IN</td>
	</tr>
	<tr>
		<td height="2" bgcolor="#2D4677"></td>
	</tr>
	</table>
	<!-- title end -->

    <!-- card start -->
    <table border="0" width="900" cellpadding="0" cellspacing="0">
    <tr>
        <td height="30" bgcolor="#FFFFFF">&nbsp;<img src="../img/arow2.gif" border="0" align="absmiddle">&nbsp;<b>신용카드정보</b>(*필수)</td>
    </tr>
    </table>

    <table border="0" width="900" cellpadding="0" cellspacing="1" bgcolor="#DCDCDC">
    <tr height="25">
        <td bgcolor="#EDEDED" width="150">&nbsp;*처리종류</td>
        <td bgcolor="#FFFFFF" width="300">&nbsp;<select name="EP_card_txtype" class="input_F">
        	<option value="" >선택</option>
        	<option value="20" selected >승인</option>
        	</select>
        </td>
        <td bgcolor="#EDEDED" width="150">&nbsp;*인증구분</td>
        <td bgcolor="#FFFFFF" width="300">&nbsp;<select name="EP_cert_type" class="input_F">
        	<option value="0" selected>인증</option>
        	<option value="1" >비인증</option>
        	</select>
        </td>
    </tr>
    <tr height="25">
    	<td bgcolor="#EDEDED" width="150">&nbsp;*카드번호</td>
        <td bgcolor="#FFFFFF" width="750" colspan="3">&nbsp;<input type="text" name="card_no1" value="" size="5" maxlength="4" class="input_F">
        &nbsp;<input type="text" name="card_no2" value="" size="5" maxlength="4" class="input_F">
        &nbsp;<input type="text" name="card_no3" value="" size="5" maxlength="4" class="input_F">
        &nbsp;<input type="text" name="card_no4" value="" size="5" maxlength="4" class="input_F">
        </td>
    </tr>
    <tr height="25">
        <td bgcolor="#EDEDED" width="150">&nbsp;*유효기간</td>
        <td bgcolor="#FFFFFF" width="300" colspan="3">&nbsp;<select name="expire_yy" class="input_F">
        	<option value="" selected>선택</option>
            <option value="2011">2011</option>
            <option value="2012">2012</option>
            <option value="2013">2013</option>
            <option value="2014">2014</option>
            <option value="2015">2015</option>
            <option value="2016">2016</option>
            <option value="2017">2017</option>
            <option value="2018">2018</option>
            <option value="2019">2019</option>
            <option value="2020">2020</option>
        </select>&nbsp;년
        &nbsp;<select name="expire_mm" class="input_F">
            <option value="" selected>선택</option>
            <option value="01">01</option>
            <option value="02">02</option>
            <option value="03">03</option>
            <option value="04">04</option>
            <option value="05">05</option>
            <option value="06">06</option>
            <option value="07">07</option>
            <option value="08">08</option>
            <option value="09">09</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
        </select>&nbsp;월
        </td>
    </tr>
    <tr height="25">
        <td bgcolor="#EDEDED" width="150">&nbsp;*할부개월</td>
        <td bgcolor="#FFFFFF" width="300">&nbsp;<select name="EP_install_period" class="input_F">
            <option value="00" selected>일시불</option>
            <option value="02">2개월</option>
            <option value="03">3개월</option>
            <option value="04">4개월</option>
            <option value="05">5개월</option>
            <option value="06">6개월</option>
            <option value="07">7개월</option>
            <option value="08">8개월</option>
            <option value="09">9개월</option>
            <option value="10">10개월</option>
            <option value="11">11개월</option>
            <option value="12">12개월</option>
        </select></td>
        <td bgcolor="#EDEDED" width="150">&nbsp;*무이자여부</td>
        <td bgcolor="#FFFFFF" width="300">&nbsp;<select name="EP_noint" class="input_F">
            <option value="00" selected>일반</option>
            <option value="02">무이자</option>
        </select></td>
    </tr>
    <tr height="25">
        <td bgcolor="#EDEDED" width="150">&nbsp;카드구분</td>
        <td bgcolor="#FFFFFF" width="300" colspan="3">&nbsp;<select name="EP_card_user_type" class="input_A">
            <option value="0" selected>개인</option>
            <option value="1">법인</option>
        </select></td>
    </tr>
    <tr height="25">
        <td bgcolor="#EDEDED" width="150">&nbsp;비밀번호</td>
        <td bgcolor="#FFFFFF" width="300" colspan="3">&nbsp;<input type="password" name="EP_password" value="" size="4" maxlength="2" class="input_A">&nbsp;**(앞2자리)</td>
    </tr>
    <tr height="25">
        <td bgcolor="#EDEDED" width="150">&nbsp;주민(사업자)등록번호</td>
        <td bgcolor="#FFFFFF" width="750"  colspan="3">&nbsp;<input type="text" name="EP_auth_value" value="" size="15" class="input_A">&nbsp;주민등록번호(뒤7자리), 사업자번호(10자리)</td>
    </tr>
    </table>
    <!-- card end -->

    <!-- order start -->
    <table border="0" width="900" cellpadding="0" cellspacing="0">
    <tr>
        <td height="30" bgcolor="#FFFFFF">&nbsp;<img src="../img/arow2.gif" border="0" align="absmiddle">&nbsp;<b>주문정보</b>(*필수)</td>
    </tr>
    </table>
    <table border="0" width="900" cellpadding="0" cellspacing="1" bgcolor="#DCDCDC">
    <tr height="25">
        <td bgcolor="#EDEDED" width="150">&nbsp;*주문번호</td>
        <td bgcolor="#FFFFFF" width="300">&nbsp;<input type="text" name="EP_order_no" size="50" class="input_F"></td>
        <td bgcolor="#EDEDED" width="150">&nbsp;사용자구분</td>
        <td bgcolor="#FFFFFF" width="300">&nbsp;<select name="EP_user_type" class="input_A">
            <option value="1">일반</option>
            <option value="2" selected>회원</option>
        </select>
        </td>
    </tr>
    <tr height="25">
        <td bgcolor="#EDEDED" width="150">&nbsp;고객ID</td>
        <td bgcolor="#FFFFFF" width="300">&nbsp;<input type="text" name="EP_user_id" size="50" class="input_A"></td>
        <td bgcolor="#EDEDED" width="150">&nbsp;고객명</td>
        <td bgcolor="#FFFFFF" width="300">&nbsp;<input type="text" name="EP_user_nm" size="50" class="input_A"></td>
    </tr>
    <tr height="25">
        <td bgcolor="#EDEDED" width="150">&nbsp;고객Email</td>
        <td bgcolor="#FFFFFF" width="300">&nbsp;<input type="text" name="EP_user_mail" size="50" class="input_A"></td>
        <td bgcolor="#EDEDED" width="150">&nbsp;고객전화번호</td>
        <td bgcolor="#FFFFFF" width="300">&nbsp;<input type="text" name="EP_user_phone1" size="50" class="input_A"></td>
    </tr>
    <tr height="25">
        <td bgcolor="#EDEDED" width="150">&nbsp;*고객휴대폰</td>
        <td bgcolor="#FFFFFF" width="750" colspan="3">&nbsp;<input type="text" name="EP_user_phone2" size="50" class="input_F"></td>
    </tr>
    <tr height="25">
        <td bgcolor="#EDEDED" width="150">&nbsp;고객주소</td>
        <td bgcolor="#FFFFFF" width="750" colspan="3">&nbsp;<input type="text" name="EP_user_addr" size="100" class="input_A"></td>
    </tr>
    <tr height="25">
        <td bgcolor="#EDEDED" width="150">&nbsp;*상품명</td>
        <td bgcolor="#FFFFFF" width="300">&nbsp;<input type="text" name="EP_product_nm" size="50" class="input_F"></td>
        <td bgcolor="#EDEDED" width="150">&nbsp;*상품금액</td>
        <td bgcolor="#FFFFFF" width="300">&nbsp;<input type="text" name="EP_product_amt" size="50" class="input_F"></td>
    </tr>
    <tr height="25">
        <td bgcolor="#EDEDED" width="150">&nbsp;상품구분</td>
        <td bgcolor="#FFFFFF" width="750" colspan="3">&nbsp;<select name="EP_product_type" class="input_A">
            <option value="0" selected>실물</option>
            <option value="1">컨텐츠</option>
        </select></td>
    </tr>
    <tr height="25">
        <td bgcolor="#EDEDED" width="150">&nbsp;과세구분</td>
        <td bgcolor="#FFFFFF" width="300">&nbsp;<select name="EP_tax_flg" class="input_A">
	    <option value="" selected>일반</option>
            <option value="TG01">복합과세</option>
        </select></td>
        <td bgcolor="#EDEDED" width="150">&nbsp;과세 승인금액</td>
        <td bgcolor="#FFFFFF" width="300">&nbsp;<input type="text" name="EP_com_tax_amt" size="50" class="input_F"></td>
    </tr>
    <tr height="25">
        <td bgcolor="#EDEDED" width="150">&nbsp;비과세 승인금액</td>
        <td bgcolor="#FFFFFF" width="300">&nbsp;<input type="text" name="EP_com_free_amt" size="50" class="input_F"></td>
        <td bgcolor="#EDEDED" width="150">&nbsp;부가세 금액</td>
        <td bgcolor="#FFFFFF" width="300">&nbsp;<input type="text" name="EP_com_vat_amt" size="50" class="input_F"></td>
    </tr>
    </table>
    <!-- order Data END -->


    <table border="0" width="900" cellpadding="0" cellspacing="0">
    <tr>
        <td height="30" align="center" bgcolor="#FFFFFF"><input type="button" value="결 제" class="input_D" style="cursor:hand;" onclick="javascript:f_submit();"></td>
    </tr>
    </table>
    </td>
</tr>
</table>
</form>
</body>
</html>