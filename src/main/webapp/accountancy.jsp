<%-- 
    Document   : middepositMasterList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>입금대사 메인</title>             
        </head>
        <body>
</c:if>
       
    <body>
        <div class="right" id="middepositMaster_search" style="width:100%;">       
            <form id="middepositMasterForm" method="POST" onsubmit="return false" action="<c:url value="/middeposit/middepositMasterList" />" modelAttribute="middepositFormBean"> 
                <table>
                    <tr>
                        <td>
                            <div class="label">입금일자</div>
                            <div class="input">
                                <input type="text" name="deposit_start_dt" id="middepositMaster_from_date" value ="" onclick="" /> ~
                                <input type="text" name="deposit_end_dt" id="middepositMaster_to_date" value ="" onclick="" />
                            </div>
                            <div class="label">승인일자</div>
                            <div class="input">
                                <input type="text" name="app_start_dt" id="appMaster_from_date" value ="" onclick="" /> ~
                                <input type="text" name="app_end_dt" id="appMaster_to_date" value ="" onclick="" />
                            </div>                            
                            <div class="label2">가맹점번호구분</div>
                            <div class="input">
                                <select name="pay_mtd">
                                    <option value="">전체</option>
                                        <option value="KICC_PG">KICC_PG</option>                                    
                                </select>
                            </div>
                            <div class="label">가맹점번호</div>
                            <div class="input"><input type="text" name="merch_no" /></div> 
                        </td>
                        <td><input type="button" name="middepositMaster_search" value="조회"/></td>
                        <td><input type="button" name="middepositMaster_excel" value="엑셀"/></td>
                        <td><input type="button" name="init" value="검색조건삭제"/></td>
                    </tr>
               </table>
            </form>
        </div>
        <div id="middepositMaster" style="position: relative;width:100%;height:100%;background-color:white;"></div>
        
         <script type="text/javascript">
            
            var middepositMastergrid={};
            var middepositMasterCalendar;
            var outamtWindows={};
            var depositMetabolismWindow;
            
            $(document).ready(function () {
                
                var middepositMaster_searchForm = document.getElementById("middepositMaster_search");
                
                var tabBarId = tabbar.getActiveTab();
                middepositMaster_layout = tabbar.cells(tabBarId).attachLayout("2E");
                     
                middepositMaster_layout.cells('a').hideHeader();
                middepositMaster_layout.cells('b').hideHeader();
                 
                middepositMaster_layout.cells('a').attachObject(middepositMaster_searchForm);
                middepositMaster_layout.cells('a').setHeight(20);
                
                middepositMastergrid = middepositMaster_layout.cells('b').attachGrid();
                
                middepositMastergrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                
                var middepositMasterHeaders = "";
                middepositMasterHeaders += "<center>입금일</center>,<center>승인일</center>,<center>가맹점번호구분</center>,<center>가맹점번호</center>,<center>매입확정액차이</center>,<center>매입확정금액</center>,<center>입금예정액</center>,<center>수수료</center>";
                middepositMasterHeaders += ",<center>총거래금액</center>,<center>승인거래액</center>,<center>취소거래액</center>";
                
                middepositMastergrid.setHeader(middepositMasterHeaders);
                middepositMastergrid.setColAlign("center,center,center,center,right,right,right,right,right,right,right");
                middepositMastergrid.setColTypes("txt,txt,txt,txt,edn,edn,edn,edn,edn,edn,edn");
                middepositMastergrid.setInitWidths("100,100,100,100,100,100,100,100,100,100,100");
                middepositMastergrid.setColSorting("str,str,str,str,int,int,int,int,int,int,int");
                middepositMastergrid.setNumberFormat("0,000",4);
                middepositMastergrid.setNumberFormat("0,000",5);
                middepositMastergrid.setNumberFormat("0,000",6);
                middepositMastergrid.setNumberFormat("0,000",8);
                middepositMastergrid.setNumberFormat("0,000",9);
                middepositMastergrid.setNumberFormat("0,000",10);
                middepositMastergrid.setNumberFormat("0,000",11);
                middepositMastergrid.enableColumnMove(true);
                middepositMastergrid.setSkin("dhx_skyblue");
                middepositMastergrid.init();
                middepositMastergrid.attachEvent("onRowDblClicked", middepositMaster_attach);
                //middepositMastergrid.parse(${ls_middepositMasterList},"json");
                
                depositMetabolismWindow = new dhtmlXWindows();
                
                //검색조건 초기화
                $("#middepositMasterForm input[name=init]").click(function () {

                    middepositMasterInit($("#middepositMasterForm"));

                });  
                
                $("#middepositMasterForm input[name=week]").click(function(){
                    midDeposit_date_search("week");
                });

                $("#middepositMasterForm input[name=month]").click(function(){
                    midDeposit_date_search("month");
                });


                //검색 이벤트
                $("#middepositMasterForm input[name=middepositMaster_search]").click(function () {
                    middepositMasterSearch();
                });
                
                //엑셀
                $("#middepositMasterForm input[name=middepositMaster_excel]").click(function () {
                    $("#middepositMasterForm").attr("action","<c:url value="/middeposit/middepositMasterListExcel" />");
                    document.getElementById("middepositMasterForm").submit();
                });                
                
                middepositMasterCalendar = new dhtmlXCalendarObject(["middepositMaster_from_date","middepositMaster_to_date","appMaster_from_date","appMaster_to_date"]);
                
                    
            });
            
            function middepositMasterInit($form) {
                searchFormInit($form);
                midDeposit_date_search("week");                    
            } 
            
            //상세조회 이벤트
            function middepositMaster_attach(rowid, col) {

              var deposit_dt = middepositMastergrid.getUserData(rowid, 'deposit_dt');
              var merch_no = middepositMastergrid.getUserData(rowid, 'merch_no');
              
              main_middepositMaster_layout.cells('b').attachURL("<c:url value="/middeposit/middepositDetailList" />"+"?deposit_dt=" + deposit_dt + "&merch_no="+merch_no+"&page_size=100", true);
              
            }
            
            //검색
            function middepositMasterSearch() {
                
                $.ajax({
                    url: "<c:url value="/middeposit/middepositMasterList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#middepositMasterForm").serialize(),
                    success: function (data) {
                        
                        var jsonData = $.parseJSON(data);
                        middepositMastergrid.clearAll();
                        middepositMastergrid.parse(jsonData,"json");
                        
                        $("button.procButton").button({
                            text:true
                        })
                        .unbind("click")
                        .bind("click", function (e) {

                            if (confirm("입금확정을 하시겠습니까?") == true){
                                var rowId = getRowId($(this));

                                depositMetabolismConfirmProcess(rowId);
                            }

                        });


                        $("button.procCancelButton").button({
                            text:true
                        })
                        .unbind("click")
                        .bind("click", function (e) {

                            if (confirm("확정취소를 하시겠습니까?") == true){
                                var rowId = getRowId($(this));

                                depositMetabolismCancelProcess(rowId);
                            }

                        });


                        $("span.outAmt,span.outAmtProc").click(function(){

                            var rowId = getRowId($(this));

                            depositMetabolismOutAmtWindowPop(rowId);

                        });

                        $("span.misAmt").click(function(){

                            var rowId = getRowId($(this));

                            depositMetabolismMisAmtWindowPop(rowId);

                        });                                 
                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }
            
            function midDeposit_date_search(day){
                var oneDate = 1000 * 3600 * 24; 
                var now = new Date();
                var week = new Date(now.getTime() + (oneDate*-6));
                var month = new Date(now.getTime() + (oneDate*-31));
                var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                var date=new Date();
                if(day=="today"){
                    this.$("input[id=middepositMaster_from_date]").val(nowTime);
                    this.$("input[id=middepositMaster_to_date]").val(nowTime);
                }else if(day=="week"){
                    this.$("input[id=middepositMaster_from_date]").val(weekTime);
                    this.$("input[id=middepositMaster_to_date]").val(nowTime);
                }else{
                    this.$("input[id=middepositMaster_from_date]").val(monthTime);
                    this.$("input[id=middepositMaster_to_date]").val(nowTime);
                }
            }
            
            function midDepositListsetSens(id, k) {
                // update range
                if (k == "min") {
                    middepositMasterCalendar.setSensitiveRange(byId(id).value, null);
                } else {
                    middepositMasterCalendar.setSensitiveRange(null, byId(id).value);
                }
            }

            function byId(id) {
                return document.getElementById(id);
            }              
            
            function depositMetabolismOutAmtWindowPop(rowId){

                var dhtmlxwindowSize = {};
                dhtmlxwindowSize.width = 1000;
                dhtmlxwindowSize.height = 300;

                var pay_mtd = middepositMastergrid.getUserData(rowId,"pay_mtd");
                var merch_no = middepositMastergrid.getUserData(rowId,"merch_no");
                var deposit_dt = middepositMastergrid.getUserData(rowId,"deposit_dt");

                var location = dhxwindowCenterAlgin(dhtmlxwindowSize);

                outamtWindows = depositMetabolismWindow.createWindow("outamtListWindow", location.x, location.y, dhtmlxwindowSize.width, dhtmlxwindowSize.height);

                outamtWindows.setText("미수금 리스트");

                depositMetabolismWindow.window("outamtListWindow").setModal(true);

                outamtWindows.attachURL("<c:url value="/outamt/outamtDetailListWindow" />" + "?pay_mtd="+pay_mtd+"&merch_no="+merch_no+"&gen_dt="+deposit_dt+"&deposit_popup_yn=Y",true);

            }
            
            function depositMetabolismMisAmtWindowPop(rowId){
                
                var dhtmlxwindowSize = {};
                dhtmlxwindowSize.width = 950;
                dhtmlxwindowSize.height = 250;

                var pay_mtd = middepositMastergrid.getUserData(rowId,"pay_mtd");
                var merch_no = middepositMastergrid.getUserData(rowId,"merch_no");
                var deposit_dt = middepositMastergrid.getUserData(rowId,"deposit_dt");

                var location = dhxwindowCenterAlgin(dhtmlxwindowSize);

                outamtWindows = depositMetabolismWindow.createWindow("outamtListWindow", location.x, location.y, dhtmlxwindowSize.width, dhtmlxwindowSize.height);

                outamtWindows.setText("미수금 등록");

                depositMetabolismWindow.window("outamtListWindow").setModal(true);

                outamtWindows.attachURL("<c:url value="/outamt/outamtMasterInsert" />" + "?pay_mtd="+pay_mtd+"&merch_no="+merch_no+"&gen_dt="+deposit_dt+"&deposit_popup_yn=Y",true);

            }                      

            function depositMetabolismConfirmProcess(rowId){
                
                var deposit_object ={};
                deposit_object.seq = middepositMastergrid.getUserData(rowId,"seq");
                deposit_object.deposit_dt = middepositMastergrid.getUserData(rowId,"deposit_dt");
                deposit_object.merch_no = middepositMastergrid.getUserData(rowId,"merch_no");
                deposit_object.proc_flag = middepositMastergrid.getUserData(rowId,"proc_flag");                
                
                $.ajax({
                    url : "<c:url value="/middeposit/depositMetabolismConfirmProcess" />",
                    type : "POST",
                    async : true,
                    dataType : "json",
                    data: {
                             seq:deposit_object.seq
                            ,deposit_dt:deposit_object.deposit_dt
                            ,merch_no:deposit_object.merch_no
                            ,proc_flag:deposit_object.proc_flag
                    },
                    success : function(data) {
                        if(data ==="success"){
                            alert("입금확정 성공");

                            middepositMastergrid.clearAll();
                            middepositMasterSearch();

                        }else{
                            alert("입금확정 실패");
                        }
                    },
                    error : function(jqXHR, textStatus, errorThrown) {
                        alert("입금확정 실패 : " + errorThrown);

                    }
                });

            }

            function depositMetabolismCancelProcess(rowId){
            
                var deposit_object ={};
                deposit_object.seq = middepositMastergrid.getUserData(rowId,"seq");
                deposit_object.deposit_dt = middepositMastergrid.getUserData(rowId,"deposit_dt");
                deposit_object.merch_no = middepositMastergrid.getUserData(rowId,"merch_no");
                deposit_object.proc_flag = middepositMastergrid.getUserData(rowId,"proc_flag");                

                $.ajax({
                    url : "<c:url value="/middeposit/depositMetabolismCancelProcess" />",
                    type : "POST",
                    async : true,
                    dataType : "json",
                    data: {
                            seq:deposit_object.seq
                           ,deposit_dt:deposit_object.deposit_dt
                           ,merch_no:deposit_object.merch_no
                           ,proc_flag:deposit_object.proc_flag
                    },
                    success : function(data) {
                        if(data ==="success"){
                            alert("확정취소 성공");
                            middepositMastergrid.clearAll();
                            middepositMasterSearch();
                        }else{
                            alert("확정취소 실패");
                        }
                    },
                    error : function(jqXHR, textStatus, errorThrown) {
                        alert("확정취소 실패 : " + errorThrown);

                    }
                });

            }         
            
            function getRowId($tag){
                var rowId = 0;

                if(middepositMastergrid.pagingOn){
                    rowId =Number((middepositMastergrid.currentPage -1) * Number(middepositMastergrid.rowsBufferOutSize))+Number($tag.parent().parent().index()-1);
                }else{
                    rowId = Number($tag.parent().parent().index()-1);
                }
                return rowId;
            }            
                
        </script>
    </body>
</html>
