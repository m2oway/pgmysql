<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<%@ page contentType="text/html; charset=euc-kr" %>
<%@page import="java.security.Security"%>
<%@ include file="./inc/easypay_config.jsp" %>
<%--
    /*****************************************************************************
     * Easypay 기본 설정정보를 include한다
     ****************************************************************************/
--%>
<%@ page import="com.kicc.*" %>
<%@ page import="com.kicc.common.Util"%>
<%@ page import="java.util.Enumeration"%>
<%@ page import="java.net.URLEncoder"%>
<%!
    /**
     * 파라미터 체크 메소드
     */
    public String getNullToSpace(String param) {
        return (param == null) ? "" : param.trim();
    }
%>
<%
    /* -------------------------------------------------------------------------- */
    /* ::: 처리구분 설정                                                          */
    /* -------------------------------------------------------------------------- */
    final String TRAN_CD_NOR_PAYMENT    = "00101000";   // 승인(일반, 에스크로)
    final String TRAN_CD_NOR_MGR        = "00201000";   // 변경(일반, 에스크로)

    String tr_cd            = getNullToSpace(request.getParameter("EP_tr_cd"));           // [필수]요청구분
    String pay_type         = getNullToSpace(request.getParameter("EP_pay_type"));        // [필수]결제수단

    /* -------------------------------------------------------------------------- */
    /* ::: 신용카드 정보 설정                                                     */
    /* -------------------------------------------------------------------------- */
    String card_amt          = getNullToSpace(request.getParameter("EP_card_amt"));        // [필수]신용카드 결제금액
    String card_txtype       = getNullToSpace(request.getParameter("EP_card_txtype"));     // [필수]처리종류
    String req_type          = getNullToSpace(request.getParameter("EP_req_type"));        // [필수]카드결제종류
    String cert_type         = getNullToSpace(request.getParameter("EP_cert_type"));       // [필수]인증여부
    String wcc               = getNullToSpace(request.getParameter("EP_wcc"));             // [필수]wcc
    String card_no           = getNullToSpace(request.getParameter("EP_card_no"));         // [필수]카드번호
    String expire_date       = getNullToSpace(request.getParameter("EP_expire_date"));     // [필수]유효기간
    String install_period    = getNullToSpace(request.getParameter("EP_install_period"));  // [필수]할부개월
    String noint             = getNullToSpace(request.getParameter("EP_noint"));           // [필수]무이자여부
    String card_user_type    = getNullToSpace(request.getParameter("EP_card_user_type"));  // [선택]카드구분 : 인증여부에 따라 필수
    String password          = getNullToSpace(request.getParameter("EP_password"));        // [선택]비밀번호 : 인증여부에 따라 필수
    String auth_value        = getNullToSpace(request.getParameter("EP_auth_value"));      // [선택]인증값   : 인증여부에 따라 필수
    String cavv              = getNullToSpace(request.getParameter("EP_cavv"));            // [선택]CAVV  : 카드결제종류에 따라 필수
    String xid               = getNullToSpace(request.getParameter("EP_xid"));             // [선택]XID   : 카드결제종류에 따라 필수
    String eci               = getNullToSpace(request.getParameter("EP_eci"));             // [선택]ECI   : 카드결제종류에 따라 필수
    String kvp_cardcode      = getNullToSpace(request.getParameter("EP_kvp_cardcode"));    // [선택]KVP_CARDCODE   : 카드결제종류에 따라 필수
    String kvp_sessionkey    = getNullToSpace(request.getParameter("EP_kvp_sessionkey"));  // [선택]KVP_SESSIONKEY : 카드결제종류에 따라 필수
    String kvp_encdata       = getNullToSpace(request.getParameter("EP_kvp_encdata"));     // [선택]KVP_ENCDATA    : 카드결제종류에 따라 필수
    String req_cno           = getNullToSpace(request.getParameter("EP_cno"));             // [선택]은련카드 인증거래번호
    String kvp_pgid          = getNullToSpace(request.getParameter("EP_kvp_pgid"));        // [선택]은련카드 PGID

    kvp_sessionkey = URLEncoder.encode( kvp_sessionkey );
    kvp_encdata = URLEncoder.encode( kvp_encdata );

    /* -------------------------------------------------------------------------- */
    /* ::: 계좌이체 정보 설정                                                     */
    /* -------------------------------------------------------------------------- */
    String acnt_amt          = getNullToSpace(request.getParameter("EP_acnt_amt"));        // [필수]계좌이체 결제금액
    String acnt_txtype       = getNullToSpace(request.getParameter("EP_acnt_txtype"));     // [필수]계좌이체 처리종류
    String kftc_signdata     = getNullToSpace(request.getParameter("hd_pi"));              // [필수]전자지갑 암호문

    /* -------------------------------------------------------------------------- */
    /* ::: 무통장입금 정보 설정                                                   */
    /* -------------------------------------------------------------------------- */
    String vacct_amt         = getNullToSpace(request.getParameter("EP_vacct_amt"));       // [필수]무통장입금 결제금액
    String vacct_txtype      = getNullToSpace(request.getParameter("EP_vacct_txtype"));    // [필수]무통장입금 처리종류
    String bank_cd           = getNullToSpace(request.getParameter("EP_bank_cd"));         // [필수]은행코드
    String vacct_expr_date   = getNullToSpace(request.getParameter("EP_expire_date"));     // [필수]입금만료일
    String vacct_expr_time   = getNullToSpace(request.getParameter("EP_expire_time"));     // [필수]입금만료시간

    /* -------------------------------------------------------------------------- */
    /* ::: 현금영수증 정보 설정                                                   */
    /* -------------------------------------------------------------------------- */
    String cash_yn           = getNullToSpace(request.getParameter("EP_cash_yn"));         // [필수]현금영수증 발행여부
    String cash_issue_type   = getNullToSpace(request.getParameter("EP_cash_issue_type")); // [선택]현금영수증발행용도
    String cash_auth_type    = getNullToSpace(request.getParameter("EP_cash_auth_type"));  // [선택]인증구분
    String cash_auth_value   = getNullToSpace(request.getParameter("EP_cash_auth_value")); // [선택]인증번호

    /* -------------------------------------------------------------------------- */
    /* ::: 휴대폰결제 정보 설정                                                   */
    /* -------------------------------------------------------------------------- */
    String mob_amt           = getNullToSpace(request.getParameter("EP_mob_amt"));         // [필수]휴대폰 결제금액
    String mob_txtype        = getNullToSpace(request.getParameter("EP_mob_txtype"));      // [필수]휴대폰 처리종류
    String mobile_cd         = getNullToSpace(request.getParameter("EP_mobile_cd"));       // [필수]이통사코드
    String mobile_no         = getNullToSpace(request.getParameter("EP_mobile_no"));       // [필수]휴대폰번호
    String jumin_no          = getNullToSpace(request.getParameter("EP_jumin_no"));        // [필수]휴대폰 가입자 주민등록번호
    String mob_auth_id       = getNullToSpace(request.getParameter("EP_mob_auth_id"));     // [필수]인증고유번호(승인 시 필수)
    String mob_billid        = getNullToSpace(request.getParameter("EP_mob_billid"));      // [필수]인증번호(승인 시 필수)
    String mob_cno           = getNullToSpace(request.getParameter("EP_mob_cno"));         // [필수]PG거래번호(승인 시 필수)

    /* -------------------------------------------------------------------------- */
    /* ::: 포인트 결제 정보 설정                                                  */
    /* -------------------------------------------------------------------------- */
    String pnt_amt           = getNullToSpace(request.getParameter("EP_pnt_amt"));         // [필수]포인트 결제금액
    String pnt_txtype        = getNullToSpace(request.getParameter("EP_pnt_txtype"));      // [필수]포인트 처리종류
    String pnt_cp_cd         = getNullToSpace(request.getParameter("EP_pnt_cp_cd"));       // [필수]포인트 서비스사 코드
    String pnt_idno          = getNullToSpace(request.getParameter("EP_pnt_idno"));        // [필수]포인트 ID(카드)번호
    String pnt_pwd           = getNullToSpace(request.getParameter("EP_pnt_pwd"));         // [옵션]포인트 비밀번호

    /* -------------------------------------------------------------------------- */
    /* ::: 결제 주문 정보 설정                                                    */
    /* -------------------------------------------------------------------------- */
    String user_type        = getNullToSpace(request.getParameter("EP_user_type"));        // [선택]사용자구분구분[1:일반,2:회원]
    String order_no         = getNullToSpace(request.getParameter("EP_order_no"));         // [필수]주문번호
    String memb_user_no     = getNullToSpace(request.getParameter("EP_memb_user_no"));     // [선택]가맹점 고객일련번호
    String user_id          = getNullToSpace(request.getParameter("EP_user_id"));          // [선택]고객 ID
    String user_nm          = getNullToSpace(request.getParameter("EP_user_nm"));          // [필수]고객명
    String user_mail        = getNullToSpace(request.getParameter("EP_user_mail"));        // [필수]고객 E-mail
    String user_phone1      = getNullToSpace(request.getParameter("EP_user_phone1"));      // [필수]가맹점 고객 연락처1
    String user_phone2      = getNullToSpace(request.getParameter("EP_user_phone2"));      // [선택]가맹점 고객 연락처2
    String user_addr        = getNullToSpace(request.getParameter("EP_user_addr"));        // [선택]가맹점 고객 주소
    String product_type     = getNullToSpace(request.getParameter("EP_product_type"));     // [필수]상품정보구분[0:실물,1:컨텐츠]
    String product_nm       = getNullToSpace(request.getParameter("EP_product_nm"));       // [필수]상품명
    String product_amt      = getNullToSpace(request.getParameter("EP_product_amt"));      // [필수]상품금액
	String user_define1     = getNullToSpace(request.getParameter("EP_user_define1"));     // [선택]가맹점필드1
	String user_define2     = getNullToSpace(request.getParameter("EP_user_define2"));     // [선택]가맹점필드2
	String user_define3     = getNullToSpace(request.getParameter("EP_user_define3"));     // [선택]가맹점필드3
	String user_define4     = getNullToSpace(request.getParameter("EP_user_define4"));     // [선택]가맹점필드4
	String user_define5     = getNullToSpace(request.getParameter("EP_user_define5"));     // [선택]가맹점필드5
	String user_define6     = getNullToSpace(request.getParameter("EP_user_define6"));     // [선택]가맹점필드6

    /* -------------------------------------------------------------------------- */
    /* ::: 기타정보 설정                                                          */
    /* -------------------------------------------------------------------------- */
    String client_ip        = request.getRemoteAddr();                                     // [필수]결제고객 IP
    String tot_amt          = getNullToSpace(request.getParameter("EP_tot_amt"));          // [필수]결제 총 금액
    String currency         = getNullToSpace(request.getParameter("EP_currency"));         // [필수]통화코드
    String escrow_yn        = getNullToSpace(request.getParameter("EP_escrow_yn"));        // [필수]에스크로여부
    String complex_yn       = getNullToSpace(request.getParameter("EP_complex_yn"));       // [필수]복합결제여부
    String tax_flg          = getNullToSpace(request.getParameter("EP_tax_flg"));          // [필수]과세구분 플래그(TG01:복합과세 승인거래)
    String com_tax_amt      = getNullToSpace(request.getParameter("EP_com_tax_amt"));      // [필수]과세 승인 금액(복합과세 거래 시 필수)
    String com_free_amt     = getNullToSpace(request.getParameter("EP_com_free_amt"));     // [필수]비과세 승인 금액(복합과세 거래 시 필수)
    String com_vat_amt      = getNullToSpace(request.getParameter("EP_com_vat_amt"));      // [필수]부가세 금액(복합과세 거래 시 필수)


    /* -------------------------------------------------------------------------- */
    /* ::: 변경관리 정보 설정                                                     */
    /* -------------------------------------------------------------------------- */
    String mgr_txtype       = getNullToSpace(request.getParameter("mgr_txtype"));          // [필수]거래구분
    String mgr_subtype      = getNullToSpace(request.getParameter("mgr_subtype"));         // [선택]변경세부구분
    String org_cno          = getNullToSpace(request.getParameter("org_cno"));             // [필수]원거래고유번호
    String mgr_amt          = getNullToSpace(request.getParameter("mgr_amt"));             // [선택]부분취소/환불요청 금액
    String mgr_tax_flg      = getNullToSpace(request.getParameter("mgr_tax_flg"));         // [필수]과세구분 플래그(TG01:복합과세 변경거래)
    String mgr_tax_amt      = getNullToSpace(request.getParameter("mgr_tax_amt"));         // [필수]과세부분취소 금액(복합과세 변경 시 필수)
    String mgr_free_amt     = getNullToSpace(request.getParameter("mgr_free_amt"));        // [필수]비과세부분취소 금액(복합과세 변경 시 필수)
    String mgr_vat_amt      = getNullToSpace(request.getParameter("mgr_vat_amt"));         // [필수]부가세 부분취소금액(복합과세 변경 시 필수)
    String mgr_bank_cd      = getNullToSpace(request.getParameter("mgr_bank_cd"));         // [선택]환불계좌 은행코드
    String mgr_account      = getNullToSpace(request.getParameter("mgr_account"));         // [선택]환불계좌 번호
    String mgr_depositor    = getNullToSpace(request.getParameter("mgr_depositor"));       // [선택]환불계좌 예금주명
    String mgr_socno        = getNullToSpace(request.getParameter("mgr_socno"));           // [선택]환불계좌 주민번호
    String mgr_telno        = getNullToSpace(request.getParameter("mgr_telno"));           // [선택]환불고객 연락처
    String deli_cd          = getNullToSpace(request.getParameter("deli_cd"));             // [선택]배송구분[자가:DE01,택배:DE02]
    String deli_corp_cd     = getNullToSpace(request.getParameter("deli_corp_cd"));        // [선택]택배사코드
    String deli_invoice     = getNullToSpace(request.getParameter("deli_invoice"));        // [선택]운송장 번호
    String deli_rcv_nm      = getNullToSpace(request.getParameter("deli_rcv_nm"));         // [선택]수령인 이름
    String deli_rcv_tel     = getNullToSpace(request.getParameter("deli_rcv_tel"));        // [선택]수령인 연락처
    String req_id           = getNullToSpace(request.getParameter("req_id"));              // [선택]가맹점 관리자 로그인 아이디
    String mgr_msg          = getNullToSpace(request.getParameter("mgr_msg"));             // [선택]변경 사유

    /* -------------------------------------------------------------------------- */
    /* ::: 전문                                                                   */
    /* -------------------------------------------------------------------------- */
    String mgr_data    = "";     // 변경정보
    String tx_req_data = "";     // 요청전문

    /* -------------------------------------------------------------------------- */
    /* ::: 결제 결과                                                              */
    /* -------------------------------------------------------------------------- */
    String bDBProc          = "";
    String res_cd           = "";
    String res_msg          = "";
    String r_order_no       = "";
    String r_complex_yn     = "";
    String r_msg_type       = "";     //거래구분
    String r_noti_type	    = "";     //노티구분
    String r_cno            = "";     //PG거래번호
    String r_amount         = "";     //총 결제금액
    String r_auth_no        = "";     //승인번호
    String r_tran_date      = "";     //거래일시
    String r_pnt_auth_no    = "";     //포인트 승인 번호
    String r_pnt_tran_date  = "";     //포인트 승인 일시
    String r_cpon_auth_no   = "";     //쿠폰 승인 번호
    String r_cpon_tran_date = "";     //쿠폰 승인 일시
    String r_card_no        = "";     //카드번호
    String r_issuer_cd      = "";     //발급사코드
    String r_issuer_nm      = "";     //발급사명
    String r_acquirer_cd    = "";     //매입사코드
    String r_acquirer_nm    = "";     //매입사명
    String r_install_period = "";     //할부개월
    String r_noint          = "";     //무이자여부
    String r_bank_cd        = "";     //은행코드
    String r_bank_nm        = "";     //은행명
    String r_account_no     = "";     //계좌번호
    String r_deposit_nm     = "";     //입금자명
    String r_expire_date    = "";     //계좌사용 만료일
    String r_cash_res_cd    = "";     //현금영수증 결과코드
    String r_cash_res_msg   = "";     //현금영수증 결과메세지
    String r_cash_auth_no   = "";     //현금영수증 승인번호
    String r_cash_tran_date = "";     //현금영수증 승인일시
    String r_auth_id        = "";     //PhoneID
    String r_billid         = "";     //인증번호
    String r_mobile_no      = "";     //휴대폰번호
    String r_ars_no         = "";     //ARS 전화번호
    String r_cp_cd          = "";     //포인트사
    String r_used_pnt       = "";     //사용포인트
    String r_remain_pnt     = "";     //잔여한도
    String r_pay_pnt        = "";     //할인/발생포인트
    String r_accrue_pnt     = "";     //누적포인트
    String r_remain_cpon    = "";     //쿠폰잔액
    String r_used_cpon      = "";     //쿠폰 사용금액
    String r_mall_nm        = "";     //제휴사명칭
    String r_escrow_yn	    = "";     //에스크로 사용유무
    String r_canc_acq_data  = "";     //매입취소일시
    String r_canc_date      = "";     //취소일시
    String r_refund_date    = "";     //환불예정일시

    /* -------------------------------------------------------------------------- */
    /* ::: EasyPayClient 인스턴스 생성 [변경불가 !!].                             */
    /* -------------------------------------------------------------------------- */
    EasyPayClient easyPayClient = new EasyPayClient();
    easyPayClient.easypay_setenv_init( g_gw_url, g_gw_port, g_cert_file, g_log_dir );
    easyPayClient.easypay_reqdata_init();

    /* -------------------------------------------------------------------------- */
    /* ::: 승인요청 전문 설정                                                     */
    /* -------------------------------------------------------------------------- */
    if( TRAN_CD_NOR_PAYMENT.equals(tr_cd) ) {

        // 승인요청 전문 설정
	// 결제 주문 정보 DATA
        int easypay_order_data_item;
        easypay_order_data_item = easyPayClient.easypay_item( "order_data" );

        easyPayClient.easypay_deli_us( easypay_order_data_item, "user_type"		, user_type 	);
        easyPayClient.easypay_deli_us( easypay_order_data_item, "order_no"		, order_no		);
        easyPayClient.easypay_deli_us( easypay_order_data_item, "memb_user_no"	, memb_user_no 	);
        easyPayClient.easypay_deli_us( easypay_order_data_item, "user_id"		, user_id		);
        easyPayClient.easypay_deli_us( easypay_order_data_item, "user_nm"		, user_nm		);
        easyPayClient.easypay_deli_us( easypay_order_data_item, "user_mail"		, user_mail 	);
        easyPayClient.easypay_deli_us( easypay_order_data_item, "user_phone1"	, user_phone1 	);
        easyPayClient.easypay_deli_us( easypay_order_data_item, "user_phone2"	, user_phone2 	);
        easyPayClient.easypay_deli_us( easypay_order_data_item, "user_addr"		, user_addr 	);
        easyPayClient.easypay_deli_us( easypay_order_data_item, "product_type"	, product_type 	);
        easyPayClient.easypay_deli_us( easypay_order_data_item, "product_nm"	, product_nm 	);
        easyPayClient.easypay_deli_us( easypay_order_data_item, "product_amt"	, product_amt 	);
        easyPayClient.easypay_deli_us( easypay_order_data_item, "user_define1"	, user_define1 	);
        easyPayClient.easypay_deli_us( easypay_order_data_item, "user_define2"	, user_define2 	);
        easyPayClient.easypay_deli_us( easypay_order_data_item, "user_define3"	, user_define3 	);
        easyPayClient.easypay_deli_us( easypay_order_data_item, "user_define4"	, user_define4 	);
        easyPayClient.easypay_deli_us( easypay_order_data_item, "user_define5"	, user_define5 	);
        easyPayClient.easypay_deli_us( easypay_order_data_item, "user_define6"	, user_define6 	);

        // 결제정보 DATA부
        int easypay_pay_data_item;
        easypay_pay_data_item = easyPayClient.easypay_item( "pay_data" );

        // 결제 공통 정보 DATA
        int easypay_common_item;
        easypay_common_item = easyPayClient.easypay_item( "common" );

        easyPayClient.easypay_deli_us( easypay_common_item, "tot_amt"       , tot_amt       );
        easyPayClient.easypay_deli_us( easypay_common_item, "currency"      , currency      );
        easyPayClient.easypay_deli_us( easypay_common_item, "client_ip"     , client_ip     );
        easyPayClient.easypay_deli_us( easypay_common_item, "escrow_yn"     , escrow_yn     );
        easyPayClient.easypay_deli_us( easypay_common_item, "complex_yn"    , complex_yn    );
        easyPayClient.easypay_deli_us( easypay_common_item, "tax_flg"       , tax_flg      );
        easyPayClient.easypay_deli_us( easypay_common_item, "com_tax_amt"   , com_tax_amt  );
        easyPayClient.easypay_deli_us( easypay_common_item, "com_free_amt"  , com_free_amt );
        easyPayClient.easypay_deli_us( easypay_common_item, "com_vat_amt"   , com_vat_amt  );

        if ( !req_cno.equals( "" ) )
            easyPayClient.easypay_deli_us( easypay_common_item, "req_cno", req_cno      );
        easyPayClient.easypay_deli_rs( easypay_pay_data_item , easypay_common_item );

        // 신용카드 결제 DATA SET
        if( pay_type.equals("card") ) {
            int easypay_card_item;

            easypay_card_item = easyPayClient.easypay_item( "card" );

            easyPayClient.easypay_deli_us( easypay_card_item, "card_txtype"    , card_txtype       );
            easyPayClient.easypay_deli_us( easypay_card_item, "req_type"       , req_type          );
            easyPayClient.easypay_deli_us( easypay_card_item, "card_amt"       , card_amt          );
            easyPayClient.easypay_deli_us( easypay_card_item, "noint"          , noint             );
            easyPayClient.easypay_deli_us( easypay_card_item, "cert_type"      , cert_type         );
            easyPayClient.easypay_deli_us( easypay_card_item, "wcc"            , wcc               );
            easyPayClient.easypay_deli_us( easypay_card_item, "install_period" , install_period    );

            if( req_type.equals("0") ) {
                // 일반(SSL)
                easyPayClient.easypay_deli_us( easypay_card_item, "card_no"        , card_no           );
                easyPayClient.easypay_deli_us( easypay_card_item, "expire_date"    , expire_date       );
                easyPayClient.easypay_deli_us( easypay_card_item, "user_type"      , card_user_type    );

                if( cert_type.equals("0") ) {

                    easyPayClient.easypay_deli_us( easypay_card_item, "password"       , password          );
                    easyPayClient.easypay_deli_us( easypay_card_item, "auth_value"     , auth_value        );
                }
            }

            easyPayClient.easypay_deli_rs( easypay_pay_data_item , easypay_card_item );
        }


    /* -------------------------------------------------------------------------- */
    /* ::: 변경관리 요청                                                          */
    /* -------------------------------------------------------------------------- */
    }else if( TRAN_CD_NOR_MGR.equals( tr_cd ) ) {

        int easypay_mgr_data_item;
        easypay_mgr_data_item = easyPayClient.easypay_item( "mgr_data" );

        easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_txtype"		, mgr_txtype 	);
        easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_subtype"		, mgr_subtype 	);
        easyPayClient.easypay_deli_us( easypay_mgr_data_item, "org_cno"			, org_cno 		);
        easyPayClient.easypay_deli_us( easypay_mgr_data_item, "order_no"		, order_no		);
        easyPayClient.easypay_deli_us( easypay_mgr_data_item, "pay_type"		, pay_type		);
        easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_amt"			, mgr_amt		);
        easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_bank_cd"		, mgr_bank_cd 	);
        easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_account"		, mgr_account 	);
        easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_depositor"	, mgr_depositor );
        easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_socno"		, mgr_socno 	);
        easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_telno"		, mgr_telno 	);
        easyPayClient.easypay_deli_us( easypay_mgr_data_item, "deli_cd"			, deli_cd 		);
        easyPayClient.easypay_deli_us( easypay_mgr_data_item, "deli_corp_cd"	, deli_corp_cd 	);
        easyPayClient.easypay_deli_us( easypay_mgr_data_item, "deli_invoice"	, deli_invoice 	);
        easyPayClient.easypay_deli_us( easypay_mgr_data_item, "deli_rcv_nm"		, deli_rcv_nm 	);
        easyPayClient.easypay_deli_us( easypay_mgr_data_item, "deli_rcv_tel"	, deli_rcv_tel 	);
        easyPayClient.easypay_deli_us( easypay_mgr_data_item, "req_ip"			, client_ip		);
        easyPayClient.easypay_deli_us( easypay_mgr_data_item, "req_id"			, req_id        );
        easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_msg"			, mgr_msg		);
    }


    /* -------------------------------------------------------------------------- */
    /* ::: 실행                                                                   */
    /* -------------------------------------------------------------------------- */
    if ( tr_cd.length() > 0 ) {
        easyPayClient.easypay_run( g_mall_id, tr_cd, order_no );

        res_cd = easyPayClient.res_cd;
        res_msg = easyPayClient.res_msg;

    }
    else {
        res_cd  = "M114";
        res_msg = "연동 오류|tr_cd값이 설정되지 않았습니다.";
    }
    /* -------------------------------------------------------------------------- */
    /* ::: 결과 처리                                                              */
    /* -------------------------------------------------------------------------- */
    r_cno             = easyPayClient.easypay_get_res( "cno"             );    // PG거래번호
    r_amount          = easyPayClient.easypay_get_res( "amount"          );    //총 결제금액
    r_order_no        = easyPayClient.easypay_get_res( "order_no"        );    //주문번호
    r_noti_type       = easyPayClient.easypay_get_res( "noti_type"       );    //노티구분
    r_auth_no         = easyPayClient.easypay_get_res( "auth_no"         );    //승인번호
    r_tran_date       = easyPayClient.easypay_get_res( "tran_date"       );    //승인일시
    r_pnt_auth_no     = easyPayClient.easypay_get_res( "pnt_auth_no"     );    //포인트승인번호
    r_pnt_tran_date   = easyPayClient.easypay_get_res( "pnt_tran_date"   );    //포인트승인일시
    r_cpon_auth_no    = easyPayClient.easypay_get_res( "cpon_auth_no"    );    //쿠폰승인번호
    r_cpon_tran_date  = easyPayClient.easypay_get_res( "cpon_tran_date"  );    //쿠폰승인일시
    r_card_no         = easyPayClient.easypay_get_res( "card_no"         );    //카드번호
    r_issuer_cd       = easyPayClient.easypay_get_res( "issuer_cd"       );    //발급사코드
    r_issuer_nm       = easyPayClient.easypay_get_res( "issuer_nm"       );    //발급사명
    r_acquirer_cd     = easyPayClient.easypay_get_res( "acquirer_cd"     );    //매입사코드
    r_acquirer_nm     = easyPayClient.easypay_get_res( "acquirer_nm"     );    //매입사명
    r_install_period  = easyPayClient.easypay_get_res( "install_period"  );    //할부개월
    r_noint           = easyPayClient.easypay_get_res( "noint"           );    //무이자여부
    r_bank_cd         = easyPayClient.easypay_get_res( "bank_cd"         );    //은행코드
    r_bank_nm         = easyPayClient.easypay_get_res( "bank_nm"         );    //은행명
    r_account_no      = easyPayClient.easypay_get_res( "account_no"      );    //계좌번호
    r_deposit_nm      = easyPayClient.easypay_get_res( "deposit_nm"      );    //입금자명
    r_expire_date     = easyPayClient.easypay_get_res( "expire_date"     );    //계좌사용만료일
    r_cash_res_cd     = easyPayClient.easypay_get_res( "cash_res_cd"     );    //현금영수증 결과코드
    r_cash_res_msg    = easyPayClient.easypay_get_res( "cash_res_msg"    );    //현금영수증 결과메세지
    r_cash_auth_no    = easyPayClient.easypay_get_res( "cash_auth_no"    );    //현금영수증 승인번호
    r_cash_tran_date  = easyPayClient.easypay_get_res( "cash_tran_date"  );    //현금영수증 승인일시
    r_auth_id         = easyPayClient.easypay_get_res( "auth_id"         );    //PhoneID
    r_billid          = easyPayClient.easypay_get_res( "billid"          );    //인증번호
    r_mobile_no       = easyPayClient.easypay_get_res( "mobile_no"       );    //휴대폰번호
    r_ars_no          = easyPayClient.easypay_get_res( "ars_no"          );    //전화번호
    r_cp_cd           = easyPayClient.easypay_get_res( "cp_cd"           );    //포인트사/쿠폰사
    r_used_pnt        = easyPayClient.easypay_get_res( "used_pnt"        );    //사용포인트
    r_remain_pnt      = easyPayClient.easypay_get_res( "remain_pnt"      );    //잔여한도
    r_pay_pnt         = easyPayClient.easypay_get_res( "pay_pnt"         );    //할인/발생포인트
    r_accrue_pnt      = easyPayClient.easypay_get_res( "accrue_pnt"      );    //누적포인트
    r_remain_cpon     = easyPayClient.easypay_get_res( "remain_cpon"     );    //쿠폰잔액
    r_used_cpon       = easyPayClient.easypay_get_res( "used_cpon"       );    //쿠폰 사용금액
    r_mall_nm         = easyPayClient.easypay_get_res( "mall_nm"         );    //제휴사명칭
    r_escrow_yn       = easyPayClient.easypay_get_res( "escrow_yn"       );    //에스크로 사용유무
    r_complex_yn      = easyPayClient.easypay_get_res( "complex_yn"      );    //복합결제 유무
    r_canc_acq_data   = easyPayClient.easypay_get_res( "canc_acq_data"   );    //매입취소일시
    r_canc_date       = easyPayClient.easypay_get_res( "canc_date"       );    //취소일시
    r_refund_date     = easyPayClient.easypay_get_res( "refund_date"     );    //환불예정일시
    

    /* -------------------------------------------------------------------------- */
    /* ::: 가맹점 DB 처리                                                         */
    /* -------------------------------------------------------------------------- */
    /* r_amount가 주문DB의 금액과 다를 시 반드시 취소 요청을 하시기 바랍니다.     */
    /* DB 처리 실패 시 취소 처리를 해주시기 바랍니다.                             */
    /* -------------------------------------------------------------------------- */
    if ( res_cd.equals("0000") && TRAN_CD_NOR_PAYMENT.equals( tr_cd ) )
    {
    	bDBProc = "true";     // DB처리 성공 시 "true", 실패 시 "false"
    	if ( bDBProc.equals("false") )
    	{
    	    int easypay_mgr_data_item;

    	    easyPayClient.easypay_reqdata_init();

    	    tr_cd = TRAN_CD_NOR_MGR;
    	    easypay_mgr_data_item = easyPayClient.easypay_item( "mgr_data" );
    	    if ( !r_escrow_yn.equals( "Y" ) )
    	    {
    	        easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_txtype", "40" 	);
    	    }
    	    else
    	    {
    	        easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_txtype",  "61" 	);
    	        easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_subtype", "ES02" );

    	    }
    	    easyPayClient.easypay_deli_us( easypay_mgr_data_item, "org_cno",  r_cno 		);
    	    easyPayClient.easypay_deli_us( easypay_mgr_data_item, "order_no", order_no 		);
    	    easyPayClient.easypay_deli_us( easypay_mgr_data_item, "req_ip",   client_ip  	);
    	    easyPayClient.easypay_deli_us( easypay_mgr_data_item, "req_id",   "MALL_R_TRANS" );
    	    easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_msg",  "DB 처리 실패로 망취소"  );

    	    easyPayClient.easypay_run( g_mall_id, tr_cd, order_no );

    	    res_cd = easyPayClient.res_cd;
    	    res_msg = easyPayClient.res_msg;
    	    r_cno       = easyPayClient.easypay_get_res( "cno"             );    // PG거래번호
    	    r_canc_date = easyPayClient.easypay_get_res( "canc_date"       );    //취소일시
    	}
    }
%>
<html>
<meta name="robots" content="noindex, nofollow">
<script type="text/javascript">
    function f_submit(){
        document.frm.submit();
    }
</script>

<body onload="f_submit();">
<form name="frm" method="post" action="./result.jsp">
    <input type="hidden" name="res_cd"          value="<%=res_cd%>">          <!-- 결과코드 //-->
    <input type="hidden" name="res_msg"         value="<%=res_msg%>">         <!-- 결과메시지 //-->
    <input type="hidden" name="order_no"        value="<%=order_no%>">        <!-- 주문번호 //-->
    <input type="hidden" name="user_nm"         value="<%=user_nm%>">         <!-- 구매자명  //-->
<% if( "0000".equals(res_cd) ) { %>
    <input type="hidden" name="cno"             value="<%=r_cno%>">             <!-- PG거래번호 //-->
    <input type="hidden" name="amount"          value="<%=r_amount%>">          <!-- 총 결제금액 //-->
    <input type="hidden" name="auth_no"         value="<%=r_auth_no%>">         <!-- 승인번호 //-->
    <input type="hidden" name="tran_date"       value="<%=r_tran_date%>">       <!-- 거래일시 //-->
    <input type="hidden" name="pnt_auth_no"     value="<%=r_pnt_auth_no%>">     <!-- 포인트 승인 번호 //-->
    <input type="hidden" name="pnt_tran_date"   value="<%=r_pnt_tran_date%>">   <!-- 포인트 승인 일시 //-->
    <input type="hidden" name="cpon_auth_no"    value="<%=r_cpon_auth_no%>">    <!-- 쿠폰 승인 번호 //-->
    <input type="hidden" name="cpon_tran_date"  value="<%=r_cpon_tran_date%>">  <!-- 쿠폰 승인 일시 //-->
    <input type="hidden" name="card_no"         value="<%=r_card_no%>">         <!-- 카드번호 //-->
    <input type="hidden" name="issuer_cd"       value="<%=r_issuer_cd%>">       <!-- 발급사코드 //-->
    <input type="hidden" name="issuer_nm"       value="<%=r_issuer_nm%>">       <!-- 발급사명 //-->
    <input type="hidden" name="acquirer_cd"     value="<%=r_acquirer_cd%>">     <!-- 매입사코드 //-->
    <input type="hidden" name="acquirer_nm"     value="<%=r_acquirer_nm%>">     <!-- 매입사명 //-->
    <input type="hidden" name="install_period"  value="<%=r_install_period%>">  <!-- 할부개월 //-->
    <input type="hidden" name="noint"           value="<%=r_noint%>">           <!-- 무이자여부 //-->
    <input type="hidden" name="bank_cd"         value="<%=r_bank_cd%>">         <!-- 은행코드 //-->
    <input type="hidden" name="bank_nm"         value="<%=r_bank_nm%>">         <!-- 은행명 //-->
    <input type="hidden" name="account_no"      value="<%=r_account_no%>">      <!-- 계좌번호 //-->
    <input type="hidden" name="deposit_nm"      value="<%=r_deposit_nm%>">      <!-- 입금자명 //-->
    <input type="hidden" name="expire_date"     value="<%=r_expire_date%>">     <!-- 계좌사용만료일시 //-->
    <input type="hidden" name="cash_res_cd"     value="<%=r_cash_res_cd%>">     <!-- 현금영수증 결과코드 //-->
    <input type="hidden" name="cash_res_msg"    value="<%=r_cash_res_msg%>">    <!-- 현금영수증 결과메세지 //-->
    <input type="hidden" name="cash_auth_no"    value="<%=r_cash_auth_no%>">    <!-- 현금영수증 승인번호 //-->
    <input type="hidden" name="cash_tran_date"  value="<%=r_cash_tran_date%>">  <!-- 현금영수증 승인일시 //-->
    <input type="hidden" name="auth_id"         value="<%=r_auth_id%>">         <!-- PhoneID //-->
    <input type="hidden" name="billid"          value="<%=r_billid%>">          <!-- 인증번호 //-->
    <input type="hidden" name="mobile_no"       value="<%=r_mobile_no%>">       <!-- 휴대폰번호 //-->
    <input type="hidden" name="ars_no"          value="<%=r_ars_no%>">          <!-- ARS 전화번호 //-->
    <input type="hidden" name="cp_cd"           value="<%=r_cp_cd%>">           <!-- 포인트사 //-->
    <input type="hidden" name="used_pnt"        value="<%=r_used_pnt%>">        <!-- 사용포인트 //-->
    <input type="hidden" name="remain_pnt"      value="<%=r_remain_pnt%>">      <!-- 잔여한도 //-->
    <input type="hidden" name="pay_pnt"         value="<%=r_pay_pnt%>">         <!-- 할인/발생포인트 //-->
    <input type="hidden" name="accrue_pnt"      value="<%=r_accrue_pnt%>">      <!-- 누적포인트 //-->
    <input type="hidden" name="remain_cpon"     value="<%=r_remain_cpon%>">     <!-- 쿠폰잔액 //-->
    <input type="hidden" name="used_cpon"       value="<%=r_used_cpon%>">       <!-- 쿠폰 사용금액 //-->
    <input type="hidden" name="mall_nm"         value="<%=r_mall_nm%>">         <!-- 제휴사명칭 //-->
    <input type="hidden" name="escrow_yn"       value="<%=r_escrow_yn%>">       <!-- 에스크로 사용유무 //-->
    <input type="hidden" name="complex_yn"      value="<%=r_complex_yn%>">      <!-- 복합결제 유무 //-->
    <input type="hidden" name="canc_acq_data"   value="<%=r_canc_acq_data%>">   <!-- 매입취소일시 //-->
    <input type="hidden" name="canc_date"       value="<%=r_canc_date%>">       <!-- 취소일시 //-->
    <input type="hidden" name="refund_date"     value="<%=r_refund_date%>">     <!-- 환불예정일시 //-->
    <input type="hidden" name="pay_type"        value="<%=pay_type%>">          <!-- 결제수단 //-->

<% } %>
</form>
</body>
</html>
