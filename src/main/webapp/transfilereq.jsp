<%-- 
    Document   : transfilereq
    Created on : 2017. 5. 29, 오후 7:16:00
    Author     : MoonBong
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>거래파일UPLOAD</title>
    </head>
    <body>
        <b>거래내역파일UPLOAD</b></br>
        <form action="./file_process.jsp" method="post" enctype="multipart/form-data">
            <!--이름 : <input type="text" name="name" /> <br/>-->
            가맹점 수수료율 : <input type="text" name="merch_cms" /> <br/>
            Van사 수수료율 : <input type="text" name="van_cms" /> <br/>
            Van사 입금일 : <input type="text" name="van_day" /> <br/>
            파일 : <input type="file" name="file" /> <br/>
            <input type="submit" value="전송">
        </form>
    </body>
</html>
