<%@ page contentType="text/html; charset=euc-kr" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%

	request.setCharacterEncoding("euc-kr");

	String res_cd			= request.getParameter("res_cd")==null?"":request.getParameter("res_cd");
	String res_msg			= request.getParameter("res_msg")==null?"":request.getParameter("res_msg");
	String user_nm			= request.getParameter("res_msg")==null?"":request.getParameter("user_nm");
	String cno				= request.getParameter("cno")==null?"":request.getParameter("cno");
	String amount			= request.getParameter("amount")==null?"":request.getParameter("amount");
	String msg_type			= request.getParameter("msg_type")==null?"":request.getParameter("msg_type");
	String order_no			= request.getParameter("order_no")==null?"":request.getParameter("order_no");
	String noti_type		= request.getParameter("noti_type")==null?"":request.getParameter("noti_type");
	String auth_no			= request.getParameter("auth_no")==null?"":request.getParameter("auth_no");
	String tran_date		= request.getParameter("tran_date")==null?"":request.getParameter("tran_date");
	String pnt_auth_no		= request.getParameter("pnt_auth_no")==null?"":request.getParameter("pnt_auth_no");
	String pnt_tran_date	= request.getParameter("pnt_tran_date")==null?"":request.getParameter("pnt_tran_date");
	String cpon_auth_no		= request.getParameter("cpon_auth_no")==null?"":request.getParameter("cpon_auth_no");
	String cpon_tran_date	= request.getParameter("cpon_tran_date")==null?"":request.getParameter("cpon_tran_date");
	String card_no			= request.getParameter("card_no")==null?"":request.getParameter("card_no");
	String issuer_cd		= request.getParameter("issuer_cd")==null?"":request.getParameter("issuer_cd");
	String issuer_nm		= request.getParameter("issuer_nm")==null?"":request.getParameter("issuer_nm");
	String acquirer_cd		= request.getParameter("acquirer_cd")==null?"":request.getParameter("acquirer_cd");
	String acquirer_nm		= request.getParameter("acquirer_nm")==null?"":request.getParameter("acquirer_nm");
	String install_period	= request.getParameter("install_period")==null?"":request.getParameter("install_period");
	String noint			= request.getParameter("noint")==null?"":request.getParameter("noint");
	String bank_cd			= request.getParameter("bank_cd")==null?"":request.getParameter("bank_cd");
	String bank_nm			= request.getParameter("bank_nm")==null?"":request.getParameter("bank_nm");
	String account_no		= request.getParameter("account_no")==null?"":request.getParameter("account_no");
	String deposit_nm		= request.getParameter("deposit_nm")==null?"":request.getParameter("deposit_nm");
	String expire_date		= request.getParameter("expire_date")==null?"":request.getParameter("expire_date");
	String cash_res_cd		= request.getParameter("cash_res_cd")==null?"":request.getParameter("cash_res_cd");
	String cash_res_msg		= request.getParameter("cash_res_msg")==null?"":request.getParameter("cash_res_msg");
	String cash_auth_no		= request.getParameter("cash_auth_no")==null?"":request.getParameter("cash_auth_no");
	String cash_tran_date	= request.getParameter("cash_tran_date")==null?"":request.getParameter("cash_tran_date");
	String auth_id			= request.getParameter("auth_id")==null?"":request.getParameter("auth_id");
	String billid			= request.getParameter("billid")==null?"":request.getParameter("billid");
	String mobile_no		= request.getParameter("mobile_no")==null?"":request.getParameter("mobile_no");
	String ars_no			= request.getParameter("ars_no")==null?"":request.getParameter("ars_no");
	String cp_cd			= request.getParameter("cp_cd")==null?"":request.getParameter("cp_cd");
	String used_pnt			= request.getParameter("used_pnt")==null?"":request.getParameter("used_pnt");
	String remain_pnt		= request.getParameter("remain_pnt")==null?"":request.getParameter("remain_pnt");
	String pay_pnt			= request.getParameter("pay_pnt")==null?"":request.getParameter("pay_pnt");
	String accrue_pnt		= request.getParameter("accrue_pnt")==null?"":request.getParameter("accrue_pnt");
	String remain_cpon		= request.getParameter("remain_cpon")==null?"":request.getParameter("remain_cpon");
	String used_cpon		= request.getParameter("used_cpon")==null?"":request.getParameter("used_cpon");
	String mall_nm			= request.getParameter("mall_nm")==null?"":request.getParameter("mall_nm");
	String escrow_yn		= request.getParameter("escrow_yn")==null?"":request.getParameter("escrow_yn");
	String complex_yn		= request.getParameter("complex_yn")==null?"":request.getParameter("complex_yn");
	String canc_acq_date	= request.getParameter("canc_acq_data")==null?"":request.getParameter("canc_acq_date");
	String canc_date		= request.getParameter("canc_date")==null?"":request.getParameter("canc_date");
	String refund_date		= request.getParameter("refund_date")==null?"":request.getParameter("refund_date");
	String pay_type			= request.getParameter("pay_type")==null?"":request.getParameter("pay_type");
	
	String deduct_pnt		= request.getParameter("deduct_pnt")==null?"":request.getParameter("deduct_pnt");
	String payback_pnt		= request.getParameter("payback_pnt")==null?"":request.getParameter("payback_pnt");
	
	String bk_pay_yn		= request.getParameter("bk_pay_yn")==null?"":request.getParameter("bk_pay_yn");
	String bk_res_cnt		= request.getParameter("bk_res_cnt")==null?"":request.getParameter("bk_res_cnt");
	
	String bk_res_oid1		= request.getParameter("bk_res_oid1")==null?"":request.getParameter("bk_res_oid1");
	String bk_res_sub_mid1	= request.getParameter("bk_res_sub_mid1")==null?"":request.getParameter("bk_res_sub_mid1");
	String bk_res_cno1		= request.getParameter("bk_res_cno1")==null?"":request.getParameter("bk_res_cno1");
	String bk_res_oid2		= request.getParameter("bk_res_oid2")==null?"":request.getParameter("bk_res_oid2");
	String bk_res_sub_mid2	= request.getParameter("bk_res_sub_mid2")==null?"":request.getParameter("bk_res_sub_mid2");
	String bk_res_cno2		= request.getParameter("bk_res_cno2")==null?"":request.getParameter("bk_res_cno2");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<title>결과</title>
<head>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/main/settlementPaymentCss.css" />"  />
</head>
<body>
<table border="0" width="910" cellpadding="0" cellspacing="0">
<tr>
    <td align="center">
    <table class="gridtable" width="100%" >
            <tr>
        <td colspan="6"  bgcolor="#FFFFFF"><b>*결과</b></td>
    </tr>
	    <tr>
	        <td  class="headcol" width="150">&nbsp;응답코드</td>
	        <td bgcolor="#FFFFFF" width="180">&nbsp;<%=res_cd%></td>
	        <td class="headcol" width="150">&nbsp;응답메시지</td>
	        <td bgcolor="#FFFFFF" width="180">&nbsp;<%=res_msg%></td>
	        <td  class="headcol" width="150">&nbsp;PG거래번호</td>
	        <td bgcolor="#FFFFFF" width="180">&nbsp;<%=cno%></td>
	    </tr>
	    <tr>
	        <td  class="headcol">&nbsp;총 결제금액</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=amount%></td>	        
	        <td  class="headcol">&nbsp;주문번호</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=order_no%></td>
	        <td  class="headcol">&nbsp;구매자명</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=user_nm%></td>
	    </tr>
<!--            
	    <tr>
	        <td  class="headcol">&nbsp;포인트승인번호</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=pnt_auth_no%></td>
	        <td class="headcol">&nbsp;포인트승인일시</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=pnt_tran_date%></td>
	        <td  class="headcol">&nbsp;쿠폰승인번호</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=cpon_auth_no%></td>
	    </tr>
-->
	    <tr>
	        <td  class="headcol">&nbsp;쿠폰승인일시</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=cpon_tran_date%></td>
	        <td class="headcol">&nbsp;카드번호</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=card_no%></td>
	        <td  class="headcol">&nbsp;발급사코드</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=issuer_cd%></td>
	    </tr>
	    <tr>
	        <td  class="headcol">&nbsp;발급사명</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=issuer_nm%></td>
	        <td class="headcol">&nbsp;매입사코드</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=acquirer_cd%></td>
	        <td  class="headcol">&nbsp;매입사명</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=acquirer_nm%></td>
	    </tr>
	    <tr>
	        <td  class="headcol">&nbsp;할부개월</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=install_period%></td>
	        <td class="headcol">&nbsp;무이자여부</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=noint%></td>
	        <td  class="headcol">&nbsp;은행코드</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=bank_cd%></td>
	    </tr>
	    <tr>
	        <td  class="headcol">&nbsp;은행명</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=bank_nm%></td>
	        <td class="headcol">&nbsp;계좌번호</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=account_no%></td>
	        <td  class="headcol">&nbsp;입금자명</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=deposit_nm%></td>
	    </tr>
<!--            
	    <tr>
	        <td  class="headcol">&nbsp;계좌사용만료일</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=expire_date%></td>
	        <td class="headcol">&nbsp;입금통보용 업체 사용영역</td>
	        <td bgcolor="#FFFFFF">&nbsp;</td>
	        <td  class="headcol">&nbsp;현금영수증 결과코드</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=cash_res_cd%></td>
	    </tr>
	    <tr>
	        <td  class="headcol">&nbsp;현금영수증 결과메세지</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=cash_res_msg%></td>
	        <td class="headcol">&nbsp;현금영수증 승인번호</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=cash_auth_no%></td>
	        <td  class="headcol">&nbsp;현금영수증 승인일시</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=cash_tran_date%></td>
	    </tr>
	    <tr>
	        <td  class="headcol">&nbsp;PhoneID</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=auth_id%></td>
	        <td class="headcol">&nbsp;인증번호</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=billid%></td>
	        <td  class="headcol">&nbsp;휴대폰번호</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=mobile_no%></td>
	    </tr>
	    <tr>
	        <td  class="headcol">&nbsp;전화번호</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=ars_no%></td>
	        <td class="headcol">&nbsp;포인트사</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=cp_cd%></td>
	        <td  class="headcol">&nbsp;사용포인트</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=used_pnt%></td>
	    </tr>
	    <tr>
	        <td  class="headcol">&nbsp;잔여한도</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=remain_pnt%></td>
	        <td class="headcol">&nbsp;할인/발생포인트</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=pay_pnt%></td>
	        <td  class="headcol">&nbsp;누적포인트</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=accrue_pnt%></td>
	    </tr>
	    <tr>
	        <td  class="headcol">&nbsp;총차감포인트</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=deduct_pnt%></td>
	        <td class="headcol">&nbsp;payback포인트</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=payback_pnt%></td>
	        <td  class="headcol">&nbsp;</td>
	        <td bgcolor="#FFFFFF">&nbsp;</td>
	    </tr>
	    <tr>
	        <td  class="headcol">&nbsp;쿠폰잔액</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=remain_cpon%></td>
	        <td class="headcol">&nbsp;쿠폰 사용금액</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=used_cpon%></td>
	        <td  class="headcol">&nbsp;제휴사명칭</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=mall_nm%></td>
	    </tr>
	    <tr>
	        <td  class="headcol">&nbsp;에스크로 사용유무</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=escrow_yn%></td>
	        <td class="headcol">&nbsp;복합결제 유무</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=complex_yn%></td>
	        <td  class="headcol">&nbsp;매입취소일시</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=canc_acq_date%></td>
	    </tr>
-->
	    <tr>
	        <td  class="headcol">&nbsp;취소일시</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=canc_date%></td>
	        <td class="headcol">&nbsp;환불예정일시</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=refund_date%></td>
	        <td class="headcol">&nbsp;결제수단</td>
	        <td bgcolor="#FFFFFF">&nbsp;<%=pay_type%></td>
	    </tr>
    </table>
    </td>
</tr>
</table>
</form>
</body>
</html>
