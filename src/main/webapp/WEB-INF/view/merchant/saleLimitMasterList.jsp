<%-- 
    Document   : saleLimitMasterList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>지급ID 결제한도 조회</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">

                var saleLimitMastergrid={};

                $(document).ready(function () {
                    
                    var saleLimitMasterForm = document.getElementById("saleLimitMaster_search");
                    
                    var tabBarId = tabbar.getActiveTab();
                    main_saleLimit_layout = tabbar.cells(tabBarId).attachLayout("1C");
                    saleLimitMaster_layout = main_saleLimit_layout.cells('a').attachLayout("2E");
                    saleLimitMaster_layout.cells('a').hideHeader();
                    saleLimitMaster_layout.cells('b').hideHeader();
                    saleLimitMaster_layout.cells('a').attachObject(saleLimitMasterForm);
                    saleLimitMaster_layout.cells('a').setHeight(60);
                    saleLimitMastergrid = saleLimitMaster_layout.cells('b').attachGrid();
                    saleLimitMastergrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");

                    var saleLimitMasterheaders = "";
                    saleLimitMasterheaders +=  "<center>NO</center>,<center>지급ID번호</center>,<center>지급ID명</center>,<center>결제한도</center>,<center>현재결제액</center>,<center>상태</center>";
                    saleLimitMastergrid.setHeader(saleLimitMasterheaders);
                    var saleLimitMasterColAlign = "";
                    saleLimitMasterColAlign +=  "center,center,center,right,right,center";
                    saleLimitMastergrid.setColAlign(saleLimitMasterColAlign);
                    var saleLimitMasterColTypes = "";
                    saleLimitMasterColTypes +=  "txt,txt,txt,edn,edn,txt";
                    saleLimitMastergrid.setColTypes(saleLimitMasterColTypes);
                    var saleLimitMasterInitWidths = "";
                    saleLimitMasterInitWidths +=   "50,150,150,150,150,150";
                    saleLimitMastergrid.setInitWidths(saleLimitMasterInitWidths);
                    var saleLimitMasterColSorting = "";
                    saleLimitMasterColSorting +=  "str,str,str,int,int,str";
                    saleLimitMastergrid.setColSorting(saleLimitMasterColSorting);
                    saleLimitMastergrid.setNumberFormat("0,000",4);       
                    saleLimitMastergrid.setNumberFormat("0,000",3);

                    saleLimitMastergrid.enableColumnMove(true);
                    saleLimitMastergrid.setSkin("dhx_skyblue");
                    saleLimitMastergrid.enablePaging(true,Number($("#saleLimitMasterForm input[name=page_size]").val()),10,"saleLimitMasterPaging",true,"saleLimitMasterrecinfoArea");
                    saleLimitMastergrid.setPagingSkin("bricks");

                    //페이징 처리
                    saleLimitMastergrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        /*
                        if(lInd !==0){
                            $("#saleLimitMasterForm input[name=page_no]").val(ind);
                            saleLimitMasterSearch();
                        }else{
                            return false;
                        }
                        */
                        
                        if(lInd !==0){
                          $("#saleLimitMasterForm input[name=page_no]").val(ind);             
                           saleLimitMastergrid.clearAll();
                           saleLimitMasterSearch();
                        }else{
                           $("#saleLimitMasterForm input[name=page_no]").val(1);
                            saleLimitMastergrid.clearAll();
                             saleLimitMasterSearch();
                        }                        
                    });

                    saleLimitMastergrid.init();
                    saleLimitMastergrid.parse(${ls_saleLimitMasterList}, "json");
<c:if test="${merchantFormBean.ses_user_cate == '00'}">                         
                    saleLimitMastergrid.attachEvent("onRowSelect", saleLimitMaster_open_attach);
</c:if>                                          
	            var ids = saleLimitMastergrid.getAllRowIds();
	            var ids_arr = ids.split(",");
	            for(var i=0;i<ids_arr.length;i++){
	            	var app_chk_status = saleLimitMastergrid.getUserData(Number(ids_arr[i]), 'app_chk_status');
	            	if(app_chk_status >= '70'){
                            saleLimitMastergrid.setRowColor(Number(ids_arr[i]),"pink");
	            	}	
	            }                    

                    //결제한도 정보 조회
                    $("#saleLimitMasterForm input[name=saleLimitMaster_search]").click(function(){
                        /*
                        $("input[name=page_no]").val("1");

                        saleLimitMastergrid.clearAll();

                        saleLimitMasterSearch();
                        */
                        saleLimitMastergrid.changePage(1);
                        //return false;

                    });
                });
                
<c:if test="${merchantFormBean.ses_user_cate == '00'}">  
            var saleLimitMasterpopWindow;
            function saleLimitMaster_open_attach(rowid, col) {
                
                if(col == '1')
                {
                    saleLimitMasterpopWindow = new dhtmlXWindows();
                    var onffmerchno = saleLimitMastergrid.cells(rowid,1).getValue();
                    w1 = saleLimitMasterpopWindow.createWindow("w1", 150, 150,  800, 700);
                    w1.setText("지급ID 정보 수정");
                    saleLimitMasterpopWindow.window('w1').setModal(true);
                    w1.attachURL("<c:url value="/merchant/merchantMasterUpdate" />" + "?onffmerch_no=" + onffmerchno+"&popupchk=1",true);
                    
                    saleLimitMastergrid.clearSelection();
                }
                
                return false;
                        
            }   
</c:if>              

                //결제한도 정보 조회
                function saleLimitMasterSearch() {
                
                    $.ajax({
                        url: $("#saleLimitMasterForm").attr("action"),
                        type: "POST",
                        async: true,
                        dataType: "json",
                        data: $("#saleLimitMasterForm").serialize(),
                        success: function (data) {

                            var jsonData = $.parseJSON(data);

                            saleLimitMastergrid.parse(jsonData,"json");
                            
                            var ids = saleLimitMastergrid.getAllRowIds();
                            var ids_arr = ids.split(",");
                            for(var i=0;i<ids_arr.length;i++){
                                var app_chk_status = saleLimitMastergrid.getUserData(Number(ids_arr[i]), 'app_chk_status');
                                if(app_chk_status >= '70'){
                                    saleLimitMastergrid.setRowColor(Number(ids_arr[i]),"pink");
                                }	
                            }                                         

                        },
                        error: function () {
                            alert("조회 실패");
                        }
                    });
                        return false;
                }
                
                var dhxSelPopupWins=new dhtmlXWindows();
                /*    
                //ONOFF지급ID번호 팝업 클릭 이벤트
                $("#saleLimitMasterForm input[name=onffmerch_no]").unbind("click").bind("click", function (){

                    onoffMerchSalesSelectPopup();

                }); 
                */
               
               //ONOFF지급ID명 팝업 클릭 이벤트
                $("#saleLimitMasterForm input[name=merch_nm]").unbind("click").bind("click", function (){

                    onoffMerchSalesSelectPopup();

                });   
               
                //검색조건 초기화
                $("#saleLimitMasterForm input[name=init]").click(function () {

                    MerchSalesMasterInit($("#saleLimitMasterForm"));

                }); 
                    
                function onoffMerchSalesSelectPopup(){
                    onoffObject.onffmerch_no = $("#saleLimitMasterForm input[name=onffmerch_no]");
                    onoffObject.merch_nm = $("#saleLimitMasterForm input[name=merch_nm]");
                    //ONOFF지급ID정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("ONOFF지급ID 선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                }              
                function MerchSalesMasterInit($form) {

                    searchFormInit($form);
                } 
                
            </script>

        <div class="right" id="saleLimitMaster_search" style="width:100%;">
             <form id="saleLimitMasterForm" method="POST" action="<c:url value="/merchant/saleLimitMasterList" />" modelAttribute="merchantFormBean">

                <input type="hidden" name="page_size" value="100" >
                <input type="hidden" name="page_no" value="1" >
                
                <table>
                    <tr>
                        <div class="label2">지급ID</div>
                        <div class="input">
                            <input type="text" name="onffmerch_no" value ="${saleLimitFormBean.onffmerch_no}" />
                        </div>                  
                        <div class="label">지급ID명</div>
                        <div class="input">
                            <input type="text" name="merch_nm" value ="${saleLimitFormBean.merch_nm}" />
                        </div>                                                                  
                        <td>
                            <input type="button" name="saleLimitMaster_search" value="조회"/>
                        </td>
                        <td><input type="button" name="init" value="검색조건지우기"/></td>
                        <td>
                            <!--<input type="button" name="saleLimitMaster_excel" value="엑셀"/>-->
                        </td>
                    </tr>
                </table>
                        
            </form>

            <div class="paging">
                <div id="saleLimitMasterPaging" style="width: 50%;"></div>
                <div id="saleLimitMasterrecinfoArea" style="width: 50%;"></div>
            </div>
                                
         </div>

    </body>
</html>
