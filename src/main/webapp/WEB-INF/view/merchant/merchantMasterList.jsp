<%-- 
    Document   : onffmerchinfoMasterList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>지급ID 정보 관리</title>
        </head>
        <body>
</c:if>
        <script type="text/javascript">
                var onffmerchinfoMasterGrid={};
                var onffmerchinfoMasterWindow;
                var onffmerchinfoMasterLayout;
                
                $(document).ready(function () {
                    var onffmerchinfoSearch = document.getElementById("onffmerchinfoSearch");            
                    onffmerchinfoMasterLayout = tabbar.cells("3-2").attachLayout("2E");
                    
                    onffmerchinfoMasterLayout.cells('a').hideHeader();
                    onffmerchinfoMasterLayout.cells('b').hideHeader();
                    onffmerchinfoMasterLayout.cells('a').attachObject(onffmerchinfoSearch);
                    onffmerchinfoMasterLayout.cells('a').setHeight(75);
                    onffmerchinfoMasterGrid = onffmerchinfoMasterLayout.cells('b').attachGrid();
                    onffmerchinfoMasterGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var onffmerchinfoMasterGridheaders = "";
                    //NO,지급ID, 지급ID명, 사업자명,개인법인구분,사업자번호, 법인번호, 대표자, 연락처1, 결제한도, 대리점, 상태
                    onffmerchinfoMasterGridheaders += "<center>NO</center>,<center>지급ID</center>,<center>지급ID명</center>,<center>사업자명</center>";
                    onffmerchinfoMasterGridheaders += ",<center>개인/법인</center>,<center>사업자번호</center>,<center>법인번호</center>,<center>대표자</center>,<center>연락처1</center>,<center>결제한도</center>,<center>대리점</center>,<center>상태</center>";
                    onffmerchinfoMasterGrid.setHeader(onffmerchinfoMasterGridheaders);
                    onffmerchinfoMasterGrid.setColAlign("center,center,center,center,center,center,center,center,center,center,center,center");
                    onffmerchinfoMasterGrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt");
                    onffmerchinfoMasterGrid.setInitWidths("50,150,200,200,80,120,120,100,100,100,100,100");
                    onffmerchinfoMasterGrid.setColSorting("str,str,str,str,str,str,str,str,str,str,str,str");
                    onffmerchinfoMasterGrid.enableColumnMove(true);
                    onffmerchinfoMasterGrid.setSkin("dhx_skyblue");
    
                    //onffmerchinfoMasterGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxToolbar/" />");
                    onffmerchinfoMasterGrid.enablePaging(true,Number($("#onffmerchinfoForm input[name=page_size]").val()),10,"onffmerchinfoMasterListPaging",true,"onffmerchinfoMasterListrecinfoArea");
                    onffmerchinfoMasterGrid.setPagingSkin("bricks");

                    //페이징 처리
                    onffmerchinfoMasterGrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        if(lInd !==0){
                          $("#onffmerchinfoForm input[name=page_no]").val(ind);             
                           onffmerchinfoMasterGrid.clearAll();
                           doSearchMerchantMaster();
                        }else{
                            $("#onffmerchinfoForm input[name=page_no]").val(1);
                            onffmerchinfoMasterGrid.clearAll();
                             doSearchMerchantMaster();
                        }                        
                    });
                    
                    onffmerchinfoMasterGrid.init();
                    onffmerchinfoMasterGrid.parse(${onffmerchinfoMasterList}, "json");
                    onffmerchinfoMasterGrid.attachEvent("onRowDblClicked", doUpdateFormMerchantMaster);
                    
                    onffmerchinfoMasterWindow = new dhtmlXWindows();
                    onffmerchinfoMasterWindow.attachViewportTo("onffmerchinfoList");

                    //버튼 추가
                    $("#onffmerchinfoForm input[name=onffmerchinfo_searchButton]").bind("click",function(e){
                        //가맹점정보 정보 조회 이벤트
                        /*
                        $("#onffmerchinfoForm input[name=page_no]").val("1");
                        onffmerchinfoMasterGrid.clearAll();
                        doSearchMerchantMaster();
                        */
                        onffmerchinfoMasterGrid.changePage(1);
                        return false;
                    });


                    $("#onffmerchinfoForm .addButton").button({
                        icons: {
                            primary: "ui-icon-plusthick"
                        },
                        text: false
                    })
                    .unbind("click")
                    .bind("click",function(e){
                        //정보 등록 이벤트
                        doInsertFormMerchantMaster();
                        return false;
                    });

                    $("#onffmerchinfoForm .deleteButton").button({
                        icons: {
                            primary: "ui-icon-minusthick"
                        },
                        text: false
                    })
                    .unbind("click")
                    .bind("click",function(e){
                        //가맹점정보 정보 삭제 이벤트
                        doDeleteMerchantMaster();
                        return false;
                    });

                });

                //가맹점정보 정보 수정
                function doUpdateFormMerchantMaster(rowid, col){
                    var onffmerchno = onffmerchinfoMasterGrid.getUserData(rowid, 'onffmerch_no');
                    w1 = onffmerchinfoMasterWindow.createWindow("w1", 150, 150,  800, 700);
                    w1.setText("지급ID 정보 수정");
                    onffmerchinfoMasterWindow.window('w1').setModal(true);
                    w1.attachURL("<c:url value="/merchant/merchantMasterUpdate" />" + "?onffmerch_no=" + onffmerchno,true);
                }

                function resizeOption($id){
                    //css 변경 : resize시 자동 변경됨.
                    $id.css("width","100%");
                    $id.css("height","100%");
                }


                //가맹점정보 정보 조회
                function doSearchMerchantMaster(){
                    $.ajax({
                        url : $("#onffmerchinfoForm").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#onffmerchinfoForm").serialize(),
                        success : function(data) {
                            //onffmerchinfoMasterGrid.clearAll();
                            //onffmerchinfoMasterGrid.parse(jQuery.parseJSON(data), "json");
                            var jsonData = $.parseJSON(data);     
                            onffmerchinfoMasterGrid.parse(jsonData,"json");
                            //resizeOption($("#onffmerchinfoList.gridbox"));
                        },
                        error : function() {
                            alert("조회 실패");
                        }
                    });
                }


                //가맹점정보 정보 등록
                function doInsertFormMerchantMaster(){

                    w1 = onffmerchinfoMasterWindow.createWindow("w1", 150, 150, 800, 700);
                    //w1 = onffmerchinfoMasterWindow.createWindow("editwin", 150, 150, 700, 400);
                    w1.setText("지급ID 등록");
                    onffmerchinfoMasterWindow.window('w1').setModal(true);
                    w1.attachURL("<c:url value="/merchant/merchantMasterInsert" />",true);   

                }
                var dhxSelPopupWins;
		dhxSelPopupWins=new dhtmlXWindows();
                
                /*
                //ONOFF지급ID 팝업 클릭 이벤트
                $("#onffmerchinfoForm input[name=onffmerch_no]").unbind("click").bind("click", function (){

                    onoffMerMerchSelectPopup();

                });       
                 $("#onffmerchinfoForm input[name=comp_nm]").click(function () {
                    
                    CompanyMerchSelectWin();
                });
                */
                //검색조건 초기화
                $("#onffmerchinfoForm input[name=init]").click(function () {

                    merchMasterInit($("#onffmerchinfoForm"));

                });  
                
                    
                function onoffMerMerchSelectPopup(){
                    onoffObject.onffmerch_no = $("#onffmerchinfoForm input[name=onffmerch_no]");
                    onoffObject.merch_nm = $("#onffmerchinfoForm input[name=merch_nm]");
                    //ONOFF가맹점정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("지급ID 선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                }
                
               
                function CompanyMerchSelectWin()
                {
                    onoffObject.comp_nm = $("#onffmerchinfoForm input[name=comp_nm]");

                    w2 = dhxSelPopupWins.createWindow("companySelPopupList", 20, 30, 640, 480);
                    w2.setText("사업자번호 선택페이지");
                    dhxSelPopupWins.window('companySelPopupList').setModal(true);
                    w2.attachURL('<c:url value = "/popup/CompanySelectPopUp"/>', true);
                }
                function merchMasterInit($form) {
                    searchFormInit($form);
                } 
            </script>            
            <div class="searchForm1row" id="onffmerchinfoSearch" style="display:none;width:100%;height:100%;background-color:white;">
                <form id="onffmerchinfoForm" method="POST" action="<c:url value="/merchant/merchantMasterList" />" modelAttribute="MerchantFormBean">
                       <input type="hidden" name="page_size" value="100" />
                       <input type="hidden" name="page_no" value="1" />
                        <div class="label2">ONOFF지급ID</div>
                        <div class="input"><input type="text" name="onffmerch_no" maxlength="30"/></div>
                        
                        <div class="label2">ONOFF지급ID명</div>
                        <div class="input"><input type="text" name="merch_nm" maxlength="30"/></div>                        
                        
                        <div class="label">사업자명</div>
                        <div class="input"><input type="text"name="comp_nm" maxlength="30"/></div>

                        <div class="label">법인번호</div>
                        <div class="input"><input type="text" name="corp_no" maxlength="13"/></div>

                        <div class="label">사업자번호</div>
                        <div class="input"><input type="text" name="biz_no" maxlength="10"/></div>

                        <div class="label">상태</div>
                        <div class="input">
                            <select name="svc_stat">
                                 <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${UseFlagList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>                        
                        <input type="button" name="onffmerchinfo_searchButton" value="조회"/>
                        <input type="button" name="init" value="검색조건지우기"/>
                        <div class="action">
                            <button class="addButton">추가</button>
                        </div>
                </form>
                <div id="onffmerchinfoMasterListPaging" style="width: 100%;"></div>
                <div id="onffmerchinfoMasterListrecinfoArea" style="width: 100%;"></div>                        
            </div>
            <div id="onffmerchinfoList" style="width:100%;height:100%;background-color:white;"></div>
 <c:if test="${!ajaxRequest}">    
        </body>
</html>
</c:if>

