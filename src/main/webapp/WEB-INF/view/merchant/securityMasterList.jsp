<%-- 
    Document   : securityMasterList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>지급ID 담보 조회</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">

                var securityMastergrid={};

                $(document).ready(function () {
                    
                    var securityMasterForm = document.getElementById("securityMaster_search");
                    
                    var tabBarId = tabbar.getActiveTab();
                    main_security_layout = tabbar.cells(tabBarId).attachLayout("1C");
                    securityMaster_layout = main_security_layout.cells('a').attachLayout("2E");
                    securityMaster_layout.cells('a').hideHeader();
                    securityMaster_layout.cells('b').hideHeader();
                    securityMaster_layout.cells('a').attachObject(securityMasterForm);
                    securityMaster_layout.cells('a').setHeight(60);
                    securityMastergrid = securityMaster_layout.cells('b').attachGrid();
                    securityMastergrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");

                    var securityMasterheaders = "";
                    securityMasterheaders +=  "<center>NO</center>,<center>지급ID번호</center>,<center>지급ID명</center>,<center>담보종류</center>,<center>담보액</center>,<center>적용시작일</center>,<center>적용종료일</center>,<center>남은 일수</center>";
                    securityMastergrid.setHeader(securityMasterheaders);
                    var securityMasterColAlign = "";
                    securityMasterColAlign +=  "center,center,center,center,right,center,center,center";
                    securityMastergrid.setColAlign(securityMasterColAlign);
                    var securityMasterColTypes = "";
                    securityMasterColTypes +=  "txt,txt,txt,txt,edn,txt,txt,edn";
                    securityMastergrid.setColTypes(securityMasterColTypes);
                    var securityMasterInitWidths = "";
                    securityMasterInitWidths +=   "50,150,150,150,150,150,150,100";
                    securityMastergrid.setInitWidths(securityMasterInitWidths);
                    var securityMasterColSorting = "";
                    securityMasterColSorting +=  "str,str,str,str,int,str,str,int";
                    securityMastergrid.setColSorting(securityMasterColSorting);
                    securityMastergrid.setNumberFormat("0,000",4);       
                    securityMastergrid.setNumberFormat("0,000",7);

                    securityMastergrid.enableColumnMove(true);
                    securityMastergrid.setSkin("dhx_skyblue");
                    securityMastergrid.enablePaging(true,Number($("#securityMasterForm input[name=page_size]").val()),10,"securityMasterPaging",true,"securityMasterrecinfoArea");
                    securityMastergrid.setPagingSkin("bricks");

                    //페이징 처리
                    securityMastergrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        /*
                        if(lInd !==0){
                            $("#securityMasterForm input[name=page_no]").val(ind);
                            securityMasterSearch();
                        }else{
                            return false;
                        }
                        */
                        
                        if(lInd !==0){
                          $("#securityMasterForm input[name=page_no]").val(ind);             
                           securityMastergrid.clearAll();
                           securityMasterSearch();
                        }else{
                            $("#securityMasterForm input[name=page_no]").val(1);
                            securityMastergrid.clearAll();
                             securityMasterSearch();
                        }                        
                        
                    });

                    securityMastergrid.init();
                    securityMastergrid.parse(${ls_securityMasterList}, "json");
                    

                securityMastergrid.attachEvent("onRowSelect", securityMaster_open_attach);

                    //담보 정보 조회
                    $("#securityMasterForm input[name=securityMaster_search]").click(function(){
                        /*
                        $("input[name=page_no]").val("1");

                        securityMastergrid.clearAll();

                        securityMasterSearch();
                        */
                        securityMastergrid.changePage(1);

                    });
                    
                    //담보 정보 엑셀다운로드
                    $("#securityMasterForm input[name=securityMaster_excel]").click(function(){

                        $("#securityMasterForm").attr("action","<c:url value="/merchant/securityMasterListExcel" />");
                        document.getElementById("securityMasterForm").submit();                        

                    });                    
                    
                });

            var securityMasterpopWindow;
            function securityMaster_open_attach(rowid, col) {
                
                if(col == '1')
                {
                    securityMasterpopWindow = new dhtmlXWindows();
                    var onffmerchno = securityMastergrid.cells(rowid,1).getValue();
                    w1 = securityMasterpopWindow.createWindow("w1", 150, 150,  800, 700);
                    w1.setText("지급ID 정보 수정");
                    securityMasterpopWindow.window('w1').setModal(true);
                    w1.attachURL("<c:url value="/merchant/merchantMasterUpdate" />" + "?onffmerch_no=" + onffmerchno+"&popupchk=1",true);
                    
                    securityMastergrid.clearSelection();
                }
                
                return false;
                        
            }    


                //담보 정보 조회
                function securityMasterSearch() {
                
                    $.ajax({
                        url: "<c:url value="/merchant/securityMasterList" />",
                        type: "POST",
                        async: true,
                        dataType: "json",
                        data: $("#securityMasterForm").serialize(),
                        success: function (data) {

                            var jsonData = $.parseJSON(data);

                            securityMastergrid.parse(jsonData,"json");

                        },
                        error: function () {
                            alert("조회 실패");
                        }
                    });
                        return false;
                }
                
                var dhxSelPopupWins=new dhtmlXWindows();
                /*    
                //ONOFF지급ID번호 팝업 클릭 이벤트
                $("#securityMasterForm input[name=onffmerch_no]").unbind("click").bind("click", function (){

                    onoffMerchSecuritySelectPopup();

                });   
                */
                
                
                //ONOFF지급ID명 팝업 클릭 이벤트
                $("#securityMasterForm input[name=merch_nm]").unbind("click").bind("click", function (){

                    onoffMerchSecuritySelectPopup();

                });   
                
                
                //검색조건 초기화
                $("#securityMasterForm input[name=init]").click(function () {

                    MerchSecurityMasterInit($("#securityMasterForm"));

                });  

                
                function onoffMerchSecuritySelectPopup(){
                    onoffObject.onffmerch_no = $("#securityMasterForm input[name=onffmerch_no]");
                    onoffObject.merch_nm = $("#securityMasterForm input[name=merch_nm]");
                    //ONOFF지급ID정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("지급ID번호 선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                } 
                function MerchSecurityMasterInit($form) {

                    searchFormInit($form);

                    transdetail_date_search("week");                    
                } 
                
            </script>

        <div class="right" id="securityMaster_search" style="width:100%;">
             <form id="securityMasterForm" method="POST" action="<c:url value="/merchant/securityMasterList" />" modelAttribute="securityFormBean">

                <input type="hidden" name="page_size" value="100" >
                <input type="hidden" name="page_no" value="1" >
                
                <table>
                    <tr>
                        <div class="label2">지급ID번호</div>
                        <div class="input">
                            <input type="text" name="onffmerch_no" value ="${securityFormBean.onffmerch_no}" />
                        </div>                  
                        <div class="label2">지급ID명</div>
                        <div class="input">
                            <input type="text" name="merch_nm" value ="${securityFormBean.merch_nm}" />
                        </div>  
                        
                        <td>
                            <input type="button" name="securityMaster_search" value="조회"/>
                        </td>
                        <td>
                            <input type="button" name="securityMaster_excel" value="엑셀"/>
                        </td>
                        <td><input type="button" name="init" value="검색조건지우기"/></td>
                    </tr>
                </table>
                        
            </form>

            <div class="paging">
                <div id="securityMasterPaging" style="width: 50%;"></div>
                <div id="securityMasterrecinfoArea" style="width: 50%;"></div>
            </div>
                                
         </div>

    </body>
</html>
