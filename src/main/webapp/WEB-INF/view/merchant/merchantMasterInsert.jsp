<%-- 
    Document   : onffmerchantMasterInsert
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>지급ID정보 등록</title>
        </head>
        <body>
</c:if>
<%
    String strCurDate = com.onoffkorea.system.common.util.Util.getTodayDate();
    String strTpDefaultEndDt = "20991231";
%>
            <script type="text/javascript">
                var regobj = new RegExp('-',"gi");
                var dhxSelPopupWins;
		dhxSelPopupWins = new dhtmlXWindows();
                
                var pay_dt_cd_day ='${PAY_DT_CD_DAY}';
                var pay_dt_cd_week = '${PAY_DT_CD_WEEK}';
                var pay_dt_cd_half = '${PAY_DT_CD_HALF}';
                var pay_dt_cd_month = '${PAY_DT_CD_MONTH}';
                               
                $(document).ready(function () {
                    
                    onffmerchant_layout = new dhtmlXLayoutObject("onffmerchantVP2", "1C", "dhx_skyblue");
                    onffmerchant_layout.cells('a').hideHeader();

                    onffmerchant_mygrid = onffmerchant_layout.cells('a').attachGrid();
                    onffmerchant_mygrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var mheaders = "";
                    mheaders += "<center>담보종류코드</center>,<center>담보종류</center>,<center>담보액</center>,<center>적용시작일</center>,<center>적용종료일</center>,<center>MEMO</center>";
                    onffmerchant_mygrid.setHeader(mheaders);
                    onffmerchant_mygrid.setColAlign("center,center,center,center,center,left");
                    onffmerchant_mygrid.setColTypes("txt,txt,txt,txt,txt,txt");
                    onffmerchant_mygrid.setInitWidths("10,100,100,80,80,200");
                    onffmerchant_mygrid.setColSorting("str,str,str,str,str,str");
                    onffmerchant_mygrid.enableColumnMove(false);
                    onffmerchant_mygrid.setSkin("dhx_skyblue");
                    onffmerchant_mygrid.init();
                    //onffmerchant_mygrid.parse(${termMasterList}, "json");
                    //onffmerchant_mygrid.attachEvent("onRowDblClicked", doInsertMerchantTidInsert);
 
                   onffmerchant_mygrid.setColumnHidden(0,true);   
                   //onffmerchant_mygrid.setColumnHidden(3,true);
                   
        
                    myCalendar = new dhtmlXCalendarObject(["view_close_dt","view_security_start_dt","view_security_end_dt"]);
                                        
                    //onffmerchant 정보 등록 이벤트
                    $("#onffmerchantFormInsert input[name=insert]").click(function(){
                       doInsertMerchantMaster(); 
                    });              
                    
                    //onffmerchant 정보 등록 닫기
                    $("#onffmerchantFormInsert input[name=close]").click(function(){
                         onffmerchinfoMasterWindow.window("w1").close();
                    });                   
                    
                    
                    $("#onffmerchantFormInsert input[name=app_chk_amt]").keyup(function(){

                          amtToHan($(this),$("#onffmerchantFormInsert span[name=insert_amt_han]"),Number($(this).attr("maxlength")));

                    });
                    
                    $("#onffmerchantFormInsert input[name=security_amt]").keyup(function(){

                        amtToHan($(this),$("#onffmerchantFormInsert span[name=insert_security_amt_han]"),Number($(this).attr("maxlength")));

                    });                    
                    
                });
                

                function CompanydSelectWin_merchantMasterIns()
                {   
                   onoffObject.comp_seq =  $("#onffmerchantFormInsert input[name=comp_seq]");
                   onoffObject.comp_nm =  $("#onffmerchantFormInsert input[name=comp_nm]");
                   onoffObject.biz_no =  $("#onffmerchantFormInsert input[name=biz_no]");
                   onoffObject.corp_no =  $("#onffmerchantFormInsert input[name=corp_no]");
                   onoffObject.comp_ceo_nm =  $("#onffmerchantFormInsert input[name=comp_ceo_nm]");
                   onoffObject.comp_tel1 =  $("#onffmerchantFormInsert input[name=comp_tel1]");
                   onoffObject.comp_tel2 =  $("#onffmerchantFormInsert input[name=comp_tel2]");
                   onoffObject.zip_cd =  $("#onffmerchantFormInsert input[name=comp_zip_cd]");
                   onoffObject.addr_1 =  $("#onffmerchantFormInsert input[name=comp_addr_1]");
                   onoffObject.addr_2 =  $("#onffmerchantFormInsert input[name=comp_addr_2]");
                   onoffObject.biz_cate =  $("#onffmerchantFormInsert input[name=comp_biz_cate]");
                   onoffObject.biz_type =  $("#onffmerchantFormInsert input[name=comp_biz_type]");
                   onoffObject.tax_flag =  $("#onffmerchantFormInsert input[name=comp_tax_flag]");
                   onoffObject.comp_cate_nm =  $("#onffmerchantFormInsert input[name=comp_cate_nm]");
                   
                   onoffObject.other_nm=$("#onffmerchantFormInsert input[name=merch_nm]"); 
                   onoffObject.other_zip_cd=$("#onffmerchantFormInsert input[name=zip_cd]"); 
                   onoffObject.other_addr_1=$("#onffmerchantFormInsert input[name=addr_1]"); 
                   onoffObject.other_addr_2=$("#onffmerchantFormInsert input[name=addr_2]"); 
                   onoffObject.other_tel1=$("#onffmerchantFormInsert input[name=tel_1]"); 
                   onoffObject.other_tel2=$("#onffmerchantFormInsert input[name=tel_2]"); 
                  
                    
                    w2 = dhxSelPopupWins.createWindow("companySelPopupList", 20, 30, 640, 480);
                    w2.setText("사업자번호 선택페이지");
                    dhxSelPopupWins.window('companySelPopupList').setModal(true);
                    dhxSelPopupWins.window('companySelPopupList').denyResize();
                    w2.attachURL('<c:url value = "/popup/CompanySelectPopUp"/>', true);
                }
                
                function AgentSelectWin_merchantMasterIns(){
                    w2 = dhxSelPopupWins.createWindow("AgentSelectPopUp", 20, 30, 640, 480);
                    w2.setText("대리점 선택페이지");
                    dhxSelPopupWins.window('AgentSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('AgentSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/AgentSelectPopUp"/>', true);
                } 
    
                /*
                //function SelCompaynyInfoInput(p_comp_seq,p_comp_nm,p_biz_no,p_corp_no,p_comp_ceo_nm,p_comp_tel1,p_comp_tel2,p_zip_cd,p_addr_1,p_addr_2,p_comp_cate_nm)
                function SelCompaynyInfoInput(p_comp_seq,p_comp_nm,p_comp_cate_nm,p_biz_no,p_corp_no,p_comp_ceo_nm,p_comp_tel1,p_comp_tel2,p_zip_cd,p_addr_1,p_addr_2,p_biz_type,p_biz_cate,p_tax_flag)
                {          
                    $("#onffmerchantFormInsert input[name=comp_seq]").attr('value',p_comp_seq);
                    $("#onffmerchantFormInsert input[name=comp_nm]").attr('value',p_comp_nm);
                    $("#onffmerchantFormInsert input[name=biz_no]").attr('value',p_biz_no);      
                    $("#onffmerchantFormInsert input[name=corp_no]").attr('value',p_corp_no);  
                    $("#onffmerchantFormInsert input[name=comp_ceo_nm]").attr('value',p_comp_ceo_nm);  
                    $("#onffmerchantFormInsert input[name=comp_tel1]").attr('value',p_comp_tel1);  
                    $("#onffmerchantFormInsert input[name=comp_tel2]").attr('value',p_comp_tel2);  
                    $("#onffmerchantFormInsert input[name=comp_zip_cd]").attr('value',p_zip_cd);  
                    $("#onffmerchantFormInsert input[name=comp_addr_1]").attr('value',p_addr_1);  
                    $("#onffmerchantFormInsert input[name=comp_addr_2]").attr('value',p_addr_2);  
                    $("#onffmerchantFormInsert input[name=comp_biz_cate]").attr('value',p_biz_cate);
                    $("#onffmerchantFormInsert input[name=comp_biz_type]").attr('value',p_biz_type);
                    $("#onffmerchantFormInsert input[name=comp_tax_flag]").attr('value',p_tax_flag);  
                    $("#onffmerchantFormInsert input[name=comp_cate_nm]").attr('value',p_comp_cate_nm);                    

                }
                */
                function SelAgentInfoInput(p_agent_seq, p_agent_nm)
                {
                    //agent_seq
                    $("#onffmerchantFormInsert input[name=agent_seq]").attr('value',p_agent_seq);  
                    $("#onffmerchantFormInsert input[name=agent_nm]").attr('value',p_agent_nm);     
                }
                
                function selChangeBalPeriod()
                {
                    var selbalperiod = $("#bal_period option:selected").val();
                    var selbalperiod_size = $("#pay_dt_cd_1 option").size();

                    var jsonobj;
                    if(selbalperiod == "DAY")
                    {
                        jsonobj = JSON.parse(pay_dt_cd_day);
                    }
                    else if(selbalperiod == "WEEK")
                    {
                        jsonobj = JSON.parse(pay_dt_cd_week);
                    }
                    else if(selbalperiod == "HALF")
                    {
                        jsonobj = JSON.parse(pay_dt_cd_half);
                    }
                    else if(selbalperiod == "MONTH")
                    {
                       jsonobj = JSON.parse(pay_dt_cd_month);
                    }
                    
                    $("#pay_dt_cd_1").find('option').remove();
                    $("#pay_dt_cd_2").find('option').remove();
                    
                    $("#pay_dt_cd_1").append($("<option></option>").val('').html('--선택하세요.--'));
                    $("#pay_dt_cd_2").append($("<option></option>").val('').html('--선택하세요.--'));
                    
                    $.each(jsonobj, function(){
                            $("#pay_dt_cd_1").append($("<option></option>").val(this['ID']).html(this['VIEW']));
                        }
                    );
            
                    if(selbalperiod == "HALF")
                    {
                            $("#pay_dt_cd_2").attr('disabled',null);
                            $.each(jsonobj, function(){
                                $("#pay_dt_cd_2").append($("<option></option>").val(this['ID']).html(this['VIEW']));
                            }
                        );
                    }
                    else
                    {
                        $("#pay_dt_cd_2").attr('disabled','true');
                    }
                    
                }
                

                function addRow()
                {
                   
                    
                   var instpval = $("#onffmerchantFormInsert select[name=security_cate] option:selected").val();
                   var strTpStartdt =  $("#onffmerchantFormInsert input[name=view_security_start_dt]").val().replace(regobj,'');
                   var strTpEnddt =  $("#onffmerchantFormInsert input[name=view_security_end_dt]").val().replace(regobj,'');
                   
                   
                   if(instpval == '') 
                   {
                       alert('담보종류는 필수 입니다.');
                       $("#onffmerchantFormInsert select[name=security_cate]").focus();
                       return false;
                   }
                   
                   if($("#onffmerchantFormInsert input[name=security_amt]").val() == '') 
                   {
                       alert('담보액은 필수 입니다.');
                       $("#onffmerchantFormInsert input[name=security_amt]").focus();
                       return false;
                   }
                   
                  if($("#onffmerchantFormInsert input[name=view_security_start_dt]").val() == '') 
                   {
                       alert('시작일은 필수 입니다.');
                       $("#onffmerchantFormInsert input[name=view_security_start_dt]").focus();
                       return false;
                   }
                   
                  if($("#onffmerchantFormInsert input[name=view_security_end_dt]").val() == '') 
                   {
                       alert('종료일 필수 입니다.');
                       $("#onffmerchantFormInsert input[name=view_security_end_dt]").focus();
                       return false;
                   }                   
                   

                   var chkflg = 0;
                   onffmerchant_mygrid.forEachRow(function(id){
 
                   if(strTpStartdt < onffmerchant_mygrid.cells(id,4).getValue())
                        {
                            chkflg = 1;
                        }
                    
                   });
                   
                    var strTpsecurity_amt = $("#onffmerchantFormInsert input[name=security_amt]").val();
                    strTpsecurity_amt =strTpsecurity_amt.replace(/,/g, '');
        
                   if(chkflg==1) 
                   {
                       alert('등록하시려는 담보의 시작일이 기존 등록된 담보의 종료일보다 큽니다.');
                       return false;
                   }
                   else
                   {   
                        //담보종류코드,담보종류,담보액,적용시작일,적용종료일,MEMO
                        //   0           1      2        3        4         5
                        var strnewRow = 
                                $("#onffmerchantFormInsert select[name=security_cate] option:selected").val() 
                                + "," + 
                                $("#onffmerchantFormInsert select[name=security_cate] option:selected").text() 
                                + "," + 
                                //$("#onffmerchantFormInsert input[name=security_amt]").val() 
                                strTpsecurity_amt
                                + "," + 
                                strTpStartdt
                                + "," + 
                                strTpEnddt
                                + "," + 
                                $("#onffmerchantFormInsert textarea[name=security_memo]").val()
                        onffmerchant_mygrid.addRow(onffmerchant_mygrid.getUID(), strnewRow, 1);
                   }

                }
                
                function delRow()
                {
                   var selRowid = onffmerchant_mygrid.getSelectedRowId();
                   
                    onffmerchant_mygrid.deleteRow(selRowid);                                
                }

                //가맹점정보 정보 등록
                function doInsertMerchantMaster(){
                    
                    if(!window.confirm("등록하시겠습니까?")){
                        return;
                    }
                    
                    if($("#onffmerchantFormInsert input[name=comp_seq]").val() == "")
                    {
                      alert("사업자 정보는 필수사항 입니다.");
                      return;
                    }
                    
                    if($("#onffmerchantFormInsert input[name=merch_nm]").val() == "")
                    {
                      alert("지급ID명은 필수사항 입니다.");                      
                      $("#onffmerchantFormInsert input[name=merch_nm]").focus();
                      return;
                    }
                    
                    if($("#onffmerchantFormInsert input[name=tel_1]").val() == "")
                    {
                      alert("가맹점 연락처1 필수사항 입니다.");
                      $("#onffmerchantFormInsert input[name=tel_1]").focus();
                      return;
                    }
                    
                    if($("#onffmerchantFormInsert select[name=bal_period]").val() == "")
                    {
                      alert("정산주기는 필수사항 입니다.");
                      $("#onffmerchantFormInsert select[name=bal_period]").focus();
                      return;
                    }         
                    
                    if($("#onffmerchantFormInsert select[name=pay_dt_cd_1]").val() == "")
                    {
                      alert("정산기준일은 필수사항 입니다.");
                      $("#onffmerchantFormInsert select[name=pay_dt_cd_1]").focus();
                      return;
                    }

                    var selbalperiod = $("#bal_period option:selected").val();
                    if(selbalperiod == 'HALF')
                    {
                        if($("#onffmerchantFormInsert select[name=pay_dt_cd_2]").val() == "")
                        {
                          alert("정산기준일은 필수사항 입니다.");
                          $("#onffmerchantFormInsert select[name=pay_dt_cd_2]").focus();
                          return;
                        }                                      
                    }
                    
                    if($("#onffmerchantFormInsert input[name=pay_dt]").val() == "")
                    {
                      alert("지급일은 필수사항 입니다.");
                      $("#onffmerchantFormInsert input[name=pay_dt]").focus();
                      return;
                    }                    
                    
                    
                    if($("#onffmerchantFormInsert select[name=bank_cd]").val() == "")
                    {
                      alert("입금은행은 필수사항 입니다.");
                      $("#onffmerchantFormInsert select[name=bank_cd]").focus();
                      return;
                    }                    
                    
                    
                    if($("#onffmerchantFormInsert input[name=acc_no]").val() == "")
                    {
                      alert("계좌번호는 필수사항 입니다.");
                      $("#onffmerchantFormInsert input[name=acc_no]").focus();
                      return;
                    }                                        
                    
                    if($("#onffmerchantFormInsert input[name=agent_seq]").val() != "")
                    {
                        if($("#onffmerchantFormInsert input[name=agent_commission]").val() == "")
                        {
                          alert("대리점수수료는 필수사항 입니다.");
                          $("#onffmerchantFormInsert input[name=agent_commission]").focus();
                          return;
                        }               
                    }                                                    
                    
                    if($("#onffmerchantFormInsert input[name=acc_nm]").val() == "")
                    {
                      alert("계좌주는 필수사항 입니다.");
                      $("#onffmerchantFormInsert input[name=acc_nm]").focus();
                      return;
                    }            
                    
                    var tmpappchkamt = $("#onffmerchantFormInsert input[name=app_chk_amt]").val();
                    
                    $("#onffmerchantFormInsert input[name=app_chk_amt]").val(tmpappchkamt.replace(/,/g, ''));                    
                    
                    if($("#onffmerchantFormInsert input[name=app_chk_amt]").val() == "")
                    {
                      alert("결제한도는 필수사항 입니다.");
                      $("#onffmerchantFormInsert input[name=app_chk_amt]").focus();
                      return;
                    }        
                    
                    
                    if($("#onffmerchantFormInsert input[name=appreq_chk_amt]").val() == "")
                    {
                      alert("건별 결제한도는 필수사항 입니다.");
                      $("#onffmerchantFormInsert input[name=appreq_chk_amt]").focus();
                      return;
                    }                         
                    
                    
                    if($("#onffmerchantFormInsert input[name=agent_seq]").val() != "")
                    {
                      if($("#onffmerchantFormInsert input[name=agent_commission]").val() == "")
                      {
                        alert("대리점 선택시 대리점 수수료는 필수사항 입니다.");
                        $("#onffmerchantFormInsert input[name=agent_commission]").focus();
                        return;
                      }
                    }
                  
                    if($("#onffmerchantFormInsert input[name=view_close_dt]").val() != "")
                    {
                        $("#onffmerchantFormInsert input[name=close_dt]").attr('value',$("#onffmerchantFormInsert input[name=view_close_dt]").val().replace(/-/g,''));
                    }
                    
        
                    var security_info = "";
                    //가맹점정보 상세정보를 만든다.
                    onffmerchant_mygrid.forEachRow(function(id){
                     
                     var strlastCol =onffmerchant_mygrid.cells(id,5).getValue();
                     if(strlastCol =='') strlastCol= " ";
                        //담보종류코드,담보종류,담보액,적용시작일,적용종료일,MEMO
                        //   0           1      2        3        4         5
                       var row = onffmerchant_mygrid.cells(id,0).getValue() + '```' + onffmerchant_mygrid.cells(id,2).getValue() + '```' + onffmerchant_mygrid.cells(id,3).getValue() + '```' + onffmerchant_mygrid.cells(id,4).getValue() + '```' + strlastCol;
                       
                       security_info = security_info + row + ']]]';
                    });
                   
                    $("#onffmerchantFormInsert input[name=security_info]").attr('value',security_info);  
                   
                    $.ajax({
                        url : $("#onffmerchantFormInsert").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#onffmerchantFormInsert").serialize(),
                        success : function(data) {
                            //alert(data);
                            if(data=='1')
                            {
                                alert("등록 완료");
                                onffmerchinfoMasterGrid.clearAll();
                                doSearchMerchantMaster();
                                onffmerchinfoMasterWindow.window("w1").close();
                            }
                            else
                            {
                                alert("등록 실패");
                            }
                        },
                        error : function() { 
                                alert("등록 실패");
                        }
                    });
                }             


            </script>
        <div style='width:100%;height:100%;overflow:auto;'>            
            <form id="onffmerchantFormInsert" method="POST" action="<c:url value="/merchant/merchantMasterInsert" />" modelAttribute="MerchantFormBean">
                <table class="gridtable" height="100%" width="100%">   
                    <tr>
                        <td align='left' colspan='4'>- 지급ID 사업자 정보</td>                        
                    </tr>
                    <tr>                        
                        <td class="headcol">사업자명*</td>
                        <td><input name="comp_nm" id="comp_nm"  readonly onclick='javascript:CompanydSelectWin_merchantMasterIns();' /><input type="hidden" name="comp_seq" id="comp_seq"/></td>
                        <td class="headcol">개인/법인구분*</tD>
                        <td><input name="comp_cate_nm" id="comp_cate_nm"  readonly onclick='javascript:CompanydSelectWin_merchantMasterIns();'/></td>
                    </tr>
                     <tr>
                        <td class="headcol">사업자번호*</td>
                        <td><input name="biz_no"  readonly onclick='javascript:CompanydSelectWin_merchantMasterIns();'/></td>
                        <td class="headcol">법인번호*</tD>
                        <td><input name="corp_no"  readonly onclick='javascript:CompanydSelectWin_merchantMasterIns();'/></td>
                    </tr>             
                     <tr>
                        <td class="headcol">업태*</td>
                        <td><input name="comp_biz_type"  readonly onclick='javascript:CompanydSelectWin_merchantMasterIns();'/></td>
                        <td class="headcol">업종*</tD>
                        <td><input name="comp_biz_cate"  readonly onclick='javascript:CompanydSelectWin_merchantMasterIns();'/></td>
                    </tr>     
                    <tr>
                        <td class="headcol">과세구분*</tD>
                        <td ><input name="comp_tax_flag"  readonly onclick='javascript:CompanydSelectWin_merchantMasterIns();'/></td>                        
                        <td class="headcol">대표자명*</tD>
                        <td ><input name="comp_ceo_nm"  readonly onclick='javascript:CompanydSelectWin_merchantMasterIns();'/></td>
                    </tr>     
                    <tr>
                        <td class="headcol">대표번호*</td>
                        <td><input name="comp_tel1"  readonly onclick='javascript:CompanydSelectWin_merchantMasterIns();'/></td>
                        <td class="headcol">FAX</tD>
                        <td><input name="comp_tel2"  readonly onclick='javascript:CompanydSelectWin_merchantMasterIns();'/></td>
                    </tr>
                    <tr>
                        <td class="headcol">우편번호</td>
                        <td colspan="3"><input name="comp_zip_cd" readonly onclick='javascript:CompanydSelectWin_merchantMasterIns();'/></td>
                    </tr>   
                    <tr>                        
                        <td class="headcol">주소</tD>
                        <td colspan="3"><input name="comp_addr_1" size="50" readonly onclick='javascript:CompanydSelectWin_merchantMasterIns();'/>&nbsp;<input name="comp_addr_2" size="50" readonly onclick='javascript:CompanydSelectWin_merchantMasterIns();'/></td>
                    </tr>   
                    <tr>
                        <td align='left' colspan='4'>- 지급ID 정보</td>                        
                    </tr>
                    <tr>                        
                        <td class="headcol" width="20%">지급ID(자동생성)</td>
                        <td ><input name="onffmerch_no" size="25" readonly /></td>
                        <td class="headcol" width="20%">지급ID명*</td>
                        <td ><input name="merch_nm" size="40" maxlength="50" onblur="javascript:checkLength(this,50);" /></td>                        
                    </tr>
                    <tr>                        
                        <td class="headcol" width="20%">가맹점연락처1*</td>
                        <td ><input name="tel_1" size="20" maxlength="20"/></td>
                        <td class="headcol" width="20%">가맹점연락처2</td>
                        <td ><input name="tel_2" size="20" maxlength="20" /></td>                        
                    </tr>
                    <tr>                        
                        <td class="headcol" width="20%">E-MAIL</td>
                        <td ><input name="email" size="25" maxlength="100" /></td>
                        <td class="headcol">우편번호</td>
                        <td><input name="zip_cd" maxlength="6" /></td>
                    </tr>             
                    <tr>
                        <td class="headcol">주소</tD>
                        <td colspan="3"><input name="addr_1" size="40" />&nbsp;<input name="addr_2" size="40" /></td>
                    </tr> 
                     <tr>
                        <td class="headcol">결제한도*</td>
                        <td ><input name="app_chk_amt"  onkeyPress="javascript:InpuOnlyNumber2(this);"  maxlength="15" /><span name="insert_amt_han"></span><span>원</span></td>     
                        <td class="headcol">건별결제한도*</td>
                        <td ><input name="appreq_chk_amt"  onkeyPress="javascript:InpuOnlyNumber2(this);"  maxlength="15" value="0" />(미사용시 0원 입력)</td>                            
                    </tr> 
                    <tr>
                        <td class="headcol">취소권한*</td>
                        <td ><select name="cncl_auth" >
                                <c:forEach var="code" items="${Cncl_AuthList.tb_code_details}">
                                    <option value="${code.detail_code}" >${code.code_nm}</option>
                                </c:forEach>
                            </select></td>  
                        <td class="headcol">가맹점상태*</td>
                        <td ><select name="svc_stat" >
                                <c:forEach var="code" items="${SvcStatList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Merchant.svc_stat}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach>
                            </select>                         
                        </td>
                    </tr>                           
                    <tr>
                        <td class="headcol">해지일</td>
                        <td><input name="view_close_dt" id="view_close_dt" maxlength="10" /><input type="hidden" name="close_dt"/></td>
                        <td class="headcol">해지사유</tD>
                        <td><input name="close_reson" size="50" /></td>
                    </tr>                    
                    <tr>
                        <td class="headcol">Memo</td>
                        <td colspan="3"><textarea name="memo" cols="60" rows="5"></textarea></td>                        
                    </tr>                          
                    <tr>
                        <td align='left' colspan='4'>- 정산정보</td>                        
                    </tr>
                     <tr>
                        <td class="headcol">정산주기*</td>
                        <td colspan="3"><select name="bal_period" id="bal_period" onchange="javascript:selChangeBalPeriod();" >
                                <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${BalPeriodList.tb_code_details}">
                                    <option value="${code.detail_code}" >${code.code_nm}</option>
                                </c:forEach>
                            </select>                         
                        </td>
                    </tr>
                     <tr>                        
                        <td class="headcol">정산기준일*</td>
                        <td colspan="3"><select name="pay_dt_cd_1" id="pay_dt_cd_1" >
                                <option value="">--선택하세요.--</option>
                            </select>&nbsp;<select name="pay_dt_cd_2" id="pay_dt_cd_2" >
                                <option value="">--선택하세요.--</option>
                            </select>   
                        </td>                        
                    </tr>
                    <tr>
                        <td class="headcol">지급일*</td>
                        <td >정산대상 최종승인일+<input name="pay_dt" size="3"  />(영업일)</td>                         
                        <td class="headcol">입금은행*</td>
                        <td><select name="bank_cd" >
                                <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${BankCdList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select></td> 
                    <tr>
                        <td class="headcol">입금계좌*</td>
                        <td><input name="acc_no" maxlength="30" size="30" onkeyPress="javascript:InpuOnlyNumber(this);" /></td>     
                        <td class="headcol">계좌주*</tD>
                        <td ><input name="acc_nm" size="30"  /></td>                        
                    </tr>  
                    <tr>
                        <td align='left' colspan='4'>- 대리점 정보</td>                        
                    </tr>
                    <tr>                        
                        <td class="headcol" width="30%">대리점명</td>
                        <td><input name="agent_nm" id="agent_nm"  readonly onclick="javascript:AgentSelectWin_merchantMasterIns();"/><input type="hidden" name="agent_seq" id="agent_seq"/></td>
                        <td class="headcol">대리점수수료</tD>
                        <td ><input name="agent_commission"  onkeyPress="javascript:InpuOnlyNumber(this);"  /></td>    
                    </tr>
                    <tr>
                        <td align='left' colspan='4'>- 담보 정보</td>                        
                    </tr>
                    <tr>                        
                        <td class="headcol" width="30%">담보종류*</td>
                        <td><select name="security_cate" >
                                <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${SecurityCateList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select></td>
                        <td class="headcol">담보액*</tD>
                        <td ><input name="security_amt"  onkeyPress="javascript:InpuOnlyNumber2(this);"   maxlength="15" /><span name="insert_security_amt_han"></span><span>원</span></td>    
                    </tr>     
                    <tr>
                        <td class="headcol">시작일*</td>
                        <td><input name="view_security_start_dt" id="view_security_start_dt" maxlength="10" value="<%=strCurDate%>" readonly/><input type="hidden" name="security_start_dt"/></td>
                        <td class="headcol">종료일*</tD>
                        <td><input name="view_security_end_dt" id="view_security_end_dt" maxlength="10" value="<%=strTpDefaultEndDt%>" readonly/><input type="hidden" name="security_end_dt"/></td>
                    </tr>  
                    <tr>
                        <td class="headcol">Memo</td>
                        <td colspan="3"><textarea name="security_memo" cols="60" rows="3"></textarea></td>                        
                    </tr>                              
                    <tr>
                        <td align='left' colspan='4' valign="top">
                            <div id = "common_btn" align = "right" style="height:30px;" >
                            <input type="button" name="security_addbt" value="담보추가" onclick="javascript:addRow();"/>
                            <input type="button" name="security_delbt" value="담보삭제" onclick="javascript:delRow();"/>
                            </div>
                            <div id="onffmerchantVP2" style="position: relative; width:100%;height:200px;background-color:white;"></div>
                        </td>                        
                    </tr>   
                    <tr>
                        <td colspan="4" align="center"><input type="button" name="insert" value="등록"/>&nbsp;<input type="button" name="close" value="닫기"/></td>
                    </tr>       
                </table>
                 <input type="hidden" name="security_info" />
            </form>
        </div>
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

