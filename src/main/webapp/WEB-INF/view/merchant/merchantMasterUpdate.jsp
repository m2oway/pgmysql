<%-- 
    Document   : onffmerchantMasterUpdate
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>지급ID정보 수정</title>
        </head>
        <body>
</c:if>
<%
    String strCurDate = com.onoffkorea.system.common.util.Util.getTodayDate();
    String strTpDefaultEndDt = "20991231";
    
    String strTpPopChk = request.getParameter("popupchk");
    
    if(strTpPopChk == null)
    {
        strTpPopChk="";
    }    
%>
            <script type="text/javascript">
                var regobj = new RegExp('-',"gi");
                var dhxSelPopupWins;
		dhxSelPopupWins = new dhtmlXWindows();
                
                var pay_dt_cd_day ='${PAY_DT_CD_DAY}';
                var pay_dt_cd_week = '${PAY_DT_CD_WEEK}';
                var pay_dt_cd_half = '${PAY_DT_CD_HALF}';
                var pay_dt_cd_month = '${PAY_DT_CD_MONTH}';
                               
                $(document).ready(function () {
                    
                    onffmerchant_layout = new dhtmlXLayoutObject("onffmerchantVP2", "1C", "dhx_skyblue");
                    onffmerchant_layout.cells('a').hideHeader();

                    onffmerchant_mygrid = onffmerchant_layout.cells('a').attachGrid();
                    onffmerchant_mygrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var mheaders = "";
                    mheaders += "<center>담보종류코드</center>,<center>담보종류</center>,<center>담보액</center>,<center>적용시작일</center>,<center>적용종료일</center>,<center>MEMO</center>";
                    onffmerchant_mygrid.setHeader(mheaders);
                    onffmerchant_mygrid.setColAlign("center,center,center,center,center,left");
                    onffmerchant_mygrid.setColTypes("txt,txt,txt,txt,txt,txt");
                    onffmerchant_mygrid.setInitWidths("10,100,100,80,80,200");
                    onffmerchant_mygrid.setColSorting("str,str,str,str,str,str");
                    onffmerchant_mygrid.enableColumnMove(false);
                    onffmerchant_mygrid.setSkin("dhx_skyblue");
                    onffmerchant_mygrid.init();
                    onffmerchant_mygrid.parse(${merchantDtlList}, "json");
                    onffmerchant_mygrid.attachEvent("onRowDblClicked", doSelectSecurityInfo);
 
                   onffmerchant_mygrid.setColumnHidden(0,true);   
                   //onffmerchant_mygrid.setColumnHidden(3,true);
                   
        
                    myCalendar = new dhtmlXCalendarObject(["view_close_dt","view_security_start_dt","view_security_end_dt"]);
                                        
                    //onffmerchant 수정 이벤트
                    $("#onffmerchantFormUpdate input[name=update]").click(function(){
                       doUpdateMerchantMaster(); 
                    });              
                    
                    //onffmerchant delete 이벤트
                    $("#onffmerchantFormUpdate input[name=delete]").click(function(){
                       doDeleteMerchantMaster(); 
                    });              
                    
                    
                    //onffmerchant 정보 등록 닫기
                    $("#onffmerchantFormUpdate input[name=close]").click(function(){
                         onffmerchinfoMasterWindow.window("w1").close();
                    });        
                    
                    InitBalPeriod('${tb_Merchant.bal_period}','${tb_Merchant.pay_dt_cd_1}','${tb_Merchant.pay_dt_cd_2}');
                    
                    $("#onffmerchantFormUpdate input[name=app_chk_amt]").keyup(function(){

                          amtToHan($(this),$("#onffmerchantFormUpdate span[name=insert_amt_han]"),Number($(this).attr("maxlength")));

                    });
                    
                    $("#onffmerchantFormUpdate input[name=security_amt]").keyup(function(){

                        amtToHan($(this),$("#onffmerchantFormUpdate span[name=insert_security_amt_han]"),Number($(this).attr("maxlength")));

                    });
                    
                });

                
                function selChangeBalPeriod()
                {
                    var selbalperiod = $("#bal_period option:selected").val();
                    var selbalperiod_size = $("#pay_dt_cd_1 option").size();

                    var jsonobj;
                    if(selbalperiod == "DAY")
                    {
                        jsonobj = JSON.parse(pay_dt_cd_day);
                    }
                    else if(selbalperiod == "WEEK")
                    {
                        jsonobj = JSON.parse(pay_dt_cd_week);
                    }
                    else if(selbalperiod == "HALF")
                    {
                        jsonobj = JSON.parse(pay_dt_cd_half);
                    }
                    else if(selbalperiod == "MONTH")
                    {
                       jsonobj = JSON.parse(pay_dt_cd_month);
                    }
                    
                    $("#pay_dt_cd_1").find('option').remove();
                    $("#pay_dt_cd_2").find('option').remove();
                    
                    $("#pay_dt_cd_1").append($("<option></option>").val('').html('--선택하세요.--'));
                    $("#pay_dt_cd_2").append($("<option></option>").val('').html('--선택하세요.--'));
                    
                    $.each(jsonobj, function(){
                            $("#pay_dt_cd_1").append($("<option></option>").val(this['ID']).html(this['VIEW']));
                        }
                    );
            
                    
                    if(selbalperiod == "HALF")
                    {
                            $("#pay_dt_cd_2").attr('disabled',null);
                            $.each(jsonobj, function(){
                                $("#pay_dt_cd_2").append($("<option></option>").val(this['ID']).html(this['VIEW']));
                            }
                        );
                    }
                    else
                    {
                        $("#pay_dt_cd_2").attr('disabled','true');
                    }
                    
                }
                
                function InitBalPeriod(init_bal_period, init_pay_dt_cd_1, init_pay_dt_cd_2)
                {
                    $("#security_cate > option[value="+init_bal_period+"]").attr("selected", "ture");

                    var jsonobj;
                    if(init_bal_period == "DAY")
                    {
                        jsonobj = JSON.parse(pay_dt_cd_day);
                    }
                    else if(init_bal_period == "WEEK")
                    {
                        jsonobj = JSON.parse(pay_dt_cd_week);
                    }
                    else if(init_bal_period == "HALF")
                    {
                        jsonobj = JSON.parse(pay_dt_cd_half);
                    }
                    else if(init_bal_period == "MONTH")
                    {
                       jsonobj = JSON.parse(pay_dt_cd_month);
                    }
                    
                    $("#pay_dt_cd_1").find('option').remove();
                    $("#pay_dt_cd_2").find('option').remove();
                    
                    $("#pay_dt_cd_1").append($("<option></option>").val('').html('--선택하세요.--'));
                    $("#pay_dt_cd_2").append($("<option></option>").val('').html('--선택하세요.--'));
                    
                    $.each(jsonobj, function(){
                            $("#pay_dt_cd_1").append($("<option></option>").val(this['ID']).html(this['VIEW']));
                        }
                    );
            
                    $("#pay_dt_cd_1 > option[value='"+init_pay_dt_cd_1+"']").attr("selected", "ture");
            
                    if(init_bal_period == "HALF")
                    {
                            $("#pay_dt_cd_2").attr('disabled',null);
                            $.each(jsonobj, function(){
                                $("#pay_dt_cd_2").append($("<option></option>").val(this['ID']).html(this['VIEW']));
                            }
                        );
                
                        $("#pay_dt_cd_2 > option[value='"+init_pay_dt_cd_2+"']").attr("selected", "ture");
                    }
                    else
                    {
                        $("#pay_dt_cd_2").attr('disabled','true');
                    }
                    
                }                
                
                
                                
                function AgentSelectWin(){
                    w2 = dhxSelPopupWins.createWindow("AgentSelectPopUp", 20, 30, 640, 480);
                    w2.setText("대리점 선택페이지");
                    dhxSelPopupWins.window('AgentSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('AgentSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/AgentSelectPopUp"/>', true);
                } 
    
    
                function SelAgentInfoInput(p_agent_seq, p_agent_nm)
                {
                    //agent_seq
                    $("#onffmerchantFormUpdate input[name=agent_seq]").attr('value',p_agent_seq);  
                    $("#onffmerchantFormUpdate input[name=agent_nm]").attr('value',p_agent_nm);
                }
                
                
                function setTransForm()
                {
                   var strTpStartdt =  $("#onffmerchantFormUpdate input[name=view_security_start_dt]").val().replace(regobj,'');
                   var strTpEnddt =  $("#onffmerchantFormUpdate input[name=view_security_end_dt]").val().replace(regobj,'');
                   
                   var strTpsecurity_amt = $("#onffmerchantFormUpdate input[name=security_amt]").val();
                   
                         $("#merchantDtlFormInfo input[name=security_seq]").attr('value',$("#onffmerchantFormUpdate input[name=security_seq]").val());
                         $("#merchantDtlFormInfo input[name=security_cate]").attr('value',$("#onffmerchantFormUpdate select[name=security_cate]").val());
                         $("#merchantDtlFormInfo input[name=security_amt]").attr('value',strTpsecurity_amt.replace(/,/g, ''));
                         $("#merchantDtlFormInfo input[name=start_dt]").attr('value',strTpStartdt);
                         $("#merchantDtlFormInfo input[name=end_dt]").attr('value',strTpEnddt);
                         $("#merchantDtlFormInfo input[name=memo]").attr('value',$("#onffmerchantFormUpdate textarea[name=security_memo]").val());
                         
                }       
                
                //담보정보 선택
                function doSelectSecurityInfo()
                {   
                    //담보종류코드,담보종류명,담보액,적용시작일,적용종료일,MEMO
                    //   0             1       2      3        4        5  
                    $("#onffmerchantFormUpdate input[name=security_amt]").attr('value',onffmerchant_mygrid.cells(onffmerchant_mygrid.getSelectedId(),2).getValue());
                    $("#onffmerchantFormUpdate input[name=security_seq]").attr('value',onffmerchant_mygrid.getSelectedId());                    
                    $("#onffmerchantFormUpdate input[name=security_start_dt]").attr('value',onffmerchant_mygrid.cells(onffmerchant_mygrid.getSelectedId(),3).getValue());  
                    $("#onffmerchantFormUpdate input[name=security_end_dt]").attr('value',onffmerchant_mygrid.cells(onffmerchant_mygrid.getSelectedId(),4).getValue());  
                    $("#onffmerchantFormUpdate input[name=view_security_start_dt]").attr('value',onffmerchant_mygrid.cells(onffmerchant_mygrid.getSelectedId(),3).getValue());  
                    $("#onffmerchantFormUpdate input[name=view_security_end_dt]").attr('value',onffmerchant_mygrid.cells(onffmerchant_mygrid.getSelectedId(),4).getValue());         
                    $("#onffmerchantFormUpdate textarea[name=security_memo]").attr('value',onffmerchant_mygrid.cells(onffmerchant_mygrid.getSelectedId(),5).getValue());         
                    
                    $("#security_cate > option[value="+ onffmerchant_mygrid.cells(onffmerchant_mygrid.getSelectedId(),0).getValue() +"]").attr("selected", "ture");
                }         
                
                function clearSelForm()
                {
                    $("#onffmerchantFormUpdate span[name=insert_security_amt_han]").text('');
                    
                    $("#onffmerchantFormUpdate input[name=security_amt]").attr('value','');
                    $("#onffmerchantFormUpdate input[name=security_seq]").attr('value','');                    
                    $("#onffmerchantFormUpdate input[name=security_start_dt]").attr('value','');  
                    $("#onffmerchantFormUpdate input[name=security_end_dt]").attr('value','');  
                    $("#onffmerchantFormUpdate input[name=view_security_start_dt]").attr('value','');  
                    $("#onffmerchantFormUpdate input[name=view_security_end_dt]").attr('value','');         
                    $("#onffmerchantFormUpdate textarea[name=security_memo]").attr('value','');   
                    
                    $("#security_cate > option[value='']").attr("selected", "ture");

                    $("#merchantDtlFormInfo input[name=security_seq]").attr('value','');
                    $("#merchantDtlFormInfo input[name=security_cate]").attr('value','');
                    $("#merchantDtlFormInfo input[name=security_amt]").attr('value','');
                    $("#merchantDtlFormInfo input[name=start_dt]").attr('value','');
                    $("#merchantDtlFormInfo input[name=end_dt]").attr('value','');
                    $("#merchantDtlFormInfo input[name=memo]").attr('value','');
                }
                
                // 정보 조회
                function doSearchMerchantDtlList(){
                    $.ajax({
                        url : "../merchant/merchantDtlList",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#merchantDtlFormInfo").serialize(),
                        success : function(data) {
                            onffmerchant_mygrid.clearAll();
                            onffmerchant_mygrid.parse(jQuery.parseJSON(data), "json");
                        },
                        error : function() { 
                                alert("조회 실패");
                        }
                    });	            
                }                               
                
                                
                function addRow()
                {                 
                   setTransForm();
          
                   var instpval = $("#onffmerchantFormUpdate select[name=security_cate] option:selected").val();
                   var strTpStartdt =  $("#onffmerchantFormUpdate input[name=view_security_start_dt]").val().replace(regobj,'');
                   var strTpEnddt =  $("#onffmerchantFormUpdate input[name=view_security_end_dt]").val().replace(regobj,'');
                   
                   if(instpval == '') 
                   {
                       alert('담보종류는 필수 입니다.');
                       $("#onffmerchantFormUpdate select[name=security_cate]").focus();
                       return false;
                   }
                   
                   if($("#onffmerchantFormUpdate input[name=security_amt]").val() == '') 
                   {
                       alert('담보액은 필수 입니다.');
                       $("#onffmerchantFormUpdate input[name=security_amt]").focus();
                       return false;
                   }
                   
                  if($("#onffmerchantFormUpdate input[name=view_security_start_dt]").val() == '') 
                   {
                       alert('시작일은 필수 입니다.');
                       $("#onffmerchantFormUpdate input[name=view_security_start_dt]").focus();
                       return false;
                   }
                   
                  if($("#onffmerchantFormUpdate input[name=view_security_end_dt]").val() == '') 
                   {
                       alert('종료일 필수 입니다.');
                       $("#onffmerchantFormUpdate input[name=view_security_end_dt]").focus();
                       return false;
                   }                   
                   

                   var chkflg = 0;
                   onffmerchant_mygrid.forEachRow(function(id){
 
                   if(strTpStartdt < onffmerchant_mygrid.cells(id,4).getValue())
                        {
                            chkflg = 1;
                        }
                    
                   });
        
                   if(chkflg==1) 
                   {
                       alert('등록하시려는 담보의 시작일이 기존 등록된 담보의 종료일보다 큽니다.');
                       return false;
                   }
                   
                   $.ajax({
                        url :  "../merchant/merchantDtlInsert",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#merchantDtlFormInfo").serialize(),
                        success : function(data) {
                            if(data == '1')
                            {
                                alert("등록 완료");
                                onffmerchant_mygrid.clearAll();
                                doSearchMerchantDtlList();
                                clearSelForm();
                            }
                            else
                            {
                                alert("등록 실패");
                            }
                        },
                        error : function() { 
                                alert("등록 실패");
                        }
                    });
                }
                
                function modRow()
                {                 
                   setTransForm();
          
                   var instpval = $("#onffmerchantFormUpdate select[name=security_cate] option:selected").val();
                   var strTpStartdt =  $("#onffmerchantFormUpdate input[name=view_security_start_dt]").val().replace(regobj,'');
                   var strTpEnddt =  $("#onffmerchantFormUpdate input[name=view_security_end_dt]").val().replace(regobj,'');

                   if($("#onffmerchantFormUpdate input[name=security_seq]").val() == '') 
                   {
                       alert('수정할 담보정보를 선택해주세요.');
                       return false;
                   }        
        
                   if(instpval == '') 
                   {
                       alert('담보종류는 필수 입니다.');
                       $("#onffmerchantFormUpdate select[name=security_cate]").focus();
                       return false;
                   }
                   
                   if($("#onffmerchantFormUpdate input[name=security_amt]").val() == '') 
                   {
                       alert('담보액은 필수 입니다.');
                       $("#onffmerchantFormUpdate input[name=security_amt]").focus();
                       return false;
                   }
                   
                  if($("#onffmerchantFormUpdate input[name=view_security_start_dt]").val() == '') 
                   {
                       alert('시작일은 필수 입니다.');
                       $("#onffmerchantFormUpdate input[name=view_security_start_dt]").focus();
                       return false;
                   }
                   
                  if($("#onffmerchantFormUpdate input[name=view_security_end_dt]").val() == '') 
                   {
                       alert('종료일 필수 입니다.');
                       $("#onffmerchantFormUpdate input[name=view_security_end_dt]").focus();
                       return false;
                   }                   
                   
                   var chkflg = 0;
                   onffmerchant_mygrid.forEachRow(function(id){
                       
                       if(id !=  $("#onffmerchantFormUpdate input[name=security_seq]").val())
                       {    
                        if(strTpStartdt < onffmerchant_mygrid.cells(id,4).getValue())
                        {
                            chkflg = 1;
                        }
                       }
                    
                   });
        
                   if(chkflg==1) 
                   {
                       alert('수정하시려는 담보의 시작일이 기존 등록된 담보의 종료일보다 큽니다.');
                       return false;
                   }
                   
                   $.ajax({
                        url :  "../merchant/merchantDtlUpdate",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#merchantDtlFormInfo").serialize(),
                        success : function(data) {
                            if(data == '1')
                            {
                                alert("수정 완료");
                                onffmerchant_mygrid.clearAll();
                                doSearchMerchantDtlList();
                                clearSelForm();
                            }
                            else
                            {
                                alert("수정 실패");
                            }
                        },
                        error : function() { 
                                alert("수정 실패");
                        }
                    });
                }
                
                function delRow()
                {                 
                   setTransForm();
                   
                   if($("#onffmerchantFormUpdate input[name=security_seq]").val() == '') 
                   {
                       alert('삭제할 담보정보를 선택해주세요.');
                       return false;
                   }        
                   
                   $.ajax({
                        url :  "../merchant/merchantDtlDelete",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#merchantDtlFormInfo").serialize(),
                        success : function(data) {
                            if(data == '1')
                            {
                                alert("삭제 완료");
                                onffmerchant_mygrid.clearAll();
                                doSearchMerchantDtlList();
                                clearSelForm();
                            }
                            else
                            {
                                alert("삭제 실패");
                            }
                        },
                        error : function() { 
                                alert("삭제 실패");
                        }
                    });
                }                
                                                  
                                                
                
                //가맹점정보 수정 등록
                function doUpdateMerchantMaster(){
                    
                    if(!window.confirm("수정하시겠습니까?")){
                        return;
                    }
                    
                    if($("#onffmerchantFormUpdate input[name=merch_nm]").val() == "")
                    {
                      alert("지급ID명은 필수사항 입니다.");                      
                      $("#onffmerchantFormUpdate input[name=merch_nm]").focus();
                      return;
                    }
                    
                    if($("#onffmerchantFormUpdate input[name=tel_1]").val() == "")
                    {
                      alert("연락처1은 필수사항 입니다.");
                      $("#onffmerchantFormUpdate input[name=tel_1]").focus();
                      return;
                    }
                    
                    if($("#onffmerchantFormUpdate select[name=tax_flag]").val() == "")
                    {
                      alert("과세/비과세 구분은 필수사항 입니다.");
                      $("#onffmerchantFormUpdate select[name=tax_flag]").focus();
                      return;
                    }                    
                    
                    if($("#onffmerchantFormUpdate select[name=bal_period]").val() == "")
                    {
                      alert("정산주기는 필수사항 입니다.");
                      $("#onffmerchantFormUpdate select[name=bal_period]").focus();
                      return;
                    }         
                    
                    if($("#onffmerchantFormUpdate select[name=pay_dt_cd_1]").val() == "")
                    {
                      alert("정산기준일은 필수사항 입니다.");
                      $("#onffmerchantFormUpdate select[name=pay_dt_cd_1]").focus();
                      return;
                    }

                    var selbalperiod = $("#bal_period option:selected").val();
                    if(selbalperiod == 'HALF')
                    {
                        if($("#onffmerchantFormUpdate select[name=pay_dt_cd_2]").val() == "")
                        {
                          alert("정산기준일은 필수사항 입니다.");
                          $("#onffmerchantFormUpdate select[name=pay_dt_cd_2]").focus();
                          return;
                        }                                      
                    }

                    if($("#onffmerchantFormUpdate input[name=pay_dt]").val() == "")
                    {
                      alert("지급일은 필수사항 입니다.");
                      $("#onffmerchantFormUpdate input[name=pay_dt]").focus();
                      return;
                    }       
                    
                    if($("#onffmerchantFormUpdate select[name=bank_cd]").val() == "")
                    {
                      alert("입금은행은 필수사항 입니다.");
                      $("#onffmerchantFormUpdate select[name=bank_cd]").focus();
                      return;
                    }                    
                    
                    
                    if($("#onffmerchantFormUpdate input[name=acc_no]").val() == "")
                    {
                      alert("계좌번호는 필수사항 입니다.");
                      $("#onffmerchantFormUpdate input[name=acc_no]").focus();
                      return;
                    }                                        
                    
                    if($("#onffmerchantFormUpdate input[name=agent_seq]").val() != "")
                    {
                        if($("#onffmerchantFormUpdate input[name=agent_commission]").val() == "")
                        {
                          alert("대리점수수료는 필수사항 입니다.");
                          $("#onffmerchantFormUpdate input[name=agent_commission]").focus();
                          return;
                        }               
                    }                                                    
                    
                    if($("#onffmerchantFormUpdate input[name=acc_nm]").val() == "")
                    {
                      alert("계좌주는 필수사항 입니다.");
                      $("#onffmerchantFormUpdate input[name=acc_nm]").focus();
                      return;
                    }            
                    
                    if($("#onffmerchantFormUpdate input[name=view_close_dt]").val() != "")
                    {
                        $("#onffmerchantFormUpdate input[name=close_dt]").attr('value',$("#onffmerchantFormUpdate input[name=view_close_dt]").val().replace(/-/g,''));
                    }

                            
                    var tmpappchkamt = $("#onffmerchantFormUpdate input[name=app_chk_amt]").val();
                    
                    $("#onffmerchantFormUpdate input[name=app_chk_amt]").val(tmpappchkamt.replace(/,/g, ''));
                    
                    if($("#onffmerchantFormUpdate input[name=app_chk_amt]").val() == "")
                    {
                      alert("결제한도는 필수사항 입니다.");
                      $("#onffmerchantFormUpdate input[name=app_chk_amt]").focus();
                      return;
                    }            

                    $.ajax({
                        url : $("#onffmerchantFormUpdate").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#onffmerchantFormUpdate").serialize(),
                        success : function(data) {
                            //alert(data);
                            if(data=='1')
                            {
                                alert("수정 완료");
<%
    if(strTpPopChk.equals(""))
    {
%>                                   
                                onffmerchinfoMasterGrid.clearAll();
                                doSearchMerchantMaster();
                                onffmerchinfoMasterWindow.window("w1").close();
<%
    }
%>
                            }
                            else
                            {
                                alert("수정 실패");
                            }
                        },
                        error : function() { 
                                alert("수정 실패");
                        }
                    });
                }                   
                
               
                //가맹점정보 삭제
                function doDeleteMerchantMaster(){
                    
                    if(!window.confirm("삭제하시겠습니까?")){
                        return;
                    }
                    

                    $.ajax({
                        url :"../merchant/merchantMasterDelete",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#onffmerchantFormUpdate").serialize(),
                        success : function(data) {
                            //alert(data);
                            if(data=='1')
                            {
                                alert("삭제 완료");
                                onffmerchinfoMasterGrid.clearAll();
                                doSearchMerchantMaster();
                                onffmerchinfoMasterWindow.window("w1").close();
                            }
                            else
                            {
                                alert("삭제 실패");
                            }
                        },
                        error : function() { 
                                alert("삭제 실패");
                        }
                    });
                }                                   
                
            </script>
        <div style='width:100%;height:100%;overflow:auto;'>            
            <form id="onffmerchantFormUpdate" method="POST" action="<c:url value="/merchant/merchantMasterUpdate" />" modelAttribute="MerchantFormBean">
                <table class="gridtable" height="100%" width="100%">   
                    <tr>
                        <td align='left' colspan='4'>- 지급ID 사업자 정보</td>                        
                    </tr>
                    <tr>                        
                        <td class="headcol">사업자명*</td>
                        <td><input name="comp_nm" id="comp_nm" readonly  value="${tb_Merchant.comp_nm}"/><input type="hidden" name="comp_seq" value="${tb_Merchant.comp_seq}"/></td>
                        <td class="headcol">개인/법인구분*</tD>
                        <td><input name="comp_cate_nm" id="comp_cate_nm" value="${tb_Merchant.comp_cate_nm}" readonly /></td>
                    </tr>
                     <tr>
                        <td class="headcol">사업자번호*</tD>
                        <td><input name="biz_no" id="biz_no" readonly value="${tb_Merchant.biz_no}"/></td>
                        <td class="headcol">법인번호*</td>
                        <td><input name="corp_no"  readonly value="${tb_Merchant.corp_no}"/></td>
                    </tr>   
                        <tr>
                        <td class="headcol">업태*</tD>
                        <td><input name="comp_biz_type" id="comp_biz_type" readonly value="${tb_Merchant.comp_biz_type}"/></td>
                        <td class="headcol">업종*</td>
                        <td><input name="comp_biz_cate"  readonly value="${tb_Merchant.comp_biz_cate}"/></td>
                    </tr> 
                    <tr>
                        <td class="headcol">과세구분*</td>
                        <td><input name="comp_tax_flag_nm"  readonly value="${tb_Merchant.comp_tax_flag_nm}"/></td>
                        <td class="headcol">대표자명*</tD>
                        <td><input name="comp_ceo_nm"  readonly value="${tb_Merchant.comp_ceo_nm}"/></td>
                    </tr> 
                    <tr>
                        <td class="headcol">연락처1*</td>
                        <td><input name="comp_tel1"  readonly value="${tb_Merchant.comp_tel1}"/></td>
                        <td class="headcol">연락처2</tD>
                        <td><input name="comp_tel2"  readonly value="${tb_Merchant.comp_tel2}"/></td>
                    </tr>
                    <tr>
                        <td class="headcol">우편번호</td>
                        <td colspan="3"><input name="comp_zip_cd" readonly value="${tb_Merchant.comp_zip_cd}" /></td>
                    </tr>   
                    <tr>                        
                        <td class="headcol">주소</tD>
                        <td colspan="3"><input name="comp_addr_1" size="50" readonly value="${tb_Merchant.comp_addr_1}"/>&nbsp;<input name="comp_addr_2" size="50" readonly value="${tb_Merchant.comp_addr_2}"/></td>
                    </tr>   
                    <tr>
                        <td align='left' colspan='4'>- 지급ID 정보</td>                        
                    </tr>
                    <tr>                        
                        <td class="headcol" >지급ID</td>
                        <td ><input name="onffmerch_no" size="25" readonly value="${tb_Merchant.onffmerch_no}"/></td>
                        <td class="headcol" >지급ID명*</td>
                        <td ><input name="merch_nm" size="40" value="${tb_Merchant.merch_nm}"/></td>                        
                    </tr>
                    <tr>                        
                        <td class="headcol" >연락처1*</td>
                        <td ><input name="tel_1" size="20" maxlength="20" value="${tb_Merchant.tel_1}"/></td>
                        <td class="headcol" >연락처2</td>
                        <td ><input name="tel_2" size="20" maxlength="20" value="${tb_Merchant.tel_2}"/></td>                        
                    </tr>
                    <tr>                        
                        <td class="headcol" >E-MAIL</td>
                        <td ><input name="email" size="25" maxlength="100" value="${tb_Merchant.email}"/></td>
                        <td class="headcol">우편번호</td>
                        <td><input name="zip_cd" maxlength="6" value="${tb_Merchant.zip_cd}"/></td>
                    </tr>             
                    <tr>
                        <td class="headcol">주소</tD>
                        <td colspan="3"><input name="addr_1" size="40" value="${tb_Merchant.addr_1}"/>&nbsp;<input name="addr_2" size="40" value="${tb_Merchant.addr_2}"/></td>
                    </tr>  
                    <tr>
                        <td class="headcol">결제한도*</td>
                        <td ><input name="app_chk_amt"  maxlength="15" onkeyPress="javascript:InpuOnlyNumber2(this);"  value="${tb_Merchant.app_chk_amt}" /><span name="insert_amt_han"></span><span>원</span></td>  
                        <td class="headcol">건별결제한도*</td>
                        <td ><input name="appreq_chk_amt" onkeyPress="javascript:InpuOnlyNumber2(this);"  maxlength="10"  value="${tb_Merchant.appreq_chk_amt}" />(미사용시 0원 입력)</td>                                
                    </tr>                             
                     <tr>
                        <td class="headcol">취소권한*</td>
                        <td ><select name="cncl_auth" >
                                <c:forEach var="code" items="${Cncl_AuthList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Merchant.cncl_auth}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach>
                            </select></td>  
                        <td class="headcol">지급ID상태*</td>
                        <td ><select name="svc_stat" >
                                <c:forEach var="code" items="${SvcStatList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Merchant.svc_stat}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach>
                            </select>                         
                        </td>
                    </tr>                      
                    <tr>
                        <td class="headcol">해지일</td>
                        <td><input name="view_close_dt" id="view_close_dt" maxlength="10" value="${tb_Merchant.close_dt}"/><input type="hidden" name="close_dt" value="${tb_Merchant.close_dt}"/></td>
                        <td class="headcol">해지사유</tD>
                        <td><input name="close_reson" size="50" value="${tb_Merchant.close_reson}"/></td>
                    </tr>                    
                    <tr>
                        <td class="headcol">Memo</td>
                        <td colspan="3"><textarea name="memo" cols="60" rows="5">${tb_Merchant.memo}</textarea></td>                        
                    </tr>                          
                    <tr>
                        <td align='left' colspan='4'>- 정산정보</td>                        
                    </tr>
                     <tr>
                        <td class="headcol">정산주기*</td>
                        <td colspan="3"><select name="bal_period" id="bal_period" onchange="javascript:selChangeBalPeriod();" >
                                <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${BalPeriodList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Merchant.bal_period}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach>
                            </select>                         
                        </td>
                    </tr>
                     <tr>                        
                        <td class="headcol">정산기준일*</td>
                        <td colspan="3"><select name="pay_dt_cd_1" id="pay_dt_cd_1" >
                                <option value="">--선택하세요.--</option>
<!--                                
                                <c:forEach var="code" items="${PayDtCdList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Merchant.pay_dt_cd_1}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach>                                
-->

                            </select>&nbsp;<select name="pay_dt_cd_2" id="pay_dt_cd_2" >
                                <option value="">--선택하세요.--</option>
<!--                                
                                <c:forEach var="code" items="${PayDtCdList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Merchant.pay_dt_cd_2}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach>       
-->
                            </select>   
                        </td>                        
                    </tr>
                    <tr>
                        <td class="headcol">지급일*</td>
                        <td >정산대상 최종승인일+<input name="pay_dt" size="3" value="${tb_Merchant.pay_dt}" />(영업일)</td>   
                        <td class="headcol">입금은행*</td>
                        <td><select name="bank_cd" >
                                <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${BankCdList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Merchant.bank_cd}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach>
                            </select></td>      
                    <tr>
                        <td class="headcol">입금계좌*</td>
                        <td><input name="acc_no" maxlength="30" size="30" onkeyPress="javascript:InpuOnlyNumber(this);" value="${tb_Merchant.acc_no}" /></td>
                        <td class="headcol">계좌주*</tD>
                        <td ><input name="acc_nm" size="50"  value="${tb_Merchant.acc_nm}"/></td>                        
                    </tr>  
                    <tr>
                        <td align='left' colspan='4'>- 대리점 정보</td>                        
                    </tr>
                    <tr>                        
                        <td class="headcol" >대리점명</td>
                        <td><input name="agent_nm" id="agent_nm" value="${tb_Merchant.agent_nm}"  readonly  onclick="javascript:AgentSelectWin();"/><input type="hidden" name="agent_seq" id="agent_seq" value="${tb_Merchant.agent_seq}"/></td>
                        <td class="headcol">대리점수수료</tD>
                        <td ><input name="agent_commission"  onkeyPress="javascript:InpuOnlyNumber(this);" value="${tb_Merchant.agent_commission}" /></td>    
                    </tr>
                    <tr>
                        <td colspan="4" align="center"><input type="button" name="update" value="수정"/>&nbsp;<input type="button" name="delete" value="삭제"/>&nbsp;<input type="button" name="close" value="닫기"/></td>
                    </tr>                           
                    <tr>
                        <td align='left' colspan='4'>- 담보 정보</td>                        
                    </tr>
                    <tr>                        
                        <td class="headcol" >담보종류*</td>
                        <td><select name="security_cate" id="security_cate" >
                                <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${SecurityCateList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select></td>
                        <td class="headcol">담보액*</tD>
                        <td ><input name="security_amt"  onkeyPress="javascript:InpuOnlyNumber2(this);" maxlength="15" /><span name="insert_security_amt_han"></span><span>원</span><input type="hidden" name="security_seq" id="security_seq"/></td>    
                    </tr>     
                    <tr>
                        <td class="headcol">시작일*</td>
                        <td><input name="view_security_start_dt" id="view_security_start_dt" maxlength="10" value="" readonly/><input type="hidden" name="security_start_dt"/></td>
                        <td class="headcol">종료일*</tD>
                        <td><input name="view_security_end_dt" id="view_security_end_dt" maxlength="10" value="" readonly/><input type="hidden" name="security_end_dt"/></td>
                    </tr>  
                    <tr>
                        <td class="headcol">Memo</td>
                        <td colspan="3"><textarea name="security_memo" cols="60" rows="3"></textarea></td>                        
                    </tr>                              
                    <tr>
                        <td align='left' colspan='4' valign="top">
                            <div id = "common_btn" align = "right" style="height:30px;" >
                            <input type="button" name="security_addbt" value="담보추가" onclick="javascript:addRow();"/>
                            <input type="button" name="security_addbt" value="담보수정" onclick="javascript:modRow();"/>
                            <input type="button" name="security_delbt" value="담보삭제" onclick="javascript:delRow();"/>
                            </div>
                            <div id="onffmerchantVP2" style="position: relative; width:100%;height:200px;background-color:white;"></div>
                        </td>                        
                    </tr>   
                </table>
            </form>
<form name="merchantDtlFormInfo" id="merchantDtlFormInfo" method="POST"  modelAttribute="SecurityFormBean">
    <input type="hidden" name="security_seq" />
    <input type="hidden" name="onffmerch_no" value="${tb_Merchant.onffmerch_no}" />
    <input type="hidden" name="security_cate"/>
    <input type="hidden" name="security_amt"/>
    <input type="hidden" name="start_dt"/>
    <input type="hidden" name="end_dt"/>    
    <input type="hidden" name="memo"/>
</form>                    
        </div>
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

