<%-- 
    Document   : acq_resultChkDetailList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>상세조회</title>
        </head>
        <body>
</c:if>
            <script type="text/javascript">

                var acq_resultChkdetailgrid={};
                var acq_resultChkdetail_layout={};
                
                var acq_resultChkCalendar = new dhtmlXCalendarObject(["detail_view_pay_dt_start","detail_view_pay_dt_end","detail_view_sal_start_dt","detail_view_sal_end_dt"]);    

                $(document).ready(function () {

                    var acq_resultChkdetailForm = document.getElementById("acq_resultChkdetail_serch");

                    acq_resultChkdetail_layout = acq_resultChk_master_layout.cells('b').attachLayout("2E");

                    acq_resultChkdetail_layout.cells('a').hideHeader();

                    acq_resultChkdetail_layout.cells('b').hideHeader();

                    acq_resultChkdetail_layout.cells('a').attachObject(acq_resultChkdetailForm);

                    acq_resultChkdetail_layout.cells('a').setHeight(60);

                    acq_resultChkdetailgrid = acq_resultChkdetail_layout.cells('b').attachGrid();

                    acq_resultChkdetailgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                

                    var acq_resultChkdetailheaders = "";
                    acq_resultChkdetailheaders += "<center>NO</center>,<center>입금예정일</center>,<center>가맹점번호</center>,<center>가맹점명</center>,<center>거래종류</center>,<center>거래대사</center>,<center>매입사</center>";
                    acq_resultChkdetailheaders += ",<center>승인일</center>,<center>원승인일</center>,<center>승인번호</center>,<center>승인금액</center>,<center>할부</center>";
                    acq_resultChkdetailheaders += ",<center>매입일</center>,<center>입금일</center>,<center>수수료</center>,<center>수수료부가세</center>,<center>입금예정금액</center>";
                    
                    acq_resultChkdetailgrid.setHeader(acq_resultChkdetailheaders);
                    acq_resultChkdetailgrid.setColAlign("center,center,center,center,center,center,center,center,center,center,right,center,center,center,right,right,right");
                    acq_resultChkdetailgrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,edn,txt,txt,txt,edn,edn,edn");
                    acq_resultChkdetailgrid.setInitWidths("50,100,100,100,100,100,100,100,100,100,100,120,100,100,100,100,100");
                    acq_resultChkdetailgrid.setColSorting("str,str,str,str,str,str,str,str,str,str,int,str,str,str,int,int,int");
                    acq_resultChkdetailgrid.setNumberFormat("0,000",10);            
                    acq_resultChkdetailgrid.setNumberFormat("0,000",14);
                    acq_resultChkdetailgrid.setNumberFormat("0,000",15);
                    acq_resultChkdetailgrid.setNumberFormat("0,000",16);
		    acq_resultChkdetailgrid.setColumnHidden(13,true);
                    acq_resultChkdetailgrid.enableColumnMove(true);
                    acq_resultChkdetailgrid.setSkin("dhx_skyblue");
                    acq_resultChkdetailgrid.enablePaging(true,Number($("#acq_resultChkdetailForm input[name=page_size]").val()),10,"acq_resultChkdetailPaging",true,"acq_resultChkdetailrecinfoArea");
                    acq_resultChkdetailgrid.setPagingSkin("bricks");
                    acq_resultChkdetailgrid.init();
                    acq_resultChkdetailgrid.parse(${ls_acq_resultChkDetailList},"json");
                    
                    //검색조건 초기화
                    $("#acq_resultChkdetailForm input[name=init]").click(function () {

                    acqdepositDetailMasterInit($("#acq_resultChkdetailForm"));

                    });  
            
                    //페이징 처리
                    acq_resultChkdetailgrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                       
                        if(lInd !==0){
                            $("#acq_resultChkdetailForm input[name=page_no]").val(ind);
                            acq_resultChkdetailgrid.clearAll();
                            acq_resultChkdetailSearch();
                        }else{
                            $("#acq_resultChkdetailForm input[name=page_no]").val(1);
                            acq_resultChkdetailgrid.clearAll();
                            acq_resultChkdetailSearch();
                        }                        
                        
                    });

                    //상세코드검색
                    $("#acq_resultChkdetailForm input[name=acq_resultChkdetail_excel]").click(function(){

                        acq_resultChkdetailSearchExcel();

                    });
                    
                    //상세코드검색
                    $("#acq_resultChkdetailForm input[name=acq_resultChkdetail_serch]").click(function(){
                        /*
                        $("input[name=page_no]").val("1");

                        acq_resultChkdetailgrid.clearAll();

                        acq_resultChkdetailSearch();
                        */
                        acq_resultChkdetailgrid.changePage(1);

                    });                    

                    //엑셀다운로드
                    function acq_resultChkdetailSearchExcel() {
                    $("#acq_resultChkdetailForm input[name=pay_dt_start]").val(($("#acq_resultChkdetailForm input[name=view_pay_dt_start]").val()).replace( /[^(0-9)]/g,""));
                    $("#acq_resultChkdetailForm input[name=pay_dt_end]").val(($("#acq_resultChkdetailForm input[name=view_pay_dt_end]").val()).replace( /[^(0-9)]/g,""));
                    $("#acq_resultChkdetailForm input[name=sal_start_dt]").val(($("#acq_resultChkdetailForm input[name=view_sal_start_dt]").val()).replace( /[^(0-9)]/g,""));
                    $("#acq_resultChkdetailForm input[name=sal_end_dt]").val(($("#acq_resultChkdetailForm input[name=view_sal_end_dt]").val()).replace( /[^(0-9)]/g,""));         

                        $("#acq_resultChkdetailForm").attr("action","<c:url value="/acq_result/acq_resultChkDetailListExcel" />");
                        document.getElementById("acq_resultChkdetailForm").submit();

                    }         
                    
                });

                //코드 정보 조회
                function acq_resultChkdetailSearch() {
                
                    $("#acq_resultChkdetailForm input[name=pay_dt_start]").val(($("#acq_resultChkdetailForm input[name=view_pay_dt_start]").val()).replace( /[^(0-9)]/g,""));
                    $("#acq_resultChkdetailForm input[name=pay_dt_end]").val(($("#acq_resultChkdetailForm input[name=view_pay_dt_end]").val()).replace( /[^(0-9)]/g,""));
                    $("#acq_resultChkdetailForm input[name=sal_start_dt]").val(($("#acq_resultChkdetailForm input[name=view_sal_start_dt]").val()).replace( /[^(0-9)]/g,""));
                    $("#acq_resultChkdetailForm input[name=sal_end_dt]").val(($("#acq_resultChkdetailForm input[name=view_sal_end_dt]").val()).replace( /[^(0-9)]/g,""));         
                
                $.ajax({
                    url: "<c:url value="/acq_result/acq_resultChkDetailList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#acq_resultChkdetailForm").serialize(),
                    success: function (data) {

                        var jsonData = $.parseJSON(data);
                        //acq_resultChkdetailgrid.clearAll();
                        acq_resultChkdetailgrid.parse(jsonData,"json");

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
                    return false;
            }
            
            function depositdetailsetSens(id, k) {
                // update range
                if (k == "min") {
                    acq_resultChkCalendar.setSensitiveRange(byId(id).value, null);
                } else {
                    acq_resultChkCalendar.setSensitiveRange(null, byId(id).value);
                }
            }

             function byId(id) {
                return document.getElementById(id);
            }                               
            
                
                function acqdepositDetailMasterInit($form) {
                    searchFormInit($form);
                } 

            
    </script>
    <body>
        <div class="right" id="acq_resultChkdetail_serch" style="width:100%;">
             <form id="acq_resultChkdetailForm" method="POST" action="<c:url value="/acq_result/acq_resultChkDetailList" />" modelAttribute="acq_ResultFormBean">
                        <input type="hidden" name="page_size" value="100" >
                        <input type="hidden" name="page_no" value="1" >
                        <input type ="hidden" id ="pay_dt_start" name ="pay_dt_start" />
                        <input type ="hidden" id ="pay_dt_end" name ="pay_dt_end" />
                        <input type ="hidden" id ="sal_start_dt" name ="sal_start_dt"/>
                        <input type ="hidden" id ="sal_end_dt" name ="sal_end_dt"/>                               
                        <table>
                            <tr>
                                <td>
                                <div class="label">입금일자</div>
                                <div class ="input">
                                    <input name="view_pay_dt_start" id="detail_view_pay_dt_start" size="10" value='${acq_ResultFormBean.pay_dt_start}' onclick="acq_resultChksetSens('view_pay_dt_end', 'max');" />~
                                    <input name="view_pay_dt_end" id="detail_view_pay_dt_end" size="10" value='${acq_ResultFormBean.pay_dt_end}' onclick="acq_resultChksetSens('view_pay_dt_start', 'min');" />
                                </div> 
                                <div class="label">승인일자</div>
                                <div class ="input">
                                    <input name="view_sal_start_dt" id="detail_view_sal_start_dt" size="10" value='${acq_ResultFormBean.sal_start_dt}' onclick="acq_resultChksetSens('view_sal_end_dt', 'max');" />~
                                    <input name="view_sal_end_dt" id="detail_view_sal_end_dt" size="10" value='${acq_ResultFormBean.sal_end_dt}' onclick="acq_resultChksetSens('view_sal_start_dt', 'min');" />
                                </div> 
                                <!--
                                    <div class="label">결제채널</div>
                                    <div class="input">
                                        <select name="pay_type">
                                            <option value="">전체</option>
                                            <c:forEach var="code" items="${payChnCateList.tb_code_details}">
                                                <option value="${code.detail_code}">${code.code_nm}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                -->
                                    <div class="label">거래종류</div>
                                    <div class="input">
                                        <select name="record_cl_cd">
                                            <option value="">전체</option>
                                            <c:forEach var="code" items="${record_cl_cdCdList.tb_code_details}">
                                                <option <c:if test="${code.detail_code == acq_ResultFormBean.record_cl_cd}">selected</c:if>  value="${code.detail_code}">${code.code_nm}</option>
                                            </c:forEach>
                                        </select>
                                    </div>    
                                      <div class="label">거래대사</div>
                                    <div class="input">
                                        <select name="tran_chk_flag">
                                            <option value="">전체</option>
                                            <c:forEach var="code" items="${tran_chk_flagCdList.tb_code_details}">
                                                <option <c:if test="${code.detail_code == acq_ResultFormBean.tran_chk_flag}">selected</c:if> value="${code.detail_code}">${code.code_nm}</option>
                                            </c:forEach>
                                        </select>
                                    </div>                                          
                                <div class="label">가맹점명</div>
                                <div class="input"><input type="text" name="mid_nm" size="12" value="${acq_ResultFormBean.mid_nm}" /></div>                                    
                                <div class="label">가맹점번호</div>
                                <div class="input"><input type="text" name="merch_no" size="12" value="${acq_ResultFormBean.merch_no}" /></div>                                
                                <div class="label">승인번호</div>
                                <div class="input"><input type="text" name="aprv_no" size="10"/></div>
                                <div class="label">승인금액</div>
                                <div class="input"><input type="text" name="tot_trx_amt" size="12"/></div>
                                <!--
                                <div class="label">매입사</div>
                                <div class="input">
                                    <select name="acq_cd">
                                        <option value="">전체</option>
                                        <c:forEach var="code" items="${acqCdList.tb_code_details}">
                                            <option value="${code.detail_code}">${code.code_nm}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                -->
                            </td>                                   
                                <td rowspan='2'>
                                    <input type="button" name="acq_resultChkdetail_serch" value="조회"/>
                                </td>
                                <td rowspan='2'>
                                    <input type="button" name="acq_resultChkdetail_excel" value="엑셀"/>
                                </td>
                                <td><input type="button" name="init" value="검색조건지우기"/></td>
                            </tr>
                        </table>
             </form>

            <div id="acq_resultChkdetailPaging" style="width: 100%;"></div>
            <div id="acq_resultChkdetailrecinfoArea" style="width: 100%;"></div>

        </div>
        
        <div id="acq_resultChkdetail" style="position: relative;width:100%;height:100%;background-color:white;"></div>
      
    </body>
</html>
