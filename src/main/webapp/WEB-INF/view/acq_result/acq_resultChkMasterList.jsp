<%-- 
    Document   : acq_resultChkMasterList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>매입세부내역 확인</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">
                acq_resultChk_popdhxWins = new dhtmlXWindows();
                acq_resultChk_popdhxWins.attachViewportTo("acq_resultChk_Master_dhx_search");
                var acqReusltChkInsertCalendar;
                
                $(document).ready(function () {
                    acqReusltChkInsertCalendar = new dhtmlXCalendarObject(["view_pay_dt_start","view_pay_dt_end","view_sal_start_dt","view_sal_end_dt"]);
                    var tabBarId = tabbar.getActiveTab();
                    var acq_resultChk_MasterSearch = document.getElementById("acq_resultChk_Master_dhx_search");
                    acq_resultChk_master_layout = tabbar.cells(tabBarId).attachLayout("2E");
                    acq_resultChk_master_layout.cells('a').hideHeader();
                    acq_resultChk_master_layout.cells('b').hideHeader();

                    acq_resultChk_Master_layout = acq_resultChk_master_layout.cells('a').attachLayout("2E");
                    
                    acq_resultChk_Master_layout.cells('a').hideHeader();
                    acq_resultChk_Master_layout.cells('b').hideHeader();
                    acq_resultChk_Master_layout.cells('a').attachObject(acq_resultChk_MasterSearch);
                    acq_resultChk_Master_layout.cells('a').setHeight(30);
                    acq_resultChk_Master_mygrid = acq_resultChk_Master_layout.cells('b').attachGrid();
                    acq_resultChk_Master_mygrid.setImagePath("<c:url value ='/resources/images/dhx/dhtmlxGrid/'/>");
                    var mheaders = "";
                    mheaders += "<center>입금일</center>,<center>가맹점번호</center>,<center>가맹점번호명</center>,<center>승인일</center>,<center>입반구분</center>,<center>거래대사</center>";
                    mheaders += ",<center>건수</center>,<center>입금예정액</center>,<center>매입대상액</center>,<center>수수료</center>,<center>수수료부가세</center>";
                    
                    acq_resultChk_Master_mygrid.setHeader(mheaders);
                    var colsel = "";
                    colsel += "center,center,center,center,center,center,right,right,right,right,right";
                    acq_resultChk_Master_mygrid.setColAlign(colsel);
                    var coltype = "txt,txt,txt,txt,txt,txt,edn,edn,edn,edn,edn";
                    
                    acq_resultChk_Master_mygrid.setColTypes(coltype);
                    var colwiths = "80,90,120,80,90,100,60,120,120,120,120";
                    
                    acq_resultChk_Master_mygrid.setInitWidths(colwiths);
                    var colsort  = "str,str,str,str,str,str,int,int,int,int,int";
                    
                    acq_resultChk_Master_mygrid.setColSorting(colsort);
                    
                    acq_resultChk_Master_mygrid.setNumberFormat("0,000",7);
                    acq_resultChk_Master_mygrid.setNumberFormat("0,000",8);
                    acq_resultChk_Master_mygrid.setNumberFormat("0,000",9);
                    acq_resultChk_Master_mygrid.setNumberFormat("0,000",10);
                    acq_resultChk_Master_mygrid.setNumberFormat("0,000",11);
                    
                    acq_resultChk_Master_mygrid.enableColumnMove(true);

                    acq_resultChk_Master_mygrid.attachEvent("onRowDblClicked", acq_resultChk_Master_attach);

                    acq_resultChk_Master_mygrid.setSkin("dhx_skyblue");
                    
                    var acq_resultChk_Master_mygridFooters = "총계,#cspan,#cspan,#cspan,#cspan,#cspan,<div id='acq_resultChk_Master_mygrid6'>0</div>,<div id='acq_resultChk_Master_mygrid7'>0</div>,<div id='acq_resultChk_Master_mygrid8'>0</div>,<div id='acq_resultChk_Master_mygrid9'>0</div>,<div id='acq_resultChk_Master_mygrid10'>0</div>";
                    acq_resultChk_Master_mygrid.attachFooter(acq_resultChk_Master_mygridFooters);
                    
                    acq_resultChk_Master_mygrid.groupBy(0,["#title","#cspan","#cspan","#cspan","#cspan","#cspan","#stat_total","#stat_total","#stat_total","#stat_total","#stat_total"]);
                    
                    acq_resultChk_Master_mygrid.init();

                    acq_resultChk_Master_mygrid.parse(${ls_acq_resultChkMasterList}, acq_resultChk_Master_mygridFooterValues, "json");
                    
                    //검색조건 초기화
                    $("#acq_resultChk_Master_Form input[name=init]").click(function () {

                    acq_resultChk_DetailMasterInit($("#acq_resultChk_Master_Form"));

                    });  

                    $("#acq_resultChk_Master_Form input[name=week]").click(function(){
                       acqdeposite_date_search("week");
                    });
                    
                      $("#acq_resultChk_Master_Form input[name=month]").click(function(){
                       acqdeposite_date_search("month");
                    });
                    
                    
                    //버튼 추가
                    $("#acq_resultChk_Master_Form input[name=searchButton]").bind("click",function(e){
                        doSearchAcq_DepositeMaster();
                        return false;
                    });
                    
                    //버튼 추가
                    $("#acq_resultChk_Master_Form input[name=excelButton]").bind("click",function(e){
                        doSearchAcq_DepositeMasterExcel();
                        return false;
                    });                    

                });
                
                function acq_resultChk_Master_mygridFooterValues() {
                    
                    var acq_resultChk_Master_mygrid6 = document.getElementById("acq_resultChk_Master_mygrid6");
                    var acq_resultChk_Master_mygrid7 = document.getElementById("acq_resultChk_Master_mygrid7");
                    var acq_resultChk_Master_mygrid8 = document.getElementById("acq_resultChk_Master_mygrid8");
                    var acq_resultChk_Master_mygrid9 = document.getElementById("acq_resultChk_Master_mygrid9");
                    var acq_resultChk_Master_mygrid10 = document.getElementById("acq_resultChk_Master_mygrid10");
                    acq_resultChk_Master_mygrid6.innerHTML = putComma(sumColumn(6));
                    acq_resultChk_Master_mygrid7.innerHTML = putComma(sumColumn(7));
                    acq_resultChk_Master_mygrid8.innerHTML = putComma(sumColumn(8));
                    acq_resultChk_Master_mygrid9.innerHTML = putComma(sumColumn(9));
                    acq_resultChk_Master_mygrid10.innerHTML = putComma(sumColumn(10));

                    return true;
                }                
                
                function sumColumn(ind) {
                    
                    var out = 0;
                    for (var i = 0; i < acq_resultChk_Master_mygrid.getRowsNum(); i++) {
                         out += parseFloat(acq_resultChk_Master_mygrid.cells2(i, ind).getValue());
                    }

                    return out;
                    
                }                        
            
                function doSearchAcq_DepositeMaster(){

                    $("#acq_resultChk_Master_Form input[name=pay_dt_start]").val(($("#acq_resultChk_Master_Form input[name=view_pay_dt_start]").val()).replace( /[^(0-9)]/g,""));
                    $("#acq_resultChk_Master_Form input[name=pay_dt_end]").val(($("#acq_resultChk_Master_Form input[name=view_pay_dt_end]").val()).replace( /[^(0-9)]/g,""));
                    $("#acq_resultChk_Master_Form input[name=sal_start_dt]").val(($("#acq_resultChk_Master_Form input[name=view_sal_start_dt]").val()).replace( /[^(0-9)]/g,""));
                    $("#acq_resultChk_Master_Form input[name=sal_end_dt]").val(($("#acq_resultChk_Master_Form input[name=view_sal_end_dt]").val()).replace( /[^(0-9)]/g,""));                    
                    
                    if(isValidDate($("#pay_dt_start").val()) ){
                    
                    }else{
                        alert("날짜 형식을 확인하세요");
                        return false;
                    }
                    
                    $.ajax({
                        url : "<c:url value = '/acq_result/acq_resultChkMasterList'/>",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#acq_resultChk_Master_Form").serialize(),
                        success : function(data) {
                            acq_resultChk_Master_mygrid.clearAll();
                            var jsonData = $.parseJSON(data);
                            acq_resultChk_Master_mygrid.parse(jsonData, acq_resultChk_Master_mygridFooterValues, "json");
                            acq_resultChk_Master_mygrid.groupBy(0,["#title","#cspan","#cspan","#cspan","#cspan","#cspan","#stat_total","#stat_total","#stat_total","#stat_total","#stat_total"]);
                        },
                        error : function() { 
                                alert("조회 실패");
                        }
                    });	            
                }
            
                function acq_resultChk_Master_attach(rowid, col) {

                    var pay_dt = acq_resultChk_Master_mygrid.getUserData(rowid, 'pay_dt').replace( /[^(0-9)]/g,"");
                    var merch_no = acq_resultChk_Master_mygrid.getUserData(rowid, 'merch_no');
                    var sal_dt = acq_resultChk_Master_mygrid.getUserData(rowid, 'sal_dt');
                    var record_cl_cd = acq_resultChk_Master_mygrid.getUserData(rowid, 'record_cl_cd');
                    var tran_chk_flag = acq_resultChk_Master_mygrid.getUserData(rowid, 'tran_chk_flag');
                    
                    var param = "?pay_dt_start=" +pay_dt+"&pay_dt_end="  +pay_dt+"&merch_no="  +merch_no+"&sal_start_dt="  +sal_dt+"&sal_end_dt="  +sal_dt+"&record_cl_cd="  +record_cl_cd+"&tran_chk_flag="  +tran_chk_flag; 
                    acq_resultChk_master_layout.cells('b').attachURL("<c:url value = '/acq_result/acq_resultChkDetailList'/>"+param , true);
                }

                
                function acqdeposite_date_search(day){
                    var oneDate = 1000 * 3600 * 24; 
                    var now = new Date();
                    var week = new Date(now.getTime() + (oneDate*-6));
                    var month = new Date(now.getTime() + (oneDate*-31));
                    var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                    var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                    var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                    var date=new Date();
                    if(day=="today"){
                        this.$("input[id=view_pay_dt_start]").val(nowTime);
                        this.$("input[id=view_pay_dt_end]").val(nowTime);
                    }else if(day=="week"){
                        this.$("input[id=view_pay_dt_start]").val(weekTime);
                        this.$("input[id=view_pay_dt_end]").val(nowTime);
                    }else{
                        this.$("input[id=view_pay_dt_start]").val(monthTime);
                        this.$("input[id=view_pay_dt_end]").val(nowTime);
                    }
                }
                
                function acq_resultChk_DetailMasterInit($form) {
                    searchFormInit($form);
                    acqdeposite_date_search("week");                    
                } 
                function acq_resultChksetSens(id, k) {
                    // update range
                    if (k == "min") {
                        acqReusltChkInsertCalendar.setSensitiveRange(byId(id).value, null);
                    } else {
                        acqReusltChkInsertCalendar.setSensitiveRange(null, byId(id).value);
                    }
                }
			
                function byId(id) {
                    return document.getElementById(id);
                }             
                
                //엑셀다운로드
                function doSearchAcq_DepositeMasterExcel() {
                    $("#acq_resultChk_Master_Form input[name=pay_dt_start]").val(($("#acq_resultChk_Master_Form input[name=view_pay_dt_start]").val()).replace( /[^(0-9)]/g,""));
                    $("#acq_resultChk_Master_Form input[name=pay_dt_end]").val(($("#acq_resultChk_Master_Form input[name=view_pay_dt_end]").val()).replace( /[^(0-9)]/g,""));
                    $("#acq_resultChk_Master_Form input[name=sal_start_dt]").val(($("#acq_resultChk_Master_Form input[name=view_sal_start_dt]").val()).replace( /[^(0-9)]/g,""));
                    $("#acq_resultChk_Master_Form input[name=sal_end_dt]").val(($("#acq_resultChk_Master_Form input[name=view_sal_end_dt]").val()).replace( /[^(0-9)]/g,""));   
                    
                    $("#acq_resultChk_Master_Form").attr("action","<c:url value="/acq_result/acq_resultChkMasterListExcel" />");
                    document.getElementById("acq_resultChk_Master_Form").submit();

                }                       
            
            </script>

            <div class="right" id="acq_resultChk_Master_dhx_search" style="width:100%; height:100%">
                <form id="acq_resultChk_Master_Form" method="POST" action="<c:url value = '/acq_result/acq_resultChkMasterList'/>">
                    <input type ="hidden" id ="pay_dt_start" name ="pay_dt_start"/>
                    <input type ="hidden" id ="pay_dt_end" name ="pay_dt_end"/>
                    <input type ="hidden" id ="sal_start_dt" name ="sal_start_dt"/>
                    <input type ="hidden" id ="sal_end_dt" name ="sal_end_dt"/>                    
                        <div class="label">입금일자</div>
                        <div class ="input">
                            <input name="view_pay_dt_start" id="view_pay_dt_start" value='${acq_ResultFormBean.pay_dt_start}' onclick="acq_resultChksetSens('view_pay_dt_end', 'max');" />~
                            <input name="view_pay_dt_end" id="view_pay_dt_end" value='${acq_ResultFormBean.pay_dt_end}' onclick="acq_resultChksetSens('view_pay_dt_start', 'min');" />
                        </div> 
                        <div class="label">승인일자</div>
                        <div class ="input">
                            <input name="view_sal_start_dt" id="view_sal_start_dt" value='${acq_ResultFormBean.sal_start_dt}' onclick="acq_resultChksetSens('view_sal_end_dt', 'max');" />~
                            <input name="view_sal_end_dt" id="view_sal_end_dt" value='${acq_ResultFormBean.sal_end_dt}' onclick="acq_resultChksetSens('view_sal_start_dt', 'min');" />
                        </div> 
                        <div class ="label">가맹점번호</div>
                        <div class ="input"><input type="text" name="merch_no" value="${acq_ResultFormBean.merch_no}" /></div>
                        <div class ="label">가맹점명</div>
                        <div class ="input"><input type="text" name="mid_nm" value="${acq_ResultFormBean.mid_nm}" /></div>                        
                        <td><input type="button" name="searchButton" value="조회"/></td>
                        <td><input type="button" name="init" value="초기화"/></td>
                        <td><input type="button" name="excelButton" value="엑셀"/></td>                        
                </form>
            </div>
            

<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

