<%-- 
    Document   : codeDetailMaster
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>매입내역 상세조회</title>
        </head>
        <body>
</c:if>
            
        <div class="right" id="acqresultdetail_serch" style="width:100%;">
             <form id="acqresultdetailForm" method="POST" action="<c:url value="/acq_result/acq_resultMasterDetailPop" />" modelAttribute="acq_ResultFormBean">
                        <input type="hidden" name="page_size" value="100" >
                        <input type="hidden" name="page_no" value="1" >
                        <table>
                            <tr>
                                <td>
                                    <div class="label">승인일</div>
                                    <div class="input">
                                        <input type="text" name="sal_start_dt" size="12" id="result_middepositDetail_from_date" value ="${acq_ResultFormBean.sal_start_dt}" onclick="acq_resultdetailsetSens('result_middepositDetail_to_date', 'max');" /> ~
                                        <input type="text" name="sal_end_dt" size="12" id="result_middepositDetail_to_date" value="${acq_ResultFormBean.sal_end_dt}" onclick="acq_resultdetailsetSens('result_middepositDetail_from_date', 'min');" />
                                    </div>
                                    <div class ="input"><input type="button" name="week" value="1주"/></div>
                                    <div class ="input"><input type="button" name="month" value="1달"/></div>
                                    <div class="label">매입일</div>
                                    <div class="input">
                                        <!--
                                        <input type="text" name="acq_dt" id="result_middepositDetail_acq_dt" value="${acq_ResultFormBean.acq_dt}" />
                                        -->
                                        <input type="text" name="acq_start_dt" size="12" id="result_middepositDetail_acq_start_dt" value="${acq_ResultFormBean.acq_start_dt}" /> ~
                                        <input type="text" name="acq_end_dt" size="12" id="result_middepositDetail_acq_end_dt" value="${acq_ResultFormBean.acq_end_dt}" />
                                    </div>                                    
                                    <div class="label">결제채널</div>
                                    <div class="input">
                                        <select name="pay_type">
                                            <option value="">전체</option>
                                            <c:forEach var="code" items="${payChnCateList.tb_code_details}">
                                                <option value="${code.detail_code}" <c:if test="${code.detail_code == acq_ResultFormBean.pay_type}">selected</c:if> >${code.code_nm}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                <div class="label">가맹점번호</div>
                                <div class="input"><input type="text" name="merch_no" value="${acq_ResultFormBean.merch_no}" /></div>                                
                                <div class="label">승인번호</div>
                                <div class="input"><input type="text" name="aprv_no" /></div>
                                <div class="label">승인금액</div>
                                <div class="input"><input type="text" name="tot_trx_amt" /></div>
                                <div class="label">매입사</div>
                                <div class="input">
                                    <select name="acq_cd">
                                        <option value="">전체</option>
                                        <c:forEach var="code" items="${acqCdList.tb_code_details}">
                                            <option value="${code.detail_code}">${code.code_nm}</option>
                                        </c:forEach>
                                    </select>
                                </div>                      
                            </td>                                   
                                <td rowspan='2'>
                                    <input type="button" name="acqresultdetail_serch" value="조회"/>
                                </td>                                
                                <td rowspan='2'>
                                    <input type="button" name="acqresultdetail_excel" value="엑셀"/>
                                </td>
                                <td><input type="button" name="init" value="검색조건지우기"/></td>
                            </tr>
                        </table>
             </form>

            <div id="acqresultdetailPaging" style="width: 100%;"></div>
            <div id="acqresultdetailrecinfoArea" style="width: 100%;"></div>

        </div>
        
        <div id="acqresultdetail" style="position: relative;width:100%;height:100%;background-color:white;"></div>            
            
            <script type="text/javascript">

                var acqresultdetailgrid={};
                var acqresultdetail_layout={};
                
                var acq_resultDetailCalendar = new dhtmlXCalendarObject(["result_middepositDetail_from_date","result_middepositDetail_to_date","result_middepositDetail_acq_start_dt","result_middepositDetail_acq_end_dt"]);    

                $(document).ready(function () {

                    var acqresultdetailForm = document.getElementById("acqresultdetail_serch");

                    acqresultdetail_layout = acq_result_master_layout.cells('b').attachLayout("2E");

                    acqresultdetail_layout.cells('a').hideHeader();

                    acqresultdetail_layout.cells('b').hideHeader();

                    acqresultdetail_layout.cells('a').attachObject(acqresultdetailForm);

                    acqresultdetail_layout.cells('a').setHeight(60);

                    acqresultdetailgrid = acqresultdetail_layout.cells('b').attachGrid();

                    acqresultdetailgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                

                    var acqresultdetailheaders = "";
                    acqresultdetailheaders += "<center>NO</center>,<center>결제채널</center>,<center>가맹점번호</center>,<center>거래종류</center>,<center>매입사</center>";
                    acqresultdetailheaders += ",<center>승인일</center>,<center>승인번호</center>,<center>승인금액</center>,<center>할부</center>";
                    acqresultdetailheaders += ",<center>매입일</center>,<center>입금예정일</center>,<center>수수료</center>,<center>수수료부가세</center>,<center>입금예정금액</center>";
                    
                    acqresultdetailgrid.setHeader(acqresultdetailheaders);
                    acqresultdetailgrid.setColAlign("center,center,center,center,center,center,center,right,right,center,center,center,right,right");
                    acqresultdetailgrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,edn,txt,txt,txt,edn,edn,edn");
                    acqresultdetailgrid.setInitWidths("50,100,100,100,100,100,100,100,120,100,100,100,100,100");
                    acqresultdetailgrid.setColSorting("str,str,str,str,str,str,str,int,str,str,str,int,int,int");
                    acqresultdetailgrid.setNumberFormat("0,000",7);            
                    acqresultdetailgrid.setNumberFormat("0,000",12);
                    acqresultdetailgrid.setNumberFormat("0,000",13);
                    acqresultdetailgrid.setNumberFormat("0,000",14);
                    acqresultdetailgrid.enableColumnMove(true);
                    acqresultdetailgrid.setSkin("dhx_skyblue");                    
                    acqresultdetailgrid.enablePaging(true,Number($("#acqresultdetailForm input[name=page_size]").val()),10,"acqresultdetailPaging",true,"acqresultdetailrecinfoArea");
                    acqresultdetailgrid.setPagingSkin("bricks");
                    acqresultdetailgrid.init();
                    acqresultdetailgrid.parse(${ls_middepositDetailList},"json");
                    
                    //검색조건 초기화
                    $("#acqresultdetailForm input[name=init]").click(function () {
                        acqresultdetailFormInit($("#acqresultdetailForm"));
                    });  

                    
                    $("#acqresultdetailForm input[name=week]").click(function(){
                       acqresult_date_search_detail("week");
                    });
                    
                      $("#acqresultdetailForm input[name=month]").click(function(){
                       acqresult_date_search_detail("month");
                    });

                    //페이징 처리
                    acqresultdetailgrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        /*
                        if(lInd !==0){
                            $("#acqresultdetailForm input[name=page_no]").val(ind);
                            acqresultdetailSearch();
                        }else{
                            return false;
                        }
                        */
                        if(lInd !==0){
                             $("#acqresultdetailForm input[name=page_no]").val(ind);
                            acqresultdetailgrid.clearAll();
                            acqresultdetailSearch();
                        }else{
                             $("#acqresultdetailForm input[name=page_no]").val(1);
                            acqresultdetailgrid.clearAll();
                            acqresultdetailSearch();
                        }                        
                    });

                    //상세코드검색
                    $("#acqresultdetailForm input[name=acqresultdetail_serch]").click(function(){
                        /*
                        $("input[name=page_no]").val("1");

                        acqresultdetailgrid.clearAll();

                        acqresultdetailSearch();
                        */
                        acqresultdetailgrid.changePage(1);

                    });
                    
                    //엑셀다운로드
                    $("#acqresultdetailForm input[name=acqresultdetail_excel]").click(function(){

                        $("#acqresultdetailForm").attr("action","<c:url value="/acq_result/acq_resultDetailExcel" />");
                        document.getElementById("acqresultdetailForm").submit();

                    });                    
                    
                });

                //코드 정보 조회
                function acqresultdetailSearch() {
                
                $.ajax({
                    url: "<c:url value="/acq_result/acq_resultMasterDetailPop" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#acqresultdetailForm").serialize(),
                    success: function (data) {

                        var jsonData = $.parseJSON(data);
                        //acqresultdetailgrid.clearAll();
                        acqresultdetailgrid.parse(jsonData,"json");

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
                    return false;
            }
            
            function acq_resultdetailsetSens(id, k) {
                // update range
                if (k == "min") {
                    acq_resultDetailCalendar.setSensitiveRange(byId(id).value, null);
                } else {
                    acq_resultDetailCalendar.setSensitiveRange(null, byId(id).value);
                }
            }

             function byId(id) {
                    return document.getElementById(id);
                }             
            
            function acqresult_date_search_detail(day){
                    var oneDate = 1000 * 3600 * 24; 
                    var now = new Date();
                    var week = new Date(now.getTime() + (oneDate*-6));
                    var month = new Date(now.getTime() + (oneDate*-31));
                    var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                    var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                    var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                    var date=new Date();
                    if(day=="today"){
                        this.$("input[id=result_middepositDetail_from_date]").val(nowTime);
                        this.$("input[id=result_middepositDetail_to_date]").val(nowTime);
                    }else if(day=="week"){
                        this.$("input[id=result_middepositDetail_from_date]").val(weekTime);
                        this.$("input[id=result_middepositDetail_to_date]").val(nowTime);
                    }else{
                        this.$("input[id=result_middepositDetail_from_date]").val(monthTime);
                        this.$("input[id=result_middepositDetail_to_date]").val(nowTime);
                    }
                }
        function acqresultdetailFormInit($form) {

               searchFormInit($form);
               acqresult_date_search_detail("week");                    
           } 
    </script>
      
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>
