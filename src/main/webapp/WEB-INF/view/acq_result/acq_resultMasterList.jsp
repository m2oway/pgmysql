<%-- 
    Document   : merchantMaterList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>매입내역조회</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">
                acq_result_popdhxWins = new dhtmlXWindows();
                acq_result_popdhxWins.attachViewportTo("acq_result_detail_dhx_search");
                var acq_resultInsertCalendar;
                
                $(document).ready(function () {
                    acq_resultInsertCalendar = new dhtmlXCalendarObject(["view_inv_dt_start","view_inv_dt_end"]);
                    var tabBarId = tabbar.getActiveTab();
                    var acq_result_detailSearch = document.getElementById("acq_result_detail_dhx_search");
                    acq_result_master_layout = tabbar.cells(tabBarId).attachLayout("2E");
                    acq_result_master_layout.cells('a').hideHeader();
                    acq_result_master_layout.cells('b').hideHeader();

                    acq_result_detail_layout = acq_result_master_layout.cells('a').attachLayout("2E");
                    
                    acq_result_detail_layout.cells('a').hideHeader();
                    acq_result_detail_layout.cells('b').hideHeader();
                    acq_result_detail_layout.cells('a').attachObject(acq_result_detailSearch);
                    acq_result_detail_layout.cells('a').setHeight(30);
                    acq_result_detail_mygrid = acq_result_detail_layout.cells('b').attachGrid();
                    acq_result_detail_mygrid.setImagePath("<c:url value ='/resources/images/dhx/dhtmlxGrid/'/>");
                    var mheaders = "";
                    mheaders += "<center>매입일</center>,<center>결제채널명</center>,<center>가맹점번호구분명</center>,<center>가맹점번호</center>";
                    mheaders += ",<center>매입일 시작</center>,<center>매입일 종료</center>"
                    mheaders += ",<center>입금예정액</center>,<center>수수료</center>,<center>수수료부가세</center>"
                    mheaders += ",<center>매입요청액</center>,<center>매입성공액</center>,<center>반송액</center>,<center>보류액</center>,<center>보류해제액</center>";
                    mheaders += ",<center>매입요청건</center>,<center>매입성공건</center>,<center>반송건</center>,<center>보류건</center>,<center>보류해제건</center>";
                    acq_result_detail_mygrid.setHeader(mheaders);
                    var colsel = "";
                    colsel += "center,center,center,center";
                    colsel += ",center,center"
                    colsel += ",right,right,right"
                    colsel += ",right,right,right,right,right";
                    colsel += ",right,right,right,right,right";                    
                    acq_result_detail_mygrid.setColAlign(colsel);
                    var coltype = "txt,txt,txt,txt";
                    coltype += ",txt,txt";
                    coltype += ",edn,edn,edn";
                    coltype += ",edn,edn,edn,edn,edn";
                    coltype += ",edn,edn,edn,edn,edn";
                    acq_result_detail_mygrid.setColTypes(coltype);
                    var colwiths = "90,120,130,100";
                    colwiths += ",90,90";
                    colwiths += ",80,80,80";
                    colwiths += ",80,80,80,80,80";
                    colwiths += ",80,80,80,80,80";
                    acq_result_detail_mygrid.setInitWidths(colwiths);
                    var colsort  = "str,str,str,str";
                    colsort += ",str,str";
                    colsort += ",int,int,int";
                    colsort += ",int,int,int,int,int";
                    colsort += ",int,int,int,int,int";
                    acq_result_detail_mygrid.setColSorting(colsort);
                    
                    acq_result_detail_mygrid.setNumberFormat("0,000",6);
                    acq_result_detail_mygrid.setNumberFormat("0,000",7);
                    acq_result_detail_mygrid.setNumberFormat("0,000",8);
                    acq_result_detail_mygrid.setNumberFormat("0,000",9);
                    acq_result_detail_mygrid.setNumberFormat("0,000",10);
                    acq_result_detail_mygrid.setNumberFormat("0,000",11);
                    acq_result_detail_mygrid.setNumberFormat("0,000",12);
                    acq_result_detail_mygrid.setNumberFormat("0,000",13);
                    acq_result_detail_mygrid.setNumberFormat("0,000",14);
                    acq_result_detail_mygrid.setNumberFormat("0,000",15);
                    acq_result_detail_mygrid.setNumberFormat("0,000",16);
                    acq_result_detail_mygrid.setNumberFormat("0,000",17);
                    acq_result_detail_mygrid.setNumberFormat("0,000",18);
                    
                    acq_result_detail_mygrid.enableColumnMove(true);
                   
                    acq_result_detail_mygrid.groupBy(0,["#title","#cspan","","","","","#stat_total","#stat_total","#stat_total","#stat_total","#stat_total","#stat_total","#stat_total","#stat_total","#stat_total","#stat_total","#stat_total","#stat_total","#stat_total"]);
                    //acq_result_detail_mygrid.enablePaging(true,Number($("#acq_result_detail_Form input[name=page_size]").val()),10,"acq_result_detailPaging",true,"acq_result_detailrecinfoArea");
                    //acq_result_detail_mygrid.setPagingSkin("bricks");
                    acq_result_detail_mygrid.attachEvent("onRowDblClicked", acq_result_detail_attach);

                    //페이징 처리
                    /*
                    acq_result_detail_mygrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        if(lInd !==0){
                            $("#acq_result_detail_Form input[name=page_no]").val(ind);
                            doSearchAcq_ResultMasterDetail();
                        }else{
                            return false;
                        }
                    });
                    */
                   
                    var acq_list_footers = "합계,#cspan,#cspan,#cspan,#cspan,#cspan";
                    acq_list_footers += ",<div id='acq_result_detail_mygrid6'>0</div>,<div id='acq_result_detail_mygrid7'>0</div>,<div id='acq_result_detail_mygrid8'>0</div>";
                    acq_list_footers += ",<div id='acq_result_detail_mygrid9'>0</div>,<div id='acq_result_detail_mygrid10'>0</div>,<div id='acq_result_detail_mygrid11'>0</div>";
                    acq_list_footers += ",<div id='acq_result_detail_mygrid12'>0</div>,<div id='acq_result_detail_mygrid13'>0</div>,<div id='acq_result_detail_mygrid14'>0</div>";
                    acq_list_footers += ",<div id='acq_result_detail_mygrid15'>0</div>,<div id='acq_result_detail_mygrid16'>0</div>,<div id='acq_result_detail_mygrid17'>0</div>,<div id='acq_result_detail_mygrid18'>0</div>";
                    acq_result_detail_mygrid.attachFooter(acq_list_footers); 
                   
                    acq_result_detail_mygrid.setSkin("dhx_skyblue");
                    acq_result_detail_mygrid.init();
                    acq_result_detail_mygrid.parse(${acq_resultDetailMasterListJsonString},acq_list_footersvalue,"json");
                    
                    //검색조건 초기화
                    $("#acq_result_detail_Form input[name=init]").click(function () {

                    acq_result_detail_MasterInit($("#acq_result_detail_Form"));

                    });  


                    $("#acq_result_detail_Form input[name=week]").click(function(){
                       acqresult_date_search("week");
                    });
                    
                      $("#acq_result_detail_Form input[name=month]").click(function(){
                       acqresult_date_search("month");
                    });
                    
                    
                    //버튼 추가
                    $("#acq_result_detail_Form input[name=searchButton]").bind("click",function(e){
                        doSearchAcq_ResultMaster();
                        return false;
                    });
                    
                    //버튼 추가
                    $("#acq_result_detail_Form input[name=excelButton]").bind("click",function(e){
                        doSearchAcq_ResultMasterExcel();
                        return false;
                    });                    

            });
            
             function doSearchAcq_ResultMaster(){

                    $("#acq_result_detail_Form input[name=inv_dt_start]").val(($("#view_inv_dt_start").val()).replace( /[^(0-9)]/g,""));
                    $("#acq_result_detail_Form input[name=inv_dt_end]").val(($("#view_inv_dt_end").val()).replace( /[^(0-9)]/g,""));
                    
                    if(isValidDate($("#inv_dt_start").val()) ){
                    
                    }else{
                        alert("날짜 형식을 확인하세요");
                        return false;
                    }
                    $.ajax({
                        url : "<c:url value = '/acq_result/acq_resultMasterList'/>",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#acq_result_detail_Form").serialize(),
                        success : function(data) {
                            acq_result_detail_mygrid.clearAll();
                            acq_result_detail_mygrid.groupBy(0,["#title","#cspan","","","","","#stat_total","#stat_total","#stat_total","#stat_total","#stat_total","#stat_total","#stat_total","#stat_total","#stat_total","#stat_total","#stat_total","#stat_total","#stat_total"]);
                            acq_result_detail_mygrid.parse($.parseJSON(data),acq_list_footersvalue,"json");
                            
                        },
                        error : function() { 
                                alert("조회 실패");
                        }
                    });	            
                }
            
            function acq_result_detail_attach(rowid, col) {
                
                //var inv_dt =acq_result_detail_mygrid.getUserData(rowid, 'inv_dt');
                var acq_start_dt = acq_result_detail_mygrid.getUserData(rowid, 'rcpt_sta_dt');
                var acq_end_dt = acq_result_detail_mygrid.getUserData(rowid, 'rcpt_end_dt');
                var pay_type =acq_result_detail_mygrid.getUserData(rowid, 'pay_type');
                var merch_no =acq_result_detail_mygrid.getUserData(rowid, 'merch_no');
                //var file_seq = acq_result_detail_mygrid.getUserData(rowid, 'file_seq');
                //var file_nm = acq_result_detail_mygrid.getUserData(rowid, 'file_nm');
                
                //var param = "?acq_dt=" + inv_dt + "&pay_type="  +pay_type  +"&merch_no="  +merch_no; 
                var param = "?acq_start_dt=" + acq_start_dt +"&acq_end_dt=" + acq_end_dt + "&pay_type="  +pay_type  +"&merch_no="  +merch_no; 
                  //var acq_dt = acq_result_detail_mygrid.getUserData(rowid, 'acq_dt');
//                var merch_no = acq_result_list_mygrid.getUserData(rowid, 'merch_no');
//                var acqfile_seq = acq_result_list_mygrid.getUserData(rowid, 'acqfile_seq');
                acq_result_master_layout.cells('b').attachURL("<c:url value = '/acq_result/acq_resultMasterDetailPop'/>"+param , true);
            }

                
            function acqresult_date_search(day){
                    var oneDate = 1000 * 3600 * 24; 
                    var now = new Date();
                    var week = new Date(now.getTime() + (oneDate*-6));
                    var month = new Date(now.getTime() + (oneDate*-31));
                    var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                    var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                    var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                    var date=new Date();
                    if(day=="today"){
                        this.$("input[id=view_inv_dt_start]").val(nowTime);
                        this.$("input[id=view_inv_dt_end]").val(nowTime);
                    }else if(day=="week"){
                        this.$("input[id=view_inv_dt_start]").val(weekTime);
                        this.$("input[id=view_inv_dt_end]").val(nowTime);
                    }else{
                        this.$("input[id=view_inv_dt_start]").val(monthTime);
                        this.$("input[id=view_inv_dt_end]").val(nowTime);
                    }
                }
                
                function acq_result_detail_MasterInit($form) {

                    searchFormInit($form);

                    acqresult_date_search("week");                    
                } 
                    
                function acq_list_footersvalue() {
		    
		    var acq_result_detail_mygrid6 = document.getElementById("acq_result_detail_mygrid6");
		    var acq_result_detail_mygrid7 = document.getElementById("acq_result_detail_mygrid7");
                    var acq_result_detail_mygrid8 = document.getElementById("acq_result_detail_mygrid8");
                    var acq_result_detail_mygrid9 = document.getElementById("acq_result_detail_mygrid9");
                    var acq_result_detail_mygrid10 = document.getElementById("acq_result_detail_mygrid10");
                    var acq_result_detail_mygrid11 = document.getElementById("acq_result_detail_mygrid11");
                    var acq_result_detail_mygrid12 = document.getElementById("acq_result_detail_mygrid12");
                    var acq_result_detail_mygrid13 = document.getElementById("acq_result_detail_mygrid13");
                    var acq_result_detail_mygrid14 = document.getElementById("acq_result_detail_mygrid14");
                    var acq_result_detail_mygrid15 = document.getElementById("acq_result_detail_mygrid15");
                    var acq_result_detail_mygrid16 = document.getElementById("acq_result_detail_mygrid16");
		    var acq_result_detail_mygrid17 = document.getElementById("acq_result_detail_mygrid17");
                    var acq_result_detail_mygrid17 = document.getElementById("acq_result_detail_mygrid18");
		    
		    acq_result_detail_mygrid6.innerHTML = putComma(sumColumn(6));
		    acq_result_detail_mygrid7.innerHTML = putComma(sumColumn(7));
                    acq_result_detail_mygrid8.innerHTML = putComma(sumColumn(8));
                    acq_result_detail_mygrid9.innerHTML = putComma(sumColumn(9));
                    acq_result_detail_mygrid10.innerHTML = putComma(sumColumn(10));
                    acq_result_detail_mygrid11.innerHTML = putComma(sumColumn(11));
                    acq_result_detail_mygrid12.innerHTML = putComma(sumColumn(12));
                    acq_result_detail_mygrid13.innerHTML = putComma(sumColumn(13));
                    acq_result_detail_mygrid14.innerHTML = putComma(sumColumn(14));
                    acq_result_detail_mygrid15.innerHTML = putComma(sumColumn(15));
                    acq_result_detail_mygrid16.innerHTML = putComma(sumColumn(16)) ;
		    acq_result_detail_mygrid17.innerHTML = putComma(sumColumn(17)) ;
		    acq_result_detail_mygrid17.innerHTML = putComma(sumColumn(18)) ;
		    return true;
                }
            
                function sumColumn(ind) {
                    
                    var out = 0;
                    for (var i = 0; i < acq_result_detail_mygrid.getRowsNum(); i++) {
                        out += parseFloat(acq_result_detail_mygrid.cells2(i, ind).getValue());
                    }
                    
                    return out;
                    
                }          
                
                function acq_resultListsetSens(id, k) {
                    // update range
                    if (k == "min") {
                        acq_resultInsertCalendar.setSensitiveRange(byId(id).value, null);
                    } else {
                        acq_resultInsertCalendar.setSensitiveRange(null, byId(id).value);
                    }
                }
                
                function byId(id) {
                    return document.getElementById(id);
                }           
                
                //매입내역 엑셀다운로드
                function doSearchAcq_ResultMasterExcel() {

                    $("#acq_result_detail_Form").attr("action","<c:url value="/acq_result/acq_resultMasterListExcel" />");
                    document.getElementById("acq_result_detail_Form").submit();

                }                       
                    
            </script>

            <div class="right" id="acq_result_detail_dhx_search" style="width:100%; height:100%">
                <form id="acq_result_detail_Form" method="POST" action="<c:url value = '/acq_result/acq_resultMasterList'/>" modelAttribute="acq_resultFormBean">
                    <input type ="hidden" id ="inv_dt_start" name ="inv_dt_start"/>
                    <input type ="hidden" id ="inv_dt_end" name ="inv_dt_end"/>
                        <div class="label">검색일자</div>
                        <div class ="input">
                            <input type="text" id="view_inv_dt_start" value = '${acq_ResultFormBean.inv_dt_start}' onclick="acq_resultListsetSens('view_inv_dt_end', 'max');"/>~
                            <input type="text" id = "view_inv_dt_end" value = '${acq_ResultFormBean.inv_dt_end}' onclick="acq_resultListsetSens('view_inv_dt_start', 'min');"/>
                        </div> 
                        <div class ="input"><input type="button" name="week" value="1주"/></div>
                        <div class ="input"><input type="button" name="month" value="1달"/></div>
                        <div class="label">결제채널</div>
                        <div class ="input">
                            <select id = "pay_type" name ="pay_type">
                                <option value="">전체</option>
                                <c:forEach var="code" items="${PayTypeList.tb_code_details}" >
                                     <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="label2">가맹점번호구분</div>
                        <div class ="input">
                            <select id = "pay_mtd" name ="pay_mtd">
                                <option value="">전체</option>
                                <c:forEach var="code" items="${PayMtdList.tb_code_details}" >
                                     <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>                        
                        <div class ="label">가맹점번호</div>
                        <div class ="input"><input type="text" name="merch_no"/></div>    
                        <td><input type="button" name="searchButton" value="조회"/></td>
                        <td><input type="button" name="excelButton" value="엑셀"/></td>                        
                </form>
                <div id="acq_result_detailPaging" style="width: 100%;"></div>
                <div id="acq_result_detailrecinfoArea" style="width: 100%;"></div>       
            </div>
            

<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

