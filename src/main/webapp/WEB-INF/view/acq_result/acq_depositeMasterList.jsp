<%-- 
    Document   : merchantMaterList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>입금예정금액조회</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">
                acq_deposite_popdhxWins = new dhtmlXWindows();
                acq_deposite_popdhxWins.attachViewportTo("acq_deposite_detail_dhx_search");
                var bankDepositInsertCalendar;
                
                $(document).ready(function () {
                    bankDepositInsertCalendar = new dhtmlXCalendarObject(["view_pay_dt_start","view_pay_dt_end"]);
                    var tabBarId = tabbar.getActiveTab();
                    var acq_deposite_detailSearch = document.getElementById("acq_deposite_detail_dhx_search");
                    acq_deposite_master_layout = tabbar.cells(tabBarId).attachLayout("2E");
                    acq_deposite_master_layout.cells('a').hideHeader();
                    acq_deposite_master_layout.cells('b').hideHeader();

                    acq_deposite_detail_layout = acq_deposite_master_layout.cells('a').attachLayout("2E");
                    
                    acq_deposite_detail_layout.cells('a').hideHeader();
                    acq_deposite_detail_layout.cells('b').hideHeader();
                    acq_deposite_detail_layout.cells('a').attachObject(acq_deposite_detailSearch);
                    acq_deposite_detail_layout.cells('a').setHeight(30);
                    acq_deposite_detail_mygrid = acq_deposite_detail_layout.cells('b').attachGrid();
                    acq_deposite_detail_mygrid.setImagePath("<c:url value ='/resources/images/dhx/dhtmlxGrid/'/>");
                    var mheaders = "";
                    mheaders += "<center>입금일</center>,<center>가맹점번호명</center>,<center>가맹점번호</center>,<center>입금예정액</center>,<center>수수료</center>,<center>수수료부가세</center>,<center>매입액</center>";
                    
                    acq_deposite_detail_mygrid.setHeader(mheaders);
                    var colsel = "";
                    colsel += "center,center,center,right,right,right,right";
                    acq_deposite_detail_mygrid.setColAlign(colsel);
                    var coltype = "txt,txt,txt,edn,edn,edn,edn";
                    
                    acq_deposite_detail_mygrid.setColTypes(coltype);
                    var colwiths = "90,150,120,80,80,100,80";
                    
                    acq_deposite_detail_mygrid.setInitWidths(colwiths);
                    var colsort  = "str,str,str,int,int,int,int";
                    
                    acq_deposite_detail_mygrid.setColSorting(colsort);
                    
                    acq_deposite_detail_mygrid.setNumberFormat("0,000",3);
                    acq_deposite_detail_mygrid.setNumberFormat("0,000",4);
                    acq_deposite_detail_mygrid.setNumberFormat("0,000",5);
                    acq_deposite_detail_mygrid.setNumberFormat("0,000",6);
                    
                    acq_deposite_detail_mygrid.enableColumnMove(true);

                    acq_deposite_detail_mygrid.attachEvent("onRowDblClicked", acq_deposite_detail_attach);

                    acq_deposite_detail_mygrid.setSkin("dhx_skyblue");
                    
                    var acq_deposite_detail_mygridFooters = "총계,#cspan,#cspan,<div id='acq_deposite_detail_mygrid3'>0</div>,<div id='acq_deposite_detail_mygrid4'>0</div>,<div id='acq_deposite_detail_mygrid5'>0</div>,<div id='acq_deposite_detail_mygrid6'>0</div>";
                    acq_deposite_detail_mygrid.attachFooter(acq_deposite_detail_mygridFooters);
                    
                    acq_deposite_detail_mygrid.groupBy(0,["#title","#cspan","#cspan","#stat_total","#stat_total","#stat_total","#stat_total"]);
                    
                    acq_deposite_detail_mygrid.init();

                    acq_deposite_detail_mygrid.parse(${acq_depositeMasterListJsonString}, acq_deposite_detail_mygridFooterValues, "json");
                    
                    //검색조건 초기화
                    $("#acq_deposite_detail_Form input[name=init]").click(function () {

                    acq_deposite_DetailMasterInit($("#acq_deposite_detail_Form"));

                    });  

                    $("#acq_deposite_detail_Form input[name=week]").click(function(){
                       acqdeposite_date_search("week");
                    });
                    
                      $("#acq_deposite_detail_Form input[name=month]").click(function(){
                       acqdeposite_date_search("month");
                    });
                    
                    
                    //버튼 추가
                    $("#acq_deposite_detail_Form input[name=searchButton]").bind("click",function(e){
                        doSearchAcq_DepositeMaster();
                        return false;
                    });
                    
                    //버튼 추가
                    $("#acq_deposite_detail_Form input[name=excelButton]").bind("click",function(e){
                        doSearchAcq_DepositeMasterExcel();
                        return false;
                    });                    

                });
                
                function acq_deposite_detail_mygridFooterValues() {
                    
                    var acq_deposite_detail_mygrid3 = document.getElementById("acq_deposite_detail_mygrid3");
                    var acq_deposite_detail_mygrid4 = document.getElementById("acq_deposite_detail_mygrid4");
                    var acq_deposite_detail_mygrid5 = document.getElementById("acq_deposite_detail_mygrid5");
                    var acq_deposite_detail_mygrid6 = document.getElementById("acq_deposite_detail_mygrid6");
                    acq_deposite_detail_mygrid3.innerHTML = putComma(sumColumn(3));
                    acq_deposite_detail_mygrid4.innerHTML = putComma(sumColumn(4));
                    acq_deposite_detail_mygrid5.innerHTML = putComma(sumColumn(5));
                    acq_deposite_detail_mygrid6.innerHTML = putComma(sumColumn(6));

                    return true;
                }                
                
                function sumColumn(ind) {
                    
                    var out = 0;
                    for (var i = 0; i < acq_deposite_detail_mygrid.getRowsNum(); i++) {
                         out += parseFloat(acq_deposite_detail_mygrid.cells2(i, ind).getValue());
                    }

                    return out;
                    
                }                        
            
                function doSearchAcq_DepositeMaster(){

                    $("#acq_deposite_detail_Form input[name=pay_dt_start]").val(($("#view_pay_dt_start").val()).replace( /[^(0-9)]/g,""));
                    $("#acq_deposite_detail_Form input[name=pay_dt_end]").val(($("#view_pay_dt_end").val()).replace( /[^(0-9)]/g,""));
                    
                    if(isValidDate($("#pay_dt_start").val()) ){
                    
                    }else{
                        alert("날짜 형식을 확인하세요");
                        return false;
                    }
                    $.ajax({
                        url : "<c:url value = '/acq_result/acq_depositeMasterList'/>",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#acq_deposite_detail_Form").serialize(),
                        success : function(data) {
                            acq_deposite_detail_mygrid.clearAll();
                            var jsonData = $.parseJSON(data);
                            acq_deposite_detail_mygrid.parse(jsonData, acq_deposite_detail_mygridFooterValues, "json");
                            acq_deposite_detail_mygrid.groupBy(0,["#title","#cspan","#cspan","#stat_total","#stat_total","#stat_total","#stat_total"]);
                        },
                        error : function() { 
                                alert("조회 실패");
                        }
                    });	            
                }
            
                function acq_deposite_detail_attach(rowid, col) {

                    var pay_dt = acq_deposite_detail_mygrid.getUserData(rowid, 'pay_dt').replace( /[^(0-9)]/g,"");
                    var merch_no = acq_deposite_detail_mygrid.getUserData(rowid, 'merch_no');

                    var param = "?pay_dt=" +pay_dt+"&merch_no="  +merch_no; 
                    acq_deposite_master_layout.cells('b').attachURL("<c:url value = '/acq_result/acq_depositeDetailList'/>"+param , true);
                }

                
                function acqdeposite_date_search(day){
                    var oneDate = 1000 * 3600 * 24; 
                    var now = new Date();
                    var week = new Date(now.getTime() + (oneDate*-6));
                    var month = new Date(now.getTime() + (oneDate*-31));
                    var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                    var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                    var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                    var date=new Date();
                    if(day=="today"){
                        this.$("input[id=view_pay_dt_start]").val(nowTime);
                        this.$("input[id=view_pay_dt_end]").val(nowTime);
                    }else if(day=="week"){
                        this.$("input[id=view_pay_dt_start]").val(weekTime);
                        this.$("input[id=view_pay_dt_end]").val(nowTime);
                    }else{
                        this.$("input[id=view_pay_dt_start]").val(monthTime);
                        this.$("input[id=view_pay_dt_end]").val(nowTime);
                    }
                }
                
                function acq_deposite_DetailMasterInit($form) {
                    searchFormInit($form);
                    acqdeposite_date_search("week");                    
                } 
                function bankDepositsetSens(id, k) {
                    // update range
                    if (k == "min") {
                        bankDepositInsertCalendar.setSensitiveRange(byId(id).value, null);
                    } else {
                        bankDepositInsertCalendar.setSensitiveRange(null, byId(id).value);
                    }
                }
			
                function byId(id) {
                    return document.getElementById(id);
                }             
                
                //엑셀다운로드
                function doSearchAcq_DepositeMasterExcel() {

                    $("#acq_deposite_detail_Form").attr("action","<c:url value="/acq_result/acq_depositeMasterListExcel" />");
                    document.getElementById("acq_deposite_detail_Form").submit();

                }                       
            
            </script>

            <div class="right" id="acq_deposite_detail_dhx_search" style="width:100%; height:100%">
                <form id="acq_deposite_detail_Form" method="POST" action="<c:url value = '/acq_result/acq_depositeMasterList'/>">
                    <input type ="hidden" id ="pay_dt_start" name ="pay_dt_start"/>
                    <input type ="hidden" id ="pay_dt_end" name ="pay_dt_end"/>
                        <div class="label">검색일자</div>
                        <div class ="input">
                            <input id="view_pay_dt_start" value='${acq_ResultFormBean.pay_dt_start}' onclick="bankDepositsetSens('view_pay_dt_end', 'max');" />~
                            <input id="view_pay_dt_end" value='${acq_ResultFormBean.pay_dt_end}' onclick="bankDepositsetSens('view_pay_dt_start', 'min');" />
                        </div> 
                        <div class ="input"><input type="button" name="week" value="1주"/></div>
                        <div class ="input"><input type="button" name="month" value="1달"/></div>
                        <div class ="label">가맹점번호</div>
                        <div class ="input"><input type="text" name="merch_no" value="${acq_ResultFormBean.merch_no}" /></div>
                        <td><input type="button" name="searchButton" value="조회"/></td>
                        <td><input type="button" name="init" value="초기화"/></td>
                        <td><input type="button" name="excelButton" value="엑셀"/></td>                        
                </form>
            </div>
            

<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

