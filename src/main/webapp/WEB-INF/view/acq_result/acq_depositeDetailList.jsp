<%--
    Document   : codeDetailMaster
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>일자별입금내역 상세조회</title>
        </head>
        <body>
</c:if>
            <script type="text/javascript">

                var acqdepositresultdetailgrid={};
                var acqdepositresultdetail_layout={};
                
                var middepositDetailCalendar = new dhtmlXCalendarObject(["middepositDetail_from_date","middepositDetail_to_date","middepositDetail_pay_dt"]);    

                $(document).ready(function () {

                    var acqdepositedetailForm = document.getElementById("acqdepositresultdetail_serch");

                    acqdepositresultdetail_layout = acq_deposite_master_layout.cells('b').attachLayout("2E");

                    acqdepositresultdetail_layout.cells('a').hideHeader();

                    acqdepositresultdetail_layout.cells('b').hideHeader();

                    acqdepositresultdetail_layout.cells('a').attachObject(acqdepositedetailForm);

                    acqdepositresultdetail_layout.cells('a').setHeight(60);

                    acqdepositresultdetailgrid = acqdepositresultdetail_layout.cells('b').attachGrid();

                    acqdepositresultdetailgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                

                    var acqdepositresultdetailheaders = "";
                    acqdepositresultdetailheaders += "<center>NO</center>,<center>결제채널</center>,<center>가맹점번호</center>,<center>거래종류</center>,<center>매입사</center>";
                    acqdepositresultdetailheaders += ",<center>승인일</center>,<center>승인번호</center>,<center>승인금액</center>,<center>할부</center>";
                    acqdepositresultdetailheaders += ",<center>매입일</center>,<center>입금예정일</center>,<center>수수료</center>,<center>수수료부가세</center>,<center>입금예정금액</center>";
                    
                    acqdepositresultdetailgrid.setHeader(acqdepositresultdetailheaders);
                    acqdepositresultdetailgrid.setColAlign("center,center,center,center,center,center,center,right,center,center,center,right,right,right");
                    acqdepositresultdetailgrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,edn,txt,txt,txt,edn,edn,edn");
                    acqdepositresultdetailgrid.setInitWidths("50,100,100,100,100,100,100,100,120,100,100,100,100,100");
                    acqdepositresultdetailgrid.setColSorting("str,str,str,str,str,str,str,int,str,str,str,int,int,int");
                    acqdepositresultdetailgrid.setNumberFormat("0,000",7);            
                    acqdepositresultdetailgrid.setNumberFormat("0,000",12);
                    acqdepositresultdetailgrid.setNumberFormat("0,000",13);
                    acqdepositresultdetailgrid.setNumberFormat("0,000",14);
                    acqdepositresultdetailgrid.enableColumnMove(true);
                    acqdepositresultdetailgrid.setSkin("dhx_skyblue");
                    acqdepositresultdetailgrid.enablePaging(true,Number($("#acqdepositedetailForm input[name=page_size]").val()),10,"acqdepositresultdetailPaging",true,"acqdepositresultdetailrecinfoArea");
                    acqdepositresultdetailgrid.setPagingSkin("bricks");
                    acqdepositresultdetailgrid.init();
                    acqdepositresultdetailgrid.parse(${ls_middepositDetailList},"json");
                    
                    //검색조건 초기화
                    $("#acqdepositedetailForm input[name=init]").click(function () {

                    acqdepositDetailMasterInit($("#acqdepositedetailForm"));

                    });  
                    
                    $("#acqdepositedetailForm input[name=week]").click(function(){
                       acqdeposite_date_search_detail("week");
                    });
                    
                      $("#acqdepositedetailForm input[name=month]").click(function(){
                       acqdeposite_date_search_detail("month");
                    });

                    //페이징 처리
                    acqdepositresultdetailgrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                       
                        if(lInd !==0){
                            $("#acqdepositedetailForm input[name=page_no]").val(ind);
                            acqdepositresultdetailgrid.clearAll();
                            acqdepositresultdetailSearch();
                        }else{
                            $("#acqdepositedetailForm input[name=page_no]").val(1);
                            acqdepositresultdetailgrid.clearAll();
                            acqdepositresultdetailSearch();
                        }                        
                        
                    });

                    //상세코드검색
                    $("#acqdepositedetailForm input[name=acqdepositresultdetail_excel]").click(function(){

                        acqdepositresultdetailSearchExcel();

                    });
                    
                    //상세코드검색
                    $("#acqdepositedetailForm input[name=acqdepositresultdetail_serch]").click(function(){
                        /*
                        $("input[name=page_no]").val("1");

                        acqdepositresultdetailgrid.clearAll();

                        acqdepositresultdetailSearch();
                        */
                        acqdepositresultdetailgrid.changePage(1);

                    });                    

                    //엑셀다운로드
                    function acqdepositresultdetailSearchExcel() {

                        $("#acqdepositedetailForm").attr("action","<c:url value="/acq_result/acq_depositeDetailListExcel" />");
                        document.getElementById("acqdepositedetailForm").submit();

                    }         
                    
                });

                //코드 정보 조회
                function acqdepositresultdetailSearch() {
                
                $.ajax({
                    url: "<c:url value="/acq_result/acq_depositeDetailList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#acqdepositedetailForm").serialize(),
                    success: function (data) {

                        var jsonData = $.parseJSON(data);
                        //acqdepositresultdetailgrid.clearAll();
                        acqdepositresultdetailgrid.parse(jsonData,"json");

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
                    return false;
            }
            
            function depositdetailsetSens(id, k) {
                // update range
                if (k == "min") {
                    middepositDetailCalendar.setSensitiveRange(byId(id).value, null);
                } else {
                    middepositDetailCalendar.setSensitiveRange(null, byId(id).value);
                }
            }

             function byId(id) {
                return document.getElementById(id);
            }                               
            
                function acqdeposite_date_search_detail(day){
                    var oneDate = 1000 * 3600 * 24; 
                    var now = new Date();
                    var week = new Date(now.getTime() + (oneDate*-6));
                    var month = new Date(now.getTime() + (oneDate*-31));
                    var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                    var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                    var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                    var date=new Date();
                    if(day=="today"){
                        this.$("input[id=middepositDetail_from_date]").val(nowTime);
                        this.$("input[id=middepositDetail_to_date]").val(nowTime);
                    }else if(day=="week"){
                        this.$("input[id=middepositDetail_from_date]").val(weekTime);
                        this.$("input[id=middepositDetail_to_date]").val(nowTime);
                    }else{
                        this.$("input[id=middepositDetail_from_date]").val(monthTime);
                        this.$("input[id=middepositDetail_to_date]").val(nowTime);
                    }
                }
                
                function acqdepositDetailMasterInit($form) {
                    searchFormInit($form);
                    acqdeposite_date_search_detail("week");                    
                } 

            
    </script>
    <body>


        <div class="right" id="acqdepositresultdetail_serch" style="width:100%;">
             <form id="acqdepositedetailForm" method="POST" action="<c:url value="/acq_result/acq_depositeDetailList" />" modelAttribute="acq_ResultFormBean">
                        <input type="hidden" name="page_size" value="100" >
                        <input type="hidden" name="page_no" value="1" >
                        <table>
                            <tr>
                                <td>
                                    <div class="label">승인일</div>
                                    <div class="input">
                                        <input type="text" name="sal_start_dt" id="middepositDetail_from_date" value ="${acq_ResultFormBean.sal_start_dt}" onclick="depositdetailsetSens('middepositDetail_to_date', 'max');" /> ~
                                        <input type="text" name="sal_end_dt" id="middepositDetail_to_date" value ="${acq_ResultFormBean.sal_end_dt}" onclick="depositdetailsetSens('middepositDetail_from_date', 'min');" />
                                    </div>
                                    <div class ="input"><input type="button" name="week" value="1주"/></div>
                                    <div class ="input"><input type="button" name="month" value="1달"/></div>
                                    <div class="label">입금일</div>
                                    <div class="input">
                                        <input type="text" name="pay_dt" id="middepositDetail_pay_dt" value="${acq_ResultFormBean.pay_dt}" />
                                    </div>                                    
                                    <div class="label">결제채널</div>
                                    <div class="input">
                                        <select name="pay_type">
                                            <option value="">전체</option>
                                            <c:forEach var="code" items="${payChnCateList.tb_code_details}">
                                                <option value="${code.detail_code}">${code.code_nm}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                <div class="label">가맹점번호</div>
                                <div class="input"><input type="text" name="merch_no" value="${acq_ResultFormBean.merch_no}" /></div>                                
                                <div class="label">승인번호</div>
                                <div class="input"><input type="text" name="aprv_no" /></div>
                                <div class="label">승인금액</div>
                                <div class="input"><input type="text" name="tot_trx_amt" /></div>
                                <div class="label">매입사</div>
                                <div class="input">
                                    <select name="acq_cd">
                                        <option value="">전체</option>
                                        <c:forEach var="code" items="${acqCdList.tb_code_details}">
                                            <option value="${code.detail_code}">${code.code_nm}</option>
                                        </c:forEach>
                                    </select>
                                </div>                      
                            </td>                                   
                                <td rowspan='2'>
                                    <input type="button" name="acqdepositresultdetail_serch" value="조회"/>
                                </td>
                                <td rowspan='2'>
                                    <input type="button" name="acqdepositresultdetail_excel" value="엑셀"/>
                                </td>
                                <td><input type="button" name="init" value="검색조건지우기"/></td>
                            </tr>
                        </table>
             </form>

            <div id="acqdepositresultdetailPaging" style="width: 100%;"></div>
            <div id="acqdepositresultdetailrecinfoArea" style="width: 100%;"></div>

        </div>
        
        <div id="acqdepositresultdetail" style="position: relative;width:100%;height:100%;background-color:white;"></div>
      
    </body>
</html>
