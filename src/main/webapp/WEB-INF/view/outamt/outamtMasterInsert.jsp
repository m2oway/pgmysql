<%-- 
    Document   : outamtMasterInsert
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>미수금 등록</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">

                $(document).ready(function () {
                    
                    var deposit_popup_yn = '${outamtFormBean.deposit_popup_yn}';
                    
                    //미수금 등록 이벤트
                    $("#outamtMasterFormInsert input[name=insert]").click(function(){
                       doInsertOutamtMaster(deposit_popup_yn);
                    });              
                    
                    //미수금 등록 닫기
                    $("#outamtMasterFormInsert input[name=close]").click(function(){
                        
                        if(deposit_popup_yn === "Y"){
                            depositMetabolismWindow.window("outamtListWindow").close();
                        }else{
                            outamtMasterWindow.window("w1").close();
                        }
                        
                    });                            
                    
                });

                //미수금 등록
                function doInsertOutamtMaster(deposit_popup_yn){
                    
                    if($("#outamtMasterFormInsert input[name=out_amt]").val().trim().length === 0){
                        alert("미수금을 입력하여 주십시오.");
                        $("#outamtMasterFormInsert input[name=out_amt]").val("");
                        $("#outamtMasterFormInsert input[name=out_amt]").focus();
                        return false;
                    }
                    
                    if(!window.confirm("등록하시겠습니까?")){
                        return false;
                    }
                    
                    $("#outamtMasterFormInsert select[name=pay_mtd]").removeAttr("disabled");
                    
                    $.ajax({
                        url : $("#outamtMasterFormInsert").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#outamtMasterFormInsert").serialize(),
                        success : function(data) {
                            alert("등록 완료");

                            if(deposit_popup_yn === "Y"){
                                middepositMasterSearch();
                                middepositMastergrid.clearAll();
                                depositMetabolismWindow.window("outamtListWindow").close();
                            }else{
                                doSearchOutamtMaster();
                                outamtDetailGrid.clearAll();
                                outamtMasterWindow.window("w1").close();
                            }
                        },
                        error : function() { 
                                alert("등록 실패");
                        }
                    });	      
        
                }                   

            </script>

            <form id="outamtMasterFormInsert" method="POST" action="<c:url value="/outamt/outamtMasterInsert" />" modelAttribute="outamtFormBean">
                <table class="gridtable" height="100%" width="100%">
                    <tr>
                        <td class="headcol">가맹점번호구분</td>
                        <td>
                            <select name="pay_mtd" disabled="disabled">
                                <c:forEach var="code" items="${midPayMtdList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == outamtFormBean.pay_mtd}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach>
                            </select>                            
                        </td>
                        <td class="headcol">가맹점번호</td>
                        <td><input name="merch_no" value="${outamtFormBean.merch_no}" readonly="readonly" /></td>
                    </tr>
                    <tr>
                        <td class="headcol">미수금액</td>
                        <td><input name="out_amt" onkeyup="javascript:InpuOnlyNumber2(this);" onblur="javascript:checkLength(this,20);" /></td>
                        <td class="headcol">미수생성일</td>
                        <td><input name="gen_dt" id="outamtMasterInsert_gen_dt" value="${outamtFormBean.gen_dt}" readonly="readonly" /></td>
                    </tr>            
                    <tr>
                        <td class="headcol">사유</td>
                        <td>
                            <select name="gen_reson">
                                <c:forEach var="code" items="${insResonList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>                                
                        </td>
                        <td class="headcol"></td>
                        <td></td>
                    </tr>                    
                    <tr>
                        <td class="headcol">메모</td>
                        <td colspan="3">
                            <textarea name="memo" cols="100" onblur="javascript:checkLength(this,500);"></textarea>
                        </td>
                    </tr> 
                    <tr>
                        <td colspan="4">
                            <input type="button" name="insert" value="등록"/>
                            <input type="button" name="close" value="닫기"/>
                        </td>
                    </tr>
                </table>
            </form>

<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

