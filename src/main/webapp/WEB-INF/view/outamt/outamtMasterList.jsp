<%-- 
    Document   : outamtMasterList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>미수금 정보 관리</title>
        </head>
        <body>
</c:if>

            <div class="searchForm1row" id="outamtMasterSearch" style="display:none;width:100%;height:100%;background-color:white;">
                
                <form id="outamtMasterForm" method="POST" action="<c:url value="/outamt/outamtMasterList" />" modelAttribute="outamtFormBean">

                    <div class="label">검색일</div>
                    <div class="input">
                        <input type="text" name="gen_start_dt" id="outamtMasterList_gen_start_dt" value ="${outamtFormBean.gen_start_dt}" onclick="outamtListsetSens('outamtMasterList_gen_end_dt', 'max');" />~
                        <input type="text" name="gen_end_dt" id="outamtMasterList_gen_end_dt"  value ="${outamtFormBean.gen_end_dt}" onclick="outamtListsetSens('outamtMasterList_gen_start_dt', 'min');" />
                    </div>
                    <div class ="input"><input type="button" name="week" value="1주"/></div>
                    <div class ="input"><input type="button" name="month" value="1달"/></div>

                    <div class ="input">
                        <input type="button" name="search" value="검색"/>
                        <input type="button" name="excel" value="엑셀"/>
                        <input type="button" name="init" value="검색조건지우기"/>                        
                    </div>

                </form>
                        
            </div>
            
            <div id="outamtMasterList" style="width:100%;height:100%;background-color:white;"></div>
            
            <script type="text/javascript">
                
                var outamtMasterGrid;
                var outamtMasterWindow;
                var outamtMasterListCalendar;
                
                $(document).ready(function () {          

                    outamtMasterListCalendar = new dhtmlXCalendarObject(["outamtMasterList_gen_start_dt","outamtMasterList_gen_end_dt"]);

                    var outamtSearch = document.getElementById("outamtMasterSearch");            
                    outamtLayout = tabbar.cells("6-3").attachLayout("2E");
                    outamtLayout.cells('a').hideHeader();
                    outamtLayout.cells('b').hideHeader();
                    
                    outamtMasterLayout = outamtLayout.cells('a').attachLayout("2E");
                    outamtMasterLayout.cells('a').hideHeader();
                    outamtMasterLayout.cells('b').hideHeader();
                    outamtMasterLayout.cells('a').attachObject(outamtSearch);
                    outamtMasterLayout.cells('a').setHeight(45);
                    outamtMasterGrid = outamtMasterLayout.cells('b').attachGrid();
                    outamtMasterGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var outamtMasterGridheaders = "";
                    outamtMasterGridheaders += "<center>NO</center>,<center>미수금 발생일</center>,<center>가맹점번호구분</center>,<center>가맹점번호</center>,<center>미수금</center>,<center>미수잔액</center>";
                    outamtMasterGrid.setHeader(outamtMasterGridheaders);
                    outamtMasterGrid.setColAlign("center,center,center,center,right,right");
                    outamtMasterGrid.setColTypes("txt,txt,txt,txt,edn,edn");
                    outamtMasterGrid.setInitWidths("50,150,150,200,150,150");
                    outamtMasterGrid.setColSorting("str,str,str,str,int,int");
                    var outamtMasterGridFooters = "총계,#cspan,#cspan,#cspan,<div id='outamtMasterGrid4'>0</div>,<div id='outamtMasterGrid5'>0</div>";
                    outamtMasterGrid.attachFooter(outamtMasterGridFooters);
                    outamtMasterGrid.setNumberFormat("0,000",4,".",",");
                    outamtMasterGrid.setNumberFormat("0,000",5,".",",");
                    outamtMasterGrid.enableColumnMove(true);
                    outamtMasterGrid.setSkin("dhx_skyblue");
                    outamtMasterGrid.groupBy(1,["","#title","#cspan","","#stat_total","#stat_total"]);
                    outamtMasterGrid.init();
                    outamtMasterGrid.parse(${outamtMasterList}, outamtMasterGridFooterValues, "json");
                    outamtMasterGrid.attachEvent("onRowDblClicked", doSearchFormOutamtDetail);
                    
                    outamtMasterWindow = new dhtmlXWindows();
                    outamtMasterWindow.attachViewportTo("outamtMasterList");
                    
                    //검색조건 초기화
                    $("#outamtMasterForm input[name=init]").click(function () {
                        outamtMasterInit($("#outamtMasterForm"));
                    });  
                    
                    $("#outamtMasterForm input[name=week]").click(function(){
                        out_amt_date_search("week");
                    });

                    $("#outamtMasterForm input[name=month]").click(function(){
                        out_amt_date_search("month");
                    });
                    
                    $("#outamtMasterForm input[name=search]").click(function(){
                        doSearchOutamtMaster();
                    });                    
                    
                    $("#outamtMasterForm input[name=excel]").click(function(){
                        doSearchOutamtMasterExcel();
                    });                                        
                    
                    $("#outamtMasterForm .addButton").button({
                        icons: {
                            primary: "ui-icon-plusthick"
                        },
                        text: false
                    })
                    .unbind("click")
                    .bind("click",function(e){
                        //미수금 등록 이벤트
                        doInsertFormOutamtMaster();
                        return false;
                    });                    
                   
                });
                
                function doSearchOutamtMasterExcel(){
                    $("#outamtMasterForm").attr("action","<c:url value="/outamt/outamtMasterListExcel" />");
                    document.getElementById("outamtMasterForm").submit();                                    
                }
                
                function outamtListsetSens(id, k) {
                    //alert(id);
                    //alert(k);
                    // update range
                    if (k == "min") {
                        outamtMasterListCalendar.setSensitiveRange(byId(id).value, null);
                    } else {
                        outamtMasterListCalendar.setSensitiveRange(null, byId(id).value);
                    }
                }
			
                function byId(id) {
                    return document.getElementById(id);
                }
                
                //미수금 정보 조회
                function doSearchOutamtMaster(){
                    $.ajax({
                        url : "<c:url value="/outamt/outamtMasterList" />",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#outamtMasterForm").serialize(),
                        success : function(data) {
                            outamtMasterGrid.clearAll();
                            outamtMasterGrid.parse(jQuery.parseJSON(data), "json");
                            outamtMasterGrid.groupBy(1,["","#title","#cspan","","#stat_total","#stat_total"]);
                        },
                        error : function() {
                            alert("조회 실패");
                        }
                    });
                }      
                
                //미수금 상세 목록 조회
                function doSearchFormOutamtDetail(rowid){
                    var pay_mtd = outamtMasterGrid.getUserData(rowid, 'pay_mtd');
                    var merch_no = outamtMasterGrid.getUserData(rowid, 'merch_no');
                    var gen_dt = outamtMasterGrid.getUserData(rowid, 'gen_dt');
                    
                    outamtLayout.cells('b').attachURL("<c:url value="/outamt/outamtDetailList" />" + "?pay_mtd="+pay_mtd+"&gen_start_dt="+gen_dt+"&gen_end_dt="+gen_dt+"&merch_no="+merch_no,true);
                }
                
                function outamtMasterGridFooterValues() {
                    
                    var outamtMasterGrid4 = document.getElementById("outamtMasterGrid4");
                    var outamtMasterGrid5 = document.getElementById("outamtMasterGrid5");
                    outamtMasterGrid4.innerHTML = putComma(sumColumn(4));
                    outamtMasterGrid5.innerHTML = putComma(sumColumn(5));

                    return true;
                }                
                
                function sumColumn(ind) {
                    
                    var out = 0;
                    for (var i = 0; i < outamtMasterGrid.getRowsNum(); i++) {
                         out += parseFloat(outamtMasterGrid.cells2(i, ind).getValue());
                    }

                    return out;
                    
                }                
                
                function outamtMasterInit($form) {
                    searchFormInit($form);
                    out_amt_date_search("week");                    
                } 
                
                function out_amt_date_search(day){
                    var oneDate = 1000 * 3600 * 24; 
                    var now = new Date();
                    var week = new Date(now.getTime() + (oneDate*-6));
                    var month = new Date(now.getTime() + (oneDate*-31));
                    var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                    var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                    var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                    var date=new Date();
                    if(day=="today"){
                        this.$("input[id=outamtMasterList_gen_start_dt]").val(nowTime);
                        this.$("input[id=outamtMasterList_gen_end_dt]").val(nowTime);
                    }else if(day=="week"){
                        this.$("input[id=outamtMasterList_gen_start_dt]").val(weekTime);
                        this.$("input[id=outamtMasterList_gen_end_dt]").val(nowTime);
                    }else{
                        this.$("input[id=outamtMasterList_gen_start_dt]").val(monthTime);
                        this.$("input[id=outamtMasterList_gen_end_dt]").val(nowTime);
                    }
                }
            </script>
            
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>
