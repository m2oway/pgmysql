<%-- 
    Document   : outamtMasterUpdate
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>미수금 상계</title>
        </head>
        <body>
</c:if>

            <form id="outamtMasterFormUpdate" method="POST" action="<c:url value="/outamt/outamtMasterUpdate" />" modelAttribute="outamtFormBean">
                <input type="hidden" name="outamt_seq" value="${ls_outamtDetailList[0].outamt_seq}" />
                <table class="gridtable" height="40%" width="100%">
                    <tr>
                        <td class="headcol">가맹점번호구분</td>
                        <td>${ls_outamtDetailList[0].mid_nm}</td>
                        <td class="headcol">가맹점번호</td>
                        <td>${ls_outamtDetailList[0].merch_no}</td>
                    </tr>
                    <tr>
                        <td class="headcol">미수금 생성금</td>
                        <td>${ls_outamtDetailList[0].out_amt}</td>
                        <td class="headcol">미수잔액</td>
                        <td>${ls_outamtDetailList[0].cur_outamt}</td>
                    </tr>
                    <tr>
                        <td class="headcol">사유</td>
                        <td>
                            <select name="gen_reson">
                                <c:forEach var="code" items="${insResonList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${ls_outamtDetailList[0].gen_reson == code.detail_code}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td class="headcol">미수생성일</td>
                        <td>${ls_outamtDetailList[0].gen_dt}</td>
                    </tr>
                    <tr>
                        <td class="headcol">메모</td>
                        <td colspan="3">
                            <textarea name="memo" cols="60">${ls_outamtDetailList[0].memo}</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            <input type="button" name="update" value="수정"/>
                            <input type="button" name="delete" value="삭제"/>
                            <input type="button" name="close" value="닫기"/>
                        </td>
                    </tr>
                </table>
            </form>
                
            <c:if test="${ls_outamtDetailList[0].outamt_stat == '0'}">

                <form id="outamtDetailFormInsert" method="POST" action="<c:url value="/outamt/outamtDetailInsert" />" modelAttribute="outamtFormBean">
                    <input type="hidden" name="outamt_seq" value="${ls_outamtDetailList[0].outamt_seq}" />
                    <table class="gridtable" height="30%" width="100%">
                        <tr>
                            <td class="headcol">상계방법</td>
                            <td>
                                <select name="setoff_cate">
                                    <c:forEach var="code" items="${setoffCateList.tb_code_details}">
                                        <option value="${code.detail_code}">${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td class="headcol">상계금액</td>
                            <td><input name="setoff_amt" onkeyup="javascript:InpuOnlyNumber(this);" onblur="javascript:checkLength(this,20);" /></td>
                        </tr>
                        <tr>
                            <td class="headcol">상계일자</td>
                            <td><input name="setoff_date" id="outamtDetailFormInsertSetoff_date"/></td>
                            <td class="headcol"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="headcol">메모</td>
                            <td colspan="3">
                                <textarea name="memo" cols="60"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center"><input type="button" name="insert" value="상계처리"/></td>
                        </tr>
                    </table>

                </form>

            </c:if>

            <div id="outamtMasterFormUpdateList" style="position: relative;width:800px;height:145px;background-color:white;"></div>
            <button id="setoffDelete">삭제</button>

            <script type="text/javascript">
                
                var outamtMasterUpdateGrid;
                
                var deposit_popup_yn = '${outamtFormBean.deposit_popup_yn}';
                
                $(document).ready(function () {
                    
                    new dhtmlXCalendarObject("outamtDetailFormInsertSetoff_date");
                        
                    outamtMasterUpdateGrid = new dhtmlXGridObject('outamtMasterFormUpdateList');
                    outamtMasterUpdateGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var outamtMasterUpdateGridheaders = "";
                    outamtMasterUpdateGridheaders += "<center>NO</center>,<center>선택</center>,<center>상계일자</center>,<center>상계방법</center>,<center>상계금액</center>,<center>메모</center>";
                    outamtMasterUpdateGrid.setHeader(outamtMasterUpdateGridheaders);
                    outamtMasterUpdateGrid.setColAlign("center,center,center,center,right,center");
                    outamtMasterUpdateGrid.setColTypes("txt,ch,txt,txt,edn,txt");
                    outamtMasterUpdateGrid.setInitWidths("50,50,150,150,100,150");
                    outamtMasterUpdateGrid.setColSorting("str,str,str,str,int,str");
                    outamtMasterUpdateGrid.setNumberFormat("0,000",4,".",",");
                    outamtMasterUpdateGrid.enableColumnMove(true);
                    outamtMasterUpdateGrid.setSkin("dhx_skyblue");
                    outamtMasterUpdateGrid.init();
                    outamtMasterUpdateGrid.parse(${ls_setoffinfoList}, "json");             
                    
                    //미수금 수정 이벤트
                    $("#outamtMasterFormUpdate input[name=update]").click(function(){
                       doUpdateOutamtMaster(); 
                    });
                    
                    //미수금 삭제 이벤트
                    $("#outamtMasterFormUpdate input[name=delete]").click(function(){
                       doDeleteOutamtMaster(); 
                    });                    
                    
                    //미수금 상계 이벤트
                    $("#outamtDetailFormInsert input[name=insert]").click(function(){
                       doInsertOutamtDetail(); 
                    });              
                    
                    //미수금 수정 닫기
                    $("#outamtMasterFormUpdate input[name=close]").click(function(){
                        
                        if(deposit_popup_yn === "Y"){
                            depositMetabolismWindow.window("outamtListWindow").close();
                            outamtDetailWindowsObject.window('outamtDetailWindow').close();
                        }else{
                            outamtMasterWindow.window("w1").close();
                        }
                        
                    });
                    
                    //상계 삭제 이벤트
                    $("#setoffDelete").click(function(){
                       doDeleteSetoff(); 
                    });                                        
                    
                });

                //미수금 수정
                function doUpdateOutamtMaster(){
                    
                    if(!window.confirm("수정하시겠습니까?")){
                        return;
                    }
                    
                    $.ajax({
                        url : $("#outamtMasterFormUpdate").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#outamtMasterFormUpdate").serialize(),
                        success : function(data) {
                            alert("수정 완료");
                            
                            if(deposit_popup_yn === "Y"){
                                middepositMasterSearch();
                                middepositMastergrid.clearAll();
                                depositMetabolismWindow.window("outamtListWindow").close();
                                outamtDetailWindowsObject.window('outamtDetailWindow').close();
                            }else{
                                doSearchOutamtDetail();
                                outamtDetailGrid.clearAll();
                                outamtMasterWindow.window("w1").close();
                            }
                            
                        },
                        error : function() { 
                                alert("수정 실패");
                        }
                    });	      
        
                }           
                
                //미수금 상계 등록
                function doInsertOutamtDetail(){
                    
                    if(!window.confirm("상계하시겠습니까?")){
                        return;
                    }
                    
                    $.ajax({
                        url : $("#outamtDetailFormInsert").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#outamtDetailFormInsert").serialize(),
                        success : function(data) {
                            alert("상계 완료");
                            
                            if(deposit_popup_yn === "Y"){
                                middepositMasterSearch();
                                middepositMastergrid.clearAll();
                                depositMetabolismWindow.window("outamtListWindow").close();
                                outamtDetailWindowsObject.window('outamtDetailWindow').close();
                            }else{
                                doSearchOutamtMaster();
                                outamtDetailGrid.clearAll();
                                outamtMasterWindow.window("w1").close();
                            }
                        },
                        error : function() { 
                                alert("상계 실패");
                        }
                    });	      
        
                }            
                
                //미수금 삭제
                function doDeleteOutamtMaster(){
                    
                    if(${ls_outamtDetailList[0].proc_flag == 'Y'}){
                        alert("입금확정된 미수금입니다.");
                        return;
                    }
                    
                    if(!window.confirm("삭제하시겠습니까?")){
                        return;
                    }
                    
                    $.ajax({
                        url : "<c:url value="/outamt/outamtMasterDelete" />",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#outamtMasterFormUpdate").serialize(),
                        success : function(data) {
                            alert("삭제 완료");                      
                            
                            if(deposit_popup_yn === "Y"){
                                middepositMasterSearch();
                                middepositMastergrid.clearAll();
                                depositMetabolismWindow.window("outamtListWindow").close();
                                outamtDetailWindowsObject.window('outamtDetailWindow').close();
                            }else{
                                doSearchOutamtMaster();
                                outamtDetailGrid.clearAll();
                                outamtMasterWindow.window("w1").close();
                            }                            
                        },
                        error : function() { 
                                alert("삭제 실패");
                        }
                    });	      
        
                }        
                
                //상계 정보 삭제
                function doDeleteSetoff(){

                    var rowId = outamtMasterUpdateGrid.getCheckedRows(1);
                    
                    if(rowId.length < 1){
                        alert("삭제할 상계내역을 체크하십시오.");
                        return;
                    }
                    
                    var rowIds = rowId.split(",");
                    
                    var setoff_amt = 0;
                    
                    for(i=0;i<rowIds.length;i++){
                        setoff_amt += Number(outamtMasterUpdateGrid.getUserData(rowIds[i], 'setoff_amt'));
                    }
                    
                    var outamt_stat = '${ls_outamtDetailList[0].outamt_stat}';
                    var outamt_seq = '${ls_outamtDetailList[0].outamt_seq}';
                    
                    if(!window.confirm("삭제하시겠습니까?")){
                        return;
                    }

                    $.ajax({
                        url : "<c:url value="/outamt/outamtSetoffDelete" />",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: {setoff_seqs : rowId
                               ,setoff_amt : setoff_amt
                               ,outamt_stat : outamt_stat
                               ,outamt_seq : outamt_seq},
                        success : function(data) {
                            alert("삭제 완료");     
                            
                            if(deposit_popup_yn === "Y"){
                                middepositMasterSearch();
                                middepositMastergrid.clearAll();
                                depositMetabolismWindow.window("outamtListWindow").close();
                                outamtDetailWindowsObject.window('outamtDetailWindow').close();
                            }else{
                                doSearchOutamtMaster();
                                outamtDetailGrid.clearAll();
                                outamtMasterWindow.window("w1").close();
                            }                                 
                        },
                        error : function(jqXHR, textStatus, errorThrown) {
                            alert("삭제 실패 = " + errorThrown);

                        }
                    });
                }                

            </script>                
                
                
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

