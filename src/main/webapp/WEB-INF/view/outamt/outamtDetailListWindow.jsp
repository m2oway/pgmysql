<%-- 
    Document   : outamtDetailList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>미수금 정보 관리</title>
        </head>
        <body>
</c:if>

            <div id="outamtDetailListWindow" style="width:100%;height:100%;background-color:white;"></div>
            
            <script type="text/javascript">
                var outamtWindowDetailGrid;

                var outamtDetailWindowsObject;
                var outamtDetailWindows={};
                $(document).ready(function () {


                    outamtWindowDetailGrid = outamtWindows.attachGrid()

                    outamtDetailWindowsObject = new dhtmlXWindows();

                    outamtWindowDetailGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var outamtDetailGridheaders = "";
                    outamtDetailGridheaders += "<center>NO</center>,<center>미수금 발생일</center>,<center>가맹점번호구분</center>,<center>가맹점번호</center>";
                    outamtDetailGridheaders += ",<center>발생미수액</center>,<center>미수잔액</center>,<center>사유</center>,<center>상계상태</center>";
                    outamtWindowDetailGrid.setHeader(outamtDetailGridheaders);
                    outamtWindowDetailGrid.setColAlign("center,center,center,center,right,right,center,center");
                    outamtWindowDetailGrid.setColTypes("txt,txt,txt,txt,edn,edn,txt,txt");
                    outamtWindowDetailGrid.setInitWidths("50,150,150,130,100,100,100,100");
                    outamtWindowDetailGrid.setColSorting("str,str,str,str,int,int,str,str");
                    outamtWindowDetailGrid.setNumberFormat("0,000",4,".",",");
                    outamtWindowDetailGrid.setNumberFormat("0,000",5,".",",");
                    outamtWindowDetailGrid.enableColumnMove(true);
                    outamtWindowDetailGrid.setSkin("dhx_skyblue");
                    outamtWindowDetailGrid.init();
                    outamtWindowDetailGrid.parse(${outamtDetailList}, "json");
                    outamtWindowDetailGrid.attachEvent("onRowDblClicked", doUpdateFormOutamtWin);
                   
                });

                //미수금 상계 조회
                function doUpdateFormOutamtWin(rowid){

                    var outamt_seq = outamtWindowDetailGrid.getUserData(rowid, 'outamt_seq');
                    var deposit_popup_yn = '${deposit_popup_yn}';

                    var dhtmlxwindowSize = {};

                    dhtmlxwindowSize.width = 900;
                    dhtmlxwindowSize.height = 600;

                    var location = dhxwindowCenterAlgin(dhtmlxwindowSize);

                    outamtDetailWindow = outamtDetailWindowsObject.createWindow("outamtDetailWindow", location.x, location.y, dhtmlxwindowSize.width, dhtmlxwindowSize.height);

                    outamtDetailWindow.setText("미수금 상계");
                    outamtDetailWindowsObject.window('outamtDetailWindow').setModal(true);
                    outamtDetailWindow.attachURL('<c:url value="/outamt/outamtMasterUpdate" />' + '?outamt_seq='+outamt_seq+"&deposit_popup_yn="+deposit_popup_yn,true);

                }

                
            </script>
            
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>
