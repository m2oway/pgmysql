<%-- 
    Document   : outamtDetailList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>미수금 정보 관리</title>
        </head>
        <body>
</c:if>

            <div class="searchForm1row" id="outamtDetailSearch" style="display:none;width:100%;height:100%;background-color:white;">
                
                <form id="outamtDetailForm" method="POST" action="<c:url value="/outamt/outamtDetailList" />" modelAttribute="outamtFormBean">
                    
                    <div class="label">검색일</div>
                    <div class="input">
                        <input type="text" name="gen_start_dt" id="outamtDetailList_gen_start_dt" value="${outamtFormBean.gen_start_dt}" onclick="outamtdetailsetSens('outamtDetailList_gen_end_dt', 'max');" />~
                        <input type="text" name="gen_end_dt" id="outamtDetailList_gen_end_dt" value="${outamtFormBean.gen_end_dt}" onclick="outamtdetailsetSens('outamtDetailList_gen_start_dt', 'min');" />
                    </div>
                    
                    <div class ="input"><input type="button" name="week" value="1주"/></div>
                    <div class ="input"><input type="button" name="month" value="1달"/></div>


                    <div class="label">가맹점번호</div>
                    <div class="input"><input type="text" name="merch_no" value="${outamtFormBean.merch_no}" /></div>

                    <div class="label">상계상태</div>
                    <div class="input">
                        <select name="outamt_stat">
                            <option value="">전체</option>
                            <c:forEach var="code" items="${outamtStatList.tb_code_details}">
                                <option value="${code.detail_code}">${code.code_nm}</option>
                            </c:forEach>
                        </select>
                    </div>                        

                    <div class="input">
                        <input type="button" name="search" value="검색"/>
                        <input type="button" name="excel" value="엑셀"/>
                        <input type="button" name="init" value="검색조건지우기"/>                        
                    </div>

<!--                    <div class="action">
                        <button class="addButton">추가</button>
                    </div>                                                -->

                </form>
                        
            </div>
            
            <div id="outamtDetailList" style="width:100%;height:100%;background-color:white;"></div>
            
            <script type="text/javascript">
                
                var outamtDetailGrid;
                var outamtDetailListCalendar;
                
                $(document).ready(function () {
                    
                    outamtDetailListCalendar = new dhtmlXCalendarObject(["outamtDetailList_gen_start_dt","outamtDetailList_gen_end_dt"]);

                    var outamtDetailSearch = document.getElementById("outamtDetailSearch");  
                    outamtDetailLayout = outamtLayout.cells('b').attachLayout("2E");
                    outamtDetailLayout.cells('a').hideHeader();
                    outamtDetailLayout.cells('b').hideHeader();
                    outamtDetailLayout.cells('a').attachObject(outamtDetailSearch);
                    outamtDetailLayout.cells('a').setHeight(45);
                    outamtDetailGrid = outamtDetailLayout.cells('b').attachGrid();
                    outamtDetailGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var outamtDetailGridheaders = "";
                    outamtDetailGridheaders += "<center>NO</center>,<center>미수금 발생일시</center>,<center>가맹점번호구분</center>,<center>가맹점번호</center>";
                    outamtDetailGridheaders += ",<center>발생미수액</center>,<center>미수잔액</center>,<center>사유</center>,<center>상계상태</center>";
                    outamtDetailGrid.setHeader(outamtDetailGridheaders);
                    outamtDetailGrid.setColAlign("center,center,center,center,right,right,center,center");
                    outamtDetailGrid.setColTypes("txt,txt,txt,txt,edn,edn,txt,txt");
                    outamtDetailGrid.setInitWidths("50,150,150,150,100,100,100,100");
                    outamtDetailGrid.setColSorting("str,str,str,str,int,int,str,str");
                    outamtDetailGrid.setNumberFormat("0,000",4,".",",");
                    outamtDetailGrid.setNumberFormat("0,000",5,".",",");
                    outamtDetailGrid.enableColumnMove(true);
                    outamtDetailGrid.setSkin("dhx_skyblue");
                    
                    var outamtDetailGridFilters = ",,#select_filter,#text_filter,#numeric_filter,#numeric_filter,#select_filter,#select_filter";
                    outamtDetailGrid.attachHeader(outamtDetailGridFilters);                    
                    
                    outamtDetailGrid.init();
                    outamtDetailGrid.parse(${outamtDetailList}, "json");
                    outamtDetailGrid.attachEvent("onRowDblClicked", doUpdateFormOutamt);
                    
                    $("#outamtDetailForm input[name=week]").click(function(){
                        outamtDetail_date_search("week");
                    });

                    $("#outamtDetailForm input[name=month]").click(function(){
                        outamtDetail_date_search("month");
                    });
                    
                    //검색조건 초기화
                    $("#outamtDetailForm input[name=init]").click(function () {

                        outamtDetailInit($("#outamtDetailForm"));

                    });  
                    
                    //조회
                    $("#outamtDetailForm input[name=search]").click(function () {

                        doSearchOutamtDetail();

                    });          
                    
                    //엑셀
                    $("#outamtDetailForm input[name=excel]").click(function () {

                        doSearchOutamtDetailExcel();

                    });                       
                    
//                    $("#outamtDetailForm .addButton").button({
//                        icons: {
//                            primary: "ui-icon-plusthick"
//                        },
//                        text: false
//                    })
//                    .unbind("click")
//                    .bind("click",function(e){
//                        //사업체 정보 등록 이벤트
//                        doInsertFormOutamtMaster();
//                        return false;
//                    });                          
                   
                });
                
                function doSearchOutamtDetailExcel(){
                    $("#outamtDetailForm").attr("action","<c:url value="/outamt/outamtDetailListExcel" />");
                    document.getElementById("outamtDetailForm").submit();                                    
                }                
                
                function outamtDetailInit($form) {

                    searchFormInit($form);

                    outamtDetail_date_search("week");                    
                } 

                
                function outamtDetail_date_search(day){
                    var oneDate = 1000 * 3600 * 24; 
                    var now = new Date();
                    var week = new Date(now.getTime() + (oneDate*-6));
                    var month = new Date(now.getTime() + (oneDate*-31));
                    var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                    var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                    var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                    var date=new Date();
                    if(day=="today"){
                        this.$("input[id=outamtDetailList_gen_start_dt]").val(nowTime);
                        this.$("input[id=outamtDetailList_gen_end_dt]").val(nowTime);
                    }else if(day=="week"){
                        this.$("input[id=outamtDetailList_gen_start_dt]").val(weekTime);
                        this.$("input[id=outamtDetailList_gen_end_dt]").val(nowTime);
                    }else{
                        this.$("input[id=outamtDetailList_gen_start_dt]").val(monthTime);
                        this.$("input[id=outamtDetailList_gen_end_dt]").val(nowTime);
                    }
                }

                function outamtdetailsetSens(id, k) {
                    // update range
                    if (k == "min") {
                        outamtDetailListCalendar.setSensitiveRange(byId(id).value, null);
                    } else {
                        outamtDetailListCalendar.setSensitiveRange(null, byId(id).value);
                    }
                }
			
                function byId(id) {
                    return document.getElementById(id);
                }                
                
                //미수금 상세 목록 조회
                function doSearchOutamtDetail(){
                    $.ajax({
                        url : "<c:url value="/outamt/outamtDetailList" />",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#outamtDetailForm").serialize(),
                        success : function(data) {
                            outamtDetailGrid.clearAll();
                            outamtDetailGrid.parse(jQuery.parseJSON(data), "json");
                        },
                        error : function() {
                            alert("조회 실패");
                        }
                    });
                }      
                
                //미수금 상계 조회
                function doUpdateFormOutamt(rowid){
                    
                    var outamt_seq = outamtDetailGrid.getUserData(rowid, 'outamt_seq');
                    w1 = outamtMasterWindow.createWindow("w1", 100, 100, 900, 600);
                    w1.setText("미수금 상계");
                    outamtMasterWindow.window('w1').setModal(true);
                    w1.attachURL('<c:url value="/outamt/outamtMasterUpdate" />' + '?outamt_seq='+outamt_seq,true);                       
                    
                }
                
                //미수금 등록
//                function doInsertFormOutamtMaster(){
//
//                    w1 = outamtMasterWindow.createWindow("w1", 150, 150, 900, 300);
//                    w1.setText("미수금 등록");
//                    outamtMasterWindow.window('w1').setModal(true);
//                    pay_mtd = '${outamtFormBean.pay_mtd}';
//                    merch_no = '${outamtFormBean.merch_no}';
//                    gen_dt = '${outamtFormBean.gen_dt}';
//                    w1.attachURL("<c:url value="/outamt/outamtMasterInsert" />" + '?pay_mtd='+pay_mtd + '&gen_dt='+gen_dt + '&merch_no='+merch_no, true);   
//
//                }                        
                
            </script>
            
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>
