<%-- 
    Document   : uid_input_sample.jsp
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>일괄 정보 등록</title>
        </head>
        <body>
</c:if>
<%
    String strCurDate = com.onoffkorea.system.common.util.Util.getTodayDate();
    String strTpDefaultEndDt = "20991231";
%>
            <script type="text/javascript">
                var regobj = new RegExp('-',"gi");
                var dhxSelPopupWins;
		dhxSelPopupWins = new dhtmlXWindows();
                
                var midinfo_pay_dt_cd_day ='${PAY_DT_CD_DAY}';
                var midinfo_pay_dt_cd_week = '${PAY_DT_CD_WEEK}';
                var midinfo_pay_dt_cd_half = '${PAY_DT_CD_HALF}';
                var midinfo_pay_dt_cd_month = '${PAY_DT_CD_MONTH}';
                
                var tot_onffmerchant_mygrid,tot_terminal_mygrid,tot_termuser_mygrid;
                
                var tot_onffmerchant_layout;
                
                var tot_terminal_layout;
                
                $(document).ready(function () {
                    
                    tot_onffmerchant_layout = new dhtmlXLayoutObject("tot_onffmerchantVP2", "1C", "dhx_skyblue");
                    tot_onffmerchant_layout.cells('a').hideHeader();

                    tot_onffmerchant_mygrid = tot_onffmerchant_layout.cells('a').attachGrid();
                    tot_onffmerchant_mygrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var mheaders = "";
                    mheaders += "<center>담보종류코드</center>,<center>담보종류</center>,<center>담보액</center>,<center>적용시작일</center>,<center>적용종료일</center>,<center>MEMO</center>";
                    tot_onffmerchant_mygrid.setHeader(mheaders);
                    tot_onffmerchant_mygrid.setColAlign("center,center,center,center,center,left");
                    tot_onffmerchant_mygrid.setColTypes("txt,txt,txt,txt,txt,txt");
                    tot_onffmerchant_mygrid.setInitWidths("10,100,100,80,80,200");
                    tot_onffmerchant_mygrid.setColSorting("str,str,str,str,str,str");
                    tot_onffmerchant_mygrid.enableColumnMove(false);
                    tot_onffmerchant_mygrid.setSkin("dhx_skyblue");
                    tot_onffmerchant_mygrid.init();
                    //tot_onffmerchant_mygrid.parse(${termMasterList}, "json");
                    //tot_onffmerchant_mygrid.attachEvent("onRowDblClicked", doInsertMerchantTidInsert);
 
                   tot_onffmerchant_mygrid.setColumnHidden(0,true);   
                   //tot_onffmerchant_mygrid.setColumnHidden(3,true);                    
                    
                    tot_terminal_layout = new dhtmlXLayoutObject("totonfftid_cmsVP2", "1C", "dhx_skyblue");
                    tot_terminal_layout.cells('a').hideHeader();

                    tot_terminal_mygrid = tot_terminal_layout.cells('a').attachGrid();
                    tot_terminal_mygrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var mheaders = "";
                    mheaders += "<center>수수료</center>,<center>적용시작일</center>,<center>적용종료일</center>,<center>MEMO</center>";
                    tot_terminal_mygrid.setHeader(mheaders);
                    tot_terminal_mygrid.setColAlign("center,center,center,center");
                    tot_terminal_mygrid.setColTypes("txt,txt,txt,txt");
                    tot_terminal_mygrid.setInitWidths("100,100,100,200");
                    tot_terminal_mygrid.setColSorting("str,str,str,str");
                    tot_terminal_mygrid.enableColumnMove(false);
                    tot_terminal_mygrid.setSkin("dhx_skyblue");
                    tot_terminal_mygrid.init();
        
                    myCalendar = new dhtmlXCalendarObject(["uid_view_onfftid_cms_start_dt","uid_view_onfftid_cms_end_dt","midinfo_view_security_start_dt","midinfo_view_security_end_dt","midinfo_view_close_dt"]);
                    
                    termuser_layout = new dhtmlXLayoutObject("onff_userVP2", "1C", "dhx_skyblue");
                    termuser_layout.cells('a').hideHeader();

                    tot_termuser_mygrid = termuser_layout.cells('a').attachGrid();
                    tot_termuser_mygrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var mheaders = "";
                    mheaders += "<center>ID</center>,<center>암호</center>,<center>이름</center>,<center>사용자유형명</center>,<center>사용자유형</center>";
                    tot_termuser_mygrid.setHeader(mheaders);
                    tot_termuser_mygrid.setColAlign("center,center,center,center,center");
                    tot_termuser_mygrid.setColTypes("txt,txt,txt,txt,txt");
                    tot_termuser_mygrid.setInitWidths("100,100,100,100,100");
                    tot_termuser_mygrid.setColSorting("str,str,str,str,str");
                    tot_termuser_mygrid.enableColumnMove(false);
                    tot_termuser_mygrid.setSkin("dhx_skyblue");
                    tot_termuser_mygrid.init();                    
                    
                    
                    $("#totInfoFormInsert input[name=midinfo_app_chk_amt]").keyup(function(){

                          amtToHan($(this),$("#totInfoFormInsert span[name=insert_amt_han]"),Number($(this).attr("maxlength")));

                    });
                    
                    $("#totInfoFormInsert input[name=midinfo_security_amt]").keyup(function(){

                        amtToHan($(this),$("#totInfoFormInsert span[name=insert_security_amt_han]"),Number($(this).attr("maxlength")));

                    });    
       
                });
                
                
                function TotInsCompInfoAutoInput()
                {
                    $("#totInfoFormInsert input[name=uid_onfftid_nm]").val($("#totInfoFormInsert input[name=comp_nm]").val());
                    $("#totInfoFormInsert input[name=midinfo_merch_nm]").val($("#totInfoFormInsert input[name=comp_nm]").val());
                    
                    $("#totInfoFormInsert input[name=midinfo_tel_1]").val($("#totInfoFormInsert input[name=comp_tel1]").val());
                    $("#totInfoFormInsert input[name=midinfo_tel_2]").val($("#totInfoFormInsert input[name=comp_tel2]").val());
                    
                    $("#totInfoFormInsert input[name=midinfo_tel_1]").val($("#totInfoFormInsert input[name=comp_tel1]").val());
                    $("#totInfoFormInsert input[name=midinfo_tel_2]").val($("#totInfoFormInsert input[name=comp_tel2]").val());
                    
                    $("#totInfoFormInsert input[name=midinfo_zip_cd]").val($("#totInfoFormInsert input[name=comp_zip_cd]").val());
                    $("#totInfoFormInsert input[name=midinfo_addr_1]").val($("#totInfoFormInsert input[name=comp_addr_1]").val());
                    $("#totInfoFormInsert input[name=midinfo_addr_2]").val($("#totInfoFormInsert input[name=comp_addr_2]").val());
                    $("#totInfoFormInsert input[name=midinfo_email]").val($("#totInfoFormInsert input[name=comp_email]").val());
                    
                }
                
                
                function TotInscompcateAction(obj)
                {
                    var selval = obj.value;
                    
                    if(selval=='2')
                    {
                        $("#totInfoFormInsert input[name=comp_corp_no]").keypress(InpuOnlyNumber_Jquery($("#totInfoFormInsert input[name=comp_corp_no]")));
                        $("#totInfoFormInsert input[name=comp_corp_no]").blur(checkLength_Jquery($("#totInfoFormInsert input[name=comp_corp_no]"),13));
                        $("#totInfoFormInsert input[name=comp_corp_no]").attr('readOnly',false); 
                    }
                    else
                    {
                        $("#totInfoFormInsert input[name=comp_corp_no]").attr('value','');
                        $("#totInfoFormInsert input[name=comp_corp_no]").attr('readOnly',true);
                    }
                }
                
                
                function TotInsselChangeBalPeriod()
                {
                    var selbalperiod = $("#midinfo_bal_period option:selected").val();
                    var selbalperiod_size = $("#midinfo_pay_dt_cd_1 option").size();

                    var jsonobj;
                    if(selbalperiod == "DAY")
                    {
                        jsonobj = JSON.parse(midinfo_pay_dt_cd_day);
                    }
                    else if(selbalperiod == "WEEK")
                    {
                        jsonobj = JSON.parse(midinfo_pay_dt_cd_week);
                    }
                    else if(selbalperiod == "HALF")
                    {
                        jsonobj = JSON.parse(midinfo_pay_dt_cd_half);
                    }
                    else if(selbalperiod == "MONTH")
                    {
                       jsonobj = JSON.parse(midinfo_pay_dt_cd_month);
                    }
                    
                    $("#midinfo_pay_dt_cd_1").find('option').remove();
                    $("#midinfo_pay_dt_cd_2").find('option').remove();
                    
                    $("#midinfo_pay_dt_cd_1").append($("<option></option>").val('').html('--선택하세요.--'));
                    $("#midinfo_pay_dt_cd_2").append($("<option></option>").val('').html('--선택하세요.--'));
                    
                    $.each(jsonobj, function(){
                            $("#midinfo_pay_dt_cd_1").append($("<option></option>").val(this['ID']).html(this['VIEW']));
                        }
                    );
            
                    if(selbalperiod == "HALF")
                    {
                            $("#midinfo_pay_dt_cd_2").attr('disabled',null);
                            $.each(jsonobj, function(){
                                $("#midinfo_pay_dt_cd_2").append($("<option></option>").val(this['ID']).html(this['VIEW']));
                            }
                        );
                    }
                    else
                    {
                        $("#midinfo_pay_dt_cd_2").attr('disabled','true');
                    }
                    
                }               
                
                
                
                //UID 수수료 정보 입력
                function TotInsmid_cms_addRow()
                {
                   var strTpStartdt =  $("#totInfoFormInsert input[name=uid_view_onfftid_cms_start_dt]").val().replace(regobj,'');
                   var strTpEnddt =  $("#totInfoFormInsert input[name=uid_view_onfftid_cms_end_dt]").val().replace(regobj,'');

                  
                   if($("#totInfoFormInsert input[name=uid_onfftid_cms_commission]").val() == '') 
                   {
                       alert('수수료는 필수 입니다.');
                       $("#totInfoFormInsert input[name=uid_onfftid_cms_commission]").focus();
                       return false;
                   }
                   
                  if($("#totInfoFormInsert input[name=uid_view_onfftid_cms_start_dt]").val() == '') 
                   {
                       alert('시작일은 필수 입니다.');
                       $("#totInfoFormInsert input[name=uid_view_onfftid_cms_start_dt]").focus();
                       return false;
                   }
                   
                  if($("#totInfoFormInsert input[name=uid_view_onfftid_cms_end_dt]").val() == '') 
                   {
                       alert('종료일 필수 입니다.');
                       $("#totInfoFormInsert input[name=uid_view_onfftid_cms_end_dt]").focus();
                       return false;
                   }                   
                   
                   if(strTpStartdt > strTpEnddt) 
                   {
                       alert('시작일,종료일이 잘못되었습니다.');
                       $("#totInfoFormInsert input[name=uid_view_onfftid_cms_start_dt]").focus();
                       return false;
                   }     
                   
                   var chkflg = 0;
                   tot_terminal_mygrid.forEachRow(function(id){
                        // "<center>수수료</center>,<center>적용시작일</center>,<center>적용종료일</center>,<center>MEMO</center>";
                        if(strTpStartdt <= tot_terminal_mygrid.cells(id,2).getValue())
                        {
                            chkflg = 1;
                        }
                    
                   });
        
                   if(chkflg==1) 
                   {
                       alert('등록하시려는 수수료의 시작일이 기존 등록된 수수료의 종료일보다 큽니다.');
                       return false;
                   }
                   else
                   {
                       
                        //"<center>수수료</center>,<center>적용시작일</center>,<center>적용종료일</center>,<center>MEMO</center>";
                        //         0                        1                          2                          3 
                        var strnewRow = 
                                $("#totInfoFormInsert input[name=uid_onfftid_cms_commission]").val() 
                                + "," + 
                                strTpStartdt
                                + "," + 
                                strTpEnddt
                                + "," + 
                                $("#totInfoFormInsert textarea[name=uid_onfftid_cms_memo]").val()
                        tot_terminal_mygrid.addRow(tot_terminal_mygrid.getUID(), strnewRow, 1);
                        
                        
                        $("#totInfoFormInsert input[name=uid_view_onfftid_cms_start_dt]").val("");
                        $("#totInfoFormInsert input[name=uid_view_onfftid_cms_end_dt]").val("");
                        $("#totInfoFormInsert input[name=uid_onfftid_cms_commission]").val("");
                        $("#totInfoFormInsert input[name=uid_view_onfftid_cms_start_dt]").val("");
                        $("#totInfoFormInsert input[name=uid_view_onfftid_cms_end_dt]").val("");
                        $("#totInfoFormInsert textarea[name=uid_onfftid_cms_memo]").val("");
                   }

                }
                
                //uid 수수료 정보 삭제
                function TotInsmid_cms_delRow()
                {
                   var selRowid = tot_terminal_mygrid.getSelectedRowId();
                   
                    tot_terminal_mygrid.deleteRow(selRowid);                                
                }
                
                
                //지급id 담보정보 
                function TotInsmidinfo_security_addRow()
                {
                   
                    
                   var instpval = $("#totInfoFormInsert select[name=midinfo_security_cate] option:selected").val();
                   var strTpStartdt =  $("#totInfoFormInsert input[name=midinfo_view_security_start_dt]").val().replace(regobj,'');
                   var strTpEnddt =  $("#totInfoFormInsert input[name=midinfo_view_security_end_dt]").val().replace(regobj,'');
                   
                   
                   if(instpval == '') 
                   {
                       alert('담보종류는 필수 입니다.');
                       $("#totInfoFormInsert select[name=midinfo_security_cate]").focus();
                       return false;
                   }
                   
                   if($("#totInfoFormInsert input[name=midinfo_security_amt]").val() == '') 
                   {
                       alert('담보액은 필수 입니다.');
                       $("#totInfoFormInsert input[name=midinfo_security_amt]").focus();
                       return false;
                   }
                   
                  if($("#totInfoFormInsert input[name=midinfo_view_security_start_dt]").val() == '') 
                   {
                       alert('시작일은 필수 입니다.');
                       $("#totInfoFormInsert input[name=midinfo_view_security_start_dt]").focus();
                       return false;
                   }
                   
                  if($("#totInfoFormInsert input[name=midinfo_view_security_end_dt]").val() == '') 
                   {
                       alert('종료일 필수 입니다.');
                       $("#totInfoFormInsert input[name=midinfo_view_security_end_dt]").focus();
                       return false;
                   }             
                   
                   
                   if(strTpStartdt > strTpEnddt) 
                   {
                       alert('시작일,종료일이 잘못되었습니다.');
                       $("#totInfoFormInsert input[name=midinfo_view_security_start_dt]").focus();
                       return false;
                   }     
                   

                   var chkflg = 0;
                   tot_onffmerchant_mygrid.forEachRow(function(id){
                   //alert(tot_onffmerchant_mygrid.cells(id,3).getValue()); 시작일 
                   //alert(tot_onffmerchant_mygrid.cells(id,4).getValue()); 종료일
                   if( (strTpStartdt <= tot_onffmerchant_mygrid.cells(id,4).getValue()) )
                        {
                            chkflg = 1;
                        }
                    
                   });
                   
                    var strTpsecurity_amt = $("#totInfoFormInsert input[name=midinfo_security_amt]").val();
                    strTpsecurity_amt =strTpsecurity_amt.replace(/,/g, '');
        
                   if(chkflg==1) 
                   {
                       alert('등록하시려는 담보의 시작일이 기존 등록된 담보의 종료일보다 큽니다.');
                       return false;
                   }
                   else
                   {   
                        //담보종류코드,담보종류,담보액,적용시작일,적용종료일,MEMO
                        //   0           1      2        3        4         5
                        var strnewRow = 
                                $("#totInfoFormInsert select[name=midinfo_security_cate] option:selected").val() 
                                + "," + 
                                $("#totInfoFormInsert select[name=midinfo_security_cate] option:selected").text() 
                                + "," + 
                                //$("#totInfoFormInsert input[name=midinfo_security_amt]").val() 
                                strTpsecurity_amt
                                + "," + 
                                strTpStartdt
                                + "," + 
                                strTpEnddt
                                + "," + 
                                $("#totInfoFormInsert textarea[name=midinfo_security_memo]").val()
                        tot_onffmerchant_mygrid.addRow(tot_onffmerchant_mygrid.getUID(), strnewRow, 1);
                        
                        
                        $("#totInfoFormInsert input[name=midinfo_view_security_start_dt]").val("");
                        $("#totInfoFormInsert input[name=midinfo_view_security_end_dt]").val("");
                        $("#totInfoFormInsert input[name=midinfo_security_amt]").val(""); 
                        $("#totInfoFormInsert textarea[name=midinfo_security_memo]").val("");
                        $("#totInfoFormInsert span[name=insert_security_amt_han]").text("");
                   }

                }
                
                function TotInsmidinfo_security_delRow()
                {
                   var selRowid = tot_onffmerchant_mygrid.getSelectedRowId();
                   
                    tot_onffmerchant_mygrid.deleteRow(selRowid);                                
                }
                
                
                //loginid
                function TotInsuserinfo_addRow()
                {
                   if($("#totInfoFormInsert input[name=userinfo_userid]").val() == '') 
                   {
                       alert('아이디는 필수 입니다.');
                       $("#totInfoFormInsert input[name=userinfo_userid]").focus();
                       return false;
                   }
                   
                  if($("#totInfoFormInsert input[name=userinfo_pwd]").val() == '') 
                   {
                       alert('암호는 필수 입니다.');
                       $("#totInfoFormInsert input[name=userinfo_pwd]").focus();
                       return false;
                   }
                   
                  if($("#totInfoFormInsert input[name=userinfo_name]").val() == '') 
                   {
                       alert('이름은 필수 입니다.');
                       $("#totInfoFormInsert input[name=userinfo_name]").focus();
                       return false;
                   }                   
                   
                   if($("#totInfoFormInsert input[name=userinfo_user_cate] option:selected").val() == '') 
                   {
                       alert('사용자유형은 필수 입니다.');
                       $("#totInfoFormInsert input[name=userinfo_user_cate]").focus();
                       return false;
                   }
                   
                   var tmpCheckloginIDReuslt = totInpuUserIDChk($("#totInfoFormInsert input[name=userinfo_userid]").val());
                   if(tmpCheckloginIDReuslt != 0)
                   {
                       alert("이미 등록된 ID 입니다.");
                       $("#totInfoFormInsert input[name=userinfo_userid]").focus();
                       return false;
                   }
                   

                    //"<center>ID</center>,<center>암호</center>,<center>이름</center>,<center>사용자유형명</center>,<center>사용자유형</center>";
                    //         0                        1                          2                          3 
                    var strnewRow = 
                    $("#totInfoFormInsert input[name=userinfo_userid]").val() 
                    + "," + 
                    $("#totInfoFormInsert input[name=userinfo_pwd]").val() 
                    + "," + 
                    $("#totInfoFormInsert input[name=userinfo_name]").val()                         
                    + "," + 
                    $("#totInfoFormInsert select[name=userinfo_user_cate] option:selected").text()
                    + "," + 
                    $("#totInfoFormInsert select[name=userinfo_user_cate] option:selected").val();
            
            
                    tot_termuser_mygrid.addRow(tot_termuser_mygrid.getUID(), strnewRow, 1);
                        
                        
                    $("#totInfoFormInsert input[name=userinfo_userid]").val("");
                    $("#totInfoFormInsert input[name=userinfo_pwd]").val("");
                    $("#totInfoFormInsert input[name=userinfo_name]").val("");
                    $("#totInfoFormInsert select[name=userinfo_user_cate]").val("");
                  

                }
                
                //사용자정보 삭제
                function TotInsuserinfo_delRow()
                {
                   var selRowid = tot_termuser_mygrid.getSelectedRowId();
                   
                    tot_termuser_mygrid.deleteRow(selRowid);                                
                }
                
                
                //결제수단 팝업(CID)
                function TotInsPaymtdSelectWin(){
                    w2 = dhxSelPopupWins.createWindow("PaymtdSelectPopUp", 20, 30, 640, 480);
                    w2.setText("가맹점 선택페이지");
                    dhxSelPopupWins.window('PaymtdSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('PaymtdSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/PayMtdSelectPopUp"/>', true);
                }                 
                //결제수단(CID) 입력
                function SelPaymtdInfoInput( p_paymtd_seq, p_paymtd_nm  )
                {
                    $("#totInfoFormInsert input[name=uid_pay_mtd_nm]").attr('value',p_paymtd_nm);
                    $("#totInfoFormInsert input[name=uid_pay_mtd_seq]").attr('value',p_paymtd_seq);
                    
                }
                
                //대리점 팝업
                function TotInsAgentSelectWin(){
                    w2 = dhxSelPopupWins.createWindow("AgentSelectPopUp", 20, 30, 640, 480);
                    w2.setText("대리점 선택페이지");
                    dhxSelPopupWins.window('AgentSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('AgentSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/AgentSelectPopUp"/>', true);
                } 
                // 대리점 입력
                function SelAgentInfoInput(p_agent_seq, p_agent_nm)
                {
                    //agent_seq
                    $("#totInfoFormInsert input[name=midinfo_agent_seq]").attr('value',p_agent_seq);  
                    $("#totInfoFormInsert input[name=midinfo_agent_nm]").attr('value',p_agent_nm);     
                } 
                
                
                //정보저장
                function doInsertTotoInfoMaster(){
                    
                    if(!window.confirm("등록하시겠습니까?")){
                        return;
                    }
                    
                    
                    //사업자정보 검사
                    if($("#totInfoFormInsert input[name=comp_biz_no_chk]").val() != "1")
                    {
                      alert("사업자번호를 중복검증 해주세요.");
                      return;
                    }
                    
                    //사업자정보 검사
                    if($("#totInfoFormInsert input[name=comp_biz_no]").val() == "")
                    {
                      alert("사업자번호를 입력해 주세요.");
                      $("#totInfoFormInsert input[name=comp_biz_no]").focus();
                      return;
                    }
                    
                    if($("#totInfoFormInsert select[name=comp_cate]").val() == "2")
                    {
                        if($("#totInfoFormInsert input[name=comp_corp_no]").val() == "")
                        {
                            alert("법인번호를 입력해 주세요.");
                            $("#totInfoFormInsert input[name=comp_corp_no]").focus();
                            return;
                        }
                     
                    }
                    
                    if($("#totInfoFormInsert input[name=comp_ceo_nm]").val() == "")
                    {
                      alert("대표자명을 입력해 주세요.");
                      $("#totInfoFormInsert input[name=comp_ceo_nm]").focus();
                      return;
                    }
                    
                    if($("#totInfoFormInsert input[name=comp_nm]").val() == "")
                    {
                      alert("사업체명을 입력해 주세요.");
                      $("#totInfoFormInsert input[name=comp_nm]").focus();
                      return;
                    }
                    
                    if($("#totInfoFormInsert input[name=comp_email]").val() == "")
                    {
                      alert("이메일을 입력해 주세요.");
                      $("#totInfoFormInsert input[name=comp_email]").focus();
                      return;
                    }          
                    
                    //UID 정보검사
                    if($("#totInfoFormInsert select[name=uid_pay_chn_cate]").val() == "")
                    {
                      alert("결제채널은 필수사항 입니다.");
                       $("#totInfoFormInsert select[name=uid_pay_chn_cate]").focus();
                      return;
                    }
                    
                    if($("#totInfoFormInsert select[name=uid_svc_stat]").val() == "")
                    {
                      alert("상태는 필수사항 입니다.");
                      $("##totInfoFormInsert select[name=uid_svc_stat]").focus();
                      return;
                    }
                    
                    if($("#totInfoFormInsert input[name=uid_onfftid_nm]").val() == "")
                    {
                      alert("UID명은 필수사항 입니다.");
                      $("#totInfoFormInsert input[name=uid_onfftid_nm]").focus();
                      return;
                    }                    
                    
                    if($("#totInfoFormInsert input[name=uid_pay_mtd_nm]").val() == "")
                    {
                      alert("결제상품은 필수사항 입니다.");
                      return;
                    }     
        
                    //지급ID 정보검사
                    if($("#totInfoFormInsert input[name=midinfo_merch_nm]").val() == "")
                    {
                      alert("가맹점명은 필수사항 입니다.");                      
                      $("#totInfoFormInsert input[name=midinfo_merch_nm]").focus();
                      return;
                    }
                    
                    if($("#totInfoFormInsert input[name=midinfo_tel_1]").val() == "")
                    {
                      alert("가맹점 연락처1 필수사항 입니다.");
                      $("#totInfoFormInsert input[name=midinfo_tel_1]").focus();
                      return;
                    }
                    
                    if($("#totInfoFormInsert select[name=midinfo_bal_period]").val() == "")
                    {
                      alert("정산주기는 필수사항 입니다.");
                      $("#totInfoFormInsert select[name=midinfo_bal_period]").focus();
                      return;
                    }         
                    
                    if($("#totInfoFormInsert select[name=midinfo_pay_dt_cd_1]").val() == "")
                    {
                      alert("정산기준일은 필수사항 입니다.");
                      $("#totInfoFormInsert select[name=midinfo_pay_dt_cd_1]").focus();
                      return;
                    }

                    var selbalperiod = $("#midinfo_bal_period option:selected").val();
                    if(selbalperiod == 'HALF')
                    {
                        if($("#totInfoFormInsert select[name=midinfo_pay_dt_cd_2]").val() == "")
                        {
                          alert("정산기준일은 필수사항 입니다.");
                          $("#totInfoFormInsert select[name=midinfo_pay_dt_cd_2]").focus();
                          return;
                        }                                      
                    }
                    
                    if($("#totInfoFormInsert input[name=midinfo_pay_dt]").val() == "")
                    {
                      alert("지급일은 필수사항 입니다.");
                      $("#totInfoFormInsert input[name=midinfo_pay_dt]").focus();
                      return;
                    }                    
                    
                    
                    if($("#totInfoFormInsert select[name=midinfo_bank_cd]").val() == "")
                    {
                      alert("입금은행은 필수사항 입니다.");
                      $("#totInfoFormInsert select[name=midinfo_bank_cd]").focus();
                      return;
                    }                    
                    
                    
                    if($("#totInfoFormInsert input[name=midinfo_acc_no]").val() == "")
                    {
                      alert("계좌번호는 필수사항 입니다.");
                      $("#totInfoFormInsert input[name=midinfo_acc_no]").focus();
                      return;
                    }                                        
                    
                    if($("#totInfoFormInsert input[name=midinfo_agent_seq]").val() != "")
                    {
                        if($("#totInfoFormInsert input[name=midinfo_agent_commission]").val() == "")
                        {
                          alert("대리점수수료는 필수사항 입니다.");
                          $("#totInfoFormInsert input[name=midinfo_agent_commission]").focus();
                          return;
                        }               
                    }                                                    
                    
                    if($("#totInfoFormInsert input[name=midinfo_acc_nm]").val() == "")
                    {
                      alert("계좌주는 필수사항 입니다.");
                      $("#totInfoFormInsert input[name=midinfo_acc_nm]").focus();
                      return;
                    }            
                    
                    var tmpappchkamt = $("#totInfoFormInsert input[name=midinfo_app_chk_amt]").val();
                    
                    $("#totInfoFormInsert input[name=midinfo_app_chk_amt]").val(tmpappchkamt.replace(/,/g, ''));                    
                    
                    if($("#totInfoFormInsert input[name=midinfo_app_chk_amt]").val() == "")
                    {
                      alert("결제한도는 필수사항 입니다.");
                      $("#totInfoFormInsert input[name=midinfo_app_chk_amt]").focus();
                      return;
                    }        
                    
                    
                    if($("#totInfoFormInsert input[name=midinfo_appreq_chk_amt]").val() == "")
                    {
                      alert("건별 결제한도는 필수사항 입니다.");
                      $("#totInfoFormInsert input[name=midinfo_appreq_chk_amt]").focus();
                      return;
                    }                         
                    
                    
                    if($("#totInfoFormInsert input[name=midinfo_agent_seq]").val() != "")
                    {
                      if($("#totInfoFormInsert input[name=midinfo_agent_commission]").val() == "")
                      {
                        alert("대리점 선택시 대리점 수수료는 필수사항 입니다.");
                        $("#totInfoFormInsert input[name=midinfo_agent_commission]").focus();
                        return;
                      }
                    }
                  
                    if($("#totInfoFormInsert input[name=midinfo_view_close_dt]").val() != "")
                    {
                        $("#totInfoFormInsert input[name=midinfo_close_dt]").attr('value',$("#totInfoFormInsert input[name=midinfo_view_close_dt]").val().replace(/-/g,''));
                    }
                    
                    //수수료 정보를 만든다.
                    var commissioninfo = "";
                    tot_terminal_mygrid.forEachRow(function(id){
                        
                     var strlastCol = tot_terminal_mygrid.cells(id,3).getValue();
                     if(strlastCol =='') strlastCol= " ";
                        
                         //"<center>수수료</center>,<center>적용시작일</center>,<center>적용종료일</center>,<center>MEMO</center>";
                        //            0                           1                          2                      3  
                       var row = tot_terminal_mygrid.cells(id,0).getValue() + '```' + tot_terminal_mygrid.cells(id,1).getValue() + '```' + tot_terminal_mygrid.cells(id,2).getValue() + '```' + strlastCol;
                       
                       commissioninfo = commissioninfo + row + ']]]';
                    });
                    
                    $("#totInfoFormInsert input[name=commissioninfo]").attr('value',commissioninfo); 
                    
                    //가맹점 담보 정보를 만든다.
                    var security_info = "";
                    tot_onffmerchant_mygrid.forEachRow(function(id){
                     
                     var strlastCol =tot_onffmerchant_mygrid.cells(id,5).getValue();
                     if(strlastCol =='') strlastCol= " ";
                        //담보종류코드,담보종류,담보액,적용시작일,적용종료일,MEMO
                        //   0           1      2        3        4         5
                       var row = tot_onffmerchant_mygrid.cells(id,0).getValue() + '```' + tot_onffmerchant_mygrid.cells(id,2).getValue() + '```' + tot_onffmerchant_mygrid.cells(id,3).getValue() + '```' + tot_onffmerchant_mygrid.cells(id,4).getValue() + '```' + strlastCol;
                       
                       security_info = security_info + row + ']]]';
                    });
                   
                    $("#totInfoFormInsert input[name=security_info]").attr('value',security_info);  
                    
                    
                    //사용자 ID 정보를 만든다.
                    var user_info = "";
                    tot_termuser_mygrid.forEachRow(function(id){                     
                        //"<center>ID</center>,<center>암호</center>,<center>이름</center>,<center>사용자유형명</center>,<center>사용자유형</center>";
                        //         0                    1                      2                       3                            4
                        var row = tot_termuser_mygrid.cells(id,0).getValue() + '```' + tot_termuser_mygrid.cells(id,1).getValue() + '```' + tot_termuser_mygrid.cells(id,2).getValue() + '```' + tot_termuser_mygrid.cells(id,4).getValue();

                        user_info = user_info + row + ']]]';
                    });
                   
                    $("#totInfoFormInsert input[name=user_info]").attr('value',user_info);  
                   
                    $.ajax({
                        url : $("#totInfoFormInsert").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#totInfoFormInsert").serialize(),
                        success : function(data) {
                            //alert(data);
                            if(data=='1')
                            {
                                alert("등록 완료");
                                tabbar_add('3-3','UID정보 관리','/terminal/terminalMasterList',true);
                            }
                            else
                            {
                                alert("등록 실패");
                            }
                        },
                        error : function() { 
                                alert("등록 실패");
                        }
                    });
                   
                }             
                
                function totInputBizChk()
                {
                    $.ajax({
                        url : "<c:url value="/onfftotinput/onfftotInputBizNoChk" />?comp_biz_no="+$("#totInfoFormInsert input[name=comp_biz_no]").val(),
                        type : "GET",
                        async : false,
                        dataType : "json",
                        data: "",
                        success : function(data) {
                            //alert(data);
                            if(data=='0')
                            {
                                alert("등록이 가능한 사업자번호 입니다.");   
                                $("#totInfoFormInsert input[name=comp_biz_no_chk]").val("1");
                            }
                            else
                            {
                                alert("이미 등록된 사업자번호 입니다.");
                                $("#totInfoFormInsert input[name=comp_biz_no_chk]").val("");
                            }
                        },
                        error : function() {
                                alert("검증실패");
                                $("#totInfoFormInsert input[name=comp_biz_no_chk]").val("1");
                        }
                        });
                }
                    
                function totInpuUserIDChk(userid)
                {
                    var chkreturnval = "-1";
                    $.ajax({
                        url : "<c:url value="/onfftotinput/onfftotInputLoginIdChk" />?userinfo_userid="+userid,
                        type : "GET",
                        async : false,
                        dataType : "json",
                        data: "",
                        success : function(data) {
                            chkreturnval = data;
                            if(data=='0')
                            {
                                alert("등록이 가능한 ID 입니다.");  
                                //alert("등록 완료");
                            }
                            else
                            {
                                alert("이미 등록된 ID 입니다.");
                            }
                        },
                        error : function() {
                                alert("검증실패");
                        }
                    });
                    
                    return chkreturnval;
                    
                }
                
            </script>
        <div style='width:100%;height:100%;overflow:auto;'>            
            <form id="totInfoFormInsert" method="POST" action="<c:url value="/onfftotinput/onfftotInput" />">
                <table class="gridtable" height="100%" width="100%">   
                    <tr>
                        <td align='left' colspan='4'>원장관리 > 통합등록</td>                        
                    </tr>
                    <tr>
                        <td align='left' colspan='4'>- 사업자 정보</td>                        
                    </tr>
                    <tr>
                        <td align='left' colspan='4'>
                            <table class="gridtable" height="100%" width="100%">  
                                <tr>
                                    <td class="headcol">사업자번호*</td>
                                    <td><input name="comp_biz_no" maxlength="10" onkeydown="javascript:InpuOnlyNumber(this);" onblur="javascript:checkLength(this,10);"/>&nbsp;<button onclick="javascript:totInputBizChk();return false;">중복검증</button><input type="hidden" name="comp_biz_no_chk" id="comp_biz_no_chk"/></td>                                    
                                    <td class="headcol">법인번호*</tD>
                                    <td><input name="comp_corp_no" size="14" onkeydown="javascript:InpuOnlyNumber(this);" onblur="javascript:checkLength(this,13);" readonly="true"/></td>
                                    <td class="headcol">개인/법인구분*</tD>
                                    <td><select name="comp_cate" onchange="javascript:TotInscompcateAction(this);">
                                <c:forEach var="code" items="${compCateList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select></td>                                     
                                    <td class="headcol">사업자명*</td>
                                    <td><input name="comp_nm" id="comp_nm"  size="30" onblur="javascirpt:TotInsCompInfoAutoInput(this);checkLength(this,20);" /></td>                                    
                                </tr>
                                 <tr>
                                    <td class="headcol">업태</td>
                                    <td><input name="comp_biz_type" onblur="javascript:checkLength(this,50);"/></td>
                                    <td class="headcol">업종</tD>
                                    <td><input name="comp_biz_cate" onblur="javascript:checkLength(this,50);"/></td>                                    
                                    <td class="headcol">과세구분*</tD>
                                    <td ><select name="comp_tax_flag">
                                <c:forEach var="code" items="${TaxFlagList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select></td>                        
                                    <td class="headcol">대표자명*</tD>
                                    <td ><input name="comp_ceo_nm"  onblur="javascript:checkLength(this,20);"/></td>                                    
                                </tr>             
                                 <tr>
                                    <td class="headcol">대표번호*</td>
                                    <td><input name="comp_tel1" onblur="javascirpt:TotInsCompInfoAutoInput(this);" onkeydown="javascript:InpuOnlyNumber(this);" /></td>                                    
                                    <td class="headcol">FAX</tD>
                                    <td><input name="comp_tel2" onblur="javascirpt:TotInsCompInfoAutoInput(this);" onkeydown="javascript:InpuOnlyNumber(this);" /></td>
                                    <td class="headcol">이메일</td>
                                    <td><input name="comp_email" onblur="javascirpt:TotInsCompInfoAutoInput(this);"/></td>
                                    <td class="headcol">우편번호</td>
                                    <td><input name="comp_zip_cd" maxlength="6" onkeydown="javascript:InpuOnlyNumber(this);" onblur="javascirpt:TotInsCompInfoAutoInput(this);"/></td>
                                </tr>     
                                <tr>
                                    <td class="headcol">주소</td>
                                    <td colspan="3"><input name="comp_addr_1" size="30" onblur="javascirpt:TotInsCompInfoAutoInput(this);"/>&nbsp;<input name="comp_addr_2" size="30" onblur="javascirpt:TotInsCompInfoAutoInput(this);"/></td>                                    
                                    <td class="headcol">Memo</tD>
                                    <td colspan="3"><textarea name="comp_memo" cols="60" rows="3"></textarea></td>
                                </tr>                                
                            </table>
                        </td>                        
                    </tr>
                    <tr>
                        <td align='left' colspan='4'>- UID 정보</td>                        
                    </tr>
                    <tr>
                        <td align='left' colspan='4'>
                            <table class="gridtable" height="100%" width="100%">  
                                <tr>
                                    <td align='left' colspan='6'>UID 기본정보</td>                        
                                </tr>                                
                                <tr>                        
                                    <td class="headcol" width="20%">UID명*</td>
                                    <td ><input name="uid_onfftid_nm" size="40" maxlength="50" onblur="javascript:checkLength(this,50);" /></td>                        
                                    <td class="headcol">결제채널*</td>
                                   <td><select name="uid_pay_chn_cate" ><c:forEach var="code" items="${PayChnCateList.tb_code_details}">
                                                    <option value="${code.detail_code}" >${code.code_nm}</option>
                                            </c:forEach></select>                           
                                   </td>
                                    <td class="headcol">가맹점상태*</td>
                                   <td ><select name="uid_svc_stat" >
                                        <c:forEach var="code" items="${UseFlagList.tb_code_details}">
                                            <option value="${code.detail_code}" >${code.code_nm}</option>
                                        </c:forEach>
                                    </select></td>                                   
                                </tr>
                                <tr>
                                    <td class="headcol">결제인증구분*</td>
                                    <td ><select name="uid_cert_type" >
                                <c:forEach var="code" items="${CertTypeList.tb_code_details}">
                                    <option value="${code.detail_code}" >${code.code_nm}</option>
                                </c:forEach></select> 
                                    </td>                        
                                    <td class="headcol">결제취소권한*</td>
                                    <td ><select name="uid_cncl_auth" >
                                <c:forEach var="code" items="${Cncl_AuthList.tb_code_details}">
                                    <option value="${code.detail_code}" >${code.code_nm}</option>
                                </c:forEach></select>
                                    </td>                        
                                    <td class="headcol">CID*선택</td>
                                    <td ><input name="uid_pay_mtd_nm" id="uid_pay_mtd_nm" size='30' readonly onclick='javascript:TotInsPaymtdSelectWin();'/><input type='hidden' name="uid_pay_mtd_seq" id='uid_pay_mtd_seq'>&nbsp;<button name="cidserchbt" id="cidserchbt" onclick='javascript:TotInsPaymtdSelectWin();return false;'>검색</button></td>
                                </tr>                     
                                <tr>
                                    <td class="headcol">Memo</td>
                                    <td colspan="5"><textarea name="uid_memo" id="uid_memo" cols="60" rows="3"></textarea></td>                        
                                </tr>                          
                                <tr>
                                    <td align='left' colspan='6'>UID 수수료 정보</td>                        
                                </tr>
                                <tr>
                                    <td class="headcol" >수수료율*</tD>
                                    <td ><input name="uid_onfftid_cms_commission"  onkeyPress="javascript:InpuOnlyNumber(this);"  />%</td>    
                                    <td class="headcol">시작일*</td>
                                    <td><input name="uid_view_onfftid_cms_start_dt" id="uid_view_onfftid_cms_start_dt" maxlength="10" value="<%=strCurDate%>" readonly/><input type="hidden" name="uid_onfftid_cms_start_dt"/></td>
                                    <td class="headcol">종료일*</tD>
                                    <td><input name="uid_view_onfftid_cms_end_dt" id="uid_view_onfftid_cms_end_dt" maxlength="10" value="<%=strTpDefaultEndDt%>" readonly/><input type="hidden" name="uid_onfftid_cms_end_dt"/></td>
                                </tr> 
                                <tr>
                                    <td class="headcol">Memo</td>
                                    <td colspan="6"><textarea name="uid_onfftid_cms_memo" cols="60" rows="2"></textarea></td>                        
                                </tr>                     
                                <tr>
                                    <td align='left' colspan='6' valign="top">
                                        <div id = "common_btn" align = "right" style="height:30px;" >
                                        <input type="button" name="onfftid_cms_addbt" value="수수료 추가" onclick="javascript:TotInsmid_cms_addRow();"/>
                                        <input type="button" name="onfftid_cms_delbt" value="수수료 삭제" onclick="javascript:TotInsmid_cms_delRow();"/>
                                        </div>
                                        <div id="totonfftid_cmsVP2" style="position: relative; width:100%;height:100px;background-color:white;"></div>
                                    </td>                        
                                </tr>                                
                            </table>
                        </td>                        
                    </tr>                    
                    <tr>
                        <td align='left' colspan='4'>- 지급ID 정보</td>                        
                    </tr>
                    <tr>
                        <td align='left' colspan='4'>
                            <table class="gridtable" height="100%" width="100%">  
                                <tr>
                                    <td align='left' colspan='6'>지급ID 기본정보</td>                        
                                </tr>    
                                <tr>                        
                                    <td class="headcol" >지급ID명*</td>
                                    <td ><input name="midinfo_merch_nm" size="40" maxlength="50" onblur="javascript:checkLength(this,50);" /></td>                        
                                    <td class="headcol" >연락처1*</td>
                                    <td ><input name="midinfo_tel_1"  maxlength="20"/></td>
                                    <td class="headcol" >연락처2</td>
                                    <td ><input name="midinfo_tel_2" size="20" maxlength="20" /></td>
                                </tr>
                                <tr>                        
                                    <td class="headcol" >E-MAIL</td>
                                    <td ><input name="midinfo_email" size="25" maxlength="100" /></td>
                                    <td class="headcol">우편번호</td>
                                    <td><input name="midinfo_zip_cd" maxlength="6" /></td>
                                    <td class="headcol">주소</tD>
                                    <td ><input name="midinfo_addr_1" size="40" />&nbsp;<input name="midinfo_addr_2" size="40" /></td>
                                </tr>             
                                 <tr>
                                    <td class="headcol">결제한도*</td>
                                    <td ><input name="midinfo_app_chk_amt"  onkeyPress="javascript:InpuOnlyNumber2(this);"  maxlength="15" /><span name="insert_amt_han"></span><span>원</span></td>     
                                    <td class="headcol">건별결제한도*</td>
                                    <td ><input name="midinfo_appreq_chk_amt"  onkeyPress="javascript:InpuOnlyNumber2(this);"  maxlength="15" value="0" />(미사용시 0원 입력)</td>                            
                                    <td class="headcol">취소권한*</td>
                                    <td ><select name="midinfo_cncl_auth" >
                                            <c:forEach var="code" items="${Cncl_AuthList.tb_code_details}">
                                                <option value="${code.detail_code}" >${code.code_nm}</option>
                                            </c:forEach>
                                        </select></td>  
                                 </tr> 
                                <tr>
                                    <td class="headcol">지급ID상태*</td>
                                    <td ><select name="midinfo_svc_stat" >
                                <c:forEach var="code" items="${SvcStatList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Merchant.svc_stat}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach>
                            </select></td>
                                    <td class="headcol">해지일</td>
                                    <td><input name="midinfo_view_close_dt" id="midinfo_view_close_dt" maxlength="10" /><input type="hidden" name="midinfo_close_dt"/></td>
                                    <td class="headcol">해지사유</tD>
                                    <td><input name="midinfo_close_reson" size="50" /></td>
                                </tr>                           
                                <tr>
                                    
                                    <td class="headcol">Memo</td>
                                    <td colspan="5"><textarea name="midinfo_memo" cols="40" rows="3"></textarea></td>     
                                </tr>                    
                                <tr>
                                    <td align='left' colspan='6'>정산정보</td>                        
                                </tr>
                                 <tr>
                                    <td class="headcol">정산주기*</td>
                                    <td ><select name="midinfo_bal_period" id="midinfo_bal_period" onchange="javascript:TotInsselChangeBalPeriod();" >
                                <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${BalPeriodList.tb_code_details}">
                                    <option value="${code.detail_code}" >${code.code_nm}</option>
                                </c:forEach>
                            </select>                          
                                    </td>
                                    <td class="headcol">정산기준일*</td>
                                    <td ><select name="midinfo_pay_dt_cd_1" id="midinfo_pay_dt_cd_1" >
                                <option value="">--선택하세요.--</option>
                            </select>&nbsp;<select name="midinfo_pay_dt_cd_2" id="midinfo_pay_dt_cd_2" >
                                <option value="">--선택하세요.--</option>
                            </select> 
                                    </td>                                    
                                    <td class="headcol">지급일*</td>
                                    <td >정산대상 최종승인일+<input name="midinfo_pay_dt" size="3"  />(영업일)</td>                         
                                </tr>
                                <tr>
                                    <td class="headcol">입금은행*</td>
                                    <td><select name="midinfo_bank_cd" >
                                <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${BankCdList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select></td> 
                                    <td class="headcol">입금계좌*</td>
                                    <td><input name="midinfo_acc_no" maxlength="30" size="30" onkeyPress="javascript:InpuOnlyNumber(this);" /></td>         
                                    <td class="headcol">계좌주*</tD>
                                    <td ><input name="midinfo_acc_nm" size="30"  /></td>                        
                                </tr>  
                                <tr>
                                    <td align='left' colspan='6'>대리점 정보</td>                        
                                </tr>
                                <tr>                        
                                    <td class="headcol" >대리점명</td>
                                    <td><input name="midinfo_agent_nm" id="midinfo_agent_nm" onclick="javascript:TotInsAgentSelectWin();"  readonly /><input type="hidden" name="midinfo_agent_seq" id="midinfo_agent_seq"/></td>
                                    <td class="headcol">대리점수수료</tD>
                                    <td ><input name="midinfo_agent_commission"  onkeyPress="javascript:InpuOnlyNumber(this);"  />%</td>    
                                    <td class="headcol"></tD>
                                    <td ></td>    
                                </tr>
                                <tr>
                                    <td align='left' colspan='6'>담보 정보</td>                        
                                </tr>
                                <tr>                        
                                    <td class="headcol" >담보종류*</td>
                                    <td><select name="midinfo_security_cate" >
                                <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${SecurityCateList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select></td>
                                    <td class="headcol">담보액*</tD>
                                    <td ><input name="midinfo_security_amt"  onkeyPress="javascript:InpuOnlyNumber2(this);"   maxlength="15" /><span name="insert_security_amt_han"></span><span>원</span></td>    
                                    <td class="headcol">시작일*</td>
                                    <td><input name="midinfo_view_security_start_dt" id="midinfo_view_security_start_dt" maxlength="10" value="<%=strCurDate%>" readonly/><input type="hidden" name="midinfo_security_start_dt"/></td>                                    
                                </tr>     
                                <tr>
                                    <td class="headcol">종료일*</tD>
                                    <td><input name="midinfo_view_security_end_dt" id="midinfo_view_security_end_dt" maxlength="10" value="<%=strTpDefaultEndDt%>" readonly/><input type="hidden" name="midinfo_security_end_dt"/></td>
                                    <td class="headcol">Memo</td>
                                    <td colspan="3"><textarea name="midinfo_security_memo" cols="60" rows="3"></textarea></td>                        
                                </tr>  
                                <tr>
                                    <td align='left' colspan='6' valign="top">
                                        <div id = "common_btn" align = "right" style="height:30px;" >
                                        <input type="button" name="midinfo_security_addbt" value="담보추가" onclick="javascript:TotInsmidinfo_security_addRow();"/>
                                        <input type="button" name="midinfo_security_delbt" value="담보삭제" onclick="javascript:TotInsmidinfo_security_delRow();"/>
                                        </div>
                                        <div id="tot_onffmerchantVP2" style="position: relative; width:100%;height:200px;background-color:white;"></div>
                                    </td>                        
                                </tr>                                                   
                            </table>
                        </td>                        
                    </tr>   
                    <tr>                        
                        <td colspan='4'>
                            <table class="gridtable" height="100%" width="100%">   
                                <tr>
                                    <td  align='left' colspan='8'>로그인ID 정보</td>
                                </tr>
                                <tr>
                                    <td class="headcol">아이디</td>
                                    <td><input name="userinfo_userid" /></td>
                                    <td class="headcol">비밀번호</td>
                                    <td><input name="userinfo_pwd"  /></td>
                                    <td class="headcol">이름</td>
                                    <td><input name="userinfo_name" /></td>
                                    <td class="headcol">사용자유형</tD>
                                    <td> <select name="userinfo_user_cate">
                                                <option value="">선택하세요</option>
                                                <c:forEach var="userCateList" items="${userCateList.tb_code_details}">
                                                    <option value="${userCateList.detail_code}">${userCateList.code_nm}</option>
                                                </c:forEach>                                      
                                        </select> 
                                    </td>
                                </tr>         
                                <tr>
                                     <td colspan="8" height="110px">
                                        <div id = "userinfo_common_btn" align = "right" style="height:30px;" >
                                        <input type="button" name="userinfo_addbt" value="로그인ID추가" onclick="javascript:TotInsuserinfo_addRow();"/>
                                        <input type="button" name="userinfo_delbt" value="로그인ID삭제" onclick="javascript:TotInsuserinfo_delRow();"/>
                                        </div>
                                        <div id="onff_userVP2" style="position: relative; width:100%;height:100px;background-color:white;"></div>
                                    </td>    
                                </tr>
                            </table>
                        </td>
                    </tr>                             
                    <tr>
                        <td colspan="4" align="center"><input type="button" name="totiinfosavebt" id="totiinfosavebt" value="등록" onclick="javascirpt:doInsertTotoInfoMaster();"/></td>
                    </tr>       
                </table>
                 <input type="hidden" name="commissioninfo" />
                 <input type="hidden" name="security_info" />       
                 <input type="hidden" name="user_info" />                 
            </form>
        </div>
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>