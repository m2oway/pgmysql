<%-- 
    Document   : codeMasterUpdate
    Created on : 2014. 11. 20, 오후 2:21:17
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>코드 정보 관리 수정</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">
                
                $(document).ready(function () {
                    
                    //코드 정보 수정 이벤트
                     $("#mcodeFormUpdate input[name=mcode_update]").button({                       
                    })
                    .unbind("click")
                    .bind("click",function(e){
                         mcodeUpdate();
                        return false;
                    });
                    //코드 정보 삭제 이벤트
                     $("#mcodeFormUpdate input[name=mcode_delete]").button({                       
                    })
                    .unbind("click")
                    .bind("click",function(e){
                         mcode_Delete();
                        return false;
                    });
                    
                    //코드 정보 수정 닫기
                     $("#mcodeFormUpdate input[name=close]").button({                       
                    })
                    .unbind("click")
                    .bind("click",function(e){
                          codedhxWins.window('codew1').close();
                        return false;
                    });
                    
                });

                //코드 정보 수정
                function mcodeUpdate(){

                    $.ajax({
                        url : $("#mcodeFormUpdate").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#mcodeFormUpdate").serialize(),
                        success : function(data) {
                            alert("수정 완료");
                            mcodeSearch();
                            codedhxWins.window('codew1').close();
                        },
                        error : function() { 
                                alert("수정 실패");
                        }
                    });	      

                }
                
                function mcode_Delete(){
                    $("#mcodeFormUpdate").attr("action","<c:url value="/code/codeMasterDelete" />");                  
                    
                    $.ajax({
                        url : $("#mcodeFormUpdate").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#mcodeFormUpdate").serialize(),
                        success : function(data) {
                            alert("삭제 완료");
                            mcodeSearch();
                            codedhxWins.window('codew1').close();
                        },
                        error : function() { 
                                alert("삭제 실패");
                        }
                    });	    
        
                }                 

            </script>
                      
            <form id="mcodeFormUpdate" method="POST"   action="<c:url value="/code/codeMasterUpdate" />" modelAttribute="codeFormBean">
                <%--<form:hidden path="main_code" value="${tb_Code.main_code}" />--%>
                <table class="gridtable">
                    <tr>
                        <th>메인코드</th>
                        <td><input name="main_code" value="${tb_Code.main_code}" maxlength="20" readonly="true"/></td>
                        <th>메인코드명</th>
                        <td><input name="code_nm" value="${tb_Code.code_nm}" maxlength="20" /></td>
                    </tr>
                    <tr>
                        <th>사용여부</th>
                        <td>
                            <select name="use_flag">
                                <c:if test="${tb_Code.use_flag == 'Y'}">
                                    <option value="Y" selected="selected">사용가능</option>
                                </c:if>
                                <c:if test="${tb_Code.use_flag != 'Y'}">
                                    <option value="Y">사용가능</option>
                                </c:if>                                    
                                <c:if test="${tb_Code.use_flag == 'N'}">
                                    <option value="N" selected="selected">사용불가</option>
                                </c:if>
                                <c:if test="${tb_Code.use_flag != 'N'}">
                                    <option value="N">사용불가</option>
                                </c:if>                    
                            </select>
                        </td> 
                        <th>MEMO</th>
                        <td><input name="code_memo" value="${tb_Code.code_memo}"maxlength="50"/></td>
                    </tr> 
                    <tr>
                        <td style="text-align: center" colspan="4">
                            <input type="button" name="mcode_update" value="수정"/>
                            <input type="button" name="mcode_delete" value="삭제"/>
                            <input type="button" name="close" value="닫기"/>
                        </td>
                    </tr>
                </table>
            </form>
            
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

