<%-- 
    Document   : codeMaster
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>코드관리</title>
        </head>
        <body>
        </c:if>

        <script type="text/javascript">
            
            var codedhxWins;
            
            $(document).ready(function () {

                codedhxWins = new dhtmlXWindows();

                codedhxWins.attachViewportTo("mcode_search");

                var searchForm = document.getElementById("mcode_search");

                mcode_layout = codeMaster_layout.cells('a').attachLayout("2E");

                mcode_layout.cells('a').hideHeader();
                mcode_layout.cells('b').hideHeader();

                mcode_layout.cells('a').attachObject(searchForm);

                mcode_layout.cells('a').setHeight(40);

                mcode_layout.cells('a').fixSize("false","false");


                mcode_layout.cells("a").progressOff();


                mcodegrid = mcode_layout.cells('b').attachGrid();

                mcodegrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");

                var mcheaders = "";
                mcheaders += "<center>NO</center>,<center>코드명</center>,<center>코드</center>,<center>사용여부</center>";
                mcodegrid.setHeader(mcheaders);
                mcodegrid.setColAlign("center,center,center,center");
                mcodegrid.setColTypes("txt,txt,txt,txt");
                mcodegrid.setInitWidths("50,150,100,100");
                mcodegrid.setColSorting("str,str,str,str");
                mcodegrid.enableColumnMove(true);
                mcodegrid.setSkin("dhx_skyblue");
                mcodegrid.init();
                mcodegrid.parse(${codeMaster}, "json");
                mcodegrid.attachEvent("onRowDblClicked", createWindowcodeUpdate);
                mcodegrid.attachEvent("onRowSelect", mcodeList);

                //코드 정보 검색 이벤트
               
                 $("#mcodeForm .searchButton").button({
                        icons: {
                            primary: "ui-icon-search"
                        }
                    })
                    .unbind("click")
                    .bind("click",function(e){
                        //사업자 정보 조회 이벤트
                        mcodeSearch();
                        return false;
                    });
                 //코드 정보 등록 이벤트    
                $("#mcodeForm .addButton").button({
                        icons: {
                            primary: "ui-icon-plusthick"
                        },
                        text: false
                    })
                    .unbind("click")
                    .bind("click",function(e){
                        //사업자 정보 등록 이벤트
                        createWindowcodeInsert();
                        return false;
                    });
            });
            
            //코드 정보 조회
            function mcodeList(rowid, col) {
                var main_codes = mcodegrid.getUserData(rowid, 'main_code');

                $("#dcodeForm input[name=main_code]").val(main_codes);

                var hd_use_flag = mcodegrid.getUserData(rowid, 'use_flag');
                $("#dcodeForm input[name=hd_use_flag]").val(hd_use_flag);

                $.ajax({
                    url: "<c:url value="/code/codeDetailMaster" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#main_code").serialize(),
                    success: function (data) {
                        dcodegrid.clearAll();
                        dcodegrid.parse(jQuery.parseJSON(data), "json");
                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });


            }
            //코드 정보 조회
            function mcodeSearch() {
                $.ajax({
                    url: $("#mcodeForm").attr("action"),
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#mcodeForm").serialize(),
                    success: function (data) {
                        mcodegrid.clearAll();
                        mcodegrid.parse(jQuery.parseJSON(data), "json");
                        $("#dcodeForm input[name=hd_use_flag]").val("");
                        $("#dcodeForm input[name=main_code]").val("");
                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }
            //코드정보입력
            function createWindowcodeInsert() {
                var dhtmlxwindowSize = {};
                    dhtmlxwindowSize.width = 440;
                    dhtmlxwindowSize.height = 155;

                var location = dhxwindowCenterAlgin(dhtmlxwindowSize);
                
                codew1 = codedhxWins.createWindow("codew1", location.x, location.y,   dhtmlxwindowSize.width, dhtmlxwindowSize.height);
                codew1.setText("메인코드등록");
                codedhxWins.window('codew1').setModal(true);
                codew1.attachURL("<c:url value="/code/codeMasterInsert" />", true);
               
            }
            //코드정보수정
            function createWindowcodeUpdate(rowid, col) {
                var main_codes = mcodegrid.getUserData(rowid, 'main_code');
                var dhtmlxwindowSize = {};
                    dhtmlxwindowSize.width = 440;
                    dhtmlxwindowSize.height = 153;
                var location = dhxwindowCenterAlgin(dhtmlxwindowSize);
                
                codew1 = codedhxWins.createWindow("codew1", location.x, location.y,   dhtmlxwindowSize.width, dhtmlxwindowSize.height);
                codew1.setText("메인코드수정");
                codedhxWins.window('codew1').setModal(true);
                codew1.attachURL("<c:url value="/code/codeMasterUpdate?main_code=" />" + main_codes, true);
                
            }

        </script>
    <body>
        <!--<h1>코드관리</h1>-->
        <div class="searchForm1row" id="mcode_search" style="display:none;width:100%;height:100%;background-color:white;">
            <form id="mcodeForm" method="POST" onsubmit="return false" action="<c:url value="/code/codeMaster" />" modelAttribute="codeFormBean">
            
                <div class="label">메인코드명</div>
                <div class="input "><input name="code_nm" /></div>
                <div class="label">사용여부</div>
                <div class="input">
                    <select name="use_flag">
                        <option value="">선택하세요</option>
                        <option value="Y">사용가능</option>
                        <option value="N">사용불가</option>
                    </select>
                </div>
              
                       <button class="searchButton">검색</button>
                         <div class="action">
                    <button class="addButton">추가</button>
                </div>
                   
            </form>
        </div>

        <div id="main_code" style="width:100%;height:100%;background-color:white;"></div>

    </body>
</html>