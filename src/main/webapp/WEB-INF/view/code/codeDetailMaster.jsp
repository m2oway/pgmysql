<%-- 
    Document   : codeDetailMaster
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>상세코드관리</title>
        </head>
        <body>
</c:if>

    <script type="text/javascript">
        
                var codedetaildhxWins;
                var idPrefix = 1;
                codedetaildhxWins = new dhtmlXWindows();
                var dcodegrid;
                var dcode_layout;

                $(document).ready(function () {

                    var searchForm = document.getElementById("dcode_search");

                    dcode_layout = codeMaster_layout.cells('b').attachLayout("2E");

                    dcode_layout.cells('a').hideHeader();
                    dcode_layout.cells('b').hideHeader();

                    dcode_layout.cells('a').attachObject(searchForm);

                    dcode_layout.cells('a').setHeight(40);

                    dcode_layout.cells('a').fixSize("false","false");

                    dcodegrid = dcode_layout.cells('b').attachGrid();

                    dcodegrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");

                    var mcheaders = "";
                    mcheaders += "<center>NO</center>,<center>메인코드</center>,<center>상세코드</center>,";
                    mcheaders += "<center>상세코드명</center>,<center>정렬순서</center>,<center>MEMO</center>,<center>사용여부</center>";
                    dcodegrid.setHeader(mcheaders);
                    dcodegrid.setColAlign("center,center,center,center,center,center,center");
                    dcodegrid.setColTypes("txt,txt,txt,txt,txt,txt,txt");
                    dcodegrid.setInitWidths("50,100,100,150,100,150,100");
                    dcodegrid.setColSorting("str,str,str,str,str,str,str");
                    dcodegrid.enableColumnMove(true);
                    dcodegrid.setSkin("dhx_skyblue");

                    dcodegrid.init();

                    dcodegrid.parse(${codeDetailMaster}, "json");

                    dcodegrid.attachEvent("onRowDblClicked", createWindowcodedetailUpdate);
                    
                    //상세코드검색
                      $("#dcodeForm .searchButton").button({
                        icons: {
                            primary: "ui-icon-search"
                        }
                    })
                    .unbind("click")
                    .bind("click",function(e){
                        //사업자 정보 조회 이벤트
                        dcodeSearch();
                        return false;
                    });
                      //상세코드등록 
                     $("#dcodeForm .addButton").button({
                         icons: {
                          primary: "ui-icon-plusthick"
                        },
                          text: false
                    })
                    .unbind("click")
                    .bind("click",function(e){
                        //사업자 정보 조회 이벤트
                        createWindowcodedetailInsert();
                        return false;
                    });
                       
                    //코드 정보 등록
//                    function dcodeInsert() {
//                        main_code = $("#dcodeForm input[name=main_code]").val();
//                        hd_use_flag = $("#dcodeForm input[name=hd_use_flag]").val();
//                        if(hd_use_flag == 'N'){
//                          alert("메인코드가 사용불가합니다.");
//                          return;
//                        }
//                        parent.tabbar_add('5-2-3', '상세코드 정보 관리 등록', '/code/codeDetailMasterInsert?main_code='+main_code+"&use_flag="+hd_use_flag, true);
//                    }
//
//                    function dcodeUpdate(rowid,col) {
//                        var main_code = dcodegrid.getUserData(rowid, 'main_code');
//                        var detail_code = dcodegrid.getUserData(rowid, 'detail_code');
//                        
//                        parent.tabbar_add('5-2-4', '상세코드 정보 수정', '/code/codeDetailMasterUpdate?detail_code=' + detail_code +'&main_code='+main_code, true);
//                    }
                });
                
                                                    
        //코드 정보 조회
                function dcodeSearch(){
                    $.ajax({
                        url : $("#dcodeForm").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#dcodeForm").serialize(),
                        success : function(data) {
                            dcodegrid.clearAll();
                            dcodegrid.parse(jQuery.parseJSON(data), "json");
                        },
                        error : function() { 
                                alert("조회 실패");
                           }
                    });	            
                }
                function createWindowcodedetailInsert() {
                main_code = $("#dcodeForm input[name=main_code]").val();
                hd_use_flag = $("#dcodeForm input[name=hd_use_flag]").val();
                 var dhtmlxwindowSize = {};
                    dhtmlxwindowSize.width = 450;
                    dhtmlxwindowSize.height = 193;
                var location = dhxwindowCenterAlgin(dhtmlxwindowSize);
                
                    if(hd_use_flag == 'N'){
                      alert("메인코드가 사용불가합니다.");
                      return;
                    }
                    
                codedetailw1 = codedetaildhxWins.createWindow("codedetailw1", location.x, location.y,   dhtmlxwindowSize.width, dhtmlxwindowSize.height);
                codedetailw1.setText("상세코드등록");
                codedetaildhxWins.window('codedetailw1').setModal(true);
                codedetailw1.attachURL("<c:url value="/code/codeDetailMasterInsert" />"+"?main_code="+main_code+"&use_flag="+hd_use_flag, true);
              
            }
             function createWindowcodedetailUpdate(rowid, col) {
                var main_code = dcodegrid.getUserData(rowid, 'main_code');
                var detail_code = dcodegrid.getUserData(rowid, 'detail_code');
                
                var dhtmlxwindowSize = {};
                    dhtmlxwindowSize.width = 440;
                    dhtmlxwindowSize.height = 220;
                var location = dhxwindowCenterAlgin(dhtmlxwindowSize);
                
                codedetailw1 = codedetaildhxWins.createWindow("codedetailw1", location.x, location.y,   dhtmlxwindowSize.width, dhtmlxwindowSize.height);
                codedetailw1.setText("상세코드수정");
                codedetaildhxWins.window('codedetailw1').setModal(true);
                codedetailw1.attachURL("<c:url value="/code/codeDetailMasterUpdate" />"+"?detail_code=" + detail_code +'&main_code='+main_code, true);
            }
    </script>
    <body>
        <!--<h1>코드관리</h1>-->
        <div class="searchForm1row" id="dcode_search" style="width:100%;">            
             <form id="dcodeForm" method="POST" onsubmit="return false" action="<c:url value="/code/codeDetailMaster" />" modelAttribute="codeFormBean">
                        <input type="hidden" name="main_code" value="${main_code}" id= "main_code" />
                        <input type="hidden" name = "page_no" value="1" />
                        <input type="hidden" name = "page_size" value="20" />

                        <input type="hidden" name="hd_use_flag" value="" id= "hd_use_flag" />
                        <div class="label">상세코드명</div>
                                 <div class="input"><input type="text" name="detailcode_nm" /></div>
                                <div class="label">사용여부</div>
                                <div class="input">
                                    <select name="use_flag">
                                        <option value="">선택하세요</option>
                                        <option value="Y">사용가능</option>
                                        <option value="N">사용불가</option>
                                    </select>
                                </div>
                                 <button class="searchButton">검색</button>
                                <div class="action">
                            <button class="addButton">추가</button>
                                </div>
                 </form>
                <div id="recinfoArea"></div>
                <div id="dcodPaging" style="width:100%;"></div>
             </div>
        

      
    </body>
</html>
