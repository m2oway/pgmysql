<%-- 
    Document   : merchantMaterList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>상세코드 정보 관리 등록</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">
                
                $(document).ready(function () {
                    
                    //코드 정보 등록 이벤트
                    $("#dcodeFormInsert input[name=dcode_insert]").button({                       
                    })
                    .unbind("click")
                    .bind("click",function(e){
                         dcodeInsert(); 
                        return false;
                    });
                    
                    //코드 정보 등록 닫기
                    $("#dcodeFormInsert input[name=close]").button({                       
                    })
                    .unbind("click")
                    .bind("click",function(e){
                           codedetaildhxWins.window('codedetailw1').close();
                        return false;
                    });
                    
                });

                //코드 정보 등록
                function dcodeInsert(){
                     if($("#dcodeFormInsert select[name=main_code]").val().trim().length === 0){
                        alert("메인코드를 선택하여 주십시오.");
                        $("#dcodeFormInsert select[name=main_code]").val("");
                        $("#dcodeFormInsert select[name=main_code]").focus();
                        return false;
                    }
                    if($("#dcodeFormInsert input[name=detail_code]").val().trim().length === 0){
                        alert("상세코드를 입력하여 주십시오.");
                        $("#dcodeFormInsert input[name=detail_code]").val("");
                        $("#dcodeFormInsert input[name=detail_code]").focus();
                        return false;
                    }
                     if($("#dcodeFormInsert input[name=code_nm]").val().trim().length === 0){
                        alert("상세코드명을 입력하여 주십시오.");
                        $("#dcodeFormInsert input[name=code_nm]").val("");
                        $("#dcodeFormInsert input[name=code_nm]").focus();
                        return false;
                    }
                     var main_code = $("#select_main_code").val();
                    $.ajax({
                        url : $("#dcodeFormInsert").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#dcodeFormInsert").serialize(),
                        success : function(data) {
                            alert("등록 완료");
                            dcodeSearch();
                            codedetaildhxWins.window('codedetailw1').close();
                        },
                        error : function() { 
                                alert("등록 실패");
                        }
                    });	      
        
                }                   

            </script>

            <form id="dcodeFormInsert" method="POST"  action="<c:url value="/code/codeDetailMasterInsert" />" modelAttribute="codeFormBean">
               <!--<input type="hidden" name="hd_main_code" value="${main_code}" />-->
                <table class="gridtable">
                    <tr>
                       
                        <th>메인코드명</th>
                        <td colspan="3">
                            <select name="main_code" id="select_main_code" >
                            <option value="">선택하세요</option>
                            <c:forEach var="ls_main_code" items="${ls_codeMaster}">
                            <option value="${ls_main_code.main_code}" <c:if test="${ls_main_code.main_code==main_code}">selected</c:if> >${ls_main_code.code_nm}</option>
                            </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>상세코드</th>
                        <td><input name="detail_code" maxlength="20"onblur="javascript:checkLength(this,20);" /></td>
                        <th>상세코드명</th>
                        <td><input name="code_nm" maxlength="20" onblur="javascript:checkLength(this,20);"/></td>
                    </tr>
                    <tr>
                        <th>정렬</th>
                        <td><input name="sort"  maxlength="3" onkeyup="javascript:InpuOnlyNumber(this);" /></td>
                        <th>MEMO</th>
                        <td><input name="code_memo" maxlength="50"onblur="javascript:checkLength(this,50);"/></td>
                    </tr>  
                      <tr>
                          <td style="text-align: center" colspan="4">
                              <input type="button" name="dcode_insert" value="저장"/>
                              <input type="button" name="close" value="취소"/>
                          </td>
                    </tr>
                </table>
                
            </form>

<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

 