<%-- 
    Document   : codeMasterUpdate
    Created on : 2014. 11. 20, 오후 2:21:17
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>코드 정보 관리 수정</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">
                
                $(document).ready(function () {
                    
                    //상세코드 정보 수정 이벤트
                     $("#dcodeFormUpdates input[name=dcode_updates]").button({                       
                    })
                    .unbind("click")
                    .bind("click",function(e){
                        //미수금 등록 이벤트
                         dcodeUpdate(); 
                        return false;
                    });
                    
                    //상세코드 정보 삭제 이벤트
                     $("#dcodeFormUpdates input[name=dcode_delete]").button({                       
                    })
                    .unbind("click")
                    .bind("click",function(e){
                        //미수금 등록 이벤트
                         dcode_Delete(); 
                        return false;
                    });
                    
                    //코드 정보 수정 닫기
                     $("#dcodeFormUpdates input[name=close]").button({                       
                    })
                    .unbind("click")
                    .bind("click",function(e){
                        codedetaildhxWins.window('codedetailw1').close();
                        return false;
                    });
                    
                });                       

                //코드 정보 수정
                function dcodeUpdate(){
                     if($("#dcodeFormUpdates input[name=code_nm]").val().trim().length === 0){
                        alert("코드명을 입력하여 주십시오.");
                        $("#dcodeFormUpdates input[name=code_nm]").val("");
                        $("#dcodeFormUpdates input[name=code_nm]").focus();
                        return false;
                    }
        
                    $.ajax({
                        url : $("#dcodeFormUpdates").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#dcodeFormUpdates").serialize(),
                        success : function(data) {
                            alert("수정 완료");
                            dcodeSearch();
                            codedetaildhxWins.window('codedetailw1').close();
                        },
                        error : function() { 
                                alert("수정 실패");
                        }
                    });	     
        
                }  
                   //코드 정보 삭제
                function dcode_Delete(){ 
                    $("#dcodeFormUpdates").attr("action","<c:url value="/code/codeDetailMasterDelete" />");
        
                    $.ajax({
                        url : $("#dcodeFormUpdates").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#dcodeFormUpdates").serialize(),
                        success : function(data) {
                            alert("삭제 완료");
                            dcodeSearch();
                            codedetaildhxWins.window('codedetailw1').close();
                        },
                        error : function() { 
                                alert("삭제 시류ㅐ");
                        }
                    });	     
        
                } 

            </script>

            <form id="dcodeFormUpdates" method="POST"  action="<c:url value="/code/codeDetailMasterUpdate" />" modelAttribute="codeFormBean">
                <input type="hidden" name="main_code" value="${tb_Code.main_code}" />
                <input type="hidden" name="detail_code" value="${tb_Code.detail_code}" />
                <table class="gridtable">
                    <tr>
                        <th>메인코드</th>
                       <td>${tb_Code.main_code}</td>
                       <th>메인코드명</th>
                       <td>${tb_Code.main_code_nm}</td>
                    </tr>
                     <tr>
                        <th>상세코드</th>
                        <td>${tb_Code.detail_code}</td>
                        <th>상세코드명</th>
                        <td><input name="code_nm" value="${tb_Code.code_nm}" maxlength="20" /></td>
                    </tr>
                    <tr>
                        <th>정렬</th>
                        <td><input name="sort" value="${tb_Code.sort}" maxlength="3" /></td>
                        <th>사용여부</th>
                        <td>
                            <select name="use_flag">
                                <c:if test="${tb_Code.use_flag == 'Y'}">
                                    <option value="Y" selected="selected">사용가능</option>
                                </c:if>
                                <c:if test="${tb_Code.use_flag != 'Y'}">
                                    <option value="Y">사용가능</option>
                                </c:if>                                    
                                <c:if test="${tb_Code.use_flag == 'N'}">
                                    <option value="N" selected="selected">사용불가</option>
                                </c:if>
                                <c:if test="${tb_Code.use_flag != 'N'}">
                                    <option value="N">사용불가</option>
                                </c:if>                    
                            </select>
                        </td> 
                    </tr>
                    <tr>
                        <th>MEMO</th>
                        <td colspan="4"><input name="code_memo"  value="${tb_Code.code_memo}"maxlength="50"/></td>
                    </tr> 
                    <tr>
                        <td style="text-align: center" colspan="4">
                            <input type="submit" name="dcode_updates" value="수정"/>
                            <input type="submit" name="dcode_delete" value="삭제"/>
                            <input type="button" name="close" value="닫기"/>
                        </td>
                    </tr>
                </table>
            </form>
            
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

