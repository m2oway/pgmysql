<%-- 
    Document   : merchantMaterList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>코드 정보 관리 등록</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">
                
                $(document).ready(function () {
                    
                    //코드 정보 등록 이벤트
                    $("#mcodeFormInsert input[name=mcode_insert]").button({                       
                    })
                    .unbind("click")
                    .bind("click",function(e){
                         mcodeInsert(); 
                        return false;
                    });
                    
                    //코드 정보 등록 닫기
                    $("#mcodeFormInsert input[name=mcode_close]").button({                       
                    })
                    .unbind("click")
                    .bind("click",function(e){
                         codedhxWins.window('codew1').close();
                        return false;
                    });
                    
                });

                //코드 정보 등록
                function mcodeInsert(){   
                      if($("#mcodeFormInsert input[name=main_code]").val().trim().length === 0){
                        alert("메인코드를 입력하여 주십시오.");
                        $("#mcodeFormInsert input[name=main_code]").val("");
                        $("#mcodeFormInsert input[name=main_code]").focus();
                        return false;
                    }
                    if($("#mcodeFormInsert input[name=code_nm]").val().trim().length === 0){
                        alert("메인코드명을 입력하여 주십시오.");
                        $("#mcodeFormInsert input[name=code_nm]").val("");
                        $("#mcodeFormInsert input[name=code_nm]").focus();
                        return false;
                    }
                    $.ajax({
                        url : $("#mcodeFormInsert").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#mcodeFormInsert").serialize(),
                        success : function(data) {
                            alert("등록 완료 ");
                            mcodeSearch();
                            codedhxWins.window('codew1').close();
                        },
                        error : function() { 
                                alert("등록 실패");
                        }
                    });	      
        
                }                   

            </script>

            <form id="mcodeFormInsert" method="POST" action="<c:url value="/code/codeMasterInsert" />" modelAttribute="codeFormBean">
                <table class="gridtable">
                    <tr>
                        <th>메인코드</th>
                        <td><input name="main_code" maxlength="20" onblur="javascript:checkLength(this,20);" /></td>
                        <th>메인코드명</th>
                        <td><input name="code_nm" maxlength="20" onblur="javascript:checkLength(this,20);" /></td>
                    </tr>
                    <tr>
                    </tr>            
                    <tr>
                        <th>MEMO</th>
                        <td  colspan="4"><input name="code_memo" maxlength="50" onblur="javascript:checkLength(this,50);"/></td>
                    </tr>           
                    <tr>
                        <td style="text-align: center" colspan="4" >
                            <input type="button" name="mcode_insert" value="저장"/>
                            <input type="button" name="mcode_close" value="취소"/>
                        </td>
                    </tr>
                </table>
            </form>

<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

