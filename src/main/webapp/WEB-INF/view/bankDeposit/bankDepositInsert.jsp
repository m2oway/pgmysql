<%--
  ~ Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  ~ Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
  ~ Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
  ~ Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
  ~ Vestibulum commodo. Ut rhoncus gravida arcu.
  --%>

<%--
    Document   : bankDepositUpdateForm
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>통장입금 내역관리 등록</title>
        </head>
        <body>
</c:if>
            <div class="">
                <div class="popContent">
                    <form id="bankDepositInsertForm" method="POST" action="<c:url value="" />" onsubmit="return false">
                        <input type="hidden" name="deposit_dt" value="" />                                                        <!--통장입금 내역 등록일자-->
                        <input type="hidden" name="pay_mtd" value="" />
                        <input type="hidden" name="merch_no" value="" /> 
                        <input type="hidden" name="account_no" value="" /> 
                        <input type="hidden" name="bank_cd" value="" /> 
                        <input type="hidden" name="deposit_amt" value="" />                                                       <!--통장입금 금액-->
                        <input type="hidden" name="collect_method" value="${collect_methods.tb_code_details[0].detail_code}" />  <!--통장입금 방법-->

                        <table class="gridtable">
                            <tr>
                                <th>*가맹점번호</th>
                                <td colspan="3">
                                    <select name="merch_no_view">
                                        <option value="">선택하세요</option>>
                                        <c:forEach var ="tb_merch" items="${tb_merchs}" >                                           
                                            <option value=${tb_merch.merch_no}>${tb_merch.mid_nm}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>통장번호</th>
                                <td><input type="text" name="account_no_view" id="account_no_view" size="25"  value="" readonly="true"/></td>
                                <th>입금일</th>
                                <td><input type="text" name="bankDepositInsert_deposit_dt" id="bankDepositInsert_deposit_dt" maxlength="10" value=""/></td>
                            </tr>
                            <tr>
                                <th>수집방법</th>
                                <td>
                                    ${collect_methods.tb_code_details[0].code_nm}
                                </td>
                                <th>입금금액</th>
                                <td ><input type="text" name="tem_deposit_amt" maxlength="13"  onkeyup="javascript:InpuOnlyNumber2(this);" />원</td>                                
                            </tr>
                            <tr>
                                <th>메모</th>
                                <td colspan="3">
                                    <textarea name="memo" cols="60"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div class="buttonArea2Button">
                                        <button name ="insert">등록</button>
                                        <button name ="close">닫기</button>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>

            </div>

            <script type="text/javascript">

                $(document).ready(function () {

                    var $insertForm = $("#bankDepositInsertForm");

                    var bankDepositInsertCalendar = new dhtmlXCalendarObject(["bankDepositInsert_deposit_dt"]);

                    $("#bankDepositInsertForm select[name=merch_no_view]").change(function(){
                        var selval = $(this).val();
                        if(selval == "")
                        {
                            $("#bankDepositInsertForm input[name=account_no_view]").val("");
                            $("#bankDepositInsertForm input[name=pay_mtd]").val("");
                            $("#bankDepositInsertForm input[name=account_no]").val("");
                            $("#bankDepositInsertForm input[name=bank_cd]").val("");
                            $("#bankDepositInsertForm input[name=merch_no]").val("");
                        }
                        else
                        {
                            var arrMerchs = selval.split(',');
                            var len = arrMerchs.length; 

                              //pay_mtd||','||MERCH_NO||','||ACC_NO||','||BANK_CD||bank_cd_nm
                              //0,                  1,        2,            3,         4,5,
                            var viewaccno = "("+ arrMerchs[4] +")" +arrMerchs[2];
                            $("#bankDepositInsertForm input[name=account_no_view]").val(viewaccno);
                            $("#bankDepositInsertForm input[name=pay_mtd]").val(arrMerchs[0]);
                            $("#bankDepositInsertForm input[name=account_no]").val(arrMerchs[2]);
                            $("#bankDepositInsertForm input[name=bank_cd]").val(arrMerchs[3]);
                            $("#bankDepositInsertForm input[name=merch_no]").val(arrMerchs[1]);
                        }
                
                        

                    });

                    // 등록
                    $insertForm.find("button[name=insert]").button({
                        text:true
                    })
                    .unbind("click")
                    .bind("click", function (e) {
                        
                        if($("#bankDepositInsertForm select[name=merch_no_view]").val() == "")
                        {
                            alert("MID는 필수항목 입니다.");
                            $("#bankDepositInsertForm select[name=merch_no_view]").focus();
                            return false;
                        }

                        if (confirm("통장 입금내역을 등록 하시겠습니까?") == true){

                            if(isValidDate($("#bankDepositInsert_deposit_dt").val()) ){
                                $insertForm.find("input[name=deposit_dt]").val($("#bankDepositInsert_deposit_dt").val().replace(/[^0-9]/g,""));
                                //$insertForm.find("input[name=deposit_amt]").val($insertForm.find("input[name=tem_deposit_amt]").val().replace(/[^0-9]/g,""));
                                $insertForm.find("input[name=deposit_amt]").val($insertForm.find("input[name=tem_deposit_amt]").val());

                                var obj ={}
                                obj.url ="<c:url value="/bankDeposit/bankDepositInsertManualForm" />";
                                obj.successText ="통장입금내역 등록 완료";
                                obj.failText ="통장입금내역 등록 실패";
                                obj.closeWindowObj = bankDepositWindowObject;
                                obj.clearGrid = bankDepsitGrid;
                                obj.searchFunction = doSearchBankDepsit;

                                BankDepositButtonAction($insertForm,obj);

                            }else{
                                $("#bankDepositInsert_deposit_dt").focus();
                                alert("입금일을 확인하세요");
                            }

                        }else{

                        }

                        return false;
                    });

                    //닫기버튼
                    $insertForm.find("button[name=close]").button({
                        text:true
                    })
                    .unbind("click")
                    .bind("click", function (e) {
                        bankDepositWindow.window("bankDepositWindowObject").close();
                        return false;
                    });

                    /*
                    $insertForm.find("input[name=tem_deposit_amt]").keyup(function(){

                        amtToHan($(this),$insertForm.find("span[name=insert_amt_han]"),Number($(this).attr("maxlength")));
                        

                    });
                    */

                });


            </script>

<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

