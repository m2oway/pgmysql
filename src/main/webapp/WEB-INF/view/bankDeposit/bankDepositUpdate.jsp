<%--
  ~ Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  ~ Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
  ~ Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
  ~ Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
  ~ Vestibulum commodo. Ut rhoncus gravida arcu.
  --%>

<%--
    Document   : bankDepositUpdateForm
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
    <head>
        <title>통장입금 내역관리 수정</title>
    </head>
    <body>
</c:if>


<div class="">
    <div class="popContent">
        <form id="bankDepositUpdateForm" method="POST" onsubmit="return false" action="<c:url value="" />" >
                        <input type="hidden" name="deposit_dt" value="" />                                                        <!--통장입금 내역 등록일자-->
                        <input type="hidden" name="pay_mtd" value="${bank_Deposit_Search.pay_mtd}" />
                        <input type="hidden" name="merch_no" value="${bank_Deposit_Search.merch_no}" /> 
                        <input type="hidden" name="account_no" value="${bank_Deposit_Search.account_no}" /> 
                        <input type="hidden" name="bank_cd" value="${bank_Deposit_Search.bank_cd}" /> 
                        <input type="hidden" name="deposit_amt" value="${bank_Deposit_Search.deposit_amt}" />                                                       <!--통장입금 금액-->
                        <input type="hidden" name="collect_method" value="${bank_Deposit_Search.collect_method}" />  <!--통장입금 방법-->            
                        <input type="hidden" name="bank_dep_seq" value="${bank_Deposit_Search.bank_dep_seq}" />                 <!--통장입금 내역 등록일자-->
            <table class="gridtable">
                <tr>
                    <th>가맹점번호</th>
                    <td class="merch_no" colspan="3">
                        <select name="merch_no_view">
                           <option value="">선택하세요</option>>
                           <c:forEach var ="tb_merch" items="${tb_merchs}" >                                           
                                    <option value=${tb_merch.merch_no} <c:if test="${tb_merch.merch_no == bank_Deposit_Search.view_merch_no}">selected="selected"</c:if>>${tb_merch.mid_nm}</option>
                           </c:forEach>                            
                        </select>
                    </td>
                </tr>
                 <tr>
                    <th>통장번호</th>
                    <td><input type="text" name="account_no_view" id="account_no_view" size="25" value="${bank_Deposit_Search.view_account_no}" readonly="true"/></td>
                    <th>입금일</th>
                    <td><input type="text" name="bankDepositUpdate_deposit_dt" id="bankDepositUpdate_deposit_dt" maxlength="10" value="${bank_Deposit_Search.deposit_dt}"/></td>
                </tr>
                <tr>
                    <th>수집방법</th>
                    <td>
                           ${collect_methods.tb_code_details[0].code_nm}
                    </td>
                     <th>입금금액</th>
                    <td ><input type="text" name="tem_deposit_amt" maxlength="13" onkeyup="javascript:InpuOnlyNumber2(this);"  value="${bank_Deposit_Search.deposit_amt}"/>원</td>                                
                 </tr>
                 <tr>
                    <th>메모</th>
                    <td colspan="3">
                        <textarea name="memo" cols="60">${bank_Deposit_Search.memo}</textarea>
                    </td>
                </tr>                
                <tr>
                    <td colspan="4">
                        <div class="buttonArea3Button">
                            <button name ="update">수정</button>
                            <button name ="delete">삭제</button>
                            <button name ="close">닫기</button>
                        </div>
                    </td>
                </tr>
            </table>
        </form>
    </div>

</div>

<script type="text/javascript">

    $(document).ready(function () {

        var $updateForm = $("#bankDepositUpdateForm");

        var bankDepositInsertCalendar = new dhtmlXCalendarObject(["bankDepositUpdate_deposit_dt"]);

        $updateForm.find("select[name=merch_no_view]").change(function(){

                        var selval = $(this).val();                        
                        if(selval == "")
                        {
                            $("#bankDepositUpdateForm input[name=account_no_view]").val("");
                            $("#bankDepositUpdateForm input[name=pay_mtd]").val("");
                            $("#bankDepositUpdateForm input[name=account_no]").val("");
                            $("#bankDepositUpdateForm input[name=bank_cd]").val("");
                            $("#bankDepositUpdateForm input[name=merch_no]").val("");
                        }
                        else
                        {
                            var arrMerchs = selval.split(',');
                            var len = arrMerchs.length; 

                              //pay_mtd||','||MERCH_NO||','||ACC_NO||','||BANK_CD||bank_cd_nm
                              //0,                  1,        2,            3,         4,5,
                            var viewaccno = "("+ arrMerchs[4] +")" +arrMerchs[2];
                            $("#bankDepositUpdateForm input[name=account_no_view]").val(viewaccno);
                            $("#bankDepositUpdateForm input[name=pay_mtd]").val(arrMerchs[0]);
                            $("#bankDepositUpdateForm input[name=account_no]").val(arrMerchs[2]);
                            $("#bankDepositUpdateForm input[name=bank_cd]").val(arrMerchs[3]);
                            $("#bankDepositUpdateForm input[name=merch_no]").val(arrMerchs[1]);
                        }

        });

        // ################# button click Event :: Start
        // 수정 버튼
        $updateForm.find("button[name=update]").button({
            text:true
        })
        .unbind("click")
        .bind("click", function (e) {
            
                        if($("#bankDepositUpdateForm select[name=merch_no_view]").val() == "")
                        {
                            alert("MID는 필수항목 입니다.");
                            $("#bankDepositUpdateForm select[name=merch_no_view]").focus();
                            return false;
                        }   
                        
                         if($("#bankDepositUpdateForm input[name=tem_deposit_amt]").val() == "")
                        {
                            alert("입금금액은 필수항목 입니다.");
                            $("#bankDepositUpdateForm input[name=tem_deposit_amt]").focus();
                            return false;
                        }   
                        

            if (confirm("통장 입금내역을 수정하시겠습니까?") == true){

                if(isValidDate($("#bankDepositUpdate_deposit_dt").val()) ){
                    $updateForm.find("input[name=deposit_dt]").val($("#bankDepositUpdate_deposit_dt").val().replace(/[^0-9]/g,""));
                    //$updateForm.find("input[name=deposit_amt]").val($updateForm.find("input[name=tem_deposit_amt]").val().replace(/[^0-9]/g,""));
                    $updateForm.find("input[name=deposit_amt]").val($updateForm.find("input[name=tem_deposit_amt]").val());

                    var obj ={}
                    obj.url ="<c:url value="/bankDeposit/bankDepositUpdate" />";
                    obj.successText ="통장입금내역 수정 완료";
                    obj.failText ="통장입금내역 수정 실패";
                    obj.closeWindowObj = bankDepositWindowObject;
                    obj.clearGrid = bankDepsitGrid;
                    obj.searchFunction = doSearchBankDepsit;

                    BankDepositButtonAction($updateForm,obj);

                }else{
                    $("#bankDepositUpdate_deposit_dt").focus();
                    alert("입금일을 확인하세요");
                }

            }else{
                return;
            }

            return false;
        });

        //삭제 버튼
        $updateForm.find("button[name=delete]").button({
            text:true
        })
        .unbind("click")
        .bind("click", function (e) {

            if (confirm("통장 입금내역을 삭제하시겠습니까?") == true){

                var obj ={}
                obj.url ="<c:url value="/bankDeposit/bankDepositDelete" />";
                obj.successText ="통장입금내역 삭제 완료";
                obj.failText ="통장입금내역 삭제 실패";
                obj.closeWindowObj = bankDepositWindowObject;
                obj.clearGrid = bankDepsitGrid;
                obj.searchFunction = doSearchBankDepsit;

                BankDepositButtonAction($updateForm,obj);

            }else{
                return;
            }

            return false;
        });

        //닫기버튼
        $updateForm.find("button[name=close]").button({
            text:true
        })
        .unbind("click")
        .bind("click", function (e) {
            bankDepositWindow.window("bankDepositWindowObject").close();
            return false;
        });
        // ################# button click Event :: End

/*
        $updateForm.find("input[name=tem_deposit_amt]").keyup(function(){

            amtToHan($(this),$updateForm.find("span[name=insert_amt_han]"),Number($(this).attr("maxlength")));

        });
*/
        amtToHan($updateForm.find("input[name=tem_deposit_amt]"),$updateForm.find("span[name=insert_amt_han]"),$updateForm.find("input[name=tem_deposit_amt]").attr("maxlength"));

    });

</script>

<c:if test="${!ajaxRequest}">
    </body>
    </html>
</c:if>

