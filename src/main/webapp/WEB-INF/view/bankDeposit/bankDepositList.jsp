<%--
  ~ Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  ~ Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
  ~ Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
  ~ Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
  ~ Vestibulum commodo. Ut rhoncus gravida arcu.
  --%>

<%--
  Created by IntelliJ IDEA.
  User: hoyeongheo
  Date: 14. 12. 2.
  Time: 오후 1:22
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
    <head>
        <title>통장입금 내역관리</title>
    </head>
    <body>
</c:if>


    <div class="searchForm1row searchFormDiv" id="bankDepositSearch">
        <form id="bankDepositSearchForm" method="POST" onsubmit="return false;" action="<c:url value="/bankDeposit/bankDepositList" />" modelAttribute="BankdepositSearchFromBean">
            <input type="hidden" name="page_no" value="1"/>
            <input type="hidden" name="page_size" value="100"/>
            <input type="hidden" name="from_date" value="">
            <input type="hidden" name="to_date" value=""/>

            <div class="label">검색일자</div>
            <div class="input">
                <input id="bankDeposit_from_date" value = '${bankdepositSearchFromBean.from_date}' onclick="bankDepositListsetSens('bankDeposit_to_date', 'max');"/>~
                <input id="bankDeposit_to_date"  value = '${bankdepositSearchFromBean.to_date}' onclick="bankDepositListsetSens('bankDeposit_from_date', 'min');"/>
            </div>
            <div class ="input"><input type="button" name="week" value="1주"/></div>
            <div class ="input"><input type="button" name="month" value="1달"/></div>

            <div class="label">통장번호</div>
            <div class="input">
                <select name="account_no">
                    <option value="">선택하세요.</option>
                    <c:forEach var="accnoList" items="${accountNoList}" varStatus="status">
                        <option value="${accnoList.acc_no}">${accnoList.acc_no}</option>
                    </c:forEach>
                </select>
            </div>

            <button class="searchButton">검색</button>
            <button class="initButton">초기화</button>
            <button class="excelButton">엑셀</button> 
            <div class="action"> 
                <button class="addButton">추가</button>                
            </div>
        </form>
        <div class="paging">
            <div id="bankDepositPaging" style="width: 50%;"></div>
            <div id="bankDepositrecinfoArea" style="width: 50%;"></div>
        </div>
    </div>



<script type="text/javascript">

    var bankDepsitGrid;
    var bankDepsitLayout;
    var bankDepositWindow;
    var bankDepositWindowObject;
    var bankDepsitCalendar;
    $(document).ready(function () {
        
        bankDepsitCalendar = new dhtmlXCalendarObject(["bankDeposit_from_date","bankDeposit_to_date"]);

        var mheaders = "";

        var $searchForm = $("#bankDepositSearchForm");

        var searchForm =  document.getElementById("bankDepositSearch");

        var tabBarId = tabbar.getActiveTab();

        bankDepositWindow = new dhtmlXWindows();

        bankDepsitLayout = tabbar.cells(tabBarId).attachLayout("2E");

        bankDepsitLayout.cells('a').hideHeader();

        bankDepsitLayout.cells('b').hideHeader();

        bankDepsitLayout.cells('a').setHeight("70");

        bankDepsitLayout.cells('a').attachObject(searchForm);

        bankDepsitGrid = bankDepsitLayout.cells('b').attachGrid();

        bankDepsitGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");

        mheaders += "<center>NO</center>,<center>입금일</center>,<center>은행</center>,<center>통장번호</center>,<center>구분</center>";
        mheaders += ",<center>가맹점번호</center>,<center>입금금액</center>,<center>수집방법</center>,<center>Memo</center>";
        bankDepsitGrid.setHeader(mheaders);
        bankDepsitGrid.setColAlign("center,center,center,center,center,center,right,center,center");
        bankDepsitGrid.setColTypes("txt,txt,txt,txt,txt,txt,edn,txt,txt");
        bankDepsitGrid.setInitWidths("50,130,100,100,130,130,120,100,150");
        bankDepsitGrid.setColSorting("str,str,str,str,str,str,int,str,str");
        bankDepsitGrid.enableColumnMove(true);
        bankDepsitGrid.setSkin("dhx_skyblue");

        bankDepsitGrid.setNumberFormat("0,000",6);

        bankDepsitGrid.enableColSpan(true);

        bankDepsitGrid.init();

        bankDepsitGrid.enablePaging(true,Number($("#bankDepositSearchForm input[name=page_size]").val()),10,"bankDepositPaging",true,"bankDepositrecinfoArea");

        bankDepsitGrid.setPagingSkin("bricks");

        
        $("#bankDepositSearchForm input[name=week]").click(function(){
            bankDeposit_date_search("week");
        });

        $("#bankDepositSearchForm input[name=month]").click(function(){
            bankDeposit_date_search("month");
        });

        

        //페이징 처리
        bankDepsitGrid.attachEvent("onPageChanged", function(ind,fInd,lInd){

            if(lInd !==0){

                $("#bankDepositSearchForm input[name=page_no]").val(ind);

                doSearchBankDepsit();

            }else{
                return false;
            }

        });
        bankDepsitGrid.parse(${bankDepositList}, "json");

        bankDepsitGrid.attachEvent("onRowDblClicked", doUpdateFormBankDepsit);

        //통장 입력 내역 조회
        $searchForm.find(".searchButton").button({
            icons: {
                primary: "ui-icon-search"
            }
        })
        .unbind("click")
        .bind("click", function (e) {
            if(isValidDate($("#bankDeposit_from_date").val()) && isValidDate($("#bankDeposit_to_date").val())){
                $("#bankDepositSearchForm input[name=page_no]").val("1");
                bankDepsitGrid.clearAll();
                doSearchBankDepsit();
            }else{
                alert("날짜 형식을 확인하세요");
            }
            return false;
        });

        $searchForm.find(".action .addButton").button({
            icons: {
                primary: "ui-icon-plusthick"
            },
            text: false
        })
        .unbind("click")
        .bind("click", function (e) {

            var dhtmlxwindow = {};
            dhtmlxwindow.type = "Insert";
            dhtmlxwindow.width = 620;
            dhtmlxwindow.height = 260;

            depositWindowPop(dhtmlxwindow);

            return false;
        });

        //엑셀다운로드
        $searchForm.find(".excelButton").button().unbind("click").bind("click", function (e) {
            
            $("#bankDepositSearchForm").attr("action","<c:url value="/bankDeposit/bankDepositListExcel" />");
            document.getElementById("bankDepositSearchForm").submit();
            
        });
        
        //초기화
        $searchForm.find(".initButton").button().unbind("click").bind("click", function (e) {
            
            bankDepositMasterInit($("#bankDepositSearchForm"));
            
        });        
        

    });
    
    function bankDepositMasterInit($form) {

    searchFormInit($form);

    bankDeposit_date_search("week");                    
    } 


    function depositWindowPop(dhtmlxwindow,obj){

        var dhtmlxwindowSize = {};
        dhtmlxwindowSize.width = dhtmlxwindow.width;
        dhtmlxwindowSize.height = dhtmlxwindow.height;

        var location = dhxwindowCenterAlgin(dhtmlxwindowSize);

        bankDepositWindowObject = bankDepositWindow.createWindow("bankDepositWindowObject", location.x, location.y, dhtmlxwindowSize.width, dhtmlxwindowSize.height);

        bankDepositWindow.window("bankDepositWindowObject").setModal(true);

        switch (dhtmlxwindow.type){
            case "Insert":
                bankDepositWindowObject.setText("통장입금 내역 등록");
                bankDepositWindowObject.attachURL( "<c:url value="/bankDeposit/bankDepositInsert" />", true);
                break;

            case "Update":
                bankDepositWindowObject.setText("통장입금 내역 수정");
                bankDepositWindowObject.attachURL( "<c:url value="/bankDeposit/bankDepositUpdate?bank_dep_seq=" />"+obj.bank_dep_seq, true);
                break;
        }

    }


    function doSearchBankDepsit(){

        $("#bankDepositSearchForm input[name=from_date]").val(($("#bankDeposit_from_date").val()).replace( /[^(0-9)]/g,""));
        $("#bankDepositSearchForm input[name=to_date]").val(($("#bankDeposit_to_date").val()).replace( /[^(0-9)]/g,""));

        $.ajax({
            url : "<c:url value="/bankDeposit/bankDepositList" />",
            type : "POST",
            async : true,
            dataType : "json",
            data: $("#bankDepositSearchForm").serialize(),
            success : function(data) {

                var jsonData = $.parseJSON(data);

                bankDepsitGrid.parse(jsonData,"json");

                bankDepsitGrid.groupBy(1,["","#title","#cspan","","","","#stat_total","","",""]);

            },
            error : function(jqXHR, textStatus, errorThrown) {
                alert("xh = " + errorThrown);

            }
        });

    }

    //통장 입금내역 수정 Form
    function doUpdateFormBankDepsit(rowid, col) {

        var bank_dep_seq = bankDepsitGrid.getUserData(rowid,"bank_dep_seq");

        var updateObject = {};

        updateObject.bank_dep_seq =bank_dep_seq;
        
        var dhtmlxwindow = {};
        dhtmlxwindow.type = "Update";
        dhtmlxwindow.width = 610;
        dhtmlxwindow.height = 260;

        depositWindowPop(dhtmlxwindow,updateObject);

    }
    
    function bankDeposit_date_search(day){
        var oneDate = 1000 * 3600 * 24; 
        var now = new Date();
        var week = new Date(now.getTime() + (oneDate*-6));
        var month = new Date(now.getTime() + (oneDate*-31));
        var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
        var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
        var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

        var date=new Date();
        if(day=="today"){
            this.$("input[id=bankDeposit_from_date]").val(nowTime);
            this.$("input[id=bankDeposit_to_date]").val(nowTime);
        }else if(day=="week"){
            this.$("input[id=bankDeposit_from_date]").val(weekTime);
            this.$("input[id=bankDeposit_to_date]").val(nowTime);
        }else{
            this.$("input[id=bankDeposit_from_date]").val(monthTime);
            this.$("input[id=bankDeposit_to_date]").val(nowTime);
        }
    }
    
      
    function bankDepositListsetSens(id, k) {
        
        // update range
        if (k == "min") {
            bankDepsitCalendar.setSensitiveRange(byId(id).value, null);
        } else {
            bankDepsitCalendar.setSensitiveRange(null, byId(id).value);
        }
    }

    function byId(id) {
        return document.getElementById(id);
    }             

</script>


<c:if test="${!ajaxRequest}">
    </body>
    </html>
</c:if>
