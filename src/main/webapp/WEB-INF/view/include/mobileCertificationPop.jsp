<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
	<div class="m-con" id="m-con">
		<form id="certificationForm" action="/include/mobileCertificationPop">
			<input type="hidden" name="hd_cert_num" id="hd_cert_num" />
			<input type="hidden" name="hd_cert_num_conf" id="hd_cert_num_conf" />
			<input type="hidden" name="hd_conf_cnt" id="hd_conf_cnt" />
			<div class="m-popt">
				<span>+휴대폰 인증</span>
			</div>
			<div class="m-popline"></div>
			<div class="m-dong">
				<span class="m-colorB">*</span>
				<span>
					  휴대폰으로 전송된 인증코드를 입력하십시오.
			    </span>
				<br/>
				<span class="m-colorB">*</span>
				<span>
					  3회이상 오류시 재발급 받으시기 바랍니다.
			    </span>
			</div>
			<div class="mobileinput">
				<div class="mobileinput-1">
					<div class="mobileinput-2">
						<span class="yn">인증코드</span>
						<input type="text" class="add_in4" name="certification" id="certification" />
						<span class="yn1" id="cert_conf">확인</span>
<%-- 						<span class="yn1"><a href="#">확인</a></span> --%>
					</div>
				</div>
			</div>
<!-- 			<div id="stap_bt"> -->
<!-- 				<div class="stap_btnext"><h5>확인</h5></div> -->
<!-- 			</div> -->
		</form>
	</div>
	
<script>
	$(document).ready(function(){
		$("#m-con").dialog({
			width:500
			,title:"회원가입- 휴대폰 인증"
			,autoOpen:false
			,modal:true
			,resizable:false
			,close:function(){
				resetCertification();
			}
		});
		
		$("#com_bt_mobile").click(function() {
			if($("#ip_mobile_number2").val() == "" || $("#ip_mobile_number3").val() == ""){
				alert("휴대전화번호를 입력하십시오.");
				return false;
			}
			if(isNaN($("#ip_mobile_number2").val()) || isNaN($("#ip_mobile_number3").val())){
				alert("휴대전화번호엔 숫자값만 입력하십시오.");
				return false;
			}
			if($.trim($("#dev_mobl_no").val()) == $("#sl_mobile_number1").val() + "-" + $.trim($("#ip_mobile_number2").val()) + "-" + $.trim($("#ip_mobile_number3").val())){
				alert("이미 인증받은 휴대전화번호 입니다.");
				return false;
			}
			
			$("#m-con").dialog("open");
			
			$("#hd_conf_cnt").val(0); 	//최초 팝업 Open시 인증번호 확인 카운트를 '0'으로 초기화
			
// 			alert($("#certificationForm").serialize());
			$.ajax({
				url: $("#certificationForm").attr("action")
				, type :"POST"
				, data   :'sl_mobile_number1=' + $("#sl_mobile_number1").val()
							+ '&ip_mobile_number2=' + $("#ip_mobile_number2").val()
							+ '&ip_mobile_number3=' + $("#ip_mobile_number3").val()
				, dataType: "json"
				, success: function(data) {
							$("#hd_cert_num").val(data);
						   }
			    , error: function(xhr) { 
			    			alert(xhr.status);
			    		 }
			    });
		});
		
		//인증코드 확인
		$("#cert_conf").click(function(){
			
			if($("#certification").val() == ""){
				alert("휴대폰으로 발송된 인증코드를 입력하십시오.");
				$("#certification").focus();
				return false;
			}
			
			$.ajax({
				url: "/include/mobileCertificationConf"
				,type: "POST"
				,data: $("#certificationForm").serialize()
				,dataType: "json"
				,success: function(data) {
							$("#hd_cert_num_conf").val(data);
							certificationChk();
					   	  }
		    	,error: function(xhr) { 
		    				alert(xhr.status);
		    		 	}
		    });
			
		});
		
		function certificationChk(){
			if($("#hd_cert_num_conf").val() == $("#hd_cert_num").val()){
				alert("정상적으로 인증되었습니다.");
				$("#hd_mobile_cert").val(true);
				$("#m-con").dialog("close");
				resetCertification();
				$("#com_bt_mobile").hide();
			}else{
				alert("인증코드가 틀립니다.");
				$("#hd_conf_cnt").val(parseInt($("#hd_conf_cnt").val()) + 1);
				
				if($("#hd_conf_cnt").val() >= 3){
					alert("휴대폰 인증코드가 3회이상 잘못 입력되었습니다.\n재발급 후 다시 입력하십시오.");
					$("#m-con").dialog("close");
					$("#hd_mobile_cert").val("");
					resetCertification();
				}
			}
		}
		
		function resetCertification(){
			$("#hd_conf_cnt").val(0);
			$("#hd_cert_num").val("");	//생성된 난수 초기화
			$("#hd_cert_num_conf").val(""); //입력 확인된 인증코드 초기화
			$("#certification").val("");
		}
	});
</script>
