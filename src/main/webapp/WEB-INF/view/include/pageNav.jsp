<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
		<div class="paging">
			<ul>
				<c:choose>
					<c:when test="${listPagingInfo.page_no ==1}">
						<li><a class="paging_first"><em>맨처음 페이지</em></a></li>
						<li><a class="paging_prev"><em>이전 페이지</em></a></li>
					</c:when>
					<c:otherwise>
						<li><a href="javascript:go_page(frm_paging, '$!listPagingInfo.first_page_no')" class="paging_first"><em>맨처음 페이지</em></a></li>
						<li><a href="javascript:go_page(frm_paging, '$!listPagingInfo.prev_block_last_page_no')" class="paging_prev"><em>이전 페이지</em></a></li>
					</c:otherwise>
				</c:choose>
				
				<c:forEach items="${listPagingInfo.page_nos}" var="testresult" varStatus="status">
						<div>${status.count}</div>
				</c:forEach>

				<c:choose>
					<c:when test="${listPagingInfo.page_no == !listPagingInfo.last_page_no}">
						<li><a class="paging_next"><em>다음 페이지</em></a></li>
						<li><a class="paging_last"><em>맨뒤 페이지</em></a></li>
					</c:when>
					<c:otherwise>
						<li><a href="javascript:go_page(frm_paging, '$!listPagingInfo.next_block_first_page_no')" class="paging_next"><em>다음 페이지</em></a></li>
						<li><a href="javascript:go_page(frm_paging, '$!listPagingInfo.last_page_no')" class="paging_last"><em>맨뒤 페이지</em></a></li>
					</c:otherwise>
				</c:choose>
			</ul>
		</div>