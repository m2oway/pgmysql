<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- topWrapper -->		
<div class="layout-header">
	<div class="header-line"></div>
	<div class="head">
		<div class="head-login-inputname">
			<form action="/auth/login" method="post" id="login_form">
				<ul id="log_input">
					<li class="login-name-input">
						<span class="logun-name">ID</span>
						<input class="logun-input" type="text" alt="ID" id="login_id" name="login_id"/>
					</li>
					<li class="login-name-input password">
						<span class="logun-name">PW</span>
						<input class="logun-input" type="password" alt="password" id="user_pw" name="user_pw"/>
					</li>
					<li class="login-name-input">
						<span class="logun-bt"><a class="login-out" href="#">login</a></span>
					</li>
				</ul>
			</form>
		</div>
		<div class="head-join">
			<ul>
				<li class="head-member">
					<span class="head-member-text joinable" style="display: none;">▶</span>
					<span class="head-member-text1 joinable" style="display: none;"><a class="head-member-link" href="#" id="private-photo-bt">투입가능일자 변경</a></span>
					<span class="head-member-text">▶</span>
					<span class="head-member-text1"><a class="head-member-link" href="<c:url value="/common/memberJoinForm"/>">JOIN MEMBER</a></span>
					<span class="head-member-text">▶</span>
					<span class="head-member-text1"><a class="head-member-link" href="#">ID/PW SEARCH</a></span>
				</li>
			</ul>
		</div>
		<div class="head-logo"><a class="logolink" href="<c:url value="/"/>"><h1>itplayers home</h1></a></div>
		<div class="head-manu">
			<ul id="com-manu_n">
				<li class="maun-text"><a class=" manu-link" href="#">마이메뉴</a>
					<ul class="manu-text">
						<li class="manu-subtext">이력서 등록</li>
						<li class="manu-subtext manu-line">|</li>
						<li class="manu-subtext">내 이력서 관리</li>
						<li class="manu-subtext manu-line">|</li>
						<li class="manu-subtext">추천 프로젝트</li>
						<li class="manu-subtext manu-line">|</li>
						<li class="manu-subtext">진행 프로젝트</li>
						<li class="manu-subtext manu-line">|</li>
						<li class="manu-subtext">회원 정보 관리</li>
					</ul>
				</li>
				<li class="maun-text"><a class=" manu-link" href="#">IT Player검색</a></li>
				<li class="maun-text"><a class=" manu-link" href="#">프로젝트</a></li>
				<li class="maun-text"><a class=" manu-link" href="#">이용안내</a>
					<ul class="manu-text5">
						<li class="manu-subtext">ITPlayers란?</li>
						<li class="manu-subtext manu-line">|</li>
						<li class="manu-subtext">회원가입 절차</li>
						<li class="manu-subtext manu-line">|</li>
						<li class="manu-subtext">이력서 등록 절차</li>
						<li class="manu-subtext manu-line">|</li>
						<li class="manu-subtext">프로젝트 등록 절차</li>
						<li class="manu-subtext manu-line">|</li>
						<li class="manu-subtext">구인/구직 절차</li>
					</ul>
				</li>
				<li class="maun-text"><a class=" manu-link" href="#">회사소개</a>
					<ul class="manu-text5">
						<li class="manu-subtext">FI.Partners 소개</li>
						<li class="manu-subtext manu-line">|</li>
						<li class="manu-subtext">Contact Us</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<%@include file="/WEB-INF/view/popup/resumeInfoIvstmtPop.jsp" %>
</div>
<script>
	$(document).ready(function(){
		
		$("#login_id").keypress(function(event){
			var code = (event.keyCode ? event.keyCode : event.which);
			if(code===13) { // Enter keycode
				$(".logun-bt").click();
		    }
		});
		
		$("#user_pw").keypress(function(event){
			var code = (event.keyCode ? event.keyCode : event.which);
			if(code===13) { // Enter keycode
				$(".logun-bt").click();
		    }
		});		
		
		$.ajax({
			url: "/menu_list"
			,type: "POST"
			,dataType: "json"
			,async : false
			,success: function(data){
					if(data != null){
						menu_list(data);						
					}
				}
			,error: function(xhr){
					alert("error : "+xhr.status);
					
				}
		});			

		$("div.head-manu ul li.maun-text").hover(function(){
			$(this).find("ul").css("display","block");
		},function(){
			$(this).find("ul").css("display","none");
		});
		
		$(".logun-bt").click(function(){
			$("#login_form").submit();
		});

		$.ajax({
			url: "/auth/login_check"
			,type: "POST"
			,dataType: "json"
			,success: function(data){
					if(data.login_check === "success"){
						$("#log_input > li > input").remove();
						$(".login-out").text("logout");
						$("#login_form").attr("action","/auth/logout");
						$("#log_input .login-name-input:eq(0)").text(data.user_name);
						$("#log_input .login-name-input:eq(1)").text(data.login_id);
						if(data.user_rght_id == '1'){
							$("#join_able_date").val(data.join_able_date);
							$(".joinable").show();	
						}
					}
				}
			,error: function(xhr){
					alert("error : "+xhr.status);
					
				}
		});	
		
	});
	
	function menu_list(data){
//		메뉴 겟수 		
// 		for(var i=0;i<(menu.gnb).length;i+=1){
// 			if(last_menu_clss_code1 !== menu.gnb[i].menu_clss_code_name1){
// 				last_menu_clss_code1 = menu.gnb[i].menu_clss_code_name1;
// 				gnb_cnt+=1;
// 			}
// 		}
//		갯수에 따라 css 적용		
// 		switch(gnb_cnt){
// 			case 1:
// 				 gnb_class_code1 = 'tlin1';
// 				 gnb_class_code2=  'tin1';
// 				 gnb_class_code3=  'bn';
// 			break;
// 			case 2:
// 				 gnb_class_code1 = 'tlin2';
// 				 gnb_class_code2=  'tin2';
// 				 gnb_class_code3=  'blin2';
// 			break;
// 			case 3:
// 				 gnb_class_code1 = 'tn';
// 				 gnb_class_code2=  'in';
// 				 gnb_class_code3=  'bn';
// 			break;
// 			case 4:
// 				 gnb_class_code1 = 'tli';
// 				 gnb_class_code2=  't1';
// 				 gnb_class_code3=  'bli';
// 			break;
// 			case 5:
// 				 gnb_class_code1 = 'tlin5';
// 				 gnb_class_code2=  'tin5';
// 				 gnb_class_code3=  'blin5';
// 			break;
// 			case 6:
// 				 gnb_class_code1 = 'tlin6';
// 				 gnb_class_code2=  'tin6';
// 				 gnb_class_code3=  'blin6';
// 			break;
// 			case 7:
// 				 gnb_class_code1 = 'tlin';
// 				 gnb_class_code2=  'tin';
// 				 gnb_class_code3=  'bn';
// 			break;
// 		}		
		
		var last_menu_clss_code1 = '';
		var html = '';
		
// 		for(var i=0;i<data.length;i++){
// 			html +='<li class="text_manu"><a class="manu_link" href="<c:url value="/biz/mymenu/resumeRegister/myResumeManage"/>">마이메뉴</a>
// 			<ul class="manu_a" id="manu_bc">
			
// 			html +='<li><a class="m_text" href="<c:url value="'+ data[i].view_pgm_id +'"/>">'+ data[i].view_name +'</a></li>';
// 			html +='<li class="bar">|</li>';
			
// 			</ul>
// 			</li>
// 		}
		
		for(var i=0;i<data.length;i++){
			if(last_menu_clss_code1 !== data[i].menu_clss_code_name1){
				last_menu_clss_code1 = data[i].menu_clss_code_name1;
				if(i === 0){
					html+=	'<li class="maun-text"><a class="manu-link" href="<c:url value="'+ data[i].view_pgm_id +'"/>">'+ data[i].menu_clss_code_name1 +'</a>';
					html+=	'	<ul class="manu-text">';
					html+=  '		<li class="manu-subtext">';
					html+=  '			<a href="<c:url value="'+ data[i].view_pgm_id +'"/>">'+ data[i].view_name +'</a>';
					html+=  '		</li>';
					html+=	'		<li class="manu-subtext manu-line">|</li>';
				}
				else{
					html+=	'	</ul>';
					html+=	'</li>';
					html+=	'<li class="maun-text"><a class=" manu-link" href="<c:url value="'+ data[i].view_pgm_id +'"/>">'+ data[i].menu_clss_code_name1 +'</a>';
					html+=	'	<ul class="manu-text5">';
					html+=  '		<li class="manu-subtext">';
					html+=  '			<a href="<c:url value="'+ data[i].view_pgm_id +'"/>">'+ data[i].view_name +'</a>';
					html+=  '		</li>';
					html+=	'		<li class="manu-subtext manu-line">|</li>';	
				}
			}
		else{
				if(i === data.length-1){
					html+=  '		<li class="manu-subtext">';
					html+=  '			<a href="<c:url value="'+ data[i].view_pgm_id +'"/>">'+ data[i].view_name +'</a>';
					html+=  '		</li>';
					html+=	'	</ul>';
					html+=	'</li>';
				}else{
					html+=  '<li class="manu-subtext">';
					html+=  '	<a href="<c:url value="'+ data[i].view_pgm_id +'"/>">'+ data[i].view_name +'</a>';
					html+=  '</li>';
					html+=	'<li class="manu-subtext manu-line">|</li>';
				}
			}
		}		
		
		
	 	$("#com-manu_n").html(html);
	 	
	}
	
	//투입가능일자 팝업 AJAX
	$("#private-photo-bt").click(function(){
		$("#ivstmt").dialog("open");
	});


</script>
