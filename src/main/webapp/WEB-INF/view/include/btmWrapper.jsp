<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- btmWrapper -->		
<div class="layout-footer">
	<div class="footer-line"></div>
	<div class="footer-body">
		<div class="footer-manu">
			<span class="footer-manulist">사이트 가이드</span>
			<span class="footer-manulist">이용약관</span>
			<span class="footer-manulist">개인정보 취급방침</span>
			<span class="footer-manulist">고객센터</span>
		</div>
		<div class="footer-logo"></div>
		<div class="footer-address">
			<ul>
				<li class="footer-address-text">서울특별시 영등포구 여의도동 44-32 에리트빌딩 703호 Tel 070-8890-2290 Fex 02-784-0063</li>
				<li class="footer-address-text">tel.070.8890.2290</li>
			</ul>
		</div>
		<div class="clean"></div>
	</div>
</div>
<!-- layer popup 창을 보여줄 DIV -->
<div id="itp_popup"></div>