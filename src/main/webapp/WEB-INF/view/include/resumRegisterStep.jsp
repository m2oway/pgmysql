<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<div class="title-img"></div>
<div class="private-title-bar">
	<ul>
		<li class="private-title-text p-title-text1" url="<c:url value="/biz/mymenu/resumeRegister/registerPersonalInformation?dev_id=8"/>" id="stap1">개인정보 등록</li>
		<li class="private-title-text p-title-text2" url="<c:url value="/biz/mymenu/resumeRegister/registerPersonalHistory"/>">경력사항 등록</li>
		<li class="private-title-text p-title-text3" url="<c:url value="/biz/mymenu/resumeRegister/registerProject"/>">프로젝트 등록</li>
		<li class="private-title-text p-title-text4" url="<c:url value="/biz/mymenu/resumeRegister/registerCertificationAndForeignLanguage"/>">자격증 및 외국어 등록</li>
		<li class="private-title-text p-title-text5" url="<c:url value="/biz/mymenu/resumeRegister/registerHopeBusiness"/>">희망업무 등록</li>
	</ul>
</div>
