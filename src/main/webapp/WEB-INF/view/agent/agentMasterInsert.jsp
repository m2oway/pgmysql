<%-- 
    Document   : agentMasterInsert
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>대리점 등록</title>
        </head>
        <body>
</c:if>
            
            <script type="text/javascript">
                               
                $(document).ready(function () {
                    
                    //대리점 정보 등록 이벤트
                    $("#agentFormInsert input[name=insert]").click(function(){
                       doInsertAgentMaster(); 
                    });              
                    
                    //대리점 정보 등록 닫기
                    $("#agentFormInsert input[name=close]").click(function(){
                        agentMasterWindow.window("agentMasterWindow").close();
                    });                            
                    
                });

                //대리점 정보 등록
                function doInsertAgentMaster(){
                    
                    if($("#agentFormInsert input[name=comp_seq]").val() == "")
                    {
                        alert("사업자를 선택 하십시오.");
                        return;
                    }
                    
                    if($("#agentFormInsert input[name=agent_nm]").val() == "")
                    {
                        alert("대리점명은 필수사항 입니다.");
                        $("#agentFormInsert input[name=agent_nm]").focus();
                        return;
                    }
                    
                    if($("#agentFormInsert input[name=tel_1]").val() == "")
                    {
                        alert("연락처1은 필수사항 입니다.");
                        $("#agentFormInsert input[name=tel_1]").focus();
                        return;
                    }
                    /*
                    if($("#agentFormInsert input[name=zip_cd]").val() == "")
                    {
                        alert("우편번호는 필수사항 입니다.");
                        $("#agentFormInsert input[name=zip_cd]").focus();
                        return;
                    }
                    */
                    
                    if($("#agentFormInsert input[name=addr_1]").val() == "")
                    {
                        alert("주소는 필수사항 입니다.");
                        $("#agentFormInsert input[name=addr_1]").focus();
                        return;
                    }
                    
                    if($("#agentFormInsert input[name=addr_2]").val() == "")
                    {
                        alert("상세주소는 필수사항 입니다.");
                        $("#agentFormInsert input[name=addr_2]").focus();
                        return;
                    }
                    
                    if($("#agentFormInsert select[name=bank_cd]").val() === "")
                    {
                        alert("은행은 필수사항 입니다.");
                        $("#agentFormInsert input[name=bank_cd]").focus();
                        return;
                    }
                    
                    if($("#agentFormInsert input[name=acc_no]").val() == "")
                    {
                        alert("계좌번호는 필수사항 입니다.");
                        $("#agentFormInsert input[name=acc_no]").focus();
                        return;
                    }
                    
                    if($("#agentFormInsert input[name=acc_ownner]").val() == "")
                    {
                        alert("예금주는 필수사항 입니다.");
                        $("#agentFormInsert input[name=acc_ownner]").focus();
                        return;
                    }
                    
                    
                    if(!window.confirm("등록하시겠습니까?")){
                        return;
                    }                    

                    $.ajax({
                        url : $("#agentFormInsert").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#agentFormInsert").serialize(),
                        success : function(data) {
                            alert("등록 완료");
                            agentgrid.clearAll();
                            agentSearch();
                            agentMasterWindow.window("agentMasterWindow").close();
                        },
                        error : function() { 
                                alert("등록 실패");
                        }
                    });	
                    
                }           
                
                //사업자 정보 검색 팝업
                $("#agentFormInsert input[name=comp_search]").click(function () {
                    
                    CompanyAgentSelectWin_agentMasterIns();
                    
                });                
                
                var dhxSelPopupWins;
		dhxSelPopupWins = new dhtmlXWindows();
                function CompanyAgentSelectWin_agentMasterIns()
                {
                    onoffObject.comp_nm = $("#agentFormInsert #comp_nm");
                    onoffObject.comp_seq = $("#agentFormInsert #comp_seq");
                    onoffObject.biz_no = $("#agentFormInsert #biz_no");
                    onoffObject.comp_cate_nm = $("#agentFormInsert #comp_cate_nm");
                    onoffObject.corp_no = $("#agentFormInsert #corp_no");
                    onoffObject.comp_ceo_nm = $("#agentFormInsert #comp_ceo_nm");
                    onoffObject.comp_tel1 = $("#agentFormInsert #comp_tel1");
                    onoffObject.comp_tel2 = $("#agentFormInsert #comp_tel2");
                    onoffObject.zip_cd = $("#agentFormInsert #zip_cd");
                    onoffObject.addr = $("#agentFormInsert #addr");
                    onoffObject.addr_1 = $("#agentFormInsert #addr_1");
                    onoffObject.addr_2 = $("#agentFormInsert #addr_2");
                    onoffObject.biz_cate = $("#agentFormInsert #biz_cate");
                    onoffObject.biz_type = $("#agentFormInsert #biz_type");
                    onoffObject.tax_flag = $("#agentFormInsert #tax_flag");
                    
                    w2 = dhxSelPopupWins.createWindow("companySelPopupList", 20, 30, 640, 480);
                    w2.setText("사업자번호 선택페이지");
                    dhxSelPopupWins.window('companySelPopupList').setModal(true);
                    w2.attachURL('<c:url value = "/popup/CompanySelectPopUp"/>', true);
                }
                
                
//                function SelCompaynyInfoInput(p_comp_seq,p_comp_nm,p_comp_cate_nm,p_biz_no,p_corp_no,p_comp_ceo_nm,p_comp_tel1,p_comp_tel2,p_zip_cd,p_addr_1,p_addr_2,p_biz_type,p_biz_cate,p_tax_flag){
//                    
//                    $("#agentFormInsert #comp_nm").text(p_comp_nm);
//                    $("#agentFormInsert #comp_seq").val(p_comp_seq);
//                    $("#agentFormInsert #biz_no").text(p_biz_no);
//                    $("#agentFormInsert #comp_cate_nm").text(p_comp_cate_nm);
//                    $("#agentFormInsert #corp_no").text(p_corp_no);
//                    $("#agentFormInsert #comp_ceo_nm").text(p_comp_ceo_nm);
//                    $("#agentFormInsert #comp_tel1").text(p_comp_tel1);
//                    $("#agentFormInsert #comp_tel2").text(p_comp_tel2);
//                    $("#agentFormInsert #zip_cd").text(p_zip_cd);
//                    $("#agentFormInsert #addr").text(p_addr_1 + " " + p_addr_2);
//                    $("#agentFormInsert #biz_cate").text(p_biz_cate);
//                    $("#agentFormInsert #biz_type").text(p_biz_type);
//                    $("#agentFormInsert #tax_flag").text(p_tax_flag);
//                    
//                    
//                }                

            </script>
        
            <form id="agentFormInsert" method="POST" action="<c:url value="/agent/agentMasterInsert" />" modelAttribute="agentFormBean">
                <table class="gridtable" height="50%" width="100%">                    
                    <tr>
                        <td align='left' colspan='4'>사업자 정보  <input type="button" name="comp_search" value="검색"/></td>                        
                    </tr>
                    <tr>
                        <td class="headcol" width="20%">사업자명</td>
                        <td colspan="3" width="80%">
                            <span id="comp_nm" />
                        </td>
                    </tr>                    
                    <tr>
                        <td class="headcol" width="20%">사업자번호</td>
                        <td  width="30%">
                            <input type="hidden" name="comp_seq" id="comp_seq" />
                            <span id="biz_no" />
                        </td>
                        <td class="headcol" width="20%">법인번호</td>
                        <td  width="30%">
                            <span id="corp_no" />
                        </td>                        
                    </tr>
                    <tr>
                        <td class="headcol">개인/법인</td>
                        <td>
                            <span id="comp_cate_nm" />
                        </td>                            
                        <td class="headcol">과세구분</td>
                        <td>
                            <span id="tax_flag" />
                        </td>       
                    </tr>
                    <tr>
                        <td class="headcol">업태</td>
                        <td>
                            <span id="biz_type" />
                        </td>                            
                        <td class="headcol">업종</td>
                        <td>
                            <span id="biz_cate" />
                        </td>       
                    </tr>         
                    <tr>
                        <td class="headcol">대표자명</td>
                        <td colspan="3">
                            <span id="comp_ceo_nm" />
                        </td>       
                    </tr>                       
                    <tr>
                        <td class="headcol">대표번호</td>
                        <td>
                            <span id="comp_tel1" />
                        </td>       
                        <td class="headcol">FAX</td>
                        <td>
                            <span id="comp_tel2" />
                        </td>       
                    </tr>
                    <tr>
                        <td class="headcol">우편번호</td>
                        <td>
                            <span id="zip_cd" />
                        </td>       
                        <td class="headcol">주소</td>
                        <td>
                            <span id="addr" />
                        </td>            
                    </tr>   
                </table>
                
                <br/>
                
                <table class="gridtable" height="50%" width="100%">      
                    <tr>
                        <td align='left' colspan='4'>대리점 정보</td>                        
                    </tr>                    
                    <tr>
                        <td class="headcol" width="25%">대리점명</td>
                        <td colspan="3"><input name="agent_nm" value="" maxlength="100" /></td>
                    </tr>       
                    <tr>
                        <td class="headcol">연락처1</td>
                        <td><input name="tel_1" value="" maxlength="20" onkeyPress="javascript:InpuOnlyNumber(this);" /></td>
                        <td class="headcol">연락처2</td>
                        <td><input name="tel_2" value="" maxlength="20" onkeyPress="javascript:InpuOnlyNumber(this);" /></td>
                    </tr>                    
                    <tr>
                        <td class="headcol">우편번호</td>
                        <td><input name="zip_cd" value="" maxlength="10" onkeyPress="javascript:InpuOnlyNumber(this);" /></td>
                        <td class="headcol">주소</td>
                        <td>
                            <input name="addr_1" value="" maxlength="50" />
                            <input name="addr_2" value="" maxlength="50" />
                        </td>
                    </tr>        
                    <tr>
                        <td class="headcol">은행</td>
                        <td>
                            <select name="bank_cd" >
                                <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${bankCdList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td class="headcol">계좌번호</td>
                        <td><input name="acc_no" value="" maxlength="40" onkeyPress="javascript:InpuOnlyNumber(this);" /></td>
                    </tr>        
                    <tr>
                        <td class="headcol">예금주</td>
                        <td colspan="3"><input name="acc_ownner" value="" maxlength="50" /></td>
                    </tr>                            
                    <tr>
                        <td class="headcol">메모</td>
                        <td colspan="3"><textarea name="memo" cols="70" rows="5"></textarea></td>                        
                    </tr>                              
                    <tr>
                        <td colspan="4" align="center"><input type="button" name="insert" value="등록"/>&nbsp;<input type="button" name="close" value="닫기"/></td>
                    </tr>
                </table>
            </form>        
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

