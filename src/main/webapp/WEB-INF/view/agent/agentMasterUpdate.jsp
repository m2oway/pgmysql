<%-- 
    Document   : agentMasterUpdate
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>대리점 수정</title>
        </head>
        <body>
</c:if>
            
            <script type="text/javascript">
                               
                $(document).ready(function () {
                    
                    //대리점 정보 수정 이벤트
                    $("#agentFormUpdate input[name=update]").click(function(){
                       doUpdateAgentMaster(); 
                    });              
                    
                    //대리점 정보 삭제 이벤트
                    $("#agentFormUpdate input[name=delete]").click(function(){
                       doDeleteAgentMaster(); 
                    });                                  
                    
                    //대리점 정보 수정 닫기
                    $("#agentFormUpdate input[name=close]").click(function(){
                        agentMasterWindow.window("agentMasterWindow").close();
                    });                            
                    
                });

                //대리점 정보 수정
                function doUpdateAgentMaster(){
                    
                    if($("#agentFormUpdate input[name=comp_seq]").val() == "")
                    {
                        alert("사업자를 선택 하십시오.");
                        return;
                    }
                    
                    if($("#agentFormUpdate input[name=agent_nm]").val() == "")
                    {
                        alert("대리점명은 필수사항 입니다.");
                        $("#agentFormUpdate input[name=agent_nm]").focus();
                        return;
                    }
                    
                    if($("#agentFormUpdate input[name=tel_1]").val() == "")
                    {
                        alert("연락처1은 필수사항 입니다.");
                        $("#agentFormUpdate input[name=tel_1]").focus();
                        return;
                    }
                    /*
                    if($("#agentFormUpdate input[name=zip_cd]").val() == "")
                    {
                        alert("우편번호는 필수사항 입니다.");
                        $("#agentFormUpdate input[name=zip_cd]").focus();
                        return;
                    }
                    */
                    
                    if($("#agentFormUpdate input[name=addr_1]").val() == "")
                    {
                        alert("주소는 필수사항 입니다.");
                        $("#agentFormUpdate input[name=addr_1]").focus();
                        return;
                    }
                    
                    if($("#agentFormUpdate input[name=addr_2]").val() == "")
                    {
                        alert("상세주소는 필수사항 입니다.");
                        $("#agentFormUpdate input[name=addr_2]").focus();
                        return;
                    }
                    
                    if($("#agentFormUpdate select[name=bank_cd]").val() === "")
                    {
                        alert("은행은 필수사항 입니다.");
                        $("#agentFormUpdate input[name=bank_cd]").focus();
                        return;
                    }
                    
                    if($("#agentFormUpdate input[name=acc_no]").val() == "")
                    {
                        alert("계좌번호는 필수사항 입니다.");
                        $("#agentFormUpdate input[name=acc_no]").focus();
                        return;
                    }
                    
                    if($("#agentFormUpdate input[name=acc_ownner]").val() == "")
                    {
                        alert("예금주는 필수사항 입니다.");
                        $("#agentFormUpdate input[name=acc_ownner]").focus();
                        return;
                    }
                    
                    
                    if(!window.confirm("수정하시겠습니까?")){
                        return;
                    }                    

                    $.ajax({
                        url : $("#agentFormUpdate").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#agentFormUpdate").serialize(),
                        success : function(data) {
                            alert("등록 완료");
                            agentgrid.clearAll();
                            agentSearch();
                            agentMasterWindow.window("agentMasterWindow").close();
                        },
                        error : function() { 
                                alert("수정 실패");
                        }
                    });	
                    
                }            
                
                //대리점 정보 삭제
                function doDeleteAgentMaster(){

                    if(!window.confirm("삭제하시겠습니까?")){
                        return;
                    }                    

                    $.ajax({
                        url : "<c:url value="/agent/agentMasterDelete" />",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#agentFormUpdate").serialize(),
                        success : function(data) {
                            alert("삭제 완료");
                            agentgrid.clearAll();
                            agentSearch();
                            agentMasterWindow.window("agentMasterWindow").close();
                        },
                        error : function() { 
                                alert("삭제 실패");
                        }
                    });	
                    
                }              
                
                //사업자 정보 검색 팝업
                $("#agentFormUpdate input[name=comp_search]").click(function () {
                    
                    CompanydSelectWin_agentMasterMod();
                    
                });                
                
                var dhxSelPopupWins;
		dhxSelPopupWins = new dhtmlXWindows();
                function CompanydSelectWin_agentMasterMod()
                {
                    onoffObject.comp_nm = $("#agentFormUpdate #comp_nm");
                    onoffObject.comp_seq = $("#agentFormUpdate #comp_seq");
                    onoffObject.biz_no = $("#agentFormUpdate #biz_no");
                    onoffObject.comp_cate_nm = $("#agentFormUpdate #comp_cate_nm");
                    onoffObject.corp_no = $("#agentFormUpdate #corp_no");
                    onoffObject.comp_ceo_nm = $("#agentFormUpdate #comp_ceo_nm");
                    onoffObject.comp_tel1 = $("#agentFormUpdate #comp_tel1");
                    onoffObject.comp_tel2 = $("#agentFormUpdate #comp_tel2");
                    onoffObject.zip_cd = $("#agentFormUpdate #zip_cd");
                    onoffObject.addr = $("#agentFormUpdate #addr");
                    onoffObject.addr_1 = $("#agentFormUpdate #addr_1");
                    onoffObject.addr_2 = $("#agentFormUpdate #addr_2");
                    onoffObject.biz_cate = $("#agentFormUpdate #biz_cate");
                    onoffObject.biz_type = $("#agentFormUpdate #biz_type");
                    onoffObject.tax_flag = $("#agentFormUpdate #tax_flag");
                    
                    w2 = dhxSelPopupWins.createWindow("companySelPopupList", 20, 30, 640, 480);
                    w2.setText("사업자번호 선택페이지");
                    dhxSelPopupWins.window('companySelPopupList').setModal(true);
                    w2.attachURL('<c:url value = "/popup/CompanySelectPopUp"/>', true);
                }
                
                //function SelCompaynyInfoInput(p_comp_seq,p_comp_nm,p_biz_no,p_corp_no,p_comp_ceo_nm,p_comp_tel1,p_comp_tel2,p_zip_cd,p_addr_1,p_addr_2,p_comp_cate_nm){
                /*
                function SelCompaynyInfoInput(p_comp_seq,p_comp_nm,p_comp_cate_nm,p_biz_no,p_corp_no,p_comp_ceo_nm,p_comp_tel1,p_comp_tel2,p_zip_cd,p_addr_1,p_addr_2,p_biz_type,p_biz_cate,p_tax_flag){
                    
                    $("#agentFormUpdate #comp_nm").text(p_comp_nm);
                    $("#agentFormUpdate #comp_seq").val(p_comp_seq);
                    $("#agentFormUpdate #biz_no").text(p_biz_no);
                    $("#agentFormUpdate #comp_cate_nm").text(p_comp_cate_nm);
                    $("#agentFormUpdate #corp_no").text(p_corp_no);
                    $("#agentFormUpdate #comp_ceo_nm").text(p_comp_ceo_nm);
                    $("#agentFormUpdate #comp_tel1").text(p_comp_tel1);
                    $("#agentFormUpdate #comp_tel2").text(p_comp_tel2);
                    $("#agentFormUpdate #zip_cd").text(p_zip_cd);
                    $("#agentFormUpdate #addr").text(p_addr_1 + " " + p_addr_2);
                    $("#agentFormUpdate #biz_cate").text(p_biz_cate);
                    $("#agentFormUpdate #biz_type").text(p_biz_type);
                    $("#agentFormUpdate #tax_flag").text(p_tax_flag);
                    
                } 
                */

            </script>
        
            <form id="agentFormUpdate" method="POST" action="<c:url value="/agent/agentMasterUpdate" />" modelAttribute="agentFormBean">
                <table class="gridtable" height="50%" width="100%">                    
                    <tr>
                        <td align='left' colspan='4'>사업자 정보  <input type="button" name="comp_search" value="검색"/></td>                        
                    </tr>
                    <tr>
                        <td class="headcol" width="20%">사업자명</td>
                        <td colspan="3" width="80%">
                            <span id="comp_nm">${ls_companyMasterList[0].comp_nm}</span>
                        </td>
                    </tr>                    
                    <tr>
                        <td class="headcol" width="20%">사업자번호</td>
                        <td  width="30%">
                            <input type="hidden" name="comp_seq" id="comp_seq" value="${ls_companyMasterList[0].comp_seq}" />
                            <span id="biz_no">${ls_companyMasterList[0].biz_no}</span>
                        </td>
                        <td class="headcol" width="20%">법인번호</td>
                        <td  width="30%">
                            <span id="corp_no">${ls_companyMasterList[0].corp_no}</span>
                        </td>                        
                    </tr>
                    <tr>
                        <td class="headcol">개인/법인</td>
                        <td>
                            <span id="comp_cate_nm">${ls_companyMasterList[0].cate_comp_nm}</span>
                        </td>                            
                        <td class="headcol">과세구분</td>
                        <td>
                            <span id="tax_flag" >${ls_companyMasterList[0].tax_flag_nm}</span>
                        </td>       
                    </tr>
                    <tr>
                        <td class="headcol">업태</td>
                        <td>
                            <span id="biz_type" >${ls_companyMasterList[0].biz_type}</span>
                        </td>                            
                        <td class="headcol">업종</td>
                        <td>
                            <span id="biz_cate" >${ls_companyMasterList[0].biz_cate}</span>
                        </td>       
                    </tr>         
                    <tr>
                        <td class="headcol">대표자명</td>
                        <td colspan="3">
                            <span id="comp_ceo_nm">${ls_companyMasterList[0].comp_ceo_nm}</span>
                        </td>       
                    </tr>                                           
                    <tr>
                        <td class="headcol">연락처1</td>
                        <td>
                            <span id="comp_tel1">${ls_companyMasterList[0].comp_tel1}</span>
                        </td>       
                        <td class="headcol">연락처2</td>
                        <td>
                            <span id="comp_tel2">${ls_companyMasterList[0].comp_tel2}</span>
                        </td>       
                    </tr>
                    <tr>
                        <td class="headcol">우편번호</td>
                        <td>
                            <span id="zip_cd">${ls_companyMasterList[0].zip_cd}</span>
                        </td>       
                        <td class="headcol">주소</td>
                        <td>
                            <span id="addr">${ls_companyMasterList[0].addr_1} ${ls_companyMasterList[0].addr_2}</span>
                        </td>            
                    </tr>   
                </table>
                
                <br/>
                
                <table class="gridtable" height="50%" width="100%">      
                    <tr>
                        <td align='left' colspan='4'>대리점 정보</td>                        
                    </tr>                    
                    <tr>
                        <td class="headcol" width="25%">대리점명</td>
                        <td colspan="3">
                            <input type="hidden" name="agent_seq" value="${ls_agentMasterList[0].agent_seq}" />
                            <input name="agent_nm" value="${ls_agentMasterList[0].agent_nm}" maxlength="100" />
                        </td>
                    </tr>       
                    <tr>
                        <td class="headcol">연락처1</td>
                        <td><input name="tel_1" value="${ls_agentMasterList[0].tel_1}" maxlength="20" onkeyPress="javascript:InpuOnlyNumber(this);" /></td>
                        <td class="headcol">연락처2</td>
                        <td><input name="tel_2" value="${ls_agentMasterList[0].tel_2}" maxlength="20" onkeyPress="javascript:InpuOnlyNumber(this);" /></td>
                    </tr>                    
                    <tr>
                        <td class="headcol">우편번호</td>
                        <td><input name="zip_cd" value="${ls_agentMasterList[0].zip_cd}" maxlength="10" onkeyPress="javascript:InpuOnlyNumber(this);" /></td>
                        <td class="headcol">주소</td>
                        <td>
                            <input name="addr_1" value="${ls_agentMasterList[0].addr_1}" maxlength="50" />
                            <input name="addr_2" value="${ls_agentMasterList[0].addr_1}" maxlength="50" />
                        </td>
                    </tr>        
                    <tr>
                        <td class="headcol">은행</td>
                        <td>
                            <select name="bank_cd" >
                                <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${bankCdList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == ls_agentMasterList[0].bank_cd}">selected</c:if> >${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td class="headcol">계좌번호</td>
                        <td><input name="acc_no" value="${ls_agentMasterList[0].acc_no}" maxlength="40" onkeyPress="javascript:InpuOnlyNumber(this);" /></td>
                    </tr>        
                    <tr>
                        <td class="headcol">예금주</td>
                        <td colspan="3"><input name="acc_ownner" value="${ls_agentMasterList[0].acc_ownner}" maxlength="50" /></td>
                    </tr>                            
                    <tr>
                        <td class="headcol">메모</td>
                        <td colspan="3"><textarea name="memo" cols="70" rows="5">${ls_agentMasterList[0].memo}</textarea></td>                        
                    </tr>                              
                    <tr>
                        <td colspan="4" align="center">
                            <input type="button" name="update" value="수정"/>&nbsp;
                            <input type="button" name="delete" value="삭제"/>&nbsp;
                            <input type="button" name="close" value="닫기"/>
                        </td>
                    </tr>
                </table>
            </form>        
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

