<%-- 
    Document   : agentMasterList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>대리점 조회</title>             
        </head>
        <body>
</c:if>
       
    <body>
        <div class="right" id="agent_search" style="width:100%;">       
            <form id="agentForm" method="POST" onsubmit="return false" action="<c:url value="/agent/agentMasterList" />" modelAttribute="agentFormBean"> 
                <input type="hidden" name="page_size" value="100" >
                <input type="hidden" name="page_no" value="1" >
                <table>
                    <tr>
                        <div class="label">대리점</div>
                        <div class="input">
                            <!--<input type="hidden" name="agent_seq" value="${agentFormBean.agent_seq}" >-->
                            <input type="text" name="agent_nm" value ="${agentFormBean.agent_nm}" />
                        </div>                        
                        <div class="label">사업자번호</div>
                        <div class="input">
                            <input type="text" name="biz_no" value ="${agentFormBean.biz_no}" />
                        </div>                        
                        <div class="label">법인번호</div>
                        <div class="input">
                            <input type="text" name="corp_no" value ="${agentFormBean.corp_no}" />
                        </div>                                                
                        
                        <!--<input type="button" name="AgentPopup_Button" value="대리점팝업" onclick="javascript:xxxx();"/>-->
                        <td><input type="button" name="agent_search" value="조회"/></td>
                        <td><input type="button" name="mcode_search" value="엑셀"/></td>
                        <td><input type="button" name="init" value="검색조건지우기"/></td>
                        <div class="action">
                            <button class="addButton">추가</button>
                        </div>                        
                    </tr>
               </table>
            </form>
            <div class="paging">
                <div id="agentPaging" style="width: 50%;"></div>
                <div id="agentrecinfoArea" style="width: 50%;"></div>
            </div>                        
        </div>
        
         <script type="text/javascript">
            
            var agentgrid={};
            var agentCalendar;
            var agentMasterWindow;
            
            $(document).ready(function () {
                
                agentMasterWindow = new dhtmlXWindows();
                agentMasterWindow.attachViewportTo("agent_search");
                
                agentCalendar = new dhtmlXCalendarObject(["agentForm_from_date","agentForm_to_date"]);    
                
                var agent_searchForm = document.getElementById("agent_search");
    
                var tabBarId = tabbar.getActiveTab();
                main_agent_layout = tabbar.cells(tabBarId).attachLayout("1C");
                main_agent_layout.cells('a').hideHeader();
                
                agent_layout = main_agent_layout.cells('a').attachLayout("2E");
                agent_layout.cells('a').hideHeader();
                agent_layout.cells('b').hideHeader();
                 
                agent_layout.cells('a').attachObject(agent_searchForm);
                agent_layout.cells('a').setHeight(55);
                
                agentgrid = agent_layout.cells('b').attachGrid();
                
                agentgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                
                var agentheaders = "";
                agentheaders += "<center>NO</center>,<center>대리점</center>,<center>사업자번호</center>,<center>법인번호</center>,<center>대표자</center>,<center>연락처1</center>,<center>연락처2</center>,<center>상태</center>,<center>등록일</center>";
                agentgrid.setHeader(agentheaders);
                agentgrid.setColAlign("center,center,center,center,center,center,center,center,center");
                agentgrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,txt,txt");
                agentgrid.setInitWidths("50,100,100,100,100,100,100,100,100");
                agentgrid.setColSorting("str,str,str,str,str,str,str,str,str");
                agentgrid.enableColumnMove(true);
                agentgrid.setSkin("dhx_skyblue");

                agentgrid.enablePaging(true,Number($("#agentForm input[name=page_size]").val()),10,"agentPaging",true,"agentrecinfoArea");
                agentgrid.setPagingSkin("bricks");    
                
                agentgrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                    if(lInd !==0){
                      $("#agentForm input[name=page_no]").val(ind);             
                       agentgrid.clearAll();
                       agentSearch();
                    }else{
                        $("#agentForm input[name=page_no]").val(1);
                        agentgrid.clearAll();
                         agentSearch();
                    }
                });                    

                agentgrid.init();
                agentgrid.parse(${ls_agentMasterList},"json");

                agentgrid.attachEvent("onRowDblClicked", doUpdateFormAgentMaster);

                //대리점 정보 검색 이벤트                
                $("#agentForm input[name=agent_search]").click(function () {
                    /*
                    $("input[name=page_no]").val("1");
                    agentSearch();
                    */
                    agentgrid.changePage(1);
                    return false;
                    
                });
                
                $("#agentForm .addButton").button({
                    icons: {
                        primary: "ui-icon-plusthick"
                    },
                    text: false
                })
                .unbind("click")
                .bind("click",function(e){
                    //대리점 정보 등록 이벤트
                    doInsertFormAgentMaster();
                    return false;
                });                
                  
            });

            //대리점 정보 조회
            function agentSearch() {
                
                $.ajax({
                    url: $("#agentForm").attr("action"),
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#agentForm").serialize(),
                    success: function (data) {
                        //agentgrid.clearAll();
                        agentgrid.parse($.parseJSON(data), "json");

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }           
            
            //대리점 정보 등록
            function doInsertFormAgentMaster(){

                agentMasterWindowPopup = agentMasterWindow.createWindow("agentMasterWindow", 150, 150, 850, 660);
                agentMasterWindowPopup.setText("대리점 등록");
                agentMasterWindow.window('agentMasterWindow').setModal(true);
                agentMasterWindowPopup.attachURL("<c:url value="/agent/agentMasterInsert" />",true);   

            }            
            
            //대리점 정보 수정
            function doUpdateFormAgentMaster(rowid, col){
                agentMasterWindowPopup = agentMasterWindow.createWindow("agentMasterWindow", 150, 150, 850, 660);
                agentMasterWindowPopup.setText("대리점 수정");
                agentMasterWindow.window('agentMasterWindow').setModal(true);
                var comp_seq = agentgrid.getUserData(rowid, 'comp_seq');
                agentMasterWindowPopup.attachURL("<c:url value="/agent/agentMasterUpdate" />" + "?agent_seq=" + rowid + "&comp_seq=" + comp_seq,true);   

            }      
            /*
            $("#agentForm input[name=agent_nm]").unbind("click").bind("click", function (){
                agentSelectPopup();
            });
            */
           
            //검색조건 초기화
            $("#agentForm input[name=init]").click(function () {

                AgentMasterInit($("#agentForm"));

            });      
                
                var dhxSelPopupWins=new dhtmlXWindows();
                dhxSelPopupWins.attachViewportTo("b");
                
                //대리점 팝업
                /*
                function agentSelectPopup(){
                    w2 = dhxSelPopupWins.createWindow("AgentSelectPopUp", 20, 30, 640, 480);
                    w2.setText("대리점선택페이지 선택페이지");
                    dhxSelPopupWins.window('AgentSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('AgentSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/AgentSelectPopUp"/>', true);
                } 
                
                function SelAgentInfoInput(p_agent_seq,p_agent_nm){
                    $("#agentForm input[name=agent_seq]").val(p_agent_seq);      
                    $("#agentForm input[name=agent_nm]").val(p_agent_nm);    
                }
                */
                
                function AgentMasterInit($form) {

                    searchFormInit($form);
                } 
    
                
        </script>
    </body>
</html>
