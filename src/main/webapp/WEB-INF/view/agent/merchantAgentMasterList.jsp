<%-- 
    Document   : agentagentonffmerchinfoMasterList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>대리점 가맹점 관리</title>
        </head>
        <body>
</c:if>
        <script type="text/javascript">
                var agentagentonffmerchinfoMasterGrid={};
                var agentagentonffmerchinfoMasterWindow;
                var agentagentonffmerchinfoMasterLayout;
                
                $(document).ready(function () {
                    var agentonffmerchinfoSearch = document.getElementById("agentonffmerchinfoSearch");            
                    agentagentonffmerchinfoMasterLayout = tabbar.cells("2-2").attachLayout("2E");
                    
                    agentagentonffmerchinfoMasterLayout.cells('a').hideHeader();
                    agentagentonffmerchinfoMasterLayout.cells('b').hideHeader();
                    agentagentonffmerchinfoMasterLayout.cells('a').attachObject(agentonffmerchinfoSearch);
                    agentagentonffmerchinfoMasterLayout.cells('a').setHeight(75);
                    agentagentonffmerchinfoMasterGrid = agentagentonffmerchinfoMasterLayout.cells('b').attachGrid();
                    agentagentonffmerchinfoMasterGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var agentagentonffmerchinfoMasterGridheaders = "";
                    //NO,가맹점번호, 가맹점명, 사업자명,개인법인구분,사업자번호, 법인번호, 대표자, 연락처1, 결제한도, 대리점, 상태
                    agentagentonffmerchinfoMasterGridheaders += "<center>NO</center>,<center>대리점</center>,<center>대리점수수료</center>,<center>지급ID번호</center>,<center>지급ID명</center>,<center>사업자명</center>";
                    agentagentonffmerchinfoMasterGridheaders += ",<center>개인/법인</center>,<center>사업자번호</center>,<center>법인번호</center>,<center>대표자</center>,<center>연락처1</center>,<center>상태</center>";
                    agentagentonffmerchinfoMasterGrid.setHeader(agentagentonffmerchinfoMasterGridheaders);
                    agentagentonffmerchinfoMasterGrid.setColAlign("center,center,center,center,center,center,center,center,center,center,center,center");
                    agentagentonffmerchinfoMasterGrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt");
                    agentagentonffmerchinfoMasterGrid.setInitWidths("50,150,90,150,150,150,80,120,100,100,100,80");
                    agentagentonffmerchinfoMasterGrid.setColSorting("str,str,str,str,str,str,str,str,str,str,str,str");
                    agentagentonffmerchinfoMasterGrid.enableColumnMove(true);
                    agentagentonffmerchinfoMasterGrid.setSkin("dhx_skyblue");
    
                    //agentagentonffmerchinfoMasterGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxToolbar/" />");
                    agentagentonffmerchinfoMasterGrid.enablePaging(true,Number($("#agentonffmerchinfoForm input[name=page_size]").val()),10,"agentonffmerchinfoMasterListPaging",true,"agentonffmerchinfoMasterListrecinfoArea");
                    agentagentonffmerchinfoMasterGrid.setPagingSkin("bricks");

                    //페이징 처리
                    agentagentonffmerchinfoMasterGrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        if(lInd !==0){
                          $("#agentonffmerchinfoForm input[name=page_no]").val(ind);             
                           agentagentonffmerchinfoMasterGrid.clearAll();
                           doSearchAgentMerchantMaster();
                        }else{
                            $("#agentonffmerchinfoForm input[name=page_no]").val(1);
                            agentagentonffmerchinfoMasterGrid.clearAll();
                             doSearchAgentMerchantMaster();
                        }                        
                    });
                    
                    agentagentonffmerchinfoMasterGrid.init();
                    agentagentonffmerchinfoMasterGrid.parse(${onffmerchinfoMasterList}, "json");
                    
                    
                    agentagentonffmerchinfoMasterWindow = new dhtmlXWindows();
                    agentagentonffmerchinfoMasterWindow.attachViewportTo("agentonffmerchinfoList");

                    //버튼 추가
                    $("#agentonffmerchinfoForm input[name=agentonffmerchinfo_searchButton]").bind("click",function(e){
                        //지급ID정보 정보 조회 이벤트
                        //alert('11111');
                        /*
                        $("#agentonffmerchinfoForm input[name=page_no]").val("1");
                        agentagentonffmerchinfoMasterGrid.clearAll();
                        doSearchAgentMerchantMaster();
                        */
                       
                        agentagentonffmerchinfoMasterGrid.changePage(1);
                        return false;
                    });

                });


                function resizeOption($id){
                    //css 변경 : resize시 자동 변경됨.
                    $id.css("width","100%");
                    $id.css("height","100%");
                }


                //지급ID정보 정보 조회
                function doSearchAgentMerchantMaster(){
                    $.ajax({
                        url : $("#agentonffmerchinfoForm").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#agentonffmerchinfoForm").serialize(),
                        success : function(data) {
                            //agentagentonffmerchinfoMasterGrid.clearAll();
                            //agentagentonffmerchinfoMasterGrid.parse(jQuery.parseJSON(data), "json");
                            var jsonData = $.parseJSON(data);     
                            agentagentonffmerchinfoMasterGrid.parse(jsonData,"json");
                            //resizeOption($("#agentonffmerchinfoList.gridbox"));
                        },
                        error : function() {
                            alert("조회 실패");
                        }
                    });
                }

                var dhxSelPopupWins;
		dhxSelPopupWins=new dhtmlXWindows();
                    
                //ONOFF지급ID번호 팝업 클릭 이벤트
                $("#agentonffmerchinfoForm input[name=onffmerch_no]").unbind("click").bind("click", function (){

                    onoffMerMerchSelectPopup_agentmasterlist();

                });       
                /*
                 $("#agentonffmerchinfoForm input[name=comp_nm]").click(function () {
                    
                    CompanyMerchSelectWin_agentmasterlist();
                });
                */
                //검색조건 초기화
                $("#agentonffmerchinfoForm input[name=init]").click(function () {

                    merchMasterInit($("#agentonffmerchinfoForm"));

                });  
                
                    
                function onoffMerMerchSelectPopup_agentmasterlist(){
                    onoffObject.onffmerch_no = $("#agentonffmerchinfoForm input[name=onffmerch_no]");
                    onoffObject.merch_nm = $("#agentonffmerchinfoForm input[name=merch_nm]");
                    //ONOFF지급ID정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("지급ID 선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                }
                
               
                function CompanyMerchSelectWin_agentmasterlist()
                {
                    onoffObject.comp_nm = $("#agentonffmerchinfoForm input[name=comp_nm]");

                    w2 = dhxSelPopupWins.createWindow("companySelPopupList", 20, 30, 640, 480);
                    w2.setText("사업자번호 선택페이지");
                    dhxSelPopupWins.window('companySelPopupList').setModal(true);
                    w2.attachURL('<c:url value = "/popup/CompanySelectPopUp"/>', true);
                }
                function merchMasterInit($form) {
                    searchFormInit($form);
                } 
            </script>            
            <div class="searchForm1row" id="agentonffmerchinfoSearch" style="display:none;width:100%;height:100%;background-color:white;">
                <form id="agentonffmerchinfoForm" method="POST" action="<c:url value="/agent/merchantAgentMasterList" />" modelAttribute="MerchantFormBean">
                       <input type="hidden" name="page_size" value="100" />
                       <input type="hidden" name="page_no" value="1" />
                        <div class="label">대리점명</div>
                        <div class="input"><input type="text" name="agent_nm" size="15" maxlength="30"/></div>
                       
                        <div class="label2">지급ID</div>
                        <div class="input"><input type="text" name="onffmerch_no" size="10" maxlength="30"/></div>
                        
                        <div class="label">지급ID명</div>
                        <div class="input"><input type="text" name="merch_nm" size="15" maxlength="30"/></div>                        
                        
                        <div class="label">사업자명</div>
                        <div class="input"><input type="text"name="comp_nm" size="15" maxlength="30"/></div>

                        <div class="label">법인번호</div>
                        <div class="input"><input type="text" name="corp_no" size="15" maxlength="13"/></div>

                        <div class="label">사업자번호</div>
                        <div class="input"><input type="text" name="biz_no" size="15" maxlength="10"/></div>

                        <div class="label">상태</div>
                        <div class="input">
                            <select name="svc_stat">
                                 <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${UseFlagList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>
                        
                        <input type="button" name="agentonffmerchinfo_searchButton" value="조회"/>
                        <input type="button" name="init" value="검색조건지우기"/>
                </form>
                <div id="agentonffmerchinfoMasterListPaging" style="width: 100%;"></div>
                <div id="agentonffmerchinfoMasterListrecinfoArea" style="width: 100%;"></div>                        
            </div>
            <div id="agentonffmerchinfoList" style="width:100%;height:100%;background-color:white;"></div>
 <c:if test="${!ajaxRequest}">    
        </body>
</html>
</c:if>

