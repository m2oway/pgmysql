<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
	<div id="formsContent">
		<h2>Test Forms</h2>
		<p>
			See the <code>org.itPlayers.sample</code> package for the @Controller code
		</p>
		<form:form id="form" method="post" modelAttribute="testFormBean" cssClass="cleanform">
			<div class="header">
		  		<h2>Form</h2>
		  		<c:if test="${not empty message}">
					<div id="message" class="success">${message}</div>	
		  		</c:if>
		  		<s:bind path="*">
		  			<c:if test="${status.error}">
				  		<div id="message" class="error">Form has errors</div>
		  			</c:if>
		  		</s:bind>
			</div>
		  	<fieldset>
		  		<legend>Search Info</legend>
		  		<form:label path="firm_name">
		  			Name <form:errors path="firm_name" cssClass="error" />
		 		</form:label>
		  		<form:input path="firm_name" />
	
		  		<form:label path="direction">
		  			direction
		  		</form:label>
		  		<form:input path="direction" />
		  		
		  	</fieldset>
	
			<fieldset>
				<legend>result</legend>
				<c:if test="${not empty message}">
					<c:forEach items="${testresults}" var="testresult" varStatus="status">
						<div>
							<span>${status.count}</span> : <span><c:out value="${testresult.firm_name}"></c:out></span> 
						</div> 
					</c:forEach>
				</c:if>
		  	</fieldset>
			<p><button type="submit">Submit</button></p>
		</form:form>
		<script type="text/javascript">
			$(document).ready(function() {
				$("#form").submit(function() {
					$.post($(this).attr("action"), $(this).serialize(), function(html) {
						alert(html);
						$("#formsContent").replaceWith(html);
						$('html, body').animate({ scrollTop: $("#message").offset().top }, 500);
					});
					return false;  
				});			
			});
		</script>
	</div>