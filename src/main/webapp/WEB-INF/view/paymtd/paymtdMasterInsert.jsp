<%-- 
    Document   : paymtdMasterInsert
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>결제상품 등록</title>
        </head>
        <body>
</c:if>
<%
    String strCurDate = com.onoffkorea.system.common.util.Util.getTodayDate();
    String strTpDefaultEndDt = "20991231";
%>
            <script type="text/javascript">
                var regobj = new RegExp('-',"gi");
                var dhxSelPopupWins;
		dhxSelPopupWins = new dhtmlXWindows();
                               
                $(document).ready(function () {
                    
                    tid_layout = new dhtmlXLayoutObject("tidVP2", "1C", "dhx_skyblue");
                    tid_layout.cells('a').hideHeader();

                    tid_mygrid = tid_layout.cells('a').attachGrid();
                    tid_mygrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var mheaders = "";
                    mheaders += "<center>결제카드코드</center>,<center>결제카드명</center>,<center>결제사명</center>,<center>결제사코드</center>,<center>TID명</center>,<center>TID</center>,<center>가맹점종류코드</center>,<center>가맹점번호명</center>,<center>가맹점번호</center>";
                    tid_mygrid.setHeader(mheaders);
                    tid_mygrid.setColAlign("center,center,center,center,center,center,center,center,center");
                    tid_mygrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,txt,txt");
                    tid_mygrid.setInitWidths("10,80,150,10,150,100,100,100,150");
                    tid_mygrid.setColSorting("str,str,str,str,str,str,str,str,str");
                    tid_mygrid.enableColumnMove(false);
                    tid_mygrid.setSkin("dhx_skyblue");
                    tid_mygrid.init();
                    //tid_mygrid.parse(${termMasterList}, "json");
                    //tid_mygrid.attachEvent("onRowDblClicked", doInsertPaymtdTidInsert);
 
                   tid_mygrid.setColumnHidden(0,true);   
                   tid_mygrid.setColumnHidden(3,true);
                   tid_mygrid.setColumnHidden(6,true);
                   
 
                    tid_companydhxWins = new dhtmlXWindows();
                    tid_companydhxWins.attachViewportTo("tidVP2");
        
        
                    myCalendar = new dhtmlXCalendarObject(["view_start_dt","view_end_dt","paymtddetail_startdt","paymtddetail_enddt"]);
                                        
                    //paymtd 정보 등록 이벤트
                    $("#paymtdFormInsert input[name=insert]").click(function(){
                       doInsertPaymtdMaster(); 
                    });              
                    
                    //paymtd 정보 등록 닫기
                    $("#paymtdFormInsert input[name=close]").click(function(){
                         paymtddhxWins.window("paymtdInsertWindow").close();
                    });                            
                    
                });
                
                
                function TidSelectWin()
                {
                    w2 = dhxSelPopupWins.createWindow("tidSelPopupList", 20, 30, 640, 480);
                    w2.setText("TID번호 선택페이지");
                    dhxSelPopupWins.window('tidSelPopupList').setModal(true);
                    w2.attachURL('<c:url value = "/popup/TidSelectPopUp"/>', true);
                }
                
                function MidSelectWin()
                {
                    w2 = dhxSelPopupWins.createWindow("midSelPopupList", 20, 30, 640, 480);
                    w2.setText("가맹점번호 선택페이지");
                    dhxSelPopupWins.window('midSelPopupList').setModal(true);
                    w2.attachURL('<c:url value = "/popup/MidSelectPopUp"/>', true);
                }                
                
                function SelTidInfoInput(p_tid_mtd, p_tid_mtd_nm, p_tid_nm ,p_terminal_no)
                {
                    //alert(p_tid_mtd+","+ p_tid_mtd_nm +","+ p_tid_nm +","+ p_terminal_no);
                    $("#paymtdFormInsert input[name=paymtddetail_tid_mtd]").attr('value',p_tid_mtd);
                    $("#paymtdFormInsert input[name=paymtddetail_tid_mtd_nm]").attr('value',p_tid_mtd_nm);
                    $("#paymtdFormInsert input[name=paymtddetail_tid_nm]").attr('value',p_tid_nm);                    
                    $("#paymtdFormInsert input[name=paymtddetail_terminal_no]").attr('value',p_terminal_no);
                }
                
                function SelMidInfoInput(p_pay_mtd, p_mid_nm, p_merch_no)
                {                    
                    //alert(p_pay_mtd+","+ p_mid_nm +","+ p_merch_no);
                    $("#paymtdFormInsert input[name=paymtddetail_pay_mtd]").attr('value',p_pay_mtd);
                    $("#paymtdFormInsert input[name=paymtddetail_mid_nm]").attr('value',p_mid_nm);                    
                    $("#paymtdFormInsert input[name=paymtddetail_merch_no]").attr('value',p_merch_no);
                }                
                
                function addRow()
                {
                   if($("#paymtdFormInsert input[name=paymtddetail_terminal_no]").val() == "")
                   {
                       alert('TID를 선택해주세요.');
                       return false;
                   }
                   
                   if($("#paymtdFormInsert input[name=paymtddetail_merch_no]").val() == "")
                   {
                       alert('가맹점번호를 선택해주세요.');
                       return false;
                   }
                                      
                   
                   var instpval = $("#paymtdFormInsert select[name=paymtddetail_app_iss_cd] option:selected").val();

                   var chkflg = 0;
                   tid_mygrid.forEachRow(function(id){
 
                   if(instpval == tid_mygrid.cells(id,0).getValue())
                        {
                            chkflg = 1;
                        }
                    
                   });
        
                   if(chkflg==1) 
                   {
                       alert('등록된 결제카드가 있습니다.');
                       return false;
                   }
                   else
                   {
                        //결제카드코드,결제카드명,결제사명,결제사코드,TID명,TID
                        var strnewRow = 
                                $("#paymtdFormInsert select[name=paymtddetail_app_iss_cd] option:selected").val() 
                                + "," + 
                                $("#paymtdFormInsert select[name=paymtddetail_app_iss_cd] option:selected").text() 
                                + "," + 
                                $("#paymtdFormInsert input[name=paymtddetail_tid_mtd_nm]").val() 
                                + "," + 
                                $("#paymtdFormInsert input[name=paymtddetail_tid_mtd]").val()
                                + "," + 
                                $("#paymtdFormInsert input[name=paymtddetail_tid_nm]").val() 
                                + "," + 
                                $("#paymtdFormInsert input[name=paymtddetail_terminal_no]").val()
                                + "," + 
                                $("#paymtdFormInsert input[name=paymtddetail_pay_mtd]").val()
                                + "," + 
                                $("#paymtdFormInsert input[name=paymtddetail_mid_nm]").val()
                                                        + "," + 
                                $("#paymtdFormInsert input[name=paymtddetail_merch_no]").val()
                        tid_mygrid.addRow(tid_mygrid.getUID(), strnewRow, 1);
                   }

                }
                
                function delRow()
                {
                   //alert('delete row'); 
                   var selRowid = tid_mygrid.getSelectedRowId();
                   
                    tid_mygrid.deleteRow(selRowid);                   
                }

                //사업체 정보 등록
                function doInsertPaymtdMaster(){
                    
                    if(!window.confirm("등록하시겠습니까?")){
                        return;
                    }

                    
                    if($("#paymtdFormInsert input[name=pay_mtd_nm]").val() == "")
                    {
                      alert("결제상품명은 필수사항 입니다.");
                      $("#paymtdFormInsert input[name=pay_mtd_nm]").focus();
                      return;
                    }
                    
                    if($("#paymtdFormInsert input[name=commision]").val() == "")
                    {
                      alert("수수료는 필수사항 입니다.");
                      $("#paymtdFormInsert input[name=commision]").focus();
                      return;
                    }
                    
        
                    if($("#paymtdFormInsert input[name=view_start_dt]").val() == "")
                    {
                      alert("적용 시작일은 필수사항 입니다.");
                      $("#paymtdFormInsert input[name=view_start_dt]").focus();
                      return;
                    }
                    else
                    {
                        var regobj = new RegExp('-',"gi");
                        var strTpStartdt =  $("#paymtdFormInsert input[name=view_start_dt]").val();
                        $("#paymtdFormInsert input[name=start_dt]").attr('value',strTpStartdt.replace(regobj,''));
                    }
                    
                    if($("#paymtdFormInsert input[name=view_end_dt]").val() == "")
                    {
                      alert("적용 종료일은 필수사항 입니다.");
                      $("#paymtdFormInsert input[name=view_end_dt]").focus();
                      return;
                    }
                    else
                    {
                        var strTpEnddt =  $("#paymtdFormInsert input[name=view_end_dt]").val();
                        $("#paymtdFormInsert input[name=end_dt]").attr('value',strTpEnddt.replace(regobj,''));
                    }
                    
                    
                    var tppaymtddetailInfo = "";
                    //결제상품 상세정보를 만든다.
                    tid_mygrid.forEachRow(function(id){
                        
                        //결제카드코드,결제카드명,결제사명,결제사코드,TID명,TID,가맹점종류코드,가맹점번호명,가맹점번호
                        //   0           1          2        3      4     5     6               7           8
                        //  결제카드코드, 결제사코드, TID
                       var row = tid_mygrid.cells(id,0).getValue() + ',' + tid_mygrid.cells(id,3).getValue() + ',' + tid_mygrid.cells(id,5).getValue() + ',' + tid_mygrid.cells(id,6).getValue() + ',' + tid_mygrid.cells(id,8).getValue();
                       
                       tppaymtddetailInfo = tppaymtddetailInfo + row + ']';
                       //alert(tppaymtddetailInfo);
                    });
                   
                    $("#paymtdFormInsert input[name=paymtd_detailInfo]").attr('value',tppaymtddetailInfo);  
                   
                    $.ajax({
                        url : $("#paymtdFormInsert").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#paymtdFormInsert").serialize(),
                        success : function(data) {
                            //alert(data);
                            if(data=='1')
                            {
                                alert("등록 완료");
                                paymtd_list_mygrid.clearAll();
                                doSearchPaymtdMaster();
                                paymtddhxWins.window("paymtdInsertWindow").close();
                            }
                            else
                            {
                                alert("등록 실패");
                            }
                        },
                        error : function() { 
                                alert("등록 실패");
                        }
                    });
                }                   

            </script>
            <form id="paymtdFormInsert" method="POST" action="<c:url value="/paymtd/paymtdMasterInsert" />" modelAttribute="paymtdFormBean">
                <table class="gridtable" height="100%" width="100%">                    
                    <tr>
                        <td align='left' colspan='4'>- 결제상품 정보</td>                        
                    </tr>
                    <tr>                        
                        <td class="headcol" width="20%">결제상품명*</td>
                        <td colspan="3"><input name="pay_mtd_nm" size="60" maxlength="50" onblur="javascript:checkLength(this,50);" /></td>
                    </tr>
                     <tr>
                        <td class="headcol">사용여부*</td>
                        <td colspan="3"><select name="use_flag" >
                                <c:forEach var="code" items="${UseFlagList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == 'Y'}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach>
                            </select></td>
                    </tr>
                    <tr>
                        <td class="headcol">시작일*</td>
                        <td><input name="view_start_dt" id="view_start_dt" maxlength="10" value="<%=strCurDate%>" readonly/></td>
                        <td class="headcol">종료일</tD>
                        <td><input name="view_end_dt" id="view_end_dt" maxlength="10" value="<%=strTpDefaultEndDt%>" readonly/></td>
                    <input type="hidden" name="start_dt">
                    <input type="hidden" name="end_dt">
                    </tr>                    
                    <tr>
                        <td class="headcol">Memo</td>
                        <td colspan="3"><textarea name="memo" cols="80" rows="5"></textarea></td>                        
                    </tr>                          
                    <tr>
                        <td align='left' colspan='4'>- 결제상품 TID정보</td>                        
                    </tr>
                    <tr>                    
                        <td class="headcol">결제카드*</td>
                        <td><select name="paymtddetail_app_iss_cd" >
                                <c:forEach var="code" items="${IssCdList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select></td>
                        <td class="headcol">결제사*</td>
                        <td><input name="paymtddetail_tid_mtd_nm" size="30" readonly  onclick="javascript:TidSelectWin();"/></td>
                    </tr>
                    <tr>
                        <td class="headcol">TID명*</td>
                        <td ><input name="paymtddetail_tid_nm" size="30" readonly onclick="javascript:TidSelectWin();"/></td>
                        <td class="headcol">*TID</td>
                        <td><input name="paymtddetail_terminal_no" readonly onclick="javascript:TidSelectWin();"/></td>
                    </tr>
                    <tr>
                        <td class="headcol">가맹점번호명*</td>
                        <td ><input name="paymtddetail_mid_nm" size="30" readonly onclick="javascript:MidSelectWin();"/></td>
                        <td class="headcol">*가맹점번호</td>
                        <td><input name="paymtddetail_merch_no" readonly onclick="javascript:MidSelectWin();"/></td>
                    </tr>                    
                    <!--
                     <tr>
                        <td class="headcol">사용여부*</td>
                        <td colspan="3"><select name="paymtddetail_use_flag" >
                                <c:forEach var="code" items="${UseFlagList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == 'Y'}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach>
                            </select></td>
                    </tr>
                    -->
                    <tr>
                        <td align='left' colspan='4' valign="top">
                            <div id = "common_btn" align = "right" style="height:30px;" >
                            <input type="button" name="tidaddbt" value="TID추가" onclick="javascript:addRow();"/>
                            <input type="button" name="tiddelbt" value="TID삭제" onclick="javascript:delRow();"/>
                            </div>
                            <div id="tidVP2" style="position: relative; width:100%;height:200px;background-color:white;"></div>
                        </td>                        
                    </tr>   
                    <tr>
                        <td colspan="4" align="center"><input type="button" name="insert" value="등록"/>&nbsp;<input type="button" name="close" value="닫기"/></td>
                    </tr>       
                </table>
                 <input type="hidden" name="paymtddetail_tid_mtd" />    
                 <input type="hidden" name="paymtddetail_pay_mtd" />  
                  <input type="hidden" name="paymtd_detailInfo" />
            </form>  
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

