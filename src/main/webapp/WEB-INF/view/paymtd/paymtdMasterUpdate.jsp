<%-- 
    Document   : paymtdMasterInsert
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>결제상품 수정</title>
        </head>
        <body>
</c:if>
<%
    String strCurDate = com.onoffkorea.system.common.util.Util.getTodayDate();
    String strTpDefaultEndDt = "20991231";
%>
            <script type="text/javascript">
                var regobj = new RegExp('-',"gi");
                var dhxSelPopupWins;
		dhxSelPopupWins = new dhtmlXWindows();
                               
                $(document).ready(function () {
                    
                    tid_layout = new dhtmlXLayoutObject("tidVP2", "1C", "dhx_skyblue");
                    tid_layout.cells('a').hideHeader();

                    tid_mygrid = tid_layout.cells('a').attachGrid();
                    tid_mygrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var mheaders = "";
                    //결제상품seq,결제카드사코드,결제카드사명,결제사명,결제사코드,tid,사용여부코드,사용여부명
                    mheaders += "<center>pay_mtd_seq</center>,<center>결제카드사코드</center>,<center>결제카드사</center>,<center>결제사명</center>,<center>결제사코드</center>,<center>TID명</center>,<center>TID</center>,<center>가맹점종류코드</center>,<center>가맹점번호명</center>,<center>가맹점번호</center>,<center>useflag</center>,<center>사용여부</center>";
                    tid_mygrid.setHeader(mheaders);
                    tid_mygrid.setColAlign("center,center,center,center,center,center,center,center,center,center,center,center");
                    tid_mygrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt");
                    tid_mygrid.setInitWidths("10,10,80,80,50,150,50,50,100,100,10,80");
                    tid_mygrid.setColSorting("str,str,str,str,str,str,str,str,str,str,str,str");
                    tid_mygrid.enableColumnMove(false);
                    tid_mygrid.setSkin("dhx_skyblue");
                    tid_mygrid.init();
                    tid_mygrid.parse(${PaymtdDtlList}, "json");
                    tid_mygrid.attachEvent("onRowDblClicked", doSelectPaymtdTid);
                    
                    tid_mygrid.setColumnHidden(0,true);   
                    tid_mygrid.setColumnHidden(1,true);
                    tid_mygrid.setColumnHidden(4,true);
                    tid_mygrid.setColumnHidden(7,true);
                    tid_mygrid.setColumnHidden(10,true);
                    
    
                    tid_companydhxWins = new dhtmlXWindows();
                    tid_companydhxWins.attachViewportTo("tidVP2");
        
        
                    myCalendar = new dhtmlXCalendarObject(["view_start_dt","view_end_dt","paymtddtl_view_startdt","paymtddtl_view_enddt"]);
                                        
                    //paymtd 정보 등록 이벤트
                    $("#paymtdFormUpdate input[name=update]").click(function(){
                                       doUpdatePaymtdMaster(); 
                    });              
                    
                                      
                    $("#paymtdFormUpdate input[name=delete]").click(function(){
                         doDeletePaymtdMaster(); 
                    });      
                    
                    //paymtd 정보 등록 닫기
                    $("#paymtdFormUpdate input[name=close]").click(function(){
                         paymtddhxWins.window("paymtdUpdateWindow").close();
                    });                            
                    
                });
                
                function doSelectPaymtdTid()
                {   
                    //pay_mtd_seq,카드사코드,카드사명,paymtd,TID명,TID,시작일,종료일,useflag,useflag_nm
                    //  0            1           2        3    4    5    6      7      8       9          
                    $("#paymtdFormUpdate input[name=paymtddtl_terminal_no]").attr('value',tid_mygrid.cells(tid_mygrid.getSelectedId(),6).getValue());
                    $("#paymtdFormUpdate input[name=paymtddtl_tid_nm]").attr('value',tid_mygrid.cells(tid_mygrid.getSelectedId(),5).getValue());                    
                    $("#paymtdFormUpdate input[name=paymtddtl_tid_mtd_nm]").attr('value',tid_mygrid.cells(tid_mygrid.getSelectedId(),3).getValue());  
                    $("#paymtdFormUpdate input[name=paymtddtl_tid_mtd]").attr('value',tid_mygrid.cells(tid_mygrid.getSelectedId(),4).getValue());  
                    $("#paymtdFormUpdate input[name=paymtddtl_pay_mtd_dtl_seq]").attr('value',tid_mygrid.getSelectedId());
                    
                    $("#paymtdFormUpdate input[name=paymtddtl_mid_nm]").attr('value',tid_mygrid.cells(tid_mygrid.getSelectedId(),8).getValue());                    
                    $("#paymtdFormUpdate input[name=paymtddtl_merch_no]").attr('value',tid_mygrid.cells(tid_mygrid.getSelectedId(),9).getValue());  
                    $("#paymtdFormUpdate input[name=paymtddtl_pay_mtd]").attr('value',tid_mygrid.cells(tid_mygrid.getSelectedId(),7).getValue());  
                    
                    
                    $("#paymtddtl_app_iss_cd > option[value="+ tid_mygrid.cells(tid_mygrid.getSelectedId(),1).getValue() +"]").attr("selected", "ture");
                    $("#paymtddtl_use_flag > option[value="+ tid_mygrid.cells(tid_mygrid.getSelectedId(),7).getValue() +"]").attr("selected", "ture");
                }
                
                function setTransForm()
                {
                         $("#paymtdDtlFormInfo input[name=pay_mtd_dtl_seq]").attr('value',$("#paymtdFormUpdate input[name=paymtddtl_pay_mtd_dtl_seq]").val());
                         $("#paymtdDtlFormInfo input[name=app_iss_cd]").attr('value',$("#paymtdFormUpdate select[name=paymtddtl_app_iss_cd]").val());
                         $("#paymtdDtlFormInfo input[name=tid_mtd]").attr('value',$("#paymtdFormUpdate input[name=paymtddtl_tid_mtd]").val());
                         $("#paymtdDtlFormInfo input[name=terminal_no]").attr('value',$("#paymtdFormUpdate input[name=paymtddtl_terminal_no]").val());
                         $("#paymtdDtlFormInfo input[name=pay_mtd]").attr('value',$("#paymtdFormUpdate input[name=paymtddtl_pay_mtd]").val());
                         $("#paymtdDtlFormInfo input[name=merch_no]").attr('value',$("#paymtdFormUpdate input[name=paymtddtl_merch_no]").val());
                         $("#paymtdDtlFormInfo input[name=use_flag]").attr('value',$("#paymtdFormUpdate select[name=paymtddtl_use_flag]").val());
                }
                
                
                function clearSelForm()
                {
                    $("#paymtdFormUpdate input[name=paymtddtl_terminal_no]").attr('value',"");
                    $("#paymtdFormUpdate input[name=paymtddtl_tid_nm]").attr('value',"");                    
                    $("#paymtdFormUpdate input[name=paymtddtl_tid_mtd]").attr('value',"");  
                    $("#paymtdFormUpdate input[name=paymtddtl_tid_mtd_nm]").attr('value',"");  
                    
                    $("#paymtdFormUpdate input[name=paymtddtl_merch_no]").attr('value',"");
                    $("#paymtdFormUpdate input[name=paymtddtl_mid_nm]").attr('value',"");                    
                    $("#paymtdFormUpdate input[name=paymtddtl_pay_mtd]").attr('value',"");  
                    
                    $("#paymtdFormUpdate input[name=paymtddtl_pay_mtd_dtl_seq]").attr('value',"");

                    $("#paymtdDtlFormInfo input[name=pay_mtd_dtl_seq]").attr('value',"");
                    $("#paymtdDtlFormInfo input[name=app_iss_cd]").attr('value',"");
                    $("#paymtdDtlFormInfo input[name=tid_mtd]").attr('value',"");
                    $("#paymtdDtlFormInfo input[name=terminal_no]").attr('value',"");
                    $("#paymtdDtlFormInfo input[name=pay_mtd]").attr('value',"");
                    $("#paymtdDtlFormInfo input[name=merch_no]").attr('value',"");                    
                    $("#paymtdDtlFormInfo input[name=use_flag]").attr('value',"");
                }
                
                
                function TidSelectWin()
                {
                    w2 = dhxSelPopupWins.createWindow("tidSelPopupList", 20, 30, 640, 480);
                    w2.setText("TID 선택페이지");
                    dhxSelPopupWins.window('tidSelPopupList').setModal(true);
                    w2.attachURL('<c:url value = "/popup/TidSelectPopUp"/>', true);
                }
                
                function MidSelectWin()
                {
                    w2 = dhxSelPopupWins.createWindow("midSelPopupList", 20, 30, 640, 480);
                    w2.setText("가맹점번호 선택페이지");
                    dhxSelPopupWins.window('midSelPopupList').setModal(true);
                    w2.attachURL('<c:url value = "/popup/MidSelectPopUp"/>', true);
                }           
                
                function SelTidInfoInput(p_tid_mtd, p_tid_mtd_nm, p_tid_nm ,p_terminal_no)
                {
                    $("#paymtdFormUpdate input[name=paymtddtl_tid_mtd]").attr('value',p_tid_mtd);
                    $("#paymtdFormUpdate input[name=paymtddtl_tid_mtd_nm]").attr('value',p_tid_mtd_nm);
                    $("#paymtdFormUpdate input[name=paymtddtl_tid_nm]").attr('value',p_tid_nm);                    
                    $("#paymtdFormUpdate input[name=paymtddtl_terminal_no]").attr('value',p_terminal_no);
                }
                
                
                function SelMidInfoInput(p_pay_mtd, p_mid_nm, p_merch_no)
                {                    
                    //alert(p_pay_mtd+","+ p_mid_nm +","+ p_merch_no);
                    $("#paymtdFormUpdate input[name=paymtddtl_pay_mtd]").attr('value',p_pay_mtd);
                    $("#paymtdFormUpdate input[name=paymtddtl_mid_nm]").attr('value',p_mid_nm);                    
                    $("#paymtdFormUpdate input[name=paymtddtl_merch_no]").attr('value',p_merch_no);
                }                
                         
                
                // 정보 조회
                function doSearchPaymtdDtlList(){
                    $.ajax({
                        url : "../paymtd/paymtdDtlList",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#paymtdDtlFormInfo").serialize(),
                        success : function(data) {
                            tid_mygrid.clearAll();
                            tid_mygrid.parse(jQuery.parseJSON(data), "json");
                        },
                        error : function() { 
                                alert("조회 실패");
                        }
                    });	            
                }                
                
                function addRow()
                {                 
                   setTransForm();
                   $("#paymtdDtlFormInfo input[name=pay_mtd_dtl_seq]").attr('value', '');
                   
                   if($("#paymtdFormUpdate input[name=paymtddtl_terminal_no]").val() == "")
                   {
                       alert('TID를 선택해주세요.');
                       return false;
                   }
                   
                   if($("#paymtdFormUpdate input[name=paymtddtl_merch_no]").val() == "")
                   {
                       alert('가맹점번호를 선택해주세요.');
                       return false;
                   }                   
                   
                   //카드사 검사
                   var chk_app_iss_cd = $("#paymtdFormUpdate select[name=paymtddtl_app_iss_cd] option:selected").val();
                   var chk_tid_mtd = $("#paymtdFormUpdate input[name=paymtddtl_tid_mtd]").val();
                   var chk_terminal_no = $("#paymtdFormUpdate input[name=paymtddtl_terminal_no]").val();
                   var chk_pay_mtd_dtl_seq = $("#paymtdFormUpdate input[name=paymtddtl_pay_mtd_dtl_seq]").val();

                   var chkflg = 0;
                   tid_mygrid.forEachRow(function(id){
                   
                   //자기자신이 아닌경우
                   //if(chk_pay_mtd_dtl_seq != id )
                   {
                        //pay_mtd_seq,카드사코드,카드사명,paymtd,TID명,TID,시작일,종료일,useflag,useflag_nm
                        //  0            1           2        3    4    5    6      7      8       9      
                        //같은결제은행으로 사용되는 tid정보가 있는지
                        //alert( tid_mygrid.cells(id,1).getValue());
                        //alert( tid_mygrid.cells(id,7).getValue());
                        //alert( tid_mygrid.cells(id,6).getValue());
                        
                        if(chk_app_iss_cd == tid_mygrid.cells(id,1).getValue() && tid_mygrid.cells(id,7).getValue() == 'Y' )
                        {
                             chkflg = 1;
                        }
                        
                        if(chk_app_iss_cd == tid_mygrid.cells(id,1).getValue() && tid_mygrid.cells(id,6).getValue() == chk_terminal_no )
                        {
                             chkflg = 2;
                        }
                   }
                    
                   });
        
                   if(chkflg==1) 
                   {
                       alert('등록된 결제카드가 있습니다.');
                       return false;
                   }
                   
                   if(chkflg==2) 
                   {
                       alert('등록된 TID입니다.');
                       return false;
                   }
                   
                   
                   
                   $.ajax({
                        url :  "../paymtd/paymtdDtlInsert",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#paymtdDtlFormInfo").serialize(),
                        success : function(data) {
                            if(data == '1')
                            {
                                alert("등록 완료");
                                tid_mygrid.clearAll();
                                doSearchPaymtdDtlList();
                                clearSelForm();
                            }
                            else
                            {
                                alert("등록 실패");
                            }
                        },
                        error : function() { 
                                alert("등록 실패");
                        }
                    });
                }
                
                function modRow()
                {
                   //카드사 검사
                   var chk_app_iss_cd = $("#paymtdFormUpdate select[name=paymtddtl_app_iss_cd] option:selected").val();
                   var chk_tid_mtd = $("#paymtdFormUpdate input[name=paymtddtl_tid_mtd]").val();
                   var chk_terminal_no = $("#paymtdFormUpdate input[name=paymtddtl_terminal_no]").val();
                   var chk_pay_mtd_dtl_seq = $("#paymtdFormUpdate input[name=paymtddtl_pay_mtd_dtl_seq]").val();                    
                    
                   if($("#paymtdFormUpdate input[name=paymtddtl_pay_mtd_dtl_seq]").val() == "")
                   {
                       alert('수정할 TID를 선택해주세요.');
                       return false;
                   }                    
                   
                   if($("#paymtdFormUpdate input[name=paymtddtl_terminal_no]").val() == "")
                   {
                       alert('TID를 선택해주세요.');
                       return false;
                   }
                   
                   
                   if($("#paymtdFormUpdate input[name=paymtddtl_merch_no]").val() == "")
                   {
                       alert('가맹점번호를 선택해주세요.');
                       return false;
                   }                   

                   //검사
                   var chkflg = 0;
                   tid_mygrid.forEachRow(function(id){
                   
                   //자기자신이 아닌경우
                   if(chk_pay_mtd_dtl_seq != id )
                   {
                        //pay_mtd_seq,카드사코드,카드사명,paymtd,TID명,TID,시작일,종료일,useflag,useflag_nm
                        //  0            1           2        3    4    5    6      7      8       9      
                        //같은결제은행으로 사용되는 tid정보가 있는지
                        //alert( tid_mygrid.cells(id,1).getValue());
                        //alert( tid_mygrid.cells(id,7).getValue());
                        //alert( tid_mygrid.cells(id,6).getValue());
                        
                        if(chk_app_iss_cd == tid_mygrid.cells(id,1).getValue() && tid_mygrid.cells(id,7).getValue() == 'Y' )
                        {
                             chkflg = 1;
                        }
                        
                        if(chk_app_iss_cd == tid_mygrid.cells(id,1).getValue() && tid_mygrid.cells(id,6).getValue() == chk_terminal_no )
                        {
                             chkflg = 2;
                        }
                   }
                    
                   });
        
                   if(chkflg==1) 
                   {
                       alert('등록된 결제카드가 있습니다.');
                       return false;
                   }
                   
                   if(chkflg==2) 
                   {
                       alert('등록된 TID입니다.');
                       return false;
                   }                   
                   
                    setTransForm();
                    
                   $.ajax({
                        url :  "../paymtd/paymtdDtlUpdate",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#paymtdDtlFormInfo").serialize(),
                        success : function(data) {
                            
                            if(data == '1')
                            {
                                alert("수정 완료");
                                $("#paymtdDtlFormInfo input[name=pay_mtd_dtl_seq]").attr('value', '');
                                tid_mygrid.clearAll();
                                doSearchPaymtdDtlList();
                                clearSelForm();
                            }
                            else
                            {
                                alert("수정 실패");
                            }
                        },
                        error : function() { 
                                alert("수정 실패");
                        }
                    });

                }                
                
                function delRow()
                {
                    if(confirm('삭제하시겠습니까?'))
                    {
                        setTransForm();
                        
                        if($("#paymtdFormUpdate input[name=paymtddtl_pay_mtd_dtl_seq]").val() == "")
                        {
                            alert('삭제할 TID를 선택해주세요.');
                            return false;
                        }  

                        $.ajax({
                             url :  "../paymtd/paymtdDtlDelete",
                             type : "POST",
                             async : true,
                             dataType : "json",
                             data: $("#paymtdDtlFormInfo").serialize(),
                             success : function(data) {
                                 if(data == '1')
                                 {
                                    alert("삭제 완료");
                                    $("#paymtdDtlFormInfo input[name=pay_mtd_dtl_seq]").attr('value', '');
                                    tid_mygrid.clearAll();
                                    doSearchPaymtdDtlList();
                                    clearSelForm();
                                }
                                else
                                {
                                    alert("삭제 실패");
                                }
                             },
                             error : function() { 
                                     alert("삭제 실패");
                             }
                         });
                    }

                }

                //사업체 정보 등록
                function doUpdatePaymtdMaster(){
                    
                    if(!window.confirm("수정하시겠습니까?")){
                        return;
                    }

                    
                    if($("#paymtdFormUpdate input[name=pay_mtd_nm]").val() == "")
                    {
                      alert("결제상품명은 필수사항 입니다.");
                      $("#paymtdFormUpdate input[name=pay_mtd_nm]").focus();
                      return;
                    }
                    
        
                    if($("#paymtdFormUpdate input[name=view_start_dt]").val() == "")
                    {
                      alert("적용 시작일은 필수사항 입니다.");
                      $("#paymtdFormUpdate input[name=view_start_dt]").focus();
                      return;
                    }
                    else
                    {
                        var regobj = new RegExp('-',"gi");
                        var strTpStartdt =  $("#paymtdFormUpdate input[name=view_start_dt]").val();
                        $("#paymtdFormUpdate input[name=start_dt]").attr('value',strTpStartdt.replace(regobj,''));
                    }
                    
                    if($("#paymtdFormUpdate input[name=view_end_dt]").val() == "")
                    {
                      alert("적용 종료일은 필수사항 입니다.");
                      $("#paymtdFormUpdate input[name=view_end_dt]").focus();
                      return;
                    }
                    else
                    {
                        var regobj = new RegExp('-',"gi");
                        var strTpEnddt =  $("#paymtdFormUpdate input[name=view_end_dt]").val();
                        $("#paymtdFormUpdate input[name=end_dt]").attr('value',strTpEnddt.replace(regobj,''));
                    }
                   
                    $.ajax({
                        url : $("#paymtdFormUpdate").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#paymtdFormUpdate").serialize(),
                        success : function(data) {
                            if(data == '1')
                            {
                                alert("수정 완료");
                                paymtd_list_mygrid.clearAll();
                                doSearchPaymtdMaster();
                                paymtddhxWins.window("paymtdUpdateWindow").close();
                            }
                            else
                            {
                                alert("수정 실패");
                            }
                        },
                        error : function() { 
                                alert("수정 실패");
                        }
                    });	
                   
                    
                }                   
                
                

                //삭제
                function doDeletePaymtdMaster(){
                    
                    if(!window.confirm("삭제하시겠습니까?")){
                        return;
                    }
                   
                    $.ajax({
                        url : "../paymtd/paymtdMasterDelete",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#paymtdFormUpdate").serialize(),
                        success : function(data) {
                            if(data == '1')
                            {
                                alert("삭제완료");
                                paymtd_list_mygrid.clearAll();
                                doSearchPaymtdMaster();
                                paymtddhxWins.window("paymtdUpdateWindow").close();
                            }
                            else
                            {
                                alert("수정 실패");
                            }
                        },
                        error : function() { 
                                alert("수정 실패");
                        }
                    });	
                   
                    
                }                                   

            </script>
            <div style='width:100%;height:100%;overflow:auto;'>
            <form id="paymtdFormUpdate" method="POST" action="<c:url value="/paymtd/paymtdMasterUpdate" />" modelAttribute="paymtdFormBean">
                <input type="hidden" name="pay_mtd_seq" value="${tb_Paymtd.pay_mtd_seq}"/>
                <table class="gridtable" height="100%" width="100%">                    
                    <tr>
                        <td align='left' colspan='4'>- 결제상품 정보</td>                        
                    </tr>
                    <tr>                        
                        <td class="headcol" >결제상품명*</td>
                        <td colspan="3"><input name="pay_mtd_nm" size="30" maxlength="50" value="${tb_Paymtd.pay_mtd_nm}" onblur="javascript:checkLength(this,50);" /></td>
                    </tr>
                     <tr>
                        <td class="headcol">사용여부*</td>
                        <td colspan="3"><select name="use_flag" >
                                <c:forEach var="code" items="${UseFlagList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Paymtd.use_flag}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach>
                            </select></td>
                    </tr>
                    <tr>
                        <td class="headcol">시작일*</td>
                        <td><input name="view_start_dt" id="view_start_dt" maxlength="10" value="${tb_Paymtd.start_dt}" readonly/></td>
                        <td class="headcol">종료일</tD>
                        <td><input name="view_end_dt" id="view_end_dt" maxlength="10" value="${tb_Paymtd.end_dt}" readonly/></td>
                    <input type="hidden" name="start_dt" value="${tb_Paymtd.start_dt}" >
                    <input type="hidden" name="end_dt" value="${tb_Paymtd.end_dt}">
                    </tr>                    
                    <tr>
                        <td class="headcol">Memo</td>
                        <td colspan="3"><textarea name="memo" cols="60" rows="5">${tb_Paymtd.memo}</textarea></td>                        
                    </tr>                          
                    <tr>
                        <td colspan="4" align="center"><input type="button" name="update" value="수정"/>&nbsp;<input type="button" name="delete" value="삭제"/>&nbsp;<input type="button" name="close" value="닫기"/></td>
                    </tr>  
                    <tr>
                        <td align='left' colspan='4'>- 결제상품 TID정보</td>                        
                    </tr>
                    <tr>                    
                        <td class="headcol">결제카드*</td>
                        <td><select name="paymtddtl_app_iss_cd" id="paymtddtl_app_iss_cd" >
                                <c:forEach var="code" items="${IssCdList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select></td>
                        <td class="headcol">결제사*</td>
                        <td><input name="paymtddtl_tid_mtd_nm" maxlength="20" readonly onclick="javascript:TidSelectWin();"/></td>
                    </tr>
                    <tr>
                        <td class="headcol">TID명*</td>
                        <td><input name="paymtddtl_tid_nm" size="40" readonly onclick="javascript:TidSelectWin();"/></td>
                        <td class="headcol">TID*</td>
                        <td><input name="paymtddtl_terminal_no" size="40" readonly onclick="javascript:TidSelectWin();"/></td>
                    </tr>
                    <tr>
                        <td class="headcol">가맹점번호명*</td>
                        <td><input name="paymtddtl_mid_nm" size="40" readonly onclick="javascript:MidSelectWin();"/></td>
                        <td class="headcol">가맹점번호*</td>
                        <td><input name="paymtddtl_merch_no" size="40" readonly onclick="javascript:MidSelectWin();"/></td>
                    </tr>                    
                    <tr>
                    <td class="headcol">사용여부*</td>
                    <td colspan="3"><select name="paymtddtl_use_flag"  id="paymtddtl_use_flag" >
                                <c:forEach var="code" items="${UseFlagList.tb_code_details}">
                                    <option value="${code.detail_code}" >${code.code_nm}</option>
                                </c:forEach>
                            </select></td>
                    </tr>
                    <tr>
                        <td align='left' colspan='4' valign="top">
                            <div id = "common_btn" align = "right" style="height:25px;" >
                            <input type="button" name="tidaddbt" value="TID추가" onclick="javascript:addRow();"/>
                            <input type="button" name="tidmodbt" value="TID수정" onclick="javascript:modRow();"/>
                            <input type="button" name="tiddelbt" value="TID삭제" onclick="javascript:delRow();"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align='left' colspan='4' valign="top">
                            <div id="tidVP2" style="position:relative; width:100%;height:200px;background-color:white;"></div>
                        </td>                        
                    </tr>   
                </table>
                <input type="hidden" name="paymtddtl_tid_mtd" />
                <input type="hidden" name="paymtddtl_pay_mtd" />
                <input type="hidden" name="paymtddtl_pay_mtd_seq" value="${tb_Paymtd.pay_mtd_seq}" />
                <input type="hidden" name="paymtddtl_pay_mtd_dtl_seq" />
</form>                        
<form name="paymtdDtlFormInfo" id="paymtdDtlFormInfo" method="POST"  modelAttribute="paymtdDtlFormBean">
    <input type="hidden" name="pay_mtd_dtl_seq" />
    <input type="hidden" name="pay_mtd_seq" value="${tb_Paymtd.pay_mtd_seq}" />
    <input type="hidden" name="app_iss_cd"/>
    <input type="hidden" name="tid_mtd"/>
    <input type="hidden" name="terminal_no"/>
    <input type="hidden" name="pay_mtd"/>
    <input type="hidden" name="merch_no"/>    
    <input type="hidden" name="use_flag"/>
</form>
            </div>
    <c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

