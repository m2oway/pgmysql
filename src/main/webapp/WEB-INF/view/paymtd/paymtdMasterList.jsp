<%-- 
    Document   : paymtdMaterList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>결제상품 관리</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">
                var paymtddhxWins;
                paymtddhxWins = new dhtmlXWindows();
                $(document).ready(function () {
                    var paymtdSearch = document.getElementById("paymtd_dhx_search");
                    var tabBarId = tabbar.getActiveTab();

                    paymtd_list_layout = tabbar.cells(tabBarId).attachLayout("2E");
                    paymtd_list_layout.cells('a').hideHeader();
                    paymtd_list_layout.cells('b').hideHeader();
                    paymtd_list_layout.cells('a').attachObject(paymtdSearch);
                    paymtd_list_layout.cells('a').setHeight("45");
                    paymtd_list_mygrid = paymtd_list_layout.cells('b').attachGrid();
                    paymtd_list_mygrid.setImagePath("<c:url value ='/resources/images/dhx/dhtmlxGrid/'/>");
                    var mheaders = "";
                    mheaders += "<center>NO</center>,<center>결제상품명</center>,<center>일반/무이자</center>,<center>자체/대행</center>,<center>시작일</center>,<center>종료일</center>,<center>사용여부</center>";
                    paymtd_list_mygrid.setHeader(mheaders);
                    paymtd_list_mygrid.setColAlign("center,center,center,center,center,center,center");
                    paymtd_list_mygrid.setColTypes("txt,txt,txt,txt,txt,txt,txt");
                    paymtd_list_mygrid.setInitWidths("50,200,100,100,100,100,100");
                    paymtd_list_mygrid.setColSorting("str,str,str,str,str,str");
                    paymtd_list_mygrid.enableColumnMove(true);
                    paymtd_list_mygrid.setSkin("dhx_skyblue");
                    paymtd_list_mygrid.init();

                    paymtd_list_mygrid.setColumnHidden(2,true);   
                    paymtd_list_mygrid.setColumnHidden(3,true);

                    paymtd_list_mygrid.parse(${paymtdMasterList}, "json");
                    paymtd_list_mygrid.attachEvent("onRowDblClicked", doUpdateFormPaymtdMaster);
                    

                    
                    paymtddhxWins.attachViewportTo("paymtdVP");
                    
                    $(".paymtd_action .insert").button({
                        icons: {
                            primary: "ui-icon-plusthick"
                        },
                        text: false
                    })
                    .unbind("click")
                    .bind("click",function(e){
                        // 정보 등록 이벤트
                        doInsertFormPaymtdMaster(); 
                        return false;
                    });
                    /*
                     $(".paymtd_action .delete").button({
                        icons: {
                            primary: "ui-icon-minusthick"
                        },
                        text: false
                    })                    
                    .unbind("click")
                    .bind("click",function(e){
                        // 정보 등록 이벤트
                        doDeletePaymtdMaster(); 
                        return false;
                    });
                    */
            
                    //버튼 추가
                    $("#paymtdForm input[name=paymtd_searchButton]").bind("click",function(e){
                        //사업체 정보 조회 이벤트
                        $("#midForm input[name=page_no]").val("1");
                        doSearchPaymtdMaster(); 
                        return false;
                    });                    
                    
                    
                    //검색조건 초기화
                    $("#paymtdForm input[name=init]").click(function () {
                        paymtdMasterInit($("#paymtdForm"));

                    }); 
                    

                });
                
                //정보 수정
                function doUpdateFormPaymtdMaster(rowid, col){
                    //alert(rowid);
                    var pay_mtd_seq = paymtd_list_mygrid.getUserData(rowid, 'pay_mtd_seq');
                    var p = 0;

                    w1 = paymtddhxWins.createWindow("paymtdUpdateWindow", 20, 30, 800, 770);
                    w1.setText("결제상품수정");
                    paymtddhxWins.window('paymtdUpdateWindow').setModal(true);
                    w1.attachURL('<c:url value = "/paymtd/paymtdMasterUpdate"/>' +"?pay_mtd_seq=" + pay_mtd_seq + '&type=update', true);
                    
                }
                
                // 정보 조회
                function doSearchPaymtdMaster(){
                    $.ajax({
                        url : $("#paymtdForm").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#paymtdForm").serialize(),
                        success : function(data) {
                            paymtd_list_mygrid.clearAll();
                            paymtd_list_mygrid.parse(jQuery.parseJSON(data), "json");
                        },
                        error : function() { 
                                alert("조회 실패");
                        }
                    });	            
                }
                

                // 정보 등록
                function doInsertFormPaymtdMaster(){
                    w1 = paymtddhxWins.createWindow("paymtdInsertWindow", 20, 30, 800, 700);
                    w1.setText("결제상품 등록페이지");
                    paymtddhxWins.window('paymtdInsertWindow').setModal(true);
                    w1.attachURL('<c:url value = "/paymtd/paymtdMasterInsert?type=insert"/>', true);
                }
                function paymtdMasterInit($form) {
                    searchFormInit($form);
                } 
            </script>

            <div class="searchForm1row" id="paymtd_dhx_search" style="display:none;width:100%;height:100%;background-color:white;">
                <form id="paymtdForm" method="POST" action="<c:url value = '/paymtd/paymtdMasterList'/>" modelAttribute="paymtdFormBean">
                    <input type ="hidden" name="pay_mtd_seqs" value="" >
                    <div class="label">결제상품명</div>
                    <div class="input"><input type="text" name="pay_mtd_nm" value="" /></div>
<!--
                        <div class="label">일반/무이자</div>
                        <div class="input"><select name="func_cate" >
                                <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${FuncCateList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select></div>

                        <div class="label">자체/대행</div>
                        <div class="input">
                            <select name="agency_flag" >
                                <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${AgencyFlagList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>
-->
                        <div class="label">사용여부</div>
                        <div class="input">
                            <select name="use_flag" >
                                <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${UseFlagList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <input type="button" name="paymtd_searchButton" value="조회"/>
                        <input type="button" name="init" value="검색조건지우기"/>
                    <div class="paymtd_action" style = "float:right">
                        <button class="insert" name="insert">추가</button>
                        <!--
                        <button class="delete" name="delete">삭제</button>
                        -->
                    </div>
                </form>
            </div>
            <div id="paymtdVP" style="width:100%;height:100%;background-color:white;"></div>

<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

