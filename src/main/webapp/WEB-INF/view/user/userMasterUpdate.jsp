<%-- 
    Document   : merchantMaterList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>회원 정보 관리 수정</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">
                     var userUpdateWins;
            
                 $(document).ready(function () {
                
                    userUpdateWins = new dhtmlXWindows();
                    
                    //회원 정보 수정 이벤트
                    $("#userFormUpdate input[name=user_update]").click(function(){
                        userUpdate(); 
                    }); 
                    
                    //회원정보 삭제 이벤트
                    $("#userFormUpdate input[name=user_delete]").click(function(){
                        user_Delete(); 
                    }); 
                    
                    //회원 정보 수정 닫기
                    $("#userFormUpdate input[name=user_close]").click(function(){
                        userupdateWins.window('userupdatew1').close();
                    });     
                    
                    //사용자구분에 따른 팝업 이벤트
                    $("#userFormUpdate input[name=user_cate_nm]").click(function(){
                        <c:if test="${userFormBean.ses_user_cate == '00'}">
                            user_cate = $("#userFormUpdate select[name=user_cate]").val();
                            if(user_cate === '01'){ //정산관리자
                                onoffMerchSelectPopup();
                            }else if(user_cate === '02'){ //결제관리자
                                 onoffTidSelectPopup();                            
                            }else if(user_cate === '03'){ //대리점관리자
                                agentSelectPopUp();
                            }
                        </c:if>
                    });        
                    
                    $("#userFormUpdate select[name=user_cate]").change(function(){
                        user_cate = $(this).val();
                        if(user_cate === '00'){
                            $("#userFormUpdate input[name=user_cate_nm]").attr("disabled","disabled");
                        }else{
                            $("#userFormUpdate input[name=user_cate_nm]").removeAttr("disabled");
                        }
                        $("#userFormUpdate input[name=user_cate_nm]").val("");
                    });
                    
                });
                
                var dhxSelPopupWins=new dhtmlXWindows();
                
                //대리점정보 팝업
                function agentSelectPopUp(){
                    w2 = dhxSelPopupWins.createWindow("AgentSelectPopUp", 20, 30, 640, 480);
                    w2.setText("대리점선택페이지");
                    dhxSelPopupWins.window('AgentSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('AgentSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/AgentSelectPopUp"/>', true);
                }                

                function SelAgentInfoInput(p_agent_seq, p_agent_nm){
                    $("#userFormUpdate input[name=agent_seq]").val(p_agent_seq);
                    $("#userFormUpdate input[name=user_cate_nm]").val(p_agent_nm);
                }             
                
                function onoffMerchSelectPopup(){
                    //ONOFF지급ID정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("지급ID 선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                }
                
                function SelOnoffmerchInfoInput(p_onffmerch_no, p_merch_nm){
                    $("#userFormUpdate input[name=onffmerch_no]").val(p_onffmerch_no);
                    $("#userFormUpdate input[name=user_cate_nm]").val(p_merch_nm);                    
                }
                
                function onoffTidSelectPopup(){
                    //ONOFFUID 팝업
                    w2 = dhxSelPopupWins.createWindow("OnofftidSelectPopUp", 20, 30, 640, 480);
                    w2.setText("UID 선택페이지");
                    dhxSelPopupWins.window('OnofftidSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnofftidSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnofftidSelectPopUp"/>', true);   
                }                
              
                function SelOnofftidInfoInput(p_onffmerch_no, p_onfftid, p_onfftid_nm){
                    $("#userFormUpdate input[name=onffmerch_no]").val(p_onffmerch_no);
                    $("#userFormUpdate input[name=onfftid]").val(p_onfftid);
                    $("#userFormUpdate input[name=user_cate_nm]").val(p_onfftid_nm);                    
                }

                //회원수정
                function userUpdate(){
                    /*
                    if(($("#userFormUpdate input[name=user_pwd]").val()).length === 0){
                        alert("비밀번호를 입력하십시오.");
                        $("#userFormUpdate input[name=user_pwd]").val("");
                        $("#userFormUpdate input[name=user_pwd]").focus();
                        return;
                    }
                    */
                    $("#userFormUpdate select[name=user_cate]").removeAttr("disabled");
                    
                    $.ajax({
                        url : $("#userFormUpdate").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#userFormUpdate").serialize(),
                        success : function(data) {
                            //alert(data);
                            if(data=='1')
                            {
                                alert("수정 완료");
                                usergrid.clearAll();
                                userSearch();
                                userupdateWins.window('userupdatew1').close();
                            }
                            else
                            {
                                alert('수정실패');
                            }
                        },
                        error : function() { 
                                alert("수정 실패");
                        }
                    });	                         

                }
                
                //회원 삭제
                function user_Delete(){
                    
                    $("#userFormUpdate").attr("action","<c:url value="/user/userMasterDelete" />");
                    
                    $.ajax({
                        url : $("#userFormUpdate").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#userFormUpdate").serialize(),
                        success : function(data) {
                            alert("삭제 완료");
                            userSearch();
                            userupdateWins.window('userupdatew1').close();
                        },
                        error : function() { 
                                alert("삭제 실패");
                        }
                    });	                                
        
                } 
            </script>

            <form id="userFormUpdate" method="POST" action="<c:url value="/user/userMasterUpdate" />" modelAttribute="userFormBean">
                 <input type = "hidden" name="org_seq" value="${tb_User.org_seq}">
                  <input type = "hidden" name="user_seq" value="${tb_User.user_seq}">
                  <input type = "hidden" name="onffmerch_no" value="${tb_User.onffmerch_no}">
                  <input type = "hidden" name="onfftid" value="${tb_User.onfftid}">
                  <input type = "hidden" name="agent_seq" value="${tb_User.agent_seq}">
                  <input type = "hidden" name = "result_colum" value = "userFormUpdate">
                <table class="gridtable">
                    <tr>
                        <th>ID</th>
                        <td  colspan="4">${tb_User.user_id} <c:if test="${userFormBean.ses_user_cate == '00'}">[${tb_User.user_pwd}]</c:if></td>
                    </tr>
                    <tr>
                        <th>비밀번호</th>
                        <td><input type="password" name="user_pwd" maxlength="20" value="" /></td>
                         <th>변경비밀번호</th>
                        <td><input type="password" name="user_pwd2" maxlength="20" value="" /></td>
                    </tr>
                    <tr>
                        <th>이름</th>
                        <td  colspan="4"><input name="user_nm" maxlength="50" value="${tb_User.user_nm}"/></td>
                    </tr>   
                    <tr>
                        <th>연락처1</th>
                        <td><input name="tel_no_1" maxlength="50" value="${tb_User.tel_no_1}"/></td>
                        <th>연락처2</th>
                        <td><input name="tel_no_2" maxlength="50" value="${tb_User.tel_no_2}"></td>
                    </tr>  
                    <tr>
                        <th>이메일</th>
                        <td  colspan="4"><input name="user_email" maxlength="50"value="${tb_User.user_email}"/></td>
                    </tr>   
                    <tr>
                       <th>사용자 유형</th>
                       <td colspan="3">
                            <c:if test="${userFormBean.ses_user_cate == '00'}">
                                <select name="user_cate">
                            </c:if>
                            <c:if test="${userFormBean.ses_user_cate != '00'}">
                                <select name="user_cate" disabled="disabled">
                            </c:if>                                    
                                    <option value="">선택하세요</option>
                                    <c:forEach var="userCateList" items="${userCateList.tb_code_details}">
                                        <option value="${userCateList.detail_code}" <c:if test="${userCateList.detail_code == tb_User.user_cate}">selected</c:if>>${userCateList.code_nm}</option>
                                    </c:forEach>                                      
                            </select> 
                                    <input name="user_cate_nm" size="40" maxlength="50" value="${tb_User.user_cate_nm}" readonly="readonly">
                        </td>
                    </tr>                                          
                    <tr>
                        <td style="text-align: center" colspan="4" >
                            <input type="button" name="user_update" value="저장"/>
                            <c:if test="${userFormBean.ses_user_cate == '00'}">
                                <input type="button" name="user_delete" value="삭제"/>
                            </c:if>
                            <input type="button" name="user_close" value="닫기"/>
                        </td>
                    </tr>
                </table>
            </form>

<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

