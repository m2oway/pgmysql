<%-- 
    Document   : codeMaster
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>회원조회</title>             
        </head>
        <body>
</c:if>
       
    <body>
        <!--<h1>신용거래</h1>-->
        <div  class="searchForm1row" class="right" id="user_search" style="width:100%;">       
            <form id="userForm" method="POST" onsubmit="return false" action="<c:url value="/user/userMasterList" />" modelAttribute="userFormBean"> 
                <input type = "hidden" name = "org_seq" value ="">
                <input type = "hidden" name = "result_colum" value = "userForm">
                <input type="hidden" name="page_size" value="100" >
                <input type="hidden" name="page_no" value="1" >                 
                <table>
                   <tr>
                        <td>ID</td>
                        <td><input name="user_id" /></td>
                        <td>이름</td>
                        <td><input name="user_nm" /></td>          
                        <td>대리점명</td>
                        <td><input name="agent_nm" /></td>                            
                        <td>지급ID명</td>
                        <td><input name="merch_nm" /></td>    
                        <td>UID명</td>
                        <td><input name="onfftid_nm" /></td>                            
                        <td><input type="button" name="user_search" value="조회"/></td>
                        <c:if test="${userFormBean.ses_user_cate == '00'}">
                            <td><input type="button" name="user_insert" value="등록"/></td>
                        </c:if>
                        <!--<td><input type="button" name="mcode_search" value="엑셀"/></td>-->
                    </tr>                   
               </table>
            </form>
            <div class="paging">
                <div id="userPaging" style="width: 50%;"></div>
                <div id="userrecinfoArea" style="width: 50%;"></div>
            </div>                                        
        </div>
        <div id="user" style="position: relative;width:100%;height:100%;background-color:white;"></div>
        
        
         <script type="text/javascript">
            
            var usergrid={};
            var userWins;
            var userupdateWins;
            
            $(document).ready(function () {
                
                userWins = new dhtmlXWindows();
                userupdateWins = new dhtmlXWindows();

                userWins.attachViewportTo("user_search");
                
                var user_searchForm = document.getElementById("user_search");
                var tabBarId = tabbar.getActiveTab();
                    
                user_layout = tabbar.cells(tabBarId).attachLayout("2E");
                     
                user_layout.cells('a').hideHeader();
                user_layout.cells('b').hideHeader();                 
                user_layout.cells('a').attachObject(user_searchForm);
                user_layout.cells('a').setHeight(80);
                
                usergrid = user_layout.cells('b').attachGrid();
                
                usergrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                
                var mcheaders = "";
                mcheaders += "<center>NO</center>,<center>ID</center>,<center>이름</center>,<center>권한그룹</center>,<center>사용자 유형</center>,<center>대리점명</center>,<center>지급ID명</center>,<center>UID명</center>,<center>연락처1</center>,<center>연락처2</center>,";
                mcheaders += "<center>이메일</center>,<center>등록일</center>";
                usergrid.setHeader(mcheaders);
                usergrid.setColAlign("center,center,center,center,center,center,center,center,center,center,center,center");
                usergrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt");
                usergrid.setInitWidths("70,100,100,100,100,100,100,100,100,100,150,100,100");
                usergrid.setColSorting("str,str,str,str,str,str,str,str,str,str,str,str");
                usergrid.enableColumnMove(true);
                usergrid.setSkin("dhx_skyblue");
                
                usergrid.setColumnHidden(3,true);   
                
                usergrid.enablePaging(true,Number($("#userForm input[name=page_size]").val()),10,"userPaging",true,"userrecinfoArea");
                usergrid.setPagingSkin("bricks");    
                
                usergrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        if(lInd !==0){
                          $("#userForm input[name=page_no]").val(ind);             
                           usergrid.clearAll();
                           userSearch();
                        }else{
                            $("#userForm input[name=page_no]").val(1);
                            usergrid.clearAll();
                            userSearch();
                        }                          
                    
                });                        
                
                usergrid.init();

                usergrid.parse(${userMasterList}, "json");
  
                usergrid.attachEvent("onRowDblClicked", user_attach);

                //회원 정보 검색 이벤트
                $("#userForm input[name=user_search]").click(function () {
                    /*          
                    $("input[name=page_no]").val("1");
                                userSearch();
                    */            
                    usergrid.changePage(1);
                    return false;
                });
                
                $("#userForm input[name=user_insert]").click(function () {
                    PopUserInsert();
                });

            });
            
            function user_attach(rowid, col){         
                var dhtmlxwindowSize = {};
                dhtmlxwindowSize.width = 480;
                dhtmlxwindowSize.height = 375;

               var location = dhxwindowCenterAlgin(dhtmlxwindowSize);
               var user_seq = usergrid.getUserData(rowid, 'user_seq');

               userupdatew1 = userupdateWins.createWindow("userupdatew1",  location.x, location.y,   dhtmlxwindowSize.width, dhtmlxwindowSize.height);
               userupdatew1.setText("회원 수정");
               userupdateWins.window('userupdatew1').setModal(true);
               userupdatew1.attachURL("<c:url value="/user/userMasterUpdate" />"+"?user_seq="+user_seq, true);
            }
               
            function userSearch() {
                $.ajax({
                    url: $("#userForm").attr("action"),
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#userForm").serialize(),
                    success: function (data) {
                        usergrid.clearAll();
                        usergrid.parse(jQuery.parseJSON(data), "json");
                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }
              
            function PopUserInsert() {
                 var dhtmlxwindowSize = {};
                 dhtmlxwindowSize.width = 480;
                 dhtmlxwindowSize.height = 335;

                 var location = dhxwindowCenterAlgin(dhtmlxwindowSize);

                 userw1 = userWins.createWindow("userw1", location.x, location.y,   dhtmlxwindowSize.width, dhtmlxwindowSize.height);
                 userw1.setText("회원등록");
                 userWins.window('userw1').setModal(true);
                 userw1.attachURL("<c:url value="/user/userMasterInsert" />", true); 
            }
  
        </script>
    </body>
</html>
