<%-- 
    Document   : creditMaster
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>입금대사</title>
        </head>
        <body>
</c:if>

    <script type="text/javascript">
        
                var main_compdepositMaster_layout={};
                $(document).ready(function () {

                    var tabBarId = tabbar.getActiveTab();

                    main_compdepositMaster_layout = tabbar.cells(tabBarId).attachLayout("2E");

                    main_compdepositMaster_layout.cells('a').hideHeader();
                    main_compdepositMaster_layout.cells('b').hideHeader();
                    main_compdepositMaster_layout.cells('a').attachURL("<c:url value="/compdeposit/compdepositMasterList" />",true);           
//                    main_compdepositMaster_layout.cells('b').attachURL("<c:url value="/compdeposit/compdepositDetailList" />",true);  
            });
             
            
    </script>

    </body>
</html>
