<%-- 
    Document   : compdepositMasterList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>입금대사 메인</title>             
        </head>
        <body>
</c:if>
       
    <body>
        <div class="right" id="compdepositMaster_search" style="width:100%;">       
            <form id="compdepositMasterForm" method="POST" onsubmit="return false" action="<c:url value="/compdeposit/compdepositMasterList" />" modelAttribute="compdepositFormBean"> 
                <table>
                    <tr>
                        <td>
                            <div class="label">입금일자</div>
                            <div class="input">
                                <input type="text" name="deposit_start_dt" id="compdepositMaster_from_date" value ="${compdepositFormBean.deposit_start_dt}" onclick="" /> ~
                                <input type="text" name="deposit_end_dt" id="compdepositMaster_to_date" value ="${compdepositFormBean.deposit_end_dt}" onclick="" />
                            </div>
                            <div class="label">승인일자</div>
                            <div class="input">
                                <input type="text" name="app_start_dt" id="appMaster_from_date" value ="${compdepositFormBean.app_start_dt}" onclick="" /> ~
                                <input type="text" name="app_end_dt" id="appMaster_to_date" value ="${compdepositFormBean.app_end_dt}" onclick="" />
                            </div>                            
                            <div class="label2">가맹점번호구분</div>
                            <div class="input">
                                <select name="pay_mtd">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${midPayMtdList.tb_code_details}">
                                        <option value="${code.detail_code}">${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="label">가맹점번호</div>
                            <div class="input"><input type="text" name="merch_no" /></div> 
                        </td>
                        <td><input type="button" name="compdepositMaster_search" value="조회"/></td>
                        <td><input type="button" name="compdepositMaster_excel" value="엑셀"/></td>
                        <td><input type="button" name="init" value="검색조건삭제"/></td>
                    </tr>
               </table>
            </form>
        </div>
        <div id="compdepositMaster" style="position: relative;width:100%;height:100%;background-color:white;"></div>
        
         <script type="text/javascript">
            
            var compdepositMastergrid={};
            var compdepositMasterCalendar;
            var outamtWindows={};
            var depositMetabolismWindow;
            
            $(document).ready(function () {
                
                var compdepositMaster_searchForm = document.getElementById("compdepositMaster_search");
                
                var tabBarId = tabbar.getActiveTab();
                compdepositMaster_layout = tabbar.cells(tabBarId).attachLayout("2E");
                     
                compdepositMaster_layout.cells('a').hideHeader();
                compdepositMaster_layout.cells('b').hideHeader();
                 
                compdepositMaster_layout.cells('a').attachObject(compdepositMaster_searchForm);
                compdepositMaster_layout.cells('a').setHeight(20);
                
                compdepositMastergrid = compdepositMaster_layout.cells('b').attachGrid();
                
                compdepositMastergrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                
                var compdepositMasterHeaders = "";
                compdepositMasterHeaders += "<center>입금일</center>,<center>승인일</center>,<center>가맹점번호구분</center>,<center>가맹점번호</center>,<center>매입확정액차이</center>,<center>매입확정금액</center>,<center>입금예정액</center>,<center>수수료</center>";
                compdepositMasterHeaders += ",<center>총거래금액</center>,<center>승인거래액</center>,<center>취소거래액</center>";
                
                compdepositMastergrid.setHeader(compdepositMasterHeaders);
                compdepositMastergrid.setColAlign("center,center,center,center,right,right,right,right,right,right,right");
                compdepositMastergrid.setColTypes("txt,txt,txt,txt,edn,edn,edn,edn,edn,edn,edn");
                compdepositMastergrid.setInitWidths("100,100,100,100,100,100,100,100,100,100,100");
                compdepositMastergrid.setColSorting("str,str,str,str,int,int,int,int,int,int,int");
                compdepositMastergrid.setNumberFormat("0,000",4);
                compdepositMastergrid.setNumberFormat("0,000",5);
                compdepositMastergrid.setNumberFormat("0,000",6);
                compdepositMastergrid.setNumberFormat("0,000",7);
                compdepositMastergrid.setNumberFormat("0,000",8);
                compdepositMastergrid.setNumberFormat("0,000",9);
                compdepositMastergrid.setNumberFormat("0,000",10);
//                compdepositMastergrid.setNumberFormat("0,000",11);
                compdepositMastergrid.enableColumnMove(true);
                compdepositMastergrid.setSkin("dhx_skyblue");
                compdepositMastergrid.init();
                compdepositMastergrid.attachEvent("onRowDblClicked", compdepositMaster_attach);
                compdepositMastergrid.parse(${ls_compdepositMasterList},"json");
                
                depositMetabolismWindow = new dhtmlXWindows();
                
                //검색조건 초기화
                $("#compdepositMasterForm input[name=init]").click(function () {

                    compdepositMasterInit($("#compdepositMasterForm"));

                });  
                
                $("#compdepositMasterForm input[name=week]").click(function(){
                    compDeposit_date_search("week");
                });

                $("#compdepositMasterForm input[name=month]").click(function(){
                    compDeposit_date_search("month");
                });


                //검색 이벤트
                $("#compdepositMasterForm input[name=compdepositMaster_search]").click(function () {
                    compdepositMasterSearch();
                });
                
                //엑셀
                $("#compdepositMasterForm input[name=compdepositMaster_excel]").click(function () {
                    $("#compdepositMasterForm").attr("action","<c:url value="/compdeposit/compdepositMasterListExcel" />");
                    document.getElementById("compdepositMasterForm").submit();
                });                
                
                compdepositMasterCalendar = new dhtmlXCalendarObject(["compdepositMaster_from_date","compdepositMaster_to_date","appMaster_from_date","appMaster_to_date"]);
                
                    
            });
            
            function compdepositMasterInit($form) {
                searchFormInit($form);
                compDeposit_date_search("week");                    
            } 
            
            //상세조회 이벤트
            function compdepositMaster_attach(rowid, col) {

              var deposit_dt = compdepositMastergrid.getUserData(rowid, 'deposit_dt');
              var merch_no = compdepositMastergrid.getUserData(rowid, 'merch_no');
              
              main_compdepositMaster_layout.cells('b').attachURL("<c:url value="/compdeposit/compdepositDetailList" />"+"?deposit_dt=" + deposit_dt + "&merch_no="+merch_no+"&page_size=100", true);
              
            }
            
            //검색
            function compdepositMasterSearch() {
                
                $.ajax({
                    url: "<c:url value="/compdeposit/compdepositMasterList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#compdepositMasterForm").serialize(),
                    success: function (data) {
                        
                        var jsonData = $.parseJSON(data);
                        compdepositMastergrid.clearAll();
                        compdepositMastergrid.parse(jsonData,"json");
                        
                        $("button.procButton").button({
                            text:true
                        })
                        .unbind("click")
                        .bind("click", function (e) {

                            if (confirm("입금확정을 하시겠습니까?") == true){
                                var rowId = getRowId($(this));

                                depositMetabolismConfirmProcess(rowId);
                            }

                        });


                        $("button.procCancelButton").button({
                            text:true
                        })
                        .unbind("click")
                        .bind("click", function (e) {

                            if (confirm("확정취소를 하시겠습니까?") == true){
                                var rowId = getRowId($(this));

                                depositMetabolismCancelProcess(rowId);
                            }

                        });


                        $("span.outAmt,span.outAmtProc").click(function(){

                            var rowId = getRowId($(this));

                            depositMetabolismOutAmtWindowPop(rowId);

                        });

                        $("span.misAmt").click(function(){

                            var rowId = getRowId($(this));

                            depositMetabolismMisAmtWindowPop(rowId);

                        });                                 
                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }
            
            function compDeposit_date_search(day){
                var oneDate = 1000 * 3600 * 24; 
                var now = new Date();
                var week = new Date(now.getTime() + (oneDate*-6));
                var month = new Date(now.getTime() + (oneDate*-31));
                var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                var date=new Date();
                if(day=="today"){
                    this.$("input[id=compdepositMaster_from_date]").val(nowTime);
                    this.$("input[id=compdepositMaster_to_date]").val(nowTime);
                }else if(day=="week"){
                    this.$("input[id=compdepositMaster_from_date]").val(weekTime);
                    this.$("input[id=compdepositMaster_to_date]").val(nowTime);
                }else{
                    this.$("input[id=compdepositMaster_from_date]").val(monthTime);
                    this.$("input[id=compdepositMaster_to_date]").val(nowTime);
                }
            }
            
            function compDepositListsetSens(id, k) {
                // update range
                if (k == "min") {
                    compdepositMasterCalendar.setSensitiveRange(byId(id).value, null);
                } else {
                    compdepositMasterCalendar.setSensitiveRange(null, byId(id).value);
                }
            }

            function byId(id) {
                return document.getElementById(id);
            }              
            
            function depositMetabolismOutAmtWindowPop(rowId){

                var dhtmlxwindowSize = {};
                dhtmlxwindowSize.width = 1000;
                dhtmlxwindowSize.height = 300;

                var pay_mtd = compdepositMastergrid.getUserData(rowId,"pay_mtd");
                var merch_no = compdepositMastergrid.getUserData(rowId,"merch_no");
                var deposit_dt = compdepositMastergrid.getUserData(rowId,"deposit_dt");

                var location = dhxwindowCenterAlgin(dhtmlxwindowSize);

                outamtWindows = depositMetabolismWindow.createWindow("outamtListWindow", location.x, location.y, dhtmlxwindowSize.width, dhtmlxwindowSize.height);

                outamtWindows.setText("미수금 리스트");

                depositMetabolismWindow.window("outamtListWindow").setModal(true);

                outamtWindows.attachURL("<c:url value="/outamt/outamtDetailListWindow" />" + "?pay_mtd="+pay_mtd+"&merch_no="+merch_no+"&gen_dt="+deposit_dt+"&deposit_popup_yn=Y",true);

            }
            
            function depositMetabolismMisAmtWindowPop(rowId){
                
                var dhtmlxwindowSize = {};
                dhtmlxwindowSize.width = 950;
                dhtmlxwindowSize.height = 250;

                var pay_mtd = compdepositMastergrid.getUserData(rowId,"pay_mtd");
                var merch_no = compdepositMastergrid.getUserData(rowId,"merch_no");
                var deposit_dt = compdepositMastergrid.getUserData(rowId,"deposit_dt");

                var location = dhxwindowCenterAlgin(dhtmlxwindowSize);

                outamtWindows = depositMetabolismWindow.createWindow("outamtListWindow", location.x, location.y, dhtmlxwindowSize.width, dhtmlxwindowSize.height);

                outamtWindows.setText("미수금 등록");

                depositMetabolismWindow.window("outamtListWindow").setModal(true);

                outamtWindows.attachURL("<c:url value="/outamt/outamtMasterInsert" />" + "?pay_mtd="+pay_mtd+"&merch_no="+merch_no+"&gen_dt="+deposit_dt+"&deposit_popup_yn=Y",true);

            }                      

            function depositMetabolismConfirmProcess(rowId){
                
                var deposit_object ={};
                deposit_object.seq = compdepositMastergrid.getUserData(rowId,"seq");
                deposit_object.deposit_dt = compdepositMastergrid.getUserData(rowId,"deposit_dt");
                deposit_object.merch_no = compdepositMastergrid.getUserData(rowId,"merch_no");
                deposit_object.proc_flag = compdepositMastergrid.getUserData(rowId,"proc_flag");                
                
                $.ajax({
                    url : "<c:url value="/compdeposit/depositMetabolismConfirmProcess" />",
                    type : "POST",
                    async : true,
                    dataType : "json",
                    data: {
                             seq:deposit_object.seq
                            ,deposit_dt:deposit_object.deposit_dt
                            ,merch_no:deposit_object.merch_no
                            ,proc_flag:deposit_object.proc_flag
                    },
                    success : function(data) {
                        if(data ==="success"){
                            alert("입금확정 성공");

                            compdepositMastergrid.clearAll();
                            compdepositMasterSearch();

                        }else{
                            alert("입금확정 실패");
                        }
                    },
                    error : function(jqXHR, textStatus, errorThrown) {
                        alert("입금확정 실패 : " + errorThrown);

                    }
                });

            }

            function depositMetabolismCancelProcess(rowId){
            
                var deposit_object ={};
                deposit_object.seq = compdepositMastergrid.getUserData(rowId,"seq");
                deposit_object.deposit_dt = compdepositMastergrid.getUserData(rowId,"deposit_dt");
                deposit_object.merch_no = compdepositMastergrid.getUserData(rowId,"merch_no");
                deposit_object.proc_flag = compdepositMastergrid.getUserData(rowId,"proc_flag");                

                $.ajax({
                    url : "<c:url value="/compdeposit/depositMetabolismCancelProcess" />",
                    type : "POST",
                    async : true,
                    dataType : "json",
                    data: {
                            seq:deposit_object.seq
                           ,deposit_dt:deposit_object.deposit_dt
                           ,merch_no:deposit_object.merch_no
                           ,proc_flag:deposit_object.proc_flag
                    },
                    success : function(data) {
                        if(data ==="success"){
                            alert("확정취소 성공");
                            compdepositMastergrid.clearAll();
                            compdepositMasterSearch();
                        }else{
                            alert("확정취소 실패");
                        }
                    },
                    error : function(jqXHR, textStatus, errorThrown) {
                        alert("확정취소 실패 : " + errorThrown);

                    }
                });

            }         
            
            function getRowId($tag){
                var rowId = 0;

                if(compdepositMastergrid.pagingOn){
                    rowId =Number((compdepositMastergrid.currentPage -1) * Number(compdepositMastergrid.rowsBufferOutSize))+Number($tag.parent().parent().index()-1);
                }else{
                    rowId = Number($tag.parent().parent().index()-1);
                }
                return rowId;
            }            
                
        </script>
    </body>
</html>
