<%-- 
    Document   : codeDetailMaster
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>대리점 지급 상세조회</title>
        </head>
        <body>
</c:if>

    <script type="text/javascript">

                var agentpaydetailgrid={};
                var dhxSelPopupWins;

                $(document).ready(function () {
                    
                    $("#agentpaydetailForm select[name=pay_yyyy]").append(Common.yyList('${agentPayFormBean.pay_yyyy}',2015,agentpay_date_search()));

                    $("#agentpaydetailForm select[name=pay_mm]").append(Common.mmList('${agentPayFormBean.pay_mm}'));                

                    dhxSelPopupWins = new dhtmlXWindows();                    

                    var agentpaydetailForm = document.getElementById("agentpaydetail_search");

                    agentpaydetail_layout = main_agentpay_layout.cells('b').attachLayout("2E");

                    agentpaydetail_layout.cells('a').hideHeader();

                    agentpaydetail_layout.cells('b').hideHeader();

                    agentpaydetail_layout.cells('a').attachObject(agentpaydetailForm);

                    agentpaydetail_layout.cells('a').setHeight(30);

                    agentpaydetailgrid = agentpaydetail_layout.cells('b').attachGrid();

                    agentpaydetailgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");

                    var mcheaders = "";
                    /*
                    mcheaders += "<center>정산 월</center>,<center>대리점</center>,<center>지급ID</center>,<center>지급예정액</center>,<center>부가세</center>,<center>지급대상액</center>,<center>수수료</center>,<center>승인금액</center>,<center>취소금액</center>,<center>승인건수</center>,<center>취소건수</center>";
                    agentpaydetailgrid.setHeader(mcheaders);
                    agentpaydetailgrid.setColAlign("center,center,center,right,right,right,right,right,right,right,right");
                    agentpaydetailgrid.setColTypes("txt,txt,txt,edn,edn,edn,edn,edn,edn,edn,edn");
                    agentpaydetailgrid.setInitWidths("100,100,100,100,100,100,100,100,100,100,100");
                    agentpaydetailgrid.setColSorting("str,str,str,int,int,int,int,int,int,int,int");
                    */
                    mcheaders += "<center>정산 월</center>,<center>대리점</center>,<center>지급ID</center>,<center>지급예정액</center>,<center>부가세</center>,<center>지급대상액</center>,<center>승인금액</center>,<center>취소금액</center>,<center>승인건수</center>,<center>취소건수</center>";
                    agentpaydetailgrid.setHeader(mcheaders);
                    agentpaydetailgrid.setColAlign("center,center,center,right,right,right,right,right,right,right");
                    agentpaydetailgrid.setColTypes("txt,txt,txt,edn,edn,edn,edn,edn,edn,edn");
                    agentpaydetailgrid.setInitWidths("100,100,100,100,100,100,100,100,100,100");
                    agentpaydetailgrid.setColSorting("str,str,str,int,int,int,int,int,int,int");
                   
                    agentpaydetailgrid.setNumberFormat("0,000",3);
                    agentpaydetailgrid.setNumberFormat("0,000",4);
                    agentpaydetailgrid.setNumberFormat("0,000",5);
                    agentpaydetailgrid.setNumberFormat("0,000",6);
                    agentpaydetailgrid.setNumberFormat("0,000",7);
                    agentpaydetailgrid.setNumberFormat("0,000",8);
                    agentpaydetailgrid.setNumberFormat("0,000",9);
                    //agentpaydetailgrid.setNumberFormat("0,000",10);
                    
                    agentpaydetailgrid.enableColumnMove(true);
                    agentpaydetailgrid.setSkin("dhx_skyblue");

                    agentpaydetailgrid.init();

                    agentpaydetailgrid.parse(${ls_agentpayDetailList}, "json");

                    //상세검색
                    $("#agentpaydetailForm input[name=agentpaydetail_search]").click(function(){

                        agentpaydetailgrid.clearAll();

                        agentpaydetailSearch();

                    });
                    
                    //엑셀 이벤트
                     $("#agentpaydetailForm input[name=agentpaydetail_excel]").click(function () {
                         
                        $("#agentpaydetailForm").attr("action","<c:url value="/agentpay/agentpayDetailListExcel" />");
                        document.getElementById("agentpaydetailForm").submit();
                        
                    });                    
                    
                    //대리점 검색 이벤트
                     $("#agentpaydetailForm input[name=agent_nm]").click(function () {
                        agentpayAgentSelectWindow();
                    });          
                    
                    //지급ID 검색 이벤트
                     $("#agentpaydetailForm input[name=merch_nm]").click(function () {
                        agentpayOnffmerchSelectWindow();
                    });                         
                    
                });

                // 정보 조회
                function agentpaydetailSearch() {
                
                $.ajax({
                    url: "<c:url value="/agentpay/agentpayDetailList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#agentpaydetailForm").serialize(),
                    success: function (data) {

                        var jsonData = $.parseJSON(data);

                        agentpaydetailgrid.parse(jsonData,"json");

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
                    return false;
            }
            
            function agentpayAgentSelectWindow() {
            
                onoffObject.agent_seq = $("#agentpaydetailForm input[name=agent_seq]");
                onoffObject.agent_nm = $("#agentpaydetailForm input[name=agent_nm]");
                //대리점정보 팝업
                w2 = dhxSelPopupWins.createWindow("AgentSelectPopUp", 20, 30, 640, 480);
                w2.setText("대리점 선택페이지");
                dhxSelPopupWins.window('AgentSelectPopUp').setModal(true);
                dhxSelPopupWins.window('AgentSelectPopUp').denyResize();
                w2.attachURL('<c:url value = "/popup/AgentSelectPopUp"/>', true);   
                
            }              
            
            function agentpayOnffmerchSelectWindow() {
            
                onoffObject.onffmerch_no = $("#agentpaydetailForm input[name=onffmerch_no]");
                onoffObject.merch_nm = $("#agentpaydetailForm input[name=merch_nm]");
                //지급ID정보 팝업
                w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                w2.setText("지급ID 선택페이지");
                dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                
            }                     
            
            function agentpay_date_search(){
            
                var now = new Date();
                var nowYear = now.getFullYear();
                return nowYear;
            
            }                  
                
    </script>
    <body>


        <div class="right" id="agentpaydetail_search" style="width:100%;">
             <form id="agentpaydetailForm" method="POST" action="<c:url value="/agentpay/agentpayDetailList" />" modelAttribute="agentPayFormBean">

                        <table><tr>
                        <td>정산대상년</td>
                            <td>
                                <select id="pay_yyyy"name="pay_yyyy">
                                </select>
                            </td>  
                            <td>정산대상월</td>
                            <td>
                                <select id="pay_mm"name="pay_mm">
                                 </select>
                            </td>                              
                            <td >대리점</td>
                            <td>
                                <input type="hidden" name="agent_seq" />
                                <input type="text" name="agent_nm" />
                            </td>  
                            <td >지급ID</td>
                            <td>
                                <input type="hidden" name="onffmerch_no" />
                                <input type="text" name="merch_nm" />
                            </td>                              
                                <td rowspan='2'>
                                    <input type="button" name="agentpaydetail_search" value="조회"/>
                                </td>
                                <td rowspan='2'>
                                    <input type="button" name="agentpaydetail_excel" value="엑셀"/>
                                </td>
                            </tr>
                        </table>
             </form>

         </div>

    </body>
</html>
