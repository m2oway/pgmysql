<%-- 
    Document   : codeMaster
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>대리점정산 메인</title>             
        </head>
        <body>
</c:if>
       
    <body>
        <!--<h1>신용거래</h1>-->
        <div class="right" id="agentpay_search" style="width:100%;">       
            <form id="agentpayForm" method="POST" onsubmit="return false" action="<c:url value="/agentpay/agentpayMasterList" />" modelAttribute="agentPayFormBean"> 
                <table>
                    <tr>
                        <td>정산대상년</td>
                            <td>
                                <select id="pay_yyyy"name="pay_yyyy">
                                </select>
                            </td>  
                            <td>정산대상월</td>
                            <td>
                                <select id="pay_mm"name="pay_mm">
                                 </select>
                            </td>                              
                            <td >대리점</td>
                            <td>
                                <input type="hidden" name="agent_seq" />
                                <input type="text" name="agent_nm" />
                            </td> 
                            <td><input type="button" name="agentpay_search" value="조회"/></td>
                            <td><input type="button" name="agentpay_excel" value="엑셀"/></td>
                    </tr>
               </table>
            </form>
        </div>

         <script type="text/javascript">
            
            var agentpaygrid={};
            
            var dhxSelPopupWins;
            
            $(document).ready(function () {
                
                $("#agentpayForm select[name=pay_yyyy]").append(Common.yyList('${agentPayFormBean.pay_yyyy}',2015,agentpay_date_search()));
                
                $("#agentpayForm select[name=pay_mm]").append(Common.mmList('${agentPayFormBean.pay_mm}'));                
                
                dhxSelPopupWins = new dhtmlXWindows();
                
                var agentpay_searchForm = document.getElementById("agentpay_search");
                
                agentpay_layout = main_agentpay_layout.cells('a').attachLayout("2E");
                     
                agentpay_layout.cells('a').hideHeader();
                agentpay_layout.cells('b').hideHeader();
                 
                agentpay_layout.cells('a').attachObject(agentpay_searchForm);
                agentpay_layout.cells('a').setHeight(20);
                
                agentpaygrid = agentpay_layout.cells('b').attachGrid();
                
                agentpaygrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                
                    var mcheaders = "";
                    //mcheaders += "<center>정산 월</center>,<center>대리점</center>,<center>입금통장</center>,<center>예금주</center>,<center>지급예정액</center>,<center>부가세</center>,<center>지급대상액</center>,<center>수수료</center>,<center>승인금액</center>,<center>취소금액</center>";
                    mcheaders += "<center>정산 월</center>,<center>대리점</center>,<center>입금통장</center>,<center>예금주</center>,<center>지급예정액</center>,<center>부가세</center>,<center>지급대상액</center>,<center>승인금액</center>,<center>취소금액</center>";
                    agentpaygrid.setHeader(mcheaders);
                    /*
                    agentpaygrid.setColAlign("center,center,center,center,right,right,right,right,right,right");
                    agentpaygrid.setColTypes("txt,txt,txt,txt,edn,edn,edn,edn,edn,edn");
                    agentpaygrid.setInitWidths("100,100,100,100,100,100,100,100,100,100");
                    agentpaygrid.setColSorting("str,str,str,str,int,int,int,int,int,int");
                    */
                    agentpaygrid.setColAlign("center,center,center,center,right,right,right,right,right");
                    agentpaygrid.setColTypes("txt,txt,txt,txt,edn,edn,edn,edn,edn");
                    agentpaygrid.setInitWidths("100,100,100,100,100,100,100,100,100");
                    agentpaygrid.setColSorting("str,str,str,str,int,int,int,int,int");                    
                    
                    agentpaygrid.setNumberFormat("0,000",4);
                    agentpaygrid.setNumberFormat("0,000",5);
                    agentpaygrid.setNumberFormat("0,000",6);
                    agentpaygrid.setNumberFormat("0,000",7);
                    agentpaygrid.setNumberFormat("0,000",8);
                    //agentpaygrid.setNumberFormat("0,000",9);
                    agentpaygrid.enableColumnMove(true);
                    agentpaygrid.setSkin("dhx_skyblue");
                    agentpaygrid.init();
                    
                    var mfooters = "총계,#cspan,#cspan,#cspan";
                    //mfooters += ",<div id='agentpay_master4'>0</div>,<div id='agentpay_master5'>0</div>,<div id='agentpay_master6'>0</div>,<div id='agentpay_master7'>0</div>,<div id='agentpay_master8'>0</div>,<div id='agentpay_master9'>0</div>";
                    mfooters += ",<div id='agentpay_master4'>0</div>,<div id='agentpay_master5'>0</div>,<div id='agentpay_master6'>0</div>,<div id='agentpay_master7'>0</div>,<div id='agentpay_master8'>0</div>";
                    agentpaygrid.attachFooter(mfooters);

                    agentpaygrid.parse(${ls_agentpayMasterList}, agentpayFooterValue,"json");

                    agentpaygrid.attachEvent("onRowDblClicked", agentpay_attach);

                    //검색 이벤트
                     $("#agentpayForm input[name=agentpay_search]").click(function () {
                        agentpaySearch();
                    });
                    
                    //엑셀 이벤트
                     $("#agentpayForm input[name=agentpay_excel]").click(function () {
                         
                        $("#agentpayForm").attr("action","<c:url value="/agentpay/agentpayMasterListExcel" />");
                        document.getElementById("agentpayForm").submit();
                        
                    });                    
                    
                    //대리점 검색 이벤트
                     $("#agentpayForm input[name=agent_nm]").click(function () {
                        agentpayAgentSelectWindow();
                    });                    

            });
            
            function agentpayFooterValue() {
                
		    var agentpay_master4 = document.getElementById("agentpay_master4");
		    var agentpay_master5 = document.getElementById("agentpay_master5");
		    var agentpay_master6 = document.getElementById("agentpay_master6");
		    var agentpay_master7 = document.getElementById("agentpay_master7");
                    var agentpay_master8 = document.getElementById("agentpay_master8");
                    //var agentpay_master9 = document.getElementById("agentpay_master9");
		    
		    agentpay_master4.innerHTML =  putComma(sumColumn(4)) ;
		    agentpay_master5.innerHTML =  putComma(sumColumn(5)) ;
		    agentpay_master6.innerHTML =  putComma(sumColumn(6)) ;
		    agentpay_master7.innerHTML =  putComma(sumColumn(7)) ;
                    agentpay_master8.innerHTML =  putComma(sumColumn(8)) ;
                    //agentpay_master9.innerHTML =  putComma(sumColumn(9)) ;
		    
		    return true;
            }
		
            function sumColumn(ind) {
                var out = 0;
                for (var i = 0; i < agentpaygrid.getRowsNum(); i++) {
                     out += parseFloat(agentpaygrid.cells2(i, ind).getValue());
                }

                return out;
            }
            
            //대리점 지급 상세조회 이벤트
            function agentpay_attach(rowid, col) {

              var pay_yyyymm = agentpaygrid.getUserData(rowid, 'pay_yyyymm');
              var pay_yyyy = agentpaygrid.getUserData(rowid, 'pay_yyyy');
              var pay_mm = agentpaygrid.getUserData(rowid, 'pay_mm');
              var agent_seq = agentpaygrid.getUserData(rowid, 'agent_seq');

                main_agentpay_layout.cells('b').attachURL("<c:url value="/agentpay/agentpayDetailList" />"+"?pay_yyyymm=" + pay_yyyymm + "&agent_seq=" + agent_seq + "&pay_yyyy=" + pay_yyyy + "&pay_mm=" + pay_mm, true);
            }
            
            //대리점 지급 정보 조회
            function agentpaySearch() {
                
                $.ajax({
                    url: "<c:url value="/agentpay/agentpayMasterList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#agentpayForm").serialize(),
                    success: function (data) {

                        agentpaygrid.clearAll();
                        agentpaygrid.parse($.parseJSON(data),agentpayFooterValue, "json");
                                
                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }

            function agentpayAgentSelectWindow() {
            
                onoffObject.agent_seq = $("#agentpayForm input[name=agent_seq]");
                onoffObject.agent_nm = $("#agentpayForm input[name=agent_nm]");
                //대리점정보 팝업
                w2 = dhxSelPopupWins.createWindow("AgentSelectPopUp", 20, 30, 640, 480);
                w2.setText("대리점 선택페이지");
                dhxSelPopupWins.window('AgentSelectPopUp').setModal(true);
                dhxSelPopupWins.window('AgentSelectPopUp').denyResize();
                w2.attachURL('<c:url value = "/popup/AgentSelectPopUp"/>', true);   
                
            }                 
            
            function agentpay_date_search(){
            
                var now = new Date();
                var nowYear = now.getFullYear();
                return nowYear;
            
            }                     
                
        </script>
    </body>
</html>
