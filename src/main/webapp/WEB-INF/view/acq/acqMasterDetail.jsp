<%-- 
    Document   : codeDetailMaster
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>신용카드 상세조회</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">

                var acqmistransdetailgrid={};
                var acqmistransdetail_layout={};
                var acqmistransdetailCalendar;
                var dhxSelPopupWins=new dhtmlXWindows();
                $(document).ready(function () {
                    
                    acqmistransdetailCalendar = new dhtmlXCalendarObject(["view_transForm_date_start","view_transForm_date_end"]);    

                    var acqhdetailForm = document.getElementById("acqmistransdetail_search");
                    acqmistransdetail_layout = acq_master_layout.cells('b').attachLayout("2E");
                    acqmistransdetail_layout.cells('a').hideHeader();
                    acqmistransdetail_layout.cells('b').hideHeader();
                    acqmistransdetail_layout.cells('a').attachObject(acqhdetailForm);
                    acqmistransdetail_layout.cells('a').setHeight(75);
                    acqmistransdetailgrid = acqmistransdetail_layout.cells('b').attachGrid();
                    acqmistransdetailgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");

                    var acqmistransdetailheaders = "";
                    /*
                    acqmistransdetailheaders +=  "<center>NO</center>,<center>가맹점명</center>,<center>UID명</center>,<center>고객명</center>,<center>고객전화번호</center>";
                    acqmistransdetailheaders += ",<center>상품명</center>,<center>거래종류</center>,<center>카드사</center>,<center>카드번호</center>,<center>할부</center>";
                    acqmistransdetailheaders += ",<center>승인금액</center>,<center>비과세금액</center>,<center>부가세</center>,<center>승인번호</center>,<center>승인일시</center>";
                    acqmistransdetailheaders += ",<center>취소일</center>,<center>거래상태</center>,<center>거래결과</center>,<center>민원</center>,<center>영수증</center>";
                    acqmistransdetailheaders += ",<center>취소</center>,<center>결제채널</center>,<center>결과코드</center>,<center>결과메시지</center>,<center>TID</center>";
                    acqmistransdetailheaders += ",<center>가맹점번호</center>,<center>매입사</center>,<center>수수료</center>,<center>입금액</center>,<center>ONOFF수수료</center>";
                    acqmistransdetailheaders += ",<center>ONOFF지급액</center>,<center>지급예정일</center>,<center>거래일련번호</center>,<center>결제IP</center>,<center>결제자ID</center>";
                    */
                    acqmistransdetailheaders +=  "<center>NO</center>,<center>지급ID명</center>,<center>UID명</center>,<center>고객명</center>,<center>고객전화번호</center>";
                    acqmistransdetailheaders += ",<center>상품명</center>,<center>거래종류</center>,<center>카드사</center>,<center>카드번호</center>,<center>할부</center>";
                    acqmistransdetailheaders += ",<center>승인금액</center>,<center>비과세금액</center>,<center>부가세</center>,<center>승인번호</center>,<center>승인일시</center>";
                    acqmistransdetailheaders += ",<center>취소일</center>,<center>거래상태</center>,<center>거래결과</center>,<center>민원</center>,<center>영수증</center>";
                    acqmistransdetailheaders += ",<center>취소</center>,<center>결제채널</center>,<center>결과코드</center>,<center>결과메시지</center>,<center>TID</center>";
                    acqmistransdetailheaders += ",<center>가맹점번호</center>,<center>매입사</center>,<center>수수료</center>,<center>입금액</center>,<center>ONOFF수수료</center>,<center>VAT</center>,<center>원천징수</center>";
                    acqmistransdetailheaders += ",<center>ONOFF지급액</center>,<center>지급예정일</center>,<center>가맹점지급예정일</center>,<center>거래일련번호</center>,<center>결제IP</center>,<center>결제자ID</center>";                    
                    
                    acqmistransdetailgrid.setHeader(acqmistransdetailheaders);
                    var acqmistransdetailColAlign = "";
                    /*
                    acqmistransdetailColAlign +=  "center,center,center,center,center";
                    acqmistransdetailColAlign += ",center,center,center,center,center";
                    acqmistransdetailColAlign += ",right,right,right,center,center";
                    acqmistransdetailColAlign += ",center,center,center,center,center";
                    acqmistransdetailColAlign += ",center,center,center,center,center";
                    acqmistransdetailColAlign += ",center,center,right,right,right,right";
                    acqmistransdetailColAlign += ",right,center,center,center,center";
                    */
                   
                    acqmistransdetailColAlign +=  "center,center,center,center,center";
                    acqmistransdetailColAlign += ",center,center,center,center,center";
                    acqmistransdetailColAlign += ",right,right,right,center,center";
                    acqmistransdetailColAlign += ",center,center,center,center,center";
                    acqmistransdetailColAlign += ",center,center,center,center,center";
                    acqmistransdetailColAlign += ",center,center,right,right,right,right,right";
                    acqmistransdetailColAlign += ",right,center,center,center,center,center";                    
                    acqmistransdetailgrid.setColAlign(acqmistransdetailColAlign);
                   
                    var acqmistransdetailColTypes = "";
                    /*
                    acqmistransdetailColTypes +=  "txt,txt,txt,txt,txt";
                    acqmistransdetailColTypes += ",txt,txt,txt,txt,txt";
                    acqmistransdetailColTypes += ",edn,edn,edn,txt,txt";
                    acqmistransdetailColTypes += ",txt,txt,txt,txt,txt";
                    acqmistransdetailColTypes += ",txt,txt,txt,txt,txt";
                    acqmistransdetailColTypes += ",txt,txt,edn,edn,edn";
                    acqmistransdetailColTypes += ",edn,txt,txt,txt,txt";
                    */
                    acqmistransdetailColTypes +=  "txt,txt,txt,txt,txt";
                    acqmistransdetailColTypes += ",txt,txt,txt,txt,txt";
                    acqmistransdetailColTypes += ",edn,edn,edn,txt,txt";
                    acqmistransdetailColTypes += ",txt,txt,txt,txt,txt";
                    acqmistransdetailColTypes += ",txt,txt,txt,txt,txt";
                    acqmistransdetailColTypes += ",txt,txt,edn,edn,edn,edn,edn";
                    acqmistransdetailColTypes += ",edn,txt,txt,txt,txt,txt";                    
                    
                    acqmistransdetailgrid.setColTypes(acqmistransdetailColTypes);
                    var acqmistransdetailInitWidths = "";
                    /*
                    acqmistransdetailInitWidths +=   "50,100,100,80,90";
                    acqmistransdetailInitWidths += ",100,80,60,110,60";
                    acqmistransdetailInitWidths += ",90,90,90,80,110";
                    acqmistransdetailInitWidths += ",80,90,70,50,50";
                    acqmistransdetailInitWidths += ",50,100,70,90,90";
                    acqmistransdetailInitWidths += ",90,70,80,80,90";
                    acqmistransdetailInitWidths += ",90,80,110,110,100";
                    */
                    acqmistransdetailInitWidths +=   "40,150,150,80,90";
                    acqmistransdetailInitWidths += ",100,80,60,110,60";
                    acqmistransdetailInitWidths += ",90,90,90,90,110";
                    acqmistransdetailInitWidths += ",80,70,70,90,90";
                    acqmistransdetailInitWidths += ",90,100,100,150,100";
                    acqmistransdetailInitWidths += ",100,100,90,90,90,90,90";
                    acqmistransdetailInitWidths += ",90,90,100,150,100,100";
                    
                    acqmistransdetailgrid.setInitWidths(acqmistransdetailInitWidths);
                    var acqmistransdetailColSorting = "";
                    /*
                    acqmistransdetailColSorting +=  "str,str,str,str,str";
                    acqmistransdetailColSorting += ",str,str,str,str,str";
                    acqmistransdetailColSorting += ",int,int,int,str,str";
                    acqmistransdetailColSorting += ",str,str,str,str,str";
                    acqmistransdetailColSorting += ",str,str,str,str,str";
                    acqmistransdetailColSorting += ",str,str,int,int,int";
                    acqmistransdetailColSorting += ",int,str,str,str,str";
                    */
                    acqmistransdetailColSorting +=  "str,str,str,str,str";
                    acqmistransdetailColSorting += ",str,str,str,str,str";
                    acqmistransdetailColSorting += ",int,int,int,str,str";
                    acqmistransdetailColSorting += ",str,str,str,str,str";
                    acqmistransdetailColSorting += ",str,str,str,str,str";
                    acqmistransdetailColSorting += ",str,str,int,int,int,int,int";
                    acqmistransdetailColSorting += ",int,str,str,str,str,str";                    
                    
                    acqmistransdetailgrid.setColSorting(acqmistransdetailColSorting);
                    acqmistransdetailgrid.setNumberFormat("0,000",10);
                    acqmistransdetailgrid.setNumberFormat("0,000",11);
                    acqmistransdetailgrid.setNumberFormat("0,000",12);
                    acqmistransdetailgrid.setNumberFormat("0,000",27);
                    acqmistransdetailgrid.setNumberFormat("0,000",28);            
                    acqmistransdetailgrid.setNumberFormat("0,000",29);
                    acqmistransdetailgrid.setNumberFormat("0,000",30); 
                    acqmistransdetailgrid.setNumberFormat("0,000",31); 
                    acqmistransdetailgrid.setNumberFormat("0,000",32); 
                    
                    acqmistransdetailgrid.setColumnHidden(18,true);
                    acqmistransdetailgrid.setColumnHidden(19,true);
                    acqmistransdetailgrid.setColumnHidden(20,true);
                    
                    acqmistransdetailgrid.enableColumnMove(true);
                    acqmistransdetailgrid.setSkin("dhx_skyblue");
                    acqmistransdetailgrid.enablePaging(true,Number($("#acqhdetailForm input[name=page_size]").val()),10,"acqmistransdetailPaging",true,"acqmistransdetailrecinfoArea");
                    acqmistransdetailgrid.setPagingSkin("bricks");

                    //페이징 처리
                    acqmistransdetailgrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        /*
                        if(lInd !==0){
                            $("#acqhdetailForm input[name=page_no]").val(ind);
                            acqmistransdetailSearch();
                        }else{
                            return false;
                        }
                        */
                        if(lInd !==0){
                             $("#acqhdetailForm input[name=page_no]").val(ind);
                            acqmistransdetailgrid.clearAll();
                            acqmistransdetailSearch();
                        }else{
                             $("#acqhdetailForm input[name=page_no]").val(1);
                            acqmistransdetailgrid.clearAll();
                            acqmistransdetailSearch();
                        }                                
                    });

                    acqmistransdetailgrid.init();
                    acqmistransdetailgrid.parse(${ls_transDetailList}, "json");
                    
                    //검색조건 초기화
                    $("#acqhdetailForm input[name=init]").click(function () {

                    acqhDetailMasterInit($("#acqhdetailForm"));

                    });  
                    
                     $("#acqhdetailForm input[name=week]").click(function(){
                       acq_date_search_detail("week");
                    });
                    
                      $("#acqhdetailForm input[name=month]").click(function(){
                       acq_date_search_detail("month");
                    });

                    //상세코드검색
                    $("#acqhdetailForm input[name=acqmistransdetail_search]").click(function(){
                        /*
                        $("input[name=page_no]").val("1");

                        acqmistransdetailgrid.clearAll();

                        acqmistransdetailSearch();
                        */
                        acqmistransdetailgrid.changePage(1);

                    });

                    //신용거래 정보 엑셀다운로드 이벤트
                    $("#acqhdetailForm input[name=acqmistransdetail_excel]").click(function () {

                        transDetailExcel();

                    });      
                    
                });
                
                function transDetailExcel(){
                    $("#acqhdetailForm").attr("action","<c:url value="/trans/transDetailListExcel" />");
                    document.getElementById("acqhdetailForm").submit();            
                }                
                
                //ONOFF가맹점번호 팝업 클릭 이벤트
                $("#acqhdetailForm input[name=onffmerch_no]").unbind("click").bind("click", function (){

                    acqonoffMerchSelectPopup();

                });                           
                 function acqonoffMerchSelectPopup(){
                     
                    onoffObject.onffmerch_no = $("#acqhdetailForm input[name=onffmerch_no]");
                    onoffObject.merch_nm = $("#acqhdetailForm input[name=merch_nm]");
                    //ONOFF가맹점정보 팝업
                   w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("가맹점 선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);     
                }

                
                //ONOFFUID 팝업 클릭 이벤트
                $("#acqhdetailForm input[name=onfftid]").unbind("click").bind("click", function (){

                    acq_detailonoffTidSelectPopup();

                });                                
                
                function acq_detailonoffTidSelectPopup(){
                    onoffObject.onfftid_nm = $("#acqhdetailForm input[name=onfftid_nm]");
                    onoffObject.onfftid = $("#acqhdetailForm input[name=onfftid]");
                    //ONOFFUID 팝업
                    w2 = dhxSelPopupWins.createWindow("OnofftidSelectPopUp", 20, 30, 640, 480);
                    w2.setText("ONOFFUID 선택페이지");
                    dhxSelPopupWins.window('OnofftidSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnofftidSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnofftidSelectPopUp"/>', true);   
                }                
              

        //코드 정보 조회
                function acqmistransdetailSearch() {
                
                $.ajax({
                    url: "<c:url value="/acq/acqMasterDetail" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#acqhdetailForm").serialize(),
                    success: function (data) {

                        var jsonData = $.parseJSON(data);

                        acqmistransdetailgrid.parse(jsonData,"json");

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
                    return false;
            }
            
            function acqdetailsetSens(id, k) {
                    // update range
                    if (k == "min") {
                        acqmistransdetailCalendar.setSensitiveRange(byId(id).value, null);
                    } else {
                        acqmistransdetailCalendar.setSensitiveRange(null, byId(id).value);
                    }
                }
			
                  function byId(id) {
                    return document.getElementById(id);
                }                        
                
                function acq_date_search_detail(day){
                    var oneDate = 1000 * 3600 * 24; 
                    var now = new Date();
                    var week = new Date(now.getTime() + (oneDate*-6));
                    var month = new Date(now.getTime() + (oneDate*-31));
                    var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                    var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                    var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                    var date=new Date();
                    if(day=="today"){
                        this.$("input[id=view_transForm_date_start]").val(nowTime);
                        this.$("input[id=view_transForm_date_end]").val(nowTime);
                    }else if(day=="week"){
                        this.$("input[id=view_transForm_date_start]").val(weekTime);
                        this.$("input[id=view_transForm_date_end]").val(nowTime);
                    }else{
                        this.$("input[id=view_transForm_date_start]").val(monthTime);
                        this.$("input[id=view_transForm_date_end]").val(nowTime);
                    }
                }
                
                function acqhDetailMasterInit($form) {
                    searchFormInit($form);
                    acq_date_search_detail("week");                    
                } 
                
    </script>
    <body>


        <div class="right" id="acqmistransdetail_search" style="width:100%;">
             <form id="acqhdetailForm" method="POST" action="<c:url value="/acq/acqMasterDetail" />" modelAttribute="transFormBean">
                <input type="hidden" name="page_size" value="100" >
                <input type="hidden" name="page_no" value="1" >
                
                <table>
                    <tr>
                            <div class="label">승인일</div>
                            <div class="input">
                            <input type="text" size="15" name="app_start_dt" id="view_transForm_date_start" value ="${transFormBean.app_start_dt}" onclick="acqmistransdetail_setSens('view_transForm_date_end', 'max');" /> ~
                            <input type="text" size="15" name="app_end_dt" id="view_transForm_date_end" value="${transFormBean.app_end_dt}" onclick="acqmistransdetail_setSens('view_transForm_date_start', 'min');" /> </div>
                            <div class ="input"><input type="button" name="week" value="1주"/></div>
                            <div class ="input"><input type="button" name="month" value="1달"/></div> 
                            <div class="label">거래상태</div>
                            <div class="input">
                                <select name="tran_status">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${tranStatusList.tb_code_details}">
                                        <option value="${code.detail_code}" <c:if test="${code.detail_code == transFormBean.tran_status}">selected</c:if> >${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="label">매입상태</div>
                            <div class="input">
                                <select name="acc_chk_flag">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${AccChkFlagList.tb_code_details}">
                                        <option value="${code.detail_code}" <c:if test="${code.detail_code == transFormBean.acc_chk_flag}">selected</c:if> >${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>                            
                            <div class="label">거래결과</div>
                            <div class="input">
                                <select name="result_status">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${resultStatusList.tb_code_details}">
                                        <option value="${code.detail_code}" <c:if test="${code.detail_code == transFormBean.result_status}">selected</c:if> >${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>  
                            <!--
                            <div class="label">UID명</div>
                            <div class="input">
                                <input type="hidden" name="onfftid" value ="${transFormBean.onfftid}" />
                                <input type="text" name="onfftid_nm" value ="${transFormBean.onfftid_nm}" />
                            </div>
                            -->
                            <div class="label">고객명</div>
                            <div class="input">
                                <input type="text" name="user_nm" value ="${transFormBean.user_nm}" />
                            </div>                               
                            <div class="label">고객전화번호</div>
                            <div class="input">
                                <input type="text" name="user_phone2" value ="${transFormBean.user_phone2}" />
                            </div>                                                             
                            <div class="label">상품명</div>
                            <div class="input">
                                <input type="text" name="product_nm" value ="${transFormBean.product_nm}" />
                            </div>                                                   
                            <div class="label">승인금액</div>
                            <div class="input">
                                <input type="text" name="tot_amt" value ="${transFormBean.tot_amt}" />
                            </div>                                               
                            <div class="label">카드번호</div>
                            <div class="input">
                                <input type="text" name="card_num" value ="${transFormBean.card_num}" />
                            </div>                                                     
                            <div class="label">승인번호</div>
                            <div class="input">
                                <input type="text" name="app_no" value ="${transFormBean.app_no}" />
                            </div>      
                            <div class="label">가맹점번호</div>
                            <div class="input">
                                <input type="text" name="merch_no" value ="${transFormBean.merch_no}" />
                            </div>                      
                            <div class="label">카드사</div>
                            <div class="input">
                                <select name="iss_cd">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${issCdList.tb_code_details}">
                                        <option value="${code.detail_code}" <c:if test="${code.detail_code == transFormBean.result_status}">selected</c:if> >${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>                                       
                        <td>
                            <input type="button" name="acqmistransdetail_search" value="조회"/>
                        </td>
                        <td>
                            <input type="button" name="acqmistransdetail_excel" value="엑셀"/>
                        </td>
                        <td><input type="button" name="init" value="검색조건지우기"/></td>
                    </tr>
                </table>
                        
            </form>

            <div class="paging">
                <div id="acqmistransdetailPaging" style="width: 50%;"></div>
                <div id="acqmistransdetailrecinfoArea" style="width: 50%;"></div>
            </div>
                                
         </div>

    </body>
</html>
