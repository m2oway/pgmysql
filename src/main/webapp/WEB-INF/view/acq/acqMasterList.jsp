<%-- 
    Document   : acqMaterList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>미매입내역조회</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">

                var acqdhxWins;
                var acq_list_mygrid={};
                var acqFormCalendar;

                $(document).ready(function () {
                    
                    acqFormCalendar = new dhtmlXCalendarObject(["view_app_start_dt","view_app_end_dt"]);

                    var acqSearch = document.getElementById("acq_dhx_search");
                    var tabBarId = tabbar.getActiveTab();

                    acq_master_layout = tabbar.cells(tabBarId).attachLayout("2E");
                    acq_master_layout.cells('a').hideHeader();
                    acq_master_layout.cells('b').hideHeader();
                    
                    
                    acq_list_layout = acq_master_layout.cells('a').attachLayout("2E");
                    acq_list_layout.cells('a').hideHeader();
                    acq_list_layout.cells('b').hideHeader();
                    acq_list_layout.cells('a').attachObject(acqSearch);
                    acq_list_layout.cells('a').setHeight(20);
                    
                    acq_list_mygrid = acq_list_layout.cells('b').attachGrid();
                    
                    acq_list_mygrid.setImagePath("<c:url value ='/resources/images/dhx/dhtmlxGrid/'/>");
                    var acq_list_headers = "";
                    acq_list_headers += "<center>승인일</center>,<center>가맹점번호</center>,<center>미매입건</center>,<center>미매입금액</center>,<center>거래건</center>,<center>거래총금액</center>";
                    acq_list_mygrid.setHeader(acq_list_headers);
                    
                    acq_list_mygrid.setColAlign("center,center,right,right,right,right");
                    acq_list_mygrid.setColTypes("txt,txt,edn,edn,edn,edn");
                    acq_list_mygrid.setInitWidths("100,100,100,100,100,100");
                    acq_list_mygrid.setColSorting("str,str,int,int,int,int");
                    acq_list_mygrid.setNumberFormat("0,000",2);
                    acq_list_mygrid.setNumberFormat("0,000",3);
                    acq_list_mygrid.setNumberFormat("0,000",4);
                    acq_list_mygrid.setNumberFormat("0,000",5);
                    
                    acq_list_mygrid.enableColumnMove(true);
                    acq_list_mygrid.setSkin("dhx_skyblue");
                    
                    var acq_list_footers = "합계,#cspan";
                    acq_list_footers += ",<div id='acq_master2'>0</div>,<div id='acq_master3'>0</div>,<div id='acq_master4'>0</div>,<div id='acq_master5'>0</div>";
                    acq_list_mygrid.attachFooter(acq_list_footers);  
                    
                    acq_list_mygrid.groupBy(0,["#title","#cspan","#stat_total","#stat_total","#stat_total","#stat_total"]);
                    
                    acq_list_mygrid.init();                    
                    acq_list_mygrid.parse(${ls_acqMasterFormList}, acqFooterValues,"json");

                    acq_list_mygrid.attachEvent("onRowDblClicked", acq_attach);
                    
                    
                    //검색조건 초기화
                    $("#acqForm input[name=init]").click(function () {
                        acqMasterInit($("#acqForm"));
                    });  
                    
                    $("#acqForm input[name=week]").click(function(){
                       acq_date_search("week");
                    });
                    
                      $("#acqForm input[name=month]").click(function(){
                       acq_date_search("month");
                    });
                    
                    $("#acqForm .searchButton").button({
                        icons: {
                            primary: "ui-icon-search"
                        }
                    })
                    .unbind("click")
                    .bind("click",function(e){
                        
                        //미매입 검색
                        doSearchAcqMaster();
                        return false;
                    });
                    
                    $("#acqForm .excelButton").button()
                    .unbind("click")
                    .bind("click",function(e){
                        
                        doSearchAcqMasterExcel();
                        return false;
                    });
                    
                });
            
                function acqFooterValues() {
		    var acq_master2 = document.getElementById("acq_master2");
		    var acq_master3 = document.getElementById("acq_master3");
		    var acq_master4 = document.getElementById("acq_master4");
		    var acq_master5 = document.getElementById("acq_master5");
		    
		    acq_master2.innerHTML = putComma(sumColumn(2)) ;
		    acq_master3.innerHTML = putComma(sumColumn(3)) ;
		    acq_master4.innerHTML = putComma(sumColumn(4));
		    acq_master5.innerHTML = putComma(sumColumn(5));
		    
		    return true;
                }
            
                function sumColumn(ind) {
                    
                    var out = 0;
                    for (var i = 0; i < acq_list_mygrid.getRowsNum(); i++) {
                        out += parseFloat(acq_list_mygrid.cells2(i, ind).getValue());
                    }
                    
                    return out;
                    
                }
            
                function acq_attach(rowid, col) {
                    var app_dt = acq_list_mygrid.getUserData(rowid, 'app_dt').replace( /[^(0-9)]/g,"");
                    var merch_no = acq_list_mygrid.getUserData(rowid, 'merch_no');
                    acq_master_layout.cells('b').attachURL("<c:url value = '/acq/acqMasterDetail'/>" + "?app_start_dt="+app_dt+"&app_end_dt="+app_dt+"&merch_no="+merch_no+"&acc_chk_flag=0", true);
                }
                
                //미매입 조회
                function doSearchAcqMaster(){
                    
                    $("#acqForm input[name=app_start_dt]").val(($("#view_app_start_dt").val()).replace( /[^(0-9)]/g,""));
                    $("#acqForm input[name=app_end_dt]").val(($("#view_app_end_dt").val()).replace( /[^(0-9)]/g,""));
                    
                    $.ajax({
                        url : "<c:url value = '/acq/acqMasterList'/>",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#acqForm").serialize(),
                        success : function(data) {
                            
                            acq_list_mygrid.clearAll();
                            acq_list_mygrid.groupBy(0,["#title","#cspan","#stat_total","#stat_total","#stat_total","#stat_total"]);
                            acq_list_mygrid.parse($.parseJSON(data),acqFooterValues, "json");
                            
                        },
                        error : function() { 
                                alert("조회 실패");
                        }
                    });	            
                   
                }
                
                function acqlistsetSens(id, k) {
                    // update range
                    if (k == "min") {
                        acqFormCalendar.setSensitiveRange(byId(id).value, null);
                    } else {
                        acqFormCalendar.setSensitiveRange(null, byId(id).value);
                    }
                }
			
                   function byId(id) {
                    return document.getElementById(id);
                }                              
                
                function acq_date_search(day){
                    var oneDate = 1000 * 3600 * 24; 
                    var now = new Date();
                    var week = new Date(now.getTime() + (oneDate*-6));
                    var month = new Date(now.getTime() + (oneDate*-31));
                    var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                    var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                    var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                    var date=new Date();
                    if(day=="today"){
                        this.$("input[id=view_app_start_dt]").val(nowTime);
                        this.$("input[id=view_app_end_dt]").val(nowTime);
                    }else if(day=="week"){
                        this.$("input[id=view_app_start_dt]").val(weekTime);
                        this.$("input[id=view_app_end_dt]").val(nowTime);
                    }else{
                        this.$("input[id=view_app_start_dt]").val(monthTime);
                        this.$("input[id=view_app_end_dt]").val(nowTime);
                    }
                }
                
                function acqMasterInit($form) {
                    searchFormInit($form);
                    acq_date_search("week");                    
                } 


            //미매입 집계 엑셀다운로드
            function doSearchAcqMasterExcel() {

                $("#acqForm").attr("action","<c:url value="/acq/acqMasterListExcel" />");
                document.getElementById("acqForm").submit();

            }    

            </script>

            <div class="right" id="acq_dhx_search" style="width:100%; height:100%">
                <form id="acqForm" method="POST" action="<c:url value = '/acq/acqMasterList'/>" modelAttribute="acqFormBean">
                    <input type="hidden" name="app_start_dt" id="app_start_dt"/>
                    <input type="hidden" name="app_end_dt" id="app_end_dt"/>
                    
                    <div class="label">검색일</div>
                    <div class="input">
                        <input type="text" name="view_app_start_dt" id="view_app_start_dt" value = "${acqFormBean.app_start_dt}" onclick="acqlistsetSens('view_app_end_dt', 'max');" />~
                        <input type="text" name="view_app_end_dt" id="view_app_end_dt" value = "${acqFormBean.app_end_dt}" onclick="acqlistsetSens('view_app_start_dt', 'min');" />
                    </div>
                    <div class ="input"><input type="button" name="week" value="1주"/></div>
                    <div class ="input"><input type="button" name="month" value="1달"/></div>
                    <div class='label'>가맹점번호</div>
                    <div class='input'><input type="text" name="merch_no" value="${acqFormBean.merch_no}" /></div>
                    
                    <td><button class="searchButton">검색</button></td>
                    <td><input type="button" name="init" value="초기화"/></td>
                    <td><button class="excelButton">엑셀</button></td>

                </form>
            </div>
            

<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

