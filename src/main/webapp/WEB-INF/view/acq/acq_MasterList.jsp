<%-- 
    Document   : merchantMaterList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>매입내역조회</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">
                acq_result_popdhxWins = new dhtmlXWindows();
                acq_result_popdhxWins.attachViewportTo("acq_result_detail_dhx_search");
                
                $(document).ready(function () {
                    var bankDepositInsertCalendar = new dhtmlXCalendarObject(["view_inv_dt_start","view_inv_dt_end"]);
                    var tabBarId = tabbar.getActiveTab();
                    var acq_result_detailSearch = document.getElementById("acq_result_detail_dhx_search");
                    acq_result_master_layout = tabbar.cells(tabBarId).attachLayout("2E");
                    acq_result_master_layout.cells('a').hideHeader();
                    acq_result_master_layout.cells('b').hideHeader();

                    acq_result_detail_layout = acq_result_master_layout.cells('a').attachLayout("2E");
                    
                    acq_result_detail_layout.cells('a').hideHeader();
                    acq_result_detail_layout.cells('b').hideHeader();
                    acq_result_detail_layout.cells('a').attachObject(acq_result_detailSearch);
                    acq_result_detail_layout.cells('a').setHeight(60);
                    acq_result_detail_mygrid = acq_result_detail_layout.cells('b').attachGrid();
                    acq_result_detail_mygrid.setImagePath("<c:url value ='/resources/images/dhx/dhtmlxGrid/'/>");
                    var mheaders = "";
                    mheaders += "<center>매입일</center>,<center>결제채널</center>,<center>결제채널명</center>,<center>MID구분</center>,<center>MID구분명</center>,<center>MID</center>";
                    mheaders += ",<center>입금예정액</center>,<center>수수료</center>"
                    mheaders += ",<center>매입요청액</center>,<center>매입성공액</center>,<center>반송액</center>,<center>보류액</center>,<center>보류해제액</center>";
                    mheaders += ",<center>매입요청건</center>,<center>매입성공건</center>,<center>반송건</center>,<center>보류건</center>,<center>보류해제건</center>";
                    acq_result_detail_mygrid.setHeader(mheaders);
                    var colsel = "";
                    colsel += "center,center,center,center,center,center";
                    colsel += ",center,center"
                    colsel += ",center,center,center,center,center";
                    colsel += ",center,center,center,center,center";                    
                    acq_result_detail_mygrid.setColAlign(colsel);
                    var coltype = "txt,txt,txt,txt,txt,txt";
                    coltype += ",edn,edn";
                    coltype += ",edn,edn,edn,edn,edn";
                    coltype += ",edn,edn,edn,edn,edn";
                    acq_result_detail_mygrid.setColTypes(coltype);
                    var colwiths = "70,80,70,80,100,80";
                    colwiths += ",80,80";
                    colwiths += ",80,80,80,80,80";
                    colwiths += ",80,80,80,80,80";
                    acq_result_detail_mygrid.setInitWidths(colwiths);
                    var colsort  = "str,str,str,str,str,str";
                    colsort += ",int,int";
                    colsort += ",int,int,int,int,int";
                    colsort += ",int,int,int,int,int";
                    acq_result_detail_mygrid.setColSorting(colsort);
                    
                    /*
                    acq_result_detail_mygrid.setNumberFormat("0,000",2);
                    acq_result_detail_mygrid.setNumberFormat("0,000",4);
                    acq_result_detail_mygrid.setNumberFormat("0,000",6);
                    acq_result_detail_mygrid.setNumberFormat("0,000",8);
                    acq_result_detail_mygrid.setNumberFormat("0,000",10);
                    acq_result_detail_mygrid.setNumberFormat("0,000",12);                    
                    */
                  acq_result_detail_mygrid.enableColumnMove(true);
                   
                  acq_result_detail_mygrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxToolbar/" />");
                  //acq_result_detail_mygrid.enablePaging(true,Number($("#acq_result_detail_Form input[name=page_size]").val()),10,"acq_result_detailPaging",true,"acq_result_detailrecinfoArea");
                  //acq_result_detail_mygrid.setPagingSkin("bricks");
                  acq_result_detail_mygrid.attachEvent("onRowDblClicked", acq_result_detail_attach);

                    //페이징 처리
                    /*
                    acq_result_detail_mygrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        if(lInd !==0){
                            $("#acq_result_detail_Form input[name=page_no]").val(ind);
                            doSearchAcq_ResultMasterDetail();
                        }else{
                            return false;
                        }
                    });
                    */
                    acq_result_detail_mygrid.setSkin("dhx_skyblue");
                    acq_result_detail_mygrid.init();
                    acq_result_detail_mygrid.parse(${acq_resultDetailMasterListJsonString},"json");


                    $("#acq_result_detail_Form input[name=week]").click(function(){
                       acqresult_date_search("week");
                    });
                    
                      $("#acq_result_detail_Form input[name=month]").click(function(){
                       acqresult_date_search("month");
                    });
                    
                    
                    //버튼 추가
                    $("#acq_result_detail_Form .searchButton").bind("click",function(e){
                        doSearchAcq_ResultMaster();
                        return false;
                    });

       
 
                    
            });
            
             function doSearchAcq_ResultMaster(){

                    $("#acq_result_detail_Form input[name=inv_dt_start]").val(($("#view_inv_dt_start").val()).replace( /[^(0-9)]/g,""));
                    $("#acq_result_detail_Form input[name=inv_dt_end]").val(($("#view_inv_dt_end").val()).replace( /[^(0-9)]/g,""));
                    
                    if(isValidDate($("#inv_dt_start").val()) ){
                    
                    }else{
                        alert("날짜 형식을 확인하세요");
                        return false;
                    }
                    $.ajax({
                        url : $("#acq_result_detail_Form").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#acq_result_detail_Form").serialize(),
                        success : function(data) {
                            acq_result_detail_mygrid.clearAll();
                            acq_result_detail_mygrid.parse($.parseJSON(data), "json");
                        },
                        error : function() { 
                                alert("조회 실패");
                        }
                    });	            
                }
            
            function acq_result_detail_attach(rowid, col) {
                
                var sal_start_dt =acq_result_detail_mygrid.getUserData(rowid, 'inv_dt');
                var sal_end_dt =acq_result_detail_mygrid.getUserData(rowid, 'inv_dt');
                var pay_type =acq_result_detail_mygrid.getUserData(rowid, 'pay_type');
                var merch_no =acq_result_detail_mygrid.getUserData(rowid, 'merch_no');
                
                var param = "?sal_end_dt=" +sal_start_dt+ "&sal_end_dt=" +sal_start_dt +"&pay_type="  +pay_type  +"&merch_no="  +merch_no; 
                  //var acq_dt = acq_result_detail_mygrid.getUserData(rowid, 'acq_dt');
//                var merch_no = acq_result_list_mygrid.getUserData(rowid, 'merch_no');
//                var acqfile_seq = acq_result_list_mygrid.getUserData(rowid, 'acqfile_seq');
                acq_result_master_layout.cells('b').attachURL("<c:url value = '/acq_result/acq_resultMasterDetailPop'/>"+param , true);
            }

                
                function acqresult_date_search(day){
                        var oneDate = 1000 * 3600 * 24; 
                        var now = new Date();
                        var week = new Date(now.getTime() + (oneDate*-6));
                        var month = new Date(now.getTime() + (oneDate*-31));
                        var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                        var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                        var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());
                        
                        var date=new Date();

                        if(day=="today"){
                            $("#acq_result_detail_Form input[id=view_inv_dt_start]").val(nowTime);
                            $("#acq_result_detail_Form input[id=view_inv_dt_end]").val(nowTime);
                        }else if(day=="week"){
                            $("#acq_result_detail_Form input[id=view_inv_dt_start]").val(weekTime);
                            $("#acq_result_detail_Form input[id=view_inv_dt_end]").val(nowTime);
                        }else{
                            $("#acq_result_detail_Form input[id=view_inv_dt_start]").val(monthTime);
                            $("#acq_result_detail_Form input[id=view_inv_dt_end]").val(nowTime);
                        }
                    }
            </script>

            <div class="right" id="acq_result_detail_dhx_search" style="width:100%; height:100%">
                <form id="acq_result_detail_Form" method="POST" action="<c:url value = '/acq_result/acq_resultMasterList'/>">
                    <input type ="hidden" id ="inv_dt_start" name ="inv_dt_start"/>
                    <input type ="hidden" id ="inv_dt_end" name ="inv_dt_end"/>
                        <div class="label">검색일자</div>
                        <div class ="input"><input id="view_inv_dt_start" value = '${acq_ResultFormBean.inv_dt_start}'/></div><div class ="input"><input id = "view_inv_dt_end" value = '${acq_ResultFormBean.inv_dt_end}'/></div> 
                        <div class ="input"><input type="button" name="week" value="1주"/></div>
                        <div class ="input"><input type="button" name="month" value="1달"/></div>
                        <div class="label">결제채널</div>
                        <div class ="input">
                            <select id = "pay_type" name ="pay_type">
                                <option value="">전체</option>
                                <c:forEach var="code" items="${PayTypeList.tb_code_details}" >
                                     <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="label">MID구분</div>
                        <div class ="input">
                            <select id = "pay_mtd" name ="pay_mtd">
                                <option value="">전체</option>
                                <c:forEach var="code" items="${PayMtdList.tb_code_details}" >
                                     <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>                        
                        <div class ="label">MID</div>
                        <div class ="input"><input name="merch_no"/></div>
                        <button class="searchButton">조회</button>                      
                </form>
                <div id="acq_result_detailPaging" style="width: 100%;"></div>
                <div id="acq_result_detailrecinfoArea" style="width: 100%;"></div>       
            </div>
            

<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

