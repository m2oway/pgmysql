<%-- 
    Document   : companyMasterList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>단말기 사업체</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">
                 
                $(document).ready(function () {
                    
                    acq_select_layout = new dhtmlXLayoutObject("org_acq_select", "1C", "dhx_skyblue");
                    acq_select_layout.cells('a').hideHeader();
                    acq_select_grid = acq_select_layout.cells('a').attachGrid();
                    acq_select_grid.setImagePath("<c:url value = '/resources/images/dhx/dhtmlxGrid/'/>");
                    var mheaders = "";
                    mheaders += "<center>조직코드</center>,<center>조직명</center>";
                    acq_select_grid.setHeader(mheaders);
                    acq_select_grid.setColAlign("center,left");
                    acq_select_grid.setColTypes("txt,txt");
                    acq_select_grid.setInitWidths("80,400");
                    acq_select_grid.setColSorting("str,str");
                    acq_select_grid.enableColumnMove(true);
                    acq_select_grid.setSkin("dhx_skyblue");
                    acq_select_grid.init();
                    acq_select_grid.parse(${acqMasterSelect}, "json");
                    acq_select_grid.attachEvent("onRowDblClicked", doSelectFormAcqMaster);
                });
                
                function doSelectFormAcqMaster(rowid,col){
                      var org_nm = acq_select_grid.getUserData(rowid, 'org_nm'); 
                      $("#acqForm input[name=org_nm]").val(org_nm);
                      acqdhxWins.window('crsw1').close();
                }
            </script>
            
          <%--<form:hidden path="org_nm" value="${org_nm}" id= "org_nm" ></form:hidden>--%>
           <%--<form:hidden path="credit_org_seq" value="" id= "credit_org_seq" ></form:hidden>--%>
        <form id = "acqMasterSelect" name ="acqMasterSelect" method="POST" action ="<c:url value = '/acq/acqMasterSelect'/>" >
            <input type="hidden" name ="org_nm" value="" />
            <input type="hidden" name ="type" value="" />
            <div id="org_acq_select" style="position: relative;width:100%;height:100%;background-color:white;"></div>
        </form> 
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>