<%-- 
    Document   : codeDetailMaster
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>신용카드 상세조회</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">

                var acqresultdetailgrid={};
                var acqresultdetail_layout={};
                
                var middepositDetailCalendar = new dhtmlXCalendarObject(["middepositDetail_from_date","middepositDetail_to_date"]);    

                $(document).ready(function () {

                    var acqresultdetailForm = document.getElementById("acqresultdetail_serch");

                    acqresultdetail_layout = acq_result_master_layout.cells('b').attachLayout("2E");

                    acqresultdetail_layout.cells('a').hideHeader();

                    acqresultdetail_layout.cells('b').hideHeader();

                    acqresultdetail_layout.cells('a').attachObject(acqresultdetailForm);

                    acqresultdetail_layout.cells('a').setHeight(60);

                    acqresultdetailgrid = acqresultdetail_layout.cells('b').attachGrid();

                    acqresultdetailgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                

                    var acqresultdetailheaders = "";
                    acqresultdetailheaders += "<center>NO</center>,<center>MID구분</center>,<center>MID</center>,<center>거래종류</center>,<center>매입사</center>";
                    acqresultdetailheaders += ",<center>승인일</center>,<center>승인번호</center>,<center>승인금액</center>,<center>부가세</center>,<center>할부</center>";
                    acqresultdetailheaders += ",<center>매입일</center>,<center>입금예정일</center>,<center>수수료</center>,<center>입금예정금액</center>";
                    
                    acqresultdetailgrid.setHeader(acqresultdetailheaders);
                    acqresultdetailgrid.setColAlign("center,center,center,center,center,center,center,right,right,center,center,center,right,right");
                    acqresultdetailgrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,edn,edn,txt,txt,txt,edn,edn");
                    acqresultdetailgrid.setInitWidths("50,100,100,100,100,100,100,100,120,100,100,100,100,100");
                    acqresultdetailgrid.setColSorting("str,str,str,str,str,str,str,int,int,str,str,str,int,int");
                    acqresultdetailgrid.setNumberFormat("0,000",7);            
                    acqresultdetailgrid.setNumberFormat("0,000",8);
                    acqresultdetailgrid.setNumberFormat("0,000",12);
                    acqresultdetailgrid.setNumberFormat("0,000",13);
                    acqresultdetailgrid.enableColumnMove(true);
                    acqresultdetailgrid.setSkin("dhx_skyblue");

                    acqresultdetailgrid.init();

                    acqresultdetailgrid.parse(${ls_middepositDetailList},"json");

                    acqresultdetailgrid.enablePaging(true,Number($("#acqresultdetailForm input[name=page_size]").val()),10,"acqresultdetailPaging",true,"acqresultdetailrecinfoArea");

                    acqresultdetailgrid.setPagingSkin("bricks");

                    //페이징 처리
                    acqresultdetailgrid.attachEvent("onPageChanged", function(ind,fInd,lInd){

                        if(lInd !==0){
                            $("#acqresultdetailForm input[name=page_no]").val(ind);
                            acqresultdetailSearch();
                        }else{
                            return false;
                        }
                    });

                    //상세코드검색
                    $("#acqresultdetailForm input[name=acqresultdetail_serch]").click(function(){

                        $("input[name=page_no]").val("1");

                        acqresultdetailgrid.clearAll();

                        acqresultdetailSearch();

                    });
                });

                //코드 정보 조회
                function acqresultdetailSearch() {
                
                $.ajax({
                    url: $("#acqresultdetailForm").attr("action"),
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#acqresultdetailForm").serialize(),
                    success: function (data) {

                        var jsonData = $.parseJSON(data);
                        acqresultdetailgrid.clearAll();
                        acqresultdetailgrid.parse(jsonData,"json");

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
                    return false;
            }
            
            function setSens(id, k) {
                // update range
                if (k == "min") {
                    middepositDetailCalendar.setSensitiveRange(byId(id).value, null);
                } else {
                    middepositDetailCalendar.setSensitiveRange(null, byId(id).value);
                }
            }

            function byId(id) {
                return document.getElementById(id);
            }                      
            
    </script>
    <body>


        <div class="right" id="acqresultdetail_serch" style="width:100%;">
             <form id="acqresultdetailForm" method="POST" action="<c:url value="/acq_result/acq_resultMasterDetailPop" />" modelAttribute="middepositFormBean">

                        <input type="hidden" name="page_size" value="100" >
                        <input type="hidden" name="page_no" value="1" >
                        <table>
                            <tr>
                                <td>
                                    <div class="label">승인일</div>
                                    <div class="input">
                                        <input type="text" name="sal_start_dt" id="middepositDetail_from_date" value ="" onclick="setSens('middepositDetail_to_date', 'max');" /> ~
                                        <input type="text" name="sal_end_dt" id="middepositDetail_to_date" onclick="setSens('middepositDetail_from_date', 'min');" />
                                    </div>
                                    <div class="label">결제채널</div>
                                    <div class="input">
                                        <select name="pay_type">
                                            <option value="">전체</option>
                                            <c:forEach var="code" items="${payChnCateList.tb_code_details}">
                                                <option value="${code.detail_code}">${code.code_nm}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                <div class="label">MID</div>
                                <div class="input"><input name="merch_no" /></div>                                
                                <div class="label">승인번호</div>
                                <div class="input"><input name="aprv_no" /></div>
                                <div class="label">승인금액</div>
                                <div class="input"><input name="tot_trx_amt" /></div>
                                <div class="label">매입사</div>
                                <div class="input">
                                    <select name="acq_cd">
                                        <option value="">전체</option>
                                        <c:forEach var="code" items="${acqCdList.tb_code_details}">
                                            <option value="${code.detail_code}">${code.code_nm}</option>
                                        </c:forEach>
                                    </select>
                                </div>                      
                            </td>                                   
                                <td rowspan='2'>
                                    <input type="button" name="acqresultdetail_serch" value="조회"/>
                                </td>
                                <td rowspan='2'>
                                    <input type="button" name="dcode_search" value="엑셀"/>
                                </td>
                            </tr>
                        </table>
             </form>

            <div id="acqresultdetailPaging" style="width: 100%;"></div>
            <div id="acqresultdetailrecinfoArea" style="width: 100%;"></div>

        </div>
        
        <div id="acqresultdetail" style="position: relative;width:100%;height:100%;background-color:white;"></div>
      
    </body>
</html>
