<%-- 
    Document   : merchTaxamtMasterListPopup
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/common.css" />"  />

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>세금계산서 발행</title>
        </head>
        <body>
        </c:if>
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tbody><tr><td valign="top">

	<div id="divBtn">
		<table border="0" cellpadding="0" cellspacing="1" width="100%">
			<tbody><tr><td style="padding:000 010 000 010" align="left"><input class="" id="printbtn" type="button" value="인쇄" />          </td></tr>
			<tr><td style="padding:010 010 000 010" align="left">
				■ 배경인쇄 방법안내						                                                    												 <br>
				■ 도구&gt;인터넷옵션&gt;고급&gt;인쇄(배경색 및 이미지인쇄 선택)   						        													 <br>
				■ 파일&gt;페이지설정&gt;(배경색 및 이미지인쇄 선택)   						        																</td></tr>
		</tbody></table>
	</div>

	<div id="divPrn">
		<table border="0" cellpadding="0" cellspacing="1" width="100%">
			<tbody><tr><td style="padding:040 010 005 010" align="center"><font style="font-size:18pt;font-weight:bold">부가세 신고용 카드매출 실적	     </font></td></tr>
			<tr><td style="padding:010 010 005 010" align="center"><font style="font-size:18pt;font-weight:bold">( ${taxamtFormBean.app_start_yyyy} 년 ${taxamtFormBean.app_start_mm} 월 ~ ${taxamtFormBean.app_end_yyyy} 년 ${taxamtFormBean.app_end_mm} 월 )</font></td></tr>
		</tbody></table>

		<table border="0" cellpadding="0" cellspacing="1" width="100%">
			<tbody><tr><td style="padding:020 010 005 010" align="left"><font style="font-size:12pt;font-weight:bold">1. 기본정보						 </font></td></tr>
			<tr><td style="padding:010 010 005 010">
					<table border="0" cellpadding="0" cellspacing="1" width="100%" bgcolor="a7a7a7" class="title">
						<tbody><tr align="center" height="32">
							<td bgcolor="#6ec9fd" width="25%">상호　(법인명)			  	      </td>
							<td bgcolor="#ffffff" width="25%">${ls_merchTaxamtMasterListPopup[0].comp_nm}</td>
							<td bgcolor="#6ec9fd" width="25%">성명　(대표자)	     			  </td>
							<td bgcolor="#ffffff" width="25%">${ls_merchTaxamtMasterListPopup[0].comp_ceo_nm}</td>
						</tr>
						<tr align="center" height="32">
							<td bgcolor="#6ec9fd" width="25%">가맹점번호		  		  	      </td>
							<td bgcolor="#ffffff" width="25%">${ls_merchTaxamtMasterListPopup[0].onffmerch_no}</td>
							<td bgcolor="#6ec9fd" width="25%">사업자등록번호		  			  </td>
							<td bgcolor="#ffffff" width="25%">${ls_merchTaxamtMasterListPopup[0].biz_no}</td>
						</tr>
					</tbody></table>
				</td>               
				
			</tr>	
			<tr><td style="padding:030 010 005 010" align="left">
                                <font style="font-size:12pt;font-weight:bold">2. 카드매출 상세내역</font>
                                <input type="button" id="merchTaxamt_excel" value="엑셀"/>
                            </td></tr>
			<tr><td style="padding:010 010 005 010">
					<table border="0" cellpadding="0" cellspacing="1" width="100%" bgcolor="a7a7a7" class="title">
						<tbody><tr align="center" height="32" bgcolor="#6ec9fd">
                                                        <td width="14%">매출 월				              </td>
							<td width="14%">처리건수				              </td>
							<!--
                                                        <td width="14%">매출액					          </td>
                                                        <td width="14%">봉사료					          </td>
							<td width="14%">VAT					              </td>
                                                        -->
							<td width="16%">총매출액				              </td>
						</tr>
                                                <c:forEach items="${ls_merchTaxamtMasterListPopup}" var="merchTaxamt">
                                                    <tr align="right" height="32" bgcolor="#ffffff" onmouseover="this.style.backgroundColor=&#39;#E1F4DA&#39;;" onmouseout="this.style.backgroundColor=&#39;#ffffff&#39;;" style="background-color: rgb(255, 255, 255);">
                                                            <td>${merchTaxamt.app_dt}</td>
                                                            <td><fmt:formatNumber value="${merchTaxamt.app_cnt}" pattern="#,###"/></td>
                                                            <!--
                                                            <td><fmt:formatNumber value="${merchTaxamt.app_amt}" pattern="#,###"/></td>
                                                            <td><fmt:formatNumber value="${merchTaxamt.svc_amt}" pattern="#,###"/></td>
                                                            <td><fmt:formatNumber value="${merchTaxamt.tax_amt}" pattern="#,###"/></td>
                                                            -->
                                                            <td><fmt:formatNumber value="${merchTaxamt.tot_amt}" pattern="#,###"/></td>
                                                    </tr>                                                    
                                                </c:forEach>

					</tbody></table>
				</td>
				
			</tr>
			<tr><td style="padding:030 010 005 010" align="left">
                                <font style="font-size:12pt;font-weight:bold">3. 현금영수증 상세내역</font>
                                <input type="button" id="merchCashTaxamt_excel" value="엑셀"/>
                            </td></tr>                        
			<tr><td style="padding:010 010 005 010">
					<table border="0" cellpadding="0" cellspacing="1" width="100%" bgcolor="a7a7a7" class="title">
						<tbody><tr align="center" height="32" bgcolor="#6ec9fd">
                                                        <td width="14%">매출 월				              </td>
							<td width="14%">처리건수				              </td>
							<!--
                                                        <td width="14%">매출액					          </td>
                                                        <td width="14%">봉사료					          </td>
							<td width="14%">VAT					              </td>
                                                        -->
							<td width="16%">총매출액				              </td>
						</tr>
                                                <c:forEach items="${ls_merchTaxamtCashbillMasterListPopup}" var="merchCashTaxamt">
                                                    <tr align="right" height="32" bgcolor="#ffffff" onmouseover="this.style.backgroundColor=&#39;#E1F4DA&#39;;" onmouseout="this.style.backgroundColor=&#39;#ffffff&#39;;" style="background-color: rgb(255, 255, 255);">
                                                            <td>${merchCashTaxamt.app_dt}</td>
                                                            <td><fmt:formatNumber value="${merchCashTaxamt.app_cnt}" pattern="#,###"/></td>
                                                            <!--
                                                            <td><fmt:formatNumber value="${merchCashTaxamt.app_amt}" pattern="#,###"/></td>
                                                            <td><fmt:formatNumber value="${merchCashTaxamt.svc_amt}" pattern="#,###"/></td>
                                                            <td><fmt:formatNumber value="${merchCashTaxamt.tax_amt}" pattern="#,###"/></td>
                                                            -->
                                                            <td><fmt:formatNumber value="${merchCashTaxamt.tot_amt}" pattern="#,###"/></td>
                                                    </tr>                                                    
                                                </c:forEach>

					</tbody></table>
				</td>
				
			</tr>	                        
			<tr><td style="padding:080 010 005 010" align="center"><font style="font-size:18pt;font-weight:bold" id="taxamt_date"></font></td></tr>
			<tr><td style="padding:010 010 005 010" align="center"><font style="font-size:10pt;                ">*위 매출내역은 온오프코리아 매출처리일 기준으로 작성된 부가세 신고 참고용 자료입니다.

			<tr>
				<td align="center">
					<table>
						<tbody><tr><td style="padding:060 010 005 010" align="center"><img src="<c:url value="/resources/images/onoffstamp2.jpg" />" ></td></tr> 
					</tbody></table>
				</td>
			</tr>
		</tbody></table>
	</div>
</td></tr>
		<tr>
			<td>
			</td>
		</tr>
	</tbody></table>
	<iframe id="actFrame" name="actFrame" frameborder="0" src="about:blank" width="0" height="0"></iframe>
        
        <form id="merchTaxamtForm" method="POST" onsubmit="return false" modelAttribute="taxamtFormBean">
            <input type="hidden" name="app_start_yyyy" value="${taxamtFormBean.app_start_yyyy}" />
            <input type="hidden" name="app_start_mm" value="${taxamtFormBean.app_start_mm}" />
            <input type="hidden" name="app_end_yyyy" value="${taxamtFormBean.app_end_yyyy}" />
            <input type="hidden" name="app_end_mm" value="${taxamtFormBean.app_end_mm}" />
        </form>
        
<script language="javascript">
	var htmlContent;
	
        $("#printbtn").click(function(){
                window.onbeforeprint = beforeDivs;
                window.onafterprint = aftersDivs;
                window.print();
        });
        
        
        $("#merchTaxamt_excel").click(function(){
            $("#merchTaxamtForm").attr("action",'<c:url value="/taxamt/merchTaxamtMasterListPopupExcel" />');
            document.getElementById("merchTaxamtForm").submit();
        });
        
        
        $("#merchCashTaxamt_excel").click(function(){
            $("#merchTaxamtForm").attr("action",'<c:url value="/taxamt/merchTaxamtCashbillMasterListPopupExcel" />');
            document.getElementById("merchTaxamtForm").submit();
        });
        
        
        
	function goPrint(){
  		if(document.all && window.print){
  			window.onbeforeprint = beforeDivs;
  			window.onafterprint = aftersDivs;
  		 	window.print();
  		} 	
	}

	function beforeDivs()	{
		if	(document.all                )	{
			var rng		= document.body.createTextRange();
			if (rng	   != null)	{
				htmlContent	= rng.htmlText;
			  	rng.pasteHTML("<table align=center><tr><td align=center>" + document.all("divPrn").innerHTML + "</td></tr></table>"        );
			}
		}
	}

	function aftersDivs()	{
		if	(document.all                )	{
			var rng		= document.body.createTextRange();
			if (rng    != null)	{
//			  	htmlContent	= rng.htmlText;
				rng.pasteHTML(htmlContent                                                                                     );
		 	}
		}
	}	
        
        $("#taxamt_date").text(taxamt_date_search());
        
        function taxamt_date_search(){

            var now = new Date();
            var nowTime = now.getFullYear() +" 년 "+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +" 월 "+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate()) + " 일 ";        
            return nowTime;

        }   
        

        
</script>        

<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

