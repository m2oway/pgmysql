<%-- 
    Document   : taxamtMasterList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
<jsp:include page="/WEB-INF/view/common/common.jsp"/>
<html>
    <head>
        <title>세금계산서 조회</title>             
    </head>
    <body>
</c:if>
        
        <script type="text/javascript">
            
            $(document).ready(function () {

                $("#taxamtForm select[name=pay_start_yyyy]").append(Common.yyList('',2015,taxamt_date_search()));
                
                $("#taxamtForm select[name=pay_start_mm]").append(Common.mmList(''));       
                
                $("#taxamtForm select[name=pay_end_yyyy]").append(Common.yyList('',2015,taxamt_date_search()));
                
                $("#taxamtForm select[name=pay_end_mm]").append(Common.mmList(''));       
                
                
                //신용거래 정보 엑셀다운로드 이벤트
                $("#taxamtForm input[name=taxamt_excel]").click(function () {
                    
                    taxamtExcel();
                    
                })
                
            });

            //신용거래 엑셀다운로드
            function taxamtExcel() {

                $("#taxamtForm").attr("action","<c:url value="/taxamt/taxamtMasterListExcel" />");
                document.getElementById("taxamtForm").submit();

            }       
            
            function taxamt_date_search(){
            
                var now = new Date();
                var nowYear = now.getFullYear();
                return nowYear;
            
            }                   

        </script>
        
        <div class="right" id="taxamt_search" style="width:100%;">       
            <form id="taxamtForm" method="POST" onsubmit="return false" action="<c:url value="/taxamt/taxamtMasterList" />" modelAttribute="taxamtFormBean"> 
                <table>
                    <tr>
                        <div class="label">날짜</div>
                        <div class="input">
                            <select name="pay_start_yyyy">
                            </select>
                            <select name="pay_start_mm">
                            </select> ~ 
                            <select name="pay_end_yyyy">
                            </select>
                            <select name="pay_end_mm">
                            </select>    
                        </div>                        
                        <td><input type="button" name="taxamt_excel" value="엑셀"/></td>
                    </tr>
               </table>
            </form>                 
        </div>         

<c:if test="${!ajaxRequest}">
    </body>
</html>
</c:if>        
