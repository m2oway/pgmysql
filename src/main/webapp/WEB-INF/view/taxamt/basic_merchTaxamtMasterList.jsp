<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/basic_common.jsp"/>
<%@include file="/WEB-INF/view/common/basic_comm_header.jsp"%>
    <table width="100%" height="42" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top" class="titleBg">
		<table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="title"><b>-세금계산서</b></td>
                        <td><img src="../resources/images/title_sright.gif" width="12" height="38" /></td>
                    </tr>
		</table>
            </td>
	</tr>
    </table>
        <script type="text/javascript">
            
            $(document).ready(function () {

                $("#taxamtForm select[name=app_start_yyyy]").append(Common.yyList('',2015,taxamt_date_search()));
                
                $("#taxamtForm select[name=app_start_mm]").append(Common.mmList(''));   
                
                $("#taxamtForm select[name=app_end_yyyy]").append(Common.yyList('',2015,taxamt_date_search()));
                
                $("#taxamtForm select[name=app_end_mm]").append(Common.mmList(''));                   
                
                //세금계산서 부가세 팝업 이벤트
                $("#taxamtForm input[name=taxamt_popup]").click(function () {
                    
                    taxamtPopup();
                    
                })
                
            });

            //세금계산서 부가세 팝업 
            function taxamtPopup() {

                var app_start_yyyy = $("#taxamtForm select[name=app_start_yyyy]").val();
                var app_start_mm = $("#taxamtForm select[name=app_start_mm]").val();
                var app_end_yyyy = $("#taxamtForm select[name=app_end_yyyy]").val();
                var app_end_mm = $("#taxamtForm select[name=app_end_mm]").val();
                
                var param = "?1=1";
                param += "&app_start_yyyy=" + app_start_yyyy + "&app_start_mm=" + app_start_mm + "&app_end_yyyy=" + app_end_yyyy + "&app_end_mm=" + app_end_mm;
                
                var strUrl = '<c:url value="/taxamt/merchTaxamtMasterListPopup" />' + param;
                var winx = window.open(strUrl,"세금계산서 발행", "width=1300,height=700,scrollbars=yes,resizeable=no");
                winx.focus();
                
            }       
            
            function taxamt_date_search(){
            
                var now = new Date();
                var nowYear = now.getFullYear();
                return nowYear;
            
            }                   

        </script>
        <div class="right" id="taxamt_search" style="width:100%;">       
            <form id="taxamtForm" method="POST" onsubmit="return false" action="<c:url value="/taxamt/merchTaxamtMasterList" />" modelAttribute="taxamtFormBean"> 
                <table>
                    <tr>
                        <div class="label">날짜</div>
                        <div class="input">
                            <select name="app_start_yyyy">
                            </select>
                            <select name="app_start_mm">
                            </select>      
                            ~
                            <select name="app_end_yyyy">
                            </select>
                            <select name="app_end_mm">
                            </select>                                  
                        </div>                        
                        <td><input type="button" name="taxamt_popup" value="부가세"/></td>
                    </tr>
               </table>
            </form>
        </div>        
<%@include file="/WEB-INF/view/common/basic_comm_tail.jsp"%>   
