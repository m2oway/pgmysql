<%-- 
    Document   : codeDetailMaster
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="/WEB-INF/view/common/basic_common.jsp"/>
<%@include file="/WEB-INF/view/common/basic_comm_header.jsp"%>
    <table width="100%" height="42" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top" class="titleBg">
		<table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="title"><b>-카드거래내역</b></td>
                        <td><img src="../resources/images/title_sright.gif" width="12" height="38" /></td>
                    </tr>
		</table>
            </td>
	</tr>
    </table>
            <div class="right" id="transdetail_search" style="width:100%;">
                <form id="transdetailForm"  name="transdetailForm"  method="POST" action="<c:url value="/trans/basic_merchTransDetailList" />" modelAttribute="transFormBean">
                    <input type="hidden" name="page_size" id="page_size" value="10" >
                    <input type="hidden" name="page_no" id="page_no" value="${transFormBean.page_no}" >
                    <input type="hidden" name="total_cnt" id="total_cnt" value="${total_count}" >
                    <input type="hidden" name="pay_chn_cate" value="${transFormBean.pay_chn_cate}" >
                    <input type="hidden" name="nopageflag" value="Y">
                    <table>
                        <tr><td>
                            <div class="label">승인일</div>
                            <div class="input">
                            <input type="text" name="app_start_dt" id="transdetailForm_from_date" value ="${transFormBean.app_start_dt}" onclick="transdetail_setSens('transdetailForm_to_date', 'max');" /> ~
                            <input type="text" name="app_end_dt" id="transdetailForm_to_date" value="${transFormBean.app_end_dt}" onclick="transdetail_setSens('transdetailForm_from_date', 'min');" /> </div>
                            <div class="label">거래종류</div>
                            <div class="input">
                                <select name="massagetype">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${massagetypeCdList.tb_code_details}">
                                        <option value="${code.detail_code}" <c:if test="${code.detail_code == transFormBean.massagetype}">selected</c:if> >${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>                               
                            <div class="label">거래상태</div>
                            <div class="input">
                                <select name="tran_status">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${tranStatusList.tb_code_details}">
                                        <option value="${code.detail_code}" <c:if test="${code.detail_code == transFormBean.tran_status}">selected</c:if> >${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="label">거래결과</div>
                            <div class="input">
                                <select name="result_status">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${resultStatusList.tb_code_details}">
                                        <option value="${code.detail_code}" <c:if test="${code.detail_code == transFormBean.result_status}">selected</c:if> >${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="label">카드사</div>
                            <div class="input">
                                <select name="iss_cd">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${issCdList.tb_code_details}">
                                        <option value="${code.detail_code}" <c:if test="${code.detail_code == transFormBean.iss_cd}">selected</c:if> >${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>                              
                            <div class="label">검색항목</div>
                            <div class="input">
                                <select name="searchinputnm" id="searchinputnm" onchange="javascirpt:searchinputnmChange(this);">
                                    <option value="user_nm" <c:if test="${'user_nm' == transFormBean.searchinputnm}">selected</c:if>>고객명</option>
                                    <option value="product_nm" <c:if test="${'product_nm' == transFormBean.searchinputnm}">selected</c:if>>상품명</option>
                                    <option value="user_phone2" <c:if test="${'user_phone2' == transFormBean.searchinputnm}">selected</c:if>>전화번호</option>
                                    <option value="tot_amt" <c:if test="${'tot_amt' == transFormBean.searchinputnm}">selected</c:if>>승인금액</option>
                                    <option value="card_num" <c:if test="${'card_num' == transFormBean.searchinputnm}">selected</c:if>>카드번호</option>
                                    <option value="app_no" <c:if test="${'app_no' == transFormBean.searchinputnm}">selected</c:if>>승인번호</option>
                                </select>
                                <input type="text" name="searchinputcontent" id="searchinputcontent"  value ="${transFormBean.searchinputcontent}" onblur="javascirpt:searchinputcontentChange(this);" />
                            </div>
                                <input type="hidden" name="user_nm" id="user_nm" value ="${transFormBean.user_nm}" />
                                <input type="hidden" name="user_phone2" id="user_phone2"  value ="${transFormBean.user_phone2}" />
                                <input type="hidden" name="product_nm" id="product_nm"  value ="${transFormBean.product_nm}" />
                                <input type="hidden" name="tot_amt" id="tot_amt" value ="${transFormBean.tot_amt}" />
                                <input type="hidden" name="card_num" id="card_num" value ="${transFormBean.card_num}" />
                                <input type="hidden" name="app_no" id="app_no" value ="${transFormBean.app_no}" />                            
                            <!--
                            <div class="label">고객명</div>
                            <div class="input">
                                <input type="text" name="user_nm" value ="${transFormBean.user_nm}" />
                            </div>                               
                            <div class="label">고객전화번호</div>
                            <div class="input">
                                <input type="text" name="user_phone2" value ="${transFormBean.user_phone2}" />
                            </div>                                                             
                            <div class="label">상품명</div>
                            <div class="input">
                                <input type="text" name="product_nm" value ="${transFormBean.product_nm}" />
                            </div>                                                   
                            <div class="label">승인금액</div>
                            <div class="input">
                                <input type="text" name="tot_amt" value ="${transFormBean.tot_amt}" />
                            </div>                                               
                            <div class="label">카드번호</div>
                            <div class="input">
                                <input type="text" name="card_num" value ="${transFormBean.card_num}" />
                            </div>                                                     
                            <div class="label">승인번호</div>
                            <div class="input">
                                <input type="text" name="app_no" value ="${transFormBean.app_no}" />
                            </div>
                            -->
                            </td>                               
                            <td><input type="button" name="transdetail_search" onclick="javascript:transdetailSearch();" value="조회"/></td>
                            <td><input type="button" name="transdetail_excel" onclick="javascript:transDetailExcel();" value="엑셀"/></td>
                        </tr>
                    </table>
                        
                </form>
            <form name="frm_card_cncl" id="frm_card_cncl" method="post" >
                <input type="hidden" name="org_cno" id="org_cno" value=""><input type="hidden" name="mgr_msg" id="mgr_msg" value="" ><input type="hidden" name="EP_tr_cd" id="EP_tr_cd" value="00201000"><input type="hidden" name="mgr_txtype" id="mgr_txtype" value="40"><input type="hidden" name="tran_seq" id="tran_seq" value="">
            </form>
        </div><br>
        <table border='0' sytle="border: #ff0000 solid 2px;" width="100%">
            <tr>
                <td width="100%" style="alignment-adjust: right;" align="right">
                <div class="CSSTableGenerator" style="width:50%;" >
                <table>
                    <tr>
                        <td>승인건</td>
                        <td>승인금액</td>
                        <td >취소건</td>
                        <td>취소금액</td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><fmt:formatNumber value="${total_appcnt}" type="number"/></td>
                        <td style="text-align: right"><fmt:formatNumber value="${total_appamt}" type="number"/></td>
                        <td style="text-align: right"><fmt:formatNumber value="${total_cnclcnt}" type="number"/></td>
                        <td style="text-align: right"><fmt:formatNumber value="${total_cnclamt}" type="number"/></td>
                    </tr>                    
                </table>
                </div>
                </td>
            </tr>
        </table>
        <br>
        <div class="CSSTableGenerator" >
                <table >
                    <tr>
                        <td>No</td>
                        <td>가맹점</td>
                        <td >고객명</td>
                        <td>연락처</td>
                        <td>상품명</td>                        
                        <td>거래종류</td>
                        <td>카드사</td>
                        <td>카드번호</td>                        
                        <td>할부</td>                        
                        <td>승인금액</td>
                        <td>승인번호</td>                        
                        <td>승인일시</td>
                        <td>취소일시</td>                        
                        <td>거래상태</td>
                        <!--
                        <td>원거래승인번호</td>
                        <td>원거래승인일</td>
                        -->
                        <td>승인취소</td>                                                
                        <td>영수증</td>                        
                    </tr>
<fmt:formatDate value="${now}" pattern="yyyyMMdd" var="today" />    
<c:forEach var="transDetailList" items="${ls_transDetailList}" begin="0">                            
<fmt:parseDate value="${transDetailList.app_dt}" var="tpchkappdt1" pattern="yyyyMMdd" scope="page"/><fmt:formatDate value="${tpchkappdt1}" pattern="yyyyMMdd" var="chkapp_dt"/>
    <tr>
                        <td height="30px;" width="30px"  style="text-align:center">${transDetailList.rnum}</td> 
                        <td>${transDetailList.merch_nm}</td>
                        <td >${transDetailList.user_nm}</td>
                        <td>${transDetailList.user_phone2}</td>
                        <td>${transDetailList.product_nm}</td>                        
                        <td>${transDetailList.massagetype_nm}</td>
                        <td>${transDetailList.iss_nm}</td>
                        <td>${transDetailList.card_num}</td>                        
                        <td>${transDetailList.installment}</td>                        
                        <td style="text-align: right"><fmt:formatNumber value="${transDetailList.tot_amt}" type="number"/></td>
                        <c:choose>
                            <c:when test="${transDetailList.massagetype == '10'}">
                                <td>${transDetailList.app_no}</td> 
                            </c:when>
                            <c:otherwise>
                               <td>${transDetailList.org_app_no}</td> 
                            </c:otherwise>
                        </c:choose>                            
                        <td><fmt:parseDate value="${transDetailList.app_dt}${transDetailList.app_tm}" var="appdt1" pattern="yyyyMMddHHmmss" scope="page"/><fmt:formatDate value="${appdt1}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
                        <td><fmt:parseDate value="${transDetailList.cncl_dt}" var="cncldt1" pattern="yyyyMMdd" scope="page"/><fmt:formatDate value="${cncldt1}" pattern="yyyy-MM-dd" /></td>
                        <td>${transDetailList.tran_status_nm}</td>
                        <!--
                        <td>${transDetailList.org_app_no}</td>
                        <td><fmt:parseDate value="${transDetailList.org_app_dd}" var="orgappdt1" pattern="yyyyMMdd" scope="page"/><fmt:formatDate value="${orgappdt1}" pattern="yyyy-MM-dd" /></td>
                        -->
                        <c:choose>
                            <c:when test="${parma_usercate == '01' }">
                                <c:choose>
                                    <c:when test="${transDetailList.result_status == '00' && transDetailList.massagetype == '10' }">
                                        <c:choose>
                                            <c:when test="${transDetailList.tran_status == '00' || transDetailList.tran_status == '05' }">
                                                <c:choose>
                                                    <c:when test="${parma_merch_cncl_auth == '00'}">
                                                        <td><button name='cncl_view_bt' onclick="javascript:tranCncl('${transDetailList.tran_seq}', '${transDetailList.pg_seq}');" >거래취소</button></td> 
                                                    </c:when>
                                                    <c:otherwise>
                                                            <c:choose>
                                                                <c:when test="${today == chkapp_dt }">
                                                                    <td><button name='cncl_view_bt' onclick="javascript:tranCncl('${transDetailList.tran_seq}', '${transDetailList.pg_seq}');" >거래취소</button></td> 
                                                                </c:when>
                                                                <c:otherwise>
                                                                   <td><button name=cnclreq_view_bt onclick="javascript:tranreqcncl('${transDetailList.tran_seq}', '${transDetailList.pg_seq}');" >취소요청</button></td> 
                                                                </c:otherwise>
                                                            </c:choose>   
                                                    </c:otherwise>
                                                </c:choose> 
                                            </c:when>
                                            <c:otherwise>
                                               <td>&nbsp;</td> 
                                            </c:otherwise>
                                        </c:choose> 
                                    </c:when>
                                    <c:otherwise>
                                        <td>&nbsp;</td> 
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                            <c:when test="${parma_usercate == '02' }">
                                <c:choose>
                                    <c:when test="${transDetailList.result_status == '00' && transDetailList.massagetype == '10' }">
                                        <c:choose>
                                            <c:when test="${transDetailList.tran_status == '00'}">
                                                <c:choose>
                                                    <c:when test="${parma_tid_cncl_auth == '00'}">
                                                        <td><button name='cncl_view_bt' onclick="javascript:tranCncl('${transDetailList.tran_seq}', '${transDetailList.pg_seq}');" >거래취소</button></td> 
                                                    </c:when>
                                                    <c:otherwise>
                                                            <c:choose>
                                                                <c:when test="${today == chkapp_dt }">
                                                                    <td><button name='cncl_view_bt' onclick="javascript:tranCncl('${transDetailList.tran_seq}', '${transDetailList.pg_seq}');" >거래취소</button></td> 
                                                                </c:when>
                                                                <c:otherwise>
                                                                   <td><button name=cnclreq_view_bt onclick="javascript:tranreqcncl('${transDetailList.tran_seq}', '${transDetailList.pg_seq}');" >취소요청</button></td> 
                                                                </c:otherwise>
                                                            </c:choose>   
                                                    </c:otherwise>
                                                </c:choose> 
                                            </c:when>
                                            <c:otherwise>
                                               <td>&nbsp;</td> 
                                            </c:otherwise>
                                        </c:choose> 
                                    </c:when>
                                    <c:otherwise>
                                        <td>&nbsp;</td> 
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                            <c:otherwise>
                                <td>5&nbsp;</td> 
                            </c:otherwise>
                        </c:choose>                            
                        <td><button onclick="javascript:Trans_MgrAction('${transDetailList.tran_seq}','${transDetailList.result_cd}' );"  >영수증</button></td>
                    </tr>
</c:forEach>
                </table>
            </div>                            
             <div id="paging" name="paging" style="margin: 15px;text-align:center;"></div>
        </div>
<%@include file="/WEB-INF/view/common/basic_comm_tail.jsp"%>    
            <script type="text/javascript">
                var complainMasterWindow;
                var transdetailCalendar1;

                $(document).ready(function () {
                    
                    transdetailCalendar1 = new dhtmlXCalendarObject(["transdetailForm_from_date","transdetailForm_to_date"]);    
                
                    var transdetailForm = document.getElementById("transdetail_search");
                    
                    //makepage(${transFormBean.page_no});
                    
                });
                
                function searchinputcontentChange(selobj)
                {
                    
                    var selvalue = selobj.value;
                    var selnm = $("#searchinputnm").val();
                   
                    if(selnm == "user_nm")
                    {
                        $("#user_nm").val(selvalue);
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                        $("#app_no").val("");                    
                    }
                    else if(selnm == "user_phone2")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val(selvalue);
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                        $("#app_no").val("");
                    }
                    else if(selnm == "product_nm")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val(selvalue);
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                        $("#app_no").val("");
                    }
                    else if(selnm == "tot_amt")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val(selvalue);
                        $("#card_num").val("");
                        $("#app_no").val("");
                    }
                    else if(selnm == "card_num")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val(selvalue);
                        $("#app_no").val("");
                    }
                    else if(selnm == "app_no")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                        $("#app_no").val(selvalue);
                    }                
                }
                
                function searchinputnmChange(selobj)
                {
                    var selnm = selobj.value;
                    var salval = $("#searchinputcontent").val();
                    
                    if(selnm == "user_nm")
                    {
                        $("#user_nm").val(salval);
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                        $("#app_no").val("");                    
                    }
                    else if(selnm == "user_phone2")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val(salval);
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                        $("#app_no").val("");
                    }
                    else if(selnm == "product_nm")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val(salval);
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                        $("#app_no").val("");
                    }
                    else if(selnm == "tot_amt")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val(salval);
                        $("#card_num").val("");
                        $("#app_no").val("");
                    }
                    else if(selnm == "card_num")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val(salval);
                        $("#app_no").val("");
                    }
                    else if(selnm == "app_no")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                        $("#app_no").val(salval);
                    }                    
                }
                
                 function transdetail_date_search(day){
                    var oneDate = 1000 * 3600 * 24; 
                    var now = new Date();
                    var week = new Date(now.getTime() + (oneDate*-6));
                    var month = new Date(now.getTime() + (oneDate*-31));
                    var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                    var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                    var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                    var date=new Date();

                    if(day=="today"){
                        $("#transdetailForm input[id=transdetailForm_from_date]").val(nowTime);
                        $("#transdetailForm input[id=transdetailForm_to_date]").val(nowTime);
                    }else if(day=="week"){
                        $("#transdetailForm input[id=transdetailForm_from_date]").val(weekTime);
                        $("#transdetailForm input[id=transdetailForm_to_date]").val(nowTime);
                    }else{
                        $("#transdetailForm input[id=transdetailForm_from_date]").val(monthTime);
                        $("#transdetailForm input[id=transdetailForm_to_date]").val(nowTime);
                    }
                }
                //거래 상세 정보 조회
                function transdetailSearch() {
                  $("#page_no").val("1");
                  document.transdetailForm.submit();
                }

            function tranCncl(param_transeq, param_org_cno)
            {            
                var tptranseq = param_transeq;
                var tporgcno =  param_org_cno; 

                if(confirm("승인거래를 취소하시겠습니까?"))
                {
                    
                    $("#frm_card_cncl input[id=tran_seq]").val(tptranseq);
                    $("#frm_card_cncl input[id=org_cno]").val(tporgcno);

                    $.ajax({
                                url : "../app/mgrAction",
                                type : "POST",
                                async : true,
                                dataType : "json",
                                data: $("#frm_card_cncl").serialize(),
                                success : function(data) {
                                    if(data.result_cd == '0000')
                                    {
                                        alert(data.result_msg);
                                        transdetailSearch();
                                    }
                                    else
                                    {
                                        alert("[" +data.result_cd+ "]" + data.result_msg);
                                    }

                                },
                                error : function() { 
                                    alert("취소 실패");
                                }
                    });	            
                }
                return false;
            }
                            
                function tranreqcncl(param_transeq, param_org_cno)
                {
                    var tptranseq = param_transeq;
                    var tporgcno =  param_org_cno; 


                    if(confirm("승인거래를 취소요청 하시겠습니까?"))
                    {                            
                        $("#frm_card_cncl input[id=tran_seq]").val(tptranseq);
                        $("#frm_card_cncl input[id=org_cno]").val(tporgcno);

                        $.ajax({
                                url : "../app/cnclReqAction",
                                type : "POST",
                                async : true,
                                dataType : "json",
                                data: $("#frm_card_cncl").serialize(),
                                success : function(data) {
                                    if(data.result_cd == '0000')
                                    {
                                        alert(data.result_msg);
                                        transdetailSearch();
                                    }
                                    else
                                    {
                                        alert("[" +data.result_cd+ "]" + data.result_msg);
                                    }

                                },
                                error : function() { 
                                    alert("취소요청 실패");
                                }
                        });	            
                    }

                    return false;

                }
            
                function transDetailExcel(){
                    $("#transdetailForm").attr("action","<c:url value="/trans/basic_merchTransDetailListExcel" />");
                    document.getElementById("transdetailForm").submit();            
                }
                
                function Trans_MgrAction(tran_seq,result_cd ) 
                {          
                    if(result_cd == '0000')
                    {    
                        var strUrl = "../app/appResult?tran_seq="+tran_seq+"&tran_result_cd="+result_cd+"&auctioncate=1";
                        var winx = window.open(strUrl,"결제내역", "width=400,height=700,scrollbars=yes,resizeable=no");
                        winx.focus();
                    }
                }
                
                function transdetail_setSens(id, k) {
                    // update range
                    if (k == "min") {
                        transdetailCalendar1.setSensitiveRange(transdetail_byId(id).value, null);
                    } else {
                        transdetailCalendar1.setSensitiveRange(null, transdetail_byId(id).value);
                    }
                }

                function transdetail_byId(id) {
                    return document.getElementById(id);
                }
                
                
               
            function makepage(startIndex) 
            {
                var pagingHTML 		= "";
		var page 		= parseInt($("#page_no").val());
		var totalCount		= parseInt($("#total_cnt").val());
                
		var pageBlock		= parseInt($("#page_size").val());
		var navigatorNum    = 10;
		var firstPageNum	= 1;
		var lastPageNum		= Math.floor((totalCount-1)/pageBlock) + 1;
		var previewPageNum  = page == 1 ? 1 : page-1;
		var nextPageNum		= page == lastPageNum ? lastPageNum : page+1;
		var indexNum		= startIndex <= navigatorNum  ? 0 : parseInt((startIndex-1)/navigatorNum) * navigatorNum;
				
		if (totalCount > 1) 
                {
					
                    if (startIndex > 1) 
                    {
                        pagingHTML += "<a href='#' id='"+firstPageNum+"'><img src='../resources/images/btn_first.gif' width='11' height='11'/></a> ";
			pagingHTML += "<a href='#' id='"+previewPageNum+"'><img src='../resources/images/btn_prev.gif' width='11' height='11'/></a> ";
                    }
		
                    for (var i=1; i<=navigatorNum; i++) 
                    {
			var pageNum = i + indexNum;
					
			if (pageNum == startIndex) 
                            pagingHTML += " <a href='#' id='"+pageNum+"'>"+pageNum+"</a> ";
			else 
                            pagingHTML += " <a href='#' id='"+pageNum+"'>"+pageNum+"</a> ";
					
			if (pageNum==lastPageNum)
			break;
		    }
					
		    if (startIndex < lastPageNum) {
			pagingHTML += " <a href='#' id='"+nextPageNum+"'><img src='../resources/images/btn_next.gif' width='11' height='11'/></a> ";
			pagingHTML += " <a href='#' id='"+lastPageNum+"'><img src='../resources/images/btn_end.gif' width='11' height='11'/></a>";
		    }
					
		}
				
				
		$("#paging").html(pagingHTML);
		
		$("#paging a").click(function (e) {
			paging_move($(this).attr('id'));
		});

	}        
        
        function paging_move(pagenum)
        {
            //alert(pagenum);
            $("#page_no").val(pagenum);
             document.transdetailForm.submit();
        }                        
                
            </script>