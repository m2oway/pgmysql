<%-- 
    Document   : codeMaster
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
<jsp:include page="/WEB-INF/view/common/common.jsp"/>
<html>
    <head>
        <title>신용거래조회</title>             
    </head>
    <body>
</c:if>
            
        <div class="right" id="trans_search" style="width:100%;">       
            <form id="transForm" method="POST" onsubmit="return false" action="<c:url value="/trans/transMasterList" />" modelAttribute="transFormBean"> 
                <!--
                <input type="hidden" name="page_size" value="100" >
                <input type="hidden" name="page_no" value="1" >
                -->
                <table>
                    <tr>
                        <div class="label">검색일자</div>
                        <div class="input">
                            <input type="text" name="app_start_dt" id="transForm_from_date" value ="${transFormBean.app_start_dt}" onclick="trans_setSens('transForm_to_date', 'max');" /> ~
                            <input type="text" name="app_end_dt" id="transForm_to_date" value="${transFormBean.app_end_dt}" onclick="trans_setSens('transForm_from_date', 'min');" /> </div>
                            <div class ="input"><input type="button" name="week" value="1주"/></div>
                            <div class ="input"><input type="button" name="month" value="1달"/></div>                       
                        <div class="label">결제채널</div>
                        <div class="input">
                            <select name="pay_chn_cate">
                                <option value="">전체</option>
                                <c:forEach var="code" items="${payChnCateList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <td><input type="button" name="trans_search" value="조회"/></td>                        
                        <td><input type="button" name="trans_excel" value="엑셀"/></td>
                        <td><input type="button" name="trans_init" value="검색조건지우기"/></td>
                    </tr>
               </table>
            </form>
            <div class="paging">
                <div id="transPaging" style="width: 50%;"></div>
                <div id="transrecinfoArea" style="width: 50%;"></div>
            </div>        
        </div>
        
        <script type="text/javascript">
            
            var transgrid={};
            var transCalendar;
            
            $(document).ready(function () {
                
                transCalendar = new dhtmlXCalendarObject(["transForm_from_date","transForm_to_date"]);    
                
                var trans_searchForm = document.getElementById("trans_search");
    
                trans_layout = main_trans_layout.cells('a').attachLayout("2E");
                     
                trans_layout.cells('a').hideHeader();
                trans_layout.cells('b').hideHeader();
                 
                trans_layout.cells('a').attachObject(trans_searchForm);
                trans_layout.cells('a').setHeight(40);
                
                transgrid = trans_layout.cells('b').attachGrid();
                
                transgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");

                var transheaders = "";
                transheaders += "<center>일자</center>,<center>결제채널</center>,<center>지급ID명</center>,<center>승인금액</center>,<center>취소금액</center>,<center>승인VAT</center>,<center>취소VAT</center>,<center>승인건</center>,<center>취소건</center>";
                transgrid.setHeader(transheaders);
                transgrid.attachHeader("#select_filter,#select_filter,#select_filter,&nbsp,#cspan,#cspan,#cspan,#cspan,#cspan");
                transgrid.setColAlign("center,center,center,right,right,right,right,right,right");
                transgrid.setColTypes("txt,txt,txt,edn,edn,edn,edn,edn,edn");
                transgrid.setInitWidths("100,150,150,100,100,100,100,100,100");
                transgrid.setColSorting("str,str,str,int,int,int,int,int,int");
                transgrid.setNumberFormat("0,000",3);
                transgrid.setNumberFormat("0,000",4);
                transgrid.setNumberFormat("0,000",5);
                transgrid.setNumberFormat("0,000",6);
                transgrid.setNumberFormat("0,000",7);
                transgrid.setNumberFormat("0,000",8);
                transgrid.enableColumnMove(true);
                transgrid.setSkin("dhx_skyblue");
                
                var mfooters_filter = "소계,#cspan,#cspan";
	            mfooters_filter += ",<div id='trans_sub_master3'>0</div>,<div id='trans_sub_master4'>0</div>,<div id='trans_sub_master5'>0</div>,<div id='trans_sub_master6'>0</div>,<div id='trans_sub_master7'>0</div>,<div id='trans_sub_master8'>0</div>";
	        transgrid.attachFooter(mfooters_filter);
                

                var transfooters = "총계,#cspan,#cspan";
                transfooters += ",<div id='trans_master3'>0</div>,<div id='trans_master4'>0</div>,<div id='trans_master5'>0</div>,<div id='trans_master6'>0</div>,<div id='trans_master7'>0</div>,<div id='trans_master8'>0</div>";
                transgrid.attachFooter(transfooters);                    

/*
                transgrid.enablePaging(true,Number($("#transForm input[name=page_size]").val()),10,"transPaging",true,"transrecinfoArea");
                transgrid.setPagingSkin("bricks");    
                
                transgrid.attachEvent("onPageChanged", function(ind,fInd,lInd){

 */ 
 /*
                    if(lInd !==0){
                        $("#transForm input[name=page_no]").val(ind);
                        transSearch();
                    }else{
                        return false;
                    }
*/                    
/*
                    if(lInd !==0){
                      $("#transForm input[name=page_no]").val(ind);      
                       transgrid.clearAll();
                        transSearch();
                    }else{
                       $("#transForm input[name=page_no]").val(1);
                        transgrid.clearAll();
                         transSearch();
                    }                    
                    
                });                    
*/
                transgrid.init();
                transgrid.parse(${ls_transMasterList}, transFooterValues,"json");

                transgrid.attachEvent("onRowSelect", transMaster_open_attach);

                transgrid.attachEvent("onRowDblClicked", trans_attach);
                
                transgrid.attachEvent("onGridReconstructed", function(){
	  	          
	            	var trans_master_sub_filter3 = document.getElementById("trans_sub_master3");
			var trans_master_sub_filter4 = document.getElementById("trans_sub_master4");
			var trans_master_sub_filter5 = document.getElementById("trans_sub_master5");
                        var trans_master_sub_filter6 = document.getElementById("trans_sub_master6");
                        var trans_master_sub_filter7 = document.getElementById("trans_sub_master7");
                        var trans_master_sub_filter8 = document.getElementById("trans_sub_master8");
                        
	            	
	            	var rowId = transgrid.getAllRowIds();
	            	
	            	var tranmastersubsum_3 = 0;
                        var tranmastersubsum_4 = 0;
                        var tranmastersubsum_5 = 0;
                        var tranmastersubsum_6 = 0;
                        var tranmastersubsum_7 = 0;
                        var tranmastersubsum_8 = 0;
                        
	    			if(rowId != ''){
	    				var rowIdArr = rowId.split(",");
	    				for(var i=0;i<rowIdArr.length;i++){
                                                
                                                tranmastersubsum_3 += Number(transgrid.cells(rowIdArr[i],3).getValue());
                                                tranmastersubsum_4 += Number(transgrid.cells(rowIdArr[i],4).getValue());
                                                tranmastersubsum_5 += Number(transgrid.cells(rowIdArr[i],5).getValue());
                                                tranmastersubsum_6 += Number(transgrid.cells(rowIdArr[i],6).getValue());
                                                tranmastersubsum_7 += Number(transgrid.cells(rowIdArr[i],7).getValue());
                                                tranmastersubsum_8 += Number(transgrid.cells(rowIdArr[i],8).getValue());
	    				}
	    			}
	    				
	    			
				    trans_master_sub_filter3.innerHTML =  putComma(tranmastersubsum_3);
				    trans_master_sub_filter4.innerHTML =  putComma(tranmastersubsum_4);
				    trans_master_sub_filter5.innerHTML =  putComma(tranmastersubsum_5);
                                    trans_master_sub_filter6.innerHTML =  putComma(tranmastersubsum_6);
				    trans_master_sub_filter7.innerHTML =  putComma(tranmastersubsum_7);
				    trans_master_sub_filter8.innerHTML =  putComma(tranmastersubsum_8);
				    				    
				    return true;
	            	
	            });                 
                

                //신용거래 정보 검색 이벤트
                $("#transForm input[name=trans_search]").click(function () {
                    
                    //$("input[name=page_no]").val("1");
                    transSearch();
                    
                   /*
                    transgrid.changePage(1);
                    return false;
                    */
                });
                
                //신용거래 정보 엑셀다운로드 이벤트
                $("#transForm input[name=trans_excel]").click(function () {
                    
                    transExcel();
                    
                })
                
                $("#transForm input[name=week]").click(function(){
                    trans_date_search("week");
                });
                    
                $("#transForm input[name=month]").click(function(){
                    trans_date_search("month");
                });     
                
                //검색조건 초기화
                $("#transForm input[name=trans_init]").click(function () {
                    
                    transMasterInit($("#transForm"));
                    
                });                
                  
            });
            
            function transFooterValues() {
                
                var trans_master3 = document.getElementById("trans_master3");
                var trans_master4 = document.getElementById("trans_master4");
                var trans_master5 = document.getElementById("trans_master5");
                var trans_master6 = document.getElementById("trans_master6");
                var trans_master7 = document.getElementById("trans_master7");
                var trans_master8 = document.getElementById("trans_master8");

                trans_master3.innerHTML =  putComma(sumColumn(3)) ;    
                trans_master4.innerHTML =  putComma(sumColumn(4)) ;
                trans_master5.innerHTML =  putComma(sumColumn(5)) ;
                trans_master6.innerHTML =  putComma(sumColumn(6)) ;
                trans_master7.innerHTML =  putComma(sumColumn(7)) ;
                trans_master8.innerHTML =  putComma(sumColumn(8)) ;

                return true;

            }
		
            function sumColumn(ind) {
                var out = 0;
                for (var i = 0; i < transgrid.getRowsNum(); i++) {
                     out += parseFloat(transgrid.cells2(i, ind).getValue());
                }

                return out;
            }

            //신용거래 상세조회 이벤트
            function trans_attach(rowid, col) {

                var app_dt = transgrid.getUserData(rowid, 'app_dt');
                var pay_chn_cate = transgrid.getUserData(rowid, 'pay_chn_cate');
                var onffmerch_no = transgrid.getUserData(rowid, 'onffmerch_no');
                
                main_trans_layout.cells('b').attachURL("<c:url value="/trans/transDetailList" />"+"?app_start_dt=" + app_dt + "&app_end_dt=" + app_dt + "&pay_chn_cate=" + pay_chn_cate + "&onffmerch_no=" + onffmerch_no, true);
                
            }
            
            //신용거래 정보 조회
            function transSearch() {
                
                $.ajax({
                    url: "<c:url value="/trans/transMasterList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#transForm").serialize(),
                    success: function (data) {

                        transgrid.clearAll();
                        transgrid.parse($.parseJSON(data),transFooterValues, "json");

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }
            
            function trans_setSens(id, k) {
                // update range
                if (k == "min") {
                    transCalendar.setSensitiveRange(trans_byId(id).value, null);
                } else {
                    transCalendar.setSensitiveRange(null, trans_byId(id).value);
                }
            }

            function trans_byId(id) {
                return document.getElementById(id);
            }          
            
            //신용거래 엑셀다운로드
            function transExcel() {

                $("#transForm").attr("action","<c:url value="/trans/transMasterListExcel" />");
                document.getElementById("transForm").submit();

            }       
            
            function trans_date_search(day){
            
                var oneDate = 1000 * 3600 * 24; 
                var now = new Date();
                var week = new Date(now.getTime() + (oneDate*-6));
                var month = new Date(now.getTime() + (oneDate*-31));
                var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                var date=new Date();

                if(day=="today"){
                    $("#transForm input[id=transForm_from_date]").val(nowTime);
                    $("#transForm input[id=transForm_to_date]").val(nowTime);
                }else if(day=="week"){
                    $("#transForm input[id=transForm_from_date]").val(weekTime);
                    $("#transForm input[id=transForm_to_date]").val(nowTime);
                }else{
                    $("#transForm input[id=transForm_from_date]").val(monthTime);
                    $("#transForm input[id=transForm_to_date]").val(nowTime);
                }
            
            }
                
            //검색조건 초기화
            function transMasterInit($form) {

                searchFormInit($form);

                trans_date_search("week");                    
            } 
            
            var dhxSelPopupWins=new dhtmlXWindows();
            
            //ONOFFUID 팝업 클릭 이벤트
            $("#transForm input[name=onfftid]").unbind("click").bind("click", function (){

                onoffTransDetailTidSelectPopup();

            });                                

            function onoffTransDetailTidSelectPopup(){
                onoffObject.onfftid_nm = $("#transForm input[name=onfftid_nm]");
                onoffObject.onfftid = $("#transForm input[name=onfftid]");
                //ONOFFUID 팝업
                w2 = dhxSelPopupWins.createWindow("OnofftidSelectPopUp", 20, 30, 640, 480);
                w2.setText("ONOFFUID 선택페이지");
                dhxSelPopupWins.window('OnofftidSelectPopUp').setModal(true);
                dhxSelPopupWins.window('OnofftidSelectPopUp').denyResize();
                w2.attachURL('<c:url value = "/popup/OnofftidSelectPopUp"/>', true);   
            }  
            
            
            var tranmstpopWindow;
            function transMaster_open_attach(rowid, col) {
                
                if(col == '2')
                {
                    tranmstpopWindow = new dhtmlXWindows();
                    var onffmerchno = transgrid.getUserData(rowid, 'onffmerch_no');
                    w1 = tranmstpopWindow.createWindow("w1", 150, 150,  800, 700);
                    w1.setText("지급ID 정보 수정");
                    tranmstpopWindow.window('w1').setModal(true);
                    w1.attachURL("<c:url value="/merchant/merchantMasterUpdate" />" + "?onffmerch_no=" + onffmerchno+"&popupchk=1",true);
                    
                    transgrid.clearSelection();
                }
                
                return false;
                        
            }                
                
        </script>

<c:if test="${!ajaxRequest}">
    </body>
</html>
</c:if>        
