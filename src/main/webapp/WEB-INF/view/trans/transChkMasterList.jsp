<%-- 
    Document   : codeMaster
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
<jsp:include page="/WEB-INF/view/common/common.jsp"/>
<html>
    <head>
        <title>신용거래조회</title>             
    </head>
    <body>
</c:if>
            
        <div class="right" id="transChkMaster_search" style="width:100%;">       
            <form id="transChkMasterForm" method="POST" onsubmit="return false" action="<c:url value="/trans/transChkMasterList" />" modelAttribute="transChkMasterFormBean"> 
                <input type="hidden" name="page_size" value="100" >
                <input type="hidden" name="page_no" value="1" >
                <table>
                    <tr>
                        <div class="label">검색일자</div>
                        <div class="input">
                            <input type="text" name="app_start_dt" size="10" id="transChkMasterForm_from_date" value ="${transChkMasterFormBean.app_start_dt}" onclick="transChkMaster_setSens('transChkMasterForm_to_date', 'max');" /> ~
                            <input type="text" name="app_end_dt" size="10" id="transChkMasterForm_to_date" value="${transChkMasterFormBean.app_end_dt}" onclick="transChkMaster_setSens('transChkMasterForm_from_date', 'min');" /> </div>
                            <div class ="input"><input type="button" name="week" value="1주"/></div>
                            <div class ="input"><input type="button" name="month" value="1달"/></div>                       
                            <div class="label">지급ID명</div>
                            <div class="input">
                                <input type="text" name="merch_nm" size="10" value ="${transFormBean.merch_nm}" />
                            </div>        
                            <div class="label">지급ID</div>
                            <div class="input">
                                <input type="text" name="onffmerch_no" size="10" value ="${transFormBean.onffmerch_no}" />
                            </div>                              
                            <div class="label">UID명</div>
                            <div class="input">
                                <input type="text" name="onfftid_nm" size="10" value ="${transFormBean.onfftid_nm}" />
                            </div>    
                            <div class="label">UID</div>
                            <div class="input">
                                <input type="text" name="onfftid"  size="10" value ="${transFormBean.onfftid}" />
                            </div>  
                            <div class="label">카드번호</div>
                            <div class="input">
                                <input type="text" size="20" name="card_num" value ="${transFormBean.card_num}" />
                            </div>        
                            <div class="label">중복건수</div>
                            <div class="input">
                                <input type="text"size="5" name="chktran_cnt" value ="${transFormBean.chktran_cnt}" />
                            </div>                              

                        <td><input type="button" name="transChkMaster_search" value="조회"/></td>                        
                        <td><input type="button" name="transChkMaster_init" value="검색조건지우기"/></td>
                    </tr>
               </table>
            </form>
            <div class="paging">
                <div id="transChkMasterPaging" style="width: 50%;"></div>
                <div id="transChkMasterrecinfoArea" style="width: 50%;"></div>
            </div>                        
        </div>
        
        <script type="text/javascript">
            
            var transChkMastergrid={};
            var transChkMasterCalendar;
            
            $(document).ready(function () {
                
                transChkMasterCalendar = new dhtmlXCalendarObject(["transChkMasterForm_from_date","transChkMasterForm_to_date"]);    
                
                var transChkMaster_searchForm = document.getElementById("transChkMaster_search");
    
                transChkMaster_layout = main_transchk_layout.cells('a').attachLayout("2E");
                     
                transChkMaster_layout.cells('a').hideHeader();
                transChkMaster_layout.cells('b').hideHeader();
                 
                transChkMaster_layout.cells('a').attachObject(transChkMaster_searchForm);
                transChkMaster_layout.cells('a').setHeight(55);
                
                transChkMastergrid = transChkMaster_layout.cells('b').attachGrid();
                
                transChkMastergrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");

                var transChkMasterheaders = "";
                transChkMasterheaders += "<center>승인일</center>,<center>지급ID명</center>,<center>UID명</center>,<center>거래종류</center>,<center>카드번호</center>,<center>건수</center>";
                transChkMastergrid.setHeader(transChkMasterheaders);
                transChkMastergrid.setColAlign("center,center,center,center,center,right");
                transChkMastergrid.setColTypes("txt,txt,txt,txt,txt,edn");
                transChkMastergrid.setInitWidths("80,150,150,80,100,80");
                transChkMastergrid.setColSorting("str,str,str,str,str,int");
                transChkMastergrid.setNumberFormat("0,000",5);
                transChkMastergrid.enableColumnMove(true);
                transChkMastergrid.setSkin("dhx_skyblue");
        

                transChkMastergrid.enablePaging(true,Number($("#transChkMasterForm input[name=page_size]").val()),10,"transChkMasterPaging",true,"transChkMasterrecinfoArea");
                transChkMastergrid.setPagingSkin("bricks");    
                
                transChkMastergrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                
                    if(lInd !==0){
                      $("#transChkMasterForm input[name=page_no]").val(ind);      
                       transChkMastergrid.clearAll();
                        transChkMasterSearch();
                    }else{
                       $("#transChkMasterForm input[name=page_no]").val(1);
                        transChkMastergrid.clearAll();
                         transChkMasterSearch();
                    }                    
                    
                });                    

                transChkMastergrid.init();
                transChkMastergrid.parse(${ls_transMasterList}, "json");

                transChkMastergrid.attachEvent("onRowDblClicked", transChkMaster_attach);

                //신용거래 정보 검색 이벤트
                $("#transChkMasterForm input[name=transChkMaster_search]").click(function () {
                    /*
                    $("input[name=page_no]").val("1");
                    transChkMasterSearch();
                    */
                    transChkMastergrid.changePage(1);
                    return false;
                });
                
                
                $("#transChkMasterForm input[name=week]").click(function(){
                    transChkMaster_date_search("week");
                });
                    
                $("#transChkMasterForm input[name=month]").click(function(){
                    transChkMaster_date_search("month");
                });     
                
                //검색조건 초기화
                $("#transChkMasterForm input[name=transChkMaster_init]").click(function () {
                    
                    transChkMasterMasterInit($("#transChkMasterForm"));
                    
                });                
                  
            });
            
        
		

            //신용거래 상세조회 이벤트
            function transChkMaster_attach(rowid, col) {

                var app_dt = transChkMastergrid.getUserData(rowid, 'app_dt');
                var onffmerch_no = transChkMastergrid.getUserData(rowid, 'onffmerch_no');
                var onfftid = transChkMastergrid.getUserData(rowid, 'onfftid');
                var massagetype = transChkMastergrid.getUserData(rowid, 'massagetype');
                var card_num = transChkMastergrid.getUserData(rowid, 'card_num');
                
                main_transchk_layout.cells('b').attachURL("<c:url value="/trans/transChkDetailList" />"+"?app_start_dt=" + app_dt + "&app_end_dt=" + app_dt + "&onffmerch_no=" + onffmerch_no + "&onfftid=" + onfftid+ "&massagetype=" + massagetype+ "&card_num=" + card_num, true);
                
            }
            
            //신용거래 정보 조회
            function transChkMasterSearch() {
                
                $.ajax({
                    url: "<c:url value="/trans/transChkMasterList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#transChkMasterForm").serialize(),
                    success: function (data) {

                        //transChkMastergrid.clearAll(1);
                        transChkMastergrid.parse($.parseJSON(data), "json");

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }
            
            function transChkMaster_setSens(id, k) {
                // update range
                if (k == "min") {
                    transChkMasterCalendar.setSensitiveRange(transChkMaster_byId(id).value, null);
                } else {
                    transChkMasterCalendar.setSensitiveRange(null, transChkMaster_byId(id).value);
                }
            }

            function transChkMaster_byId(id) {
                return document.getElementById(id);
            }          
            
            //신용거래 엑셀다운로드
            function transChkMasterExcel() {

                $("#transChkMasterForm").attr("action","<c:url value="/transChkMaster/transChkMasterListExcel" />");
                document.getElementById("transChkMasterForm").submit();

            }       
            
            function transChkMaster_date_search(day){
            
                var oneDate = 1000 * 3600 * 24; 
                var now = new Date();
                var week = new Date(now.getTime() + (oneDate*-6));
                var month = new Date(now.getTime() + (oneDate*-31));
                var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                var date=new Date();

                if(day=="today"){
                    $("#transChkMasterForm input[id=transChkMasterForm_from_date]").val(nowTime);
                    $("#transChkMasterForm input[id=transChkMasterForm_to_date]").val(nowTime);
                }else if(day=="week"){
                    $("#transChkMasterForm input[id=transChkMasterForm_from_date]").val(weekTime);
                    $("#transChkMasterForm input[id=transChkMasterForm_to_date]").val(nowTime);
                }else{
                    $("#transChkMasterForm input[id=transChkMasterForm_from_date]").val(monthTime);
                    $("#transChkMasterForm input[id=transChkMasterForm_to_date]").val(nowTime);
                }
            
            }
                
            //검색조건 초기화
            function transChkMasterMasterInit($form) {

                searchFormInit($form);

                transChkMaster_date_search("week");                    
            } 
            
 
                
        </script>

<c:if test="${!ajaxRequest}">
    </body>
</html>
</c:if>        
