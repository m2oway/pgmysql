<%-- 
    Document   : cashtransDetailList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>현금 상세조회</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">

                var cashtransdetailgrid={};
                var cashtransdetail_layout={};
                var cashtransdetailCalendar;
                
                $(document).ready(function () {
                    
                    cashtransdetailCalendar = new dhtmlXCalendarObject(["cashtransForm_date","cashtransto_date"]);    
                
                    var cashcashtransdetailForm = document.getElementById("cashtransdetail_search");
                    cashtransdetail_layout = main_cashtrans_layout.cells('b').attachLayout("2E");
                    cashtransdetail_layout.cells('a').hideHeader();
                    cashtransdetail_layout.cells('b').hideHeader();
                    cashtransdetail_layout.cells('a').attachObject(cashcashtransdetailForm);
                    cashtransdetail_layout.cells('a').setHeight(80);
                    cashtransdetailgrid = cashtransdetail_layout.cells('b').attachGrid();
                    cashtransdetailgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");

                    var transdetailheaders = "";
                    transdetailheaders +=  "<center>NO</center>,<center>지급ID명</center>,<center>UID명</center>,<center>고객명</center>,<center>고객전화번호</center>";
                    transdetailheaders += ",<center>상품명</center>,<center>거래종류</center>,<center>인증번호</center>,<center>발행구분</center>,<center>인증구분</center>";
                    transdetailheaders += ",<center>승인금액</center>,<center>비과세금액</center>,<center>부가세</center>,<center>승인번호</center>,<center>승인일시</center>";
                    transdetailheaders += ",<center>취소일</center>,<center>거래상태</center>,<center>거래결과</center>,<center>민원</center>,<center>영수증</center>";
                    transdetailheaders += ",<center>취소</center>,<center>결제채널</center>,<center>결과코드</center>,<center>결과메시지</center>,<center>TID</center>";
                    transdetailheaders += ",<center>결제IP</center>,<center>결제자ID</center>";
                    cashtransdetailgrid.setHeader(transdetailheaders);
                    var transdetailColAlign = "";
                    transdetailColAlign +=  "center,center,center,center,center";
                    transdetailColAlign += ",center,center,center,center,center";
                    transdetailColAlign += ",right,right,right,center,center";
                    transdetailColAlign += ",center,center,center,center,center";
                    transdetailColAlign += ",center,center,center,center,center";
                    transdetailColAlign += ",center,center";
                    cashtransdetailgrid.setColAlign(transdetailColAlign);
                    var transdetailColTypes = "";
                    transdetailColTypes +=  "txt,txt,txt,txt,txt";
                    transdetailColTypes += ",txt,txt,txt,txt,txt";
                    transdetailColTypes += ",edn,edn,edn,txt,txt";
                    transdetailColTypes += ",txt,txt,txt,txt,txt";
                    transdetailColTypes += ",txt,txt,txt,txt,txt";
                    transdetailColTypes += ",txt,txt";
                    cashtransdetailgrid.setColTypes(transdetailColTypes);
                    var transdetailInitWidths = "";
                    transdetailInitWidths +=   "50,100,100,80,90";
                    transdetailInitWidths += ",150,80,100,80,80";
                    transdetailInitWidths += ",90,90,90,90,100";
                    transdetailInitWidths += ",90,90,90,100,100";
                    transdetailInitWidths += ",100,150,150,150,150";
                    transdetailInitWidths += ",150,150";
                    cashtransdetailgrid.setInitWidths(transdetailInitWidths);
                    var transdetailColSorting = "";
                    transdetailColSorting +=  "str,str,str,str,str";
                    transdetailColSorting += ",str,str,str,str,str";
                    transdetailColSorting += ",int,int,int,str,str";
                    transdetailColSorting += ",str,str,str,str,str";
                    transdetailColSorting += ",str,str,str,str,str";
                    transdetailColSorting += ",str,str";
                    cashtransdetailgrid.setColSorting(transdetailColSorting);
                    cashtransdetailgrid.setNumberFormat("0,000",10);
                    cashtransdetailgrid.setNumberFormat("0,000",11);
                    cashtransdetailgrid.setNumberFormat("0,000",12);
                    cashtransdetailgrid.enableColumnMove(true);
                    cashtransdetailgrid.setSkin("dhx_skyblue");
                    cashtransdetailgrid.enablePaging(true,Number($("#cashcashtransdetailForm input[name=page_size]").val()),10,"cashtransdetailPaging",true,"cashtransdetailrecinfoArea");
                    cashtransdetailgrid.setPagingSkin("bricks");
                    
                    cashtransdetailgrid.setColumnHidden(18,true);  

                    //페이징 처리
                    cashtransdetailgrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        /*
                        if(lInd !==0){
                            $("#cashcashtransdetailForm input[name=page_no]").val(ind);
                            cashtransdetailSearch();
                        }else{
                            return false;
                        }
                        */
                        if(lInd !==0){
                           $("#cashcashtransdetailForm input[name=page_no]").val(ind);      
                           cashtransdetailgrid.clearAll();
                             cashtransdetailSearch();
                        }else{
                            $("#cashcashtransdetailForm input[name=page_no]").val(1);
                            cashtransdetailgrid.clearAll();
                              cashtransdetailSearch();
                        }                               
                    });

                    cashtransdetailgrid.init();
                    cashtransdetailgrid.parse(${ls_cashtransDetailList}, "json");
                    
                    <c:if test="${transFormBean.ses_user_cate == '00' ||transFormBean.ses_user_cate == '01' || transFormBean.ses_user_cate == '02'}">
                    //cashtransdetailgrid.attachEvent("onRowDblClicked", Trans_MgrAction);
                    </c:if>      
                    //영수증보기
                    $("button[name=cashbill_view_bt]").unbind("click").bind("click", function (){

                        var tptranseq =  $(this).attr("tran_seq");
                        var tpresult_cd =  $(this).attr("tran_result_cd"); 
                        if(tpresult_cd == '0000')
                        {    
                            var strUrl = "../app/appCashResult?tran_seq="+tptranseq+"&tran_result_cd="+tpresult_cd+"&auctioncate=1";
                            var winx = window.open(strUrl,"결제내역", "width=400,height=700,scrollbars=yes,resizeable=no");
                            winx.focus();
                        }                        
                        
                        return false;

                    });                            
                        
                    //취소버튼
                    $("button[name=cncl_cash_view_bt]").unbind("click").bind("click", function (){

                        var tptranseq = $(this).attr("tran_seq");
                        var tporgcno =  $(this).attr("org_cno"); 
                        
                        if(confirm("승인거래를 취소하시겠습니까?"))
                        {
                            
                            $("#frm_cash_cncl input[id=tran_seq]").val(tptranseq);
                            $("#frm_cash_cncl input[id=org_cno]").val(tporgcno);
                            
                             $.ajax({
                                        url : "../app/mgrAction",
                                        type : "POST",
                                        async : true,
                                        dataType : "json",
                                        data: $("#frm_cash_cncl").serialize(),
                                        success : function(data) {
                                            if(data.result_cd == '0000')
                                            {
                                                alert(data.result_msg);
                                                cashtransdetailgrid.clearAll();
                                                cashtransdetailSearch();
                                            }
                                            else
                                            {
                                                alert("[" +data.result_cd+ "]" + data.result_msg);
                                            }

                                        },
                                        error : function() { 
                                            alert("취소 실패");
                                        }
                                    });	            
                        }
                        
                        return false;

                    });                       
                    
                    //상세코드검색
                    $("#cashcashtransdetailForm input[name=cashtransdetail_search]").click(function(){
                        /*
                        $("input[name=page_no]").val("1");

                        cashtransdetailgrid.clearAll();

                        cashtransdetailSearch();
                        */
                        cashtransdetailgrid.changePage(1);
                        return false;  
                    });
                    $("#cashcashtransdetailForm input[name=week]").click(function(){
                        cashtrans_date_search("week");
                    });

                    $("#cashcashtransdetailForm input[name=month]").click(function(){
                        cashtrans_date_search("month");
                    });
                    
                    //현금거래 정보 엑셀다운로드 이벤트
                    $("#cashcashtransdetailForm input[name=cashtransdetail_excel]").click(function () {

                        cashTransDetailExcel();

                    });            
                    $("#cashcashtransdetailForm input[name=init]").click(function () {
                    
                        transCashDetailMasterInit($("#cashcashtransdetailForm"));

                    }); 
                    
                });

        //코드 정보 조회
                function cashtransdetailSearch() {
                
                $.ajax({
                    url: "<c:url value="/trans/cashtransDetailList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#cashcashtransdetailForm").serialize(),
                    success: function (data) {

                        var jsonData = $.parseJSON(data);
                        //cashtransdetailgrid.clearAll();
                        cashtransdetailgrid.parse(jsonData,"json");
                        
                        
                            //영수증보기
                            $("button[name=cashbill_view_bt]").unbind("click").bind("click", function (){

                                var tptranseq =  $(this).attr("tran_seq");
                                var tpresult_cd =  $(this).attr("tran_result_cd"); 
                                if(tpresult_cd == '0000')
                                {    
                                    var strUrl = "../app/appCashResult?tran_seq="+tptranseq+"&tran_result_cd="+tpresult_cd+"&auctioncate=1";
                                    var winx = window.open(strUrl,"결제내역", "width=400,height=700,scrollbars=yes,resizeable=no");
                                    winx.focus();
                                }                        

                                return false;

                            });                            

                            //취소버튼
                            $("button[name=cncl_cash_view_bt]").unbind("click").bind("click", function (){

                                var tptranseq = $(this).attr("tran_seq");
                                var tporgcno =  $(this).attr("org_cno"); 

                                if(confirm("현금영수증거래를 취소하시겠습니까?"))
                                {

                                    $("#frm_cash_cncl input[id=tran_seq]").val(tptranseq);
                                    $("#frm_cash_cncl input[id=org_cno]").val(tporgcno);

                                     $.ajax({
                                                url : "../app/mgrAction",
                                                type : "POST",
                                                async : true,
                                                dataType : "json",
                                                data: $("#frm_cash_cncl").serialize(),
                                                success : function(data) {
                                                    if(data.result_cd == '0000')
                                                    {
                                                        alert(data.result_msg);
                                                        cashtransdetailgrid.clearAll();
                                                        cashtransdetailSearch();
                                                    }
                                                    else
                                                    {
                                                        alert("[" +data.result_cd+ "]" + data.result_msg);
                                                    }

                                                },
                                                error : function() { 
                                                    alert("취소 실패");
                                                }
                                            });	            
                                }

                                return false;

                            });                            
                        

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
                    return false;
            }

            var dhxSelPopupWins=new dhtmlXWindows();
                    
                //ONOFF가맹점번호 팝업 클릭 이벤트
                $("#cashcashtransdetailForm input[name=onffmerch_no]").unbind("click").bind("click", function (){

                    onoffCashMerchSelectPopup();

                });  
                
                //ONOFF가맹점번호 팝업 클릭 이벤트
                $("#cashcashtransdetailForm input[name=merch_nm]").unbind("click").bind("click", function (){

                    onoffCashMerchSelectPopup();

                });                             
                    
                function onoffCashMerchSelectPopup(){
                    onoffObject.onffmerch_no = $("#cashcashtransdetailForm input[name=onffmerch_no]");
                    onoffObject.merch_nm = $("#cashcashtransdetailForm input[name=merch_nm]");
                    
                    //ONOFF가맹점정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("지급ID 선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                }
                               
                //ONOFFUID 팝업 클릭 이벤트
                $("#cashcashtransdetailForm input[name=onfftid]").unbind("click").bind("click", function (){

                    onoffCashTidSelectPopup();

                });        
                
                $("#cashcashtransdetailForm input[name=onfftid_nm]").unbind("click").bind("click", function (){

                    onoffCashTidSelectPopup();

                });                              
                
                function onoffCashTidSelectPopup(){                    
                    onoffObject.onfftid_nm = $("#cashcashtransdetailForm input[name=onfftid_nm]");
                    onoffObject.onfftid = $("#cashcashtransdetailForm input[name=onfftid]");
                    //ONOFFUID 팝업
                    w2 = dhxSelPopupWins.createWindow("OnofftidSelectPopUp", 20, 30, 640, 480);
                    w2.setText("UID 선택페이지");
                    dhxSelPopupWins.window('OnofftidSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnofftidSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnofftidSelectPopUp"/>', true);   
                }                
              
//                function SelOnofftidInfoInput(p_onffmerch_no, p_onfftid, p_onfftid_nm){
//                    $("#cashcashtransdetailForm input[name=onfftid]").val(p_onfftid);                 
//                    $("#cashcashtransdetailForm input[name=onfftid_nm]").val(p_onfftid_nm);
//                }
                function cash_setSens(id, k) {
                    // update range
                    if (k == "min") {
                        cashtransdetailCalendar.setSensitiveRange(cash_byId(id).value, null);
                    } else {
                        cashtransdetailCalendar.setSensitiveRange(null, cash_byId(id).value);
                    }
                }

                function cash_byId(id) {
                    return document.getElementById(id);
                }
                function cashtrans_date_search(day){
                    var oneDate = 1000 * 3600 * 24; 
                    var now = new Date();
                    var week = new Date(now.getTime() + (oneDate*-6));
                    var month = new Date(now.getTime() + (oneDate*-31));
                    var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                    var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                    var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                    var date=new Date();

                    if(day=="today"){
                        $("#cashcashtransdetailForm input[id=cashtransForm_date]").val(nowTime);
                        $("#cashcashtransdetailForm input[id=cashtransto_date]").val(nowTime);
                    }else if(day=="week"){
                        $("#cashcashtransdetailForm input[id=cashtransForm_date]").val(weekTime);
                        $("#cashcashtransdetailForm input[id=cashtransto_date]").val(nowTime);
                    }else{
                        $("#cashcashtransdetailForm input[id=cashtransForm_date]").val(monthTime);
                        $("#cashcashtransdetailForm input[id=cashtransto_date]").val(nowTime);
                    }
                }
                

                <c:if test="${transFormBean.ses_user_cate == '00' ||transFormBean.ses_user_cate == '01' || transFormBean.ses_user_cate == '02'}">            
                        function Trans_MgrAction(rowId, col) 
                        {
                            var tptranseq = cashtransdetailgrid.getUserData(rowId, 'tran_seq');
                            var tpresult_cd =  cashtransdetailgrid.getUserData(rowId, 'result_cd');
                            if(tpresult_cd == '0000')
                            {    
                                var strUrl = "../app/appCashResult?tran_seq="+tptranseq+"&tran_result_cd="+tpresult_cd+"&auctioncate=1";
                                var winx = window.open(strUrl,"결제내역", "width=400,height=700,scrollbars=yes,resizeable=no");
                                winx.focus();
                            }
                        }
                </c:if>                 
                
            //현금거래 엑셀다운로드
            function cashTransDetailExcel() {

                $("#cashcashtransdetailForm").attr("action","<c:url value="/trans/cashTransDetailListExcel" />");
                document.getElementById("cashcashtransdetailForm").submit();

            }  
            function transCashDetailMasterInit($form) {

                searchFormInit($form);

                cashtrans_date_search("week");                    
            } 

        </script>

        <div class="right" id="cashtransdetail_search" style="width:100%;">
             <form id="cashcashtransdetailForm" method="POST" action="<c:url value="/trans/cashtransDetailList" />" modelAttribute="transFormBean">

                <input type="hidden" name="page_size" value="100" >
                <input type="hidden" name="page_no" value="1" >
                <input type="hidden" name="pay_chn_cate" value="${transFormBean.pay_chn_cate}" >
                
                <table>
                    <tr>
                        <div class="label">승인일</div>
                        <div class="input">
                        <input type="text" size="15" name="app_start_dt" id="cashtransForm_date" value ="${transFormBean.app_start_dt}" onclick="transdetail_setSens('cashtransto_date', 'max');" /> ~
                        <input type="text" size="15" name="app_end_dt" id="cashtransto_date" value="${transFormBean.app_end_dt}" onclick="transdetail_setSens('cashtransForm_date', 'min');" /> </div>
                        <div class ="input"><input type="button" name="week" value="1주"/></div>
                        <div class ="input"><input type="button" name="month" value="1달"/></div> 
                        <div class="label">거래상태</div>
                        <div class="input">
                            <select name="tran_status">
                                <option value="">전체</option>
                                <c:forEach var="code" items="${tranStatusList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == transFormBean.tran_status}">selected</c:if> >${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="label">거래결과</div>
                        <div class="input">
                            <select name="result_status">
                                <option value="">전체</option>
                                <c:forEach var="code" items="${resultStatusList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == transFormBean.result_status}">selected</c:if> >${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>    
                        <div class="label">지급ID명</div>
                        <div class="input">
                            <input type="text" name="merch_nm" value ="${transFormBean.merch_nm}" readonly/>
                        </div>                            
                        <div class="label">지급IDID</div>
                        <div class="input">
                            <input type="text" name="onffmerch_no" value ="${transFormBean.onffmerch_no}" readonly/>                            
                        </div>                                             
                        <div class="label">UID명</div>
                        <div class="input">
                            <input type="text" name="onfftid_nm" value ="${transFormBean.onfftid_nm}" readonly/>
                        </div>                            
                        <div class="label">UID</div>
                        <div class="input">
                            <input type="text" name="onfftid" value ="${transFormBean.onfftid}" readonly/>                            
                        </div>    
                        <div class="label">고객명</div>
                        <div class="input">
                            <input type="text" name="user_nm" value ="${transFormBean.user_nm}" />
                        </div>                               
                        <div class="label">고객전화번호</div>
                        <div class="input">
                            <input type="text" name="user_phone2" value ="${transFormBean.user_phone2}" />
                        </div>                                                             
                        <div class="label">상품명</div>
                        <div class="input">
                            <input type="text" name="product_nm" value ="${transFormBean.product_nm}" />
                        </div>                                                   
                        <div class="label">승인금액</div>
                        <div class="input">
                            <input type="text" name="tot_amt" value ="${transFormBean.tot_amt}" />
                        </div>                                               
                        <div class="label">인증번호</div>
                        <div class="input">
                            <input type="text" name="card_num" value ="${transFormBean.card_num}" />
                        </div>                                                     
                        <div class="label">승인번호</div>
                        <div class="input">
                            <input type="text" name="app_no" value ="${transFormBean.app_no}" />
                        </div>                              
                        <td>
                            <input type="button" name="cashtransdetail_search" value="조회"/>
                        </td>
                        <td>
                            <input type="button" name="cashtransdetail_excel" value="엑셀"/>
                        </td>
                        <td><input type="button" name="init" value="검색조건지우기"/></td>
                    </tr>
                </table>
                        
            </form>
            <form name="frm_cash_cncl" id="frm_cash_cncl" method="post" >
                <input type="hidden" name="org_cno" id="org_cno" value=""><input type="hidden" name="mgr_msg" id="mgr_msg" value="" ><input type="hidden" name="EP_tr_cd" id="EP_tr_cd" value="00201050"><input type="hidden" name="mgr_txtype" name="id_txtype" value="51"><input type="hidden" name="tran_seq" id="tran_seq" value="">
            </form>
            <div class="paging">
                <div id="cashtransdetailPaging" style="width: 50%;"></div>
                <div id="cashtransdetailrecinfoArea" style="width: 50%;"></div>
            </div>
                                
         </div>

    </body>
</html>
