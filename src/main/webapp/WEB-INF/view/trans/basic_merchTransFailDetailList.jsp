<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="/WEB-INF/view/common/basic_common.jsp"/>
<%@include file="/WEB-INF/view/common/basic_comm_header.jsp"%>
    <table width="100%" height="42" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top" class="titleBg">
		<table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="title"><b>-카드실패내역</b></td>
                        <td><img src="../resources/images/title_sright.gif" width="12" height="38" /></td>
                    </tr>
		</table>
            </td>
	</tr>
    </table>
        <div class="right" id="transFaildetail_search" style="width:100%;">
             <form id="transFaildetailForm" name="transFaildetailForm" method="POST" action="<c:url value="/trans/basic_merchTransFailDetailList" />" modelAttribute="transFormBean">
                    <input type="hidden" name="page_size" id="page_size" value="10" >
                    <input type="hidden" name="page_no" id="page_no" value="${transFormBean.page_no}" >
                    <input type="hidden" name="total_cnt" id="total_cnt" value="${total_count}" >
                    <input type="hidden" name="nopageflag" value="Y">                
                    <table>
                        <tr><td>
                            <div class="label">거래일</div>
                            <div class="input">
                            <input type="text" size="12" name="app_start_dt" id="app_start_dt" value ="${transFormBean.app_start_dt}" onclick="TransFail_setSens('transFaildetailForm_to_date', 'max');" /> ~
                            <input type="text"  size="12" name="app_end_dt" id="app_end_dt" value="${transFormBean.app_end_dt}" onclick="TransFail_setSens('transFailto_date', 'min');" /> </div>                            
                            <div class="label">검색항목</div>
                            <div class="input">
                                <select name="searchinputnm" id="searchinputnm" onchange="javascirpt:searchinputnmChange(this);">
                                    <option value="user_nm" <c:if test="${'user_nm' == transFormBean.searchinputnm}">selected</c:if>>고객명</option>
                                    <option value="product_nm" <c:if test="${'product_nm' == transFormBean.searchinputnm}">selected</c:if>>상품명</option>
                                    <option value="user_phone2" <c:if test="${'user_phone2' == transFormBean.searchinputnm}">selected</c:if>>전화번호</option>
                                    <option value="tot_amt" <c:if test="${'tot_amt' == transFormBean.searchinputnm}">selected</c:if>>승인금액</option>
                                    <option value="card_num" <c:if test="${'card_num' == transFormBean.searchinputnm}">selected</c:if>>카드번호</option>
                                </select>
                                <input type="text" name="searchinputcontent" id="searchinputcontent"  value ="${transFormBean.searchinputcontent}" onblur="javascirpt:searchinputcontentChange(this);" />
                            </div>
                                <input type="hidden" name="user_nm" id="user_nm" value ="${transFormBean.user_nm}" />
                                <input type="hidden" name="user_phone2" id="user_phone2"  value ="${transFormBean.user_phone2}" />
                                <input type="hidden" name="product_nm" id="product_nm"  value ="${transFormBean.product_nm}" />
                                <input type="hidden" name="tot_amt" id="tot_amt" value ="${transFormBean.tot_amt}" />
                                <input type="hidden" name="card_num" id="card_num" value ="${transFormBean.card_num}" />
                            <!---                            
                            <div class="label">고객명</div>
                            <div class="input">
                                <input type="text" name="user_nm" value ="${transFormBean.user_nm}" />
                            </div>                               
                            <div class="label">고객전화번호</div>
                            <div class="input">
                                <input type="text" name="user_phone2" value ="${transFormBean.user_phone2}" />
                            </div>                                                             
                            <div class="label">상품명</div>
                            <div class="input">
                                <input type="text" name="product_nm" value ="${transFormBean.product_nm}" />
                            </div>                                                   
                            <div class="label">승인금액</div>
                            <div class="input">
                                <input type="text" name="tot_amt" value ="${transFormBean.tot_amt}" />
                            </div>                                               
                            <div class="label">카드번호</div>
                            <div class="input">
                                <input type="text" name="card_num" value ="${transFormBean.card_num}" />
                            </div>
-->
                            </td>
                            <td><input type="button" name="transFaildetail_search" onclick="javascript:transFaildetailSearch();" value="조회"/></td>
                            <td><input type="button" name="transFaildetail_excel" onclick="javascirpt:transFaildetailExcel();" value="엑셀"/></td>
                        </tr>
                    </table>
                        
            </form>
        <div class="CSSTableGenerator" >
                <table >
                    <tr>
                        <td>No</td>
                        <td>가맹점</td>
                        <td >거래요청일</td>
                        <td >고객명</td>
                        <td>연락처</td>
                        <td>상품명</td>                        
                        <td>거래종류</td>
                        <td>카드번호</td>                        
                        <td>할부</td>                        
                        <td>승인금액</td>
                        <td>실패사유</td>                      
                    </tr>
<c:forEach var="failtransDetailList" items="${ls_transFailDetailList}" begin="0">                            
    <tr>
                        <td height="30px;" width="30px"  style="text-align:center">${failtransDetailList.rnum}</td> 
                        <td>${failtransDetailList.merch_nm}</td>
                        <td><fmt:parseDate value="${failtransDetailList.tran_dt}${failtransDetailList.tran_tm}" var="trandt1" pattern="yyyyMMddHHmmss" scope="page"/><fmt:formatDate value="${trandt1}" pattern="yyyy-MM-dd HH:mm:ss" /></td></td>
                        <td >${failtransDetailList.user_nm}</td>
                        <td>${failtransDetailList.user_phone2}</td>
                        <td>${failtransDetailList.product_nm}</td>                        
                        <td>${failtransDetailList.massagetype_nm}</td>
                        <td>${failtransDetailList.card_num}</td>                        
                        <td>${failtransDetailList.installment}</td>              
                        <td style="text-align: right"><fmt:formatNumber value="${failtransDetailList.tot_amt}" type="number"/></td>    
                        <td>${failtransDetailList.result_msg}</td>
                    </tr>
</c:forEach>
                </table>
            </div>                            
             <div id="paging" name="paging" style="margin: 15px;text-align:center;"></div>
        </div>
<%@include file="/WEB-INF/view/common/basic_comm_tail.jsp"%>    
            <script type="text/javascript">
var transFaildetailCalendar;

                $(document).ready(function () {
                    
                    transFaildetailCalendar = new dhtmlXCalendarObject(["app_start_dt","app_end_dt"]);    

                    var transFaildetailForm = document.getElementById("transFaildetail_search");


                });

            //코드 정보 조회
            function transFaildetailSearch() {
                    document.transFaildetailForm.submit();
            }
           
           function TransFail_setSens(id, k) {
                // update range
                if (k == "min") {
                    transFaildetailCalendar.setSensitiveRange(TransFail_byId(id).value, null);
                } else {
                    transFaildetailCalendar.setSensitiveRange(null, TransFail_byId(id).value);
                }
            }

            function TransFail_byId(id) {
                return document.getElementById(id);
            }  
                
                
                function TransFail_setSens(id, k) {
                // update range
                if (k == "min") {
                    transFailCalendar.setSensitiveRange(TransFail_byId(id).value, null);
                } else {
                    transFailCalendar.setSensitiveRange(null, TransFail_byId(id).value);
                }
            }                
                
            function transFaildetailExcel(){
                    $("#transFaildetailForm").attr("action","<c:url value="/trans/basic_merchTransFailDetailListExcel" />");
                    document.getElementById("transFaildetailForm").submit();            
            } 
               

                function searchinputcontentChange(selobj)
                {
                    
                    var selvalue = selobj.value;
                    var selnm = $("#searchinputnm").val();
                   
                    if(selnm == "user_nm")
                    {
                        $("#user_nm").val(selvalue);
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                    }
                    else if(selnm == "user_phone2")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val(selvalue);
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                    }
                    else if(selnm == "product_nm")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val(selvalue);
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                    }
                    else if(selnm == "tot_amt")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val(selvalue);
                        $("#card_num").val("");
                    }
                    else if(selnm == "card_num")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val(selvalue);
                    }               
                }
                
                function searchinputnmChange(selobj)
                {
                    var selnm = selobj.value;
                    var salval = $("#searchinputcontent").val();
                    
                    if(selnm == "user_nm")
                    {
                        $("#user_nm").val(salval);
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                    }
                    else if(selnm == "user_phone2")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val(salval);
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                    }
                    else if(selnm == "product_nm")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val(salval);
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                    }
                    else if(selnm == "tot_amt")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val(salval);
                        $("#card_num").val("");
                    }
                    else if(selnm == "card_num")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val(salval);
                    }
                     
                }
    </script>