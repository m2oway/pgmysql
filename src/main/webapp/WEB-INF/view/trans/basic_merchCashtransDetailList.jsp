<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="/WEB-INF/view/common/basic_common.jsp"/>
<%@include file="/WEB-INF/view/common/basic_comm_header.jsp"%>
    <table width="100%" height="42" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top" class="titleBg">
		<table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="title"><b>-현금영수증내역</b></td>
                        <td><img src="../resources/images/title_sright.gif" width="12" height="38" /></td>
                    </tr>
		</table>
            </td>
	</tr>
    </table>
        <div class="right" id="cashtransdetail_search" style="width:100%;">
             <form id="cashcashtransdetailForm" name="cashcashtransdetailForm" method="POST" action="<c:url value="/trans/basic_merchCashtransDetailList" />" modelAttribute="transFormBean">
                <input type="hidden" name="page_size" id="page_size" value="10" >
                <input type="hidden" name="page_no" id="page_no" value="${transFormBean.page_no}" >
                <input type="hidden" name="total_cnt" id="total_cnt" value="${total_count}" >
                <input type="hidden" name="pay_chn_cate" value="${transFormBean.pay_chn_cate}" >
                <input type="hidden" name="nopageflag" value="Y">
                <table>
                    <tr><td>
                        <div class="label">승인일</div>
                        <div class="input">
                        <input type="text" name="app_start_dt" size="15" id="cashtransForm_date" value ="${transFormBean.app_start_dt}" onclick="javascirpt:cash_setSens('cashtransto_date', 'max');" /> ~
                        <input type="text" name="app_end_dt" size="15" id="cashtransto_date" value="${transFormBean.app_end_dt}" onclick="javascirpt:cash_setSens('cashtransForm_date', 'min');" /> </div>
                        <div class="label">거래상태</div>
                        <div class="input">
                            <select name="tran_status">
                                <option value="">전체</option>
                                <c:forEach var="code" items="${tranStatusList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == transFormBean.tran_status}">selected</c:if> >${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="label">거래결과</div>
                        <div class="input">
                            <select name="result_status">
                                <option value="">전체</option>
                                <c:forEach var="code" items="${resultStatusList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == transFormBean.result_status}">selected</c:if> >${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div> 
                        <div class="label">검색항목</div>
                            <div class="input">
                                <select name="searchinputnm" id="searchinputnm" onchange="javascirpt:searchinputnmChange(this);">
                                    <option value="user_nm" <c:if test="${'user_nm' == transFormBean.searchinputnm}">selected</c:if>>고객명</option>
                                    <option value="product_nm" <c:if test="${'product_nm' == transFormBean.searchinputnm}">selected</c:if>>상품명</option>
                                    <option value="user_phone2" <c:if test="${'user_phone2' == transFormBean.searchinputnm}">selected</c:if>>전화번호</option>
                                    <option value="tot_amt" <c:if test="${'tot_amt' == transFormBean.searchinputnm}">selected</c:if>>승인금액</option>
                                    <option value="card_num" <c:if test="${'card_num' == transFormBean.searchinputnm}">selected</c:if>>인증번호</option>
                                    <option value="app_no" <c:if test="${'app_no' == transFormBean.searchinputnm}">selected</c:if>>승인번호</option>
                                </select>
                                <input type="text" name="searchinputcontent" id="searchinputcontent"  value ="${transFormBean.searchinputcontent}" onblur="javascirpt:searchinputcontentChange(this);" />
                        </div>
                                <input type="hidden" name="user_nm" id="user_nm" value ="${transFormBean.user_nm}" />
                                <input type="hidden" name="user_phone2" id="user_phone2"  value ="${transFormBean.user_phone2}" />
                                <input type="hidden" name="product_nm" id="product_nm"  value ="${transFormBean.product_nm}" />
                                <input type="hidden" name="tot_amt" id="tot_amt" value ="${transFormBean.tot_amt}" />
                                <input type="hidden" name="card_num" id="card_num" value ="${transFormBean.card_num}" />
                                <input type="hidden" name="app_no" id="app_no" value ="${transFormBean.app_no}" />  
                        <!--
                       <div class="label">UID명</div>
                        <div class="input">
                            <input type="text" name="onfftid_nm" value ="${transFormBean.onfftid_nm}" readonly/>
                        </div>                            
                        <div class="label">UID</div>
                        <div class="input">
                            <input type="text" name="onfftid" value ="${transFormBean.onfftid}" readonly/>                            
                        </div>     
                        <div class="label">고객명</div>
                        <div class="input">
                            <input type="text" name="user_nm" value ="${transFormBean.user_nm}" />
                        </div>                               
                        <div class="label">고객전화번호</div>
                        <div class="input">
                            <input type="text" name="user_phone2" value ="${transFormBean.user_phone2}" />
                        </div>                                                             
                        <div class="label">상품명</div>
                        <div class="input">
                            <input type="text" name="product_nm" value ="${transFormBean.product_nm}" />
                        </div>                                                   
                        <div class="label">승인금액</div>
                        <div class="input">
                            <input type="text" name="tot_amt" value ="${transFormBean.tot_amt}" />
                        </div>                                               
                        <div class="label">인증번호</div>
                        <div class="input">
                            <input type="text" name="card_num" value ="${transFormBean.card_num}" />
                        </div>                                                     
                        <div class="label">승인번호</div>
                        <div class="input">
                            <input type="text" name="app_no" value ="${transFormBean.app_no}" />
                        </div> 
                        -->
                        </td>
                        <td>
                            <input type="button" name="cashtransdetail_search" onclick="javascript:cashtransdetailSearch();" value="조회"/>
                        </td>                         
                        <td>
                            <input type="button" name="cashtransdetail_excel" onclick="javascript:cashTransDetailExcel();" value="엑셀"/>
                        </td>
                    </tr>
                </table>
            </form>
   <form name="frm_cash_cncl" id="frm_cash_cncl" method="post" >
                <input type="hidden" name="org_cno" id="org_cno" value=""><input type="hidden" name="mgr_msg" id="mgr_msg" value="" ><input type="hidden" name="EP_tr_cd" id="EP_tr_cd" value="00201050"><input type="hidden" name="mgr_txtype" name="id_txtype" value="51"><input type="hidden" name="tran_seq" id="tran_seq" value="">
    </form>
<br>
        <div class="CSSTableGenerator" >
                        <table >
                    <tr>
                        <td>승인건</td>
                        <td>승인금액</td>
                        <td >취소건</td>
                        <td>취소금액</td>
                    </tr>
                    <tr>
                        <td style="text-align: right"><fmt:formatNumber value="${total_appcnt}" type="number"/></td>
                        <td style="text-align: right"><fmt:formatNumber value="${total_appamt}" type="number"/></td>
                        <td style="text-align: right"><fmt:formatNumber value="${total_cnclcnt}" type="number"/></td>
                        <td style="text-align: right"><fmt:formatNumber value="${total_cnclamt}" type="number"/></td>
                    </tr>                    
                </table>
        </div><br>                            
        <div class="CSSTableGenerator" >
                <table >
                    <tr>
                        <td>No</td>
                        <td>가맹점</td>
                        <td >고객명</td>
                        <td>연락처</td>
                        <td>상품명</td>                        
                        <td>거래종류</td>
                        <td>인증번호</td>                        
                        <td>승인금액</td>
                        <td>승인번호</td>                        
                        <td>승인일시</td>
                        <td>취소일시</td>                        
                        <td>거래상태</td>  
                        <td>승인취소</td>                                                
                        <td>영수증</td>                        
                    </tr>
<fmt:formatDate value="${now}" pattern="yyyyMMdd" var="today" />    
<c:forEach var="transDetailList" items="${ls_cashtransDetailList}" begin="0">                            
<fmt:parseDate value="${transDetailList.app_dt}" var="tpchkappdt1" pattern="yyyyMMdd" scope="page"/><fmt:formatDate value="${tpchkappdt1}" pattern="yyyyMMdd" var="chkapp_dt"/>
    <tr>
                        <td height="30px;" width="30px"  style="text-align:center">${transDetailList.rnum}</td> 
                        <td>${transDetailList.merch_nm}</td>
                        <td >${transDetailList.user_nm}</td>
                        <td>${transDetailList.user_phone2}</td>
                        <td>${transDetailList.product_nm}</td>                        
                        <td>${transDetailList.massagetype_nm}</td>
                        <td>${transDetailList.card_num}</td>                        
                        <td style="text-align: right"><fmt:formatNumber value="${transDetailList.tot_amt}" type="number"/></td>
                        <c:choose>
                            <c:when test="${transDetailList.massagetype == '10'}">
                                <td>${transDetailList.app_no}</td> 
                            </c:when>
                            <c:otherwise>
                               <td>${transDetailList.org_app_no}</td> 
                            </c:otherwise>
                        </c:choose>                            
                        <td><fmt:parseDate value="${transDetailList.app_dt}${transDetailList.app_tm}" var="appdt1" pattern="yyyyMMddHHmmss" scope="page"/><fmt:formatDate value="${appdt1}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
                        <td><fmt:parseDate value="${transDetailList.cncl_dt}" var="cncldt1" pattern="yyyyMMdd" scope="page"/><fmt:formatDate value="${cncldt1}" pattern="yyyy-MM-dd" /></td>
                        <td>${transDetailList.tran_status_nm}</td>
                        <c:choose>
                            <c:when test="${parma_usercate == '01' }">
                                <c:choose>
                                    <c:when test="${transDetailList.result_status == '00' && transDetailList.massagetype == '10' }">
                                        <c:choose>
                                            <c:when test="${transDetailList.tran_status == '00' || transDetailList.tran_status == '05'}">
                                                <td><button name='cncl_view_bt' onclick="javascript:cashCnclAction('${transDetailList.tran_seq}', '${transDetailList.pg_seq}');" >거래취소</button></td> 
                                            </c:when>
                                            <c:otherwise>
                                               <td>&nbsp;</td> 
                                            </c:otherwise>
                                        </c:choose> 
                                    </c:when>
                                    <c:otherwise>
                                        <td>&nbsp;</td> 
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                            <c:when test="${parma_usercate == '02' }">
                                <c:choose>
                                    <c:when test="${transDetailList.result_status == '00' && transDetailList.massagetype == '10' }">
                                        <c:choose>
                                            <c:when test="${transDetailList.tran_status == '00'}">
                                                <td><button name='cncl_view_bt' onclick="javascript:cashCnclAction('${transDetailList.tran_seq}', '${transDetailList.pg_seq}');" >거래취소</button></td> 
                                            </c:when>
                                            <c:otherwise>
                                               <td>&nbsp;</td> 
                                            </c:otherwise>
                                        </c:choose> 
                                    </c:when>
                                    <c:otherwise>
                                        <td>&nbsp;</td> 
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                            <c:otherwise>
                                <td>&nbsp;</td> 
                            </c:otherwise>
                        </c:choose>                            
                        <td><button onclick="javascript:cashbillView('${transDetailList.tran_seq}','${transDetailList.result_cd}');"  >영수증</button></td>
                    </tr>
</c:forEach>
                </table>
            </div>                            
             <div id="paging" name="paging" style="margin: 15px;text-align:center;"></div>
        </div>                            
<%@include file="/WEB-INF/view/common/basic_comm_tail.jsp"%>    
            <script type="text/javascript">
                var cashtransdetailCalendar;
                
                $(document).ready(function () {
                    
                    cashtransdetailCalendar = new dhtmlXCalendarObject(["cashtransForm_date","cashtransto_date"]);    
                    
                });

                //코드 정보 조회
                function cashtransdetailSearch() {
                document.cashcashtransdetailForm.submit();
                }
            
                function cash_setSens(id, k) 
                {
                    // update range
                    if (k == "min") {
                        cashtransdetailCalendar.setSensitiveRange(cash_byId(id).value, null);
                    } else {
                        cashtransdetailCalendar.setSensitiveRange(null, cash_byId(id).value);
                    }
                }

                function cash_byId(id) {
                    return document.getElementById(id);
                }
                
                function cashCnclAction(tptranseq,tporgcno)
                {
                        
                    if(confirm("승인거래를 취소하시겠습니까?"))
                    {
                        $("#frm_cash_cncl input[id=tran_seq]").val(tptranseq);
                        $("#frm_cash_cncl input[id=org_cno]").val(tporgcno);
                            
                        $.ajax({
                                        url : "../app/mgrAction",
                                        type : "POST",
                                        async : true,
                                        dataType : "json",
                                        data: $("#frm_cash_cncl").serialize(),
                                        success : function(data) {
                                            if(data.result_cd == '0000')
                                            {
                                                alert(data.result_msg);
                                                cashtransdetailSearch();
                                            }
                                            else
                                            {
                                                alert("[" +data.result_cd+ "]" + data.result_msg);
                                            }

                                        },
                                        error : function() { 
                                            alert("취소 실패");
                                        }
                        });	 
                    }
                    
                     return false;
                }
                
             //현금거래 엑셀다운로드
            function cashTransDetailExcel() {

                $("#cashcashtransdetailForm").attr("action","<c:url value="/trans/basic_merchCashTransDetailListExcel" />");
                document.getElementById("cashcashtransdetailForm").submit();

            }
            
            function cashbillView(tptranseq,tpresult_cd)
            {
                if(tpresult_cd == '0000')
                {    
                    var strUrl = "../app/appCashResult?tran_seq="+tptranseq+"&tran_result_cd="+tpresult_cd+"&auctioncate=1";
                    var winx = window.open(strUrl,"결제내역", "width=400,height=700,scrollbars=yes,resizeable=no");
                    winx.focus();
                }                        
                        
                return false;
            }
            
            
                function searchinputcontentChange(selobj)
                {
                    
                    var selvalue = selobj.value;
                    var selnm = $("#searchinputnm").val();
                   
                    if(selnm == "user_nm")
                    {
                        $("#user_nm").val(selvalue);
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                    }
                    else if(selnm == "user_phone2")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val(selvalue);
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                    }
                    else if(selnm == "product_nm")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val(selvalue);
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                    }
                    else if(selnm == "tot_amt")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val(selvalue);
                        $("#card_num").val("");
                    }
                    else if(selnm == "card_num")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val(selvalue);
                    }               
                }
                
                function searchinputnmChange(selobj)
                {
                    var selnm = selobj.value;
                    var salval = $("#searchinputcontent").val();
                    
                    if(selnm == "user_nm")
                    {
                        $("#user_nm").val(salval);
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                    }
                    else if(selnm == "user_phone2")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val(salval);
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                    }
                    else if(selnm == "product_nm")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val(salval);
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                    }
                    else if(selnm == "tot_amt")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val(salval);
                        $("#card_num").val("");
                    }
                    else if(selnm == "card_num")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val(salval);
                    }
                     
                }            
        </script>