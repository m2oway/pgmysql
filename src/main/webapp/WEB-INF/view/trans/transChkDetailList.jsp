<%-- 
    Document   : codeDetailMaster
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>신용카드 상세조회</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">

                var transChkgrid={};
                var transChk_layout={};
                var complainMasterWindow;
                var transChkCalendar1;

                $(document).ready(function () {
                    
                    transChkCalendar1 = new dhtmlXCalendarObject(["transChkForm_from_date","transChkForm_to_date"]);    
                
                    var transChkForm = document.getElementById("transChk_search");
                    transChk_layout = main_transchk_layout.cells('b').attachLayout("2E");
                    transChk_layout.cells('a').hideHeader();
                    transChk_layout.cells('b').hideHeader();
                    transChk_layout.cells('a').attachObject(transChkForm);
                    transChk_layout.cells('a').setHeight(85);
                    transChkgrid = transChk_layout.cells('b').attachGrid();
                    transChkgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");

                    var transChkheaders = "";
                    transChkheaders +=  "<center>NO</center>,<center>지급ID명</center>,<center>UID명</center>,<center>고객명</center>,<center>고객전화번호</center>";
                    transChkheaders += ",<center>상품명</center>,<center>거래종류</center>,<center>카드사</center>,<center>카드번호</center>,<center>할부</center>";
                    transChkheaders += ",<center>승인금액</center>,<center>비과세금액</center>,<center>부가세</center>,<center>승인번호</center>,<center>승인일시</center>";
                    transChkheaders += ",<center>취소일</center>,<center>거래상태</center>,<center>거래결과</center>,<center>민원</center>,<center>영수증</center>";
                    transChkheaders += ",<center>취소</center>,<center>결제채널</center>,<center>결과코드</center>,<center>결과메시지</center>,<center>TID</center>";
                    transChkheaders += ",<center>가맹점번호</center>,<center>매입사</center>,<center>수수료</center>,<center>입금액</center>,<center>ONOFF수수료</center>";
                    transChkheaders += ",<center>부가세</center>,<center>ONOFF지급액</center>,<center>입금일</center>,<center>지급일</center>,<center>거래일련번호</center>";
                    transChkgrid.setHeader(transChkheaders);
                    var transChkColAlign = "";
                    transChkColAlign +=  "center,center,center,center,center";
                    transChkColAlign += ",center,center,center,center,center";
                    transChkColAlign += ",right,right,right,center,center";
                    transChkColAlign += ",center,center,center,center,center";
                    transChkColAlign += ",center,center,center,center,center";
                    transChkColAlign += ",center,center,right,right,right";
                    transChkColAlign += ",right,right,center,center,center";
                    transChkgrid.setColAlign(transChkColAlign);
                    var transChkColTypes = "";
                    transChkColTypes +=  "txt,txt,txt,txt,txt";
                    transChkColTypes += ",txt,txt,txt,txt,txt";
                    transChkColTypes += ",edn,edn,edn,txt,txt";
                    transChkColTypes += ",txt,txt,txt,txt,txt";
                    transChkColTypes += ",txt,txt,txt,txt,txt";
                    transChkColTypes += ",txt,txt,edn,edn,edn";
                    transChkColTypes += ",edn,edn,txt,txt,txt";
                    transChkgrid.setColTypes(transChkColTypes);
                    var transChkInitWidths = "";
                    transChkInitWidths +=   "40,150,150,80,90";
                    transChkInitWidths += ",100,80,60,110,60";
                    transChkInitWidths += ",90,90,90,90,110";
                    transChkInitWidths += ",80,70,70,90,90";
                    transChkInitWidths += ",90,100,100,150,100";
                    transChkInitWidths += ",100,100,90,90,90";
                    transChkInitWidths += ",90,90,100,100,100";
                    transChkgrid.setInitWidths(transChkInitWidths);
                    var transChkColSorting = "";
                    transChkColSorting +=  "str,str,str,str,str";
                    transChkColSorting += ",str,str,str,str,str";
                    transChkColSorting += ",int,int,int,str,str";
                    transChkColSorting += ",str,str,str,str,str";
                    transChkColSorting += ",str,str,str,str,str";
                    transChkColSorting += ",str,str,int,int,int";
                    transChkColSorting += ",int,int,str,str,str";
                    transChkgrid.setColSorting(transChkColSorting);
                    transChkgrid.setNumberFormat("0,000",10);
                    transChkgrid.setNumberFormat("0,000",11);
                    transChkgrid.setNumberFormat("0,000",12);
                    transChkgrid.setNumberFormat("0,000",27);
                    transChkgrid.setNumberFormat("0,000",28);            
                    transChkgrid.setNumberFormat("0,000",29);
                    transChkgrid.setNumberFormat("0,000",30);                                
                    transChkgrid.setNumberFormat("0,000",31); 
                    
                    transChkgrid.enableColumnMove(true);
                    transChkgrid.setSkin("dhx_skyblue");
                    transChkgrid.enablePaging(true,Number($("#transChkForm input[name=page_size]").val()),10,"transChkPaging",true,"transChkrecinfoArea");
                    transChkgrid.setPagingSkin("bricks");

                    <c:if test="${transFormBean.ses_user_cate == '03'}">
                    transChkgrid.setColumnHidden(18,true);  
                    transChkgrid.setColumnHidden(19,true); 
                    transChkgrid.setColumnHidden(20,true); 
                    </c:if>                            
                    

                    //페이징 처리
                    transChkgrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        if(lInd !==0){
                            $("#transChkForm input[name=page_no]").val(ind);
                            transChkgrid.clearAll();
                            transChkSearch();
                        }else{
                            $("#transChkForm input[name=page_no]").val(1);
                            transChkgrid.clearAll();
                            transChkSearch();
                        }
                    });

                    transChkgrid.init();
                    transChkgrid.parse(${ls_transDetailList}, "json");

                    <c:if test="${transFormBean.ses_user_cate == '00' ||transFormBean.ses_user_cate == '01' || transFormBean.ses_user_cate == '02'}">
                    //transChkgrid.attachEvent("onRowDblClicked", Trans_MgrAction);
                    </c:if>

                    //상세 검색
                    $("#transChkForm input[name=transChk_search]").unbind("click").bind("click", function (){

                        //$("input[name=page_no]").val("1");
                        //transChkSearch();
                        transChkgrid.changePage(1);
                    });
                    
                    //민원 등록
                    $("button[name=complain_insert]").unbind("click").bind("click", function (){

                        complainMasterWindow = new dhtmlXWindows();
                        w1 = complainMasterWindow.createWindow("w1", 25, 25, 700, 580);
                        w1.setText("민원 등록");
                        complainMasterWindow.window('w1').setModal(true);
                        tran_seq = $(this).attr("tran_seq");
                        w1.attachURL("<c:url value="/complain/complainMasterInsert" />" + "?tran_seq=" + tran_seq + "&complain_popup_yn=Y" + "&grid_nm=transChkgrid", true);   
                        
                        return false;

                    });                
                    
                    
                    //영수증보기
                    $("button[name=bill_view_bt]").unbind("click").bind("click", function (){

                        var tptranseq = $(this).attr("tran_seq");
                        var tpresult_cd =  $(this).attr("tran_result_cd"); 
                        if(tpresult_cd == '0000')
                        {    
                            var strUrl = "../app/appResult?tran_seq="+tptranseq+"&tran_result_cd="+tpresult_cd+"&auctioncate=1";
                            var winx = window.open(strUrl,"영수증", "width=400,height=700,scrollbars=yes,resizeable=no");
                            winx.focus();
                        }
                        
                        return false;

                    });            
                    
                    //취소버튼
                    $("button[name=cncl_view_bt]").unbind("click").bind("click", function (){

                        var tptranseq = $(this).attr("tran_seq");
                        var tporgcno =  $(this).attr("org_cno"); 
                        
                        if(confirm("승인거래를 취소하시겠습니까?"))
                        {
                            
                            $("#frm_card_cncl input[id=tran_seq]").val(tptranseq);
                            $("#frm_card_cncl input[id=org_cno]").val(tporgcno);
                            
                             $.ajax({
                                        url : "../app/mgrAction",
                                        type : "POST",
                                        async : true,
                                        dataType : "json",
                                        data: $("#frm_card_cncl").serialize(),
                                        success : function(data) {
                                            if(data.result_cd == '0000')
                                            {
                                                alert(data.result_msg);
                                                transChkgrid.clearAll();
                                                transChkSearch();
                                            }
                                            else
                                            {
                                                alert("[" +data.result_cd+ "]" + data.result_msg);
                                            }

                                        },
                                        error : function() { 
                                            alert("취소 실패");
                                        }
                                    });	            
                        }
                        
                        return false;

                    });                             
                    
                    //민원 보기
                    $("button[name=complain_search]").unbind("click").bind("click", function (){

                        complainMasterWindow = new dhtmlXWindows();
                        w1 = complainMasterWindow.createWindow("w1", 25, 25, 700, 850);
                        w1.setText("민원 보기");
                        complainMasterWindow.window('w1').setModal(true);
                        complain_seq = $(this).attr("complain_seq");
                        tran_seq = $(this).attr("tran_seq");
                        w1.attachURL("<c:url value="/complain/complainMasterUpdate" />" + "?complain_seq=" + complain_seq + "&tran_seq=" + tran_seq + "&complain_popup_yn=Y", true);   
                        
                        return false;

                    });                    
                    
                    //신용거래 정보 엑셀다운로드 이벤트
                    $("#transChkForm input[name=transChk_excel]").click(function () {

                        transDetailExcel();

                    });               
                     $("#transChkForm input[name=week]").click(function(){
                        transChk_date_search("week");
                    });

                    $("#transChkForm input[name=month]").click(function(){
                        transChk_date_search("month");
                    });
                    //검색조건 초기화
                    $("#transChkForm input[name=init]").click(function () {
                    
                        transDetailMasterInit($("#transChkForm"));

                    });  
                    
                });
                 function transChk_date_search(day){
                    var oneDate = 1000 * 3600 * 24; 
                    var now = new Date();
                    var week = new Date(now.getTime() + (oneDate*-6));
                    var month = new Date(now.getTime() + (oneDate*-31));
                    var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                    var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                    var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                    var date=new Date();

                    if(day=="today"){
                        $("#transChkForm input[id=transChkForm_from_date]").val(nowTime);
                        $("#transChkForm input[id=transChkForm_to_date]").val(nowTime);
                    }else if(day=="week"){
                        $("#transChkForm input[id=transChkForm_from_date]").val(weekTime);
                        $("#transChkForm input[id=transChkForm_to_date]").val(nowTime);
                    }else{
                        $("#transChkForm input[id=transChkForm_from_date]").val(monthTime);
                        $("#transChkForm input[id=transChkForm_to_date]").val(nowTime);
                    }
                }
                //거래 상세 정보 조회
                function transChkSearch() {
                    $.ajax({
                        url: "<c:url value="/trans/transChkDetailList" />",
                        type: "POST",
                        async: true,
                        dataType: "json",
                        data: $("#transChkForm").serialize(),
                        success: function (data) {

                            var jsonData = $.parseJSON(data);
                            transChkgrid.parse(jsonData,"json");

                            //민원 등록
                            $("button[name=complain_insert]").unbind("click").bind("click", function (){

                                complainMasterWindow = new dhtmlXWindows();
                                w1 = complainMasterWindow.createWindow("w1", 1, 1, 700, 500);
                                w1.setText("민원 등록");
                                complainMasterWindow.window('w1').setModal(true);
                                tran_seq = $(this).attr("tran_seq");
                                w1.attachURL("<c:url value="/complain/complainMasterInsert" />" + "?tran_seq=" + tran_seq + "&complain_popup_yn=Y", true);   

                                return false;

                            });                    

                            //민원 보기
                            $("button[name=complain_search]").unbind("click").bind("click", function (){

                                complainMasterWindow = new dhtmlXWindows();
                                w1 = complainMasterWindow.createWindow("w1", 1, 1, 700, 900);
                                w1.setText("민원 등록");
                                complainMasterWindow.window('w1').setModal(true);
                                complain_seq = $(this).attr("complain_seq");
                                tran_seq = $(this).attr("tran_seq");
                                w1.attachURL("<c:url value="/complain/complainMasterUpdate" />" + "?complain_seq=" + complain_seq + "&tran_seq=" + tran_seq + "&complain_popup_yn=Y", true);   

                                return false;

                            });      
                            

                            //영수증보기
                            $("button[name=bill_view_bt]").unbind("click").bind("click", function (){

                                var tptranseq = $(this).attr("tran_seq");
                                var tpresult_cd =  $(this).attr("tran_result_cd"); 
                                if(tpresult_cd == '0000')
                                {    
                                    var strUrl = "../app/appResult?tran_seq="+tptranseq+"&tran_result_cd="+tpresult_cd+"&auctioncate=1";
                                    var winx = window.open(strUrl,"영수증", "width=400,height=700,scrollbars=yes,resizeable=no");
                                    winx.focus();
                                }

                                return false;

                            });     
                            
                    
                            //취소버튼
                            $("button[name=cncl_view_bt]").unbind("click").bind("click", function (){

                                var tptranseq = $(this).attr("tran_seq");
                                var tporgcno =  $(this).attr("org_cno"); 

                                if(confirm("승인거래를 취소하시겠습니까?"))
                                {

                                    $("#frm_card_cncl input[id=tran_seq]").val(tptranseq);
                                    $("#frm_card_cncl input[id=org_cno]").val(tporgcno);

                                     $.ajax({
                                                url : "../app/mgrAction",
                                                type : "POST",
                                                async : true,
                                                dataType : "json",
                                                data: $("#frm_card_cncl").serialize(),
                                                success : function(data) {
                                                    if(data.result_cd == '0000')
                                                    {
                                                        alert(data.result_msg);
                                                        transChkgrid.clearAll();
                                                        transChkSearch();
                                                    }
                                                    else
                                                    {
                                                        alert("[" +data.result_cd+ "]" + data.result_msg);
                                                    }

                                                },
                                                error : function() { 
                                                    alert("취소 실패");
                                                }
                                            });	            
                                }



                                return false;

                            });                                                         


                        },
                        error: function () {
                            alert("조회 실패");
                        }
                    });
                    return false;
                }
            
                function transDetailExcel(){
                    $("#transChkForm").attr("action","<c:url value="/trans/transDetailListExcel" />");
                    document.getElementById("transChkForm").submit();            
                }
                
                <c:if test="${transFormBean.ses_user_cate == '00' ||transFormBean.ses_user_cate == '01' || transFormBean.ses_user_cate == '02'}">            
                        function Trans_MgrAction(rowId, col) 
                        {
                            var tptranseq = transChkgrid.getUserData(rowId, 'tran_seq');
                            var tpresult_cd =  transChkgrid.getUserData(rowId, 'result_cd');
                            if(tpresult_cd == '0000')
                            {    
                                var strUrl = "../app/appResult?tran_seq="+tptranseq+"&tran_result_cd="+tpresult_cd+"&auctioncate=1";
                                var winx = window.open(strUrl,"결제내역", "width=400,height=700,scrollbars=yes,resizeable=no");
                                winx.focus();
                            }
                        }
                </c:if>  
                    
                var dhxSelPopupWins=new dhtmlXWindows();
                    
                //ONOFF가맹점번호 팝업 클릭 이벤트
                $("#transChkForm input[name=merch_nm]").unbind("click").bind("click", function (){

                    onoffTransDetailMerchSelectPopup_tranChk();

                });                           
                    
                function onoffTransDetailMerchSelectPopup_tranChk(){
                    onoffObject.onffmerch_no = $("#transChkForm input[name=onffmerch_no]");
                    onoffObject.merch_nm = $("#transChkForm input[name=merch_nm]");
                    //ONOFF지급ID정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("지급ID 선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                }
                                  
                //ONOFFUID 팝업 클릭 이벤트
                $("#transChkForm input[name=onfftid_nm]").unbind("click").bind("click", function (){

                    onoffTransDetailTidSelectPopup_tranChk();

                });                                
                
                function onoffTransDetailTidSelectPopup_tranChk(){
                    onoffObject.onfftid_nm = $("#transChkForm input[name=onfftid_nm]");
                    onoffObject.onfftid = $("#transChkForm input[name=onfftid]");
                    //ONOFFUID 팝업
                    w2 = dhxSelPopupWins.createWindow("OnofftidSelectPopUp", 20, 30, 640, 480);
                    w2.setText("ONOFFUID 선택페이지");
                    dhxSelPopupWins.window('OnofftidSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnofftidSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnofftidSelectPopUp"/>', true);   
                }                
              
                   
                
                function transChk_setSens(id, k) {
                    // update range
                    if (k == "min") {
                        transChkCalendar1.setSensitiveRange(transChk_byId(id).value, null);
                    } else {
                        transChkCalendar1.setSensitiveRange(null, transChk_byId(id).value);
                    }
                }

                function transChk_byId(id) {
                    return document.getElementById(id);
                }
                
                function transDetailMasterInit($form) {

                    searchFormInit($form);

                    transChk_date_search("week");                    
                } 
                
            </script>

            <div class="right" id="transChk_search" style="width:100%;">
                <form id="transChkForm" method="POST" action="<c:url value="/trans/transChkDetailList" />" modelAttribute="transFormBean">

                    <input type="hidden" name="page_size" value="100" >
                    <input type="hidden" name="page_no" value="1" >
                    <input type="hidden" name="pay_chn_cate" value="${transFormBean.pay_chn_cate}" >
                    <table>
                        <tr><td>
                            <div class="label">승인일</div>
                            <div class="input">
                            <input type="text" size="10" name="app_start_dt" id="transChkForm_from_date" value ="${transFormBean.app_start_dt}" onclick="transChk_setSens('transChkForm_to_date', 'max');" /> ~
                            <input type="text" size="10"name="app_end_dt" id="transChkForm_to_date" value="${transFormBean.app_end_dt}" onclick="transChk_setSens('transChkForm_from_date', 'min');" /> </div>
                            <div class ="input"><input type="button" name="week" value="1주"/></div>
                            <div class ="input"><input type="button" name="month" value="1달"/></div> 
                            <div class="label">거래상태</div>
                            <div class="input">
                                <select name="tran_status">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${tranStatusList.tb_code_details}">
                                        <option value="${code.detail_code}" <c:if test="${code.detail_code == transFormBean.tran_status}">selected</c:if> >${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="label">거래결과</div>
                            <div class="input">
                                <select name="result_status">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${resultStatusList.tb_code_details}">
                                        <option value="${code.detail_code}" <c:if test="${code.detail_code == transFormBean.result_status}">selected</c:if> >${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>    
                            <div class="label">지급ID명</div>
                            <div class="input">
                                <input type="text" name="merch_nm" value ="${transFormBean.merch_nm}" />
                            </div>        
                            <div class="label">지급ID번호</div>
                            <div class="input">
                                <input type="text" name="onffmerch_no" value ="${transFormBean.onffmerch_no}" />
                            </div>                              
                            <div class="label">UID명</div>
                            <div class="input">
                                <input type="text" name="onfftid_nm" value ="${transFormBean.onfftid_nm}" />
                            </div>    
                            <div class="label">UID</div>
                            <div class="input">
                                <input type="text" name="onfftid" value ="${transFormBean.onfftid}" />
                            </div>    
                            <div class="label">고객명</div>
                            <div class="input">
                                <input type="text" name="user_nm" value ="${transFormBean.user_nm}" />
                            </div>                               
                            <div class="label">고객전화번호</div>
                            <div class="input">
                                <input type="text" size="14" name="user_phone2" value ="${transFormBean.user_phone2}" />
                            </div>                                                             
                            <div class="label">상품명</div>
                            <div class="input">
                                <input type="text" name="product_nm" value ="${transFormBean.product_nm}" />
                            </div>                                                   
                            <div class="label">승인금액</div>
                            <div class="input">
                                <input type="text" size="10" name="tot_amt" value ="${transFormBean.tot_amt}" />
                            </div>                                               
                            <div class="label">카드번호</div>
                            <div class="input">
                                <input type="text" size="20" name="card_num" value ="${transFormBean.card_num}" />
                            </div>                                                     
                            <div class="label">승인번호</div>
                            <div class="input">
                                <input type="text" size="10" name="app_no" value ="${transFormBean.app_no}" />
                            </div>   
                            <div class="label">거래종류</div>
                            <div class="input">
                                <select name="massagetype">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${massagetypeCdList.tb_code_details}">
                                        <option value="${code.detail_code}" <c:if test="${code.detail_code == transFormBean.massagetype}">selected</c:if> >${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>                            
                            <div class="label">카드사</div>
                            <div class="input">
                                <select name="iss_cd">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${issCdList.tb_code_details}">
                                        <option value="${code.detail_code}" <c:if test="${code.detail_code == transFormBean.result_status}">selected</c:if> >${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div></td>              

                            <td><input type="button" name="transChk_search" value="조회"/></td>
                            <td><input type="button" name="transChk_excel" value="엑셀"/></td>
                            <td><input type="button" name="init" value="검색조건지우기"/></td>
                        </tr>
                    </table>
                        
                </form>
            <form name="frm_card_cncl" id="frm_card_cncl" method="post" >
                <input type="hidden" name="org_cno" id="org_cno" value=""><input type="hidden" name="mgr_msg" id="mgr_msg" value="" ><input type="hidden" name="EP_tr_cd" id="EP_tr_cd" value="00201000"><input type="hidden" name="mgr_txtype" id="mgr_txtype" value="40"><input type="hidden" name="tran_seq" id="tran_seq" value="">
            </form>
            <div class="paging">
                <div id="transChkPaging" style="width: 50%;"></div>
                <div id="transChkrecinfoArea" style="width: 50%;"></div>
            </div>
                                
        </div>

<c:if test="${!ajaxRequest}">
    </body>
</html>    
</c:if>