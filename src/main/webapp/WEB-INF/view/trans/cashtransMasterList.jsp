<%-- 
    Document   : cashtransMasterList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>현금거래조회</title>             
        </head>
        <body>
</c:if>

        <div class="right" id="cashtrans_search" style="width:100%;">       
            <form id="cashtransForm" method="POST" onsubmit="return false" action="<c:url value="/trans/cashtransMasterList" />" modelAttribute="transFormBean"> 
                <input type="hidden" name="page_size" value="100" >
                <input type="hidden" name="page_no" value="1" >
                <table>
                    <tr>
                        <div class="label">검색일자</div>
                        <div class="input">
                            <input type="text" name="app_start_dt" id="cashtransForm_from_date" value ="${transFormBean.app_start_dt}" onclick="setSens('cashtransForm_to_date', 'max');" /> ~
                            <input type="text" name="app_end_dt" id="cashtransForm_to_date" value="${transFormBean.app_end_dt}" onclick="setSens('cashtransForm_from_date', 'min');" />
                        </div>
                        <div class ="input"><input type="button" name="week" value="1주"/></div>
                        <div class ="input"><input type="button" name="month" value="1달"/></div>
                        <div class="label">결제채널</div>
                        <div class="input">
                            <select name="pay_chn_cate">
                                <option value="">전체</option>
                                <c:forEach var="code" items="${payChnCateList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <td><input type="button" name="cashtrans_search" value="조회"/></td>
                        <td><input type="button" name="cashtrans_excel" value="엑셀"/></td>
                        <td><input type="button" name="init" value="검색조건지우기"/></td>                        
                    </tr>
               </table>
            </form>
            <div class="paging">
                <div id="cashtransPaging" style="width: 50%;"></div>
                <div id="cashtransrecinfoArea" style="width: 50%;"></div>
            </div>                        
        </div>
        
         <script type="text/javascript">
            
            var cashtransgrid={};
            var cashtransCalendar;
            
            $(document).ready(function () {
                
                cashtransCalendar = new dhtmlXCalendarObject(["cashtransForm_from_date","cashtransForm_to_date"]);    
                
                var cashtrans_searchForm = document.getElementById("cashtrans_search");
    
                cashtrans_layout = main_cashtrans_layout.cells('a').attachLayout("2E");
                     
                cashtrans_layout.cells('a').hideHeader();
                cashtrans_layout.cells('b').hideHeader();
                 
                cashtrans_layout.cells('a').attachObject(cashtrans_searchForm);
                cashtrans_layout.cells('a').setHeight(40);
                
                cashtransgrid = cashtrans_layout.cells('b').attachGrid();
                
                cashtransgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                
                var transheaders = "";
                transheaders += "<center>일자</center>,<center>결제채널</center>,<center>지급ID명</center>,<center>승인금액</center>,<center>취소금액</center>,<center>승인VAT</center>,<center>취소VAT</center>,<center>승인건</center>,<center>취소건</center>";
                cashtransgrid.setHeader(transheaders);
                cashtransgrid.setColAlign("center,center,center,right,right,right,right,right,right");
                cashtransgrid.setColTypes("txt,txt,txt,edn,edn,edn,edn,edn,edn");
                cashtransgrid.setInitWidths("100,150,150,100,100,100,100,100,100,100");
                cashtransgrid.setColSorting("str,str,str,int,int,int,int,int,int");
                cashtransgrid.setNumberFormat("0,000",3);
                cashtransgrid.setNumberFormat("0,000",4);
                cashtransgrid.setNumberFormat("0,000",5);
                cashtransgrid.setNumberFormat("0,000",6);
                cashtransgrid.setNumberFormat("0,000",7);
                cashtransgrid.setNumberFormat("0,000",8);
                cashtransgrid.enableColumnMove(true);
                cashtransgrid.setSkin("dhx_skyblue");

                var cashtransfooters = "총계,#cspan,#cspan";
                cashtransfooters += ",<div id='cashtrans_master3'>0</div>,<div id='cashtrans_master4'>0</div>,<div id='cashtrans_master5'>0</div>,<div id='cashtrans_master6'>0</div>,<div id='cashtrans_master7'>0</div>,<div id='cashtrans_master8'>0</div>";
                cashtransgrid.attachFooter(cashtransfooters);
                
                /*
                cashtransgrid.enablePaging(true,Number($("#cashtransForm input[name=page_size]").val()),10,"cashtransPaging",true,"cashtransrecinfoArea");
                cashtransgrid.setPagingSkin("bricks");    
               
                cashtransgrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                */
                    /*
                    if(lInd !==0){
                        $("#cashtransForm input[name=page_no]").val(ind);
                        cashtransSearch();
                    }else{
                        return false;
                    }
                    */
                /*    
                    if(lInd !==0){
                      $("#cashtransForm input[name=page_no]").val(ind);      
                       cashtransgrid.clearAll();
                        cashtransSearch();
                    }else{
                       $("#cashtransForm input[name=page_no]").val(1);
                        cashtransgrid.clearAll();
                         cashtransSearch();
                    }                                 
                    
                });                    
                */
                cashtransgrid.init();
                cashtransgrid.parse(${ls_cashtransMasterList}, cashtransFooterValues,"json");

                cashtransgrid.attachEvent("onRowDblClicked", cashtrans_attach);

                //신용거래 정보 검색 이벤트
                $("#cashtransForm input[name=cashtrans_search]").click(function () {
                    /*
                    $("input[name=page_no]").val("1");
                    */
                    cashtransSearch();
                    
                   /*
                    cashtransgrid.changePage(1);
                    return false;
                    */
                });
                $("#cashtransForm input[name=week]").click(function(){
                    cash_date_search("week");
                 });

                $("#cashtransForm input[name=month]").click(function(){
                    cash_date_search("month");
                 });
                 
                //현금거래 정보 엑셀다운로드 이벤트
                $("#cashtransForm input[name=cashtrans_excel]").click(function () {
                    cashTransExcel();
                });    
                
                $("#cashtransForm input[name=init]").click(function () {
                    transMasterInit($("#cashtransForm"));
                });  
                  
            });
            
            //검색조건 초기화
            function transMasterInit($form) {

                searchFormInit($form);

                cash_date_search("week");                    
            } 
            
            function cashtransFooterValues() {
		    var cashtrans_master8 = document.getElementById("cashtrans_master8");
		    var cashtrans_master3 = document.getElementById("cashtrans_master3");
		    var cashtrans_master4 = document.getElementById("cashtrans_master4");
		    var cashtrans_master5 = document.getElementById("cashtrans_master5");
		    var cashtrans_master6 = document.getElementById("cashtrans_master6");
		    var cashtrans_master7 = document.getElementById("cashtrans_master7");
		    
		    cashtrans_master8.innerHTML =  putComma(sumColumn(8)) ;
		    cashtrans_master3.innerHTML =  putComma(sumColumn(3)) ;
		    cashtrans_master4.innerHTML =  putComma(sumColumn(4)) ;
		    cashtrans_master5.innerHTML =  putComma(sumColumn(5)) ;
		    cashtrans_master6.innerHTML =  putComma(sumColumn(6)) ;
		    cashtrans_master7.innerHTML =  putComma(sumColumn(7)) ;
		    
		    return true;
            }
		
            function sumColumn(ind) {
                var out = 0;
                for (var i = 0; i < cashtransgrid.getRowsNum(); i++) {
                     out += parseFloat(cashtransgrid.cells2(i, ind).getValue());
                }

                return out;
            }

            //신용거래 상세조회 이벤트
            function cashtrans_attach(rowid, col) {

                var app_dt = cashtransgrid.getUserData(rowid, 'app_dt');
                var pay_chn_cate = cashtransgrid.getUserData(rowid, 'pay_chn_cate');
                var onffmerch_no = cashtransgrid.getUserData(rowid, 'onffmerch_no');
                main_cashtrans_layout.cells('b').attachURL("<c:url value="/trans/cashtransDetailList" />"+"?app_start_dt=" + app_dt+"&app_end_dt=" + app_dt+"&pay_chn_cate="+pay_chn_cate+"&onffmerch_no="+onffmerch_no, true);
            }
            
            //신용거래 정보 조회
            function cashtransSearch() {
                
                $.ajax({
                    url: "<c:url value="/trans/cashtransMasterList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#cashtransForm").serialize(),
                    success: function (data) {

                        cashtransgrid.clearAll();
                        cashtransgrid.parse($.parseJSON(data),cashtransFooterValues, "json");

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }
            
            function setSens(id, k) {
                // update range
                if (k == "min") {
                    cashtransCalendar.setSensitiveRange(byId(id).value, null);
                } else {
                    cashtransCalendar.setSensitiveRange(null, byId(id).value);
                }
            }

            function byId(id) {
                return document.getElementById(id);
            } 
            function cash_date_search(day){
                    var oneDate = 1000 * 3600 * 24; 
                    var now = new Date();
                    var week = new Date(now.getTime() + (oneDate*-6));
                    var month = new Date(now.getTime() + (oneDate*-31));
                    var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                    var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                    var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                    var date=new Date();

                    if(day=="today"){
                        $("#cashtransForm input[id=cashtransForm_from_date]").val(nowTime);
                        $("#cashtransForm input[id=cashtransForm_to_date]").val(nowTime);
                    }else if(day=="week"){
                        $("#cashtransForm input[id=cashtransForm_from_date]").val(weekTime);
                        $("#cashtransForm input[id=cashtransForm_to_date]").val(nowTime);
                    }else{
                        $("#cashtransForm input[id=cashtransForm_from_date]").val(monthTime);
                        $("#cashtransForm input[id=cashtransForm_to_date]").val(nowTime);
                    }
                }
                
            //현금거래 엑셀다운로드
            function cashTransExcel() {

                $("#cashtransForm").attr("action","<c:url value="/trans/cashTransMasterListExcel" />");
                document.getElementById("cashtransForm").submit();

            }     
            function transCashDetailMasterInit($form) {
                searchFormInit($form);
                cash_date_search("week");                    
            } 
                
        </script>
    </body>
</html>
