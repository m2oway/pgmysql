    <%-- 
    Document   : transFailFailDetailList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>신용카드 실패 상세조회</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">

                var transFaildetailgrid={};
                var transFaildetail_layout={};
                var transFaildetailCalendar;

                $(document).ready(function () {
                    
                    transFaildetailCalendar = new dhtmlXCalendarObject(["transFailForm_date","transFailto_date"]);    

                    var transFaildetailForm = document.getElementById("transFaildetail_search");
                    transFaildetail_layout = main_transFail_layout.cells('a').attachLayout("2E");
                    transFaildetail_layout.cells('a').hideHeader();
                    transFaildetail_layout.cells('b').hideHeader();
                    transFaildetail_layout.cells('a').attachObject(transFaildetailForm);
                    transFaildetail_layout.cells('a').setHeight(85);
                    transFaildetailgrid = transFaildetail_layout.cells('b').attachGrid();
                    transFaildetailgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");

                    var transdetailheaders = "";
                    transdetailheaders +=  "<center>NO</center>,<center>지급ID명</center>,<center>UID명</center>,<center>결제요청일시</center>,<center>고객명</center>";
                    transdetailheaders += ",<center>고객전화번호</center>,<center>상품명</center>,<center>거래종류</center>,<center>카드사</center>,<center>카드번호</center>";
                    transdetailheaders += ",<center>할부</center>,<center>승인금액</center>,<center>비과세금액</center>,<center>부가세</center>,<center>거래결과</center>";
                    transdetailheaders += ",<center>결제채널</center>,<center>결과코드</center>,<center>결과메시지</center>";
                    transdetailheaders += ",<center>매입사</center>,<center>결제IP</center>";                    
                    
                    transFaildetailgrid.setHeader(transdetailheaders);
                    var transdetailColAlign = "";
                    transdetailColAlign +=  "center,center,center,center,center";
                    transdetailColAlign += ",center,center,center,center,center";
                    transdetailColAlign += ",center,right,right,right,center";
                    transdetailColAlign += ",center,center,center";
                    transdetailColAlign += ",right,center";
                    transFaildetailgrid.setColAlign(transdetailColAlign);
                    var transdetailColTypes = "";
                    transdetailColTypes +=  "txt,txt,txt,txt,txt";
                    transdetailColTypes += ",txt,txt,txt,txt,txt";
                    transdetailColTypes += ",txt,edn,edn,edn,txt";
                    transdetailColTypes += ",txt,txt,txt";
                    transdetailColTypes += ",txt,txt";
                    transFaildetailgrid.setColTypes(transdetailColTypes);
                    var transdetailInitWidths = "";
                    transdetailInitWidths +=   "50,150,150,100,100";
                    transdetailInitWidths += ",100,150,80,80,120";
                    transdetailInitWidths += ",80,90,90,90,90";
                    transdetailInitWidths += ",90,90,200";
                    transdetailInitWidths += ",90,100";
                    transFaildetailgrid.setInitWidths(transdetailInitWidths);
                    var transdetailColSorting = "";
                    transdetailColSorting +=  "str,str,str,str,str";
                    transdetailColSorting += ",str,str,str,str,str";
                    transdetailColSorting += ",str,int,int,int,str";
                    transdetailColSorting += ",str,str,str";
                    transdetailColSorting += ",str,str";
                    transFaildetailgrid.setColSorting(transdetailColSorting);
                    transFaildetailgrid.setNumberFormat("0,000",11);
                    transFaildetailgrid.setNumberFormat("0,000",12);
                    transFaildetailgrid.setNumberFormat("0,000",13);                                       

                    transFaildetailgrid.enableColumnMove(true);
                    transFaildetailgrid.setSkin("dhx_skyblue");
                    transFaildetailgrid.enablePaging(true,Number($("#transFaildetailForm input[name=page_size]").val()),10,"transFaildetailPaging",true,"transFaildetailrecinfoArea");
                    transFaildetailgrid.setPagingSkin("bricks");

                    //페이징 처리
                    transFaildetailgrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        /*
                        if(lInd !==0){
                            $("#transFaildetailForm input[name=page_no]").val(ind);
                            transFaildetailSearch();
                        }else{
                            return false;
                        }
                        */
                        
                        if(lInd !==0){
                           $("#transFaildetailForm input[name=page_no]").val(ind);             
                           transFaildetailgrid.clearAll();
                           transFaildetailSearch();
                        }else{
                             $("#transFaildetailForm input[name=page_no]").val(1);
                            transFaildetailgrid.clearAll();
                             transFaildetailSearch();
                        }                            
                    });

                    transFaildetailgrid.init();
                    transFaildetailgrid.parse(${ls_transFailDetailList}, "json");

                    //상세코드검색
                    $("#transFaildetailForm input[name=transFaildetail_search]").click(function(){

                        /*
                        $("input[name=page_no]").val("1");

                        transFaildetailgrid.clearAll();

                        transFaildetailSearch();
                        */
                        transFaildetailgrid.changePage(1);
                        return false;   

                    });

                    $("#transFaildetailForm input[name=week]").click(function(){
                        transFail_date_search("week");
                    });

                    $("#transFaildetailForm input[name=month]").click(function(){
                        transFail_date_search("month");
                    });
                    //검색조건 초기화
                    $("#transFaildetailForm input[name=init]").click(function () {
                        transFailDetailMasterInit($("#transFaildetailForm"));
                    });

                    
                    //엑셀다운로드
                    $("#transFaildetailForm input[name=transFaildetail_excel]").click(function(){
                        
                        transFaildetailExcel();

                    });                    
                    

                });
                
                 //UID 팝업 클릭 이벤트
                $("#transFaildetailForm input[name=onfftid]").unbind("click").bind("click", function (){

                    onofftranFailTidSelectPopup();

                });                                
                
                function onofftranFailTidSelectPopup(){
                    onoffObject.onfftid = $("#transFaildetailForm input[name=onfftid]");
                    onoffObject.onfftid_nm = $("#transFaildetailForm input[name=onfftid_nm]");
                    //UID 팝업
                    w2 = dhxSelPopupWins.createWindow("OnofftidSelectPopUp", 20, 30, 640, 480);
                    w2.setText("UID 선택페이지");
                    dhxSelPopupWins.window('OnofftidSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnofftidSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnofftidSelectPopUp"/>', true);   
                }  
                
                function transFail_date_search(day){
                    var oneDate = 1000 * 3600 * 24; 
                    var now = new Date();
                    var week = new Date(now.getTime() + (oneDate*-6));
                    var month = new Date(now.getTime() + (oneDate*-31));
                    var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                    var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                    var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                    var date=new Date();

                    if(day=="today"){
                        $("#transFaildetailForm input[id=transFailForm_date]").val(nowTime);
                        $("#transFaildetailForm input[id=transFailto_date]").val(nowTime);
                    }else if(day=="week"){
                        $("#transFaildetailForm input[id=transFailForm_date]").val(weekTime);
                        $("#transFaildetailForm input[id=transFailto_date]").val(nowTime);
                    }else{
                        $("#transFaildetailForm input[id=transFailForm_date]").val(monthTime);
                        $("#transFaildetailForm input[id=transFailto_date]").val(nowTime);
                    }
                }

        //코드 정보 조회
                function transFaildetailSearch() {
                
                $.ajax({
                    url: "<c:url value="/trans/merchTransFailDetailList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#transFaildetailForm").serialize(),
                    success: function (data) {

                        var jsonData = $.parseJSON(data);

                        transFaildetailgrid.parse(jsonData,"json");

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
                    return false;
                    
            }
            var dhxSelPopupWins=new dhtmlXWindows();
                
                function TransFail_setSens(id, k) {
                // update range
                if (k == "min") {
                    transFailCalendar.setSensitiveRange(TransFail_byId(id).value, null);
                } else {
                    transFailCalendar.setSensitiveRange(null, TransFail_byId(id).value);
                }
            }

            function TransFail_byId(id) {
                return document.getElementById(id);
            }  
                
                
                function transFaildetailExcel(){
                    $("#transFaildetailForm").attr("action","<c:url value="/trans/merchTransFailDetailListExcel" />");
                    document.getElementById("transFaildetailForm").submit();            
                } 
            function transFailDetailMasterInit($form) {

                searchFormInit($form);

                transFail_date_search("week");                    
            } 
                
    </script>
    <body>


        <div class="right" id="transFaildetail_search" style="width:100%;">
             <form id="transFaildetailForm" method="POST" action="<c:url value="/trans/merchTransFailDetailList" />" modelAttribute="transFormBean">

                <input type="hidden" name="page_size" value="100" >
                <input type="hidden" name="page_no" value="1" >
                
                    <table>
                        <tr>
                            <div class="label">거래일</div>
                            <div class="input">
                            <input type="text" size="12" name="app_start_dt" id="transFailForm_date" value ="${transFormBean.app_start_dt}" onclick="TransFail_setSens('transFaildetailForm_to_date', 'max');" /> ~
                            <input type="text"  size="12" name="app_end_dt" id="transFailto_date" value="${transFormBean.app_end_dt}" onclick="TransFail_setSens('transFailto_date', 'min');" /> </div>                            
                            <div class ="input"><input type="button" name="week" value="1주"/></div>
                            <div class ="input"><input type="button" name="month" value="1달"/></div> 
                            <!--
                            <div class="label">거래상태</div>
                            <div class="input">
                                <select name="tran_status">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${tranStatusList.tb_code_details}">
                                        <option value="${code.detail_code}" <c:if test="${code.detail_code == transFormBean.tran_status}">selected</c:if> >${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="label">거래결과</div>
                            <div class="input">
                                <select name="result_status">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${resultStatusList.tb_code_details}">
                                        <option value="${code.detail_code}" <c:if test="${code.detail_code == transFormBean.result_status}">selected</c:if> >${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>    -->                        
                            <div class="label2">UID명</div>
                            <div class="input">
                                <input type="text" name="onfftid_nm" value ="${transFormBean.onfftid_nm}" />
                            </div>    
                            <div class="label2">UID</div>
                            <div class="input">
                                <input type="text" name="onfftid" value ="${transFormBean.onfftid}" />
                            </div>                                
                            <div class="label">고객명</div>
                            <div class="input">
                                <input type="text" name="user_nm" value ="${transFormBean.user_nm}" />
                            </div>                               
                            <div class="label">고객전화번호</div>
                            <div class="input">
                                <input type="text" name="user_phone2" value ="${transFormBean.user_phone2}" />
                            </div>                                                             
                            <div class="label">상품명</div>
                            <div class="input">
                                <input type="text" name="product_nm" value ="${transFormBean.product_nm}" />
                            </div>                                                   
                            <div class="label">승인금액</div>
                            <div class="input">
                                <input type="text" name="tot_amt" value ="${transFormBean.tot_amt}" />
                            </div>                                               
                            <div class="label">카드번호</div>
                            <div class="input">
                                <input type="text" name="card_num" value ="${transFormBean.card_num}" />
                            </div>
                            <div class="label">카드사</div>
                            <div class="input">
                                <select name="iss_cd">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${issCdList.tb_code_details}">
                                        <option value="${code.detail_code}" <c:if test="${code.detail_code == transFormBean.result_status}">selected</c:if> >${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>              

                            <td><input type="button" name="transFaildetail_search" value="조회"/></td>
                            <td><input type="button" name="transFaildetail_excel" value="엑셀"/></td>
                            <td><input type="button" name="init" value="검색조건지우기"/></td>                            
                        </tr>
                    </table>
                        
            </form>

            <div class="paging">
                <div id="transFaildetailPaging" style="width: 50%;"></div>
                <div id="transFaildetailrecinfoArea" style="width: 50%;"></div>
            </div>
                                
         </div>

    </body>
</html>
