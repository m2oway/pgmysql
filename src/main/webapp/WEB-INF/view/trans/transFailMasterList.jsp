<%-- 
    Document   : transFailMasterList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>신용카드 실패거래 조회</title>             
        </head>
        <body>
</c:if>
       
    <body>
        <!--<h1>신용거래</h1>-->
        <div class="right" id="transFail_search" style="width:100%;">       
            <form id="transFailForm" method="POST" onsubmit="return false" action="<c:url value="/trans/transFailMasterList" />" modelAttribute="transFormBean"> 
                <input type="hidden" name="page_size" value="100" >
                <input type="hidden" name="page_no" value="1" >
                <table>
                    <tr>
                        <div class="label">검색일자</div>
                        <div class="input">
                            <input type="text" name="app_start_dt" id="transFailForm_from_date" value ="${transFormBean.app_start_dt}" onclick="setSens('transFailForm_to_date', 'max');" /> ~
                            <input type="text" name="app_end_dt" id="transFailForm_to_date" value="${transFormBean.app_end_dt}" onclick="setSens('transFailForm_from_date', 'min');" />
                        </div>
                        <div class="label">결제채널</div>
                        <div class="input">
                            <select name="pay_chn_cate">
                                <option value="">전체</option>
                                <c:forEach var="code" items="${payChnCateList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <td><input type="button" name="transFail_search" value="조회"/></td>
                        <td><input type="button" name="mcode_search" value="엑셀"/></td>
                    </tr>
               </table>
            </form>
            <div class="paging">
                <div id="transFailPaging" style="width: 50%;"></div>
                <div id="transFailrecinfoArea" style="width: 50%;"></div>
            </div>                        
        </div>
        
         <script type="text/javascript">
            
            var transFailgrid={};
            var transFailCalendar;
            
            $(document).ready(function () {
                
                transFailCalendar = new dhtmlXCalendarObject(["transFailForm_from_date","transFailForm_to_date"]);    
                
                var transFail_searchForm = document.getElementById("transFail_search");
    
                transFail_layout = main_transFail_layout.cells('a').attachLayout("2E");
                     
                transFail_layout.cells('a').hideHeader();
                transFail_layout.cells('b').hideHeader();
                 
                transFail_layout.cells('a').attachObject(transFail_searchForm);
                transFail_layout.cells('a').setHeight(55);
                
                transFailgrid = transFail_layout.cells('b').attachGrid();
                
                transFailgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                
                var transFailheaders = "";
                transFailheaders += "<center>NO</center>,<center>승인일</center>,<center>결제채널</center>,<center>승인금액</center>,<center>취소금액</center>,<center>승인VAT</center>,<center>취소VAT</center>,<center>승인건</center>,<center>취소건</center>";
                transFailgrid.setHeader(transFailheaders);
                transFailgrid.setColAlign("center,center,center,right,right,right,right,right,right");
                transFailgrid.setColTypes("txt,txt,txt,edn,edn,edn,edn,edn,edn");
                transFailgrid.setInitWidths("50,100,100,100,100,100,100,100,100");
                transFailgrid.setColSorting("str,str,str,int,int,int,int,int,int");
                transFailgrid.setNumberFormat("0,000",3);
                transFailgrid.setNumberFormat("0,000",4);
                transFailgrid.setNumberFormat("0,000",5);
                transFailgrid.setNumberFormat("0,000",6);
                transFailgrid.setNumberFormat("0,000",7);
                transFailgrid.setNumberFormat("0,000",8);
                transFailgrid.enableColumnMove(true);
                transFailgrid.setSkin("dhx_skyblue");

                var transfooters = "총계,#cspan,#cspan";
                transfooters += ",<div id='transFail_master3'>0</div>,<div id='transFail_master4'>0</div>,<div id='transFail_master5'>0</div>,<div id='transFail_master6'>0</div>,<div id='transFail_master7'>0</div>,<div id='transFail_master8'>0</div>";
                transFailgrid.attachFooter(transfooters);

                transFailgrid.enablePaging(true,Number($("#transFailForm input[name=page_size]").val()),10,"transFailPaging",true,"transFailrecinfoArea");
                transFailgrid.setPagingSkin("bricks");    
                
                transFailgrid.attachEvent("onPageChanged", function(ind,fInd,lInd){

                    if(lInd !==0){
                        $("#transFailForm input[name=page_no]").val(ind);
                        transFailSearch();
                    }else{
                        return false;
                    }
                    
                });                    

                transFailgrid.init();
                transFailgrid.parse(${ls_transFailMasterList}, transFailFooterValues,"json");

                transFailgrid.attachEvent("onRowDblClicked", transFail_attach);

                //신용거래 정보 검색 이벤트
                $("#transFailForm input[name=transFail_search]").click(function () {
                    
                    $("input[name=page_no]").val("1");
                    transFailSearch();
                    
                });
                  
            });
            
            function transFailFooterValues() {
		    var transFail_master8 = document.getElementById("transFail_master8");
		    var transFail_master3 = document.getElementById("transFail_master3");
		    var transFail_master4 = document.getElementById("transFail_master4");
		    var transFail_master5 = document.getElementById("transFail_master5");
		    var transFail_master6 = document.getElementById("transFail_master6");
		    var transFail_master7 = document.getElementById("transFail_master7");
		    
		    transFail_master8.innerHTML =  putComma(sumColumn(8)) ;
		    transFail_master3.innerHTML =  putComma(sumColumn(3)) ;
		    transFail_master4.innerHTML =  putComma(sumColumn(4)) ;
		    transFail_master5.innerHTML =  putComma(sumColumn(5)) ;
		    transFail_master6.innerHTML =  putComma(sumColumn(6)) ;
		    transFail_master7.innerHTML =  putComma(sumColumn(7)) ;
		    
		    return true;
            }
		
            function sumColumn(ind) {
                var out = 0;
                for (var i = 0; i < transFailgrid.getRowsNum(); i++) {
                     out += parseFloat(transFailgrid.cells2(i, ind).getValue());
                }

                return out;
            }

            //신용거래 상세조회 이벤트
            function transFail_attach(rowid, col) {

                var app_dt = transFailgrid.getUserData(rowid, 'app_dt');
                var pay_chn_cate = transFailgrid.getUserData(rowid, 'pay_chn_cate');
                main_transFail_layout.cells('b').attachURL("<c:url value="/trans/transFailDetailList" />"+"?app_dt=" + app_dt+"&pay_chn_cate="+pay_chn_cate, true);
            }
            
            //신용거래 정보 조회
            function transFailSearch() {
                
                $.ajax({
                    url: $("#transFailForm").attr("action"),
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#transFailForm").serialize(),
                    success: function (data) {

                        transFailgrid.clearAll();
                        transFailgrid.parse($.parseJSON(data),transFailFooterValues, "json");

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }
            
            function setSens(id, k) {
                // update range
                if (k == "min") {
                    transFailCalendar.setSensitiveRange(byId(id).value, null);
                } else {
                    transFailCalendar.setSensitiveRange(null, byId(id).value);
                }
            }

            function byId(id) {
                return document.getElementById(id);
            }                            
                
        </script>
    </body>
</html>
