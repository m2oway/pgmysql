<%-- 
    Document   : companyMasterList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>사업자 정보 관리</title>
        </head>
        <body>
</c:if>
        <script type="text/javascript">
                var companyMasterGrid={};
                var companyMasterWindow;
                var companyMasterLayout;
                
                $(document).ready(function () {
                    var companySearch = document.getElementById("companySearch");            
                    companyMasterLayout = tabbar.cells("3-1").attachLayout("2E");
                    
                    companyMasterLayout.cells('a').hideHeader();
                    companyMasterLayout.cells('b').hideHeader();
                    companyMasterLayout.cells('a').attachObject(companySearch);
                    companyMasterLayout.cells('a').setHeight(75);
                    companyMasterGrid = companyMasterLayout.cells('b').attachGrid();
                    companyMasterGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var companyMasterGridheaders = "";
                    companyMasterGridheaders += "<center>NO</center>,<center>사업자명</center>,<center>사업자번호</center>,<center>법인번호</center>";
                    companyMasterGridheaders += ",<center>대표자</center>,<center>대표번호</center>,<center>팩스</center>,<center>업태</center>,<center>업종</center>,<center>과세구분</center>,<center>등록일</center>";
                    companyMasterGrid.setHeader(companyMasterGridheaders);
                    companyMasterGrid.setColAlign("center,center,center,center,center,center,center,center,center,center,center,center");
                    companyMasterGrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt");
                    companyMasterGrid.setInitWidths("70,150,100,100,100,100,100,100,100,100,100");
                    companyMasterGrid.setColSorting("str,str,str,str,str,str,str,str,str,str,str");
                    companyMasterGrid.enableColumnMove(true);
                    companyMasterGrid.setSkin("dhx_skyblue");
    
                    //companyMasterGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxToolbar/" />");
                    companyMasterGrid.enablePaging(true,Number($("#companyForm input[name=page_size]").val()),10,"companyMasterListPaging",true,"companyMasterListrecinfoArea");
                    companyMasterGrid.setPagingSkin("bricks");

                    //페이징 처리
                    companyMasterGrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        if(lInd !==0){
                          $("#companyForm input[name=page_no]").val(ind);             
                           companyMasterGrid.clearAll();
                           doSearchCompanyMaster();
                        }else{
                            $("#companyForm input[name=page_no]").val(1);
                            companyMasterGrid.clearAll();
                            doSearchCompanyMaster();
                        }                        
                    });
                    
                    companyMasterGrid.init();
                    companyMasterGrid.parse(${companyMasterList}, "json");
                    companyMasterGrid.attachEvent("onRowDblClicked", doUpdateFormCompanyMaster);
                    
                    companyMasterWindow = new dhtmlXWindows();
                    companyMasterWindow.attachViewportTo("companyList");

                    //버튼 추가
                    $("#companyForm input[name=company_searchButton]").bind("click",function(e){
                        //사업체 정보 조회 이벤트
                        /*
                        $("#companyForm input[name=page_no]").val("1");
                        companyMasterGrid.clearAll();
                        doSearchCompanyMaster();
                        */
                        companyMasterGrid.changePage(1);
                        return false;
                    });

                    $("#companyForm .addButton").button({
                        icons: {
                            primary: "ui-icon-plusthick"
                        },
                        text: false
                    })
                    .unbind("click")
                    .bind("click",function(e){
                        //사업체 정보 등록 이벤트
                        doInsertFormCompanyMaster();
                        return false;
                    });

                });

                //사업체 정보 수정
                function doUpdateFormCompanyMaster(rowid, col){
                    //alert(rowid);
                    //alert(col);
                    //alert(companyMasterGrid.getUserData(rowid,"comp_seq"));
                    w1 = companyMasterWindow.createWindow("w1", 150, 150, 700, 500);
                    w1.setText("사업자정보 수정");
                    companyMasterWindow.window('w1').setModal(true);
                    w1.attachURL("<c:url value="/company/companyMasterUpdate" />" + "?comp_seq=" + companyMasterGrid.getUserData(rowid,"comp_seq"),true);   
                    
                }

                function resizeOption($id){
                    //css 변경 : resize시 자동 변경됨.
                    $id.css("width","100%");
                    $id.css("height","100%");
                }


                //사업체 정보 조회
                function doSearchCompanyMaster(){
                    $.ajax({
                        url : $("#companyForm").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#companyForm").serialize(),
                        success : function(data) {
                            //alert(data);
                            //companyMasterGrid.clearAll();
                            //companyMasterGrid.parse(jQuery.parseJSON(data), "json");
                            var jsonData = $.parseJSON(data);     
                            companyMasterGrid.parse(jsonData,"json");
                            //resizeOption($("#companyList.gridbox"));
                        },
                        error : function() {
                            alert("조회 실패");
                        }
                    });
                }

                //사업체 정보 등록
                function doInsertFormCompanyMaster(){

                    w1 = companyMasterWindow.createWindow("w1", 150, 150, 680, 450);
                    //w1 = companyMasterWindow.createWindow("editwin", 150, 150, 700, 400);
                    w1.setText("사업체 등록");
                    companyMasterWindow.window('w1').setModal(true);
                    w1.attachURL("<c:url value="/company/companyMasterInsert" />",true);   

                }
                
                //사업자 정보 검색 팝업
                /*
                $("#companyForm input[name=comp_nm]").click(function () {
                    
                    CompanyComSelectWin();
                    
                });
                */
                $("#companyForm input[name=init]").click(function () {

                    companyMasterInit($("#companyForm"));

                });  
                
                function companyMasterInit($form) {
                    searchFormInit($form);
                } 
                
                var dhxSelPopupWins;
		dhxSelPopupWins = new dhtmlXWindows();
                function CompanyComSelectWin()
                {
                     onoffObject.comp_nm = $("#companyForm input[name=comp_nm]");
                     
                    w2 = dhxSelPopupWins.createWindow("companySelPopupList", 20, 30, 640, 480);
                    w2.setText("사업자번호 선택페이지");
                    dhxSelPopupWins.window('companySelPopupList').setModal(true);
                    w2.attachURL('<c:url value = "/popup/CompanySelectPopUp"/>', true);
                } 
            </script>            
            <div class="searchForm1row" id="companySearch" style="display:none;width:100%;height:100%;background-color:white;">
                <form id="companyForm" method="POST" action="<c:url value="/company/companyMasterList" />" modelAttribute="companyFormBean">
                       <input type="hidden" name="page_size" value="100" />
                       <input type="hidden" name="page_no" value="1" />
                       <input type="hidden" name="comp_seqs" value="" />
                        
                        <div class="label">사업자명</div>
                        <div class="input"><input type="text" name="comp_nm" maxlength="30" /></div>

                        <div class="label">법인번호</div>
                        <div class="input"><input type="text" name="corp_no" maxlength="13"/></div>

                        <div class="label">사업자번호</div>
                        <div class="input"><input type="text" name="biz_no" maxlength="10"/></div>

                        <div class="label">상태</div>
                        <div class="input">
                            <select name="use_flag">
                                <option value="Y">사용가능</option>
                                <option value="N">사용불가</option>
                            </select>
                        </div>                        
                        <input type="button" name="company_searchButton" value="조회"/>                        
                        <input type="button" name="init" value="검색조건지우기"/>
                        <div class="action">
                            <button class="addButton">추가</button>
                        </div>
                </form>
                <div id="companyMasterListPaging" style="width: 100%;"></div>
                <div id="companyMasterListrecinfoArea" style="width: 100%;"></div>                        
            </div>
            <div id="companyList" style="width:100%;height:100%;background-color:white;"></div>
 <c:if test="${!ajaxRequest}">    
        </body>
</html>
</c:if>

