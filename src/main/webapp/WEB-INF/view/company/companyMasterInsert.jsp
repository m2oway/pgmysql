<%-- 
    Document   : companyMasterInsert
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>사업체 정보 관리 등록</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">
                //개인법인 변화
                function compcateAction(obj)
                {
                    var selval = obj.value;
                    
                    if(selval=='2' || selval=='3' || selval=='4')
                    {
                        $("#companyFormInsert input[name=corp_no]").keypress(InpuOnlyNumber_Jquery($("#companyFormInsert input[name=corp_no]")));
						if(selval=='2')
						{
							//$("#companyFormInsert input[name=corp_no]").blur(checkLength_Jquery($("#companyFormInsert input[name=corp_no]"),13));
							$("#companyFormInsert input[name=corp_no]").blur(checkLength_Jquery($("#companyFormInsert input[name=corp_no]"),20));
	                        $("#companyFormInsert input[name=corp_no]").attr('readOnly',false); 
						}
						else
						{
							$("#companyFormInsert input[name=corp_no]").attr('readOnly',false); 
						}
                        
                    }
                    else
                    {
                        $("#companyFormInsert input[name=corp_no]").attr('value','');
                        $("#companyFormInsert input[name=corp_no]").attr('readOnly',true);
                    }
                }
                
                
                $(document).ready(function () {
                    
                    //사업체 정보 등록 이벤트
                    $("#companyFormInsert input[name=insert]").click(function(){
                       doInsertCompanyMaster(); 
                    });              
                    
                    //사업체 정보 등록 닫기
                    $("#companyFormInsert input[name=close]").click(function(){
                        companyMasterWindow.window("w1").close();
                    });                            
                    
                });

                //사업체 정보 등록
                function doInsertCompanyMaster(){
                    
                    if(!window.confirm("등록하시겠습니까?")){
                        return;
                    }
                    
                    if($("#companyFormInsert input[name=biz_no]").val() == "")
                    {
                      alert("사업자번호를 입력해 주세요.");
                      $("#companyFormInsert input[name=biz_no]").focus();
                      return;
                    }
                    
                    if($("#companyFormInsert select[name=comp_cate]").val() == "2"  || $("#companyFormInsert select[name=comp_cate]").val() == "3" || $("#companyFormInsert select[name=comp_cate]").val() == "4" )
                    {
                        if($("#companyFormInsert input[name=corp_no]").val() == "")
                        {
                            alert("법인번호를 입력해 주세요.");
                            $("#companyFormInsert input[name=corp_no]").focus();
                            return;
                        }
                     
                    }
                    
                    if($("#companyFormInsert input[name=comp_ceo_nm]").val() == "")
                    {
                      alert("대표자명을 입력해 주세요.");
                      $("#companyFormInsert input[name=comp_ceo_nm]").focus();
                      return;
                    }
                    
                    if($("#companyFormInsert input[name=comp_nm]").val() == "")
                    {
                      alert("사업체명을 입력해 주세요.");
                      $("#companyFormInsert input[name=comp_nm]").focus();
                      return;
                    }
                    
                    if($("#companyFormInsert input[name=comp_email]").val() == "")
                    {
                      alert("이메일을 입력해 주세요.");
                      $("#companyFormInsert input[name=comp_email]").focus();
                      return;
                    }                    
                    
                    $.ajax({
                        url : $("#companyFormInsert").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#companyFormInsert").serialize(),
                        success : function(data) {
                            //alert(data);
                            if(data=='1')
                            {
                                alert("등록 완료");
                                companyMasterGrid.clearAll();
                                doSearchCompanyMaster();
                                companyMasterWindow.window("w1").close();
                            }
                            else
                            {
                                alert("등록 실패");
                            }
                        },
                        error : function() { 
                                alert("등록 실패");
                        }
                    });                    
                    
                }                   

            </script>
        <div style='width:100%;height:100%;overflow:auto;'>
            <form id="companyFormInsert" method="POST" action="<c:url value="/company/companyMasterInsert" />" modelAttribute="companyFormBean">
                <table class="gridtable" height="100%" width="100%">                    
                    <tr>
                        <td class="headcol" width="15%">사업자번호*</td>
                        <td><input name="biz_no" maxlength="20" onkeyPress="javascript:InpuOnlyNumber(this);" onblur="javascript:checkLength(this,20);"/></td>
                        <td class="headcol" width="15%">법인번호</td>
                        <td><input name="corp_no" maxlength="20" onkeyPress="javascript:InpuOnlyNumber(this);" onblur="javascript:checkLength(this,20);" readonly="true"/></td>
                    </tr>
                    <tr>
                        <td class="headcol">대표자명*</td>
                        <td><input name="comp_ceo_nm" onblur="javascript:checkLength(this,20);"/></td>
                        <td class="headcol">사업자명*</td>
                        <td><input name="comp_nm" onblur="javascript:checkLength(this,20);"/></td>
                    </tr>
                    <tr>
                        <td class="headcol">개인/법인</td>
                        <td>
                            <select name="comp_cate" onchange="javascript:compcateAction(this);">
                                <c:forEach var="code" items="${compCateList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td class="headcol">과세구분</td>
                        <td>
                            <select name="tax_flag">
                                <c:forEach var="code" items="${TaxFlagList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </td>                        
                    </tr>   
                    <tr>
                        <td class="headcol">업태</td>
                        <td><input name="biz_type" onblur="javascript:checkLength(this,50);"/></td>
                        <td class="headcol">업종</td>
                        <td><input name="biz_cate" onblur="javascript:checkLength(this,50);"/></td>
                    </tr>                    
                    <tr>
                        <td class="headcol">대표번호</td>
                        <td><input name="comp_tel1" /></td>
                        <td class="headcol">FAX</td>
                        <td><input name="comp_tel2" /></td>
                    </tr>                    
                    <tr>
                        <td class="headcol">이메일*</td>
                        <td><input name="comp_email" /></td>                        
                        <td class="headcol">우편번호</td>
                        <td ><input name="zip_cd" maxlength="6" /></td>
                    </tr>
                    <tr>
                        <td class="headcol">주소</tD>
                        <td colspan="3"><input name="addr_1" size="30" />&nbsp;<input name="addr_2" size="30"/></td>                        
                                      
                    <tr>
                        <td class="headcol">Memo</td>
                        <td colspan="3"><textarea name="memo" cols="60" rows="5"></textarea></td>                        
                    </tr>                          
                    <tr>
                        <td colspan="4" align="center"><input type="button" name="insert" value="등록"/>&nbsp;<input type="button" name="close" value="닫기"/></td>
                    </tr>
                </table>
            </form>
        </div>
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

