<%-- 
    Document   : companyMasterUpdate
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>사업체 정보 관리 수정</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">
                
                
                $(document).ready(function () {
                    
                    //사업체 정보 수정 이벤트
                    $("#companyFormUpdate input[name=update]").click(function(){
                       doUpdateCompanyMaster(); 
                    });             
                    
                    //onffmerchant delete 이벤트
                    $("#companyFormUpdate input[name=delete]").click(function(){
                       doDeleteCompanyMaster(); 
                    });     
                    
                    //사업체 정보 수정 닫기
                    $("#companyFormUpdate input[name=close]").click(function(){       

                        companyMasterWindow.window("w1").close();
                        
                    });                           
                    
                });

                //개인법인 변화
                function compcateAction(obj)
                {
                    var selval = obj.value;
                    
                    if(selval=='2' || selval=='3' || selval=='4')
                    {
                        $("#companyFormUpdate input[name=corp_no]").keypress(InpuOnlyNumber_Jquery($("#companyFormUpdate input[name=corp_no]")));
						//$("#companyFormUpdate input[name=corp_no]").blur(checkLength_Jquery($("#companyFormUpdate input[name=corp_no]"),13));
						//$("#companyFormUpdate input[name=corp_no]").blur(checkLength_Jquery($("#companyFormUpdate input[name=corp_no]"),20));
                        //$("#companyFormUpdate input[name=corp_no]").attr('readOnly',false); 

						if(selval=='2')
						{
							$("#companyFormInsert input[name=corp_no]").blur(checkLength_Jquery($("#companyFormInsert input[name=corp_no]"),13));
	                        $("#companyFormInsert input[name=corp_no]").attr('readOnly',false); 
						}
						else
						{
							$("#companyFormInsert input[name=corp_no]").blur(checkLength_Jquery($("#companyFormInsert input[name=corp_no]"),20));
							$("#companyFormInsert input[name=corp_no]").attr('readOnly',false); 
						}
                    }
                    else
                    {
                        $("#companyFormUpdate input[name=corp_no]").attr('value','');
                        $("#companyFormUpdate input[name=corp_no]").attr('readOnly',true);
                    }
                }
                

                //사업체 정보 수정
                function doUpdateCompanyMaster(){
                    
                    if(!window.confirm("수정하시겠습니까?")){
                        return;
                    }
                    
                    if($("#companyFormUpdate input[name=biz_no]").val() == "")
                    {
                      alert("사업자번호를 입력해 주세요.");
                      $("#companyFormUpdate input[name=biz_no]").focus();
                      return;
                    }
                    
                    if($("#companyFormUpdate select[name=comp_cate]").val() == "2" || $("#companyFormUpdate select[name=comp_cate]").val() == "3")
                    {
                        if($("#companyFormUpdate input[name=corp_no]").val() == "")
                        {
                            alert("법인번호를 입력해 주세요.");
                            $("#companyFormUpdate input[name=corp_no]").focus();
                            return;
                        }
                     
                    }
                    
                    if($("#companyFormUpdate input[name=comp_ceo_nm]").val() == "")
                    {
                      alert("대표자명을 입력해 주세요.");
                      $("#companyFormUpdate input[name=comp_ceo_nm]").focus();
                      return;
                    }
                    
                    if($("#companyFormUpdate input[name=comp_nm]").val() == "")
                    {
                      alert("사업체명을 입력해 주세요.");
                      $("#companyFormUpdate input[name=comp_nm]").focus();
                      return;
                    }
                    if($("#companyFormUpdate input[name=comp_email]").val() == "")
                    {
                      alert("이메일을 입력해 주세요.");
                      $("#companyFormUpdate input[name=comp_email]").focus();
                      return;
                    }                                        

                    $.ajax({
                            url : $("#companyFormUpdate").attr("action"),
                            type : "POST",
                            async : true,
                            dataType : "json",
                            data: $("#companyFormUpdate").serialize(),
                            success : function(data) {
                                if(data=='1')
                                {
                                    alert("수정 완료");
                                    companyMasterGrid.clearAll();
                                    doSearchCompanyMaster();
                                    companyMasterWindow.window("w1").close();
                                }
                                else
                                {
                                    alert("수정 실패");
                                }
                            },
                            error : function() { 
                                    alert("수정 실패");
                            }
                        });                    
        
                }                   


              
                //가맹점정보 삭제
                function doDeleteCompanyMaster(){
                    
                    if(!window.confirm("삭제하시겠습니까?")){
                        return;
                    }
                    

                    $.ajax({
                        url :"../company/companyMasterDelete",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#companyFormUpdate").serialize(),
                        success : function(data) {
                            //alert(data);
                            if(data=='1')
                            {
                                alert("삭제 완료");
                                companyMasterGrid.clearAll();
                                doSearchCompanyMaster();
                                companyMasterWindow.window("w1").close();
                            }
                            else
                            {
                                alert("삭제 실패");
                            }
                        },
                        error : function() { 
                                alert("삭제 실패");
                        }
                    });
                }         
            </script>
            <!--
        <div style='width:100%;hieght:100%;overflow:scroll;'>
           -->
            <form id="companyFormUpdate" method="POST" action="<c:url value="/company/companyMasterUpdate" />" modelAttribute="companyFormBean">
                <input type="hidden" name="comp_seq" value="${tb_Company.comp_seq}" />
                <table class="gridtable" height="90%" width="100%">
                    <tr>
                        <td class="headcol" width="15%">사업자번호</td>
                        <td><input name="biz_no" value="${tb_Company.biz_no}" maxlength="10" onkeyPress="javascript:InpuOnlyNumber(this);" onblur="javascript:checkLength(this,10);" /></td>
                        <td class="headcol" width="15%">법인번호</td>
                        <td><input name="corp_no" value="${tb_Company.corp_no}" maxlength="20" onkeyPress="javascript:InpuOnlyNumber(this);" onblur="javascript:checkLength(this,20);" /></td>
                    </tr>
                    <tr>
                        <td class="headcol">대표자명</td>
                        <td><input name="comp_ceo_nm" value="${tb_Company.comp_ceo_nm}" onblur="javascript:checkLength(this,20);"/></td>
                        <td class="headcol">사업자명</td>
                        <td><input name="comp_nm" value="${tb_Company.comp_nm}" onblur="javascript:checkLength(this,20);"/></td>
                    </tr>
                    <tr>
                        <td class="headcol">개인/법인</td>
                        <td >
                            <select name="comp_cate" onchange="javascript:compcateAction(this);">
                                <c:forEach var="code" items="${compCateList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Company.comp_cate}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach>
                            </select>                            
                        </td>
                        <td class="headcol">과세구분</td>
                        <td>
                            <select name="tax_flag" >
                                <c:forEach var="code" items="${TaxFlagList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Company.tax_flag}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </td>                        
                    </tr>   
                    <tr>
                        <td class="headcol">업태</td>
                        <td><input name="biz_type" value="${tb_Company.biz_type}" onblur="javascript:checkLength(this,50);"/></td>
                        <td class="headcol">업종</td>
                        <td><input name="biz_cate" value="${tb_Company.biz_cate}" onblur="javascript:checkLength(this,50);"/></td>
                    </tr>                    
                    <tr>
                        <td class="headcol">연락처1</td>
                        <td><input name="comp_tel1" value="${tb_Company.comp_tel1}" /></td>
                        <td class="headcol">연락처2</td>
                        <td><input name="comp_tel2" value="${tb_Company.comp_tel2}" /></td>
                    </tr>                    
                    <tr>
                        <td class="headcol">이메일*</td>
                        <td><input name="comp_email" value="${tb_Company.comp_email}"/></td>                               
                        <td class="headcol">우편번호</td>
                        <td colspan="3"><input name="zip_cd" value="${tb_Company.zip_cd}" maxlength="6" /></td>
                    </tr>   
                    <tr>
                        <td class="headcol">주소</td>
                        <td colspan="3"><input name="addr_1" value="${tb_Company.addr_1}" size="30" />&nbsp;<input name="addr_2" value="${tb_Company.addr_2}" size="30"/></td>                        
                    </tr>
                    <tr>
                        <td class="headcol">Memo</td>
                        <td colspan="3"><textarea name="memo" cols="60" rows="5">${tb_Company.memo}</textarea></td>                        
                    </tr>                           
                    <tr>
                        <td class="headcol">등록일</td>
                        <td>${tb_Company.ins_dt}</td>
                        <td class="headcol">등록자</td>
                        <td>${tb_Company.ins_user}</td>
                    </tr>                          
                    <tr>
                        <td colspan="4" align="center"><input type="button" name="update" value="수정"/>&nbsp;<input type="button" name="delete" value="삭제"/>&nbsp;<input type="button" name="close" value="닫기"/></td>
                    </tr>
                </table>
            </form>
                    <!--
            </div>
                    -->
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

