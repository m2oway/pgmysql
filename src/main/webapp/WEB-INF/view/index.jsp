<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>ITP</title>
	</head>
	<body>
		<%@include file="/WEB-INF/view/include/head_resource_main.jsp" %>
		<div id="container">
			<!--topWarapper-->
			<%@include file="/WEB-INF/view/include/topWrapper.jsp" %>
			<div class="layout-container">	
				<div class="mainlayout-contents layout-body">
					<div class="layout-main">
						<div class="main-contents">
							<div class="main-img"></div>
							<div class="main-center">
								<div class="main-note">
									<ul>
										<li class="main-notehead">공지사항</li>
										<li class="main-notemore"><a class="notehead-text" href="#">+</a></li>
									</ul>
									<div class="main-notebody">
										<ul>
											<li class="main-notebody-text">여기는 공지 사항 란입니다.여기는 공지 사항 란입니다.여기는 공지 사항 란입니sdfsdfsd </li>
											<li class="main-notebody-text">여기는 공지 사항 란입니다.여기는 공지 사항 란입니다.여기는 공지 </li>
											<li class="main-notebody-text">여기는 공지 사항 란입니다.여기는 공지 사항 란입니다.여기는 공지 </li>
											<li class="main-notebody-text">여기는 공지 사항 란입니다.여기는 공지 사항 란입니다.여기는 공지 </li>
											<li class="main-notebody-text">여기는 공지 사항 란입니다.여기는 공지 사항 란입니다.여기는 공지 </li>
											<li class="main-notebody-text">여기는 공지 사항 란입니다.여기는 공지 사항 란입니다.여기는 공지 </li>
											<li class="main-notebody-text">여기는 공지 사항 란입니다.여기는 공지 사항 란입니다.여기는 공지 </li>
										</ul>
									</div>
								</div>
								<div class="main-itplyer-number">
									<ul class="number-allbox">
										<li class="number-box">
											<span class="number-more"><a class="number-morelink" href="#">more</a></span>
											<div class="number-alltitle">
												<span class="number-title">나를 지목한</span>
												<span class="number-title1">PROJECT</span>
											</div>
											<div class="clean"/>
											<div class="number-counter-box">
												<span class="number-counter">5</span>
												<span class="number-counter-title">건</span>
											</div>
										</li>
										<li class="number-box1">
											<span class="number-more"><a class="number-morelink" href="#">more</a></span>
											<div class="number-alltitle">
												<span class="number-title">내가 지원한</span>
												<span class="number-title1">PROJECT</span>
											</div>
											<div class="clean"/>
											<div class="number-counter-box">
												<span class="number-counter">0,000,405</span>
												<span class="number-counter-title">건</span>
											</div>
										</li>
										<li class="number-box1">
											<span class="number-more"><a class="number-morelink" href="#">more</a></span>
											<div class="number-alltitle">
												<span class="number-title">추천</span>
												<span class="number-title1">PROJECT</span>
											</div>
											<div class="clean"/>
											<div class="number-counter-box">
												<span class="number-counter">0,000,005</span>
												<span class="number-counter-title">건</span>
											</div>
										</li>
									</ul>
									<ul class="number-allbox">
										<li class="number-box2">
											<span class="number-box2-title">전체 현황</span>
											<span class="number-box2-more"><a class="number-box2-morelink" href="#">more</a></span>
											<div class="number-box2-text">현재 ITPlayrs에 등록된 업체와 인재의 총집계입니다.</div>
											<div class="number-box2-line"></div>
											<div class="number-box2-counter-box">
												<span class="number-box2-counter">302,319</span>
												<span class="number-counter-title">건</span>
											</div>
											<div class="number-box2-line"></div>
											<div class="number-box2-counter-box">
												<span class="number-box2-counter">2,319</span>
												<span class="number-counter-title">건</span>
											</div>
										</li>
									</ul>
								</div>
								<div class="clean"></div>
							</div>
							<div class="main-iconlink">
								<div class="main-icon-box">
									<ul>
										<a class="main-link" href="#">
											<li class="main-icon">
												<ul>
													<li class="main-icon-title">프로젝트 등록</li>
													<li class="main-icon-text">ITPalyers에서 다양한<br />인제를 만나보세요</li>
												</ul>
											</li>
										</a>
										<a class="main-link" href="#">
										<li class="main-icon main-iconleft">
											<ul>
												<li class="main-icon-title">내이력서 등록</li>
												<li class="main-icon-text">ITPalyers에서 믿을 수<br />있는 기업을 만나보시기 <br />바랍니다.</li>
											</ul>
										</li>
										</a>
										<a class="main-link" href="#">
										<li class="main-icon main-icontop">
											<ul>
												<li class="main-icon-title">프로젝트 검색</li>
												<li class="main-icon-text">ITPalyers에서는 필요한<br />인재들을 손쉽게 검색<br />하실 수 있습니다.</li>
											</ul>
										</li>
										</a>
										<a class="main-link" href="#">
										<li class="main-icon main-icontop main-iconleft">
											<ul>
												<li class="main-icon-title">ITPalyers 검색</li>
												<li class="main-icon-text">ITPalyers에서는 편리한 <br />검색 시스템으로 기업에 <br />맞는 ITP를 검색해 <br /> 드립니다.</li>
											</ul>
										</li>
										</a>
									</ul>
								</div>
								<div class="main-icon-contatc"></div>
							</div>
							<div class="main-tabmanu">
								<div id="tap">
									<div id="tabs">
										<ul class="tap_m">
											<li class="text_box" id="t_box"><a href="#tabs-1">전체보기</a></li>
											<li class="text_box"><a href="#tabs-2">JAVA</a></li>
											<li class="text_box"><a href="#tabs-3">C/C++</a></li>
											<li class="text_box"><a href="#tabs-4">.NET</a></li>
											<li class="text_box"><a href="#tabs-5">ASP/PHP</a></li>
											<li class="text_box"><a href="#tabs-6">Design</a></li>
											<li class="text_box"><a href="#tabs-7">기획</a></li>
											<li class="text_box"><a href="#tabs-8">기타</a></li>
											<li class="clean"></li>
										</ul>
										<div class="tap_box" id="tabs-1">
											<ul>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">C/C++</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
											</ul>
										</div>
										<div class="tap_box" id="tabs-2">
											<ul>
												<li class="tap_list"><span class="text_a">C/C++</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] ...</span><span class="text_c">1개월</span><span class="text_d">6.000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
											</ul>
										</div>
										<div class="tap_box" id="tabs-3">
											<ul>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">C/C++</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
											</ul>
										</div>
										<div class="tap_box" id="tabs-4">
											<ul>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">C/C++</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
											</ul>
										</div>
										<div class="tap_box" id="tabs-5">
											<ul>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">C/C++</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
											</ul>
										</div>
										<div class="tap_box" id="tabs-6">
											<ul>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">C/C++</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
											</ul>
										</div>
										<div class="tap_box" id="tabs-7">
											<ul>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">C/C++</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
											</ul>
										</div>
										<div class="tap_box" id="tabs-8">
											<ul>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">C/C++</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
												<li class="tap_list"><span class="text_a">퍼블리셔</span><span class="text_b">[퍼블리셔] 용산국제업무지구 웹사이트 관....</span><span class="text_c">12개월</span><span class="text_d">6.000.0000</span><span class="text_e">04/30</span></li>
											</ul>
										</div>
									</div>	
								</div> 
							</div>
						</div>						
					</div>
				</div>
			</div>
			<!--btmWarpper-->
			<%@include file="/WEB-INF/view/include/btmWrapper.jsp" %>
		</div>
		<script>
			// Tabs
			$('#tabs').tabs();
			
			$(document).ready(function(){
                                alert('>>>>>>>>>>>');
		 		if("${loginStatus}" !== ""){
		 			alert("${loginStatus}");
		 		}
			});
		</script>
</body>
</html>
