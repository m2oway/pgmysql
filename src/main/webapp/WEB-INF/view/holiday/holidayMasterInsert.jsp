<%-- 
    Document   : userMaterInsert
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>휴일  정보 관리 등록</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">
                var userInsertWins;
            
                $(document).ready(function () {
                    
                    transCalendar2 = new dhtmlXCalendarObject(["holiday_date"]); 
                
                    userInsertWins = new dhtmlXWindows();
                    
                    //휴일  정보 등록 이벤트
                    $("#userFormInsert input[name=user_insert]").click(function(){
                       userInsert(); 
                    });              
                    
                    //휴일  정보 등록 닫기
                    $("#userFormInsert input[name=user_close]").click(function(){
                        holidayWins.window('holidayw1').close();
                    });          
                    
                    //사용자구분에 따른 팝업 이벤트
                    $("#userFormInsert input[name=user_cate_nm]").click(function(){
                        user_cate = $("#userFormInsert select[name=user_cate]").val();
                        if(user_cate === '01'){ //정산관리자
                            onoffMerchSelectPopup();
                        }else if(user_cate === '02'){ //결제관리자
                            onoffTidSelectPopup();                            
                        }else if(user_cate === '03'){ //대리점관리자
                            agentSelectPopUp();
                        }
                    });        
                    
                    $("#userFormInsert select[name=user_cate]").change(function(){
                        user_cate = $(this).val();
                        if(user_cate === '00'){
                            $("#userFormInsert input[name=user_cate_nm]").attr("disabled","disabled");
                        }else{
                            $("#userFormInsert input[name=user_cate_nm]").removeAttr("disabled");
                        }
                        $("#userFormInsert input[name=user_cate_nm]").val("");
                    });                    
                    
                });
                
                var dhxSelPopupWins=new dhtmlXWindows();
                
                //대리점정보 팝업
                function agentSelectPopUp(){
                    w2 = dhxSelPopupWins.createWindow("AgentSelectPopUp", 20, 30, 640, 480);
                    w2.setText("대리점선택페이지");
                    dhxSelPopupWins.window('AgentSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('AgentSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/AgentSelectPopUp"/>', true);
                }                

                function SelAgentInfoInput(p_agent_seq, p_agent_nm){
                    $("#userFormInsert input[name=agent_seq]").val(p_agent_seq);
                    $("#userFormInsert input[name=user_cate_nm]").val(p_agent_nm);
                }             
                
                function onoffMerchSelectPopup(){
                    //ONOFF가맹점정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("가맹점선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                }
              
                function SelOnoffmerchInfoInput(p_onffmerch_no, p_merch_nm){
                    $("#userFormInsert input[name=onffmerch_no]").val(p_onffmerch_no);
                    $("#userFormInsert input[name=user_cate_nm]").val(p_merch_nm);                    
                }               
                
                function onoffTidSelectPopup(){
                    //ONOFF가맹점정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnofftidSelectPopUp", 20, 30, 640, 480);
                    w2.setText("결제TID 선택페이지");
                    dhxSelPopupWins.window('OnofftidSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnofftidSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnofftidSelectPopUp"/>', true);   
                }                
              
                function SelOnofftidInfoInput(p_onffmerch_no, p_onfftid, p_onfftid_nm){
                    $("#userFormInsert input[name=onffmerch_no]").val(p_onffmerch_no);
                    $("#userFormInsert input[name=onfftid]").val(p_onfftid);
                    $("#userFormInsert input[name=user_cate_nm]").val(p_onfftid_nm);                    
                }                

                //휴일  정보 등록
                function userInsert(){   
                    
                    
                    
                    if($("#userFormInsert input[name=holiday_date]").val().trim().length === 0){
                        alert("날짜를 선택하여 주십시오.");
                        $("#userFormInsert input[name=holiday_date]").val("");
                        $("#userFormInsert input[name=holiday_date]").focus();
                        return false;
                    }
                    
                    
                    $("#userFormInsert input[name=yyyymmdd]").val(($("#userFormInsert input[name=holiday_date]").val()).replace( /[^(0-9)]/g,""));
                    
                    if($("#userFormInsert input[name=holidayname]").val().trim().length === 0){
                        alert("휴일명을 입력하여 주십시오.");
                        $("#userFormInsert input[name=holidayname]").val("");
                        $("#userFormInsert input[name=holidayname]").focus();
                        return false;
                    }
                    
                    
                    
                    $.ajax({
                        url : $("#userFormInsert").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#userFormInsert").serialize(),
                        success : function(data) {
                            alert("등록 완료 ");
                            holidaySearch();
                            holidayWins.window('holidayw1').close();
                        },
                        error : function() { 
                                alert("등록 실패");
                        }
                    });	      
        
                } 

            </script>

            <form id="userFormInsert" method="POST" action="<c:url value="/holiday/holidayMasterInsert" />" modelAttribute="userFormBean">
                  <input type = "hidden" name = "result_colum" value = "userFormInsert">
                  <input type = "hidden" name="onffmerch_no" value="">
                  <input type = "hidden" name="onfftid" value="">
                  <input type = "hidden" name="agent_seq" value="">                  
                  
                <table class="gridtable">
                    <tr>
                        <th>날짜</th>
                        <td>
                            <input type="hidden" name="yyyymmdd" id="yyyymmdd" value =""  /> 
                            <input type="text" name="holiday_date" id="holiday_date" value =""  /> 
                        </td>
                        <th>휴일명</th>
                        <td><input name="holidayname" maxlength="20"/></td>
                    </tr> 
                    <tr>
                        <td style="text-align: center" colspan="4" >
                            <input type="button" name="user_insert" value="저장"/>
                            <input type="button" name="user_close" value="닫기"/>
                        </td>
                    </tr>
                </table>
            </form>

<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

