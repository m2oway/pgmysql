<%-- 
    Document   : holidayMaster
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>휴일관리</title>             
        </head>
        <body>
</c:if>
       
    <body>
        <!--<h1>신용거래</h1>-->
        <div  class="searchForm1row" class="right" id="holiday_search" style="width:100%;">       
            <form id="holidayForm" method="POST" onsubmit="return false" action="<c:url value="/holiday/holidayMasterList" />" modelAttribute="holidayFormBean"> 
                <input type = "hidden" name = "yyyymmdd" value ="">
                <input type="hidden" name="page_size" value="100" >
                <input type="hidden" name="page_no" value="1" >                 
                <table>
                   <tr>
                        <td>일자</td>
                        <td><input type="text" name="holiday_date_start" id="holiday_date_start" value ="${holidayFormBean.holiday_date_start}"  /> ~
                            <input type="text" name="holiday_date_end" id="holiday_date_end" value="${holidayFormBean.holiday_date_end}" />
                            <input type="button" name="holiday_date_search" value="조회"/></td>
                        <c:if test="${holidayFormBean.ses_user_cate == '00'}">
                            <td><input type="button" name="holiday_insert" value="등록"/></td>
                        </c:if>
                    </tr>                   
               </table>
            </form>
            <div class="paging">
                <div id="holidayPaging" style="width: 50%;"></div>
                <div id="holidayrecinfoArea" style="width: 50%;"></div>
            </div>                                        
        </div>
        <div id="holiday" style="position: relative;width:100%;height:100%;background-color:white;"></div>
        
        
         <script type="text/javascript">
            
            var holidaygrid={};
            var holidayWins;
            var holidayupdateWins;
            var holidayCalendar;
            
            $(document).ready(function () {
                
                transCalendar = new dhtmlXCalendarObject(["holiday_date_start","holiday_date_end"]); 
                
                holidayWins = new dhtmlXWindows();
                holidayupdateWins = new dhtmlXWindows();

                holidayWins.attachViewportTo("holiday_search");
                
                var holiday_searchForm = document.getElementById("holiday_search");
                var tabBarId = tabbar.getActiveTab();
                    
                holiday_layout = tabbar.cells(tabBarId).attachLayout("2E");
                     
                holiday_layout.cells('a').hideHeader();
                holiday_layout.cells('b').hideHeader();                 
                holiday_layout.cells('a').attachObject(holiday_searchForm);
                holiday_layout.cells('a').setHeight(80);
                
                holidaygrid = holiday_layout.cells('b').attachGrid();
                
                holidaygrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                
                var mcheaders = "";
                mcheaders += "<center>NO</center>,<center>날자</center>,<center>휴일명</center>";
                holidaygrid.setHeader(mcheaders);
                holidaygrid.setColAlign("center,center,center");
                holidaygrid.setColTypes("txt,txt,txt");
                holidaygrid.setInitWidths("70,100,100");
                holidaygrid.setColSorting("str,str,str");
                holidaygrid.enableColumnMove(true);
                holidaygrid.setSkin("dhx_skyblue");
                
                holidaygrid.enablePaging(true,Number($("#holidayForm input[name=page_size]").val()),10,"holidayPaging",true,"holidayrecinfoArea");
                holidaygrid.setPagingSkin("bricks");    
                
                holidaygrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        if(lInd !==0){
                          $("#holidayForm input[name=page_no]").val(ind);             
                           holidaygrid.clearAll();
                           holidaySearch();
                        }else{
                            $("#holidayForm input[name=page_no]").val(1);
                            holidaygrid.clearAll();
                            holidaySearch();
                        }                          
                    
                });                        
                
                holidaygrid.init();

                holidaygrid.parse(${HolidayMasterList}, "json");

                holidaygrid.attachEvent("onRowDblClicked", holiday_attach);

                //회원 정보 검색 이벤트
                $("#holidayForm input[name=holiday_date_search]").click(function () {
                    /*          
                    $("input[name=page_no]").val("1");
                                holidaySearch();
                    */            
                    holidaygrid.changePage(1);
                    return false;
                });
                
                $("#holidayForm input[name=holiday_insert]").click(function () {
                    PopHolidayInsert();
                });

            });
            
            function holiday_attach(rowid, col){         
                var dhtmlxwindowSize = {};
                dhtmlxwindowSize.width = 390;
                dhtmlxwindowSize.height = 140;

               var location = dhxwindowCenterAlgin(dhtmlxwindowSize);
               var holiday_seq = holidaygrid.getUserData(rowid, 'yyyymmdd');

               holidayupdatew1 = holidayupdateWins.createWindow("holidayupdatew1",  location.x, location.y,   dhtmlxwindowSize.width, dhtmlxwindowSize.height);
               holidayupdatew1.setText("휴일 수정");
               holidayupdateWins.window('holidayupdatew1').setModal(true);
               holidayupdatew1.attachURL("<c:url value="/holiday/holidayMasterUpdate" />"+"?yyyymmdd="+holiday_seq, true);
            }
               
            function holidaySearch() {                
                $.ajax({
                    url: $("#holidayForm").attr("action"),
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#holidayForm").serialize(),
                    success: function (data) {
                        holidaygrid.clearAll();
                        holidaygrid.parse(jQuery.parseJSON(data), "json");
                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }
              
            function PopHolidayInsert() {
                 var dhtmlxwindowSize = {};
                 dhtmlxwindowSize.width = 390;
                 dhtmlxwindowSize.height = 140;

                 var location = dhxwindowCenterAlgin(dhtmlxwindowSize);

                 holidayw1 = holidayWins.createWindow("holidayw1", location.x, location.y,   dhtmlxwindowSize.width, dhtmlxwindowSize.height);
                 holidayw1.setText("휴일등록");
                 holidayWins.window('holidayw1').setModal(true);
                 holidayw1.attachURL("<c:url value="/holiday/holidayMasterInsert" />", true); 
            }
  
        </script>
    </body>
</html>
