<%-- 
    Document   : merchantMaterList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>회원 정보 관리 수정</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">
                     var holidayUpdateWins;
            
                 $(document).ready(function () {
                
                    holidayUpdateWins = new dhtmlXWindows();
                    
                    //회원 정보 수정 이벤트
                    $("#holidayFormUpdate input[name=holiday_update]").click(function(){
                        holidayUpdate(); 
                    }); 
                    
                    //회원정보 삭제 이벤트
                    $("#holidayFormUpdate input[name=holiday_delete]").click(function(){
                        holiday_Delete(); 
                    }); 
                    
                    //회원 정보 수정 닫기
                    $("#holidayFormUpdate input[name=holiday_close]").click(function(){
                        holidayupdateWins.window('holidayupdatew1').close();
                    });     
                    
                    
                });
                
                var dhxSelPopupWins=new dhtmlXWindows();
                

                //회원수정
                function holidayUpdate(){
                    
                    $.ajax({
                        url : $("#holidayFormUpdate").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#holidayFormUpdate").serialize(),
                        success : function(data) {
                            //alert(data);
                            if(data=='1')
                            {
                                alert("수정 완료");
                                holidaySearch();
                                holidayupdateWins.window('holidayupdatew1').close();
                            }
                            else
                            {
                                alert('수정실패');
                            }
                        },
                        error : function() { 
                                alert("수정 실패");
                        }
                    });	                         

                }
                
                //회원 삭제
                function holiday_Delete(){
                    
                    $("#holidayFormUpdate").attr("action","<c:url value="/holiday/holidayMasterDelete" />");
                    
                    $.ajax({
                        url : $("#holidayFormUpdate").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#holidayFormUpdate").serialize(),
                        success : function(data) {
                            alert("삭제 완료");
                            holidaySearch();
                            holidayupdateWins.window('holidayupdatew1').close();
                        },
                        error : function() { 
                                alert("삭제 실패");
                        }
                    });	                                
        
                } 
            </script>

            <form id="holidayFormUpdate" method="POST" action="<c:url value="/holiday/holidayMasterUpdate" />" modelAttribute="holidayFormBean">
                  <input type = "hidden" name = "result_colum" value = "holidayFormUpdate">
                <table class="gridtable">
                    <tr>
                        <th>날짜</th>
                        <td>
                            ${tb_Holiday.yyyymmdd}
                            <input type="hidden" name="yyyymmdd" id="yyyymmdd" value ="${tb_Holiday.yyyymmdd}"  /> 
                            <!--<input type="text" name="holiday_date" id="holiday_date" value ="${tb_Holiday.yyyymmdd}"  />--> 
                        </td>
                        <th>휴일명</th>
                        <td><input name="holidayname" value = "${tb_Holiday.holidayname}" maxlength="20"/></td>
                        <input style="VISIBILITY: hidden; WIDTH: 0px;height: 0px;overflow: hidden">
                    </tr> 
                    <tr>
                        <td style="text-align: center" colspan="4" >
                            <input type="button" name="holiday_update" value="수정"/>
                            <input type="button" name="holiday_delete" value="삭제"/>
                            <input type="button" name="holiday_close" value="닫기"/>
                        </td>
                    </tr>
                </table>
            </form>

<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

