<%-- 
    Document   : bbsMasterList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>공지사항</title>             
        </head>
        <body>
</c:if>
       
    <body>
        <div class="right" id="bbs_search" style="width:100%;">       
            <form id="bbsForm" method="POST" onsubmit="return false" action="<c:url value="/bbs/bbsMasterList" />" modelAttribute="bbsFormBean"> 
                <input type="hidden" name="page_size" value="100" >
                <input type="hidden" name="page_no" value="1" >
                <table>
                    <tr>
                        <div class="label">등록일</div>
                        <div class="input">
                            <input type="text" name="ins_start_dt" id="bbsForm_from_date" value ="${bbsFormBean.ins_start_dt}" onclick="setSens('bbsForm_to_date', 'max');" /> ~
                            <input type="text" name="ins_end_dt" id="bbsForm_to_date" value="${bbsFormBean.ins_end_dt}" onclick="setSens('bbsForm_from_date', 'min');" />
                        </div>
                        <div class="label">제목</div>
                        <div class="input">
                            <input type="text" name="title" value ="${bbsFormBean.title}" />
                        </div>                 
                        <div class="label">내용</div>
                        <div class="input">
                            <input type="text" name="content" value ="${bbsFormBean.content}" />
                        </div>                        
                        <td><input type="button" name="bbs_search" value="조회"/></td>
                        <td><input type="button" name="init" value="검색조건삭제"/></td>
                    </tr>
                    <c:if test="${bbsFormBean.ses_user_cate == '00'}">
                        <div class="action">
                            <button class="addButton">추가</button>
                        </div>                                                                 
                    </c:if>                      
               </table>
            </form>
            <div class="paging">
                <div id="bbsPaging" style="width: 50%;"></div>
                <div id="bbsrecinfoArea" style="width: 50%;"></div>
            </div>                        
        </div>
        
         <script type="text/javascript">
            
            var bbsgrid={};
            var bbsCalendar;
            var bbsMasterWindow;
            
            $(document).ready(function () {
                
                bbsCalendar = new dhtmlXCalendarObject(["bbsForm_from_date","bbsForm_to_date"]);    
                
                var bbs_searchForm = document.getElementById("bbs_search");
                var tabBarId = tabbar.getActiveTab();
                bbs_layout = tabbar.cells(tabBarId).attachLayout("2E");                
                bbs_layout.cells('a').hideHeader();
                bbs_layout.cells('b').hideHeader();
                bbs_layout.cells('a').attachObject(bbs_searchForm);
                bbs_layout.cells('a').setHeight(55);
                bbsgrid = bbs_layout.cells('b').attachGrid();
                bbsgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                
                var bbsheaders = "";
                bbsheaders += "<center>NO</center>,<center>제목</center>,<center>등록일</center>,<center>팝업여부</center>,<center>팝업시작일자</center>,<center>팝업종료일자</center>";
                bbsgrid.setHeader(bbsheaders);
                bbsgrid.setColAlign("center,left,center,center,center,center");
                bbsgrid.setColTypes("txt,txt,txt,txt,txt,txt");
                bbsgrid.setInitWidths("50,700,100,100,100,100");
                bbsgrid.setColSorting("str,str,str,str,str,str");
                bbsgrid.enableColumnMove(true);
                bbsgrid.setSkin("dhx_skyblue");

                bbsgrid.enablePaging(true,Number($("#bbsForm input[name=page_size]").val()),10,"bbsPaging",true,"bbsrecinfoArea");
                bbsgrid.setPagingSkin("bricks");    
                
                bbsgrid.attachEvent("onPageChanged", function(ind,fInd,lInd){

                       if(lInd !==0){
                           $("#bbsForm input[name=page_no]").val(ind);             
                           bbsgrid.clearAll();
                           bbsSearch();
                        }else{
                            $("#bbsForm input[name=page_no]").val(1);
                            bbsgrid.clearAll();
                            bbsSearch();
                        }                           
                    
                });                    

                bbsgrid.init();
                bbsgrid.parse(${ls_bbsMasterList},"json");

                bbsgrid.attachEvent("onRowDblClicked", bbs_attach);

                //공지사항 검색 이벤트
                $("#bbsForm input[name=bbs_search]").click(function () {
                    
                    bbsgrid.changePage(1);
                    
                });
                
                 //검색조건 초기화
                $("#bbsForm input[name=init]").click(function () {

                    bbsInit($("#bbsForm"));

                });
                
                //공지사항 등록
                $("#bbsForm .addButton").button({
                    icons: {
                        primary: "ui-icon-plusthick"
                    },
                    text: false
                })
                .unbind("click")
                .bind("click",function(e){
                    //공지사항 등록 이벤트
                    doInsertBbs();
                    return false;
                });              
                
                bbsMasterWindow = new dhtmlXWindows();
                  
            });

            //공지사항 상세 조회 이벤트
            function bbs_attach(rowid, col) {

                w1 = bbsMasterWindow.createWindow("w1", 25, 25, 700, 280);
                w1.setText("공지사항 수정");
                bbsMasterWindow.window('w1').setModal(true);
                w1.attachURL("<c:url value="/bbs/bbsMasterUpdate" />" + "?bbs_seq=" + rowid, true);   

                return false;
                        
            }
            
            //공지사항 조회
            function bbsSearch() {
                
                $.ajax({
                    url: $("#bbsForm").attr("action"),
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#bbsForm").serialize(),
                    success: function (data) {

                        bbsgrid.clearAll();
                        bbsgrid.parse($.parseJSON(data), "json");

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }
            
            function bbsInit($form) {
                    searchFormInit($form);               
            } 
            
            
            function setSens(id, k) {
                // update range
                if (k == "min") {
                    bbsCalendar.setSensitiveRange(byId(id).value, null);
                } else {
                    bbsCalendar.setSensitiveRange(null, byId(id).value);
                }
            }

            function byId(id) {
                return document.getElementById(id);
            }        

            //공지사항 등록
            function doInsertBbs(){

                w1 = bbsMasterWindow.createWindow("w1", 25, 25, 700, 280);
                w1.setText("공지사항 등록");
                bbsMasterWindow.window('w1').setModal(true);
                w1.attachURL("<c:url value="/bbs/bbsMasterInsert" />", true);   

            }         
                
        </script>
    </body>
</html>
