<%-- 
    Document   : bbsMasterList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <jsp:include page="/WEB-INF/view/common/basic_common.jsp"/>
    <%@include file="/WEB-INF/view/common/basic_comm_header.jsp"%>
    <table width="100%" height="42" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top" class="titleBg">
		<table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="title"><b>-공지사항</b></td>
                        <td><img src="../resources/images/title_sright.gif" width="12" height="38" /></td>
                    </tr>
		</table>
            </td>
	</tr>
    </table>
        <div class="right" id="bbs_search" style="width:100%;">       
            <form id="bbsForm" name="bbsForm" method="POST" onsubmit="return false" action="<c:url value="/bbs/basic_bbsMasterList" />" modelAttribute="bbsFormBean"> 
                <input type="hidden" name="page_size" id="page_size" value="10" >
                <input type="hidden" name="page_no" id="page_no" value="${bbsFormBean.page_no}" >
                <input type="hidden" name="total_cnt" id="total_cnt" value="${total_count}" >
                <table>
                    <tr>
                        <div class="label">등록일</div>
                        <div class="input">
                            <input type="text" name="ins_start_dt" id="bbsForm_from_date" value ="${bbsFormBean.ins_start_dt}" onclick="setSens('bbsForm_to_date', 'max');" /> ~
                            <input type="text" name="ins_end_dt" id="bbsForm_to_date" value="${bbsFormBean.ins_end_dt}" onclick="setSens('bbsForm_from_date', 'min');" />
                        </div>
                        <div class="label">제목</div>
                        <div class="input">
                            <input type="text" name="title" value ="${bbsFormBean.title}" />
                        </div>                 
                        <div class="label">내용</div>
                        <div class="input">
                            <input type="text" name="content" value ="${bbsFormBean.content}" />
                        </div>                        
                        <td><input type="button" name="bbs_search" value="조회" onclick="javascript:bbsSearch();"/></td>
                    </tr>
               </table>
            </form><br>
                <div class="CSSTableGenerator" >
                <table>
                    <tr>
                        <td width="20px;" height="40px;"  style="text-align:center">
                            NO
                        </td>
                        <td >
                            제목
                        </td>
                        <td width="80px;">
                            등록일
                        </td>
                    </tr>
 <c:forEach var="bbstblist" items="${ls_bbsMasterList}" begin="0">              
                    <tr>
                        <td width="30px;" height="40px;" style="text-align:center">
                            ${bbstblist.rnum} 
                        </td>
                        <td style="text-align:left">
                            <a href="#" onclick="javascirpt:bbs_attach('${bbstblist.bbs_seq}');">${bbstblist.title}</a> 
                        </td>
                        <td width="80px;" align="center" style="text-align:center">
                            ${bbstblist.ins_dt}
                        </td>
                    </tr>
</c:forEach>
                </table>
            </div>       
            <div id="paging" name="paging" style="margin: 15px;"></div>
        </div>
<%@include file="/WEB-INF/view/common/basic_comm_tail.jsp"%>
        <script type="text/javascript">
            
            var bbsgrid={};
            var bbsCalendar;
            var bbsMasterWindow;
            
            $(document).ready(function () {
                
                bbsMasterWindow = new dhtmlXWindows();
                bbsCalendar = new dhtmlXCalendarObject(["bbsForm_from_date","bbsForm_to_date"]);   
                
                makepage(${bbsFormBean.page_no});
            });

            //공지사항 상세 조회 이벤트
            function bbs_attach(bbs_seq) {

                w1 = bbsMasterWindow.createWindow("w1", 25, 25, 700, 280);
                w1.setText("공지사항 수정");
                bbsMasterWindow.window('w1').setModal(true);
                w1.attachURL("<c:url value="/bbs/bbsMasterUpdate" />" + "?bbs_seq=" + bbs_seq, true);   

                return false;
                        
            }
            
            //공지사항 조회
            function bbsSearch() {
                 $("#page_no").val("1");
                document.bbsForm.submit();
            }
            
            function setSens(id, k) {
                // update range
                if (k == "min") {
                    bbsCalendar.setSensitiveRange(byId(id).value, null);
                } else {
                    bbsCalendar.setSensitiveRange(null, byId(id).value);
                }
            }

            function byId(id) {
                return document.getElementById(id);
            }        

            
            function makepage(startIndex) 
            {
     			
                var pagingHTML 		= "";
		var page 		= parseInt($("#page_no").val());
		var totalCount		= parseInt($("#total_cnt").val());
		var pageBlock		= parseInt($("#page_size").val());
		var navigatorNum    = 10;
		var firstPageNum	= 1;
		var lastPageNum		= Math.floor((totalCount-1)/pageBlock) + 1;
		var previewPageNum  = page == 1 ? 1 : page-1;
		var nextPageNum		= page == lastPageNum ? lastPageNum : page+1;
		var indexNum		= startIndex <= navigatorNum  ? 0 : parseInt((startIndex-1)/navigatorNum) * navigatorNum;
				
		if (totalCount > 1) 
                {
					
                    if (startIndex > 1) 
                    {
                        pagingHTML += "<a href='#' id='"+firstPageNum+"'><img src='../resources/images/btn_first.gif' width='11' height='11'/></a> ";
			pagingHTML += "<a href='#' id='"+previewPageNum+"'><img src='../resources/images/btn_prev.gif' width='11' height='11'/></a>";
                    }
		
                    for (var i=1; i<=navigatorNum; i++) 
                    {
			var pageNum = i + indexNum;
					
			if (pageNum == startIndex) 
                            pagingHTML += "<a href='#' id='"+pageNum+"'>"+pageNum+"</a> ";
			else 
                            pagingHTML += "<a href='#' id='"+pageNum+"'>"+pageNum+"</a>  ";
					
			if (pageNum==lastPageNum)
			break;
		    }
					
		    if (startIndex < lastPageNum) {
			pagingHTML += "<a href='#' id='"+nextPageNum+"'><img src='../resources/images/btn_next.gif' width='11' height='11'/></a> ";
			pagingHTML += "<a href='#' id='"+lastPageNum+"'><img src='../resources/images/btn_end.gif' width='11' height='11'/></a>";
		    }
					
		}
				
				
		$("#paging").html(pagingHTML);
		
		$("#paging a").click(function (e) {
			paging_move($(this).attr('id'));
		});

	}        
        
        
        function paging_move(pagenum)
        {
            //alert(pagenum);
            $("#page_no").val(pagenum);
             document.bbsForm.submit();
        }
                
        </script>