<%-- 
    Document   : bbsMasterUpdate
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>공지사항 수정</title>
        </head>
        <body>
</c:if>
                
            <script type="text/javascript">
                
                var bbsCalendar;
                
                $(document).ready(function () {

                    //공지사항 수정 이벤트
                    $("#bbsMasterFormUpdate input[name=update]").click(function(){
                       doUpdateBbsMaster(); 
                    });                                        
                    
                    //공지사항 수정 닫기
                    $("#bbsMasterFormUpdate input[name=close]").click(function(){
                        bbsMasterWindow.window("w1").close();
                    });          
                    
                    bbsCalendar = new dhtmlXCalendarObject(["bbsMasterFormUpdate_popup_start_dt","bbsMasterFormUpdate_popup_end_dt"]);
                    
                    $("#bbsMasterFormUpdate select[name=popup_chk]").change(function(){
                        popup_chk = $(this).val();
                        if(popup_chk === '1'){
                            $("#bbsMasterFormUpdate input[name=popup_start_dt]").removeAttr("disabled");
                            $("#bbsMasterFormUpdate input[name=popup_end_dt]").removeAttr("disabled");
                        }else{
                            $("#bbsMasterFormUpdate input[name=popup_start_dt]").attr("disabled","disabled");
                            $("#bbsMasterFormUpdate input[name=popup_end_dt]").attr("disabled","disabled");
                            $("#bbsMasterFormUpdate input[name=popup_start_dt]").attr("value","");
                            $("#bbsMasterFormUpdate input[name=popup_end_dt]").attr("value","");
                        }
                    });
                    
                    if($("#bbsMasterFormUpdate select[name=popup_chk]").val() === '1'){
                        $("#bbsMasterFormUpdate input[name=popup_start_dt]").removeAttr("disabled");
                        $("#bbsMasterFormUpdate input[name=popup_end_dt]").removeAttr("disabled");
                    }else{
                        $("#bbsMasterFormUpdate input[name=popup_start_dt]").attr("disabled","disabled");
                        $("#bbsMasterFormUpdate input[name=popup_end_dt]").attr("disabled","disabled");
                        $("#bbsMasterFormUpdate input[name=popup_start_dt]").attr("value","");
                        $("#bbsMasterFormUpdate input[name=popup_end_dt]").attr("value","");
                    }
                    
                });
                
                function setSens(id, k) {
                    // update range
                    if (k == "min") {
                        bbsCalendar.setSensitiveRange(byId(id).value, null);
                    } else {
                        bbsCalendar.setSensitiveRange(null, byId(id).value);
                    }
                }

                function byId(id) {
                    return document.getElementById(id);
                }                

                //공지사항 수정
                function doUpdateBbsMaster(){
                    
                    if($("#bbsMasterFormUpdate input[name=title]").val().trim() === ''){
                        alert("제목을 입력하십시오.");
                        return false;
                    }

                    if($("#bbsMasterFormUpdate textarea[name=content]").val().trim() === ''){
                        alert("내용을 입력하십시오.");
                        return false;
                    }        
                    
                    if($("#bbsMasterFormUpdate select[name=popup_chk]").val() === ''){
                        alert("팝업여부 선택하십시오.");
                        return false;
                    }
                    
                    if($("#bbsMasterFormUpdate select[name=popup_chk]").val() === '1'){
                        if($("#bbsMasterFormUpdate input[name=popup_start_dt]").val().trim() === ''){
                            alert("팝업시작일자를 선택하십시오.");
                            return false;
                        }

                        if($("#bbsMasterFormUpdate input[name=popup_end_dt]").val().trim() === ''){
                            alert("팝업종료일자를 선택하십시오.");
                            return false;
                        }                           
                    }
                    
                    if(!window.confirm("수정하시겠습니까?")){
                        return false;
                    }
                    
                    $("#bbsMasterFormUpdate input[name=popup_start_dt]").removeAttr("disabled");
                    $("#bbsMasterFormUpdate input[name=popup_end_dt]").removeAttr("disabled");

                    $.ajax({
                        url : $("#bbsMasterFormUpdate").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#bbsMasterFormUpdate").serialize(),
                        success : function(data) {
                            
                            alert("수정 완료");
                            bbsSearch();
                            bbsMasterWindow.window("w1").close();
                            
                        },
                        error : function() { 
                                alert("수정 실패");
                        }
                    });	      
                
                }                                

            </script>
            
            <form id="bbsMasterFormUpdate" method="POST" action="<c:url value="/bbs/bbsMasterUpdate" />" modelAttribute="bbsFormBean">
                <input type="hidden" name="bbs_seq" value="${ls_bbsMasterList[0].bbs_seq}" />
                <table class="gridtable" height="20%" width="100%">
                    <tr>
                        <td align='left' colspan='4'>공지사항</td>                        
                    </tr>                    
                    <tr>
                        <td class="headcol">제목</td>
                        <td colspan="3"><input name="title" value="${ls_bbsMasterList[0].title}" onblur="javascript:checkLength(this,250);" style="width:90%;" /></td>
                    </tr>                          
                    <tr>
                        <td class="headcol">내용</td>
                        <td colspan="3">
                            <textarea name="content" cols="70" onblur="javascript:checkLength(this,1000);">${ls_bbsMasterList[0].content}</textarea>
                        </td>
                    </tr> 
                    <tr>
                        <td class="headcol">팝업여부</td>
                        <td colspan="3">
                            <select name="popup_chk">
                                <option value="">선택하세요</option>
                                <c:forEach var="code" items="${popupChkList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${ls_bbsMasterList[0].popup_chk == code.detail_code}">selected</c:if> >${code.code_nm}</option>
                                </c:forEach>
                            </select>                            
                        </td>
                    </tr>                     
                    <tr>
                        <td class="headcol">팝업시작일자</td>
                        <td><input name="popup_start_dt" id="bbsMasterFormUpdate_popup_start_dt" value="${ls_bbsMasterList[0].popup_start_dt}" onclick="setSens('bbsMasterFormUpdate_popup_end_dt', 'max');" disabled="disabled" /></td>
                        <td class="headcol">팝업종료일자</td>
                        <td><input name="popup_end_dt" id="bbsMasterFormUpdate_popup_end_dt" value="${ls_bbsMasterList[0].popup_end_dt}" onclick="setSens('bbsMasterFormUpdate_popup_start_dt', 'min');" disabled="disabled" /></td>                        
                    </tr>
                    <tr>
                        <td colspan="4">
                            <c:if test="${bbsFormBean.ses_user_cate == '00'}">
                                <input type="button" name="update" value="수정"/>
                            </c:if>
                            <input type="button" name="close" value="닫기"/>
                        </td>
                    </tr>
                </table>
                
            </form>            
                

<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

