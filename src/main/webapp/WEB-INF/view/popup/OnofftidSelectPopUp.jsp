<%-- 
    Document   : MidSelectPopUp.jsp
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title> 온오프 UID 선택창</title>
        </head>
        <body>
</c:if>
            <script type="text/javascript">

                var popuponofftid_mygrid;
                var popuponofftid_layout;
                
                $(document).ready(function () {
                    popuponofftid_layout = new dhtmlXLayoutObject("poponofftidVP", "1C", "dhx_skyblue");
                    popuponofftid_layout.cells('a').hideHeader();
                    popuponofftid_mygrid = popuponofftid_layout.cells('a').attachGrid();
                    popuponofftid_mygrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var mheaders = "";
                    //no, 결제채널, 결제TID명,결제TID,지급ID명, 지급ID번호, 결제상품명 ,상태
                    mheaders += "<center>NO</center>,<center>결제채널</center>,<center>UID명</center>,<center>UID</center>,<center>지급ID명</center>,<center>지급ID번호</center>,<center>결제상품명</center>,<center>상태</center>";
                    popuponofftid_mygrid.setHeader(mheaders);
                    popuponofftid_mygrid.setColAlign("center,center,center,center,center,center,center,center");
                    popuponofftid_mygrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,txt");
                    popuponofftid_mygrid.setInitWidths("50,120,150,150,150,120,120,100");
                    popuponofftid_mygrid.setColSorting("str,str,str,str,str,str,str,strr");
                    popuponofftid_mygrid.enableColumnMove(true);
                    popuponofftid_mygrid.setSkin("dhx_skyblue");
                    
                    
                    popuponofftid_mygrid.enablePaging(true,Number($("#popuponfftidinfoSearch input[name=page_size]").val()),10,"popuponfftidinfoMasterListPaging",true,"popuponfftidinfoMasterListrecinfoArea");
                    popuponofftid_mygrid.setPagingSkin("bricks");

                    //페이징 처리
                    popuponofftid_mygrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                       // alert(ind+'|' + fInd + '|' + lInd);
                       /*
                      if(lInd !==0){
                            $("#popuponfftidinfoForm input[name=page_no]").val(ind);                            
                            doPopupSearchTidMaster();
                        }else{
                            return false;
                        }
                        */
                        
                        if(lInd !==0){
                           $("#popuponfftidinfoForm input[name=page_no]").val(ind);             
                           popuponofftid_mygrid.clearAll();
                           doPopupSearchTidMaster();
                        }else{
                            $("#popuponfftidinfoForm input[name=page_no]").val(1);
                            popuponofftid_mygrid.clearAll();
                             doPopupSearchTidMaster();
                        }                                
                        
                    });
                    
                    popuponofftid_mygrid.init();
                    popuponofftid_mygrid.parse(${onofftidPopupList}, "json");
                    popuponofftid_mygrid.attachEvent("onRowSelect", function(rowId,cellIndex){
                    selectOnofftidRow(rowId,cellIndex);
                    });
                               
                    
                    
                     //버튼 추가
                    $("#popuponfftidinfoForm input[name=popuponfftidinfo_searchButton]").bind("click",function(e){
                        //가맹점정보 정보 조회 이벤트
                        /*
                        $("#popuponfftidinfoForm input[name=page_no]").val("1");
                        popuponofftid_mygrid.clearAll();
                        doPopupSearchTidMaster();
                        */
                       popuponofftid_mygrid.changePage(1);
                        return false;
                    });
                    
                });
                
                //가맹점정보 정보 조회
                function doPopupSearchTidMaster(){
                    $.ajax({
                        url : $("#popuponfftidinfoForm").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#popuponfftidinfoForm").serialize(),
                        success : function(data) {
                            var jsonData = $.parseJSON(data);     
                            popuponofftid_mygrid.parse(jsonData,"json");
                            
                        },
                        error : function() {
                            alert("조회 실패");
                        }
                    });
                }                
                
                function selectOnofftidRow(rowId,cellIndex)
                {
                    //p_onfftid, p_onfftid_nm
                    //alert(popuponofftid_mygrid.cells(rowId,3).getValue());
                    //alert( popuponofftid_mygrid.cells(rowId,2).getValue() );
                    SelOnofftidInfoInput(
                                popuponofftid_mygrid.cells(rowId,5).getValue()
                              , popuponofftid_mygrid.cells(rowId,3).getValue()
                              , popuponofftid_mygrid.cells(rowId,2).getValue()                              
                            );
                    
                    dhxSelPopupWins.window("OnofftidSelectPopUp").close();
                }
                
            </script>
            <div class="searchForm1row" id="popuponfftidinfoSearch" style="width:100%;background-color:white;">
                <form id="popuponfftidinfoForm" method="POST" action="<c:url value="/popup/OnofftidSelectPopUp" />" modelAttribute="TerminalFormBean">                    
                       <input type="hidden" name="page_size" value="100" />
                       <input type="hidden" name="page_no" value="1" />
                        <div class="label2">UID명</div>
                        <div class="input"><input type="text" name="onfftid_nm" maxlength="30"/></div>                        
                        <input type="button" name="popuponfftidinfo_searchButton" value="조회"/>
                        <input style="VISIBILITY: hidden; WIDTH: 0px;height: 0px;overflow: hidden">
                </form>                
            </div>           
            <div id="poponofftidVP" style="position:relative;width:100%;height:80%;background-color:white;"></div>
<div id="popuponfftidinfoMasterListPaging" style="width: 100%;"></div>
                <div id="popuponfftidinfoMasterListrecinfoArea" style="width: 100%;"></div>                                        
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

