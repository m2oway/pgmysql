<%-- 
    Document   : PayMtdSelectPopUp.jsp
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title> 온오프 결제상품 선택창</title>
        </head>
        <body>
</c:if>
            <script type="text/javascript">

                var popuppaymtd_mygrid;
                var popuppaymtd_layout;
                
                $(document).ready(function () {
                    popuppaymtd_layout = new dhtmlXLayoutObject("popuppaymtdVP", "1C", "dhx_skyblue");
                    popuppaymtd_layout.cells('a').hideHeader();
                    popuppaymtd_mygrid = popuppaymtd_layout.cells('a').attachGrid();
                    popuppaymtd_mygrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");

                    var mheaders = "";
                    mheaders += "<center>NO</center>,<center>결제상품명</center>,<center>시작일</center>,<center>종료일</center>,<center>사용여부</center>";
                    popuppaymtd_mygrid.setHeader(mheaders);
                    popuppaymtd_mygrid.setColAlign("center,center,center,center,center");
                    popuppaymtd_mygrid.setColTypes("txt,txt,txt,txt,txt");
                    popuppaymtd_mygrid.setInitWidths("50,200,100,100,100");
                    popuppaymtd_mygrid.setColSorting("str,str,str,str,str");
                    popuppaymtd_mygrid.enableColumnMove(true);
                    popuppaymtd_mygrid.setSkin("dhx_skyblue");
                    
                    popuppaymtd_mygrid.init();
                    popuppaymtd_mygrid.parse(${ls_paymtdPopupList}, "json");
                    popuppaymtd_mygrid.attachEvent("onRowSelect", function(rowId,cellIndex){
                    selectPaymtdRow(rowId,cellIndex);
                    });
                   
                });
                
                function selectPaymtdRow(rowId,cellIndex)
                {
                    //p_paymtd_seq, p_paymtd_nm
                    SelPaymtdInfoInput(
                                rowId
                              , popuppaymtd_mygrid.cells(rowId,1).getValue()                              
                            );
                    dhxSelPopupWins.window("PaymtdSelectPopUp").close();
                }
                
            </script>
            <div id="popuppaymtdVP" style="width:100%;height:100%;background-color:white;"></div>
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

