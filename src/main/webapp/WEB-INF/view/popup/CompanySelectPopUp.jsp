<%-- 
    Document   : MidSelectPopUp.jsp
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title> 사업자정보 선택창</title>
        </head>
        <body>
</c:if>
            <script type="text/javascript">
                var popupcompany_mygrid;
                var popupcompany_layout;
  
                $(document).ready(function () {
                    popupcompany_layout = new dhtmlXLayoutObject("popupcompanyVP", "1C", "dhx_skyblue");
                    popupcompany_layout.cells('a').hideHeader();
                    popupcompany_mygrid = popupcompany_layout.cells('a').attachGrid();
                    popupcompany_mygrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var mheaders = "";
                    mheaders += "<center>NO</center>,<center>사업자명</center>,<center>개인/법인</center>,<center>사업자번호</center>,<center>법인번호</center>,<center>대표자</center>,<center>대표번호</center>,<center>FAX</center>,<center>우편번호</center>,<center>주소1</center>,<center>주소2</center>,<center>업태</center>,<center>업종</center>,<center>과세구분</center>,<center>사용여부코드</center>,<center>사용여부</center>";
                    popupcompany_mygrid.setHeader(mheaders);
                    popupcompany_mygrid.setColAlign("center,center,center,center,center,center,center,center,center,left,left,center,center,center,center,center");
                    popupcompany_mygrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt");
                    popupcompany_mygrid.setInitWidths("70,150,80,100,100,100,100,100,100,200,200,100,100,100,100,100");
                    popupcompany_mygrid.setColSorting("str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str");
                    popupcompany_mygrid.enableColumnMove(true);
                    popupcompany_mygrid.setSkin("dhx_skyblue");
                    popupcompany_mygrid.enablePaging(true,Number($("#popupcompanyForm input[name=page_size]").val()),10,"popupcompanyMasterListPaging",true,"popupcompanyMasterListrecinfoArea");
                    popupcompany_mygrid.setPagingSkin("bricks");

                    //페이징 처리
                    popupcompany_mygrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                       // alert(ind+'|' + fInd + '|' + lInd);
                       /*
                      if(lInd !==0){
                            $("#popupcompanyForm input[name=page_no]").val(ind);                            
                            doPopupSearchCompanyMaster();
                        }else{
                            return false;
                        }
                        */
                        
                        if(lInd !==0){
                           $("#popupcompanyForm input[name=page_no]").val(ind);             
                           popupcompany_mygrid.clearAll();
                           doPopupSearchCompanyMaster();
                        }else{
                            $("#popupcompanyForm input[name=page_no]").val(1);
                            popupcompany_mygrid.clearAll();
                            doPopupSearchCompanyMaster();
                        }                            
                    });
              
                    popupcompany_mygrid.init();
                    popupcompany_mygrid.parse(${companyPopupList}, "json");
                    
                    popupcompany_mygrid.attachEvent("onRowSelect", function(rowId,cellIndex){
                    selectCompanyRow(rowId,cellIndex);
                    });
                    
                    popupcompany_mygrid.setColumnHidden(14,true); 
                    
                    
                     //버튼 추가
                    $("#popupcompanyForm input[name=popupcompany_searchButton]").bind("click",function(e){
                        //가맹점정보 정보 조회 이벤트
                        /*
                        $("#popupcompanyForm input[name=page_no]").val("1");
                        popupcompany_mygrid.clearAll();
                        doPopupSearchCompanyMaster();
                        */
                       popupcompany_mygrid.changePage(1);
                        return false;
                    });
                                        
                    
                });
                
                function selectCompanyRow(rowId,cellIndex)
                {
                    
                    ////NO,사업자명,개인/법인,사업자번호,법인번호,대표자,연락처1,연락처2,우편번호,주소1,주소2,업태,업종,과세여부,사용여부,사용여부코드
                    //  0     1        2         3      4      5       6        7       8      9    10    11   12     13        14       15
                    //SelCompaynyInfoInput(p_comp_seq,p_comp_nm,p_comp_cate_nm,p_biz_no,p_corp_no,p_comp_ceo_nm,p_comp_tel1,p_comp_tel2,p_zip_cd,p_addr_1,p_addr_2,p_biz_cate,p_biz_type, p_tax_flag)
                    
//                    alert("popupcompany_mygrid : "+ popupcompany_mygrid.cells(rowId));

//                    alert("popupcompany_mygrid : "+ popupcompany_mygrid.getUserObject(rowId).get("comp_nm"));
                    
//                    alert("getRowAttribute : "+ popupcompany_mygrid.getUserData(rowId,"comp_nm"));
                    SelCompaynyInfoInput(popupcompany_mygrid.getUserObject(rowId));
//                                            rowId//p_comp_seq
//                                            popupcompany_mygrid.getUserObject(rowId).get("comp_seq")
//                                            ,popupcompany_mygrid.getUserObject(rowId).get("comp_nm")
//                                            ,popupcompany_mygrid.getUserObject(rowId).get("comp_cate_nm")
//                                            ,popupcompany_mygrid.getUserObject(rowId).get("biz_no")
//                                            ,popupcompany_mygrid.getUserObject(rowId).get("corp_no")
//                                            ,popupcompany_mygrid.getUserObject(rowId).get("comp_ceo_nm")
//                                            ,popupcompany_mygrid.getUserObject(rowId).get("comp_tel1")
//                                            ,popupcompany_mygrid.getUserObject(rowId).get("comp_tel2")
//                                            ,popupcompany_mygrid.getUserObject(rowId).get("zip_cd")
//                                            ,popupcompany_mygrid.getUserObject(rowId).get("addr_1")
//                                            ,popupcompany_mygrid.getUserObject(rowId).get("addr_2")
//                                            ,popupcompany_mygrid.getUserObject(rowId).get("biz_cate")
//                                            ,popupcompany_mygrid.getUserObject(rowId).get("biz_type")
//                                            ,popupcompany_mygrid.getUserObject(rowId).get("tax_flag")
                                            
//                                            ,popupcompany_mygrid.cells(rowId,1).getValue()//p_comp_nm
//                                            ,popupcompany_mygrid.cells(rowId,2).getValue()//p_comp_cate_nm
//                                            ,popupcompany_mygrid.cells(rowId,3).getValue()//p_biz_no
//                                            ,popupcompany_mygrid.cells(rowId,4).getValue()//p_corp_no
//                                            ,popupcompany_mygrid.cells(rowId,5).getValue()//p_comp_ceo_nm
//                                            ,popupcompany_mygrid.cells(rowId,6).getValue()//p_comp_tel1
//                                            ,popupcompany_mygrid.cells(rowId,7).getValue()//p_comp_tel2
//                                            ,popupcompany_mygrid.cells(rowId,8).getValue()//p_zip_cd
//                                            ,popupcompany_mygrid.cells(rowId,9).getValue()//p_addr_1
//                                            ,popupcompany_mygrid.cells(rowId,10).getValue()//p_addr_2
//                                            ,popupcompany_mygrid.cells(rowId,11).getValue()//p_biz_cate
//                                            ,popupcompany_mygrid.cells(rowId,12).getValue()//p_biz_type
//                                            ,popupcompany_mygrid.cells(rowId,13).getValue()//p_tax_flag
//                                        );
                    dhxSelPopupWins.window("companySelPopupList").close();
                }
                
                
                //사업체 정보 조회
                function doPopupSearchCompanyMaster(){
                    $.ajax({
                        url : $("#popupcompanyForm").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#popupcompanyForm").serialize(),
                        success : function(data) {
                            //companyMasterGrid.clearAll();
                            //companyMasterGrid.parse(jQuery.parseJSON(data), "json");
                            var jsonData = $.parseJSON(data);     
                            popupcompany_mygrid.parse(jsonData,"json");
                            //resizeOption($("#companyList.gridbox"));
                        },
                        error : function() {
                            alert("조회 실패");
                        }
                    });
                }                
            </script>
            <div class="searchForm1row" id="popupcompanySearch" style="width:100%;background-color:white;">
                <form id="popupcompanyForm" method="POST" action="<c:url value="/popup/CompanySelectPopUp" />" modelAttribute="CompanyFormBean">
                       <input type="hidden" name="page_size" value="100" />
                       <input type="hidden" name="page_no" value="1" />
                        
                        <div class="label">사업자명</div>
                        <div class="input"><input type="text" name="comp_nm" maxlength="30"/></div>
                        <div class="label">사업자번호</div>
                        <div class="input"><input type="text" name="biz_no" maxlength="10"/></div>
                        <input type="button" name="popupcompany_searchButton" value="조회"/>
                </form>
            </div>
            <div id="popupcompanyVP" style="position:relative;width:100%;height:80%;background-color:white;"></div>
            
            <div id="popupcompanyMasterListPaging" style="width: 100%;"></div>
            <div id="popupcompanyMasterListrecinfoArea" style="width: 100%;"></div>   
            
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

