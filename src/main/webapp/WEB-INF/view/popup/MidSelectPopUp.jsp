<%-- 
    Document   : MidSelectPopUp.jsp
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title> 가맹점번호 선택창</title>
        </head>
        <body>
</c:if>
            <script type="text/javascript">
                var middhxWins;
                var popupmid_mygrid;
                middhxWins = new dhtmlXWindows();
                $(document).ready(function () {
                    mid_layout = new dhtmlXLayoutObject("poptidVP", "1C", "dhx_skyblue");
                    mid_layout.cells('a').hideHeader();
                    popupmid_mygrid = mid_layout.cells('a').attachGrid();
                    popupmid_mygrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var mheaders = "";
                    mheaders += "<center>NO</center>,<center>MerchantSeq</center>,<center>구분명</center>,<center>구분</center>,<center>가맹점번호명</center>,<center>가맹점번호</center>,<center>시작일</center>,<center>종료일</center>";
                    popupmid_mygrid.setHeader(mheaders);
                    popupmid_mygrid.setColAlign("center,center,center,center,center,center,center,center");
                    popupmid_mygrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,txt");
                    popupmid_mygrid.setInitWidths("70,70,100,100,150,100,100,100");
                    popupmid_mygrid.setColSorting("str,str,str,str,str,str,str,str");
                    popupmid_mygrid.enableColumnMove(true);
                    popupmid_mygrid.setSkin("dhx_skyblue");
                    popupmid_mygrid.init();
                    popupmid_mygrid.parse(${midPopupList}, "json");
                    popupmid_mygrid.attachEvent("onRowSelect", function(rowId,cellIndex){
    selectMidRow(rowId,cellIndex);
});
                    popupmid_mygrid.setColumnHidden(1,true); 
                    popupmid_mygrid.setColumnHidden(3,true);                    
                    middhxWins.attachViewportTo("poptidVP");
                    
                });
                
                function selectMidRow(rowId,cellIndex)
                {
                    
                    //NO,   MerchantSeq,   MID구분명,     MID구분,       MID명,    MID,  시작일,  종료일
                    //0          1           2               3              4       5       6        7  
                    //p_merchant_no, p_merchant_seq, p_paymtd, p_merchant_nm, p_startdt, p_enddt
                    SelMidInfoInput(popupmid_mygrid.cells(rowId,3).getValue(), popupmid_mygrid.cells(rowId,4).getValue(), popupmid_mygrid.cells(rowId,5).getValue());       
                    dhxSelPopupWins.window("midSelPopupList").close();
                }
                
            </script>
            <div id="poptidVP" style="width:100%;height:100%;background-color:white;"></div>
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

