<%-- 
    Document   : MidSelectPopUp.jsp
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title> 온오프 대리점 선택창</title>
        </head>
        <body>
</c:if>
            <script type="text/javascript">

                var popuponagent_mygrid;
                var popuponagent_layout;
                
                $(document).ready(function () {
                    popuponagent_layout = new dhtmlXLayoutObject("poponagentVP", "1C", "dhx_skyblue");
                    popuponagent_layout.cells('a').hideHeader();
                    popuponagent_mygrid = popuponagent_layout.cells('a').attachGrid();
                    popuponagent_mygrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var mheaders = "";
                    
                    mheaders += "<center>NO</center>,<center>대리점</center>,<center>사업자번호</center>,<center>법인번호</center>,<center>대표자</center>,<center>연락처1</center>,<center>연락처2</center>,<center>상태</center>,<center>등록일</center>";
                    popuponagent_mygrid.setHeader(mheaders);
                    popuponagent_mygrid.setColAlign("center,center,center,center,center,center,center,center,center");
                    popuponagent_mygrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,txt,txt");
                    popuponagent_mygrid.setInitWidths("50,100,100,100,100,100,100,100,100");
                    popuponagent_mygrid.setColSorting("str,str,str,str,str,str,str,str,str");
                    popuponagent_mygrid.enableColumnMove(true);
                    popuponagent_mygrid.setSkin("dhx_skyblue");
                    popuponagent_mygrid.enablePaging(true,Number($("#popupagentSearch input[name=page_size]").val()),10,"popupagentMasterListPaging",true,"popupagentMasterListrecinfoArea");
                    popuponagent_mygrid.setPagingSkin("bricks");

                    //페이징 처리
                    popuponagent_mygrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        if(lInd !==0){
                            $("#popupagentForm input[name=page_no]").val(ind);             
                           popuponagent_mygrid.clearAll();
                           doPopupSearchAgentMaster();
                        }else{
                              $("#popupagentForm input[name=page_no]").val(1);
                            popuponagent_mygrid.clearAll();
                            doPopupSearchAgentMaster();
                        }                              
                    });
                                      
                    popuponagent_mygrid.init();
                    popuponagent_mygrid.parse(${ls_agentPopupList}, "json");
                    
                    popuponagent_mygrid.attachEvent("onRowSelect", function(rowId,cellIndex){
                    selectAgentRow(rowId,cellIndex);
                    });
                    
                    popuponagent_mygrid.setColumnHidden(2,true);
                    
                    
                     //버튼 추가
                    $("#popupagentForm input[name=popupagent_searchButton]").bind("click",function(e){
                        //가맹점정보 정보 조회 이벤트
                        /*
                        $("#popupagentForm input[name=page_no]").val("1");
                        popuponagent_mygrid.clearAll();
                        doPopupSearchAgentMaster();
                        */
                        popuponagent_mygrid.changePage(1);
                        return false;
                    });
                    
                });
                
                //가맹점정보 정보 조회
                function doPopupSearchAgentMaster(){
                    $.ajax({
                        url : $("#popupagentForm").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#popupagentForm").serialize(),
                        success : function(data) {
                            var jsonData = $.parseJSON(data);     
                            popuponagent_mygrid.parse(jsonData,"json");
                            
                        },
                        error : function() {
                            alert("조회 실패");
                        }
                    });
                }                
                
                function selectAgentRow(rowId,cellIndex)
                {
                  
                    //p_agent_seq, p_agent_nm
                    SelAgentInfoInput(
                                rowId
                              , popuponagent_mygrid.cells(rowId,1).getValue()                              
                            );
                    dhxSelPopupWins.window("AgentSelectPopUp").close();
                }
                
            </script>
            <div class="searchForm1row" id="popupagentSearch" style="width:100%;background-color:white;">
                <form id="popupagentForm" method="POST" action="<c:url value="/popup/AgentSelectPopUp" />" modelAttribute="AgentFormBean">
                       <input type="hidden" name="page_size" value="100" />
                       <input type="hidden" name="page_no" value="1" />
                        <div class="label">대리점</div>
                        <div class="input"><input type="text" name="agent_nm" maxlength="30"/></div>                        
                        <input type="button" name="popupagent_searchButton" value="조회"/>
                </form>                
            </div>           
            <div id="poponagentVP" style="position:relative;width:100%;height:80%;background-color:white;"></div>
<div id="popupagentMasterListPaging" style="width: 100%;"></div>
                <div id="popupagentMasterListrecinfoArea" style="width: 100%;"></div>                                        
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

