<%-- 
    Document   : MidSelectPopUp.jsp
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title> TID 선택창</title>
        </head>
        <body>
</c:if>
            <script type="text/javascript">
                var tiddhxWins;
                var popuptid_mygrid;
                tiddhxWins = new dhtmlXWindows();
                $(document).ready(function () {
                    tid_layout = new dhtmlXLayoutObject("poptidVP", "1C", "dhx_skyblue");
                    tid_layout.cells('a').hideHeader();
                    popuptid_mygrid = tid_layout.cells('a').attachGrid();
                    popuptid_mygrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var mheaders = "";
                    mheaders += "<center>NO</center>,<center>tid_Seq</center>,<center>결제사명</center>,<center>결제사</center>,<center>TID명</center>,<center>TID</center>,<center>시작일</center>,<center>종료일</center>";
                    popuptid_mygrid.setHeader(mheaders);
                    popuptid_mygrid.setColAlign("center,center,center,center,center,center,center,center");
                    popuptid_mygrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,txt");
                    popuptid_mygrid.setInitWidths("70,70,100,100,150,100,100,100");
                    popuptid_mygrid.setColSorting("str,str,str,str,str,str,str,str");
                    popuptid_mygrid.enableColumnMove(true);
                    popuptid_mygrid.setSkin("dhx_skyblue");
                    popuptid_mygrid.init();
                    popuptid_mygrid.parse(${tidPopupList}, "json");
                    popuptid_mygrid.attachEvent("onRowSelect", function(rowId,cellIndex){
                    selectTidRow(rowId,cellIndex);
                    });
                    popuptid_mygrid.setColumnHidden(1,true); 
                    popuptid_mygrid.setColumnHidden(3,true);                    
                    tiddhxWins.attachViewportTo("poptidVP");
                    
                });
                
                function selectTidRow(rowId,cellIndex)
                {
                    
                    //NO     ,tid_Seq     ,결제사명       ,결제사         ,TID명   ,TID   ,시작일   ,종료일
                    //0          1           2               3              4       5       6        7  
                    //p_tid_mtd, p_tid_mtd_nm, p_tid_nm ,p_terminal_no)
                    SelTidInfoInput(
                                popuptid_mygrid.cells(rowId,3).getValue()
                              , popuptid_mygrid.cells(rowId,2).getValue()
                              , popuptid_mygrid.cells(rowId,4).getValue()
                              , popuptid_mygrid.cells(rowId,5).getValue()
                            );       
                    dhxSelPopupWins.window("tidSelPopupList").close();
                }
                
            </script>
            <div id="poptidVP" style="width:100%;height:100%;background-color:white;"></div>
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

