<%-- 
    Document   : MidSelectPopUp.jsp
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title> 온오프 가맹점정보 선택창</title>
        </head>
        <body>
</c:if>
            <script type="text/javascript">

                var popuponoffmerch_mygrid;
                var popuponoffmerch_layout;
                
                $(document).ready(function () {
                    popuponoffmerch_layout = new dhtmlXLayoutObject("poponoffmerchVP", "1C", "dhx_skyblue");
                    popuponoffmerch_layout.cells('a').hideHeader();
                    popuponoffmerch_mygrid = popuponoffmerch_layout.cells('a').attachGrid();
                    popuponoffmerch_mygrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var mheaders = "";
                    //NO,지급ID명,지급IDID, 사업자명,개인법인구분,사업자번호, 법인번호, 대표자, 연락처1, 결제한도, 대리점, 상태
                    mheaders += "<center>NO</center>,<center>지급ID명</center>,<center>지급ID</center>,<center>사업자명</center>,<center>구분</center>,<center>사업자번호</center>,<center>법인번호</center>,<center>대표자</center>,<center>연락처1</center>,<center>결제한도</center>,<center>대리점</center>,<center>상태</center>";
                    popuponoffmerch_mygrid.setHeader(mheaders);
                    popuponoffmerch_mygrid.setColAlign("center,center,center,center,center,center,center,center,center,center,center,center");
                    popuponoffmerch_mygrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt");
                    popuponoffmerch_mygrid.setInitWidths("70,150,150,150,100,100,100,100,100,100,100,100");
                    popuponoffmerch_mygrid.setColSorting("str,str,str,str,str,str,str,str,str,str,str,str");
                    popuponoffmerch_mygrid.enableColumnMove(true);
                    popuponoffmerch_mygrid.setSkin("dhx_skyblue");
                    
                    
                    popuponoffmerch_mygrid.enablePaging(true,Number($("#popuponffmerchinfoSearch input[name=page_size]").val()),10,"popuponffmerchinfoMasterListPaging",true,"popuponffmerchinfoMasterListrecinfoArea");
                    popuponoffmerch_mygrid.setPagingSkin("bricks");

                    //페이징 처리
                    popuponoffmerch_mygrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        if(lInd !==0){
                          $("#popuponffmerchinfoForm input[name=page_no]").val(ind);             
                           popuponoffmerch_mygrid.clearAll();
                           doPopupSearchMerchantMaster();
                        }else{
                            $("#popuponffmerchinfoForm input[name=page_no]").val(1);
                            popuponoffmerch_mygrid.clearAll();
                             doPopupSearchMerchantMaster();
                        }                        
                    });
                                      
                    
                    
                    popuponoffmerch_mygrid.init();
                    popuponoffmerch_mygrid.parse(${onoffmerchPopupList}, "json");
                    popuponoffmerch_mygrid.attachEvent("onRowSelect", function(rowId,cellIndex){
                    selectOnoffmerchRow(rowId,cellIndex);
                    });
                               
                    
                    
                     //버튼 추가
                    $("#popuponffmerchinfoForm input[name=popuponffmerchinfo_searchButton]").bind("click",function(e){
                        //가맹점정보 정보 조회 이벤트
                        /*
                        $("#popuponffmerchinfoForm input[name=page_no]").val("1");
                        popuponoffmerch_mygrid.clearAll();
                        doPopupSearchMerchantMaster();
                        */
                        popuponoffmerch_mygrid.changePage(1);
                        return false;
                    });
                    
                });
                
                //가맹점정보 정보 조회
                function doPopupSearchMerchantMaster(){
                    $.ajax({
                        url : $("#popuponffmerchinfoForm").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#popuponffmerchinfoForm").serialize(),
                        success : function(data) {
                            var jsonData = $.parseJSON(data);     
                            popuponoffmerch_mygrid.parse(jsonData,"json");
                            
                        },
                        error : function() {
                            alert("조회 실패");
                        }
                    });
                }                
                
                function selectOnoffmerchRow(rowId,cellIndex)
                {
                    //NO,가맹점명,가맹점ID,사업자명,구분,사업자번호,법인번호,대표자,연락처1,결제한도,대리점,상태
                    //0     1        2      3      4       5       6        7      8       9      10    11
                    //p_onffmerch_no, merch_nm
                    SelOnoffmerchInfoInput(
                                popuponoffmerch_mygrid.cells(rowId,2).getValue()
                              , popuponoffmerch_mygrid.cells(rowId,1).getValue()                              
                            );
                    dhxSelPopupWins.window("OnoffmerchSelectPopUp").close();
                }
                
            </script>
            <div class="searchForm1row" id="popuponffmerchinfoSearch" style="width:100%;background-color:white;">
                <form id="popuponffmerchinfoForm" method="POST" action="<c:url value="/popup/OnoffmerchSelectPopUp" />" modelAttribute="MerchantFormBean">
                    <input style="VISIBILITY: hidden; WIDTH: 0px;height: 0px;overflow: hidden">
                       <input type="hidden" name="page_size" value="100" />
                       <input type="hidden" name="page_no" value="1" />
                        <div class="label">지급ID명</div>
                        <div class="input"><input type="text" name="merch_nm" maxlength="30"/></div>                        
                        <input type="button" name="popuponffmerchinfo_searchButton" value="조회"/>
                </form>                
            </div>           
            <div id="poponoffmerchVP" style="position:relative;width:100%;height:80%;background-color:white;"></div>
<div id="popuponffmerchinfoMasterListPaging" style="width: 100%;"></div>
                <div id="popuponffmerchinfoMasterListrecinfoArea" style="width: 100%;"></div>                                        
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

