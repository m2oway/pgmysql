<%-- 
    Document   : merchpayMasterList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>지급ID 지급</title>             
        </head>
        <body>
</c:if>
       
    <body>
        <div class="right" id="merchpayMaster_search" style="width:100%;">       
            <form id="merchpayMasterForm" method="POST" onsubmit="return false" action="<c:url value="/merchpay/merchpayMasterList" />" modelAttribute="merchpayFormBean"> 
                <table>
                    <tr><td>
                        <div class="label">검색일자</div>
                        <div class="input">
                            <input type="text" name="exp_pay_start_dt" id="merchpayMaster_from_date" value ="${merchpayFormBean.exp_pay_start_dt}" onclick="merchpaysetSens('merchpayMaster_to_date', 'max');" /> ~
                            <input type="text" name="exp_pay_end_dt" id="merchpayMaster_to_date" value ="${merchpayFormBean.exp_pay_end_dt}" onclick="merchpaysetSens('merchpayMaster_from_date', 'min');" />
                        </div>
                        
                        <div class ="input"><input type="button" name="week" value="1주"/></div>
                        <div class ="input"><input type="button" name="month" value="1달"/></div>

                        
                         <div class="label2">지급ID</div>
                        <div class="input">
                            <input type="text" name="onffmerch_no" value ="${merchpayFormBean.onffmerch_no}" readonly/>
                        </div> 
                        <div class="label">지급ID명</div>
                        <div class="input">
                            <input type="text" name="merch_nm" value ="${merchpayFormBean.merch_nm}" readonly/>
                        </div>                  
                        <div class="label">입금대사구분</div>
                        <div class="input">
                            <select name="decision_flag">
                                <option value="">전체</option>
                                <c:forEach var="code" items="${decisionFlagList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div></td>                         
                        <td><input type="button" name="merchpayMaster_search" value="조회"/></td>                        
                        <td><input type="button" name="merchpayMaster_excel" value="엑셀"/></td>
                        <td><input type="button" name="init" value="검색조건지우기"/></td>
                        <td><!--<input type="button" name="merchpayMaster_cash" value="선지급"/>--></td>
                    </tr>
               </table>
            </form>
        </div>
        <div id="merchpayMaster" style="position: relative;width:100%;height:100%;background-color:white;"></div>
        
        
         <script type="text/javascript">
            
            var merchpayMastergrid={};
            var merchpayMasterCalendar;
            var outamtWindows={};
            var cashInAdvanceWindows={};
            var merchpayMetabolismWindow;
            var main_merchpayMaster_layout={};
            var dhxSelPopupWins=new dhtmlXWindows();
            
            $(document).ready(function () {
                
                var merchpayMaster_searchForm = document.getElementById("merchpayMaster_search");
                
                var tabBarId = tabbar.getActiveTab();
                main_merchpayMaster_layout = tabbar.cells(tabBarId).attachLayout("2E");
                
                main_merchpayMaster_layout.cells('a').hideHeader();
                main_merchpayMaster_layout.cells('b').hideHeader();
                
                merchpayMaster_layout = main_merchpayMaster_layout.cells('a').attachLayout("2E");
                     
                merchpayMaster_layout.cells('a').hideHeader();
                merchpayMaster_layout.cells('b').hideHeader();
                 
                merchpayMaster_layout.cells('a').attachObject(merchpayMaster_searchForm);
                merchpayMaster_layout.cells('a').setHeight(20);
                
                merchpayMastergrid = merchpayMaster_layout.cells('b').attachGrid();
                
                merchpayMastergrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                
                var merchpayMasterHeaders = "";
                merchpayMasterHeaders += "<center>지급예정일</center>,<center>지급ID</center>,<center>지급ID명</center>,<center>지급확정액</center>,<center>지급예정액</center>";
                merchpayMasterHeaders += ",<center>보류잔액</center>,<center>보류확정액</center>,<center>보류확정잔액</center>,<center>보류해제액</center>,<center>미수잔액</center>";
                merchpayMasterHeaders += ",<center>미수생성액</center>,<center>미수상계액</center>,<center>지급대상액</center>,<center>수수료</center>,<center>부가세</center>,<center>원천징수</center>,<center>승인금액</center>";
                merchpayMasterHeaders += ",<center>취소금액</center>,<center>은행</center>,<center>계좌번호</center>,<center>예금주</center>,<center>대사처리</center>";
                merchpayMastergrid.setHeader(merchpayMasterHeaders);
                
                merchpayMastergrid.attachHeader("#select_filter,#select_filter,#select_filter,&nbsp,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan");
                
                merchpayMastergrid.setColAlign("center,center,center,right,right,right,right,right,right,right,right,right,right,right,right,right,right,right,center,center,center,center");
                merchpayMastergrid.setColTypes("txt,txt,txt,edn,edn,edn,txt,edn,txt,edn,txt,txt,edn,edn,edn,edn,edn,edn,txt,txt,txt,txt");
                merchpayMastergrid.setInitWidths("100,150,150,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,150,100,100");
                merchpayMastergrid.setColSorting("str,str,str,int,int,int,int,str,int,str,int,str,str,int,int,int,int,str,str,str,str");
                merchpayMastergrid.setNumberFormat("0,000",3);
                merchpayMastergrid.setNumberFormat("0,000",4);
                merchpayMastergrid.setNumberFormat("0,000",5);
                //merchpayMastergrid.setNumberFormat("0,000",6);
                merchpayMastergrid.setNumberFormat("0,000",7);
                //merchpayMastergrid.setNumberFormat("0,000",8)
                merchpayMastergrid.setNumberFormat("0,000",9);
                //merchpayMastergrid.setNumberFormat("0,000",10);
                //merchpayMastergrid.setNumberFormat("0,000",11);
                merchpayMastergrid.setNumberFormat("0,000",12);
                merchpayMastergrid.setNumberFormat("0,000",13);
                merchpayMastergrid.setNumberFormat("0,000",14);
                merchpayMastergrid.setNumberFormat("0,000",15);
                merchpayMastergrid.setNumberFormat("0,000",16);
                merchpayMastergrid.setNumberFormat("0,000",17);
                merchpayMastergrid.enableColumnMove(true);
                
                
                var mfooters_filter = "소계,#cspan,#cspan";
	        mfooters_filter += ",<div id='merchpayMaster_sub3'>0</div>,<div id='merchpayMaster_sub4'>0</div>,<div id='merchpayMaster_sub5'>0</div>,<div id='merchpayMaster_sub6'>0</div>,<div id='merchpayMaster_sub7'>0</div>,<div id='merchpayMaster_sub8'>0</div>";
                mfooters_filter += ",<div id='merchpayMaster_sub9'>0</div>,<div id='merchpayMaster_sub10'>0</div>,<div id='merchpayMaster_sub11'>0</div>,<div id='merchpayMaster_sub12'>0</div>,<div id='merchpayMaster_sub13'>0</div>,<div id='merchpayMaster_sub14'>0</div>";
                mfooters_filter += ",<div id='merchpayMaster_sub15'>0</div>,<div id='merchpayMaster_sub16'>0</div>,<div id='merchpayMaster_sub17'>0</div>,&nbsp,#cspan,#cspan,#cspan";
	        merchpayMastergrid.attachFooter(mfooters_filter);
                
                
                var merchpayMastertransfooters = "총계,#cspan,#cspan";
                merchpayMastertransfooters += ",<div id='merchpayMaster3'>0</div>,<div id='merchpayMaster4'>0</div>,<div id='merchpayMaster5'>0</div>,<div id='merchpayMaster6'>0</div>,<div id='merchpayMaster7'>0</div>,<div id='merchpayMaster8'>0</div>";
                merchpayMastertransfooters += ",<div id='merchpayMaster9'>0</div>,<div id='merchpayMaster10'>0</div>,<div id='merchpayMaster11'>0</div>,<div id='merchpayMaster12'>0</div>,<div id='merchpayMaster13'>0</div>,<div id='merchpayMaster14'>0</div>";
                merchpayMastertransfooters += ",<div id='merchpayMaster15'>0</div>,<div id='merchpayMaster16'>0</div>,<div id='merchpayMaster17'>0</div>,&nbsp,#cspan,#cspan,#cspan";
                
                merchpayMastergrid.attachFooter(merchpayMastertransfooters);                       
                
                
                merchpayMastergrid.setSkin("dhx_skyblue");
                merchpayMastergrid.init();
                merchpayMastergrid.attachEvent("onRowDblClicked",onRowDblClicked_merchpayDetail);
                
                
                merchpayMastergrid.parse(${ls_merchpayMasterList},merchpayMasterFooterValues,"json");
                
                
                 
                merchpayMastergrid.attachEvent("onGridReconstructed", function(){
	  	          
	            	var merchpayMaster_sub3 = document.getElementById("merchpayMaster_sub3");
                        var merchpayMaster_sub4 = document.getElementById("merchpayMaster_sub4");
                        var merchpayMaster_sub5 = document.getElementById("merchpayMaster_sub5");
                        var merchpayMaster_sub6 = document.getElementById("merchpayMaster_sub6");
                        var merchpayMaster_sub7 = document.getElementById("merchpayMaster_sub7");
                        var merchpayMaster_sub8 = document.getElementById("merchpayMaster_sub8");
                        var merchpayMaster_sub9 = document.getElementById("merchpayMaster_sub9");
                        var merchpayMaster_sub10 = document.getElementById("merchpayMaster_sub10");
                        var merchpayMaster_sub11 = document.getElementById("merchpayMaster_sub11");
                        var merchpayMaster_sub12 = document.getElementById("merchpayMaster_sub12");
                        var merchpayMaster_sub13 = document.getElementById("merchpayMaster_sub13");
                        var merchpayMaster_sub14 = document.getElementById("merchpayMaster_sub14");
                        var merchpayMaster_sub15 = document.getElementById("merchpayMaster_sub15");
                        var merchpayMaster_sub16 = document.getElementById("merchpayMaster_sub16");
                        var merchpayMaster_sub17 = document.getElementById("merchpayMaster_sub17");
                                               
	            	
	            	var rowId = merchpayMastergrid.getAllRowIds();
	            	
	            	var merchpayMaster_subsum_3 = 0;
                        var merchpayMaster_subsum_4 = 0;
                        var merchpayMaster_subsum_5 = 0;
                        var merchpayMaster_subsum_6 = 0;
                        var merchpayMaster_subsum_7 = 0;
                        var merchpayMaster_subsum_8 = 0;
                        var merchpayMaster_subsum_9 = 0;
                        var merchpayMaster_subsum_10 = 0;
                        var merchpayMaster_subsum_11 = 0;
                        var merchpayMaster_subsum_12 = 0;
                        var merchpayMaster_subsum_13 = 0;
                        var merchpayMaster_subsum_14 = 0;
                        var merchpayMaster_subsum_15 = 0;
                        var merchpayMaster_subsum_16 = 0;
                        var merchpayMaster_subsum_17 = 0;
                        
	    			if(rowId != ''){
	    				var rowIdArr = rowId.split(",");
	    				for(var i=0;i<rowIdArr.length;i++){                                                
                                                                    merchpayMaster_subsum_3  += Number(merchpayMastersub_removetag(merchpayMastergrid.cells(rowIdArr[i],3).getValue()));
                                                                    merchpayMaster_subsum_4  += Number(merchpayMastersub_removetag(merchpayMastergrid.cells(rowIdArr[i],4).getValue()));
                                                                    merchpayMaster_subsum_5  += Number(merchpayMastersub_removetag(merchpayMastergrid.cells(rowIdArr[i],5).getValue()));
                                                                    merchpayMaster_subsum_6  += Number(merchpayMastersub_removetag(merchpayMastergrid.cells(rowIdArr[i],6).getValue()));
                                                                    merchpayMaster_subsum_7  += Number(merchpayMastersub_removetag(merchpayMastergrid.cells(rowIdArr[i],7).getValue()));
                                                                    merchpayMaster_subsum_8  += Number(merchpayMastersub_removetag(merchpayMastergrid.cells(rowIdArr[i],8).getValue()));
                                                                    merchpayMaster_subsum_9  += Number(merchpayMastersub_removetag(merchpayMastergrid.cells(rowIdArr[i],9).getValue()));
                                                                    merchpayMaster_subsum_10 += Number(merchpayMastersub_removetag(merchpayMastergrid.cells(rowIdArr[i],10).getValue()));
                                                                    merchpayMaster_subsum_11 += Number(merchpayMastersub_removetag(merchpayMastergrid.cells(rowIdArr[i],11).getValue()));
                                                                    merchpayMaster_subsum_12 += Number(merchpayMastersub_removetag(merchpayMastergrid.cells(rowIdArr[i],12).getValue()));
                                                                    merchpayMaster_subsum_13 += Number(merchpayMastersub_removetag(merchpayMastergrid.cells(rowIdArr[i],13).getValue()));
                                                                    merchpayMaster_subsum_14 += Number(merchpayMastersub_removetag(merchpayMastergrid.cells(rowIdArr[i],14).getValue()));
                                                                    merchpayMaster_subsum_15 += Number(merchpayMastersub_removetag(merchpayMastergrid.cells(rowIdArr[i],15).getValue()));
                                                                    merchpayMaster_subsum_16 += Number(merchpayMastersub_removetag(merchpayMastergrid.cells(rowIdArr[i],16).getValue()));
                                                                    merchpayMaster_subsum_17 += Number(merchpayMastersub_removetag(merchpayMastergrid.cells(rowIdArr[i],17).getValue()));
	    				}
	    			}
	    				
	    			
                                    merchpayMaster_sub3.innerHTML =  putComma(merchpayMaster_subsum_3);
                                    merchpayMaster_sub4.innerHTML =  putComma(merchpayMaster_subsum_4);
                                    merchpayMaster_sub5.innerHTML =  putComma(merchpayMaster_subsum_5);
                                    merchpayMaster_sub6.innerHTML =  putComma(merchpayMaster_subsum_6);
                                    merchpayMaster_sub7.innerHTML =  putComma(merchpayMaster_subsum_7);
                                    merchpayMaster_sub8.innerHTML =  putComma(merchpayMaster_subsum_8);
                                    merchpayMaster_sub9.innerHTML =  putComma(merchpayMaster_subsum_9);
                                    merchpayMaster_sub10.innerHTML =  putComma(merchpayMaster_subsum_10);
                                    merchpayMaster_sub11.innerHTML =  putComma(merchpayMaster_subsum_11);
                                    merchpayMaster_sub12.innerHTML =  putComma(merchpayMaster_subsum_12);
                                    merchpayMaster_sub13.innerHTML =  putComma(merchpayMaster_subsum_13);
                                    merchpayMaster_sub14.innerHTML =  putComma(merchpayMaster_subsum_14);
                                    merchpayMaster_sub15.innerHTML =  putComma(merchpayMaster_subsum_15);
                                    merchpayMaster_sub16.innerHTML =  putComma(merchpayMaster_subsum_16);
                                    merchpayMaster_sub17.innerHTML =  putComma(merchpayMaster_subsum_16);
				    				    
				    return true;
	            	
	            });                 
                
                
                merchpayMetabolismWindow = new dhtmlXWindows();
                
                //검색조건 초기화
                $("#merchpayMasterForm input[name=init]").click(function () {
                    merchpayMasterInit($("#merchpayMasterForm"));
                });  
                
                $("#merchpayMasterForm input[name=week]").click(function(){
                    merchPay_date_search("week");
                });

                $("#merchpayMasterForm input[name=month]").click(function(){
                    merchPay_date_search("month");
                });

                //검색 이벤트
                $("#merchpayMasterForm input[name=merchpayMaster_search]").click(function () {
                    merchpayMasterSearch();
                });
                
                //엑셀 이벤트
                $("#merchpayMasterForm input[name=merchpayMaster_excel]").click(function () {
                    merchpayMasterSearchExcel();
                });           
                
                //선지급 이벤트
                $("#merchpayMasterForm input[name=merchpayMaster_cash]").click(function () {
                    merchpayCaseInAdvanceWindowPop();
                });                           
                
                merchpayMasterCalendar = new dhtmlXCalendarObject(["merchpayMaster_from_date","merchpayMaster_to_date"]);
                
                $("button.procButton").button({
                    text:true
                })
                .unbind("click")
                .bind("click", function (e) {

                    if (confirm("지급확정을 하시겠습니까?") == true){
                        var rowId = getRowId($(this));

                        merchpayMetabolismConfirmProcess(rowId);
                    }

                });


                $("button.procCancelButton").button({
                    text:true
                })
                .unbind("click")
                .bind("click", function (e) {

                    if (confirm("지급확정취소를 하시겠습니까?") == true){
                        var rowId = getRowId($(this));

                        merchpayMetabolismCancelProcess(rowId);
                    }

                });
                
                $("span.outAmt").click(function(){
                    
                    var rowId = getRowId($(this));

                    payMetabolismOutAmtWindowPop(rowId);

                });
                
                    
            });

            //지급ID번호 팝업 클릭 이벤트
            $("#merchpayMasterForm input[name=onffmerch_no]").unbind("click").bind("click", function (){

                merchpayonoffMerchSelectPopup();

            });        
            
                        //지급ID번호 팝업 클릭 이벤트
            $("#merchpayMasterForm input[name=merch_nm]").unbind("click").bind("click", function (){

                merchpayonoffMerchSelectPopup();

            });   
            
            
            
            function merchpayMasterFooterValues() {
                
                var merchpayMaster3 = document.getElementById("merchpayMaster3");
                var merchpayMaster4 = document.getElementById("merchpayMaster4");
                var merchpayMaster5 = document.getElementById("merchpayMaster5");
                var merchpayMaster6 = document.getElementById("merchpayMaster6");
                var merchpayMaster7 = document.getElementById("merchpayMaster7");
                var merchpayMaster8 = document.getElementById("merchpayMaster8");
                var merchpayMaster9 = document.getElementById("merchpayMaster9");
                var merchpayMaster10 = document.getElementById("merchpayMaster10");
                var merchpayMaster11 = document.getElementById("merchpayMaster11");
                var merchpayMaster12 = document.getElementById("merchpayMaster12");
                var merchpayMaster13 = document.getElementById("merchpayMaster13");
                var merchpayMaster14 = document.getElementById("merchpayMaster14");
                var merchpayMaster15 = document.getElementById("merchpayMaster15");
                var merchpayMaster16 = document.getElementById("merchpayMaster16");
                var merchpayMaster17 = document.getElementById("merchpayMaster17");

                merchpayMaster3.innerHTML =  putComma(merchpayMastersumColumn(3)) ;    
                merchpayMaster4.innerHTML =  putComma(merchpayMastersumColumn(4)) ;
                merchpayMaster5.innerHTML =  putComma(merchpayMastersumColumn(5)) ;
                merchpayMaster6.innerHTML =  putComma(merchpayMastersumColumn(6)) ;
                merchpayMaster7.innerHTML =  putComma(merchpayMastersumColumn(7)) ;
                merchpayMaster8.innerHTML =  putComma(merchpayMastersumColumn(8)) ;
                merchpayMaster9.innerHTML =  putComma(merchpayMastersumColumn(9)) ;
                merchpayMaster10.innerHTML =  putComma(merchpayMastersumColumn(10)) ;
                merchpayMaster11.innerHTML =  putComma(merchpayMastersumColumn(11)) ;
                merchpayMaster12.innerHTML =  putComma(merchpayMastersumColumn(12)) ;
                merchpayMaster13.innerHTML =  putComma(merchpayMastersumColumn(13)) ;
                merchpayMaster14.innerHTML =  putComma(merchpayMastersumColumn(14)) ;
                merchpayMaster15.innerHTML =  putComma(merchpayMastersumColumn(15)) ;
                merchpayMaster16.innerHTML =  putComma(merchpayMastersumColumn(16)) ;
                merchpayMaster17.innerHTML =  putComma(merchpayMastersumColumn(17)) ;

                return true;

            }
            
            function merchpayMastersub_removetag (target_val) 
            {
                var returnval;
            	if(target_val.indexOf("</span>") != -1)
                {
                    returnval= target_val.substring(target_val.indexOf(">")+1,target_val.indexOf("</span>")).replace(/[\',']/gi,"");
                }
                else
                {
                    returnval = target_val.replace(/[\',']/gi,"");
                }
                
                return returnval;
            }            
            
            function merchpayMastersumColumn(ind) {
               	var out = 0;
	    	for (var i = 0; i < merchpayMastergrid.getRowsNum(); i++) 
                {
                    
                    if(ind == 6 || ind == 8 || ind == 10 || ind == 11)
                    {
                        if(merchpayMastergrid.cells2(i, ind).getValue().indexOf("</span>") != -1)
                        {
                            out += parseFloat(merchpayMastergrid.cells2(i, ind).getValue().substring(merchpayMastergrid.cells2(i, ind).getValue().indexOf(">")+1,merchpayMastergrid.cells2(i, ind).getValue().indexOf("</span>")).replace(/[\',']/gi,""));
                        }
                        else
                        {
                            out += parseFloat(merchpayMastergrid.cells2(i, ind).getValue().replace(/[\',']/gi,""));
                        }
                        
	    	    }
                    else
                    {
                        out += parseFloat(merchpayMastergrid.cells2(i, ind).getValue().replace(/[\',']/gi,""));
	    	    }
	    	}
	    	return out;
            }
            
             function merchpayonoffMerchSelectPopup(){
                onoffObject.onffmerch_no = $("#merchpayMasterForm input[name=onffmerch_no]");
                onoffObject.merch_nm = $("#merchpayMasterForm input[name=merch_nm]");
                //지급ID정보 팝업
                w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                w2.setText("지급ID 선택페이지");
                dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
            }

            function merchpayMasterInit($form) {
                searchFormInit($form);
                merchPay_date_search("week");                    
            } 
            
            function merchPay_date_search(day){
                var oneDate = 1000 * 3600 * 24; 
                var now = new Date();
                var week = new Date(now.getTime() + (oneDate*-6));
                var month = new Date(now.getTime() + (oneDate*-31));
                var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                var date=new Date();
                if(day=="today"){
                    this.$("input[id=merchpayMaster_from_date]").val(nowTime);
                    this.$("input[id=merchpayMaster_to_date]").val(nowTime);
                }else if(day=="week"){
                    this.$("input[id=merchpayMaster_from_date]").val(weekTime);
                    this.$("input[id=merchpayMaster_to_date]").val(nowTime);
                }else{
                    this.$("input[id=merchpayMaster_from_date]").val(monthTime);
                    this.$("input[id=merchpayMaster_to_date]").val(nowTime);
                }
            }
            
            //검색
            function merchpayMasterSearch() {
                
                $.ajax({
                    url: "<c:url value="/merchpay/merchpayMasterList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#merchpayMasterForm").serialize(),
                    success: function (data) {
                        
                        var jsonData = $.parseJSON(data);
                        merchpayMastergrid.clearAll();
                        merchpayMastergrid.parse(jsonData,merchpayMasterFooterValues,"json");
                        
                        $("button.procButton").button({
                            text:true
                        })
                        .unbind("click")
                        .bind("click", function (e) {

                            if (confirm("지급확정을 하시겠습니까?") == true){
                                var rowId = getRowId($(this));

                                merchpayMetabolismConfirmProcess(rowId);
                            }

                        });


                        $("button.procCancelButton").button({
                            text:true
                        })
                        .unbind("click")
                        .bind("click", function (e) {

                            if (confirm("지급확정취소를 하시겠습니까?") == true){
                                var rowId = getRowId($(this));

                                merchpayMetabolismCancelProcess(rowId);
                            }

                        });


                        $("span.outAmt").click(function(){

                            var rowId = getRowId($(this));
                            payMetabolismOutAmtWindowPop(rowId);

                        });
                            
                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }
            
            function merchpaysetSens(id, k) {
                // update range
                if (k == "min") {
                    merchpayMasterCalendar.setSensitiveRange(byId(id).value, null);
                } else {
                    merchpayMasterCalendar.setSensitiveRange(null, byId(id).value);
                }
            }

            function byId(id) {
                return document.getElementById(id);
            }              
            
            function payMetabolismOutAmtWindowPop(rowId){

                var dhtmlxwindowSize = {};
                dhtmlxwindowSize.width = 1400;
                dhtmlxwindowSize.height = 230;

                var onffmerch_no = merchpayMastergrid.getUserData(rowId,"onffmerch_no");
                var exp_pay_dt = merchpayMastergrid.getUserData(rowId,"exp_pay_dt");
                
                if(exp_pay_dt == '' || onffmerch_no== '')
                {
                    alert('선택값이 잘못되었습니다.');
                    return false;
                }
				                

                var location = dhxwindowCenterAlgin(dhtmlxwindowSize);

                outamtWindows = merchpayMetabolismWindow.createWindow("outamtListWindow", location.x, location.y, dhtmlxwindowSize.width, dhtmlxwindowSize.height);

                outamtWindows.setText("임시 등록");

                merchpayMetabolismWindow.window("outamtListWindow").setModal(true);

                outamtWindows.attachURL("<c:url value="/merchpay/merchpayScheTmpList" />" + '?exp_pay_dt='+exp_pay_dt + '&onffmerch_no='+onffmerch_no, true);   

            }

            function merchpayMetabolismConfirmProcess(rowId){
                
				
                var exp_pay_dt = merchpayMastergrid.getUserData(rowId,"exp_pay_dt");
                var onffmerch_no = merchpayMastergrid.getUserData(rowId,"onffmerch_no");
                var decision_flag = merchpayMastergrid.getUserData(rowId,"decision_flag");
                
                if(exp_pay_dt == '' || onffmerch_no== '')
                {
                    alert('선택값이 잘못되었습니다.');
                    return false;
                }
				
                
                $.ajax({
                    url : "<c:url value="/merchpay/merchpayMetabolismConfirmProcess" />",
                    type : "POST",
                    async : true,
                    dataType : "json",
                    data: {
                         exp_pay_dt:exp_pay_dt
                        ,onffmerch_no:onffmerch_no
                        ,decision_flag:decision_flag
                    },
                    success : function(data) {
                        if(data ==="success"){
                            alert("지급확정 성공");
                            merchpayMasterSearch();
                        }else{
                            alert("지급확정 실패");
                        }
                    },
                    error : function(jqXHR, textStatus, errorThrown) {
                        alert("지급확정 실패 : " + errorThrown);

                    }
                });
				

            }
            
            function merchpayMetabolismCancelProcess(rowId){
            
                var exp_pay_dt = merchpayMastergrid.getUserData(rowId,"exp_pay_dt");
                var onffmerch_no = merchpayMastergrid.getUserData(rowId,"onffmerch_no");
                var decision_flag = merchpayMastergrid.getUserData(rowId,"decision_flag");
                var pay_sche_seq = merchpayMastergrid.getUserData(rowId,"pay_sche_seq");
                
                
                if(exp_pay_dt == '' || onffmerch_no== '' || decision_flag== '' || pay_sche_seq== '')
                {
                    alert('선택값이 잘못되었습니다.');
                    return false;
                }

                $.ajax({
                    url : "<c:url value="/merchpay/merchpayMetabolismCancelProcess" />",
                    type : "POST",
                    async : true,
                    dataType : "json",
                    data: {
                        exp_pay_dt:exp_pay_dt
                        ,onffmerch_no:onffmerch_no
                        ,decision_flag:decision_flag
                        ,pay_sche_seq:pay_sche_seq
                    },
                    success : function(data) {
                        if(data ==="success"){
                            alert("지급확정취소 성공");
                            merchpayMasterSearch();
                        }else{
                            alert("지급확정취소 실패");
                        }
                    },
                    error : function(jqXHR, textStatus, errorThrown) {
                        alert("지급확정취소 실패 : " + errorThrown);

                    }
                });

            }         
            
            function getRowId($tag){
                /*
                var rowId = 0;

                if(merchpayMastergrid.pagingOn){
                    rowId =Number((merchpayMastergrid.currentPage -1) * Number(merchpayMastergrid.rowsBufferOutSize))+Number($tag.parent().parent().index()-1);
                }else{
                    rowId = Number($tag.parent().parent().index()-1);
                }
                return rowId;
                */
               
               var rowid = 0;
               rowid = Number($tag.attr("row_id"));
               return rowid;
            }            
            
            //지급 상세 조회
            function onRowDblClicked_merchpayDetail(rowid){
                
                var exp_pay_dt = merchpayMastergrid.getUserData(rowid, 'exp_pay_dt');
                var onffmerch_no = merchpayMastergrid.getUserData(rowid, 'onffmerch_no');

                main_merchpayMaster_layout.cells('b').attachURL("<c:url value="/merchpay/merchpayDetailList" />"+"?exp_pay_start_dt=" + exp_pay_dt+"&exp_pay_end_dt=" + exp_pay_dt + "&onffmerch_no="+onffmerch_no, true);                
            }
            
            function merchpayMasterSearchExcel(){
                $("#merchpayMasterForm").attr("action","<c:url value="/merchpay/merchpayMasterListExcel" />");
                document.getElementById("merchpayMasterForm").submit();                    
            }
            
            //선지급
            function merchpayCaseInAdvanceWindowPop(rowId){

                var dhtmlxwindowSize = {};
                dhtmlxwindowSize.width = 1400;
                dhtmlxwindowSize.height = 220;

                var location = dhxwindowCenterAlgin(dhtmlxwindowSize);

                cashInAdvanceWindows = merchpayMetabolismWindow.createWindow("cashInAdvanceWindow", location.x, location.y, dhtmlxwindowSize.width, dhtmlxwindowSize.height);

                cashInAdvanceWindows.setText("선지급");

                merchpayMetabolismWindow.window("cashInAdvanceWindow").setModal(true);

                cashInAdvanceWindows.attachURL("<c:url value="/merchpay/merchpayCashInAdvanceList" />", true);   

            }            
                
        </script>
    </body>
</html>
