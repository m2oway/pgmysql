<%-- 
    Document   : merchpayDetailList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>지급ID 지급 상세 정보</title>             
        </head>
        <body>
</c:if>
       
    <body>
        <div class="right" id="merchpayDetail_search" style="width:100%;">       
            <form id="merchpayDetailForm" method="POST" onsubmit="return false" action="<c:url value="/merchpay/merchpayDetailList" />" modelAttribute="merchpayFormBean"> 
                
                <input type="hidden" name="page_size" value="100" >
                <input type="hidden" name="page_no" value="1" >
                
                <table>
                    <tr><td>
                        <div class="label">검색일자</div>
                        <div class="input">
                            <input type="text" size="12" name="exp_pay_start_dt" id="merchpayDetail_from_date" value ="${merchpayFormBean.exp_pay_start_dt}" onclick="merchpayDetailsetSens('merchpayDetail_to_date', 'max');" /> ~
                            <input type="text" size="12" name="exp_pay_end_dt" id="merchpayDetail_to_date" value="${merchpayFormBean.exp_pay_end_dt}" onclick="merchpayDetailsetSens('merchpayDetail_from_date', 'min');" />
                        </div>
                        
                        <div class ="input"><input type="button" name="week" value="1주"/></div>
                        <div class ="input"><input type="button" name="month" value="1달"/></div>
                        
                        <div class="label2">지급ID</div>
                        <div class="input">
                            <!--<input type="hidden" name="onffmerch_no" value ="${merchpayFormBean.onffmerch_no}" />-->
                            <input type="text" name="onffmerch_no" value ="${merchpayFormBean.onffmerch_no}" readonly/>
                        </div> 
                        <div class="label">지급ID명</div>
                        <div class="input">
                            <input type="text" name="merch_nm" value ="${merchpayFormBean.merch_nm}" readonly/>
                        </div>                  
                        <div class="label">UID</div>
                        <div class="input">
                            <input type="text" name="onfftid" value ="${merchpayFormBean.onfftid}" readonly/>
                        </div>          
                        <div class="label2">UID명</div>
                        <div class="input">
                            <!--<input type="hidden" name="onfftid" value ="${merchpayFormBean.onfftid}" />-->
                            <input type="text" name="onfftid_nm" value ="${merchpayFormBean.onfftid_nm}" readonly/>
                        </div>  
                        <div class="label">결제채널</div>
                        <div class="input">
                            <select name="pay_chn_cate">
                                <option value="">전체</option>
                                <c:forEach var="code" items="${payChnCateList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div></td>  
                        <td><input type="button" name="merchpayDetail_search" value="조회"/></td>                        
                        <td><input type="button" name="merchpayDetail_excel" value="엑셀"/></td>
                        <td><input type="button" name="init" value="검색조건지우기"/></td>
                    </tr>
               </table>
            </form>
            <div class="paging">
                <div id="merchpayDetailPaging" style="width: 50%;"></div>
                <div id="merchpayDetailrecinfoArea" style="width: 50%;"></div>
            </div>            
        </div>
        <div id="merchpayDetail" style="position: relative;width:100%;height:100%;background-color:white;"></div>
        
        
         <script type="text/javascript">
            
            var merchpayDetailgrid={};
            var merchpayDetailCalendar;
            var dhxSelPopupWins=new dhtmlXWindows();
            
            $(document).ready(function () {
                
                var merchpayDetail_searchForm = document.getElementById("merchpayDetail_search");
                
                merchpayDetail_layout = main_merchpayMaster_layout.cells('b').attachLayout("2E");
                     
                merchpayDetail_layout.cells('a').hideHeader();
                merchpayDetail_layout.cells('b').hideHeader();
                 
                merchpayDetail_layout.cells('a').attachObject(merchpayDetail_searchForm);
                merchpayDetail_layout.cells('a').setHeight(60);
                
                merchpayDetailgrid = merchpayDetail_layout.cells('b').attachGrid();
                
                merchpayDetailgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                
                var merchpayDetailHeaders = "";
                merchpayDetailHeaders += "<center>NO</center>,<center>지급예정일</center>,<center>지급ID</center>,<center>지급ID명</center>,<center>UID</center>,<center>ONOFF결제명</center>,<center>결제채널</center>,<center>지급예정액</center>";
                merchpayDetailHeaders += ",<center>수수료</center>,<center>부가세</center>,<center>원천징수</center>,<center>승인금액</center>,<center>취소금액</center>";
                merchpayDetailgrid.setHeader(merchpayDetailHeaders);
                merchpayDetailgrid.setColAlign("center,center,center,center,center,center,center,right,right,right,right,right,right");
                merchpayDetailgrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,edn,edn,edn,edn,edn,edn");
                merchpayDetailgrid.setInitWidths("50,100,200,200,200,200,100,100,100,100,100,100,100");
                merchpayDetailgrid.setColSorting("str,str,str,str,str,str,str,int,int,int,int,int,int");
                merchpayDetailgrid.setNumberFormat("0,000",9);
                merchpayDetailgrid.setNumberFormat("0,000",10);
                merchpayDetailgrid.setNumberFormat("0,000",11);
                merchpayDetailgrid.setNumberFormat("0,000",12);
                merchpayDetailgrid.setNumberFormat("0,000",7);
                merchpayDetailgrid.setNumberFormat("0,000",8);
                merchpayDetailgrid.enableColumnMove(true);
                merchpayDetailgrid.setSkin("dhx_skyblue");
                merchpayDetailgrid.enablePaging(true,Number($("#merchpayDetailForm input[name=page_size]").val()),10,"merchpayDetailPaging",true,"merchpayDetailrecinfoArea");
                merchpayDetailgrid.setPagingSkin("bricks");                                    
                merchpayDetailgrid.init();
                merchpayDetailgrid.parse(${ls_merchpayDetailList},"json");
                
                //검색조건 초기화
                $("#merchpayDetailForm input[name=init]").click(function () {
                    merchPayDetailInit($("#merchpayDetailForm"));
                });
                
                $("#merchpayDetailForm input[name=week]").click(function(){
                    merchPayDetail_date_search("week");
                });

                $("#merchpayDetailForm input[name=month]").click(function(){
                    merchPayDetail_date_search("month");
                });
                
                //검색 이벤트
                $("#merchpayDetailForm input[name=merchpayDetail_search]").click(function () {
                    /*
                    $("input[name=page_no]").val("1");
                    merchpayDetailSearch();
                    */
                    
                    merchpayDetailgrid.changePage(1);
                    
                });
                
                //엑셀 이벤트
                $("#merchpayDetailForm input[name=merchpayDetail_excel]").click(function () {
                    
                    $("#merchpayDetailForm").attr("action","<c:url value="/merchpay/merchpayDetailListExcel" />");
                    document.getElementById("merchpayDetailForm").submit();    
                    
                });                
                
                //페이징 처리
                merchpayDetailgrid.attachEvent("onPageChanged", function(ind,fInd,lInd){

                        if(lInd !==0){
                          $("#merchpayDetailForm input[name=page_no]").val(ind);             
                           merchpayDetailgrid.clearAll();
                           doSearchAgentMerchantMaster();
                        }else{
                            $("#merchpayDetailForm input[name=page_no]").val(1);
                            merchpayDetailgrid.clearAll();
                             merchpayDetailSearch();
                        }                       
                });                
                
                merchpayDetailCalendar = new dhtmlXCalendarObject(["merchpayDetail_from_date","merchpayDetail_to_date"]);

            });
            
             //ONOFF가맹점번호 팝업 클릭 이벤트
                $("#merchpayDetailForm input[name=onffmerch_no]").unbind("click").bind("click", function (){

                    merchpayDetailonoffMerchSelectPopup();

                });
                
                $("#merchpayDetailForm input[name=merch_nm]").unbind("click").bind("click", function (){

                    merchpayDetailonoffMerchSelectPopup();

                });                           
                
                 function merchpayDetailonoffMerchSelectPopup(){
                    onoffObject.onffmerch_no = $("#merchpayDetailForm input[name=onffmerch_no]");
                    onoffObject.merch_nm = $("#merchpayDetailForm input[name=merch_nm]");
                    //ONOFF가맹점정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("가맹점 선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                }

                
                //UID 팝업 클릭 이벤트
                $("#merchpayDetailForm input[name=onfftid]").unbind("click").bind("click", function (){

                    merchpayDetailonoffTidSelectPopup();

                });       
                
                
                //UID 팝업 클릭 이벤트
                $("#merchpayDetailForm input[name=onfftid_nm]").unbind("click").bind("click", function (){

                    merchpayDetailonoffTidSelectPopup();

                });                                
                                
                
                function merchpayDetailonoffTidSelectPopup(){
                    onoffObject.onfftid_nm = $("#merchpayDetailForm input[name=onfftid_nm]");
                    onoffObject.onfftid = $("#merchpayDetailForm input[name=onfftid]");
                    //UID 팝업
                    w2 = dhxSelPopupWins.createWindow("OnofftidSelectPopUp", 20, 30, 640, 480);
                    w2.setText("UID 선택페이지");
                    dhxSelPopupWins.window('OnofftidSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnofftidSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnofftidSelectPopUp"/>', true);   
                }                
              
            
            function merchPayDetail_date_search(day){
                var oneDate = 1000 * 3600 * 24; 
                var now = new Date();
                var week = new Date(now.getTime() + (oneDate*-6));
                var month = new Date(now.getTime() + (oneDate*-31));
                var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                var date=new Date();
                if(day=="today"){
                    this.$("input[id=merchpayDetail_from_date]").val(nowTime);
                    this.$("input[id=merchpayDetail_to_date]").val(nowTime);
                }else if(day=="week"){
                    this.$("input[id=merchpayDetail_from_date]").val(weekTime);
                    this.$("input[id=merchpayDetail_to_date]").val(nowTime);
                }else{
                    this.$("input[id=merchpayDetail_from_date]").val(monthTime);
                    this.$("input[id=merchpayDetail_to_date]").val(nowTime);
                }
            }
            
            function merchPayDetailInit($form) {
                searchFormInit($form);
                merchPayDetail_date_search("week");                    
            } 

            
            
            //검색
            function merchpayDetailSearch() {
                
                $.ajax({
                    url: "<c:url value="/merchpay/merchpayDetailList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#merchpayDetailForm").serialize(),
                    success: function (data) {
                        
                        var jsonData = $.parseJSON(data);
                        //merchpayDetailgrid.clearAll();
                        merchpayDetailgrid.parse(jsonData,"json");
                            
                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }
            
            function merchpayDetailsetSens(id, k) {
                // update range
                if (k == "min") {
                    merchpayDetailCalendar.setSensitiveRange(byId(id).value, null);
                } else {
                    merchpayDetailCalendar.setSensitiveRange(null, byId(id).value);
                }
            }

            function byId(id) {
                return document.getElementById(id);
            }                        
                
        </script>
    </body>
</html>
