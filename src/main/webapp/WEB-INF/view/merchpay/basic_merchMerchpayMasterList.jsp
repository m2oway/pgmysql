<%-- 
    Document   : merchpayMasterList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="/WEB-INF/view/common/basic_common.jsp"/>
<%@include file="/WEB-INF/view/common/basic_comm_header.jsp"%>
    <table width="100%" height="42" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top" class="titleBg">
		<table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="title"><b>-일별정산</b></td>
                        <td><img src="../resources/images/title_sright.gif" width="12" height="38" /></td>
                    </tr>
		</table>
            </td>
	</tr>
    </table>
        <div class="right" id="merchpayMaster_search" style="width:100%;">       
            <form name="merchpayMasterForm" id="merchpayMasterForm" method="POST" onsubmit="return false" action="<c:url value="/merchpay/basic_merchMerchpayMasterList" />" modelAttribute="merchpayFormBean"> 
                <table>
                    <tr><td>
                        <div class="label">검색일자</div>
                        <div class="input">
                            <input type="text" name="exp_pay_start_dt" id="exp_pay_start_dt" value ="${merchpayFormBean.exp_pay_start_dt}" onclick="merchpaysetSens('exp_pay_start_dt', 'max');" /> ~
                            <input type="text" name="exp_pay_end_dt" id="exp_pay_end_dt" value ="${merchpayFormBean.exp_pay_end_dt}" onclick="merchpaysetSens('exp_pay_end_dt', 'min');" />
                        </div>
                        <div class="label">입금대사구분</div>
                        <div class="input">
                            <select name="decision_flag">
                                <option value="">전체</option>
                                <c:forEach var="code" items="${decisionFlagList.tb_code_details}">
                                    <c:if test="${merchpayFormBean.decision_flag == code.detail_code}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                    </c:if>
                                    <c:if test="${merchpayFormBean.decision_flag != code.detail_code}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </div></td>         
                        <td><input type="button" name="merchpayMaster_search" onclick="javascript:merchpayMasterSearch();" value="조회"/></td>
                        <td><input type="button" name="merchpayMaster_excel" onclick="javascript:merchpayMasterSearchExcel();" value="엑셀"/></td>
                    </tr>
               </table>
            </form>
        </div><br>
        <div class="CSSTableGenerator" >
                        <table >
                    <tr>
                        <td>&nbsp;</td>
                        <td>거래금액</td>
                        <td >거래취소금액</td>
                        <td>거래신고액</td>
                        <td>가맹점수수료</td>
                        <td>가맹점부가세</td>
                        <td>지급금액</td>
                    </tr>
                    <tr>
                        <td>합계</td>
                        <td style="text-align: right"><fmt:formatNumber value="${total_app_amt}" type="number"/></td>
                        <td style="text-align: right"><fmt:formatNumber value="${total_cncl_amt}" type="number"/></td>
                        <td style="text-align: right"><fmt:formatNumber value="${total_pay_targetamt}" type="number"/></td>
                        <td style="text-align: right"><fmt:formatNumber value="${total_commission}" type="number"/></td>
                        <td style="text-align: right"><fmt:formatNumber value="${total_vat}" type="number"/></td>
                        <td style="text-align: right"><fmt:formatNumber value="${total_decision_pay_amt}" type="number"/></td>
                    </tr>                    
                </table>
        </div><br>        
                <div class="CSSTableGenerator" >
                    <table>
                        <tr>
                            <td height="30px;" width="30px"  style="text-align:center">
                                No
                            </td>                            
                            <td height="30px;"  style="text-align:center">
                                가맹점
                            </td>
                            <td  >
                                정산일자
                            </td>
                            <td >
                                거래금액
                            </td>
                            <td >
                                거래취소금액
                            </td>
                            <td >
                                거래신고액
                            </td>                            
                            <td >
                                가맹점수수료
                            </td>
                            <td >
                                가맹점부가세
                            </td>
                            <td >
                                지급금액
                            </td>  
                        </tr>
     <c:forEach var="mechpaylist" items="${ls_merchpayMasterList}" begin="0">              
                        <tr>
                            <td height="30px;" width="30px"  style="text-align:center">${mechpaylist.rnum}</td>                                 
                            <td height="30px;"  style="text-align:center">${mechpaylist.merch_nm}</td>
                            <td ><fmt:parseDate value="${mechpaylist.exp_pay_dt}" var="exppaydt1" pattern="yyyyMMdd" scope="page"/><fmt:formatDate value="${exppaydt1}" pattern="yyyy-MM-dd" /></td>
                            <td style="text-align: right"><fmt:formatNumber value="${mechpaylist.app_amt}" type="number"/></td>
                            <td style="text-align: right"><fmt:formatNumber value="${mechpaylist.cncl_amt}" type="number"/></td>
                            <td style="text-align: right"><fmt:formatNumber value="${mechpaylist.pay_targetamt}" type="number"/></td>
                            <td style="text-align: right"><fmt:formatNumber value="${mechpaylist.commission}" type="number"/></td>
                            <td style="text-align: right"><fmt:formatNumber value="${mechpaylist.vat}" type="number"/></td>
                            <td style="text-align: right"><fmt:formatNumber value="${mechpaylist.decision_pay_amt}" type="number"/></td>
                        </tr>
    </c:forEach>
                    </table>
                </div>                                  
<%@include file="/WEB-INF/view/common/basic_comm_tail.jsp"%>        
         <script type="text/javascript">
            
            var merchpayMasterCalendar;
            var dhxSelPopupWins=new dhtmlXWindows();
            
            $(document).ready(function () {
                
                merchpayMetabolismWindow = new dhtmlXWindows();
                 merchpayMasterCalendar = new dhtmlXCalendarObject(["exp_pay_start_dt","exp_pay_end_dt"]);
                
                /*
                //검색조건 초기화
                $("#merchpayMasterForm input[name=init]").click(function () {
                    merchpayMasterInit($("#merchpayMasterForm"));
                });  
                
                $("#merchpayMasterForm input[name=week]").click(function(){
                    merchPay_date_search("week");
                });

                $("#merchpayMasterForm input[name=month]").click(function(){
                    merchPay_date_search("month");
                });

                //검색 이벤트
                $("#merchpayMasterForm input[name=merchpayMaster_search]").click(function () {
                    merchpayMasterSearch();
                });
                
                //엑셀 이벤트
                $("#merchpayMasterForm input[name=merchpayMaster_excel]").click(function () {
                    merchpayMasterSearchExcel();
                });                
                */
               
                    
            });
            
            //검색
            function merchpayMasterSearch() {
                document.merchpayMasterForm.submit();
            }
            
            function merchpaysetSens(id, k) {
                // update range
                if (k == "min") {
                    merchpayMasterCalendar.setSensitiveRange(byId(id).value, null);
                } else {
                    merchpayMasterCalendar.setSensitiveRange(null, byId(id).value);
                }
            }

            function byId(id) {
                return document.getElementById(id);
            }                    
            
            
            function merchpayMasterSearchExcel(){
                $("#merchpayMasterForm").attr("action","<c:url value="/merchpay/basic_merchMerchpayMasterListExcel" />");
                document.getElementById("merchpayMasterForm").submit();                    
            }
            
                
        </script>

