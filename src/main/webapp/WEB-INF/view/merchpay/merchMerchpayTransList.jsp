<%-- 
    Document   : codeDetailMaster
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>개별 정산 조회</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">

                var merchpaytransdetailgrid={};
                var merchpaytransdetail_layout={};
                var complainMasterWindow;
                var merchpaytransdetailCalendar1;

                $(document).ready(function () {
                    
                    merchpaytransdetailCalendar1 = new dhtmlXCalendarObject(["merchMerchpayTransForm_app_dt_start","merchMerchpayTransForm_app_dt_end","merchMerchpayTransForm_adj_from_date","merchMerchpayTransForm_adj_to_date","merchMerchpayTransForm_pay_from_date","merchMerchpayTransForm_pay_to_date"]);    
                
                    var merchMerchpayTransForm = document.getElementById("merchpaytransdetail_search");
                    
                    var tabBarId = tabbar.getActiveTab();
                    merchpaytransdetail_layout = tabbar.cells(tabBarId).attachLayout("2E");
                    merchpaytransdetail_layout.cells('a').hideHeader();
                    merchpaytransdetail_layout.cells('b').hideHeader();
                    merchpaytransdetail_layout.cells('a').attachObject(merchMerchpayTransForm);
                    merchpaytransdetail_layout.cells('a').setHeight(85);
                    merchpaytransdetailgrid = merchpaytransdetail_layout.cells('b').attachGrid();
                    merchpaytransdetailgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");

                    var merchpaytransdetailheaders = "";
                    merchpaytransdetailheaders +=  "<center>NO</center>,<center>지급일</center>,<center>승인일</center>,<center>지급ID명</center>,<center>UID명</center>,<center>고객명</center>,<center>고객전화번호</center>";
                    merchpaytransdetailheaders += ",<center>상품명</center>,<center>거래종류</center>,<center>카드사</center>,<center>카드번호</center>,<center>할부</center>";
                    merchpaytransdetailheaders += ",<center>승인금액</center>,<center>승인번호</center>,<center>승인시간</center>,<center>취소일</center>,<center>거래상태</center>";
                    merchpaytransdetailheaders += ",<center>지급예정금액</center>,<center>수수료</center>,<center>부가세</center>,<center>원천징수</center>,<center>정산일</center>";
                    merchpaytransdetailgrid.setHeader(merchpaytransdetailheaders);
                    var merchpaytransdetailColAlign = "";
                    merchpaytransdetailColAlign +=  "center,center,center,center,center,center,center";
                    merchpaytransdetailColAlign += ",center,center,center,center,center";
                    merchpaytransdetailColAlign += ",right,center,center,center,center";
                    merchpaytransdetailColAlign += ",right,right,right,right,center";
                    merchpaytransdetailgrid.setColAlign(merchpaytransdetailColAlign);
                    var merchpaytransdetailColTypes = "";
                    merchpaytransdetailColTypes +=  "txt,txt,txt,txt,txt,txt,txt";
                    merchpaytransdetailColTypes += ",txt,txt,txt,txt,txt";
                    merchpaytransdetailColTypes += ",edn,txt,txt,txt,txt";
                    merchpaytransdetailColTypes += ",edn,edn,edn,edn,txt";
                    merchpaytransdetailgrid.setColTypes(merchpaytransdetailColTypes);
                    var merchpaytransdetailInitWidths = "";
                    merchpaytransdetailInitWidths +=   "50,80,80,150,150,100,90";
                    merchpaytransdetailInitWidths += ",150,70,80,140,60";
                    merchpaytransdetailInitWidths += ",80,90,100,80,80";
                    merchpaytransdetailInitWidths += ",80,80,80,80,80";
                    merchpaytransdetailgrid.setInitWidths(merchpaytransdetailInitWidths);
                    var merchpaytransdetailColSorting = "";
                    merchpaytransdetailColSorting +=  "str,str,str,str,str,str,str";
                    merchpaytransdetailColSorting += ",str,str,str,str,str";
                    merchpaytransdetailColSorting += ",int,str,str,str,str";
                    merchpaytransdetailColSorting += ",int,int,int,int,str";
                    merchpaytransdetailgrid.setColSorting(merchpaytransdetailColSorting);
                    merchpaytransdetailgrid.setNumberFormat("0,000",12);
                    merchpaytransdetailgrid.setNumberFormat("0,000",17);
                    merchpaytransdetailgrid.setNumberFormat("0,000",18);                                
                    merchpaytransdetailgrid.setNumberFormat("0,000",19);
                    merchpaytransdetailgrid.setNumberFormat("0,000",20);
                    
                    merchpaytransdetailgrid.enableColumnMove(true);
                    merchpaytransdetailgrid.setSkin("dhx_skyblue");
                    merchpaytransdetailgrid.enablePaging(true,Number($("#merchMerchpayTransForm input[name=page_size]").val()),10,"merchpaytransdetailPaging",true,"merchpaytransdetailrecinfoArea");
                    merchpaytransdetailgrid.setPagingSkin("bricks");

                    //페이징 처리
                    merchpaytransdetailgrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        
                        if(lInd !==0){
                          $("#merchMerchpayTransForm input[name=page_no]").val(ind);             
                           merchpaytransdetailgrid.clearAll();
                           merchpaytransdetailSearch();
                        }else{
                            $("#merchMerchpayTransForm input[name=page_no]").val(1);
                            merchpaytransdetailgrid.clearAll();
                            merchpaytransdetailSearch();
                        }                            
                    });

                    merchpaytransdetailgrid.init();
                    merchpaytransdetailgrid.parse(${ls_merchMerchpayTransList}, "json");

                    //상세 검색
                    $("#merchMerchpayTransForm input[name=merchpaytransdetail_search]").unbind("click").bind("click", function (){
                        /*
                        $("input[name=page_no]").val("1");

                        merchpaytransdetailSearch();
                        */
                        merchpaytransdetailgrid.changePage(1);

                    });              

                    //개별 정산 정보 엑셀다운로드 이벤트
                    $("#merchMerchpayTransForm input[name=merchpaytransdetail_excel]").click(function () {

                        merchpaytransDetailExcel();

                    });               
                     $("#merchMerchpayTransForm input[name=week]").click(function(){
                        merchpaytransdetail_date_search("week");
                    });

                    $("#merchMerchpayTransForm input[name=month]").click(function(){
                        merchpaytransdetail_date_search("month");
                    });
                    //검색조건 초기화
                    $("#merchMerchpayTransForm input[name=init]").click(function () {
                    
                        transDetailMasterInit($("#merchMerchpayTransForm"));

                    });  
                    
                });
                 function merchpaytransdetail_date_search(day){
                    var oneDate = 1000 * 3600 * 24; 
                    var now = new Date();
                    var week = new Date(now.getTime() + (oneDate*-6));
                    var month = new Date(now.getTime() + (oneDate*-31));
                    var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                    var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                    var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                    var date=new Date();

                    if(day=="today"){
                        $("#merchMerchpayTransForm input[id=merchMerchpayTransForm_app_dt_start]").val(nowTime);
                        $("#merchMerchpayTransForm input[id=merchMerchpayTransForm_app_dt_end]").val(nowTime);
                    }else if(day=="week"){
                        $("#merchMerchpayTransForm input[id=merchMerchpayTransForm_app_dt_start]").val(weekTime);
                        $("#merchMerchpayTransForm input[id=merchMerchpayTransForm_app_dt_end]").val(nowTime);
                    }else{
                        $("#merchMerchpayTransForm input[id=merchMerchpayTransForm_app_dt_start]").val(monthTime);
                        $("#merchMerchpayTransForm input[id=merchMerchpayTransForm_app_dt_end]").val(nowTime);
                    }
                }
                //개별 정산 정보 조회
                function merchpaytransdetailSearch() {
                
                    $.ajax({
                        url: "<c:url value="/merchpay/merchMerchpayTransList" />",
                        type: "POST",
                        async: true,
                        dataType: "json",
                        data: $("#merchMerchpayTransForm").serialize(),
                        success: function (data) {

                            var jsonData = $.parseJSON(data);
                            //merchpaytransdetailgrid.clearAll();
                            merchpaytransdetailgrid.parse(jsonData,"json");                       

                        },
                        error: function () {
                            alert("조회 실패");
                        }
                    });
                    return false;
                }
            
                function merchpaytransDetailExcel(){
                    $("#merchMerchpayTransForm").attr("action","<c:url value="/merchpay/merchMerchpayTransListExcel" />");
                    document.getElementById("merchMerchpayTransForm").submit();            
                }
                    
                var dhxSelPopupWins=new dhtmlXWindows();
                    
                //ONOFF지급ID번호 팝업 클릭 이벤트
                $("#merchMerchpayTransForm input[name=onffmerch_no]").unbind("click").bind("click", function (){

                    onoffTransDetailMerchSelectPopup();

                });                           
                    
                function onoffTransDetailMerchSelectPopup(){
                    onoffObject.onffmerch_no = $("#merchMerchpayTransForm input[name=onffmerch_no]");
                    onoffObject.merch_nm = $("#merchMerchpayTransForm input[name=merch_nm]");
                    //ONOFF지급ID정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("지급ID 선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                }
                                  
                //ONOFFUID 팝업 클릭 이벤트
                $("#merchMerchpayTransForm input[name=onfftid_nm]").unbind("click").bind("click", function (){

                    onoffTransDetailTidSelectPopup();

                });                                
                
                function onoffTransDetailTidSelectPopup(){
                    onoffObject.onfftid_nm = $("#merchMerchpayTransForm input[name=onfftid_nm]");
                    onoffObject.onfftid = $("#merchMerchpayTransForm input[name=onfftid]");
                    //ONOFFUID 팝업
                    w2 = dhxSelPopupWins.createWindow("OnofftidSelectPopUp", 20, 30, 640, 480);
                    w2.setText("UID 선택페이지");
                    dhxSelPopupWins.window('OnofftidSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnofftidSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnofftidSelectPopUp"/>', true);   
                }                
              
                   
                
                function merchpaytransdetail_setSens(id, k) {
                    // update range
                    if (k == "min") {
                        merchpaytransdetailCalendar1.setSensitiveRange(merchpaytransdetail_byId(id).value, null);
                    } else {
                        merchpaytransdetailCalendar1.setSensitiveRange(null, merchpaytransdetail_byId(id).value);
                    }
                }

                function merchpaytransdetail_byId(id) {
                    return document.getElementById(id);
                }
                
                function transDetailMasterInit($form) {

                    searchFormInit($form);

                    //merchpaytransdetail_date_search("week");                    
                } 
                
            </script>

            <div class="right" id="merchpaytransdetail_search" style="width:100%;">
                <form id="merchMerchpayTransForm" method="POST" action="<c:url value="/merchpay/merchMerchpayTransList" />" modelAttribute="transFormBean">

                    <input type="hidden" name="page_size" value="100" >
                    <input type="hidden" name="page_no" value="1" >
                    <!--
                    <input type="hidden" name="onffmerch_no" value="${transFormBean.onffmerch_no}" >
                    -->
                    <table>
                        <tr>
                            <div class="label">승인일</div>
                            <div class="input">
                            <input type="text" size=10 name="app_dt_start" id="merchMerchpayTransForm_app_dt_start" value ="${transFormBean.app_dt_start}" onclick="merchpaytransdetail_setSens('merchMerchpayTransForm_app_dt_end', 'max');" /> ~
                            <input type="text" size=10 name="app_dt_end" id="merchMerchpayTransForm_app_dt_end" value="${transFormBean.app_dt_end}" onclick="merchpaytransdetail_setSens('merchMerchpayTransForm_app_dt_start', 'min');" /> </div>
                            <div class ="input" ><input type="button" name="week" value="1주"/></div>
                            <div class ="input" ><input type="button" name="month" value="1달"/></div>   
                            <div class="label">지급일</div>
                            <div class="input">
                            <input type="text" size=10 name="pay_start_dt" id="merchMerchpayTransForm_pay_from_date" value ="${transFormBean.pay_start_dt}" onclick="merchpaytransdetail_setSens('merchMerchpayTransForm_pay_to_date', 'max');" /> ~
                            <input type="text" size=10 name="pay_end_dt" id="merchMerchpayTransForm_pay_to_date" value="${transFormBean.pay_end_dt}" onclick="merchpaytransdetail_setSens('merchMerchpayTransForm_pay_from_date', 'min');" /> </div>   
                            <div class="label">지급ID명</div>
                            <div class="input">
                                <input type="text" name="merch_nm" value ="${transFormBean.merch_nm}" />
                            </div>        
                            <div class="label">지급ID</div>
                            <div class="input">
                                <input type="text" name="onffmerch_no" value ="${transFormBean.onffmerch_no}" />
                            </div>                                     
                            <div class="label">UID명</div>
                            <div class="input">                                
                                <input type="text" name="onfftid_nm" value ="${transFormBean.onfftid_nm}" />
                            </div>    
                            <div class="label">UID</div>
                            <div class="input">
                                <input type="text" name="onfftid" value ="${transFormBean.onfftid}" />
                            </div>    
                            <div class="label">고객명</div>
                            <div class="input">
                                <input type="text" name="user_nm" value ="${transFormBean.user_nm}" />
                            </div>                               
                            <div class="label">고객전화번호</div>
                            <div class="input">
                                <input size=10 type="text" name="user_phone2" value ="${transFormBean.user_phone2}" />
                            </div>                                                             
                            <div class="label">상품명</div>
                            <div class="input">
                                <input type="text" name="product_nm" value ="${transFormBean.product_nm}" />
                            </div>                                                   
                            <div class="label">승인금액</div>
                            <div class="input">
                                <input size=10 type="text" name="tot_amt" value ="${transFormBean.tot_amt}" />
                            </div>                                               
                            <div class="label">카드번호</div>
                            <div class="input">
                                <input type="text" name="card_num" value ="${transFormBean.card_num}" />
                            </div>                                                     
                            <div class="label">승인번호</div>
                            <div class="input">
                                <input type="text" size=10 name="app_no" value ="${transFormBean.app_no}" />
                            </div>                               
                            <div class="label">카드사</div>
                            <div class="input">
                                <select name="iss_cd">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${issCdList.tb_code_details}">
                                        <option value="${code.detail_code}" <c:if test="${code.detail_code == transFormBean.result_status}">selected</c:if> >${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>              
                            <div class="label">정산일</div>
                            <div class="input">
                            <input type="text" size=10 name="adj_start_dt" id="merchMerchpayTransForm_adj_from_date" value ="${transFormBean.adj_start_dt}" onclick="merchpaytransdetail_setSens('merchMerchpayTransForm_adj_to_date', 'max');" /> ~
                            <input type="text" size=10 name="adj_end_dt" id="merchMerchpayTransForm_adj_to_date" value="${transFormBean.adj_end_dt}" onclick="merchpaytransdetail_setSens('merchMerchpayTransForm_adj_from_date', 'min');" /> </div>

                            <td><input type="button" name="merchpaytransdetail_search" value="조회"/></td>
                            <td><input type="button" name="merchpaytransdetail_excel" value="엑셀"/></td>
                            <td><input type="button" name="init" value="검색조건지우기"/></td>                            
                        </tr>
                    </table>
                        
                </form>

            <div class="paging">
                <div id="merchpaytransdetailPaging" style="width: 50%;"></div>
                <div id="merchpaytransdetailrecinfoArea" style="width: 50%;"></div>
            </div>
                                
        </div>

<c:if test="${!ajaxRequest}">
    </body>
</html>    
</c:if>