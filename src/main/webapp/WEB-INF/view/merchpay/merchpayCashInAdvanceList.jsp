<%-- 
    Document   : merchpayCashInAdvanceList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>선지급</title>             
        </head>
        <body>
</c:if>
       
    <body>
        <div class="right" style="width:100%;">       
            <form id="merchpayCashInAdvanceForm" method="POST" onsubmit="return false" action="<c:url value="/merchpay/merchpayCashInAdvanceInsert" />" modelAttribute="merchpayFormBean"> 
                <br/><br/>
                <table class="gridtable" height="100%" width="100%">
                    <tr>
                        <td class="headcol">지급ID번호</td>
                        <td><input type="text" name="onffmerch_no" value="" /></td>
                        <td class="headcol">지급ID</td>
                        <td><input type="text" name="merch_nm" value="" /></td>
                    </tr>
                </table>
                <br/><br/>
                <table class="gridtable" height="100%" width="100%">
                    <tr>
                        <td class="headcol">지급예정일</td>
                        <td><input type="text" name="exp_pay_dt" id="merchpayCashInAdvanceForm_exp_pay_dt" value ="" /></td>
                        <td class="headcol">지급확정액</td>
                        <td>0</td>
                        <td class="headcol">지급예정액</td>
                        <td>0</td>                        
                        <td class="headcol">보류잔액</td>
                        <td>0</td>
                        <td class="headcol">보류확정액</td>
                        <td>0</td>
                        <td class="headcol">보류확정잔액</td>
                        <td>0</td>                        
                        <td class="headcol">보류해제액</td>
                        <td>0</td>                        
                        <td class="headcol">미수잔액</td>
                        <td>0</td>                        
                        <td class="headcol">미수생성액</td>
                        <td><input type="text" name="out_amt" value ="" style="width: 50px" /></td>                        
                        <td class="headcol">미수상계액</td>
                        <td>0</td>                                                
                    </tr>
                    <tr>
                        <td colspan="20">
                            <center>
                                <input type="button" name="insert" value="저장" />
                                <input type="button" name="close" value="닫기" />
                            </center>
                        </td>
                    </tr>                    
                </table>                    
            </form>
        </div>
        
         <script type="text/javascript">
            
            $(document).ready(function () {
                
                merchpayMasterCalendar = new dhtmlXCalendarObject(["merchpayCashInAdvanceForm_exp_pay_dt"]);
                    
            });
            
            //닫기
            $("#merchpayCashInAdvanceForm input[name=close]").click(function(){
                merchpayMetabolismWindow.window("cashInAdvanceWindow").close();
            });   
            
            //지급ID번호 팝업 클릭 이벤트
            $("#merchpayCashInAdvanceForm input[name=onffmerch_no]").unbind("click").bind("click", function (){

                merchpayCashInputonoffMerchSelectPopup();

            });          
            
            function merchpayCashInputonoffMerchSelectPopup(){
                onoffObject.onffmerch_no = $("#merchpayCashInAdvanceForm input[name=onffmerch_no]");
                onoffObject.merch_nm = $("#merchpayCashInAdvanceForm input[name=merch_nm]");
                //지급ID정보 팝업
                w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                w2.setText("지급ID 선택페이지");
                dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
            }            
            
            //저장
            $("#merchpayCashInAdvanceForm input[name=insert]").click(function(){             
                
                if($("#merchpayCashInAdvanceForm input[name=out_amt]").val().trim().length === 0){
                    alert("미수생성액을 입력하여 주십시오.");
                    $("#merchpayCashInAdvanceForm input[name=out_amt]").focus();
                    return false;
                } 
                
                if(!window.confirm("저장하시겠습니까?")){
                    return;
                }

                $.ajax({
                    url : $("#merchpayCashInAdvanceForm").attr("action"),
                    type : "POST",
                    async : true,
                    dataType : "json",
                    data: $("#merchpayCashInAdvanceForm").serialize(),
                    success : function(data) {
                        alert("저장 완료");
                        merchpayMasterSearch();
                        merchpayMetabolismWindow.window("cashInAdvanceWindow").close();
                    },
                    error : function() { 
                            alert("저장 실패");
                    }
                });	   
            });               

        </script>
    </body>
</html>
