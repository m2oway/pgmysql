<%-- 
    Document   : codeDetailMaster
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="/WEB-INF/view/common/basic_common.jsp"/>
<%@include file="/WEB-INF/view/common/basic_comm_header.jsp"%>
    <table width="100%" height="42" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top" class="titleBg">
		<table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="title"><b>-개별정산</b></td>
                        <td><img src="../resources/images/title_sright.gif" width="12" height="38" /></td>
                    </tr>
		</table>
            </td>
	</tr>
    </table>
            <div class="right" id="merchpaytransdetail_search" style="width:100%;">
                <form name="merchMerchpayTransForm" id="merchMerchpayTransForm" method="POST" action="<c:url value="/merchpay/basic_merchMerchpayTransList" />" modelAttribute="transFormBean">
                    <input type="hidden" name="page_size" id="page_size" value="20" >
                    <input type="hidden" name="page_no" id="page_no" value="${transFormBean.page_no}" >
                    <input type="hidden" name="total_cnt" id="total_cnt" value="${total_count}" >
                    <input type="hidden" name="nopageflag" value="Y">
                    <table>
                        <tr><td>
                            <div class="label">승인일</div>
                            <div class="input">
                            <input type="text" size=10 name="app_dt_start" id="merchMerchpayTransForm_app_dt_start" value ="${transFormBean.app_dt_start}" onclick="merchpaytransdetail_setSens('merchMerchpayTransForm_app_dt_end', 'max');" /> ~
                            <input type="text" size=10 name="app_dt_end" id="merchMerchpayTransForm_app_dt_end" value="${transFormBean.app_dt_end}" onclick="merchpaytransdetail_setSens('merchMerchpayTransForm_app_dt_start', 'min');" /> </div>
                            <div class="label">지급일</div>
                            <div class="input">
                            <input type="text" size=10 name="pay_start_dt" id="merchMerchpayTransForm_pay_from_date" value ="${transFormBean.pay_start_dt}" onclick="merchpaytransdetail_setSens('merchMerchpayTransForm_pay_to_date', 'max');" /> ~
                            <input type="text" size=10 name="pay_end_dt" id="merchMerchpayTransForm_pay_to_date" value="${transFormBean.pay_end_dt}" onclick="merchpaytransdetail_setSens('merchMerchpayTransForm_pay_from_date', 'min');" /> </div>   
                            <div class="label">카드사</div>
                            <div class="input">
                                <select name="iss_cd">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${issCdList.tb_code_details}">
                                        <option value="${code.detail_code}" <c:if test="${code.detail_code == transFormBean.iss_cd}">selected</c:if> >${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="label">검색항목</div>
                            <div class="input">
                                <select name="searchinputnm" id="searchinputnm" onchange="javascirpt:searchinputnmChange(this);">
                                    <option value="user_nm" <c:if test="${'user_nm' == transFormBean.searchinputnm}">selected</c:if>>고객명</option>
                                    <option value="product_nm" <c:if test="${'product_nm' == transFormBean.searchinputnm}">selected</c:if>>상품명</option>
                                    <option value="user_phone2" <c:if test="${'user_phone2' == transFormBean.searchinputnm}">selected</c:if>>전화번호</option>
                                    <option value="tot_amt" <c:if test="${'tot_amt' == transFormBean.searchinputnm}">selected</c:if>>승인금액</option>
                                    <option value="card_num" <c:if test="${'card_num' == transFormBean.searchinputnm}">selected</c:if>>카드번호</option>
                                    <option value="app_no" <c:if test="${'app_no' == transFormBean.searchinputnm}">selected</c:if>>승인번호</option>
                                </select>
                                <input type="text" name="searchinputcontent" id="searchinputcontent"  value ="${transFormBean.searchinputcontent}" onblur="javascirpt:searchinputcontentChange(this);" />
                            </div>                            
                            <input type="hidden" name="user_nm" id="user_nm" value ="${transFormBean.user_nm}" />
                            <input type="hidden" name="user_phone2" id="user_phone2"  value ="${transFormBean.user_phone2}" />
                            <input type="hidden" name="product_nm" id="product_nm"  value ="${transFormBean.product_nm}" />
                            <input type="hidden" name="tot_amt" id="tot_amt" value ="${transFormBean.tot_amt}" />
                            <input type="hidden" name="card_num" id="card_num" value ="${transFormBean.card_num}" />
                            <input type="hidden" name="app_no" id="app_no" value ="${transFormBean.app_no}" />     
                            <!--
                            <div class="label">고객명</div>
                            <div class="input">
                                <input type="text" name="user_nm" value ="${transFormBean.user_nm}" />
                            </div>                               
                            <div class="label">고객전화번호</div>
                            <div class="input">
                                <input size=10 type="text" name="user_phone2" value ="${transFormBean.user_phone2}" />
                            </div>                                                             
                            <div class="label">상품명</div>
                            <div class="input">
                                <input type="text" name="product_nm" value ="${transFormBean.product_nm}" />
                            </div>                                                   
                            <div class="label">승인금액</div>
                            <div class="input">
                                <input size=10 type="text" name="tot_amt" value ="${transFormBean.tot_amt}" />
                            </div>                                               
                            <div class="label">카드번호</div>
                            <div class="input">
                                <input type="text" name="card_num" value ="${transFormBean.card_num}" />
                            </div>                                                     
                            <div class="label">승인번호</div>
                            <div class="input">
                                <input type="text" size=10 name="app_no" value ="${transFormBean.app_no}" />
                            </div></td>
                            -->
                            <td><input type="button" name="merchpaytransdetail_search" onclick="javascript:merchpaytransdetailSearch();" value="조회"/></td>
                            <td><input type="button" name="merchpaytransdetail_excel" onclick="javascript:merchpaytransDetailExcel();" value="엑셀"/></td>
                        </tr>
                    </table>
                </form>
        </div><br>
 <div class="CSSTableGenerator" >
                        <table >
                    <tr>
                        <td>&nbsp;</td>
                        <td>거래금액</td>
                        <td >거래취소금액</td>
                        <td>거래신고액</td>
                        <td>가맹점수수료</td>
                        <td>가맹점부가세</td>
                        <td>지급금액</td>
                    </tr>
                    <tr>
                        <td>합계</td>
                        <td style="text-align: right"><fmt:formatNumber value="${TotAppAmt}" type="number"/></td>
                        <td style="text-align: right"><fmt:formatNumber value="${TotCnclAmt}" type="number"/></td>
                        <td style="text-align: right"><fmt:formatNumber value="${TotAmt}" type="number"/></td>
                        <td style="text-align: right"><fmt:formatNumber value="${Onofftid_commision}" type="number"/></td>
                        <td style="text-align: right"><fmt:formatNumber value="${Onofftid_tax_amt}" type="number"/></td>
                        <td style="text-align: right"><fmt:formatNumber value="${TotOnofftid_pay_amt}" type="number"/></td>
                    </tr>                    
                </table>
        </div><br>                
                <div class="CSSTableGenerator" >
                <table>
                    <tr>
                        <td height="30px;" width="30px"  style="text-align:center">No</td>  
                        <td height="40px;"  style="text-align:center">가맹점</td>
                        <td >카드사</td>
                        <td >카드번호</td>
                        <td >할부</td>
                        <td >승인번호</td>
                        <td >승인금액</td>
                        <td >승인일시</td>
                        <td >승인취소일</td>
                        <td >원승인번호</td>
                        <td >결제상태</td>
                        <td >가맹점수수료</td>
                        <td >가맹점부가세</td>
                        <td >지급금액</td>
                        <td width="80px;">지급일</td>
                    </tr>
 <c:forEach var="merchtrantblist" items="${ls_merchMerchpayTransList}" begin="0">              
                    <tr>
                        <td height="30px;" width="30px"  style="text-align:center">${merchtrantblist.rnum}</td>
                        <td height="40px;" style="text-align:center">${merchtrantblist.merch_nm}</td>
                        <td >${merchtrantblist.iss_nm}</td>
                        <td >${merchtrantblist.card_num}</td>
                        <td >${merchtrantblist.installment}</td>
                        <td >${merchtrantblist.app_no}</td>
                        <td style="text-align: right"><fmt:formatNumber value="${merchtrantblist.tot_amt}" type="number"/></td>
                        <td width="120px;"><fmt:parseDate value="${merchtrantblist.app_dt} ${merchtrantblist.app_tm}" var="appdt1" pattern="yyyyMMdd HHmmss" scope="page"/><fmt:formatDate value="${appdt1}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
                        <td width="80px;"><fmt:parseDate value="${merchtrantblist.cncl_dt}" var="cncldt1" pattern="yyyyMMdd" scope="page"/><fmt:formatDate value="${cncldt1}" pattern="yyyy-MM-dd" /></td>
                        <td >${merchtrantblist.org_app_no}</td>
                        <td >${merchtrantblist.tran_status_nm}</td>
                        <td style="text-align: right"><fmt:formatNumber value="${merchtrantblist.onofftid_commision}" type="number"/></td>
                        <td style="text-align: right"><fmt:formatNumber value="${merchtrantblist.onofftid_tax_amt}" type="number"/></td>
                        <td style="text-align: right"><fmt:formatNumber value="${merchtrantblist.onofftid_pay_amt}" type="number"/></td>
                        <td width="80px;"><fmt:parseDate value="${merchtrantblist.onoffmerch_ext_pay_dt}" var="paydt1" pattern="yyyyMMdd" scope="page"/><fmt:formatDate value="${paydt1}" pattern="yyyy-MM-dd" /></td>
                    </tr>
</c:forEach>
                </table>
            </div>       
            <div id="paging" name="paging" style="margin: 15px;"></div>
        </div>
<%@include file="/WEB-INF/view/common/basic_comm_tail.jsp"%>
            <script type="text/javascript">

                
                var merchpaytransdetailCalendar1;
                var dhxSelPopupWins=new dhtmlXWindows();
                $(document).ready(function () {
                    
                    merchpaytransdetailCalendar1 = new dhtmlXCalendarObject(["merchMerchpayTransForm_app_dt_start","merchMerchpayTransForm_app_dt_end","merchMerchpayTransForm_pay_from_date","merchMerchpayTransForm_pay_to_date"]);    
                    //makepage(${transFormBean.page_no});
                });
 
                //개별 정산 정보 조회
                function merchpaytransdetailSearch() {
                    $("#page_no").val("1");
                    document.merchMerchpayTransForm.submit();
                }
            
                function merchpaytransDetailExcel(){
                    $("#merchMerchpayTransForm").attr("action","<c:url value="/merchpay/basic_merchMerchpayTransListExcel" />");
                    document.getElementById("merchMerchpayTransForm").submit();            
                }
                
                function merchpaytransdetail_setSens(id, k) {
                    // update range
                    if (k == "min") {
                        merchpaytransdetailCalendar1.setSensitiveRange(merchpaytransdetail_byId(id).value, null);
                    } else {
                        merchpaytransdetailCalendar1.setSensitiveRange(null, merchpaytransdetail_byId(id).value);
                    }
                }

                function merchpaytransdetail_byId(id) {
                    return document.getElementById(id);
                }
                
                function transDetailMasterInit($form) {

                    searchFormInit($form);
                } 
                
                
            function makepage(startIndex) 
            {
     			
                var pagingHTML 		= "";
		var page 		= parseInt($("#page_no").val());
		var totalCount		= parseInt($("#total_cnt").val());
		var pageBlock		= parseInt($("#page_size").val());
		var navigatorNum    = 10;
		var firstPageNum	= 1;
		var lastPageNum		= Math.floor((totalCount-1)/pageBlock) + 1;
		var previewPageNum  = page == 1 ? 1 : page-1;
		var nextPageNum		= page == lastPageNum ? lastPageNum : page+1;
		var indexNum		= startIndex <= navigatorNum  ? 0 : parseInt((startIndex-1)/navigatorNum) * navigatorNum;
				
		if (totalCount > 1) 
                {
					
                    if (startIndex > 1) 
                    {
                        pagingHTML += "<a href='#' id='"+firstPageNum+"'><img src='../resources/images/btn_first.gif' width='11' height='11'/></a> ";
			pagingHTML += "<a href='#' id='"+previewPageNum+"'><img src='../resources/images/btn_prev.gif' width='11' height='11'/></a>";
                    }
		
                    for (var i=1; i<=navigatorNum; i++) 
                    {
			var pageNum = i + indexNum;
					
			if (pageNum == startIndex) 
                            pagingHTML += "<a href='#' id='"+pageNum+"'>"+pageNum+"</a> ";
			else 
                            pagingHTML += "<a href='#' id='"+pageNum+"'>"+pageNum+"</a>  ";
					
			if (pageNum==lastPageNum)
			break;
		    }
					
		    if (startIndex < lastPageNum) {
			pagingHTML += "<a href='#' id='"+nextPageNum+"'><img src='../resources/images/btn_next.gif' width='11' height='11'/></a> ";
			pagingHTML += "<a href='#' id='"+lastPageNum+"'><img src='../resources/images/btn_end.gif' width='11' height='11'/></a>";
		    }
					
		}
				
				
		$("#paging").html(pagingHTML);
		
		$("#paging a").click(function (e) {
			paging_move($(this).attr('id'));
		});

	}        
        
        
        function paging_move(pagenum)
        {
            //alert(pagenum);
            $("#page_no").val(pagenum);
             document.merchMerchpayTransForm.submit();
        }  
        
                function searchinputcontentChange(selobj)
                {
                    
                    var selvalue = selobj.value;
                    var selnm = $("#searchinputnm").val();
                   
                    if(selnm == "user_nm")
                    {
                        $("#user_nm").val(selvalue);
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                        $("#app_no").val("");                    
                    }
                    else if(selnm == "user_phone2")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val(selvalue);
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                        $("#app_no").val("");
                    }
                    else if(selnm == "product_nm")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val(selvalue);
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                        $("#app_no").val("");
                    }
                    else if(selnm == "tot_amt")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val(selvalue);
                        $("#card_num").val("");
                        $("#app_no").val("");
                    }
                    else if(selnm == "card_num")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val(selvalue);
                        $("#app_no").val("");
                    }
                    else if(selnm == "app_no")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                        $("#app_no").val(selvalue);
                    }                
                }
                
                function searchinputnmChange(selobj)
                {
                    var selnm = selobj.value;
                    var salval = $("#searchinputcontent").val();
                    
                    if(selnm == "user_nm")
                    {
                        $("#user_nm").val(salval);
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                        $("#app_no").val("");                    
                    }
                    else if(selnm == "user_phone2")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val(salval);
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                        $("#app_no").val("");
                    }
                    else if(selnm == "product_nm")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val(salval);
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                        $("#app_no").val("");
                    }
                    else if(selnm == "tot_amt")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val(salval);
                        $("#card_num").val("");
                        $("#app_no").val("");
                    }
                    else if(selnm == "card_num")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val(salval);
                        $("#app_no").val("");
                    }
                    else if(selnm == "app_no")
                    {
                        $("#user_nm").val("");
                        $("#user_phone2").val("");
                        $("#product_nm").val("");
                        $("#tot_amt").val("");
                        $("#card_num").val("");
                        $("#app_no").val(salval);
                    }                    
                }        
            </script>