<%-- 
    Document   : merchpayMasterList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>가맹점 지급</title>             
        </head>
        <body>
</c:if>
       
    <body>
        <div class="right" id="merchpayMaster_search" style="width:100%;">       
            <form id="merchpayMasterForm" method="POST" onsubmit="return false" action="<c:url value="/merchpay/merchMerchpayMasterList" />" modelAttribute="merchpayFormBean"> 
                <table>
                    <tr><td>
                        <div class="label">검색일자</div>
                        <div class="input">
                            <input type="text" name="exp_pay_start_dt" id="merchpayMaster_from_date" value ="${merchpayFormBean.exp_pay_start_dt}" onclick="merchpaysetSens('merchpayMaster_to_date', 'max');" /> ~
                            <input type="text" name="exp_pay_end_dt" id="merchpayMaster_to_date" value ="${merchpayFormBean.exp_pay_end_dt}" onclick="merchpaysetSens('merchpayMaster_from_date', 'min');" />
                        </div>
                        
                        <div class ="input"><input type="button" name="week" value="1주"/></div>
                        <div class ="input"><input type="button" name="month" value="1달"/></div>

                        
                         <div class="label2">지급ID</div>
                        <div class="input">
                            <!--<input type="hidden" name="onffmerch_no" value ="${merchpayFormBean.onffmerch_no}" />-->
                            <input type="text" name="onffmerch_no" value ="${merchpayFormBean.onffmerch_no}" readonly/>
                        </div> 
                        <div class="label">지급ID명</div>
                        <div class="input">
                            <input type="text" name="merch_nm" value ="${merchpayFormBean.onffmerch_no}" />
                        </div>
                        <div class="label">입금대사구분</div>
                        <div class="input">
                            <select name="decision_flag">
                                <option value="">전체</option>
                                <c:forEach var="code" items="${decisionFlagList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div></td>         
                        <td><input type="button" name="merchpayMaster_search" value="조회"/></td>
                        <td><input type="button" name="merchpayMaster_excel" value="엑셀"/></td>
                        <td><input type="button" name="init" value="검색조건지우기"/></td>
                    </tr>
               </table>
            </form>
        </div>
        <div id="merchpayMaster" style="position: relative;width:100%;height:100%;background-color:white;"></div>
        
        
         <script type="text/javascript">
            
            var merchpayMastergrid={};
            var merchpayMasterCalendar;
            var outamtWindows={};
            var merchpayMetabolismWindow;
            var main_merchpayMaster_layout={};
            var dhxSelPopupWins=new dhtmlXWindows();
            
            $(document).ready(function () {
                
                var merchpayMaster_searchForm = document.getElementById("merchpayMaster_search");
                
                var tabBarId = tabbar.getActiveTab();
                main_merchpayMaster_layout = tabbar.cells(tabBarId).attachLayout("2E");
                
                main_merchpayMaster_layout.cells('a').hideHeader();
                main_merchpayMaster_layout.cells('b').hideHeader();
                
                merchpayMaster_layout = main_merchpayMaster_layout.cells('a').attachLayout("2E");
                     
                merchpayMaster_layout.cells('a').hideHeader();
                merchpayMaster_layout.cells('b').hideHeader();
                 
                merchpayMaster_layout.cells('a').attachObject(merchpayMaster_searchForm);
                merchpayMaster_layout.cells('a').setHeight(20);
                
                merchpayMastergrid = merchpayMaster_layout.cells('b').attachGrid();
                
                merchpayMastergrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                
                var merchpayMasterHeaders = "";
                merchpayMasterHeaders += "<center>지급예정일</center>,<center>지급ID</center>,<center>지급ID명</center>,<center>지급확정액</center>,<center>지급예정액</center>";
                merchpayMasterHeaders += ",<center>보류확정액</center>,<center>보류해제액</center>,<center>미수생성액</center>,<center>미수상계액</center>,<center>지급대상액</center>,<center>수수료</center>,<center>부가세</center>,<center>승인금액</center>,<center>취소금액</center>";
                merchpayMasterHeaders += ",<center>은행</center>,<center>계좌번호</center>,<center>예금주</center>";
                merchpayMastergrid.setHeader(merchpayMasterHeaders);
                merchpayMastergrid.setColAlign("center,center,center,right,right,right,right,right,right,right,right,right,right,right,center,center,center");
                merchpayMastergrid.setColTypes("txt,txt,txt,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,edn,txt,txt,txt");
                merchpayMastergrid.setInitWidths("100,150,150,100,100,100,100,100,100,100,100,100,100,100,100,100,150,100");
                merchpayMastergrid.setColSorting("str,str,str,int,int,int,int,int,int,int,int,int,int,int,int,str,str,str");
                merchpayMastergrid.setNumberFormat("0,000",3);
                merchpayMastergrid.setNumberFormat("0,000",4);
                merchpayMastergrid.setNumberFormat("0,000",5);
                merchpayMastergrid.setNumberFormat("0,000",6);
                merchpayMastergrid.setNumberFormat("0,000",7);
                merchpayMastergrid.setNumberFormat("0,000",8);
                merchpayMastergrid.setNumberFormat("0,000",9);
                merchpayMastergrid.setNumberFormat("0,000",10);
                merchpayMastergrid.setNumberFormat("0,000",11);
                merchpayMastergrid.setNumberFormat("0,000",12);
                merchpayMastergrid.setNumberFormat("0,000",13);
                merchpayMastergrid.enableColumnMove(true);
                

                var merchpayMastertransfooters = "총계,#cspan,#cspan";
                merchpayMastertransfooters += ",<div id='merchpayMaster3'>0</div>,<div id='merchpayMaster4'>0</div>,<div id='merchpayMaster5'>0</div>,<div id='merchpayMaster6'>0</div>,<div id='merchpayMaster7'>0</div>,<div id='merchpayMaster8'>0</div>";
                merchpayMastertransfooters += ",<div id='merchpayMaster9'>0</div>,<div id='merchpayMaster10'>0</div>,<div id='merchpayMaster11'>0</div>,<div id='merchpayMaster12'>0</div>,<div id='merchpayMaster13'>0</div>";
                merchpayMastertransfooters += ",기타,#cspan,#cspan";
                
                merchpayMastergrid.attachFooter(merchpayMastertransfooters);                                       
                
                
                merchpayMastergrid.setSkin("dhx_skyblue");
                merchpayMastergrid.init();
                merchpayMastergrid.attachEvent("onRowDblClicked",onRowDblClicked_merchpayDetail);
                merchpayMastergrid.parse(${ls_merchpayMasterList},merchpayMasterFooterValues,"json");
                
                merchpayMetabolismWindow = new dhtmlXWindows();
                
                //검색조건 초기화
                $("#merchpayMasterForm input[name=init]").click(function () {
                    merchpayMasterInit($("#merchpayMasterForm"));
                });  
                
                $("#merchpayMasterForm input[name=week]").click(function(){
                    merchPay_date_search("week");
                });

                $("#merchpayMasterForm input[name=month]").click(function(){
                    merchPay_date_search("month");
                });

                //검색 이벤트
                $("#merchpayMasterForm input[name=merchpayMaster_search]").click(function () {
                    merchpayMasterSearch();
                });
                
                //엑셀 이벤트
                $("#merchpayMasterForm input[name=merchpayMaster_excel]").click(function () {
                    merchpayMasterSearchExcel();
                });                
                
                merchpayMasterCalendar = new dhtmlXCalendarObject(["merchpayMaster_from_date","merchpayMaster_to_date"]);
                    
            });

                //지급ID번호 팝업 클릭 이벤트
                $("#merchpayMasterForm input[name=onffmerch_no]").unbind("click").bind("click", function (){

                    merchpayonoffMerchSelectPopup();

                });                           
                 function merchpayonoffMerchSelectPopup(){
                    onoffObject.onffmerch_no = $("#merchpayMasterForm input[name=onffmerch_no]");
                    onoffObject.merch_nm = $("#merchpayMasterForm input[name=merch_nm]");
                    //지급ID정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("가맹점 선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                }
                
                

            function merchpayMasterFooterValues() {
                
                var merchpayMaster3 = document.getElementById("merchpayMaster3");
                var merchpayMaster4 = document.getElementById("merchpayMaster4");
                var merchpayMaster5 = document.getElementById("merchpayMaster5");
                var merchpayMaster6 = document.getElementById("merchpayMaster6");
                var merchpayMaster7 = document.getElementById("merchpayMaster7");
                var merchpayMaster8 = document.getElementById("merchpayMaster8");
                var merchpayMaster9 = document.getElementById("merchpayMaster9");
                var merchpayMaster10 = document.getElementById("merchpayMaster10");
                var merchpayMaster11 = document.getElementById("merchpayMaster11");
                var merchpayMaster12 = document.getElementById("merchpayMaster12");
                var merchpayMaster13 = document.getElementById("merchpayMaster13");

                merchpayMaster3.innerHTML =  putComma(merchpayMastersumColumn(3)) ;    
                merchpayMaster4.innerHTML =  putComma(merchpayMastersumColumn(4)) ;
                merchpayMaster5.innerHTML =  putComma(merchpayMastersumColumn(5)) ;
                merchpayMaster6.innerHTML =  putComma(merchpayMastersumColumn(6)) ;
                merchpayMaster7.innerHTML =  putComma(merchpayMastersumColumn(7)) ;
                merchpayMaster8.innerHTML =  putComma(merchpayMastersumColumn(8)) ;
                merchpayMaster9.innerHTML =  putComma(merchpayMastersumColumn(9)) ;
                merchpayMaster10.innerHTML =  putComma(merchpayMastersumColumn(10)) ;
                merchpayMaster11.innerHTML =  putComma(merchpayMastersumColumn(11)) ;
                merchpayMaster12.innerHTML =  putComma(merchpayMastersumColumn(12)) ;
                merchpayMaster13.innerHTML =  putComma(merchpayMastersumColumn(13)) ;

                return true;

            }
            
            function merchpayMastersumColumn(ind) {
                var out = 0;
                for (var i = 0; i < merchpayMastergrid.getRowsNum(); i++) {
                     out += parseFloat(merchpayMastergrid.cells2(i, ind).getValue());
                }

                return out;
            }                
                
                function merchpayMasterInit($form) {
                    searchFormInit($form);
                    merchPay_date_search("week");                    
                } 
            
            function merchPay_date_search(day){
                var oneDate = 1000 * 3600 * 24; 
                var now = new Date();
                var week = new Date(now.getTime() + (oneDate*-6));
                var month = new Date(now.getTime() + (oneDate*-31));
                var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                var date=new Date();
                if(day=="today"){
                    this.$("input[id=merchpayMaster_from_date]").val(nowTime);
                    this.$("input[id=merchpayMaster_to_date]").val(nowTime);
                }else if(day=="week"){
                    this.$("input[id=merchpayMaster_from_date]").val(weekTime);
                    this.$("input[id=merchpayMaster_to_date]").val(nowTime);
                }else{
                    this.$("input[id=merchpayMaster_from_date]").val(monthTime);
                    this.$("input[id=merchpayMaster_to_date]").val(nowTime);
                }
            }
            
            //검색
            function merchpayMasterSearch() {
                
                $.ajax({
                    url: "<c:url value="/merchpay/merchMerchpayMasterList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#merchpayMasterForm").serialize(),
                    success: function (data) {
                        
                        var jsonData = $.parseJSON(data);
                        merchpayMastergrid.clearAll();
                        merchpayMastergrid.parse(jsonData,merchpayMasterFooterValues,"json");
                            
                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }
            
            function merchpaysetSens(id, k) {
                // update range
                if (k == "min") {
                    merchpayMasterCalendar.setSensitiveRange(byId(id).value, null);
                } else {
                    merchpayMasterCalendar.setSensitiveRange(null, byId(id).value);
                }
            }

            function byId(id) {
                return document.getElementById(id);
            }                    
            
            //지급 상세 조회
            function onRowDblClicked_merchpayDetail(rowid){
                
                var exp_pay_dt = merchpayMastergrid.getUserData(rowid, 'exp_pay_dt');
                var onffmerch_no = merchpayMastergrid.getUserData(rowid, 'onffmerch_no');

                main_merchpayMaster_layout.cells('b').attachURL("<c:url value="/merchpay/merchMerchpayDetailList" />"+"?exp_pay_start_dt=" + exp_pay_dt+"&exp_pay_end_dt=" + exp_pay_dt + "&onffmerch_no="+onffmerch_no, true);                
            }
            
            function merchpayMasterSearchExcel(){
                $("#merchpayMasterForm").attr("action","<c:url value="/merchpay/merchMerchpayMasterListExcel" />");
                document.getElementById("merchpayMasterForm").submit();                    
            }
                
        </script>
    </body>
</html>
