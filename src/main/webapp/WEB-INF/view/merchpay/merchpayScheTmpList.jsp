<%-- 
    Document   : merchpayMasterList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>임시 정보</title>             
        </head>
        <body>
</c:if>
       
    <body>
        <div class="right" style="width:100%;">       
            <form id="merchpayScheTmpForm" method="POST" onsubmit="return false" action="<c:url value="/merchpay/merchpayScheTmpInsert" />" modelAttribute="merchpayFormBean"> 
                <input type="hidden" name="onffmerch_no" value="${ls_merchpayMasterList[0].onffmerch_no}" />
                <input type="hidden" name="exp_pay_dt" value="${ls_merchpayMasterList[0].exp_pay_dt}" />
                <br/><br/>
                <table class="gridtable" height="100%" width="100%">
                    <tr>
                        <td class="headcol">지급ID</td>
                        <td>${ls_merchpayMasterList[0].onffmerch_no}</td>
                        <td class="headcol">지급ID명</td>
                        <td>${ls_merchpayMasterList[0].merch_nm}</td>
                    </tr>
                </table>
                <br/><br/>
                <table class="gridtable" height="100%" width="100%">
                    <tr>
                        <td class="headcol">지급예정일</td>
                        <td>${ls_merchpayMasterList[0].exp_pay_dt}</td>
                            
                        <td class="headcol">지급확정액</td>
                        <td>${ls_merchpayMasterList[0].decision_pay_amt}</td>
                        <td class="headcol">지급예정액</td>
                        <td>${ls_merchpayMasterList[0].pay_amt}</td>                        
                        <td class="headcol">보류잔액</td>
                        <td>${ls_merchpayMasterList[0].dfr_bal_amt}</td>
                        <td class="headcol">보류확정액</td>
                        <td><input type="text" name="dec_dfr_amt" value ="${ls_merchpayScheTmpList[0].dec_dfr_amt}" style="width: 50px" /></td>
                        <td class="headcol">보류확정잔액</td>
                        <td>${ls_merchpayMasterList[0].dec_bal_amt}</td>                        
                        <td class="headcol">보류해제액</td>
                        <td><input type="text" name="rel_dfr_amt" value ="${ls_merchpayScheTmpList[0].rel_dfr_amt}" style="width: 50px" /></td>                        
                        <td class="headcol">미수잔액</td>
                        <td>${ls_merchpayMasterList[0].cur_outamt}</td>                        
                        <td class="headcol">미수생성액</td>
                        <td><input type="text" name="out_amt" value ="${ls_merchpayScheTmpList[0].out_amt}" style="width: 50px" /></td>                        
                        <td class="headcol">미수상계액</td>
                        <td><input type="text" name="setoff_amt" value ="${ls_merchpayScheTmpList[0].setoff_amt}" style="width: 50px" /></td>                                                
                    </tr>
                    <tr>
                        <td colspan="20">
                            <center>
                                <input type="button" name="insert" value="저장" />
                                <input type="button" name="close" value="닫기" />
                            </center>
                        </td>
                    </tr>                    
                </table>                    
            </form>
        </div>
        
         <script type="text/javascript">
            
            $(document).ready(function () {
                    
            });
            
            //닫기
            $("#merchpayScheTmpForm input[name=close]").click(function(){
                merchpayMetabolismWindow.window("outamtListWindow").close();
            });   
            
            //저장
            $("#merchpayScheTmpForm input[name=insert]").click(function(){
                
                if($("#merchpayScheTmpForm input[name=dec_dfr_amt]").val().trim().length == 0){
                    alert("보류확정액을 입력하여 주십시오.");
                    $("#merchpayScheTmpForm input[name=dec_dfr_amt]").focus();
                    return false;
                }
                
                var tporgdfr_bal_amt = "${ls_merchpayMasterList[0].dfr_bal_amt}";
                var tpdecdframt = $("#merchpayScheTmpForm input[name=dec_dfr_amt]").val();
               
                //if($("#merchpayScheTmpForm input[name=dec_dfr_amt]").val() > tporgdfr_bal_amt)
                if(Number(tpdecdframt) > Number(tporgdfr_bal_amt))
                {
                    alert("보류확정액이 잘못되었습니다.");
                    $("#merchpayScheTmpForm input[name=dfr_bal_amt]").focus();
                    return false;
                }
                
                if($("#merchpayScheTmpForm input[name=rel_dfr_amt]").val().trim().length == 0){
                    alert("보류해제액을 입력하여 주십시오.");
                    $("#merchpayScheTmpForm input[name=rel_dfr_amt]").focus();
                    return false;
                } 
                
                var tporgdec_bal_amt = "${ls_merchpayMasterList[0].dec_bal_amt}";
                var tpreldframt= $("#merchpayScheTmpForm input[name=rel_dfr_amt]").val();
                //if($("#merchpayScheTmpForm input[name=rel_dfr_amt]").val() > tporgdec_bal_amt)
                if(Number(tpreldframt) > Number(tporgdec_bal_amt))
                {
                    alert("보류해제액이 잘못되었습니다.");
                    $("#merchpayScheTmpForm input[name=rel_dfr_amt]").focus();
                    return false;
                }                
                
                if($("#merchpayScheTmpForm input[name=out_amt]").val().trim().length == 0){
                    alert("미수생성액을 입력하여 주십시오.");
                    $("#merchpayScheTmpForm input[name=out_amt]").focus();
                    return false;
                } 
                
                if($("#merchpayScheTmpForm input[name=setoff_amt]").val().trim().length == 0){
                    alert("미수상계액을 입력하여 주십시오.");
                    $("#merchpayScheTmpForm input[name=setoff_amt]").focus();
                    return false;
                } 
                
                var tporgtpsetoff_amt = "${ls_merchpayMasterList[0].cur_outamt}";
                var tpsetoffamt = $("#merchpayScheTmpForm input[name=setoff_amt]").val();
                //if($("#merchpayScheTmpForm input[name=setoff_amt]").val() > tporgtpsetoff_amt)
                if(Number(tpsetoffamt) > Number(tporgtpsetoff_amt))
                {
                    alert("미수상계액이 잘못되었습니다.");
                    $("#merchpayScheTmpForm input[name=setoff_amt]").focus();
                    return false;
                }
                
                if(!window.confirm("저장하시겠습니까?")){
                    return;
                }

                $.ajax({
                    url : $("#merchpayScheTmpForm").attr("action"),
                    type : "POST",
                    async : true,
                    dataType : "json",
                    data: $("#merchpayScheTmpForm").serialize(),
                    success : function(data) {
                        alert("저장 완료");
                        merchpayMasterSearch();
                        merchpayMetabolismWindow.window("outamtListWindow").close();
                    },
                    error : function() { 
                            alert("저장 실패");
                    }
                });	   
                
            });               

        </script>
    </body>
</html>
