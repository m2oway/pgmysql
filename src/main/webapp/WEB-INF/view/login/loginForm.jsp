<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<!-- start : title -->
<!--
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
-->
 <!DOCTYPE html >
    <html>
    <head>
        <title>TKON 관리사이트 로그인</title>
        <meta hㅅttp-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- 	jQuery  스크립트 -->

        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/smoothness/jquery-ui-1.8.17.custom.css"  />" />
        <script type="text/javascript" src="<c:url value="/resources/javascript/jquery-1.6.2.min.js" />"></script>
        <script type="text/javascript" src="<c:url value="/resources/javascript/jquery-ui-1.8.17.custom.min.js" />"></script>
        <script type="text/javascript" src="<c:url value="/resources/javascript/jqueryform/2.8/jquery.form.js" />"></script>


        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/main/settlementPaymentCss.css" />"/>
    </head>
    <body>
<c:if test="${not empty message}">
    <c:if test="${'success' eq message}">
        <script type="text/javascript">
            <c:if test="${loginFormBean.basicsiteflag != '1'}">    
                location.href = "${pageContext.request.contextPath}/common/main";
            </c:if>
            <c:if test="${loginFormBean.basicsiteflag == '1'}">    
            location.href = "${pageContext.request.contextPath}/paycalender/basic_merchpayCalenderList";
            </c:if>
        </script>
    </c:if>
    <c:if test="${'fail' eq message}">
        <script type="text/javascript">
            alert(">>>>>>>> 로그인 정보를 확인하세요");
        </script>
    </c:if>
</c:if>
<div class="mainlogin" >
    <div id="formsContent" >
        <form id="loginForm" name="loginForm" action='<c:url value="/login/loginForm"/>' method="post" modelAttribute="loginFormBean">

            <div class="loginTitle" > <span>tkon 서비스</span></div>

            <div class="loginInput" >
                <div>
                    <span class="loginLable">ID</span>
                    <input type="text" name="user_id"/>
                </div>
                <div>
                    <span class="loginLable">PWD</span>
                    <input type="password" name="user_pwd"/>
                </div>
                <div style="width:210px;height: 50px;vertical-align:top;">
                   <input type="checkbox" class="loginInput" name="chkbasicsiteflag" id="chkbasicsiteflag" onclick="javascript:chkbasicsite();" style="margin: 0 0 0 0;padding: 0 0 0 0;"><img src="../resources/images/basicsitelogin.gif" width="150" height="30"/>                
                </div>
            </div>            
            <button class="login">로그인</button><input type="hidden" name="basicsiteflag" id="basicsiteflag" value="">
        </form>
    </div>
</div>
        <form name="logindirect" action="${pageContext.request.contextPath}/login/loginForm"></form>

<script type="text/javascript">
    $(document).ready(function () {

        $("#loginForm").submit(function () {
            $.post($(this).attr("action"), $(this).serialize(), function (html) {

                $("#formsContent").replaceWith(html);

            });
            return false;
        });

        $(window).resize();

        $("input[name = user_id]").focus();

        $("#loginForm").find(".login").button()

        .unbind("click")
        .bind("click", function (event) {
            $("#loginForm").submit();
            return false;
        });
    });

    $(window).resize(function () {
        var margins = ($(window).height() - $(".mainlogin").outerHeight()) / 2

        $(".mainlogin").css({
            "margin-top": margins
            , "margin-left": "auto"
            , "margin-right": "auto"
        });
    });


            function chkbasicsite()
            {
                if(document.loginForm.chkbasicsiteflag.checked)
                {                    
                    document.loginForm.basicsiteflag.value = "1";
                }
                else
                {
                    document.loginForm.basicsiteflag.value = "";
                }
            }
</script>
<c:if test="${ajaxRequest}">
    <script type="text/javascript">
        //location.href = "${pageContext.request.contextPath}/login/loginForm";
        document.form.logindirect.submit();
    </script>
 </c:if>
    </body>
    </html>
