<%-- 
    Document   : tidMasterUpdate
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>TID 수정</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">
                
                
                $(document).ready(function () {
                    
                    myCalendar = new dhtmlXCalendarObject(["view_start_dt","view_end_dt"]);
                    
                    //TID 수정 이벤트
                    $("#tidFormUpdate input[name=update]").click(function(){
                       doUpdateTidMaster(); 
                    });               
                    
                    //TID 수정 닫기
                    $("#tidFormUpdate input[name=close]").click(function(){       

                        tidMasterWindow.window("w1").close();
                        
                    });                           
                    
                });
                

                //TID 수정
                function doUpdateTidMaster(){
                    
                    if(!window.confirm("수정하시겠습니까?")){
                        return;
                    }
        
                    if($("#tidFormUpdate input[name=tid_mtd]").val() == "")
                    {
                      alert("결제사는 필수사항 입니다.");
                      $("#tidFormUpdate input[name=tid_mtd]").focus();
                      return;
                    }
                    
                    if($("#tidFormUpdate input[name=tid_nm]").val() == "")
                    {
                      alert("TID명은 필수사항 입니다.");
                      $("#tidFormUpdate input[name=tid_nm]").focus();
                      return;
                    }
                    
                    if($("#tidFormUpdate input[name=tid_no]").val() == "")
                    {
                      alert("TID는 필수사항 입니다.");
                      $("#tidFormUpdate input[name=tid_no]").focus();
                      return;
                    }
                    
                    if($("#tidFormUpdate input[name=view_start_dt]").val() == "")
                    {
                      alert("적용 시작일은 필수사항 입니다.");
                      $("#tidFormUpdate input[name=view_start_dt]").focus();
                      return;
                    }
                    else
                    {
                        var regobj = new RegExp('-',"gi");
                        var strTpStartdt =  $("#tidFormUpdate input[name=view_start_dt]").val();
                        $("#tidFormUpdate input[name=start_dt]").attr('value',strTpStartdt.replace(regobj,''));
                    }
                    
                    if($("#tidFormUpdate input[name=view_end_dt]").val() == "")
                    {
                      alert("적용 종료일은 필수사항 입니다.");
                      $("#tidFormUpdate input[name=view_end_dt]").focus();
                      return;
                    }
                    else
                    {
                        var regobj = new RegExp('-',"gi");
                        var strTpEnddt =  $("#tidFormUpdate input[name=view_end_dt]").val();
                        $("#tidFormUpdate input[name=end_dt]").attr('value',strTpEnddt.replace(regobj,''));
                    }
        
                    $.ajax({
                        url :  "../tid/tidMasterUpdate",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#tidFormUpdate").serialize(),
                        success : function(data) {
                            if(data == '1')
                            {
                                alert("수정 완료");
                                tidMasterGrid.clearAll();
                                doSearchTidMaster();
                                tidMasterWindow.window("w1").close();
                            }
                            else
                            {
                                alert("수정 실패");
                            }
                        },
                        error : function(data) { 
                                alert("수정 실패");
                        }
                    });
        
                }                   

                //TID 삭제
                function doDeleteTidMaster(){
                    
                    if(!window.confirm("삭제하시겠습니까?")){
                        return;
                    }

        
                    $.ajax({
                        url :  "../tid/tidMasterDelete",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#tidFormUpdate").serialize(),
                        success : function(data) {
                            if(data == '1')
                            {
                                alert("삭제 완료");
                                tidMasterGrid.clearAll();
                                doSearchTidMaster();
                                tidMasterWindow.window("w1").close();
                            }
                            else
                            {
                                alert("삭제 실패");
                            }
                        },
                        error : function() {
                                alert("삭제 실패");
                        }
                    });
        
                }                   
            </script>
            <form id="tidFormUpdate" method="POST" action="<c:url value="/tid/tidMasterUpdate" />" modelAttribute="tidFormBean">
                <input type="hidden" name="tid_seq" value="${tb_Tid.tid_seq}" />
                <input type="hidden" name="start_dt" value="${tb_Tid.start_dt}">
                <input type="hidden" name="end_dt" value="${tb_Tid.end_dt}">                
                <table class="gridtable" height="100%" width="100%">                    
                    <tr>
                        <td align='left' colspan='4'>- TID 관리</td>                        
                    </tr>
                    <tr>
                        <td class="headcol" width="25%">결제사*</td>
                        <td >
                            <select name="tid_mtd" >
                                    <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${TidPayMtdList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Tid.tid_mtd}">selected</c:if> >${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td class="headcol" width="25%">TID명*</td>
                        <td><input name="tid_nm" maxlength="50" size="40" value="${tb_Tid.tid_nm}"  onblur="javascript:checkLength(this,50);" /></td>
                    </tr>
                    <tr>
                        <td class="headcol">TID*</td>
                        <td><input name="terminal_no" value="${tb_Tid.terminal_no}" maxlength="20" /></td>                            
                        <td class="headcol" width="25%">TID암호</td>
                        <td><input name="terminal_pwd" maxlength="50" size="40" value="${tb_Tid.terminal_pwd}"  onblur="javascript:checkLength(this,50);" /></td>
                    </tr>
<!--                          
                    <tr>
                        <td class="headcol">일반/무이자구분*</td>
                        <td><select name="func_cate" >
                                <c:forEach var="code" items="${FuncCateList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Tid.func_cate}">selected</c:if> >${code.code_nm}</option>
                                </c:forEach>
                            </select></td>
                        <td class="headcol">자체/대행*</td>
                        <td><select name="agency_flag" >
                                <c:forEach var="code" items="${AgencyFlagList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Tid.agency_flag}">selected</c:if> >${code.code_nm}</option>
                                </c:forEach>
                            </select></td>
                    </tr>
                    <tr>
   
                        <td class="headcol">할부개월(,구분)</tD>
                        <td><input name="installment" value="${tb_Tid.installment}" maxlength="100"/></td>                        
                    </tr>
                    <tr>
                        <td class="headcol">포인트구분(,구분)</td>
                        <td><input name="point_cate" value="${tb_Tid.point_cate}" maxlength="100" /></td>
                        <td class="headcol">수수료*</tD>
                        <td><input name="commision" value="${tb_Tid.commision}" maxlength="20" onkeyPress="javascript:InpuOnlyNumber(this);" /></td>                        
                    </tr>                    
                     <tr>
                        <td class="headcol">첵크카드수수료</td>
                        <td><input name="commision2" value="${tb_Tid.commision2}" maxlength="20" onkeyPress="javascript:InpuOnlyNumber(this);" /></td>
                        <td class="headcol">포인트수수료</tD>
                        <td><input name="point_commsion" value="${tb_Tid.point_commsion}" maxlength="20" onkeyPress="javascript:InpuOnlyNumber(this);" /></td>                        
                    </tr>
-->
                    <tr>
                        <td class="headcol">시작일*</td>
                        <td><input name="view_start_dt" value="${tb_Tid.start_dt}" id="view_start_dt" maxlength="10" readonly/></td>
                        <td class="headcol">종료일</tD>
                        <td><input name="view_end_dt" value="${tb_Tid.end_dt}" id="view_end_dt" maxlength="10"  readonly/></td>
                    </tr>  
<!--                    
                    <tr>
                        <td class="headcol">입금은행*</td>
                        <td><select name="bank_cd" >
                                <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${BankCdList.tb_code_details}">
                                    <option value="${code.detail_code}"  <c:if test="${code.detail_code == tb_Tid.bank_cd}">selected</c:if> >${code.code_nm}</option>
                                </c:forEach>
                            </select></td>
                        <td class="headcol">입금계좌*</tD>
                        <td><input name="acc_no" value="${tb_Tid.acc_no}" maxlength="30" onkeyPress="javascript:InpuOnlyNumber(this);" /></td>                        
                    </tr>
-->
                    <tr>
                        <td class="headcol">사용여부*</td>
                        <td ><select name="use_flag" >
                                <c:forEach var="code" items="${UseFlagList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Tid.use_flag}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach>
                            </select></td>
                        <td class="headcol">당일취소가능여부*</td>
                        <td ><select name="appcnclflag" >
                                <c:forEach var="code" items="${AppcnclflagList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Tid.appcnclflag}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach>
                            </select></td>                             
                    </tr>
                    <tr>
                        <td class="headcol">Memo</td>
                        <td colspan="3"><textarea name="memo" cols="70" rows="5">${tb_Tid.memo}</textarea></td>                        
                    </tr>
                    <tr>
                        <td class="headcol">등록일</td>
                        <td>${tb_Tid.ins_dt}</td>
                        <td class="headcol">등록자</td>
                        <td>${tb_Tid.ins_user}</td>
                    </tr>                          
                    <tr>
                        <td colspan="4" align="center"><input type="button" name="tidbt" value="수정" onclick="javascript:doUpdateTidMaster();"/>&nbsp;<input type="button" name="tiddel" value="삭제" onclick="javascript:doDeleteTidMaster();"/>&nbsp;<input type="button" name="close" value="닫기"/></td>
                    </tr>
                </table>
            </form>
                    <!--
            </div>
                    -->
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

