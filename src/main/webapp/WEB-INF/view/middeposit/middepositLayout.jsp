<%-- 
    Document   : creditMaster
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>입금대사</title>
        </head>
        <body>
</c:if>
<%
    //deposit_start_dt='+mvtargetdt+'&deposit_end_dt
    String strTpdeposit_start_dt = request.getParameter("deposit_start_dt");
    String strTpdeposit_end_dt = request.getParameter("deposit_end_dt");
    String strTpmvparam = "";
    
    if(strTpdeposit_start_dt!=null && strTpdeposit_end_dt!= null)
    {
        strTpmvparam = "?deposit_start_dt="+strTpdeposit_start_dt+"&deposit_end_dt="+strTpdeposit_end_dt;
    }
%>
    <script type="text/javascript">
        
                var main_middepositMaster_layout={};
                $(document).ready(function () {

                    var tabBarId = tabbar.getActiveTab();

                    main_middepositMaster_layout = tabbar.cells(tabBarId).attachLayout("2E");

                    main_middepositMaster_layout.cells('a').hideHeader();
                    main_middepositMaster_layout.cells('b').hideHeader();
                    main_middepositMaster_layout.cells('a').attachURL("<c:url value="/middeposit/middepositMasterList" /><%=strTpmvparam%>",true);               
//                    main_middepositMaster_layout.cells('b').attachURL("<c:url value="/middeposit/middepositDetailList" />",true);  
            });
             
            
    </script>

    </body>
</html>
