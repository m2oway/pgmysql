<%-- 
    Document   : codeDetailMaster
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>입금정산 상세조회</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">

                var middepositdetailgrid={};
                var middepositdetail_layout={};
                
                var middepositDetailCalendar = new dhtmlXCalendarObject(["middepositDetail_from_date","middepositDetail_to_date"]);    

                $(document).ready(function () {

                    var middepositdetailForm = document.getElementById("middepositdetail_search");

                    middepositdetail_layout = main_middepositMaster_layout.cells('b').attachLayout("2E");

                    middepositdetail_layout.cells('a').hideHeader();

                    middepositdetail_layout.cells('b').hideHeader();

                    middepositdetail_layout.cells('a').attachObject(middepositdetailForm);

                    middepositdetail_layout.cells('a').setHeight(60);

                    middepositdetailgrid = middepositdetail_layout.cells('b').attachGrid();

                    middepositdetailgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                

                    var middepositdetailheaders = "";

                    middepositdetailheaders += "<center>NO</center>,<center>결제채널</center>,<center>가맹점번호</center>,<center>거래종류</center>,<center>매입사</center>";
                    middepositdetailheaders += ",<center>승인일</center>,<center>승인번호</center>,<center>승인금액</center>,<center>부가세</center>,<center>할부</center>";
                    middepositdetailheaders += ",<center>매입일</center>,<center>입금예정일</center>,<center>수수료</center>,<center>입금예정금액</center>";
                    
                    middepositdetailgrid.setHeader(middepositdetailheaders);
                    middepositdetailgrid.setColAlign("center,center,center,center,center,center,center,right,right,center,center,center,right,right");
                    middepositdetailgrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,edn,edn,txt,txt,txt,edn,edn");
                    middepositdetailgrid.setInitWidths("50,150,100,100,100,100,100,100,120,100,100,100,100,100");
                    middepositdetailgrid.setColSorting("str,str,str,str,str,str,str,int,int,str,str,str,int,int");
                    middepositdetailgrid.setNumberFormat("0,000",7);            
                    middepositdetailgrid.setNumberFormat("0,000",8);
                    middepositdetailgrid.setNumberFormat("0,000",12);
                    middepositdetailgrid.setNumberFormat("0,000",13);
                    middepositdetailgrid.enableColumnMove(true);
                    middepositdetailgrid.setSkin("dhx_skyblue");
                    middepositdetailgrid.enablePaging(true,Number($("#middepositdetailForm input[name=page_size]").val()),10,"middepositdetailPaging",true,"middepositdetailrecinfoArea");
                    middepositdetailgrid.setPagingSkin("bricks");                    
//                    var middepositdetailgridFilters = ",#select_filter,#select_filter,#text_filter,#select_filter,#text_filter,#text_filter,#numeric_filter,#numeric_filter,#select_filter,#text_filter,#text_filter,#numeric_filter,#numeric_filter";
//                    middepositdetailgrid.attachHeader(middepositdetailgridFilters);                    
                    middepositdetailgrid.init();
                    middepositdetailgrid.parse(${ls_middepositDetailList},"json");
                    
                    //검색조건 초기화
                    $("#middepositdetailForm input[name=init]").click(function () {
                        middepositdetailInit($("#middepositdetailForm"));
                    });  
                    
                    $("#middepositdetailForm input[name=week]").click(function(){
                        middepositdetail_date_search("week");
                    });

                    $("#middepositdetailForm input[name=month]").click(function(){
                        middepositdetail_date_search("month");
                    });

                    //페이징 처리
                    middepositdetailgrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        if(lInd !==0){
                          $("#middepositdetailForm input[name=page_no]").val(ind);             
                           middepositdetailgrid.clearAll();
                           middepositdetailSearch();
                        }else{
                            $("#middepositdetailForm input[name=page_no]").val(1);
                            middepositdetailgrid.clearAll();
                             middepositdetailSearch();
                        }                            
                    });

                    //상세코드검색
                    $("#middepositdetailForm input[name=middepositdetail_search]").click(function(){
                                /*
                        $("input[name=page_no]").val("1");

                        middepositdetailgrid.clearAll();

                        middepositdetailSearch();
                        */
                        middepositdetailgrid.changePage(1);

                    });
                    
                    //엑셀
                    $("#middepositdetailForm input[name=middepositdetail_excel]").click(function () {
                        $("#middepositdetailForm").attr("action","<c:url value="/middeposit/middepositDetailListExcel" />");
                        document.getElementById("middepositdetailForm").submit();
                    });                                    
                    
                });
                
                function middepositdetailInit($form) {
                    searchFormInit($form);
                    middepositdetail_date_search("week");                    
                } 
                
                function middepositdetail_date_search(day){
                var oneDate = 1000 * 3600 * 24; 
                var now = new Date();
                var week = new Date(now.getTime() + (oneDate*-6));
                var month = new Date(now.getTime() + (oneDate*-31));
                var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                var date=new Date();
                if(day=="today"){
                    this.$("input[id=middepositDetail_from_date]").val(nowTime);
                    this.$("input[id=middepositDetail_to_date]").val(nowTime);
                }else if(day=="week"){
                    this.$("input[id=middepositDetail_from_date]").val(weekTime);
                    this.$("input[id=middepositDetail_to_date]").val(nowTime);
                }else{
                    this.$("input[id=middepositDetail_from_date]").val(monthTime);
                    this.$("input[id=middepositDetail_to_date]").val(nowTime);
                }
            }


                //코드 정보 조회
                function middepositdetailSearch() {
                //alert('search !!!');
                $.ajax({
                    url: "<c:url value="/middeposit/middepositDetailList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#middepositdetailForm").serialize(),
                    success: function (data) {

                        var jsonData = $.parseJSON(data);
                        //middepositdetailgrid.clearAll();
                        middepositdetailgrid.parse(jsonData,"json");

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
                    return false;
            }
            
            function setSens(id, k) {
                // update range
                if (k == "min") {
                    middepositDetailCalendar.setSensitiveRange(byId(id).value, null);
                } else {
                    middepositDetailCalendar.setSensitiveRange(null, byId(id).value);
                }
            }

            function byId(id) {
                return document.getElementById(id);
            }                      
            
    </script>
    <body>


        <div class="right" id="middepositdetail_search" style="width:100%;">
             <form id="middepositdetailForm" method="POST" action="<c:url value="/middeposit/middepositDetailList" />" modelAttribute="middepositFormBean">

                        <input type="hidden" name="page_size" value="100" >
                        <input type="hidden" name="page_no" value="1" >
                        
                        <table>
                            <tr>
                                <td>
                                    <div class="label">승인일</div>
                                    <div class="input">
                                        <input type="text" size='15' name="sal_start_dt" id="middepositDetail_from_date" value ="" onclick="setSens('middepositDetail_to_date', 'max');" /> ~
                                        <input type="text" size='15' name="sal_end_dt" id="middepositDetail_to_date" onclick="setSens('middepositDetail_from_date', 'min');" />
                                    </div>
                                    <div class ="input"><input type="button" name="week" value="1주"/></div>
                                    <div class ="input"><input type="button" name="month" value="1달"/></div>
                                    <div class="label">입금일</div>
                                    <div class="input">
                                        <input type="text" name="deposit_dt" id="middepositDetail_from_date" value ="${middepositFormBean.deposit_dt}" onclick="setSens('middepositDetail_to_date', 'max');" />
                                    </div>                                    
                                    <div class="label">결제채널</div>
                                    <div class="input">
                                        <select name="pay_type">
                                            <option value="">전체</option>
                                            <c:forEach var="code" items="${payChnCateList.tb_code_details}">
                                                <option value="${code.detail_code}">${code.code_nm}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                <div class="label">가맹점번호</div>
                                <div class="input"><input type = "text" size='10' name="merch_no" value ="${middepositFormBean.merch_no}" /></div>                                
                                <div class="label">승인번호</div>
                                <div class="input"><input type = "text" size='10' name="aprv_no" /></div>
                                <div class="label">승인금액</div>
                                <div class="input"><input type = "text" size='10' name="tot_trx_amt" /></div>
                                <div class="label">매입사</div>
                                <div class="input">
                                    <select name="acq_cd">
                                        <option value="">전체</option>
                                        <c:forEach var="code" items="${acqCdList.tb_code_details}">
                                            <option value="${code.detail_code}">${code.code_nm}</option>
                                        </c:forEach>
                                    </select>
                                </div>                      
                            </td>                                   
                                <td >
                                    <input type="button" name="middepositdetail_search" value="조회"/>
                                </td>
                                <td >
                                    <input type="button" name="middepositdetail_excel" value="엑셀"/>
                                </td>
                                 <td><input type="button" name="init" value="검색조건삭제"/></td>
                            </tr>
                        </table>
             </form>

            <div class="paging">
                <div id="middepositdetailPaging" style="width: 50%;"></div>
                <div id="middepositdetailrecinfoArea" style="width: 50%;"></div>
            </div>

        </div>
        
    </body>
</html>
