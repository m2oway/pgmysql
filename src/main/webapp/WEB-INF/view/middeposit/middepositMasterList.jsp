<%-- 
    Document   : middepositMasterList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>입금정산 메인</title>             
        </head>
        <body>
</c:if>
       
    <body>
        <div class="right" id="middepositMaster_search" style="width:100%;">       
            <form id="middepositMasterForm" method="POST" onsubmit="return false" action="<c:url value="/middeposit/middepositMasterList" />" modelAttribute="middepositFormBean"> 
                <table>
                    <tr>
                        <td>
                            <div class="label">검색일자</div>
                            <div class="input">
                                <input type="text" name="deposit_start_dt" id="middepositMaster_from_date" value ="${middepositFormBean.deposit_start_dt}" onclick="midDepositListsetSens('middepositMaster_to_date', 'max');" /> ~
                                <input type="text" name="deposit_end_dt" id="middepositMaster_to_date" value ="${middepositFormBean.deposit_end_dt}" onclick="midDepositListsetSens('middepositMaster_from_date', 'min');" />
                            </div>
                            <div class ="input"><input type="button" name="week" value="1주"/></div>
                            <div class ="input"><input type="button" name="month" value="1달"/></div>
                            
                            <div class="label2">가맹점번호구분</div>
                            <div class="input">
                                <select name="pay_mtd">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${midPayMtdList.tb_code_details}">
                                        <option value="${code.detail_code}">${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="label">가맹점번호</div>
                            <div class="input"><input type="text" name="merch_no" /></div> 
                            <div class="label">입금대사구분</div>
                            <div class="input">
                                <select name="proc_flag">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${procFlagList.tb_code_details}">
                                        <option value="${code.detail_code}">${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>      
                        </td>
                        <td><input type="button" name="middepositMaster_search" value="조회"/></td>
                        <td><input type="button" name="middepositMaster_excel" value="엑셀"/></td>
                        <td><input type="button" name="init" value="검색조건삭제"/></td>
                    </tr>
               </table>
            </form>
        </div>
        <div id="middepositMaster" style="position: relative;width:100%;height:100%;background-color:white;"></div>
        
        
         <script type="text/javascript">
            
            var middepositMastergrid={};
            var middepositMasterCalendar;
            var outamtWindows={};
            var depositMetabolismWindow;
            
            $(document).ready(function () {
                
                var middepositMaster_searchForm = document.getElementById("middepositMaster_search");
                
                middepositMaster_layout = main_middepositMaster_layout.cells('a').attachLayout("2E");
                     
                middepositMaster_layout.cells('a').hideHeader();
                middepositMaster_layout.cells('b').hideHeader();
                 
                middepositMaster_layout.cells('a').attachObject(middepositMaster_searchForm);
                middepositMaster_layout.cells('a').setHeight(20);
                
                middepositMastergrid = middepositMaster_layout.cells('b').attachGrid();
                
                middepositMastergrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                
                var middepositMasterHeaders = "";
                middepositMasterHeaders += "<center>입금일</center>,<center>가맹점번호구분</center>,<center>가맹점번호</center>,<center>가맹점명</center>,<center>입금예정액</center>,<center>실입금액</center>,<center>오차액</center>,<center>미수액</center>,<center>매입확정금액</center>,<center>수수료</center>,<center>onoff가맹점지급액</center>,<center>대리점수수료</center>,<center>영업이익금</center>,<center>대사처리</center>";
                middepositMastergrid.setHeader(middepositMasterHeaders);
                
                middepositMastergrid.attachHeader("#select_filter,#select_filter,#select_filter,#select_filter,&nbsp,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan,#cspan");                
                
                middepositMastergrid.setColAlign("center,center,center,center,right,right,right,right,right,right,right,right,right,center");
                middepositMastergrid.setColTypes("txt,txt,txt,txt,edn,edn,txt,txt,edn,edn,edn,edn,edn,txt");
                middepositMastergrid.setInitWidths("100,100,100,150,100,100,100,100,100,100,120,100,100,100");
                middepositMastergrid.setColSorting("str,str,str,str,int,int,str,int,int,int,int,int,int,str");
                
                middepositMastergrid.setNumberFormat("0,000",4);
                middepositMastergrid.setNumberFormat("0,000",5);
                
                
                middepositMastergrid.setNumberFormat("0,000",6);
                middepositMastergrid.setNumberFormat("0,000",7);
                middepositMastergrid.setNumberFormat("0,000",8);
                middepositMastergrid.setNumberFormat("0,000",9);
                
                middepositMastergrid.setNumberFormat("0,000",10);
                middepositMastergrid.setNumberFormat("0,000",11);
                middepositMastergrid.setNumberFormat("0,000",12);
                
                middepositMastergrid.enableColumnMove(true);
                
                
                var middepositsub_mfooters_filter = "소계,#cspan,#cspan,#cspan";
                middepositsub_mfooters_filter += ",<div id='middepositMasterSubSum4'>0</div>,<div id='middepositMasterSubSum5'>0</div>,<div id='middepositMasterSubSum6'>0</div>,<div id='middepositMasterSubSum7'>0</div>,<div id='middepositMasterSubSum8'>0</div>,<div id='middepositMasterSubSum9'>0</div>";
                middepositsub_mfooters_filter += ",<div id='middepositMasterSubSum10'>0</div>,<div id='middepositMasterSubSum11'>0</div>,<div id='middepositMasterSubSum12'>0</div>";
                middepositsub_mfooters_filter += ",&nbsp";                
	        middepositMastergrid.attachFooter(middepositsub_mfooters_filter);
                
                
                var middepositMasterfooters = "총계,#cspan,#cspan,#cspan";
                middepositMasterfooters += ",<div id='middepositMasterSum4'>0</div>,<div id='middepositMasterSum5'>0</div>,<div id='middepositMasterSum6'>0</div>,<div id='middepositMasterSum7'>0</div>,<div id='middepositMasterSum8'>0</div>,<div id='middepositMasterSum9'>0</div>";
                middepositMasterfooters += ",<div id='middepositMasterSum10'>0</div>,<div id='middepositMasterSum11'>0</div>,<div id='middepositMasterSum12'>0</div>";
                middepositMasterfooters += ",&nbsp";
                
                middepositMastergrid.attachFooter(middepositMasterfooters);  
                
                
                middepositMastergrid.setSkin("dhx_skyblue");
                middepositMastergrid.init();
                middepositMastergrid.attachEvent("onRowDblClicked", middepositMaster_attach);
                middepositMastergrid.parse(${ls_middepositMasterList},middepositMasterFooterValues,"json");
                
                 
                middepositMastergrid.attachEvent("onGridReconstructed", function(){
	  	          
                        var middepositMaster_sub4 = document.getElementById("middepositMasterSubSum4");
                        var middepositMaster_sub5 = document.getElementById("middepositMasterSubSum5");
                        var middepositMaster_sub6 = document.getElementById("middepositMasterSubSum6");
                        var middepositMaster_sub7 = document.getElementById("middepositMasterSubSum7");
                        var middepositMaster_sub8 = document.getElementById("middepositMasterSubSum8");
                        var middepositMaster_sub9 = document.getElementById("middepositMasterSubSum9");
                        var middepositMaster_sub10 = document.getElementById("middepositMasterSubSum10");
                        var middepositMaster_sub11 = document.getElementById("middepositMasterSubSum11");
                        var middepositMaster_sub12 = document.getElementById("middepositMasterSubSum12");                                               
	            	
	            	var rowId = middepositMastergrid.getAllRowIds();
	            	
                        var middepositMaster_subsum_4 = 0;
                        var middepositMaster_subsum_5 = 0;
                        var middepositMaster_subsum_6 = 0;
                        var middepositMaster_subsum_7 = 0;
                        var middepositMaster_subsum_8 = 0;
                        var middepositMaster_subsum_9 = 0;
                        var middepositMaster_subsum_10 = 0;
                        var middepositMaster_subsum_11 = 0;
                        var middepositMaster_subsum_12 = 0;

                        
	    			if(rowId != ''){
	    				var rowIdArr = rowId.split(",");
	    				for(var i=0;i<rowIdArr.length;i++){                                                
                                                                    middepositMaster_subsum_4  += Number(middepositMastersub_removetag(middepositMastergrid.cells(rowIdArr[i],4).getValue()));
                                                                    middepositMaster_subsum_5  += Number(middepositMastersub_removetag(middepositMastergrid.cells(rowIdArr[i],5).getValue()));
                                                                    middepositMaster_subsum_6  += Number(middepositMastersub_removetag(middepositMastergrid.cells(rowIdArr[i],6).getValue()));
                                                                    middepositMaster_subsum_7  += Number(middepositMastersub_removetag(middepositMastergrid.cells(rowIdArr[i],7).getValue()));
                                                                    middepositMaster_subsum_8  += Number(middepositMastersub_removetag(middepositMastergrid.cells(rowIdArr[i],8).getValue()));
                                                                    middepositMaster_subsum_9  += Number(middepositMastersub_removetag(middepositMastergrid.cells(rowIdArr[i],9).getValue()));
                                                                    middepositMaster_subsum_10 += Number(middepositMastersub_removetag(middepositMastergrid.cells(rowIdArr[i],10).getValue()));
                                                                    middepositMaster_subsum_11 += Number(middepositMastersub_removetag(middepositMastergrid.cells(rowIdArr[i],11).getValue()));
                                                                    middepositMaster_subsum_12 += Number(middepositMastersub_removetag(middepositMastergrid.cells(rowIdArr[i],12).getValue()));                                                                    
	    				}
	    			}
	    			
                                    middepositMaster_sub4.innerHTML =  putComma(middepositMaster_subsum_4);
                                    middepositMaster_sub5.innerHTML =  putComma(middepositMaster_subsum_5);
                                    middepositMaster_sub6.innerHTML =  putComma(middepositMaster_subsum_6);
                                    middepositMaster_sub7.innerHTML =  putComma(middepositMaster_subsum_7);
                                    middepositMaster_sub8.innerHTML =  putComma(middepositMaster_subsum_8);
                                    middepositMaster_sub9.innerHTML =  putComma(middepositMaster_subsum_9);
                                    middepositMaster_sub10.innerHTML =  putComma(middepositMaster_subsum_10);
                                    middepositMaster_sub11.innerHTML =  putComma(middepositMaster_subsum_11);
                                    middepositMaster_sub12.innerHTML =  putComma(middepositMaster_subsum_12);
                                    				    
				    return true;
	            	
	            });                                 
                
                
                
                depositMetabolismWindow = new dhtmlXWindows();
                
                //검색조건 초기화
                $("#middepositMasterForm input[name=init]").click(function () {

                    middepositMasterInit($("#middepositMasterForm"));

                });  
                
                $("#middepositMasterForm input[name=week]").click(function(){
                    midDeposit_date_search("week");
                });

                $("#middepositMasterForm input[name=month]").click(function(){
                    midDeposit_date_search("month");
                });


                //검색 이벤트
                $("#middepositMasterForm input[name=middepositMaster_search]").click(function () {
                    middepositMasterSearch();
                });
                
                //엑셀
                $("#middepositMasterForm input[name=middepositMaster_excel]").click(function () {
                    $("#middepositMasterForm").attr("action","<c:url value="/middeposit/middepositMasterListExcel" />");
                    document.getElementById("middepositMasterForm").submit();
                });                
                
                middepositMasterCalendar = new dhtmlXCalendarObject(["middepositMaster_from_date","middepositMaster_to_date"]);
                
                $("button.procButton").button({
                    text:true
                })
                .unbind("click")
                .bind("click", function (e) {

                    if (confirm("입금확정을 하시겠습니까?") == true){
                        var rowId = getRowId($(this));
                        depositMetabolismConfirmProcess(rowId);
                    }

                });


                $("button.procCancelButton").button({
                    text:true
                })
                .unbind("click")
                .bind("click", function (e) {

                    if (confirm("확정취소를 하시겠습니까?") == true){
                        var rowId = getRowId($(this));

                        depositMetabolismCancelProcess(rowId);
                    }

                });


                $("span.outAmt,span.outAmtProc").click(function(){
                    
                    var rowId = getRowId($(this));

                    depositMetabolismOutAmtWindowPop(rowId);

                });

                $("span.misAmt").click(function(){

                    var rowId = getRowId($(this));

                    depositMetabolismMisAmtWindowPop(rowId);

                });                
                    
            });
            
            function middepositMasterInit($form) {
                searchFormInit($form);
                midDeposit_date_search("week");                    
            } 
            
            //상세조회 이벤트
            function middepositMaster_attach(rowid, col) {

              var deposit_dt = middepositMastergrid.getUserData(rowid, 'deposit_dt');
              var merch_no = middepositMastergrid.getUserData(rowid, 'merch_no');
              
              main_middepositMaster_layout.cells('b').attachURL("<c:url value="/middeposit/middepositDetailList" />"+"?deposit_dt=" + deposit_dt + "&merch_no="+merch_no+"&page_size=100", true);
              
            }
            
            //검색
            function middepositMasterSearch() {
                
                $.ajax({
                    url: "<c:url value="/middeposit/middepositMasterList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#middepositMasterForm").serialize(),
                    success: function (data) {
                        
                        var jsonData = $.parseJSON(data);
                        middepositMastergrid.clearAll();
                        middepositMastergrid.parse(jsonData,middepositMasterFooterValues,"json");
                        
                        $("button.procButton").button({
                            text:true
                        })
                        .unbind("click")
                        .bind("click", function (e) {

                            if (confirm("입금확정을 하시겠습니까?") == true){
                                var rowId = getRowId($(this));
                                depositMetabolismConfirmProcess(rowId);
                            }

                        });


                        $("button.procCancelButton").button({
                            text:true
                        })
                        .unbind("click")
                        .bind("click", function (e) {

                            if (confirm("확정취소를 하시겠습니까?") == true){
                                var rowId = getRowId($(this));

                                depositMetabolismCancelProcess(rowId);
                            }

                        });


                        $("span.outAmt,span.outAmtProc").click(function(){

                            var rowId = getRowId($(this));

                            depositMetabolismOutAmtWindowPop(rowId);

                        });

                        $("span.misAmt").click(function(){

                            var rowId = getRowId($(this));

                            depositMetabolismMisAmtWindowPop(rowId);

                        });                                 
                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }
            
            function midDeposit_date_search(day){
                var oneDate = 1000 * 3600 * 24; 
                var now = new Date();
                var week = new Date(now.getTime() + (oneDate*-6));
                var month = new Date(now.getTime() + (oneDate*-31));
                var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                var date=new Date();
                if(day=="today"){
                    this.$("input[id=middepositMaster_from_date]").val(nowTime);
                    this.$("input[id=middepositMaster_to_date]").val(nowTime);
                }else if(day=="week"){
                    this.$("input[id=middepositMaster_from_date]").val(weekTime);
                    this.$("input[id=middepositMaster_to_date]").val(nowTime);
                }else{
                    this.$("input[id=middepositMaster_from_date]").val(monthTime);
                    this.$("input[id=middepositMaster_to_date]").val(nowTime);
                }
            }
            
            function midDepositListsetSens(id, k) {
                // update range
                if (k == "min") {
                    middepositMasterCalendar.setSensitiveRange(byId(id).value, null);
                } else {
                    middepositMasterCalendar.setSensitiveRange(null, byId(id).value);
                }
            }

            function byId(id) {
                return document.getElementById(id);
            }              
            
            function depositMetabolismOutAmtWindowPop(rowId){

                var dhtmlxwindowSize = {};
                dhtmlxwindowSize.width = 1000;
                dhtmlxwindowSize.height = 300;

                var pay_mtd = middepositMastergrid.getUserData(rowId,"pay_mtd");
                var merch_no = middepositMastergrid.getUserData(rowId,"merch_no");
                var deposit_dt = middepositMastergrid.getUserData(rowId,"deposit_dt");

                var location = dhxwindowCenterAlgin(dhtmlxwindowSize);

                outamtWindows = depositMetabolismWindow.createWindow("outamtListWindow", location.x, location.y, dhtmlxwindowSize.width, dhtmlxwindowSize.height);

                outamtWindows.setText("미수금 리스트");

                depositMetabolismWindow.window("outamtListWindow").setModal(true);

                outamtWindows.attachURL("<c:url value="/outamt/outamtDetailListWindow" />" + "?pay_mtd="+pay_mtd+"&merch_no="+merch_no+"&gen_dt="+deposit_dt+"&deposit_popup_yn=Y",true);

            }
            
            function depositMetabolismMisAmtWindowPop(rowId){
                
                var dhtmlxwindowSize = {};
                dhtmlxwindowSize.width = 950;
                dhtmlxwindowSize.height = 250;

                var pay_mtd = middepositMastergrid.getUserData(rowId,"pay_mtd");
                var merch_no = middepositMastergrid.getUserData(rowId,"merch_no");
                var deposit_dt = middepositMastergrid.getUserData(rowId,"deposit_dt");

                var location = dhxwindowCenterAlgin(dhtmlxwindowSize);

                outamtWindows = depositMetabolismWindow.createWindow("outamtListWindow", location.x, location.y, dhtmlxwindowSize.width, dhtmlxwindowSize.height);

                outamtWindows.setText("미수금 등록");

                depositMetabolismWindow.window("outamtListWindow").setModal(true);

                outamtWindows.attachURL("<c:url value="/outamt/outamtMasterInsert" />" + "?pay_mtd="+pay_mtd+"&merch_no="+merch_no+"&gen_dt="+deposit_dt+"&deposit_popup_yn=Y",true);

            }                      

            function depositMetabolismConfirmProcess(rowId){
                
                var deposit_object ={};
                deposit_object.seq = middepositMastergrid.getUserData(rowId,"seq");
                deposit_object.deposit_dt = middepositMastergrid.getUserData(rowId,"deposit_dt");
                deposit_object.merch_no = middepositMastergrid.getUserData(rowId,"merch_no");
                deposit_object.proc_flag = middepositMastergrid.getUserData(rowId,"proc_flag");                
                
                $.ajax({
                    url : "<c:url value="/middeposit/depositMetabolismConfirmProcess" />",
                    type : "POST",
                    async : true,
                    dataType : "json",
                    data: {
                             seq:deposit_object.seq
                            ,deposit_dt:deposit_object.deposit_dt
                            ,merch_no:deposit_object.merch_no
                            ,proc_flag:deposit_object.proc_flag
                    },
                    success : function(data) {
                        if(data ==="success"){
                            alert("입금확정 성공");

                            middepositMastergrid.clearAll();
                            middepositMasterSearch();

                        }else{
                            alert("입금확정 실패");
                        }
                    },
                    error : function(jqXHR, textStatus, errorThrown) {
                        alert("입금확정 실패 : " + errorThrown);

                    }
                });

            }

            function depositMetabolismCancelProcess(rowId){
            
                var deposit_object ={};
                deposit_object.seq = middepositMastergrid.getUserData(rowId,"seq");
                deposit_object.deposit_dt = middepositMastergrid.getUserData(rowId,"deposit_dt");
                deposit_object.merch_no = middepositMastergrid.getUserData(rowId,"merch_no");
                deposit_object.proc_flag = middepositMastergrid.getUserData(rowId,"proc_flag");                

                $.ajax({
                    url : "<c:url value="/middeposit/depositMetabolismCancelProcess" />",
                    type : "POST",
                    async : true,
                    dataType : "json",
                    data: {
                            seq:deposit_object.seq
                           ,deposit_dt:deposit_object.deposit_dt
                           ,merch_no:deposit_object.merch_no
                           ,proc_flag:deposit_object.proc_flag
                    },
                    success : function(data) {
                        if(data ==="success"){
                            alert("확정취소 성공");
                            middepositMastergrid.clearAll();
                            middepositMasterSearch();
                        }else{
                            alert("확정취소 실패");
                        }
                    },
                    error : function(jqXHR, textStatus, errorThrown) {
                        alert("확정취소 실패 : " + errorThrown);

                    }
                });

            }      
            

            function middepositMasterFooterValues() {
                
                var middepositMaster4 = document.getElementById("middepositMasterSum4");
                var middepositMaster5 = document.getElementById("middepositMasterSum5");
                var middepositMaster6 = document.getElementById("middepositMasterSum6");
                var middepositMaster7 = document.getElementById("middepositMasterSum7");
                var middepositMaster8 = document.getElementById("middepositMasterSum8");
                var middepositMaster9 = document.getElementById("middepositMasterSum9");
                var middepositMaster10 = document.getElementById("middepositMasterSum10");
                var middepositMaster11 = document.getElementById("middepositMasterSum11");
                var middepositMaster12 = document.getElementById("middepositMasterSum12");
                
                middepositMaster4.innerHTML =  putComma(middepositMastersumColumn(4)) ;
                middepositMaster5.innerHTML =  putComma(middepositMastersumColumn(5)) ;
                middepositMaster6.innerHTML =  putComma(middepositMastersumColumn(6)) ;
                middepositMaster7.innerHTML =  putComma(middepositMastersumColumn(7)) ;
                middepositMaster8.innerHTML =  putComma(middepositMastersumColumn(8)) ;
                middepositMaster9.innerHTML =  putComma(middepositMastersumColumn(9)) ;
                middepositMaster10.innerHTML =  putComma(middepositMastersumColumn(10)) ;
                middepositMaster11.innerHTML =  putComma(middepositMastersumColumn(11)) ;
                middepositMaster12.innerHTML =  putComma(middepositMastersumColumn(12)) ;
                
                return true;

            }  
            
            
            function middepositMastersub_removetag (target_val) 
            {
                var returnval;
            	if(target_val.indexOf("</span>") != -1)
                {
                    returnval= target_val.substring(target_val.indexOf(">")+1,target_val.indexOf("</span>")).replace(/[\',']/gi,"");
                }
                else
                {
                    returnval = target_val.replace(/[\',']/gi,"");
                }
                
                return returnval;
            }                  
            
            function middepositMastersumColumn(ind) {
               	var out = 0;
	    	for (var i = 0; i < middepositMastergrid.getRowsNum(); i++) 
                {
                    
                    if(ind == 6 || ind == 7)
                    {
                        if(middepositMastergrid.cells2(i, ind).getValue().indexOf("</span>") != -1)
                        {
                            out += parseFloat(middepositMastergrid.cells2(i, ind).getValue().substring(middepositMastergrid.cells2(i, ind).getValue().indexOf(">")+1,middepositMastergrid.cells2(i, ind).getValue().indexOf("</span>")).replace(/[\',']/gi,""));
                        }
                        else
                        {
                            out += parseFloat(middepositMastergrid.cells2(i, ind).getValue().replace(/[\',']/gi,""));
                        }
                        
	    	    }
                    else
                    {
                        out += parseFloat(middepositMastergrid.cells2(i, ind).getValue().replace(/[\',']/gi,""));
	    	    }
	    	}
	    	return out;
            }
            
            
            function getRowId($tag){
            /*
                var rowId = 0;

                if(middepositMastergrid.pagingOn){
                    rowId =Number((middepositMastergrid.currentPage -1) * Number(middepositMastergrid.rowsBufferOutSize))+Number($tag.parent().parent().index()-1);
                }else{
                    rowId = Number($tag.parent().parent().index()-1);
                }
                return rowId;
            */    
               
               var rowid = 0;
               rowid = Number($tag.attr("row_id"));
               return rowid;                
                
            }            
                
        </script>
    </body>
</html>
