<%-- 
    Document   : midMasterUpdate
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>가맹점번호 수정</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">
                
                
                $(document).ready(function () {
                    
                    myCalendar = new dhtmlXCalendarObject(["view_start_dt","view_end_dt"]);
                    
                    //가맹점번호 수정 이벤트
                    $("#midFormUpdate input[name=update]").click(function(){
                       doUpdateMidMaster(); 
                    });               
                    
                    //가맹점번호 수정 닫기
                    $("#midFormUpdate input[name=close]").click(function(){       

                        midMasterWindow.window("w1").close();
                        
                    });                           
                    
                });
                

                //가맹점번호 수정
                function doUpdateMidMaster(){
                    
                    if(!window.confirm("수정하시겠습니까?")){
                        return;
                    }
        
                    if($("#midFormUpdate input[name=pay_mtd]").val() == "")
                    {
                      alert("가맹점번호 구분은 필수사항 입니다.");
                      $("#midFormUpdate input[name=pay_mtd]").focus();
                      return;
                    }
                    
                    if($("#midFormUpdate input[name=mid_nm]").val() == "")
                    {
                      alert("가맹점번호명은 필수사항 입니다.");
                      $("#midFormUpdate input[name=mid_nm]").focus();
                      return;
                    }
                    
                    if($("#midFormUpdate input[name=merch_no]").val() == "")
                    {
                      alert("가맹점번호는 필수사항 입니다.");
                      $("#midFormUpdate input[name=merch_no]").focus();
                      return;
                    }
                    
                    if($("#midFormUpdate input[name=commision]").val() == "")
                    {
                      alert("수수료는 필수사항 입니다.");
                      $("#midFormUpdate input[name=commision]").focus();
                      return;
                    }
                    
                    if($("#midFormUpdate input[name=bank_cd]").val() == "")
                    {
                      alert("입금은행은 필수사항 입니다.");
                      $("#midFormUpdate input[name=bank_cd]").focus();
                      return;
                    }
                    
                    if($("#midFormUpdate input[name=acc_no]").val() == "")
                    {
                      alert("입금계좌는 필수사항 입니다.");
                      $("#midFormUpdate input[name=acc_no]").focus();
                      return;
                    }
        
                    if($("#midFormUpdate input[name=view_start_dt]").val() == "")
                    {
                      alert("적용 시작일은 필수사항 입니다.");
                      $("#midFormUpdate input[name=view_start_dt]").focus();
                      return;
                    }
                    else
                    {
                        var regobj = new RegExp('-',"gi");
                        var strTpStartdt =  $("#midFormUpdate input[name=view_start_dt]").val();
                        $("#midFormUpdate input[name=start_dt]").attr('value',strTpStartdt.replace(regobj,''));
                    }
                    
                    if($("#midFormUpdate input[name=view_end_dt]").val() == "")
                    {
                      alert("적용 종료일은 필수사항 입니다.");
                      $("#midFormUpdate input[name=view_end_dt]").focus();
                      return;
                    }
                    else
                    {
                        var regobj = new RegExp('-',"gi");
                        var strTpEnddt =  $("#midFormUpdate input[name=view_end_dt]").val();
                        $("#midFormUpdate input[name=end_dt]").attr('value',strTpEnddt.replace(regobj,''));
                    }
        
                     $.ajax({
                        url :  "../mid/midMasterUpdate",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#midFormUpdate").serialize(),
                        success : function(data) {
                            if(data == '1')
                            {
                                alert("수정 완료");
                                midMasterGrid.clearAll();
                                doSearchMidMaster();
                                midMasterWindow.window("w1").close();
                            }
                            else
                            {
                                alert("수정 실패");
                            }
                        },
                        error : function(data) { 
                                alert("수정 실패");
                        }
                    });
                }                   
                
                
                //가맹점번호 수정
                function doDeleteMidMaster(){
                    
                    if(!window.confirm("삭제하시겠습니까?")){
                        return;
                    }
        
                     $.ajax({
                        url :  "../mid/midMasterDelete",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#midFormUpdate").serialize(),
                        success : function(data) {
                            if(data == '1')
                            {
                                alert("삭제 완료");
                                midMasterGrid.clearAll();
                                doSearchMidMaster();
                                midMasterWindow.window("w1").close();
                            }
                            else
                            {
                                alert("삭제 실패");
                            }
                        },
                        error : function(data) { 
                                alert("삭제 실패");
                        }
                    });


                    /*
                    $("#midFormUpdate").submit(function() {
                        $.post($(this).attr("action"), $(this).serialize(), function(html) {
                            alert("수정 완료");
                            midMasterGrid.clearAll();
                            doSearchMidMaster();
                            midMasterWindow.window("w1").close();
                        });
                    return false;  
                    });	   
                    */
               }

            </script>
            <form id="midFormUpdate" method="POST" action="<c:url value="/mid/midMasterUpdate" />" modelAttribute="midFormBean">
                <input type="hidden" name="merchant_seq" value="${tb_Mid.merchant_seq}" />
                <input type="hidden" name="start_dt" value="${tb_Mid.start_dt}">
                <input type="hidden" name="end_dt" value="${tb_Mid.end_dt}">                
                <table class="gridtable" height="100%" width="100%">                    
                    <tr>
                        <td align='left' colspan='4'>- 가맹점번호 관리</td>                        
                    </tr>
                    <tr>
                        <td class="headcol" width="25%">가맹점번호 구분*</td>
                        <td>
                            <select name="pay_mtd" >
                                    <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${MidPayMtdList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Mid.pay_mtd}">selected</c:if> >${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td class="headcol" width="25%">가맹점번호명*</td>
                        <td><input name="mid_nm" maxlength="50" value="${tb_Mid.mid_nm}"  onblur="javascript:checkLength(this,50);" /></td>
                    </tr>
                    <tr>
                        <td class="headcol">일반/무이자구분*</td>
                        <td><select name="func_cate" >
                                <c:forEach var="code" items="${FuncCateList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Mid.func_cate}">selected</c:if> >${code.code_nm}</option>
                                </c:forEach>
                            </select></td>
                        <td class="headcol">자체/대행*</td>
                        <td><select name="agency_flag" >
                                <c:forEach var="code" items="${AgencyFlagList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Mid.agency_flag}">selected</c:if> >${code.code_nm}</option>
                                </c:forEach>
                            </select></td>
                    </tr>
                    <tr>
                        <td class="headcol">가맹점번호*</td>
                        <td><input name="merch_no" value="${tb_Mid.merch_no}" maxlength="20" /></td>
                        <td class="headcol">할부개월(,구분)</tD>
                        <td><input name="installment" value="${tb_Mid.installment}" maxlength="100"/></td>                        
                    </tr>
                    <tr>
                        <td class="headcol">포인트구분(,구분)</td>
                        <td><input name="point_cate" value="${tb_Mid.point_cate}" maxlength="100" /></td>
                        <td class="headcol">수수료*</tD>
                        <td><input name="commision" value="${tb_Mid.commision}" maxlength="20"  /></td>                        
                    </tr>                    
                     <tr>
                        <td class="headcol">첵크카드수수료</td>
                        <td><input name="commision2" value="${tb_Mid.commision2}" maxlength="20"  /></td>
                        <td class="headcol">포인트수수료</tD>
                        <td><input name="point_commsion" value="${tb_Mid.point_commsion}" maxlength="20" onkeyPress="javascript:InpuOnlyNumber(this);" /></td>                        
                    </tr>
                    <tr>
                        <td class="headcol">시작일*</td>
                        <td><input name="view_start_dt" value="${tb_Mid.start_dt}" id="view_start_dt" maxlength="10" readonly/></td>
                        <td class="headcol">종료일</tD>
                        <td><input name="view_end_dt" value="${tb_Mid.end_dt}" id="view_end_dt" maxlength="10"  readonly/></td>
                    </tr>                    
                    <tr>
                        <td class="headcol">입금은행*</td>
                        <td><select name="bank_cd" >
                                <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${BankCdList.tb_code_details}">
                                    <option value="${code.detail_code}"  <c:if test="${code.detail_code == tb_Mid.bank_cd}">selected</c:if> >${code.code_nm}</option>
                                </c:forEach>
                            </select></td>
                        <td class="headcol">입금계좌*</tD>
                        <td><input name="acc_no" value="${tb_Mid.acc_no}" maxlength="30" onkeyPress="javascript:InpuOnlyNumber(this);" /></td>                        
                    </tr>
                    <tr>
                        <td class="headcol">사용여부*</td>
                        <td colspan='3'><select name="use_flag" >
                                <c:forEach var="code" items="${UseFlagList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Mid.use_flag}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach>
                            </select></td>                        
                    </tr>
                    <tr>
                        <td class="headcol">Memo</td>
                        <td colspan="3"><textarea name="memo" cols="70" rows="5">${tb_Mid.memo}</textarea></td>                        
                    </tr>
                    <tr>
                        <td class="headcol">등록일</td>
                        <td>${tb_Mid.ins_dt}</td>
                        <td class="headcol">등록자</td>
                        <td>${tb_Mid.ins_user}</td>
                    </tr>                          
                    <tr>
                        <td colspan="4" align="center"><input type="button" name="midbt" value="수정" onclick="javascript:doUpdateMidMaster();"/>&nbsp;<input type="button" name="middel" value="삭제" onclick="javascript:doDeleteMidMaster();"/>&nbsp;<input type="button" name="close" value="닫기"/></td>
                    </tr>
                </table>
            </form>
                    <!--
            </div>
                    -->
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

