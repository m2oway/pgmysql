<%-- 
    Document   : midMasterInsert
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>가맹점번호 등록</title>
        </head>
        <body>
</c:if>
<%
    String strCurDate = com.onoffkorea.system.common.util.Util.getTodayDate();
    String strTpDefaultEndDt = "20991231";
%>
            <script type="text/javascript">
                               
                $(document).ready(function () {
                    
                    myCalendar = new dhtmlXCalendarObject(["view_start_dt","view_end_dt"]);
                    
                    
                    //mid 정보 등록 이벤트
                    $("#midFormInsert input[name=insert]").click(function(){
                       doInsertMidMaster(); 
                    });              
                    
                    //mid 정보 등록 닫기
                    $("#midFormInsert input[name=close]").click(function(){
                        midMasterWindow.window("w1").close();
                    });                            
                    
                });

                //사업체 정보 등록
                function doInsertMidMaster(){
                    
                    if(!window.confirm("등록하시겠습니까?")){
                        return;
                    }

                    if($("#midFormInsert input[name=pay_mtd]").val() == "")
                    {
                      alert("가맹점번호 구분은 필수사항 입니다.");
                      $("#midFormInsert input[name=pay_mtd]").focus();
                      return;
                    }
                    
                    if($("#midFormInsert input[name=mid_nm]").val() == "")
                    {
                      alert("가맹점번호명은 필수사항 입니다.");
                      $("#midFormInsert input[name=mid_nm]").focus();
                      return;
                    }
                    
                    if($("#midFormInsert input[name=merch_no]").val() == "")
                    {
                      alert("가맹점번호는 필수사항 입니다.");
                      $("#midFormInsert input[name=merch_no]").focus();
                      return;
                    }
                    
                    if($("#midFormInsert input[name=commision]").val() == "")
                    {
                      alert("수수료는 필수사항 입니다.");
                      $("#midFormInsert input[name=commision]").focus();
                      return;
                    }
                    
                    if($("#midFormInsert input[name=bank_cd]").val() == "")
                    {
                      alert("입금은행은 필수사항 입니다.");
                      $("#midFormInsert input[name=bank_cd]").focus();
                      return;
                    }
                    
                    if($("#midFormInsert input[name=acc_no]").val() == "")
                    {
                      alert("입금계좌는 필수사항 입니다.");
                      $("#midFormInsert input[name=acc_no]").focus();
                      return;
                    }
        
                    if($("#midFormInsert input[name=view_start_dt]").val() == "")
                    {
                      alert("적용 시작일은 필수사항 입니다.");
                      $("#midFormInsert input[name=view_start_dt]").focus();
                      return;
                    }
                    else
                    {
                        var regobj = new RegExp('-',"gi");
                        var strTpStartdt =  $("#midFormInsert input[name=view_start_dt]").val();
                        $("#midFormInsert input[name=start_dt]").attr('value',strTpStartdt.replace(regobj,''));
                    }
                    
                    if($("#midFormInsert input[name=view_end_dt]").val() == "")
                    {
                      alert("적용 종료일은 필수사항 입니다.");
                      $("#midFormInsert input[name=view_end_dt]").focus();
                      return;
                    }
                    else
                    {
                        var regobj = new RegExp('-',"gi");
                        var strTpEnddt =  $("#midFormInsert input[name=view_end_dt]").val();
                        $("#midFormInsert input[name=end_dt]").attr('value',strTpEnddt.replace(regobj,''));
                    }
                   
                    
                    $.ajax({
                        url : $("#midFormInsert").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#midFormInsert").serialize(),
                        success : function(data) {
                            alert("등록 완료");
                            midMasterGrid.clearAll();
                            doSearchMidMaster();
                            midMasterWindow.window("w1").close();
                        },
                        error : function() { 
                                alert("등록 실패");
                        }
                    });	
                    
                }                   

            </script>
        
            <form id="midFormInsert" method="POST" action="<c:url value="/mid/midMasterInsert" />" modelAttribute="midFormBean">
                <table class="gridtable" height="100%" width="100%">                    
                    <tr>
                        <td align='left' colspan='4'>- 가맹점번호 관리</td>                        
                    </tr>
                    <tr>
                        <td class="headcol" width="25%">가맹점번호 구분*</td>
                        <td>
                            <select name="pay_mtd" >
                                    <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${MidPayMtdList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td class="headcol" width="25%">가맹점번호명*</td>
                        <td><input name="mid_nm" maxlength="50" onblur="javascript:checkLength(this,50);" /></td>
                    </tr>
                    <tr>
                        <td class="headcol">일반/무이자구분*</td>
                        <td><select name="func_cate" >
                                <c:forEach var="code" items="${FuncCateList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select></td>
                        <td class="headcol">자체/대행*</td>
                        <td><select name="agency_flag" >
                                <c:forEach var="code" items="${AgencyFlagList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select></td>
                    </tr>
                    <tr>
                        <td class="headcol">가맹점번호*</td>
                        <td><input name="merch_no" maxlength="20" /></td>
                        <td class="headcol">할부개월(,구분)</tD>
                        <td><input name="installment" maxlength="100"/></td>                        
                    </tr>
                    <tr>
                        <td class="headcol">포인트구분(,구분)</td>
                        <td><input name="point_cate" maxlength="100" /></td>
                        <td class="headcol">수수료*</tD>
                        <td><input name="commision" maxlength="20" onkeyPress="javascript:InpuOnlyNumber(this);" value="0" /></td>                        
                    </tr>                    
                     <tr>
                        <td class="headcol">첵크카드수수료</td>
                        <td><input name="commision2" maxlength="20" onkeyPress="javascript:InpuOnlyNumber(this);" value="0" /></td>
                        <td class="headcol">포인트수수료</tD>
                        <td><input name="point_commsion" maxlength="20" onkeyPress="javascript:InpuOnlyNumber(this);" value="0"/></td>                        
                    </tr>
                    <tr>
                        <td class="headcol">시작일*</td>
                        <td><input name="view_start_dt" id="view_start_dt" maxlength="10" value="<%=strCurDate%>" readonly/></td>
                        <td class="headcol">종료일</tD>
                        <td><input name="view_end_dt" id="view_end_dt" maxlength="10" value="<%=strTpDefaultEndDt%>" readonly/></td>
                    <input type="hidden" name="start_dt">
                    <input type="hidden" name="end_dt">
                    </tr>                    
                    <tr>
                        <td class="headcol">입금은행*</td>
                        <td><select name="bank_cd" >
                                <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${BankCdList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select></td>
                        <td class="headcol">입금계좌*</tD>
                        <td><input name="acc_no" maxlength="30" onkeyPress="javascript:InpuOnlyNumber(this);" /></td>                        
                    </tr>
                    <tr>
                        <td class="headcol">사용여부*</td>
                        <td colspan='3'><select name="use_flag" >
                                <c:forEach var="code" items="${UseFlagList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == 'Y'}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach>
                            </select></td>                        
                    </tr>
                    <tr>
                        <td class="headcol">Memo</td>
                        <td colspan="3"><textarea name="memo" cols="70" rows="5"></textarea></td>                        
                    </tr>                          
                    <tr>
                        <td colspan="4" align="center"><input type="button" name="insert" value="등록"/>&nbsp;<input type="button" name="close" value="닫기"/></td>
                    </tr>
                </table>
            </form>        
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

