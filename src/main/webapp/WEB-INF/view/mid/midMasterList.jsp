<%-- 
    Document   : midMasterList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>가맹점번호 관리</title>
        </head>
        <body>
</c:if>
        <script type="text/javascript">
                var midMasterGrid={};
                var midMasterWindow;
                var midMasterLayout;
                
                $(document).ready(function () {
                    var midSearch = document.getElementById("midSearch");            
                    midMasterLayout = tabbar.cells("1-2").attachLayout("2E");
                    
                    midMasterLayout.cells('a').hideHeader();
                    midMasterLayout.cells('b').hideHeader();
                    midMasterLayout.cells('a').attachObject(midSearch);
                    midMasterLayout.cells('a').setHeight(75);
                    midMasterGrid = midMasterLayout.cells('b').attachGrid();
                    midMasterGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var midMasterGridheaders = "";
                    midMasterGridheaders += "<center>번호</center>,<center>선택</center>,<center>구분</center>,<center>가맹점번호명</center>,<center>가맹점번호</center>,<center>기능분류</center>,<center>자체/대행</center>,<center>수수료</center>,<center>첵크카드수수료</center>,<center>시작일</center>,<center>종료일</center>,<center>사용여부</center>";
                    midMasterGrid.setHeader(midMasterGridheaders);
                    midMasterGrid.setColAlign("center,center,center,center,center,center,center,center,center,center,center,center");
                    midMasterGrid.setColTypes("txt,ch,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt");
                    midMasterGrid.setInitWidths("60,80,150,200,100,100,100,100,120,100,100,100");
                    midMasterGrid.setColSorting("str,str,str,str,str,str,str,str,str,str,str,str");
                    midMasterGrid.enableColumnMove(true);
                    midMasterGrid.setSkin("dhx_skyblue");
                    
                    midMasterGrid.setColumnHidden(1,true);  
    
                    //midMasterGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxToolbar/" />");
                    midMasterGrid.enablePaging(true,Number($("#midForm input[name=page_size]").val()),10,"midMasterListPaging",true,"midMasterListrecinfoArea");
                    midMasterGrid.setPagingSkin("bricks");

                    //페이징 처리                    
                    midMasterGrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        if(lInd !==0){
                           $("#midForm input[name=page_no]").val(ind);             
                            midMasterGrid.clearAll();
                            doSearchMidMaster();
                        }else{
                            $("#midForm input[name=page_no]").val(1);
                            midMasterGrid.clearAll();
                            doSearchMidMaster();
                        }                        
                    });
                    
                    midMasterGrid.init();
                    midMasterGrid.parse(${midMasterList}, "json");
                    midMasterGrid.attachEvent("onRowDblClicked", doUpdateFormMidMaster);
                    
                    midMasterWindow = new dhtmlXWindows();
                    midMasterWindow.attachViewportTo("midList");

                    //버튼 추가
                    $("#midForm input[name=mid_searchButton]").bind("click",function(e){
                        //사업체 정보 조회 이벤트
                        $("#midForm input[name=page_no]").val("1");
                        /*
                        midMasterGrid.clearAll();
                        doSearchMidMaster();
                        */
                        midMasterGrid.changePage(1);
                        return false;
                    });


                    $("#midForm .addButton").button({
                        icons: {
                            primary: "ui-icon-plusthick"
                        },
                        text: false
                    })
                    .unbind("click")
                    .bind("click",function(e){
                        //MID 정보 등록 이벤트
                        doInsertFormMidMaster();
                        return false;
                    });

                    $("#midForm .deleteButton").button({
                        icons: {
                            primary: "ui-icon-minusthick"
                        },
                        text: false
                    })
                    .unbind("click")
                    .bind("click",function(e){
                        //사업체 정보 삭제 이벤트
                        doDeleteMidMaster();
                        return false;
                    });
                     //검색조건 초기화
                    $("#midForm input[name=init]").click(function () {
                    
                        midMasterInit($("#midForm"));

                    }); 
                });

                //사업체 정보 수정
                function doUpdateFormMidMaster(rowid, col){
                                        
                    var merchant_seq = midMasterGrid.getUserData(rowid, 'merchant_seq');
                    
                    w1 = midMasterWindow.createWindow("w1", 150, 150, 800, 540);
                    w1.setText("MID 수정");
                    midMasterWindow.window('w1').setModal(true);
                    w1.attachURL("<c:url value="/mid/midMasterUpdate" />" + "?merchant_seq=" + merchant_seq,true);   
                    
                }

                function resizeOption($id){
                    //css 변경 : resize시 자동 변경됨.
                    $id.css("width","100%");
                    $id.css("height","100%");
                }


                //사업체 정보 조회
                function doSearchMidMaster(){
                    $.ajax({
                        url : $("#midForm").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#midForm").serialize(),
                        success : function(data) {
                            //midMasterGrid.clearAll();
                            //midMasterGrid.parse(jQuery.parseJSON(data), "json");
                            var jsonData = $.parseJSON(data);     
                            midMasterGrid.parse(jsonData,"json");
                            //resizeOption($("#midList.gridbox"));
                        },
                        error : function() {
                            alert("조회 실패");
                        }
                    });
                }

                //사업체 정보 삭제
                /*
                function doDeleteMidMaster(){

                    var rowIds = midMasterGrid.getCheckedRows(1);
                    $("#midForm input[name=merchant_seqs]").val(rowIds.split(","));

                    if(rowIds.length < 1){
                        alert("삭제할 Mid를 선택하십시오.");
                        return;
                    }

                    if(!window.confirm("삭제하시겠습니까?")){
                        return;
                    }

                    $.ajax({
                        url : "<c:url value="/mid/midMasterDelete" />",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#midForm").serialize(),
                        success : function(data) {
                            midMasterGrid.clearAll();
                            doSearchMidMaster();
                        },
                        error : function(jqXHR, textStatus, errorThrown) {
                            alert("삭제 실패 = " + errorThrown);

                        }
                    });
                }
                */

                //사업체 정보 등록
                function doInsertFormMidMaster(){

                    w1 = midMasterWindow.createWindow("w1", 150, 150, 800, 520);
                    //w1 = midMasterWindow.createWindow("editwin", 150, 150, 700, 400);
                    w1.setText("MID 등록");
                    midMasterWindow.window('w1').setModal(true);
                    w1.attachURL("<c:url value="/mid/midMasterInsert" />",true);   

                }
                function midMasterInit($form) {
                    searchFormInit($form);
                } 
            </script>            
            <div class="searchForm1row" id="midSearch" style="display:none;width:100%;height:100%;background-color:white;">
                <form id="midForm" method="POST" action="<c:url value="/mid/midMasterList" />" modelAttribute="midFormBean">
                       <input type="hidden" name="page_size" value="10" />
                       <input type="hidden" name="page_no" value="1" />
                       <input type="hidden" name="merchant_seqs" value="" />
                        
                        <div class="label">구분</div>
                        <div class="input"><select name="pay_mtd" >
                                    <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${MidPayMtdList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select></div>
                        <div class="label">가맹점번호명</div>
                        <div class="input"><input type="text" name="mid_nm" maxlength="20"/></div>
                        <div class="label">가맹점번호</div>
                        <div class="input"><input type="text" name="merch_no" maxlength="20"/></div>

                        <div class="label">일반/무이자</div>
                        <div class="input"><select name="func_cate" >
                                <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${FuncCateList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select></div>

                        <div class="label">자체/대행</div>
                        <div class="input">
                            <select name="agency_flag" >
                                <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${AgencyFlagList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="label">사용여부</div>
                        <div class="input">
                            <select name="use_flag" >
                                <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${UseFlagList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>                        
                        <input type="button" name="mid_searchButton" value="조회"/>
                        <input type="button" name="init" value="검색조건 지우기"/>
                        <div class="action">
                            <button class="addButton">추가</button>
                            <!--
                            <button class="deleteButton">삭제</button>
                            -->
                        </div>
                </form>
                <div id="midMasterListPaging" style="width: 100%;"></div>
                <div id="midMasterListrecinfoArea" style="width: 100%;"></div>                        
            </div>
            <div id="midList" style="width:100%;height:100%;background-color:white;"></div>
 <c:if test="${!ajaxRequest}">    
        </body>
</html>
</c:if>

