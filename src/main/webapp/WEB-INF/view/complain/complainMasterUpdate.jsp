<%-- 
    Document   : complainMasterUpdate
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>민원 수정</title>
        </head>
        <body>
</c:if>
        <div style='width:100%;height:100%;overflow:auto;'>                
            <script type="text/javascript">
                
                var transMasterGrid;
                var complainDetailGrid;
                
                $(document).ready(function () {
                    
                    <c:if test="${ls_complainMasterList[0].tran_seq != '' && fn:length(ls_complainMasterList[0].tran_seq) != 0}">
                        /*
                        transMasterGrid = new dhtmlXGridObject('complainTransList');
                        transMasterGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                        var transMasterGridheaders = "";
                        transMasterGridheaders += "<center>UID</center>,<center>UID명</center>,<center>거래종류</center>,<center>결제채널</center>,<center>TID</center>";
                        transMasterGridheaders += ",<center>카드번호</center>,<center>승인일시</center>,<center>승인번호</center>,<center>승인금액</center>";
                        transMasterGridheaders += ",<center>할부</center>,<center>가맹점번호</center>,<center>매입사</center>,<center>거래상태</center>";
                        transMasterGrid.setHeader(transMasterGridheaders);
                        transMasterGrid.setColAlign("center,center,center,center,center,center,center,center,right,center,center,center,center");
                        transMasterGrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,txt,edn,txt,txt,txt,txt");
                        transMasterGrid.setInitWidths("150,150,100,100,100,100,100,100,100,100,100,100,100");
                        transMasterGrid.setColSorting("str,str,str,str,str,str,str,str,int,str,str,str,str");
                        transMasterGrid.setNumberFormat("0,000",8,".",",");
                        transMasterGrid.enableColumnMove(true);
                        transMasterGrid.setSkin("dhx_skyblue");
                        transMasterGrid.init();
                        transMasterGrid.parse(${ls_transMasterList}, "json");  
                        */
                        transMasterGrid = new dhtmlXGridObject('complainTransList');
                        transMasterGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                        var transMasterGridheaders = "";
                        transMasterGridheaders += "<center>지급ID명</center>,<center>UID명</center>,<center>거래종류</center>,<center>결제채널</center>";
                        transMasterGridheaders += ",<center>카드번호</center>,<center>승인일시</center>,<center>승인번호</center>,<center>승인금액</center>";
                        transMasterGridheaders += ",<center>할부</center>,<center>발급사</center>,<center>거래상태</center>,<center>고객명</center>,<center>전화번호</center>,<center>상품명</center>";
                        transMasterGrid.setHeader(transMasterGridheaders);
                        transMasterGrid.setColAlign("center,center,center,center,center,center,center,right,center,center,center,center,center,center");
                        transMasterGrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,edn,txt,txt,txt,txt,txt,txt");
                        transMasterGrid.setInitWidths("120,120,70,100,110,110,80,80,60,70,80,80,100,150");
                        transMasterGrid.setColSorting("str,str,str,str,str,str,str,str,str,int,str,str,str,str");
                        transMasterGrid.setColumnHidden(3,true);
                        transMasterGrid.setNumberFormat("0,000",7,".",",");
                        transMasterGrid.enableColumnMove(true);
                        transMasterGrid.setSkin("dhx_skyblue");
                        transMasterGrid.init();
                        transMasterGrid.parse(${ls_transMasterList}, "json");                              
                    </c:if>
                        
                    complainDetailGrid = new dhtmlXGridObject('complainDetailList');
                    complainDetailGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var complainDetailGridheaders = "";
                    complainDetailGridheaders += "<center>NO</center>,<center>담당자</center>,<center>등록일</center>,<center>제목</center>,<center>내용</center>";
                    complainDetailGrid.setHeader(complainDetailGridheaders);
                    complainDetailGrid.setColAlign("center,center,center,left,left");
                    complainDetailGrid.setColTypes("txt,txt,txt,txt,txt");
                    complainDetailGrid.setInitWidths("50,100,150,400,700");
                    complainDetailGrid.setColSorting("str,str,str,str,str");
                    complainDetailGrid.enableColumnMove(true);
                    complainDetailGrid.setSkin("dhx_skyblue");
                    complainDetailGrid.init();
                    complainDetailGrid.parse(${ls_complainDetailList}, "json");        
                    complainDetailGrid.attachEvent("onRowDblClicked", complainDetailGrid_DblClicked);
                    
                    //민원 수정 이벤트
                    $("#complainMasterFormUpdate input[name=update]").click(function(){
                       doUpdateComplainMaster(); 
                    });           
                    
                    //민원 삭제 이벤트
                    $("#complainMasterFormUpdate input[name=delete]").click(function(){
                       doDeleteComplainMaster(); 
                    });                               
                    
                    //민원 팝업 닫기
                    $("#complainMasterFormUpdate input[name=close]").click(function(){
                        complainMasterWindow.window("w1").close();
                    });               
                    
                    //onoff가맹점 검색 팝업 호출
                    $("#complainMasterFormUpdate input[name=onffmerch_search]").click(function(){                
                        onoffMerchSelectPopup();
                    });      
                    
                    //onofftid 검색 팝업 호출
                    $("#complainMasterFormUpdate input[name=onfftid_search]").click(function(){                
                        onoffTidSelectPopup();
                    });                      
                    
                    //응답 등록 호출
                    $("#complainDetailForm input[name=insert]").click(function(){                
                        doInsertComplainDetail();
                    });               
                    
                    //응답 수정 호출
                    $("#complainDetailForm input[name=update]").click(function(){                
                        doUpdateComplainDetail();
                    });               
                    
                    //응답 삭제 호출
                    $("#complainDetailForm input[name=delete]").click(function(){                
                        doDeleteComplainDetail();
                    });         
                    
                    //민원 완료 호출
                    $("#complainMasterFormUpdate input[name=complete]").click(function(){                
                        doUpdateComplainMasterComplete();
                    });    
                    
                    //민원 완료 호출
                    $("#complainMasterFormUpdate input[name=uncomplete]").click(function(){                
                        doUpdateComplainMasterUnComplete();
                    });                       
                                        
                    
                });
                
                //민원 완료
                function doUpdateComplainMasterComplete(){
                    
                    if(!window.confirm("완료하시겠습니까?")){
                        return false;
                    }
                    
                    var complain_popup_yn = '${complainFormBean.complain_popup_yn}';
                    
                    $.ajax({
                        url : "<c:url value="/complain/complainMasterUpdateComplete" />",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#complainMasterFormUpdate").serialize(),
                        success : function(data) {

                            alert("완료");

                            if(complain_popup_yn === "Y"){
                                transdetailSearch();
                            }else{
                                complainSearch();
                            }   
                            
                            complainMasterWindow.window("w1").close();
                            
                        },
                        error : function() { 
                                alert("실패");
                        }
                    });	       
                            
                }          
                
                //민원 미완료
                function doUpdateComplainMasterUnComplete(){
                    
                    if(!window.confirm("미완료처리하시겠습니까?")){
                        return false;
                    }
                    
                    var complain_popup_yn = '${complainFormBean.complain_popup_yn}';
                    
                    $.ajax({
                        url : "<c:url value="/complain/complainMasterUpdateUnComplete" />",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#complainMasterFormUpdate").serialize(),
                        success : function(data) {

                            alert("완료");

                            if(complain_popup_yn === "Y"){
                                transdetailSearch();
                            }else{
                                complainSearch();
                            }   
                            
                            complainMasterWindow.window("w1").close();
                            
                        },
                        error : function() { 
                                alert("실패");
                        }
                    });	       
                            
                }                           

                //민원 수정
                function doUpdateComplainMaster(){
                   /*
                    if($("#complainMasterFormUpdate input[name=cust_nm]").val().trim() === ''){
                        alert("고객명을 입력하십시오.");
                        return false;
                    }
                    
                    if($("#complainMasterFormUpdate input[name=cust_tel]").val().trim() === ''){
                        alert("연락처를 입력하십시오.");
                        return false;
                    }
                    */
                   /*
                    if($("#complainMasterFormUpdate input[name=title]").val().trim() === ''){
                        alert("제목을 입력하십시오.");
                        return false;
                    }
*/
                    if($("#complainMasterFormUpdate textarea[name=content]").val() === ''){
                        alert("내용을 입력하십시오.");
                        return false;
                    }        
                    
                    if(!window.confirm("수정하시겠습니까?")){
                        return false;
                    }
                    
                    var complain_popup_yn = '${complainFormBean.complain_popup_yn}';
                    
                    $.ajax({
                        url : $("#complainMasterFormUpdate").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#complainMasterFormUpdate").serialize(),
                        success : function(data) {

                            alert("수정 완료");

                            if(complain_popup_yn === "Y"){
                                transdetailSearch();
                            }else{
                                complainSearch();
                            }   
                            
                            complainMasterWindow.window("w1").close();   

                        },
                        error : function() { 
                                alert("수정 실패");
                        }
                    });	      
        
                }              
                
                //민원 삭제
                function doDeleteComplainMaster(){
                    
                    if(!window.confirm("삭제하시겠습니까?")){
                        return false;
                    }
                    
                    var complain_popup_yn = '${complainFormBean.complain_popup_yn}';
                    
                    $.ajax({
                        url : "<c:url value="/complain/complainMasterDelete" />",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#complainMasterFormUpdate").serialize(),
                        success : function(data) {

                            alert("삭제 완료");

                            if(complain_popup_yn === "Y"){
                                transdetailSearch();
                            }else{
                                complainSearch();
                            }   
                            
                            complainMasterWindow.window("w1").close();
                            
                        },
                        error : function() { 
                                alert("삭제 실패");
                        }
                    });	       
                            
                }
                
                var dhxSelPopupWins = new dhtmlXWindows();
                
                function onoffMerchSelectPopup(){
                    //지급ID정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("지급ID선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                }
              
                function SelOnoffmerchInfoInput(p_onffmerch_no, p_merch_nm){
                    $("#complainMasterFormUpdate input[name=onffmerch_no]").val(p_onffmerch_no);
                    $("#complainMasterFormUpdate input[name=merch_nm]").val(p_merch_nm);                    
                }               
                
                function onoffTidSelectPopup(){
                    //지급ID정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnofftidSelectPopUp", 20, 30, 640, 480);
                    w2.setText("UID 선택페이지");
                    dhxSelPopupWins.window('OnofftidSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnofftidSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnofftidSelectPopUp"/>', true);   
                }                
              
                function SelOnofftidInfoInput(p_onfftid, p_onfftid_nm){
                    $("#complainMasterFormUpdate input[name=onfftid]").val(p_onfftid);
                    $("#complainMasterFormUpdate input[name=onfftid_nm]").val(p_onfftid_nm);                    
                }                         
                
                //응답내용 초기화
                function clearSelForm()
                {
                    $("#complainDetailForm input[name=cust_nm]").attr('value',"");               
                    $("#complainDetailForm input[name=title]").attr('value',"");  
                    $("#complainDetailForm textarea[name=content]").attr('value',"");  
                }         
                
                //응답 등록
                function doInsertComplainDetail(){
                   
                    if($("#complainDetailForm input[name=cust_nm]").val().trim() === ''){
                        alert("담당자를 입력하십시오.");
                        return false;
                    }
                    
                    if($("#complainDetailForm input[name=title]").val().trim() === ''){
                        alert("제목을 입력하십시오.");
                        return false;
                    }

                    if($("#complainDetailForm textarea[name=content]").val() === ''){
                        alert("내용을 입력하십시오.");
                        return false;
                    }        
                    
                    if(!window.confirm("등록하시겠습니까?")){
                        return false;
                    }
                    
                    $.ajax({
                        url : "<c:url value="/complain/complainDetailInsert" />",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#complainDetailForm").serialize(),
                        success : function(data) {
                            
                            alert("등록 완료");
                            clearSelForm();
                            $("#complainDetailForm input[name=complain_seq]").attr('value','');
                            complainSearch();
                            complainDetailSearch();
                            
                        },
                        error : function() { 
                                alert("등록 실패");
                        }
                    });	      
        
                }        
                
                function complainDetailGrid_DblClicked(rowid){
                
                    $("#complainDetailForm input[name=complain_seq]").attr('value',rowid);
                    $("#complainDetailForm input[name=cust_nm]").attr('value',complainDetailGrid.cells(rowid,1).getValue());
                    $("#complainDetailForm input[name=title]").attr('value',complainDetailGrid.cells(rowid,3).getValue());  
                    $("#complainDetailForm textarea[name=content]").attr('value',complainDetailGrid.cells(rowid,4).getValue());  
                
                }
                
                //응답 수정
                function doUpdateComplainDetail(){
                   
                    var complain_seq = $("#complainDetailForm input[name=complain_seq]").val();
                   
                    if(complain_seq === ''){
                        alert("수정할 응답 리스트를 선택하십시오.");
                        return false;
                    }
                   
                    if($("#complainDetailForm input[name=cust_nm]").val().trim() === ''){
                        alert("담당자를 입력하십시오.");
                        return false;
                    }
                    
                    if($("#complainDetailForm input[name=title]").val().trim() === ''){
                        alert("제목을 입력하십시오.");
                        return false;
                    }
                    
                    if($("#complainDetailForm textarea[name=content]").val() === ''){
                        alert("내용을 입력하십시오.");
                        return false;
                    }        
                    
                    if(!window.confirm("수정하시겠습니까?")){
                        return false;
                    }
                    
                    $.ajax({
                        url : "<c:url value="/complain/complainDetailUpdate" />",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#complainDetailForm").serialize(),
                        success : function(data) {
                            
                            alert("수정 완료");
                            clearSelForm();
                            $("#complainDetailForm input[name=complain_seq]").attr('value','');
                            complainSearch();
                            complainDetailSearch();
                            
                        },
                        error : function() { 
                                alert("수정 실패");
                        }
                    });	      
        
                }                
                
                //응답 삭제
                function doDeleteComplainDetail(){
                    
                    var complain_seq = $("#complainDetailForm input[name=complain_seq]").val();
                   
                    if(complain_seq === ''){
                        alert("삭제할 응답 리스트를 선택하십시오.");
                        return false;
                    }
                    
                    if(!window.confirm("삭제하시겠습니까?")){
                        return false;
                    }
                    
                    $.ajax({
                        url : "<c:url value="/complain/complainDetailDelete" />",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#complainDetailForm").serialize(),
                        success : function(data) {
                            
                            alert("삭제 완료");
                            clearSelForm();
                            $("#complainDetailForm input[name=complain_seq]").attr('value','');
                            complainSearch();
                            complainDetailSearch();
                            
                        },
                        error : function() { 
                                alert("삭제 실패");
                        }
                    });	      
        
                }                       
                
                //응답내역 조회
                function complainDetailSearch() {

                    $.ajax({
                        url: "<c:url value="/complain/complainDetailList" />",
                        type: "POST",
                        async: true,
                        dataType: "json",
                        data: $("#complainDetailForm").serialize(),
                        success: function (data) {

                            complainDetailGrid.clearAll();
                            complainDetailGrid.parse($.parseJSON(data), "json");

                        },
                        error: function () {
                            alert("조회 실패");
                        }
                    });
                }                

            </script>
            
            <form id="complainMasterFormUpdate" method="POST" action="<c:url value="/complain/complainMasterUpdate" />" modelAttribute="complainFormBean">
                <input type="hidden" name="tran_seq" value="${ls_complainMasterList[0].tran_seq}" />
                <input type="hidden" name="complain_seq" value="${ls_complainMasterList[0].complain_seq}" />
                <table class="gridtable" height="20%" width="100%">
                    <tr>
                        <td align='left' colspan='4'>지급ID 정보   
                            <c:if test="${complainFormBean.ses_user_cate == '00'}">
                                <input type="button" name="onffmerch_search" value="검색"/>                                
                            </c:if>
                        </td>                        
                    </tr>                    
                    <tr>
                        <td class="headcol">지급ID번호</td>
                        <td><input name="onffmerch_no" value="${ls_complainMasterList[0].onffmerch_no}" readonly="readonly" /></td>
                        <td class="headcol">지급ID명</td>
                        <td><input name="merch_nm" value="${ls_complainMasterList[0].merch_nm}" readonly="readonly" /></td>
                    </tr> 
                    <tr>
                        <td align='left' colspan='4'>UID 정보   
                            <c:if test="${complainFormBean.ses_user_cate == '00'}">
                                <input type="button" name="onfftid_search" value="검색"/>
                            </c:if>
                        </td>                        
                    </tr>                                
                    <tr>
                        <td class="headcol">UID</td>
                        <td><input name="onfftid" value="${ls_complainMasterList[0].onfftid}" readonly="readonly" /></td>
                        <td class="headcol">UID명</td>
                        <td><input name="onfftid_nm" value="${ls_complainMasterList[0].onfftid_nm}" readonly="readonly" /></td>
                    </tr>                                                                   
                    <tr>
                        <td align='left' colspan='4'>민원 접수</td>                        
                    </tr>
                    <tr>
                        <td class="headcol">업체연락처1</td>
                        <td>${ls_complainMasterList[0].tel_1}</td>
                        <td class="headcol">업체연락처2</td>
                        <td>${ls_complainMasterList[0].tel_2}</td>
                    </tr>                    
                    <tr>
                        <td class="headcol">고객명</td>
                        <td><input name="cust_nm" value="${ls_complainMasterList[0].cust_nm}" /></td>
                        <td class="headcol">연락처</td>
                        <td><input name="cust_tel" value="${ls_complainMasterList[0].cust_tel}" onkeyup="javascript:InpuOnlyNumber(this);" onblur="javascript:checkLength(this,20);" /></td>
                    </tr>
                    <tr>
                        <td class="headcol">카드번호</td>
                        <td>${ls_complainMasterList[0].card_num}</td>
                        <td class="headcol">할부</td>
                        <td>${ls_complainMasterList[0].installment}</td>
                    </tr>
                    <tr>
                        <td class="headcol">승인일시</td>
                        <td>${ls_complainMasterList[0].app_dt}${ls_complainMasterList[0].tot_amt}</td>
                        <td class="headcol">승인번호</td>
                        <td>${ls_complainMasterList[0].app_no}</td>
                    </tr>
                    <tr>
                        <td class="headcol">승인금액</td>
                        <td>${ls_complainMasterList[0].tot_amt}</td>
                        <td class="headcol">취소일</td>
                        <td>${ls_complainMasterList[0].cncl_dt}</td>
                    </tr>
                    <tr>
                        <td class="headcol">거래상태</td>
                        <td>${ls_complainMasterList[0].tran_status_nm}</td>
                        <td class="headcol"></td>
                        <td></td>
                    </tr> 
                    <!--
                    <tr>
                        <td class="headcol">제목</td>
                        <td colspan="3"><input name="title" value="${ls_complainMasterList[0].title}" onblur="javascript:checkLength(this,250);" style="width:90%;" /></td>
                    </tr>
                    -->
                    <tr>
                        <td class="headcol">민원종류</td>
                        <td colspan="3"><select name="complain_cate">
                                <c:forEach var="code" items="${complainCateList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == complainFormBean.complain_cate}">selected</c:if> >${code.code_nm}</option>
                                </c:forEach>
                            </select><input type="hidden" name="title" valule="" /></td>
                    </tr>                    
                    <tr>
                        <td class="headcol">내용</td>
                        <td colspan="3">
                            <textarea name="content" rows="5" cols="80" onblur="javascript:checkLength(this,1000);">${ls_complainMasterList[0].content}</textarea>
                        </td>
                    </tr> 
                
                <c:if test="${ls_complainMasterList[0].tran_seq != '' && fn:length(ls_complainMasterList[0].tran_seq) != 0}">
                        <tr>
                            <td align='left' colspan='4'>연관 거래</td>                        
                        </tr>
                        <tr>
                            <td colspan='4'>
                                <div id="complainTransList" style="height:70px;background-color:white;"></div>                        
                            </td>
                        </tr>
                </c:if>

                    <tr>
                        <td colspan="4">
                            <c:if test="${complainFormBean.ses_user_cate == '00'}">
                                <input type="button" name="update" value="수정"/>
                                <input type="button" name="delete" value="삭제"/>      
                                <c:if test="${ls_complainMasterList[0].complete_flag == 'N'}">
                                    <input type="button" name="complete" value="완료"/>
                                </c:if>
                                <c:if test="${ls_complainMasterList[0].complete_flag == 'Y'}">
                                    <input type="button" name="uncomplete" value="미완료"/>
                                </c:if>                                    
                            </c:if>
                            <input type="button" name="close" value="닫기"/>
                        </td>
                    </tr>
                </table>
                
                <br/>
            </form>
            
            <form id="complainDetailForm" method="POST" modelAttribute="complainFormBean">
                <input type="hidden" name="parent_complain_seq" value="${ls_complainMasterList[0].complain_seq}" />
                <input type="hidden" name="complain_seq" value="" />
                <table class="gridtable" height="20%" width="100%">
                    <tr>
                        <td align='left' colspan='4'>가맹점 응답내역   
                            <input type="button" name="insert" value="등록"/>
                            <input type="button" name="update" value="수정"/>
                            <c:if test="${complainFormBean.ses_user_cate == '00'}">
                                <input type="button" name="delete" value="삭제"/>                                
                            </c:if>
                        </td>
                    </tr>                    
                    <tr>
                        <td class="headcol">담당자</td>
                        <td colspan="3"><input name="cust_nm" value="" /></td>
                    </tr>
                    <tr>
                        <td class="headcol">제목</td>
                        <td colspan="3"><input name="title" onblur="javascript:checkLength(this,250);" style="width:90%;" /></td>
                    </tr>                          
                    <tr>
                        <td class="headcol">내용</td>
                        <td colspan="3">
                            <textarea name="content" cols="60" onblur="javascript:checkLength(this,1000);"></textarea>
                        </td>
                    </tr> 
                </table>       
                
                <br/>
                
                <table class="gridtable" height="20%" width="100%">
                    <tr>
                        <td>
                            <div id="complainDetailList" style="height:100px;background-color:white;"></div>                        
                        </td>
                    </tr>
                </table>                
                
            </form>            
                

<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

