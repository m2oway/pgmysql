<%-- 
    Document   : complainMasterInsert
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>민원 등록</title>
        </head>
        <body>
</c:if>
                
            <script type="text/javascript">
                
                var transMasterGrid;
                
                $(document).ready(function () {
                    
                    <c:if test="${complainFormBean.tran_seq != '' && fn:length(complainFormBean.tran_seq) != 0}">
                        transMasterGrid = new dhtmlXGridObject('complainTransList');
                        transMasterGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                        var transMasterGridheaders = "";
                        transMasterGridheaders += "<center>지급ID명</center>,<center>UID명</center>,<center>거래종류</center>,<center>결제채널</center>";
                        transMasterGridheaders += ",<center>카드번호</center>,<center>승인일시</center>,<center>승인번호</center>,<center>승인금액</center>";
                        transMasterGridheaders += ",<center>할부</center>,<center>발급사</center>,<center>거래상태</center>,<center>고객명</center>,<center>전화번호</center>,<center>상품명</center>";
                        transMasterGrid.setHeader(transMasterGridheaders);
                        transMasterGrid.setColAlign("center,center,center,center,center,center,center,right,center,center,center,center,center,center");
                        transMasterGrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,edn,txt,txt,txt,txt,txt,txt");
                        transMasterGrid.setInitWidths("120,120,70,100,110,110,80,80,60,70,80,80,100,150");
                        transMasterGrid.setColSorting("str,str,str,str,str,str,str,str,str,int,str,str,str,str");
                        transMasterGrid.setColumnHidden(3,true);
                        transMasterGrid.setNumberFormat("0,000",7,".",",");
                        transMasterGrid.enableColumnMove(true);
                        transMasterGrid.setSkin("dhx_skyblue");
                        transMasterGrid.init();
                        transMasterGrid.parse(${transMasterList}, "json");                        
                    </c:if>
                    <c:if test="${complainFormBean.tran_seq != '' && fn:length(complainFormBean.tran_seq) != 0}">    
                        var tprowsnum = transMasterGrid.getRowsNum();
                        
                        if(tprowsnum > 0 )
                        {
                           var tpUserNm = transMasterGrid.cells(transMasterGrid.getRowId(0),11).getValue();
                           var tpUserTel = transMasterGrid.cells(transMasterGrid.getRowId(0),12).getValue();
                           $("#complainMasterFormInsert input[name=cust_nm]").val(tpUserNm);
                           $("#complainMasterFormInsert input[name=cust_tel]").val(tpUserTel);
                        }
                    </c:if>
                    //민원 등록 이벤트
                    $("#complainMasterFormInsert input[name=insert]").click(function(){
                       doInsertComplainMaster(); 
                    });              
                    
                    //민원 등록 닫기
                    $("#complainMasterFormInsert input[name=close]").click(function(){
                        complainMasterWindow.window("w1").close();
                    });               
                    
                    //onoff가맹점 검색 팝업 호출
                    $("#complainMasterFormInsert input[name=onffmerch_search]").click(function(){                
                        onoffMerchSelectPopup();
                    });           
                    
                    //onofftid 검색 팝업 호출
                    $("#complainMasterFormInsert input[name=onfftid_search]").click(function(){                
                        onoffTidSelectPopup();
                    });             
                    
                    //onoff가맹점 검색 초기화
                    $("#complainMasterFormInsert input[name=onffmerch_clean]").click(function(){                
                        $("#complainMasterFormInsert input[name=onffmerch_no]").val('');
                        $("#complainMasterFormInsert input[name=merch_nm]").val('');      
                        $("#complainMasterFormInsert input[name=onfftid]").val('');
                        $("#complainMasterFormInsert input[name=onfftid_nm]").val('');
                    });           
                    
                    //onofftid 검색 초기화
                    $("#complainMasterFormInsert input[name=onfftid_clean]").click(function(){                
                        $("#complainMasterFormInsert input[name=onfftid]").val('');
                        $("#complainMasterFormInsert input[name=onfftid_nm]").val('');
                    });                                 
                    
                });

                //민원 등록
                function doInsertComplainMaster(){
                   
                    if($("#complainMasterFormInsert input[name=onffmerch_no]").val().trim() === ''){
                        alert("지급ID번호를 선택하십시오.");
                        return false;
                    }
                    
                    if($("#complainMasterFormInsert input[name=onfftid]").val().trim() === ''){
                        alert("UID를 선택하십시오.");
                        return false;
                    }
                    /*
                    if($("#complainMasterFormInsert input[name=cust_nm]").val().trim() === ''){
                        alert("고객명을 입력하십시오.");
                        return false;
                    }
                    
                    if($("#complainMasterFormInsert input[name=cust_tel]").val().trim() === ''){
                        alert("연락처를 입력하십시오.");
                        return false;
                    }
                    */
                   
                   /*
                    
                    if($("#complainMasterFormInsert input[name=title]").val().trim() === ''){
                        alert("제목을 입력하십시오.");
                        return false;
                    }
                    */

                    if($("#complainMasterFormInsert textarea[name=content]").val() === ''){
                        alert("내용을 입력하십시오.");
                        return false;
                    }        
                    
                    if(!window.confirm("등록하시겠습니까?")){
                        return false;
                    }
                    
                    var complain_popup_yn = '${complainFormBean.complain_popup_yn}';
                    var grid_nm = '${complainFormBean.grid_nm}';
                    
                    $.ajax({
                        url : $("#complainMasterFormInsert").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#complainMasterFormInsert").serialize(),
                        success : function(data) {
                            
                            alert("등록 완료");
                            
                            if(complain_popup_yn === "Y"){ 
                                complainMasterWindow.window("w1").close();
                                
                                if(grid_nm==="trans_searchdetailgrid"){
//                                 거래내역
                                    trans_searchdetailgrid.clearAll();
                                    trans_searchdetailSearch();
                                }else if(grid_nm==="transdetailgrid"){
//                              신용카드 거래내역
                                    transdetailgrid.clearAll();
                                    transdetailSearch();                                    
                                }else if(grid_nm==="transChkgrid"){
 //                              신용카드 의심거래내역
                                    transChkgrid.clearAll();
                                    transChkSearch();                        
                                }
                            }else{
                                complainMasterWindow.window("w1").close();
                                complainSearch();
                            }
                            
//                            complainMasterWindow.window("w1").close();
                            
                        },
                        error : function() { 
                                alert("등록 실패");
                        }
                    });	      
        
                }                
                
                var dhxSelPopupWins = new dhtmlXWindows();
                
                function onoffMerchSelectPopup(){
                    //지급ID정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("지급ID선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                }
              
                function SelOnoffmerchInfoInput(p_onffmerch_no, p_merch_nm){
                    $("#complainMasterFormInsert input[name=onffmerch_no]").val(p_onffmerch_no);
                    $("#complainMasterFormInsert input[name=merch_nm]").val(p_merch_nm);      
                    $("#complainMasterFormInsert input[name=onfftid]").val('');
                    $("#complainMasterFormInsert input[name=onfftid_nm]").val('');
                }            
                
                function onoffTidSelectPopup(){
                    //지급ID정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnofftidSelectPopUp", 20, 30, 640, 480);
                    w2.setText("UID 선택페이지");
                    dhxSelPopupWins.window('OnofftidSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnofftidSelectPopUp').denyResize();
                    onffmerch_no = $("#complainMasterFormInsert input[name=onffmerch_no]").val();
                    param = '?onffmerch_no=' + onffmerch_no;
                    w2.attachURL('<c:url value = "/popup/OnofftidSelectPopUp"/>' + param, true);   
                }                
              
                function SelOnofftidInfoInput(p_onffmerch_no, p_onfftid, p_onfftid_nm){
                    $("#complainMasterFormInsert input[name=onffmerch_no]").val(p_onffmerch_no);
                    $("#complainMasterFormInsert input[name=onfftid]").val(p_onfftid);
                    $("#complainMasterFormInsert input[name=onfftid_nm]").val(p_onfftid_nm);                    
                }                

            </script>
            
            <form id="complainMasterFormInsert" method="POST" action="<c:url value="/complain/complainMasterInsert" />" modelAttribute="complainFormBean">
                <input type="hidden" name="tran_seq" value="${complainFormBean.tran_seq}" />
                <table class="gridtable" height="20%" width="100%">
                    <tr>
                        <td align='left' colspan='4'>지급ID 정보   <input type="button" name="onffmerch_search" value="검색"/><input type="button" name="onffmerch_clean" value="초기화"/></td>                        
                    </tr>                    
                    <tr>
                        <td class="headcol">지급ID</td>
                        <td><input name="onffmerch_no" value="${ls_transDetailList[0].onffmerch_no}" readonly="readonly" /></td>
                        <td class="headcol">지급ID명</td>
                        <td><input name="merch_nm" value="${ls_transDetailList[0].merch_nm}" readonly="readonly" /></td>
                    </tr>
                    <tr>
                        <td align='left' colspan='4'>UID 정보   <input type="button" name="onfftid_search" value="검색"/><input type="button" name="onfftid_clean" value="초기화"/></td>                        
                    </tr>                                
                    <tr>
                        <td class="headcol">UID</td>
                        <td><input name="onfftid" value="${ls_transDetailList[0].onfftid}" readonly="readonly" /></td>
                        <td class="headcol">UID명</td>
                        <td><input name="onfftid_nm" value="${ls_transDetailList[0].onfftid_nm}" readonly="readonly" /></td>
                    </tr>                     
                    <tr>
                        <td align='left' colspan='4'>민원 접수</td>                        
                    </tr>                    
                    <tr>
                        <td class="headcol">고객명</td>
                        <td><input name="cust_nm" value="" /></td>
                        <td class="headcol">연락처</td>
                        <td><input name="cust_tel" value="" onkeyup="javascript:InpuOnlyNumber(this);" onblur="javascript:checkLength(this,20);" /></td>
                    </tr>
                    <!--
                    <tr>
                        <td class="headcol">제목</td>
                        <td colspan="3"><input name="title" onblur="javascript:checkLength(this,250);" style="width:90%;" /></td>
                    </tr>
                    -->
                    <tr>
                        <td class="headcol">민원종류</td>
                        <td colspan="3"><select name="complain_cate">
                                <c:forEach var="code" items="${complainCateList.tb_code_details}">
                                    <option value="${code.detail_code}" >${code.code_nm}</option>
                                </c:forEach>
                            </select><input type="hidden" name="title" valule="" /></td>
                    </tr>                    
                    <tr>
                        <td class="headcol">내용</td>
                        <td colspan="3">
                            <textarea name="content" cols="70" onblur="javascript:checkLength(this,1000);"></textarea>
                        </td>
                    </tr> 

                <c:if test="${complainFormBean.tran_seq != '' && fn:length(complainFormBean.tran_seq) != 0}">
                        <tr>
                            <td align='left' colspan='4'>연관 거래</td>                        
                        </tr>
                        <tr>
                            <td colspan='4'>
                                <div id="complainTransList" style="height:70px;background-color:white;"></div>                        
                            </td>
                        </tr>
                </c:if>

                    <tr>
                        <td colspan="4">
                            <input type="button" name="insert" value="등록"/>
                            <input type="button" name="close" value="닫기"/>
                        </td>
                    </tr>
                </table>
                
            </form>            
                

<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

