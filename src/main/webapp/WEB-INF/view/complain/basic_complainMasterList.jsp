<%-- 
    Document   : complainMasterList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="/WEB-INF/view/common/basic_common.jsp"/>
<%@include file="/WEB-INF/view/common/basic_comm_header.jsp"%>
    <table width="100%" height="42" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top" class="titleBg">
		<table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="title"><b>-민원관리</b></td>
                        <td><img src="../resources/images/title_sright.gif" width="12" height="38" /></td>
                    </tr>
		</table>
            </td>
	</tr>
    </table>
<div class="right" id="complain_search" style="width:100%;">       
            <form id="complainForm" name="complainForm"  method="POST" onsubmit="return false" action="<c:url value="/complain/basic_complainMasterList" />" modelAttribute="complainFormBean"> 
                    <input type="hidden" name="page_size" id="page_size" value="15" >
                    <input type="hidden" name="page_no" id="page_no" value="${complainFormBean.page_no}" >
                    <input type="hidden" name="total_cnt" id="total_cnt" value="${total_count}" >
                <table>
                    <tr><td>
                        <div class="label">등록일</div>
                        <div class="input">
                            <input type="text" size="10" name="ins_start_dt" id="complainForm_from_date" value ="${complainFormBean.ins_start_dt}" onclick="setSens('complainForm_to_date', 'max');" /> ~
                            <input type="text" size="10" name="ins_end_dt" id="complainForm_to_date" value="${complainFormBean.ins_end_dt}" onclick="setSens('complainForm_from_date', 'min');" />
                        </div>
                        <div class="label">고객명</div>
                        <div class="input"><input type="text" name="cust_nm" maxlength="10" value="${complainFormBean.cust_nm}"/></div>
                        <div class="label">전화번호</div>
                        <div class="input"><input type="text" name="cust_tel" maxlength="15" value="${complainFormBean.cust_tel}"/></div>
                        <div class="label">거래금액</div>
                        <div class="input"><input type="text" name="tot_amt" maxlength="15" value="${complainFormBean.tot_amt}"/></div>
                        <div class="label">승인번호</div>
                        <div class="input"><input type="text" name="app_no" maxlength="15" value="${complainFormBean.app_no}"/></div>
                        <div class="label">카드번호</div>
                        <div class="input"><input type="text" name="card_num" maxlength="15" value="${complainFormBean.card_num}"/></div>
                        <div class="label">민원종류</div>
                        <div class="input">
                            <select name="complain_cate">
                                <option value="">전체</option>
                                <c:forEach var="code" items="${complainCateList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == complainFormBean.complain_cate}">selected</c:if> >${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>                          
                        
                        <div class="label">완료여부</div>
                        <div class="input">
                            <select name="complete_flag">
                                <option value="tot">전체</option>
                                <c:forEach var="code" items="${completeFlagList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == complainFormBean.complete_flag}">selected</c:if> >${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div></td> 
                        <td><input type="button" name="complain_search" onclick="javascript:complainSearch();" value="조회"/></td>
                         <td><input type="button" name="complainlist_excel" onclick="javascirpt:ComplainExcel();" value="엑셀"/></td>
                    </tr>
               </table>
            </form>
        </div>
<br>
                <div class="CSSTableGenerator" >
                <table>
                    <tr>
                        <td height="30px;" width="30px"  style="text-align:center">No</td> 
                        <td height="40px;"  style="text-align:center">가맹점</td>
                        <td >고객명</td>
                        <td >연락처</td>
                        <td >상품명</td>
                        <td >카드번호</td>
                        <td >할부</td>
                        <td >승인금액</td>
                        <td >승인번호</td>
                        <td >승인일시</td>
                        <td >민원내용</td>                        
                        <td width="80px;">등록일</td>
                    </tr>
 <c:forEach var="complainlist" items="${ls_complainMasterList}" begin="0">              
                    <tr onclick="javascript:complain_attach('${complainlist.complain_seq}','${complainlist.tran_seq}');">
                        <td height="30px;" width="30px"  style="text-align:center">${complainlist.rnum}</td> 
                        <td height="40px;" style="text-align:center">${complainlist.merch_nm}</td>
                        <td >${complainlist.cust_nm}</td>
                        <td >${complainlist.tel_1}</td>
                        <td >${complainlist.product_nm}</td>
                        <td >${complainlist.card_num}</td>
                        <td >${complainlist.installment}</td>
                        <td ><fmt:formatNumber value="${complainlist.tot_amt}" type="number"/></td>
                        <td >${complainlist.app_no}</td>
                        <td ><fmt:parseDate value="${complainlist.app_dt}" var="appdt1" pattern="yyyymmdd" scope="page"/><fmt:formatDate value="${appdt1}" pattern="yyyy-mm-dd" /></td>
                        <td >${complainlist.content}</td>
                        <td width="80px;"><fmt:parseDate value="${complainlist.ins_dt}" var="insdt1" pattern="yyyymmddHHmmss" scope="page"/><fmt:formatDate value="${insdt1}" pattern="yyyy-mm-dd HH:mm:ss" /></td>
                    </tr>
</c:forEach>
                </table>
            </div>       
            <div id="paging" name="paging" style="margin: 15px;"></div>
        </div>
<%@include file="/WEB-INF/view/common/basic_comm_tail.jsp"%>        
         <script type="text/javascript">

            var complainCalendar;
            var complainMasterWindow;
            
            var dhxSelPopupWins=new dhtmlXWindows();
            
            $(document).ready(function () {
                complainCalendar = new dhtmlXCalendarObject(["complainForm_from_date","complainForm_to_date"]);    
                complainMasterWindow = new dhtmlXWindows();
                
                 //makepage(${complainFormBean.page_no});
            });

            //민원 상세조회 이벤트
            function complain_attach(complain_seq,tran_seq) {

                w1 = complainMasterWindow.createWindow("w1", 25, 25, 700, 850);
                w1.setText("민원 수정");
                complainMasterWindow.window('w1').setModal(true);
                
                var ins_start_dt = $("#complainForm input[name=ins_start_dt]").val();
                var ins_end_dt = $("#complainForm input[name=ins_end_dt]").val();
                w1.attachURL("<c:url value="/complain/complainMasterUpdate" />" + "?complain_seq=" + complain_seq + "&tran_seq=" + tran_seq + "&ins_start_dt=" + ins_start_dt + "&ins_end_dt=" + ins_end_dt+"&updatepopup=Y", true);   
                
                return false;
                        
            }
            //민원 정보 조회
            function complainSearch() {
                $("#page_no").val("1");
                 document.complainForm.submit();
            }
            
            function setSens(id, k) {
                // update range
                if (k == "min") {
                    complainCalendar.setSensitiveRange(byId(id).value, null);
                } else {
                    complainCalendar.setSensitiveRange(null, byId(id).value);
                }
            }

            function byId(id) {
                return document.getElementById(id);
            }        

                function complainMasterInit($form) {

                    searchFormInit($form);
                    complain_date_search("week");
                } 
                
                function ComplainExcel(){
                    $("#complainForm").attr("action","<c:url value="/complain/basic_complainListExcel" />");
                    document.getElementById("complainForm").submit();            
                }
                
                
               
            function makepage(startIndex) 
            {
                var pagingHTML 		= "";
		var page 		= parseInt($("#page_no").val());
		var totalCount		= parseInt($("#total_cnt").val());
                
		var pageBlock		= parseInt($("#page_size").val());
		var navigatorNum    = 10;
		var firstPageNum	= 1;
		var lastPageNum		= Math.floor((totalCount-1)/pageBlock) + 1;
		var previewPageNum  = page == 1 ? 1 : page-1;
		var nextPageNum		= page == lastPageNum ? lastPageNum : page+1;
		var indexNum		= startIndex <= navigatorNum  ? 0 : parseInt((startIndex-1)/navigatorNum) * navigatorNum;
				
		if (totalCount > 1) 
                {
					
                    if (startIndex > 1) 
                    {
                        pagingHTML += "<a href='#' id='"+firstPageNum+"'><img src='../resources/images/btn_first.gif' width='11' height='11'/></a> ";
			pagingHTML += "<a href='#' id='"+previewPageNum+"'><img src='../resources/images/btn_prev.gif' width='11' height='11'/></a>";
                    }
		
                    for (var i=1; i<=navigatorNum; i++) 
                    {
			var pageNum = i + indexNum;
					
			if (pageNum == startIndex) 
                            pagingHTML += "<a href='#' id='"+pageNum+"'>"+pageNum+"</a> ";
			else 
                            pagingHTML += "<a href='#' id='"+pageNum+"'>"+pageNum+"</a>  ";
					
			if (pageNum==lastPageNum)
			break;
		    }
					
		    if (startIndex < lastPageNum) {
			pagingHTML += "<a href='#' id='"+nextPageNum+"'><img src='../resources/images/btn_next.gif' width='11' height='11'/></a> ";
			pagingHTML += "<a href='#' id='"+lastPageNum+"'><img src='../resources/images/btn_end.gif' width='11' height='11'/></a>";
		    }
					
		}
				
				
		$("#paging").html(pagingHTML);
		
		$("#paging a").click(function (e) {
			paging_move($(this).attr('id'));
		});

	}        
        
        function paging_move(pagenum)
        {
            //alert(pagenum);
            $("#page_no").val(pagenum);
             document.complainForm.submit();
        }                                
                
        </script>
