<%-- 
    Document   : complainMasterList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>민원관리</title>             
        </head>
        <body>
</c:if>
       
    <body>
        <div class="right" id="complain_search" style="width:100%;">       
            <form id="complainForm" method="POST" onsubmit="return false" action="<c:url value="/complain/complainMasterList" />" modelAttribute="complainFormBean"> 
                <input type="hidden" name="page_size" value="100" >
                <input type="hidden" name="page_no" value="1" >
                <table>
                    <tr>
                        <div class="label">등록일</div>
                        <div class="input">
                            <input type="text" size="10" name="ins_start_dt" id="complainForm_from_date" value ="${complainFormBean.ins_start_dt}" onclick="setSens('complainForm_to_date', 'max');" /> ~
                            <input type="text" size="10" name="ins_end_dt" id="complainForm_to_date" value="${complainFormBean.ins_end_dt}" onclick="setSens('complainForm_from_date', 'min');" />
                        </div>
                        <div class ="input"><input type="button" name="week" value="1주"/></div>
                        <div class ="input"><input type="button" name="month" value="1달"/></div>                       
                        
                        <div class="label2">지급ID</div>
                        <div class="input"><input type="text" name="onffmerch_no" maxlength="30"/></div>
                        
                        <div class="label2">지급ID명</div>
                        <div class="input"><input type="text" name="merch_nm" maxlength="30"/></div>    
                      
                        <div class="label2">UID</div>
                        <div class="input"><input type="text" name="onfftid" maxlength="30"/></div>
                        
                        <div class="label2">UID명</div>
                        <div class="input"><input type="text" name="onfftid_nm" maxlength="30"/></div>    
                        
                        <div class="label2">고객명</div>
                        <div class="input"><input type="text" name="cust_nm" maxlength="10"/></div>    
                        
                        <div class="label2">연락처</div>
                        <div class="input"><input type="text" name="cust_tel" maxlength="15"/></div>    
                        
                        <div class="label">민원종류</div>
                        <div class="input">
                            <select name="complain_cate">
                                <option value="">전체</option>
                                <c:forEach var="code" items="${complainCateList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == complainFormBean.complain_cate}">selected</c:if> >${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>                          
                        
                        <div class="label">완료여부</div>
                        <div class="input">
                            <select name="complete_flag">
                                <option value="tot">전체</option>
                                <c:forEach var="code" items="${completeFlagList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == complainFormBean.complete_flag}">selected</c:if> >${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>       
                        
                        
                        <td><input type="button" name="complain_search" value="조회"/></td>
                         <td><input type="button" name="init" value="초기화"/></td>
                         <td><input type="button" name="complainlist_excel" value="엑셀"/></td>
                    </tr>
                    <c:if test="${complainFormBean.ses_user_cate == '00'}">
                        <div class="action">
                            <button class="addButton">추가</button>
                        </div>                                                                                       
                    </c:if>
               </table>
            </form>
            <div class="paging">
                <div id="complainPaging" style="width: 50%;"></div>
                <div id="complainrecinfoArea" style="width: 50%;"></div>
            </div>                        
        </div>
        
         <script type="text/javascript">
            
            var complaingrid={};
            var complainCalendar;
            var complainMasterWindow;
            
            $(document).ready(function () {
                
                complainCalendar = new dhtmlXCalendarObject(["complainForm_from_date","complainForm_to_date"]);    
                
                var complain_searchForm = document.getElementById("complain_search");
                var tabBarId = tabbar.getActiveTab();
                complain_layout = tabbar.cells(tabBarId).attachLayout("2E");                
                complain_layout.cells('a').hideHeader();
                complain_layout.cells('b').hideHeader();
                complain_layout.cells('a').attachObject(complain_searchForm);
                complain_layout.cells('a').setHeight(55);
                complaingrid = complain_layout.cells('b').attachGrid();
                complaingrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                
                var complainheaders = "";
                //complainheaders += "<center>NO</center>,<center>지급ID번호</center>,<center>지급ID명</center>,<center>UID</center>,<center>UID명</center>,<center>제목</center>,<center>고객명</center>,<center>등록일</center>,<center>응답여부</center>,<center>완료여부</center>";
                complainheaders += "<center>NO</center>,<center>지급ID번호</center>,<center>지급ID명</center>,<center>UID</center>,<center>UID명</center>";
                complainheaders += ",<center>업체Tel1</center>,<center>업체Tel2</center>";
                complainheaders += ",<center>민원종류</center>,<center>고객명</center>,<center>전화번호</center>,<center>카드번호</center>,<center>할부</center>";
                complainheaders += ",<center>승인번호</center>,<center>승인일시</center>,<center>승인금액</center>,<center>취소일</center>,<center>거래상태</center>";
                complainheaders += ",<center>등록일</center>,<center>응답여부</center>,<center>완료여부</center>";
                complaingrid.setHeader(complainheaders);
                complaingrid.setColAlign("center,center,center,center,center,center,center,center,center,center,center,center,center,center,right,center,center,center,center,center");
                complaingrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,txt,edn,txt,txt,txt,txt,txt");
                complaingrid.setInitWidths("50,150,150,150,150,100,100,80,100,120,150,60,90,100,100,80,100,100,100,100");
                complaingrid.setColSorting("str,str,str,str,str,str,str,str,str,str,str,str,str,str,int,str,str,str,str,str");
                
                complaingrid.setNumberFormat("0,000",14);
                
                complaingrid.enableColumnMove(true);
                complaingrid.setSkin("dhx_skyblue");

                complaingrid.enablePaging(true,Number($("#complainForm input[name=page_size]").val()),10,"complainPaging",true,"complainrecinfoArea");
                complaingrid.setPagingSkin("bricks");    
                
                complaingrid.attachEvent("onPageChanged", function(ind,fInd,lInd){

                    if(lInd !==0){
                        $("#complainForm input[name=page_no]").val(ind);
                        complainSearch();
                    }else{
                        return false;
                    }
                    
                });            
                
                
                complaingrid.setColumnHidden(1,true); 
                complaingrid.setColumnHidden(3,true); 
              
                complaingrid.init();
                complaingrid.parse(${ls_complainMasterList},"json");
<c:if test="${complainFormBean.ses_user_cate == '00'}">
                complaingrid.attachEvent("onRowSelect", complain_open_attach);
</c:if>                
                complaingrid.attachEvent("onRowDblClicked", complain_attach);
                
                

                //민원 정보 검색 이벤트
                $("#complainForm input[name=complain_search]").click(function () {
                    
                    $("input[name=page_no]").val("1");
                    complainSearch();
                    
                });
                
                //민원 관리 등록
                $("#complainForm .addButton").button({
                    icons: {
                        primary: "ui-icon-plusthick"
                    },
                    text: false
                })
                .unbind("click")
                .bind("click",function(e){
                    //민원 등록 이벤트
                    doInsertComplain();
                    return false;
                });              
                
                complainMasterWindow = new dhtmlXWindows();
                  
            });
            
            //엑셀다운로드 이벤트
            $("#complainForm input[name=complainlist_excel]").click(function () {
                ComplainExcel();
             });                 
            
            $("#complainForm input[name=week]").click(function(){
                complain_date_search("week");
            });

            $("#complainForm input[name=month]").click(function(){
                complain_date_search("month");
            });
            //검색조건 초기화
            $("#complainForm input[name=init]").click(function () {

                complainMasterInit($("#complainForm"));

            });
            //민원 상세조회 이벤트
            function complain_attach(rowid, col) {

                w1 = complainMasterWindow.createWindow("w1", 25, 25, 700, 850);
                w1.setText("민원 수정");
                complainMasterWindow.window('w1').setModal(true);
                var complain_seq = complaingrid.getUserData(rowid, 'complain_seq');
                var tran_seq = complaingrid.getUserData(rowid, 'tran_seq');
                var ins_start_dt = $("#complainForm input[name=ins_start_dt]").val();
                var ins_end_dt = $("#complainForm input[name=ins_end_dt]").val();
                w1.attachURL("<c:url value="/complain/complainMasterUpdate" />" + "?complain_seq=" + complain_seq + "&tran_seq=" + tran_seq + "&ins_start_dt=" + ins_start_dt + "&ins_end_dt=" + ins_end_dt+"&updatepopup=Y", true);   
                
                return false;
                        
            }
<c:if test="${complainFormBean.ses_user_cate == '00'}">            
            function complain_open_attach(rowid, col) {
                
                if(col == '2')
                {
                    var onffmerchno = complaingrid.cells(rowid,1).getValue();
                    w1 = complainMasterWindow.createWindow("w1", 150, 150,  800, 700);
                    w1.setText("지급ID 정보 수정");
                    complainMasterWindow.window('w1').setModal(true);
                    w1.attachURL("<c:url value="/merchant/merchantMasterUpdate" />" + "?onffmerch_no=" + onffmerchno+"&popupchk=1",true);
                    
                    complaingrid.clearSelection();
                }
                else if(col == '4')
                {
                    var onfftid =  complaingrid.cells(rowid,3).getValue();
                    w1 = complainMasterWindow.createWindow("w1", 150, 150,  800, 700);
                    w1.setText("UID정보 수정");
                    complainMasterWindow.window('w1').setModal(true);
                    w1.attachURL("<c:url value="/terminal/terminalMasterUpdate" />" + "?onfftid=" + onfftid+"&popupchk=1",true); 
                    complaingrid.clearSelection();
                }
                
                return false;
                        
            }            
</c:if>              
            //민원 정보 조회
            function complainSearch() {
                $("#complainForm").attr("action","<c:url value="/complain/complainMasterList" />");
                $.ajax({
                    url: $("#complainForm").attr("action"),
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#complainForm").serialize(),
                    success: function (data) {

                        complaingrid.clearAll();
                        
                        complaingrid.parse($.parseJSON(data), "json");

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }
            
            function setSens(id, k) {
                // update range
                if (k == "min") {
                    complainCalendar.setSensitiveRange(byId(id).value, null);
                } else {
                    complainCalendar.setSensitiveRange(null, byId(id).value);
                }
            }

            function byId(id) {
                return document.getElementById(id);
            }        

            //민원 등록
            function doInsertComplain(){

                w1 = complainMasterWindow.createWindow("w1", 25, 25, 700, 500);
                w1.setText("민원 등록");
                complainMasterWindow.window('w1').setModal(true);
                w1.attachURL("<c:url value="/complain/complainMasterInsert" />", true);   

            } 
            function complain_date_search(day){
                    var oneDate = 1000 * 3600 * 24; 
                    var now = new Date();
                    var week = new Date(now.getTime() + (oneDate*-6));
                    var month = new Date(now.getTime() + (oneDate*-31));
                    var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                    var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                    var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                    var date=new Date();

                    if(day=="today"){
                        $("#complainForm input[id=complainForm_from_date]").val(nowTime);
                        $("#complainForm input[id=complainForm_to_date]").val(nowTime);
                    }else if(day=="week"){
                        $("#complainForm input[id=complainForm_from_date]").val(weekTime);
                        $("#complainForm input[id=complainForm_to_date]").val(nowTime);
                    }else{
                        $("#complainForm input[id=complainForm_from_date]").val(monthTime);
                        $("#complainForm input[id=complainForm_to_date]").val(nowTime);
                    }
                }
            var dhxSelPopupWins=new dhtmlXWindows();
                                   
                //지급ID번호 팝업 클릭 이벤트
                $("#complainForm input[name=onffmerch_no]").unbind("click").bind("click", function (){

                    onoffcomplainMerchSelectPopup();

                });                           
                

                function onoffcomplainMerchSelectPopup(){
                    onoffObject.onffmerch_no = $("#complainForm input[name=onffmerch_no]");
                    onoffObject.merch_nm = $("#complainForm input[name=merch_nm]");
                    //지급ID정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("가맹점 선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                }
               

                
                //UID 팝업 클릭 이벤트
                $("#complainForm input[name=onfftid]").unbind("click").bind("click", function (){

                    onoffcomplainTidSelectPopup();

                });                                
                
                function onoffcomplainTidSelectPopup(){
                    onoffObject.onfftid = $("#complainForm input[name=onfftid]");
                    onoffObject.onfftid_nm = $("#complainForm input[name=onfftid_nm]");
                    //UID 팝업
                    w2 = dhxSelPopupWins.createWindow("OnofftidSelectPopUp", 20, 30, 640, 480);
                    w2.setText("UID 선택페이지");
                    dhxSelPopupWins.window('OnofftidSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnofftidSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnofftidSelectPopUp"/>', true);   
                }  
                
                function complainMasterInit($form) {

                    searchFormInit($form);
                    complain_date_search("week");
                } 
                
                function ComplainExcel(){
                    $("#complainForm").attr("action","<c:url value="/complain/complainListExcel" />");
                    document.getElementById("complainForm").submit();            
                }                
                
        </script>
    </body>
</html>
