<%-- 
    Document   : cardbinMasterInsert
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>MID 등록</title>
        </head>
        <body>
</c:if>
<%
    String strCurDate = com.onoffkorea.system.common.util.Util.getTodayDate();
    String strTpDefaultEndDt = "20991231";
%>
            <script type="text/javascript">
                               
                $(document).ready(function () {
                    
                    myCalendar = new dhtmlXCalendarObject(["view_start_dt","view_end_dt"]);
                    
                    
                    //cardbin 정보 등록 이벤트
                    $("#cardbinFormInsert input[name=insert]").click(function(){
                       doInsertCardbinMaster(); 
                    });              
                    
                    //cardbin 정보 등록 닫기
                    $("#cardbinFormInsert input[name=close]").click(function(){
                        cardbinMasterWindow.window("w1").close();
                    });                            
                    
                });

                //사업체 정보 등록
                function doInsertCardbinMaster(){
                    
                    if(!window.confirm("등록하시겠습니까?")){
                        return;
                    }

                    if($("#cardbinFormInsert input[name=iss_cd]").val() == "")
                    {
                      alert("발급사는 필수사항 입니다.");
                      $("#cardbinFormInsert input[name=iss_cd]").focus();
                      return;
                    }
                    
                    if($("#cardbinFormInsert input[name=cardbin]").val() == "")
                    {
                      alert("카드빈은 필수사항 입니다.");
                      $("#cardbinFormInsert input[name=cardbin]").focus();
                      return;
                    }
                    
                   
                    $.ajax({
                        url : $("#cardbinFormInsert").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#cardbinFormInsert").serialize(),
                        success : function(data) {
                            alert("등록 완료");
                            cardbinMasterGrid.clearAll();
                            doSearchCardbinMaster();
                            cardbinMasterWindow.window("w1").close();
                        },
                        error : function() { 
                                alert("등록 실패");
                        }
                    });	
                    
                }                   

            </script>
        
            <form id="cardbinFormInsert" method="POST" action="<c:url value="/cardbin/cardbinMasterInsert" />" modelAttribute="cardbinFormBean">
                <table class="gridtable" height="100%" width="100%">                    
                    <tr>
                        <td align='left' colspan='4'>- 카드빈 관리</td>                        
                    </tr>
                    <tr>
                        <td class="headcol" width="20%">발급사*</td>
                        <td>
                            <select name="iss_cd" >
                                    <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${IssCdList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td class="headcol" width="20%">카드빈*</td>
                        <td><input name="cardbin" maxlength="40" onblur="javascript:checkLength(this,40);" /></td>
                    </tr>
                    <tr>
                        <td class="headcol">Memo</td>
                        <td colspan="3"><textarea name="memo" cols="80" rows="5"></textarea></td>                        
                    </tr>                          
                    <tr>
                        <td colspan="4" align="center"><input type="button" name="insert" value="등록"/>&nbsp;<input type="button" name="close" value="닫기"/></td>
                    </tr>
                </table>
                <input style="VISIBILITY: hidden; WIDTH: 0px;overflow: hidden">
            </form>        
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

