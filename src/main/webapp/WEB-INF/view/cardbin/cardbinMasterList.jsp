<%-- 
    Document   : cardbinMasterList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>카드빈 정보 관리</title>
        </head>
        <body>
</c:if>
        <script type="text/javascript">
                var cardbinMasterGrid={};
                var cardbinMasterWindow;
                var cardbinMasterLayout;
                
                $(document).ready(function () {
                    var cardbinSearch = document.getElementById("cardbinSearch");            
                    cardbinMasterLayout = tabbar.cells("1-4").attachLayout("2E");
                    
                    cardbinMasterLayout.cells('a').hideHeader();
                    cardbinMasterLayout.cells('b').hideHeader();
                    cardbinMasterLayout.cells('a').attachObject(cardbinSearch);
                    cardbinMasterLayout.cells('a').setHeight(75);
                    cardbinMasterGrid = cardbinMasterLayout.cells('b').attachGrid();
                    cardbinMasterGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var cardbinMasterGridheaders = "";
                    cardbinMasterGridheaders += "<center>번호</center>,<center>선택</center>,<center>발급사</center>,<center>카드빈</center>,<center>등록일</center>";
                    cardbinMasterGrid.setHeader(cardbinMasterGridheaders);
                    cardbinMasterGrid.setColAlign("center,center,center,center,center");
                    cardbinMasterGrid.setColTypes("txt,ch,txt,txt,txt");
                    cardbinMasterGrid.setInitWidths("60,80,150,200,100");
                    cardbinMasterGrid.setColSorting("str,str,str,str,str");
                    cardbinMasterGrid.enableColumnMove(true);
                    cardbinMasterGrid.setSkin("dhx_skyblue");
    
                    //cardbinMasterGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxToolbar/" />");
                    cardbinMasterGrid.enablePaging(true,Number($("#cardbinForm input[name=page_size]").val()),10,"cardbinMasterListPaging",true,"cardbinMasterListrecinfoArea");
                    cardbinMasterGrid.setPagingSkin("bricks");

                    //페이징 처리
                    cardbinMasterGrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        if(lInd !==0){
                           $("#cardbinForm input[name=page_no]").val(ind);                  
                            cardbinMasterGrid.clearAll();
                            doSearchCardbinMaster();
                        }else{
                            $("#cardbinForm input[name=page_no]").val(1);
                            cardbinMasterGrid.clearAll();
                            doSearchCardbinMaster();
                        }                           
                    });
                    
                    cardbinMasterGrid.init();
                    cardbinMasterGrid.parse(${cardbinMasterList}, "json");
                    cardbinMasterGrid.attachEvent("onRowDblClicked", doUpdateFormCardbinMaster);
                    
                    cardbinMasterWindow = new dhtmlXWindows();
                    cardbinMasterWindow.attachViewportTo("cardbinList");

                    //버튼 추가
                    $("#cardbinForm input[name=cardbin_searchButton]").bind("click",function(e){
                        // 정보 조회 이벤트
                        /*
                        $("#cardbinForm input[name=page_no]").val("1");
                        cardbinMasterGrid.clearAll();
                        doSearchCardbinMaster();
                        */
                        cardbinMasterGrid.changePage(1);
                        return false;                        
                    });


                    $("#cardbinForm .addButton").button({
                        icons: {
                            primary: "ui-icon-plusthick"
                        },
                        text: false
                    })
                    .unbind("click")
                    .bind("click",function(e){
                        //정보 등록 이벤트
                        doInsertFormCardbinMaster();
                        return false;
                    });

                    $("#cardbinForm .deleteButton").button({
                        icons: {
                            primary: "ui-icon-minusthick"
                        },
                        text: false
                    })
                    .unbind("click")
                    .bind("click",function(e){
                        // 정보 삭제 이벤트
                        doDeleteCardbinMaster();
                        return false;
                    });
                    //검색조건 초기화
                    $("#cardbinForm input[name=init]").click(function () {
                    
                        cardBeanMasterInit($("#cardbinForm"));

                    });  

                });

                // 정보 수정
                function doUpdateFormCardbinMaster(rowid, col){
                    w1 = cardbinMasterWindow.createWindow("w1", 150, 150, 700, 300);
                    w1.setText("카드빈 수정");
                    cardbinMasterWindow.window('w1').setModal(true);
                    w1.attachURL("<c:url value="/cardbin/cardbinMasterUpdate" />" + "?cardbin_seq=" + rowid,true);   
                    
                }

                function resizeOption($id){
                    //css 변경 : resize시 자동 변경됨.
                    $id.css("width","100%");
                    $id.css("height","100%");
                }


                // 정보 조회
                function doSearchCardbinMaster(){
                    $.ajax({
                        url : $("#cardbinForm").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#cardbinForm").serialize(),
                        success : function(data) {
                            //cardbinMasterGrid.clearAll();
                            //cardbinMasterGrid.parse(jQuery.parseJSON(data), "json");
                            var jsonData = $.parseJSON(data);     
                            cardbinMasterGrid.parse(jsonData,"json");
                            //resizeOption($("#cardbinList.gridbox"));
                        },
                        error : function() {
                            alert("조회 실패");
                        }
                    });
                }

                // 정보 삭제
                function doDeleteCardbinMaster(){

                    var rowIds = cardbinMasterGrid.getCheckedRows(1);
                    $("#cardbinForm input[name=cardbin_seqs]").val(rowIds.split(","));

                    if(rowIds.length < 1){
                        alert("삭제할 카드빈을 선택하십시오.");
                        return;
                    }

                    if(!window.confirm("삭제하시겠습니까?")){
                        return;
                    }

                    $.ajax({
                        url : "<c:url value="/cardbin/cardbinMasterDelete" />",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#cardbinForm").serialize(),
                        success : function(data) {
                            cardbinMasterGrid.clearAll();
                            doSearchCardbinMaster();
                        },
                        error : function(jqXHR, textStatus, errorThrown) {
                            alert("삭제 실패 = " + errorThrown);

                        }
                    });
                }

                // 정보 등록
                function doInsertFormCardbinMaster(){

                    w1 = cardbinMasterWindow.createWindow("w1", 150, 150, 700, 250);
                    //w1 = cardbinMasterWindow.createWindow("editwin", 150, 150, 700, 400);
                    w1.setText("카드빈 등록");
                    cardbinMasterWindow.window('w1').setModal(true);
                    w1.attachURL("<c:url value="/cardbin/cardbinMasterInsert" />",true);   

                }
                function cardBeanMasterInit($form) {
                    searchFormInit($form);
                } 

            </script>            
            <div class="searchForm1row" id="cardbinSearch" style="display:none;width:100%;height:100%;background-color:white;">
                <form id="cardbinForm" method="POST" action="<c:url value="/cardbin/cardbinMasterList" />" modelAttribute="cardbinFormBean">
                       <input type="hidden" name="page_size" value="100" />
                       <input type="hidden" name="page_no" value="1" />
                       <input type="hidden" name="cardbin_seqs" value="" />
                        
                        <div class="label">발급사</div>
                        <div class="input"><select name="iss_cd" >
                                    <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${IssCdList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select></div>

                        <div class="label">카드빈</div>
                        <div class="input"><input type="text" name="cardbin" maxlength="40"/></div>
                        <input type="button" name="cardbin_searchButton" value="조회"/>
                        <input type="button" name="init" value="검색조건지우기"/>
                        <div class="action">
                            <button class="addButton">추가</button>
                            <button class="deleteButton">삭제</button>
                        </div>
                </form>
                <div id="cardbinMasterListPaging" style="width: 100%;"></div>
                <div id="cardbinMasterListrecinfoArea" style="width: 100%;"></div>                        
            </div>
            <div id="cardbinList" style="width:100%;height:100%;background-color:white;"></div>
 <c:if test="${!ajaxRequest}">    
        </body>
</html>
</c:if>

