<%-- 
    Document   : cardbinMasterUpdate
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>카드빈 수정</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">
                
                $(document).ready(function () {
                    
                    myCalendar = new dhtmlXCalendarObject(["view_start_dt","view_end_dt"]);
                    
                    //CARDBIN 수정 이벤트
                    $("#cardbinFormUpdate input[name=update]").click(function(){
                       doUpdateCardbinMaster(); 
                    });               
                    
                    //CARDBIN 수정 닫기
                    $("#cardbinFormUpdate input[name=close]").click(function(){       

                        cardbinMasterWindow.window("w1").close();
                        
                    });                           
                    
                });
                

                //CARDBIN 수정
                function doUpdateCardbinMaster(){
                    
                    if(!window.confirm("수정하시겠습니까?")){
                        return;
                    }

                    if($("#cardbinFormInsert input[name=iss_cd]").val() == "")
                    {
                      alert("발급사는 필수사항 입니다.");
                      $("#cardbinFormInsert input[name=iss_cd]").focus();
                      return;
                    }
                    
                    if($("#cardbinFormInsert input[name=cardbin]").val() == "")
                    {
                      alert("카드빈은 필수사항 입니다.");
                      $("#cardbinFormInsert input[name=cardbin]").focus();
                      return;
                    }        
        
                    
                    $("#cardbinFormUpdate").submit(function() {
                        $.post($(this).attr("action"), $(this).serialize(), function(html) {
                            alert("수정 완료");
                            cardbinMasterGrid.clearAll();
                            doSearchCardbinMaster();
                            cardbinMasterWindow.window("w1").close();
                        });
                    return false;  
                    });	                    
        
                }                   

            </script>
            <form id="cardbinFormUpdate" method="POST" action="<c:url value="/cardbin/cardbinMasterUpdate" />" modelAttribute="cardbinFormBean">
                <input type="hidden" name="cardbin_seq" value="${tb_Cardbin.cardbin_seq}" />
                <table class="gridtable" height="100%" width="100%">                    
                    <tr>
                        <td align='left' colspan='4'>- 카드빈 관리</td>                        
                    </tr>
                    <tr>
                        <td class="headcol" width="20%">발급사*</td>
                        <td>
                            <select name="iss_cd" >
                                    <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${IssCdList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Cardbin.iss_cd}">selected</c:if> >${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td class="headcol" width="20%">카드빈*</td>
                        <td><input name="cardbin" maxlength="50" value="${tb_Cardbin.cardbin}"  onblur="javascript:checkLength(this,50);" /></td>
                    </tr>
                    <tr>
                        <td class="headcol">Memo</td>
                        <td colspan="3"><textarea name="memo" cols="80" rows="5">${tb_Cardbin.memo}</textarea></td>                        
                    </tr>
                    <tr>
                        <td class="headcol">등록일</td>
                        <td>${tb_Cardbin.ins_dt}</td>
                        <td class="headcol">등록자</td>
                        <td>${tb_Cardbin.ins_user}</td>
                    </tr>                          
                    <tr>
                        <td colspan="4" align="center"><input type="submit" name="update" value="수정"/>&nbsp;<input type="button" name="close" value="닫기"/></td>
                    </tr>
                </table>
            </form>
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

