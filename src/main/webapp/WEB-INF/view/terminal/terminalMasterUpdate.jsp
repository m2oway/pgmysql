<%-- 
    Document   : terminalMasterInsert
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>UID 정보 등록</title>
        </head>
        <body>
</c:if>
<%
    String strCurDate = com.onoffkorea.system.common.util.Util.getTodayDate();
    String strTpDefaultEndDt = "20991231";
    
    String strTpPopChk = request.getParameter("popupchk");
    
    if(strTpPopChk == null)
    {
        strTpPopChk="";
    }
%>
            <script type="text/javascript">
                var regobj = new RegExp('-',"gi");
                var dhxSelPopupWins;
		dhxSelPopupWins = new dhtmlXWindows();
           
                               
                $(document).ready(function () {
                    
                    terminal_layout = new dhtmlXLayoutObject("onfftid_cmsVP2", "1C", "dhx_skyblue");
                    terminal_layout.cells('a').hideHeader();

                    terminal_mygrid = terminal_layout.cells('a').attachGrid();
                    terminal_mygrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var mheaders = "";
                    mheaders += "<center>수수료</center>,<center>적용시작일</center>,<center>적용종료일</center>,<center>MEMO</center>";
                    terminal_mygrid.setHeader(mheaders);
                    terminal_mygrid.setColAlign("center,center,center,center");
                    terminal_mygrid.setColTypes("txt,txt,txt,txt");
                    terminal_mygrid.setInitWidths("100,100,100,200");
                    terminal_mygrid.setColSorting("str,str,str,str");
                    terminal_mygrid.enableColumnMove(false);
                    terminal_mygrid.setSkin("dhx_skyblue");
                    terminal_mygrid.init();
                    
                    terminal_mygrid.parse(${terminalDtlList}, "json");
                    terminal_mygrid.attachEvent("onRowDblClicked", doSelectCmsInfo);
        
                    myCalendar = new dhtmlXCalendarObject(["view_onfftid_cms_start_dt","view_onfftid_cms_end_dt"]);
                                        
                    
                   //정보 수정 이벤트
                    $("#terminalFormUpdate input[name=update]").click(function(){
                       doUpdateTerminalMaster(); 
                    });              
                    
                   //정보 삭제 이벤트                   
                    $("#terminalFormUpdate input[name=delete]").click(function(){
                        doDeleteTerminalMaster(); 
                    });      
                    
                    //정보 등록 닫기
                    $("#terminalFormUpdate input[name=close]").click(function(){
                         terminalMasterWindow.window("w1").close();
                    });                           
                    
                });
                
                var dhxSelPopupWins=new dhtmlXWindows();
                dhxSelPopupWins.attachViewportTo("terminalList");
                //가맹점정보 팝업
                function OnOffMerchantSelectWin(){
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("지급ID 선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);
                }        
                
                //결제수단보 팝업
                function PaymtdSelectWin(){
                    w2 = dhxSelPopupWins.createWindow("PaymtdSelectPopUp", 20, 30, 640, 480);
                    w2.setText("결제상품 선택페이지");
                    dhxSelPopupWins.window('PaymtdSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('PaymtdSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/PayMtdSelectPopUp"/>', true);
                }                 
                

                function SelOnoffmerchInfoInput( p_onffmerch_no, p_merch_nm  )
                {
                    $("#terminalFormUpdate input[name=onffmerch_no]").attr('value',p_onffmerch_no);
                    $("#terminalFormUpdate input[name=merch_nm]").attr('value',p_merch_nm);
                }
                
                function SelPaymtdInfoInput( p_terminal_seq, p_terminal_nm  )
                {
                    $("#terminalFormUpdate input[name=pay_mtd_nm]").attr('value',p_terminal_nm);
                    $("#terminalFormUpdate input[name=pay_mtd_seq]").attr('value',p_terminal_seq);
                }
                
                function doSelectCmsInfo()
                {   
                    //<center>수수료</center>,<center>적용시작일</center>,<center>적용종료일</center>,<center>MEMO</center>
                    //          0                       1                             2                      3 
                    $("#terminalFormUpdate input[name=onfftid_cms_seq]").attr('value',terminal_mygrid.getSelectedId());
                    $("#terminalFormUpdate input[name=onfftid_cms_commission]").attr('value',terminal_mygrid.cells(terminal_mygrid.getSelectedId(),0).getValue());                    
                    $("#terminalFormUpdate input[name=view_onfftid_cms_start_dt]").attr('value',terminal_mygrid.cells(terminal_mygrid.getSelectedId(),1).getValue());  
                    $("#terminalFormUpdate input[name=view_onfftid_cms_end_dt]").attr('value',terminal_mygrid.cells(terminal_mygrid.getSelectedId(),2).getValue());  
                    $("#terminalFormUpdate textarea[name=onfftid_cms_memo]").attr('value',terminal_mygrid.cells(terminal_mygrid.getSelectedId(),3).getValue());
                    
                }
                
                                
                function setTransForm()
                {
                         $("#terminalDtlFormInfo input[name=onfftid_cms_seq]").attr('value',$("#terminalFormUpdate input[name=onfftid_cms_seq]").val());
                         $("#terminalDtlFormInfo input[name=commission]").attr('value',$("#terminalFormUpdate input[name=onfftid_cms_commission]").val());
                         $("#terminalDtlFormInfo input[name=start_dt]").attr('value',$("#terminalFormUpdate input[name=view_onfftid_cms_start_dt]").val().replace(regobj,''));
                         $("#terminalDtlFormInfo input[name=end_dt]").attr('value',$("#terminalFormUpdate input[name=view_onfftid_cms_end_dt]").val().replace(regobj,''));
                         $("#terminalDtlFormInfo input[name=memo]").attr('value',$("#terminalFormUpdate textarea[name=onfftid_cms_memo]").val());
                }
                
                function clearSelForm()
                {
                    $("#terminalFormUpdate input[name=onfftid_cms_commission]").attr('value',"");
                    $("#terminalFormUpdate input[name=onfftid_cms_seq]").attr('value',"");                    
                    $("#terminalFormUpdate input[name=view_onfftid_cms_start_dt]").attr('value',"");  
                    $("#terminalFormUpdate input[name=view_onfftid_cms_end_dt]").attr('value',"");  
                    $("#terminalFormUpdate textarea[name=onfftid_cms_memo]").attr('value',"");
        
                    $("#terminalDtlFormInfo input[name=onfftid_cms_seq]").attr('value',"");
                    $("#terminalDtlFormInfo input[name=commission]").attr('value',"");
                    $("#terminalDtlFormInfo input[name=start_dt]").attr('value',"");
                    $("#terminalDtlFormInfo input[name=end_dt]").attr('value',"");
                    $("#terminalDtlFormInfo input[name=memo]").attr('value',"");
                }                
                
                // 정보 조회
                function doSearchTerminalDtlList(){
                    $.ajax({
                        url : "../terminal/terminalDtlList",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#terminalDtlFormInfo").serialize(),
                        success : function(data) {
                            terminal_mygrid.clearAll();
                            terminal_mygrid.parse(jQuery.parseJSON(data), "json");
                        },
                        error : function() { 
                                alert("조회 실패");
                        }
                    });	            
                }                              
                                
                
                function addRow()
                {
                   var strTpStartdt =  $("#terminalFormUpdate input[name=view_onfftid_cms_start_dt]").val().replace(regobj,'');
                   var strTpEnddt =  $("#terminalFormUpdate input[name=view_onfftid_cms_end_dt]").val().replace(regobj,'');

                  
                   if($("#terminalFormUpdate input[name=onfftid_cms_commission]").val() == '') 
                   {
                       alert('수수료는 필수 입니다.');
                       $("#terminalFormUpdate input[name=onfftid_cms_commission]").focus();
                       return false;
                   }
                   
                  if($("#terminalFormUpdate input[name=view_onfftid_cms_start_dt]").val() == '') 
                   {
                       alert('시작일은 필수 입니다.');
                       $("#terminalFormUpdate input[name=view_onfftid_cms_start_dt]").focus();
                       return false;
                   }
                   
                  if($("#terminalFormUpdate input[name=view_onfftid_cms_end_dt]").val() == '') 
                   {
                       alert('종료일 필수 입니다.');
                       $("#terminalFormUpdate input[name=view_onfftid_cms_end_dt]").focus();
                       return false;
                   }         
                   
                   var chkflg = 0;
                   terminal_mygrid.forEachRow(function(id){
                        //if(chk_onfftid_cms_seq != id )
                        {
                             // "<center>수수료</center>,<center>적용시작일</center>,<center>적용종료일</center>,<center>MEMO</center>";
                             // 종료일이 시작일보다 작을 경우
                            if(strTpStartdt <= terminal_mygrid.cells(id,2).getValue())
                            {
                                if(strTpEnddt >= terminal_mygrid.cells(id,1).getValue())
                                 {
                                     chkflg = 1;
                                 }
                                 else
                                 {
                                     //시작일이 종료일 보다 작을 경우
                                     if(strTpStartdt >= terminal_mygrid.cells(id,2).getValue())
                                     {
                                          chkflg = 1;
                                     }
                                 }
                            }
                            
                        }
                   });
        
                   if(chkflg==1) 
                   {
                       alert('입력하시려는 수수료의 날자에 이상이 있습니다.');
                       return false;
                   }                   
                   
                   setTransForm();
                   
                   $.ajax({
                        url :  "../terminal/terminalDtlInsert",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#terminalDtlFormInfo").serialize(),
                        success : function(data) {
                            if(data == '1')
                            {
                                alert("등록 완료");
                                terminal_mygrid.clearAll();
                                doSearchTerminalDtlList();
                                clearSelForm();
                            }
                            else
                            {
                                alert("등록 실패");
                            }
                        },
                        error : function() { 
                                alert("등록 실패");
                        }
                    });

                }
                
                function modRow()
                {
                   var strTpStartdt =  $("#terminalFormUpdate input[name=view_onfftid_cms_start_dt]").val().replace(regobj,'');
                   var strTpEnddt =  $("#terminalFormUpdate input[name=view_onfftid_cms_end_dt]").val().replace(regobj,'');

                   var chk_onfftid_cms_seq = $("#terminalFormUpdate input[name=onfftid_cms_seq]").val();   
                   
                   if($("#terminalFormUpdate input[name=onfftid_cms_commission]").val() == '') 
                   {
                       alert('수수료는 필수 입니다.');
                       $("#terminalFormUpdate input[name=onfftid_cms_commission]").focus();
                       return false;
                   }
                   
                  if($("#terminalFormUpdate input[name=view_onfftid_cms_start_dt]").val() == '') 
                   {
                       alert('시작일은 필수 입니다.');
                       $("#terminalFormUpdate input[name=view_onfftid_cms_start_dt]").focus();
                       return false;
                   }
                   
                  if($("#terminalFormUpdate input[name=view_onfftid_cms_end_dt]").val() == '') 
                   {
                       alert('종료일 필수 입니다.');
                       $("#terminalFormUpdate input[name=view_onfftid_cms_end_dt]").focus();
                       return false;
                   }         
                   
                   var chkflg = 0;
                   
                   terminal_mygrid.forEachRow(function(id){
                       if(chk_onfftid_cms_seq != id )
                        {
                            //alert(chk_onfftid_cms_seq);
                            //alert(id);                            
                             // "<center>수수료</center>,<center>적용시작일</center>,<center>적용종료일</center>,<center>MEMO</center>";
                             // 종료일이 시작일보다 작을 경우
                            //if(strTpStartdt <= terminal_mygrid.cells(id,2).getValue())
                            //if(chk_onfftid_cms_seq >= id)
                            if(chk_onfftid_cms_seq >= id)
                            {
                                //alert("strTpEnddt : " + strTpEnddt);
                                //alert("검사 시작일 terminal_mygrid.cells(id,1).getValue() : " +terminal_mygrid.cells(id,1).getValue());
                                //alert("검사 종료일 terminal_mygrid.cells(id,2).getValue() : " +terminal_mygrid.cells(id,2).getValue());
                                
                                //종료일 검사
                                //입력 종료일보다 큰 시작일이 있다면 error 
                                if(strTpEnddt <= terminal_mygrid.cells(id,1).getValue())
                                {
                                     chkflg = 1;
                                }
                                
                                //시작일 검사
                                //입력 시작일보다 큰 종료일이 있다면 error
                                if(strTpStartdt <= terminal_mygrid.cells(id,2).getValue())
                                {
                                    chkflg = 1;
                                }
                            }
                            
                        }
                   });
        
                   if(chkflg==1) 
                   {
                       alert('수정하시려는 수수료의 날자에 이상이 있습니다.');
                       return false;
                   }                   
                   
                   setTransForm();
                   
                   $.ajax({
                        url :  "../terminal/terminalDtlUpdate",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#terminalDtlFormInfo").serialize(),
                        success : function(data) {
                            if(data == '1')
                            {
                                alert("수정 완료");
                                terminal_mygrid.clearAll();
                                doSearchTerminalDtlList();
                                clearSelForm();
                            }
                            else
                            {
                                alert("수정 실패");
                            }
                        },
                        error : function() { 
                                alert("수정 실패");
                        }
                    });

                }                
                
                
                function delRow()
                {
                    if(confirm('삭제하시겠습니까?'))
                    {
                        setTransForm();
                        
                        if($("#terminalFormUpdate input[name=onfftid_cms_seq]").val() == "")
                        {
                            alert('삭제할 수수료정보를 선택해주세요.');
                            return false;
                        }  

                        $.ajax({
                             url :  "../terminal/terminalDtlDelete",
                             type : "POST",
                             async : true,
                             dataType : "json",
                             data: $("#terminalDtlFormInfo").serialize(),
                             success : function(data) {
                                 if(data == '1')
                                 {
                                    alert("삭제 완료");
                                    terminal_mygrid.clearAll();
                                    doSearchTerminalDtlList();
                                    clearSelForm();
                                }
                                else
                                {
                                    alert("삭제 실패");
                                }
                             },
                             error : function() { 
                                     alert("삭제 실패");
                             }
                         });
                    }

                }

                // 정보 등록
                function doUpdateTerminalMaster(){
                    
                    if(!window.confirm("수정하시겠습니까?")){
                        return;
                    }
                    
                    if($("#terminalFormUpdate select[name=pay_chn_cate]").val() == "")
                    {
                      alert("결제채널은 필수사항 입니다.");
                       $("#terminalFormUpdate select[name=pay_chn_cate]").focus();
                      return;
                    }
                    
                    if($("#terminalFormUpdate input[name=onffmerch_no]").val() == "")
                    {
                      alert("지급ID정보는 필수사항 입니다.");                      
                      return;
                    }
                    
                    if($("#terminalFormUpdate select[name=svc_stat]").val() == "")
                    {
                      alert("상태는 필수사항 입니다.");
                      $("##terminalFormUpdate select[name=svc_stat]").focus();
                      return;
                    }
                    
                    if($("#terminalFormUpdate input[name=onfftid_nm]").val() == "")
                    {
                      alert("UID명은 필수사항 입니다.");
                      $("#terminalFormUpdate input[name=onfftid_nm]").focus();
                      return;
                    }                    
                    
                    if($("#terminalFormUpdate input[name=pay_mtd_nm]").val() == "")
                    {
                      alert("결제상품은 필수사항 입니다.");
                      return;
                    }         
                    
                   
                    $.ajax({
                        url : $("#terminalFormUpdate").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#terminalFormUpdate").serialize(),
                        success : function(data) {
                            //alert(data);
                            if(data=='1')
                            {
                                alert("수정 완료");
<%
    if(strTpPopChk.equals(""))
    {
%>   
                                terminalMasterGrid.clearAll();
                                doSearchTerminalMaster();
                                terminalMasterWindow.window("w1").close();
<%
    }
%>                                
                            }
                            else
                            {
                                alert("수정 실패");
                            }
                        },
                        error : function() { 
                                alert("수정 실패");
                        }
                    });
                }             
                
                // 정보 등록
                function doDeleteTerminalMaster(){
                    
                    if(!window.confirm("삭제하시겠습니까?")){
                        return;
                    }
                    
                    $.ajax({
                        url : "../terminal/terminalMasterDelete",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#terminalFormUpdate").serialize(),
                        success : function(data) {
                            //alert(data);
                            if(data=='1')
                            {
                                alert("삭제 완료");
                                terminalMasterGrid.clearAll();
                                doSearchTerminalMaster();
                                terminalMasterWindow.window("w1").close();
                            }
                            else
                            {
                                alert("삭제 실패");
                            }
                        },
                        error : function() { 
                                alert("삭제 실패");
                        }
                    });
                }                      

            </script>
        <div style='width:100%;height:100%;overflow:auto;'>            
            <form id="terminalFormUpdate" method="POST" action="<c:url value="/terminal/terminalMasterUpdate" />" modelAttribute="TerminalFormBean">
                <table class="gridtable" height="100%" width="100%">   
                    <tr>
                        <td align='left' colspan='4'>- 지급ID 정보</td>                        
                    </tr>
                    <tr>                        
                        <td class="headcol">지급ID명*</td>
                        <td><input name="merch_nm" id="merch_nm"  value="${tb_Terminal.merch_nm}" readonly onclick='javascript:OnOffMerchantSelectWin();'/></td>
                        <td class="headcol">지급ID*</tD>
                        <td><input name="onffmerch_no" id="onffmerch_no"  value="${tb_Terminal.onffmerch_no}" readonly onclick='javascript:OnOffMerchantSelectWin();'/></td>
                    </tr>
                    <tr>
                        <td align='left' colspan='4'>- UID 정보</td>                        
                    </tr>
                    <tr>
                        <td class="headcol">결제채널*</td>
                        <td><select name="pay_chn_cate" >
                                <c:forEach var="code" items="${PayChnCateList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Terminal.pay_chn_cate}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach>
                            </select>                         
                        </td>
                        <td class="headcol">UID상태*</td>
                        <td ><select name="svc_stat" >
                                <c:forEach var="code" items="${UseFlagList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Terminal.svc_stat}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach>
                            </select>                         
                        </td>                        
                    </tr>                                        
                    <tr>                        
                        <td class="headcol" width="20%">UID</td>
                        <td ><input name="onfftid" value="${tb_Terminal.onfftid}" size="25" readonly /></td>
                        <td class="headcol" width="20%">UID명</td>
                        <td ><input name="onfftid_nm" value="${tb_Terminal.onfftid_nm}" size="40" maxlength="50" onblur="javascript:checkLength(this,50);" /></td>                        
                    </tr>
                    <tr>
                        <td class="headcol">결제인증구분*</td>
                        <td ><select name="cert_type" >
                                <c:forEach var="code" items="${CertTypeList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Terminal.cert_type}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach></select> 
                        </td>                     
                        <td class="headcol">결제인증구분*</td>
                        <td ><select name="cncl_auth" >
                                <c:forEach var="code" items="${Cncl_AuthList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == tb_Terminal.cncl_auth}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach></select> 
                        </td>                                
                    </tr>                      
                    <tr>
                        <td class="headcol">Memo</td>
                        <td colspan="3"><textarea name="memo" cols="60" rows="3">${tb_Terminal.memo}</textarea></td>                        
                    </tr>  
                    <tr>
                        <td align='left' colspan='4'>- 결제상품</td>                        
                    </tr>
                    <tr>                        
                        <td class="headcol">결제상품명*</td>
                        <td colspan='3'><input name="pay_mtd_nm" id="pay_mtd_nm" size='30' value="${tb_Terminal.pay_mtd_nm}" readonly onclick='javascript:PaymtdSelectWin();'/><input type='hidden' name="pay_mtd_seq" id='pay_mtd_seq' value="${tb_Terminal.pay_mtd_seq}"></td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center"><input type="button" name="update" value="수정"/>&nbsp;<input type="button" name="delete" value="삭제"/>&nbsp;<input type="button" name="close" value="닫기"/></td>
                    </tr>                         
                    <tr>
                        <td align='left' colspan='4'>- 수수료 정보</td>                        
                    </tr>
                    <tr>                        
                        <td class="headcol" >수수료율*</tD>
                        <td colspan="3"><input name="onfftid_cms_commission" onkeyPress="javascript:InpuOnlyNumber(this);"  /><input type='hidden' name="onfftid_cms_seq"/></td>    
                    </tr>     
                    <tr>
                        <td class="headcol">시작일*</td>
                        <td><input name="view_onfftid_cms_start_dt" id="view_onfftid_cms_start_dt" maxlength="10" value="<%=strCurDate%>" readonly/><input type="hidden" name="onfftid_cms_start_dt"/></td>
                        <td class="headcol">종료일*</tD>
                        <td><input name="view_onfftid_cms_end_dt" id="view_onfftid_cms_end_dt" maxlength="10" value="<%=strTpDefaultEndDt%>" readonly/><input type="hidden" name="onfftid_cms_end_dt"/></td>
                    </tr> 
                    <tr>
                        <td class="headcol">Memo</td>
                        <td colspan="3"><textarea name="onfftid_cms_memo" cols="60" rows="2"></textarea></td>                        
                    </tr>                     
                    <tr>
                        <td align='left' colspan='4' valign="top">
                            <div id = "common_btn" align = "right" style="height:30px;" >
                            <input type="button" name="onfftid_cms_addbt" value="수수료 추가" onclick="javascript:addRow();"/>
                            <input type="button" name="onfftid_cms_addbt" value="수수료 수정" onclick="javascript:modRow();"/>
                            <input type="button" name="onfftid_cms_delbt" value="수수료 삭제" onclick="javascript:delRow();"/>
                            </div>
                            <div id="onfftid_cmsVP2" style="position: relative; width:100%;height:100px;background-color:white;"></div>
                        </td>                        
                    </tr>   
                </table>
            </form>
<form name="terminalDtlFormInfo" id="terminalDtlFormInfo" method="POST"  modelAttribute="TmlCmsFormBean">
    <input type="hidden" name="onfftid_cms_seq" />
    <input type="hidden" name="onfftid" value="${tb_Terminal.onfftid}" />
    <input type="hidden" name="commission"/>
    <input type="hidden" name="start_dt"/>
    <input type="hidden" name="end_dt"/>
    <input type="hidden" name="memo"/>
</form>                    
        </div>
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

