<%-- 
    Document   : terminalMasterInsert
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>UID 정보 등록</title>
        </head>
        <body>
</c:if>
<%
    String strCurDate = com.onoffkorea.system.common.util.Util.getTodayDate();
    String strTpDefaultEndDt = "20991231";
%>
            <script type="text/javascript">
                var regobj = new RegExp('-',"gi");
                var dhxSelPopupWins;
		dhxSelPopupWins = new dhtmlXWindows();
           
                               
                $(document).ready(function () {
                    
                    terminal_layout = new dhtmlXLayoutObject("onfftid_cmsVP2", "1C", "dhx_skyblue");
                    terminal_layout.cells('a').hideHeader();

                    terminal_mygrid = terminal_layout.cells('a').attachGrid();
                    terminal_mygrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var mheaders = "";
                    mheaders += "<center>수수료</center>,<center>적용시작일</center>,<center>적용종료일</center>,<center>MEMO</center>";
                    terminal_mygrid.setHeader(mheaders);
                    terminal_mygrid.setColAlign("center,center,center,center");
                    terminal_mygrid.setColTypes("txt,txt,txt,txt");
                    terminal_mygrid.setInitWidths("100,100,100,200");
                    terminal_mygrid.setColSorting("str,str,str,str");
                    terminal_mygrid.enableColumnMove(false);
                    terminal_mygrid.setSkin("dhx_skyblue");
                    terminal_mygrid.init();
        
                    myCalendar = new dhtmlXCalendarObject(["view_onfftid_cms_start_dt","view_onfftid_cms_end_dt"]);
                                        
                    //terminal 정보 등록 이벤트
                    $("#terminalFormInsert input[name=insert]").click(function(){
                       doInsertTerminalMaster(); 
                    });              
                    
                    //terminal 정보 등록 닫기
                    $("#terminalFormInsert input[name=close]").click(function(){
                         terminalMasterWindow.window("w1").close();
                    });                            
                    
                });
                
                var dhxSelPopupWins=new dhtmlXWindows();
                dhxSelPopupWins.attachViewportTo("terminalList");
                //가맹점정보 팝업
                function OnOffMerchantSelectWin_termMstIns(){
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("지급ID 선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);
                }        
                
                //결제수단보 팝업
                function PaymtdSelectWin_termMstIns(){
                    w2 = dhxSelPopupWins.createWindow("PaymtdSelectPopUp", 20, 30, 640, 480);
                    w2.setText("결제상품 선택페이지");
                    dhxSelPopupWins.window('PaymtdSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('PaymtdSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/PayMtdSelectPopUp"/>', true);
                }                 
                

                function SelOnoffmerchInfoInput( p_onffmerch_no, p_merch_nm  )
                {
                    $("#terminalFormInsert input[name=onffmerch_no]").attr('value',p_onffmerch_no);
                    $("#terminalFormInsert input[name=merch_nm]").attr('value',p_merch_nm);
                    $("#terminalFormInsert input[name=onfftid_nm]").attr('value',p_merch_nm);
                    
                }
                
                function SelPaymtdInfoInput( p_paymtd_seq, p_paymtd_nm  )
                {
                    $("#terminalFormInsert input[name=pay_mtd_nm]").attr('value',p_paymtd_nm);
                    $("#terminalFormInsert input[name=pay_mtd_seq]").attr('value',p_paymtd_seq);
                    
                }
                function addRow()
                {
                   var strTpStartdt =  $("#terminalFormInsert input[name=view_onfftid_cms_start_dt]").val().replace(regobj,'');
                   var strTpEnddt =  $("#terminalFormInsert input[name=view_onfftid_cms_end_dt]").val().replace(regobj,'');

                  
                   if($("#terminalFormInsert input[name=onfftid_cms_commission]").val() == '') 
                   {
                       alert('수수료는 필수 입니다.');
                       $("#terminalFormInsert input[name=onfftid_cms_commission]").focus();
                       return false;
                   }
                   
                  if($("#terminalFormInsert input[name=view_onfftid_cms_start_dt]").val() == '') 
                   {
                       alert('시작일은 필수 입니다.');
                       $("#terminalFormInsert input[name=view_onfftid_cms_start_dt]").focus();
                       return false;
                   }
                   
                  if($("#terminalFormInsert input[name=view_onfftid_cms_end_dt]").val() == '') 
                   {
                       alert('종료일 필수 입니다.');
                       $("#terminalFormInsert input[name=view_onfftid_cms_end_dt]").focus();
                       return false;
                   }                   
                   

                   var chkflg = 0;
                   terminal_mygrid.forEachRow(function(id){
                        // "<center>수수료</center>,<center>적용시작일</center>,<center>적용종료일</center>,<center>MEMO</center>";
                        if(strTpStartdt <= terminal_mygrid.cells(id,2).getValue())
                        {
                            chkflg = 1;
                        }
                    
                   });
        
                   if(chkflg==1) 
                   {
                       alert('등록하시려는 수수료의 시작일이 기존 등록된 수수료의 종료일보다 큽니다.');
                       return false;
                   }
                   else
                   {
                       
                        //"<center>수수료</center>,<center>적용시작일</center>,<center>적용종료일</center>,<center>MEMO</center>";
                        //         0                        1                          2                          3 
                        var strnewRow = 
                                $("#terminalFormInsert input[name=onfftid_cms_commission]").val() 
                                + "," + 
                                strTpStartdt
                                + "," + 
                                strTpEnddt
                                + "," + 
                                $("#terminalFormInsert textarea[name=onfftid_cms_memo]").val()
                        terminal_mygrid.addRow(terminal_mygrid.getUID(), strnewRow, 1);
                   }

                }
                
                function delRow()
                {
                   var selRowid = terminal_mygrid.getSelectedRowId();
                   
                    terminal_mygrid.deleteRow(selRowid);                                
                }

                // 정보 등록
                function doInsertTerminalMaster(){
                    
                    if(!window.confirm("등록하시겠습니까?")){
                        return;
                    }
                    
                    if($("#terminalFormInsert select[name=pay_chn_cate]").val() == "")
                    {
                      alert("결제채널은 필수사항 입니다.");
                       $("#terminalFormInsert select[name=pay_chn_cate]").focus();
                      return;
                    }
                    
                    if($("#terminalFormInsert input[name=onffmerch_no]").val() == "")
                    {
                      alert("지급ID정보는 필수사항 입니다.");                      
                      return;
                    }
                    
                    if($("#terminalFormInsert select[name=svc_stat]").val() == "")
                    {
                      alert("상태는 필수사항 입니다.");
                      $("##terminalFormInsert select[name=svc_stat]").focus();
                      return;
                    }
                    
                    if($("#terminalFormInsert input[name=onfftid_nm]").val() == "")
                    {
                      alert("UID명은 필수사항 입니다.");
                      $("#terminalFormInsert input[name=onfftid_nm]").focus();
                      return;
                    }                    
                    
                    if($("#terminalFormInsert input[name=pay_mtd_nm]").val() == "")
                    {
                      alert("결제상품은 필수사항 입니다.");
                      return;
                    }         
                    
                    var commissioninfo = "";
                    //가맹점정보 상세정보를 만든다.
                    terminal_mygrid.forEachRow(function(id){
                        
                     var strlastCol = terminal_mygrid.cells(id,3).getValue();
                     if(strlastCol =='') strlastCol= " ";
                        
                         //"<center>수수료</center>,<center>적용시작일</center>,<center>적용종료일</center>,<center>MEMO</center>";
                        //            0                           1                          2                      3  
                       var row = terminal_mygrid.cells(id,0).getValue() + '```' + terminal_mygrid.cells(id,1).getValue() + '```' + terminal_mygrid.cells(id,2).getValue() + '```' + strlastCol;
                       
                       commissioninfo = commissioninfo + row + ']]]';
                    });
                   
                    $("#terminalFormInsert input[name=commissioninfo]").attr('value',commissioninfo);  
                   
                    $.ajax({
                        url : $("#terminalFormInsert").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#terminalFormInsert").serialize(),
                        success : function(data) {
                            //alert(data);
                            if(data=='1')
                            {
                                alert("등록 완료");
                                terminalMasterGrid.clearAll();
                                doSearchTerminalMaster();
                                terminalMasterWindow.window("w1").close();
                            }
                            else
                            {
                                alert("등록 실패");
                            }
                        },
                        error : function() { 
                                alert("등록 실패");
                        }
                    });
                }                   

            </script>
        <div style='width:100%;height:100%;overflow:auto;'>            
            <form id="terminalFormInsert" method="POST" action="<c:url value="/terminal/terminalMasterInsert" />" modelAttribute="TerminalFormBean">
                <table class="gridtable" height="100%" width="100%">   
                    <tr>
                        <td align='left' colspan='4'>- 지급ID 정보</td>                        
                    </tr>
                    <tr>                        
                        <td class="headcol">지급ID*</td>
                        <td><input name="onffmerch_no" id="onffmerch_no"  readonly onclick='javascript:OnOffMerchantSelectWin_termMstIns();'/></td>
                        <td class="headcol">지급ID명*</tD>
                        <td><input name="merch_nm" id="merch_nm"  readonly onclick='javascript:OnOffMerchantSelectWin_termMstIns();'/></td>
                    </tr>
                    <tr>
                        <td align='left' colspan='4'>- UID 정보</td>                        
                    </tr>
                    <tr>
                        <td class="headcol">결제채널*</td>
                        <td><select name="pay_chn_cate" >
                                <c:forEach var="code" items="${PayChnCateList.tb_code_details}">
                                    <option value="${code.detail_code}" >${code.code_nm}</option>
                                </c:forEach>
                            </select>                         
                        </td>
                        <td class="headcol">UID상태*</td>
                        <td ><select name="svc_stat" >
                                <c:forEach var="code" items="${UseFlagList.tb_code_details}">
                                    <option value="${code.detail_code}" >${code.code_nm}</option>
                                </c:forEach>
                            </select>                         
                        </td>                        
                    </tr>                                        
                    <tr>                        
                        <td class="headcol" width="20%">UID(자동생성)</td>
                        <td ><input name="onfftid" size="25" readonly /></td>
                        <td class="headcol" width="20%">UID명*</td>
                        <td ><input name="onfftid_nm" size="40" maxlength="50" onblur="javascript:checkLength(this,50);" /></td>                        
                    </tr>
                    <tr>
                        <td class="headcol">결제인증구분*</td>
                        <td ><select name="cert_type" >
                                <c:forEach var="code" items="${CertTypeList.tb_code_details}">
                                    <option value="${code.detail_code}" >${code.code_nm}</option>
                                </c:forEach></select> 
                        </td>                        
                        <td class="headcol">결제취소권한*</td>
                        <td ><select name="cncl_auth" >
                                <c:forEach var="code" items="${Cncl_AuthList.tb_code_details}">
                                    <option value="${code.detail_code}" >${code.code_nm}</option>
                                </c:forEach></select> 
                        </td>                        
                    </tr>                     
                    <tr>
                        <td class="headcol">Memo</td>
                        <td colspan="3"><textarea name="memo" cols="60" rows="3"></textarea></td>                        
                    </tr>                          
                    <tr>
                        <td align='left' colspan='4'>- 결제상품</td>                        
                    </tr>
                    <tr>                        
                        <td class="headcol">결제상품명*</td>
                        <td colspan='3'><input name="pay_mtd_nm" id="pay_mtd_nm" size='30' readonly onclick='javascript:PaymtdSelectWin_termMstIns();'/><input type='hidden' name="pay_mtd_seq" id='pay_mtd_seq'></td>
                    </tr>
                    <tr>
                        <td align='left' colspan='4'>- 수수료 정보</td>                        
                    </tr>
                    <tr>                        
                        <td class="headcol" >수수료율*</tD>
                        <td colspan="3"><input name="onfftid_cms_commission"  onkeyPress="javascript:InpuOnlyNumber(this);"  /></td>    
                    </tr>     
                    <tr>
                        <td class="headcol">시작일*</td>
                        <td><input name="view_onfftid_cms_start_dt" id="view_onfftid_cms_start_dt" maxlength="10" value="<%=strCurDate%>" readonly/><input type="hidden" name="onfftid_cms_start_dt"/></td>
                        <td class="headcol">종료일*</tD>
                        <td><input name="view_onfftid_cms_end_dt" id="view_onfftid_cms_end_dt" maxlength="10" value="<%=strTpDefaultEndDt%>" readonly/><input type="hidden" name="onfftid_cms_end_dt"/></td>
                    </tr> 
                    <tr>
                        <td class="headcol">Memo</td>
                        <td colspan="3"><textarea name="onfftid_cms_memo" cols="60" rows="2"></textarea></td>                        
                    </tr>                     
                    <tr>
                        <td align='left' colspan='4' valign="top">
                            <div id = "common_btn" align = "right" style="height:30px;" >
                            <input type="button" name="onfftid_cms_addbt" value="수수료 추가" onclick="javascript:addRow();"/>
                            <input type="button" name="onfftid_cms_delbt" value="수수료 삭제" onclick="javascript:delRow();"/>
                            </div>
                            <div id="onfftid_cmsVP2" style="position: relative; width:100%;height:100px;background-color:white;"></div>
                        </td>                        
                    </tr>   
                    <tr>
                        <td colspan="4" align="center"><input type="button" name="insert" value="등록"/>&nbsp;<input type="button" name="close" value="닫기"/></td>
                    </tr>       
                </table>
                 <input type="hidden" name="commissioninfo" />
            </form>
        </div>
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

