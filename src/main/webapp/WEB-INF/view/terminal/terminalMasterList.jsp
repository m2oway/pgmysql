<%-- 
    Document   : terminalMasterList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>UID 정보 관리</title>
        </head>
        <body>
</c:if>
        <script type="text/javascript">
                var terminalMasterGrid={};
                var terminalMasterWindow;
                var terminalMasterLayout;
                
                $(document).ready(function () {
                    var terminalSearch = document.getElementById("terminalSearch");            
                    terminalMasterLayout = tabbar.cells("3-3").attachLayout("2E");
                    
                    terminalMasterLayout.cells('a').hideHeader();
                    terminalMasterLayout.cells('b').hideHeader();
                    terminalMasterLayout.cells('a').attachObject(terminalSearch);
                    terminalMasterLayout.cells('a').setHeight(75);
                    terminalMasterGrid = terminalMasterLayout.cells('b').attachGrid();
                    terminalMasterGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var terminalMasterGridheaders = "";
                    //no, 결제채널, 결제TID명,결제TID,가맹점명, 가맹점번호, 결제상품명 ,상태
                    terminalMasterGridheaders += "<center>NO</center>,<center>결제채널</center>,<center>UID명</center>,<center>UID</center>,<center>지급ID명</center>,<center>지급ID</center>,<center>결제상품명</center>,<center>상태</center>,<center>결제인증구분</center>";
                    terminalMasterGrid.setHeader(terminalMasterGridheaders);
                    terminalMasterGrid.setColAlign("center,center,center,center,center,center,center,center,center");
                    terminalMasterGrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,txt,txt");
                    terminalMasterGrid.setInitWidths("50,200,150,150,200,120,200,100,100");
                    terminalMasterGrid.setColSorting("str,str,str,str,str,str,str,str,str");
                    terminalMasterGrid.enableColumnMove(true);
                    terminalMasterGrid.setSkin("dhx_skyblue");
    
                    //terminalMasterGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxToolbar/" />");
                    terminalMasterGrid.enablePaging(true,Number($("#terminalForm input[name=page_size]").val()),10,"terminalMasterListPaging",true,"terminalMasterListrecinfoArea");
                    terminalMasterGrid.setPagingSkin("bricks");

                    //페이징 처리
                    terminalMasterGrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        if(lInd !==0){
                          $("#terminalForm input[name=page_no]").val(ind);             
                           terminalMasterGrid.clearAll();
                           doSearchTerminalMaster();
                        }else{
                           $("#terminalForm input[name=page_no]").val(1);
                            terminalMasterGrid.clearAll();
                            doSearchTerminalMaster();
                        }                        
                        
                    });
                    
                    terminalMasterGrid.init();
                    terminalMasterGrid.parse(${ResultSet}, "json");
                    terminalMasterGrid.attachEvent("onRowDblClicked", doUpdateFormTerminalMaster);
                    
                    terminalMasterWindow = new dhtmlXWindows();
                    terminalMasterWindow.attachViewportTo("terminalList");

                    //버튼 추가
                    $("#terminalForm input[name=terminal_searchButton]").bind("click",function(e){
                        //가맹점정보 정보 조회 이벤트
                        /*
                        $("#terminalForm input[name=page_no]").val("1");
                        terminalMasterGrid.clearAll();
                        doSearchTerminalMaster();
                        */
                        terminalMasterGrid.changePage(1);
                        return false;
                    });


                    $("#terminalForm .addButton").button({
                        icons: {
                            primary: "ui-icon-plusthick"
                        },
                        text: false
                    })
                    .unbind("click")
                    .bind("click",function(e){
                        //정보 등록 이벤트
                        doInsertFormTerminalMaster();
                        return false;
                    });

                    $("#terminalForm .deleteButton").button({
                        icons: {
                            primary: "ui-icon-minusthick"
                        },
                        text: false
                    })
                    .unbind("click")
                    .bind("click",function(e){
                        //가맹점정보 정보 삭제 이벤트
                        doDeleteTerminalMaster();
                        return false;
                    });
                    //검색조건 초기화
                    $("#terminalForm input[name=init]").click(function () {
                    
                        terminalMasterInit($("#terminalForm"));

                    });  

                });

                //가맹점정보 정보 수정
                function doUpdateFormTerminalMaster(rowid, col){
                    var onfftid = terminalMasterGrid.getUserData(rowid, 'onfftid');
                    w1 = terminalMasterWindow.createWindow("w1", 150, 150,  800, 700);
                    w1.setText("UID정보 수정");
                    terminalMasterWindow.window('w1').setModal(true);
                    w1.attachURL("<c:url value="/terminal/terminalMasterUpdate" />" + "?onfftid=" + onfftid,true);
                }

                function resizeOption($id){
                    //css 변경 : resize시 자동 변경됨.
                    $id.css("width","100%");
                    $id.css("height","100%");
                }


                //가맹점정보 정보 조회
                function doSearchTerminalMaster(){
                    $.ajax({
                        url : $("#terminalForm").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#terminalForm").serialize(),
                        success : function(data) {
                            //terminalMasterGrid.clearAll();
                            //terminalMasterGrid.parse(jQuery.parseJSON(data), "json");
                            var jsonData = $.parseJSON(data);     
                            terminalMasterGrid.parse(jsonData,"json");
                            //resizeOption($("#terminalList.gridbox"));
                        },
                        error : function() {
                            alert("조회 실패");
                        }
                    });
                }


                //가맹점정보 정보 등록
                function doInsertFormTerminalMaster(){

                    w1 = terminalMasterWindow.createWindow("w1", 150, 150, 800, 700);
                    //w1 = terminalMasterWindow.createWindow("editwin", 150, 150, 700, 400);
                    w1.setText("UID 정보 등록");
                    terminalMasterWindow.window('w1').setModal(true);
                    w1.attachURL("<c:url value="/terminal/terminalMasterInsert" />",true);   

                }
                var dhxSelPopupWins=new dhtmlXWindows();
                
/*
                //ONOFF가맹점번호 팝업 클릭 이벤트
                $("#terminalForm input[name=onffmerch_no]").unbind("click").bind("click", function (){

                    onofftermMerchSelectPopup();

                }); 
 */               
                /*
                function onofftermMerchSelectPopup(){
                    onoffObject.onffmerch_no = $("#terminalForm input[name=onffmerch_no]");
                    onoffObject.merch_nm = $("#terminalForm input[name=merch_nm]");
                    //ONOFF가맹점정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("가맹점 선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                }
                */

//                function SelOnoffmerchInfoInput(p_onffmerch_no, p_merch_nm){
//                    $("#terminalForm input[name=onffmerch_no]").val(p_onffmerch_no);
//                    $("#terminalForm input[name=merch_nm]").val(p_merch_nm);                    
//                }
                /*
                 $("#terminalForm input[name=onfftid]").unbind("click").bind("click", function (){

                    onofftermTidSelectPopup();

                }); 
                */
                
                /*
                function onofftermTidSelectPopup(){
                    onoffObject.onfftid_nm = $("#terminalForm input[name=onfftid_nm]");
                    onoffObject.onfftid = $("#terminalForm input[name=onfftid]");
                    //ONOFF결제ID 팝업
                    w2 = dhxSelPopupWins.createWindow("OnofftidSelectPopUp", 20, 30, 640, 480);
                    w2.setText("ONOFF결제ID 선택페이지");
                    dhxSelPopupWins.window('OnofftidSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnofftidSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnofftidSelectPopUp"/>', true);   
                } 
                */
                function terminalMasterInit($form) {

                    searchFormInit($form);
                } 
            </script>            
            <div class="searchForm1row" id="terminalSearch" style="display:none;width:100%;height:100%;background-color:white;">
                <form id="terminalForm" method="POST" action="<c:url value="/terminal/terminalMasterList" />" modelAttribute="TerminalFormBean">
                       <input type="hidden" name="page_size" value="100" />
                       <input type="hidden" name="page_no" value="1" />
                       
                        <div class="label">결제채널</div>
                        <div class="input">
                            <select name="pay_chn_cate">
                                 <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${PayChnCateList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>                       
                       
                        <div class="label">UID</div>
                        <div class="input"><input type="text" name="onfftid" maxlength="30"/></div>
                        
                        <div class="label">UID명</div>
                        <div class="input"><input type="text" name="onfftid_nm" maxlength="30"/></div>    
                       
                       <div class="label">지급ID</div>
                        <div class="input"><input type="text" name="onffmerch_no" maxlength="30"/></div>
                        
                        <div class="label">지급ID명</div>
                        <div class="input"><input type="text" name="merch_nm" maxlength="30"/></div>                        

                        
                        <div class="label">상태</div>
                        <div class="input">
                            <select name="svc_stat">
                                 <option value="">--선택하세요.--</option>
                                <c:forEach var="code" items="${UseFlagList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>                        
                        <input type="button" name="terminal_searchButton" value="조회"/>
                        <input type="button" name="init" value="검색조건지우기"/>
                        <div class="action">
                            <button class="addButton">추가</button>
                        </div>
                </form>
                <div id="terminalMasterListPaging" style="width: 100%;"></div>
                <div id="terminalMasterListrecinfoArea" style="width: 100%;"></div>                        
            </div>
            <div id="terminalList" style="width:100%;height:100%;background-color:white;"></div>
 <c:if test="${!ajaxRequest}">    
        </body>
</html>
</c:if>

