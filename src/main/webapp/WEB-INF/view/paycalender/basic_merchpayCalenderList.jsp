<%-- 
    Document   : basic_merchpayCalenderList.jsp
    Created on : 2015. 1. 7, 오후 4:53:48
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="/WEB-INF/view/common/basic_common.jsp"/>
<%@include file="/WEB-INF/view/common/basic_comm_header.jsp"%>
<form name="paycalcurform" id="paycalcurform" action="<c:url value="/paycalender/basic_merchpayCalenderList" />">
    <input type="hidden" name="paycalyyyymm" id="paycalyyyymm">
</form>
<table border="0" bordercolor='red' cellpadding="0" cellspacing="0" width="100%" height="100%">
<tr>
    <td align="center" height="10%" >
        <table border="0" bodercolor='blue' cellpadding="0" cellspacing="0" width="100%" height="100%">
            <tr>
                <td align="right"><font size='5'><a href="#" Onclick="javascirpt:moveMonth('-');">◁</a></font></td>
                <td id='paycalyyyymmtitle' align="center">년월</td>
                <td align="left"><font size='5'><a href="#" Onclick="javascirpt:moveMonth('+');">▷</a></font></td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td height="80%">
        <table border="1" width="100%" height="100%">
            <thead> 
                <tr> 
                    <th height="7%" align="center"><font size='5'>일</font></td>
                    <th align="center"><font size='5'>월</font></td>
                    <th align="center"><font size='5'>화</font></td>
                    <th align="center"><font size='5'>수</font></td>
                    <th align="center"><font size='5'>목</font></td>
                    <th align="center"><font size='5'>금</font></td>
                    <th align="center"><font size='5'>토</font></td>
                </tr>
            </thead>
            <tbody id="paycaltbody" name="paycaltbody">
                
            </tbody>
        </table>

    </td>
</tr>
</table>
<%@include file="/WEB-INF/view/common/basic_comm_tail.jsp"%>    
<script type="text/javascript">
            function numberWithCommas(x) {
                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }
            
            
            function moveMerchPay(mvtargetdt)
            {
                basicmenu_move('/merchpay/basic_merchMerchpayMasterList?nopageflag=Y&exp_pay_start_dt='+mvtargetdt+'&exp_pay_end_dt='+mvtargetdt);
            }
            
            function moveMonth(movemonth)
            {   
                var strMovTargetMonth = $("#paycalyyyymm").val();
                
                var tpx_yyyy = strMovTargetMonth.slice(0,4);
                var tpx_mm = strMovTargetMonth.slice(4,6);
                
                var TpTargetday_cur = new Date(tpx_yyyy+"-"+tpx_mm+"-"+"01");
                var TpTargetday_first = new Date(tpx_yyyy+"-"+tpx_mm+"-"+"01");
                var TpTargetday_last = new Date(TpTargetday_cur.getFullYear(),TpTargetday_cur.getMonth()+1,""); 
                
                //익월
                if(movemonth=="+")
                {
                    //날자계산
                    var tplastDay = new Date ( TpTargetday_last.setDate( TpTargetday_last.getDate() + 1 ) );       
                    var tplastDayyyy = tplastDay.getFullYear();
                    var tplastDamonth = tplastDay.getMonth()+1;
                    if(tplastDamonth < 10)
                    {
                        tplastDamonth = "0"+tplastDamonth; 
                    }
                    $("#paycalyyyymm").val(tplastDayyyy+""+tplastDamonth);
                }
                //전월
                else
                {
                    var tppreMonth = new Date ( TpTargetday_first.setDate( TpTargetday_first.getDate() - 1 ) );
                    var preMonthyyyy = tppreMonth.getFullYear();
                    var preMonthmonth = tppreMonth.getMonth()+1;
                    if(preMonthmonth < 10)
                    {
                        preMonthmonth = "0"+preMonthmonth; 
                    }
                    $("#paycalyyyymm").val(preMonthyyyy+""+preMonthmonth);                    
                }  
                
                $.ajax({
                    url: $("#paycalcurform").attr("action"),
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#paycalcurform").serialize(),
                    success: function (data) {
                        createCal(data);
                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }
            
            
            function createCal(jasondata)
            {
               var tp_jsonobj = JSON.parse(jasondata);
               var tprowslist = tp_jsonobj.rows;
               var tprowslistcnt = tprowslist.length
               var tbody = document.getElementById("paycaltbody");
               while(tbody.rows.length > 0){
                      tbody.deleteRow(0);
               }
               
               var week = new Array('일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일');
               var calroopcnt = 0;
               var tpCaltablehtml = "";
               
               $.each(tprowslist, function(key)
               { 
                    var tpx_yyyymmdd="";
                    tpx_yyyymmdd = tprowslist[key].data[0]; 
                    var tpx_yyyy = tpx_yyyymmdd.slice(0,4);
                    var tpx_mm = tpx_yyyymmdd.slice(4,6);
                    var tpx_dd = tpx_yyyymmdd.slice(6,8);
       
                    var TpTargetday = new Date(tpx_yyyy+"-"+tpx_mm+"-"+tpx_dd).getDay();
                    var TpWeekDayCnt =  calroopcnt%7;
                    
                    if(calroopcnt==0 && TpTargetday!=0)
                    {
                         $("#paycalyyyymmtitle").text(tpx_yyyy+"년 " + tpx_mm + "월");
                         $("#paycalyyyymm").val(tpx_yyyy+""+tpx_mm);
                         
                        tpCaltablehtml+= "<tr>";
                        
                        for(var tpblkloopcnt=0 ; tpblkloopcnt < TpTargetday; tpblkloopcnt++)
                        {
                            tpCaltablehtml += "<td>&nbsp;</td>";
                            TpWeekDayCnt++;
                        }                        
                    }
                    else if(calroopcnt==0 && TpTargetday ==0)
                    {
                         $("#paycalyyyymmtitle").text(tpx_yyyy+"년 " + tpx_mm + "월");
                         $("#paycalyyyymm").val(tpx_yyyy+""+tpx_mm);
                         
                        tpCaltablehtml+= "<tr>";
                    }
                    
                    tpCaltablehtml += "<td valign='top' align='left' width='14%'>";
                    tpCaltablehtml += "<font size='5'>" + tpx_dd + "</font><br>";
                    tpCaltablehtml += "<font size='3'><a href='#' Onclick=\"javascirpt:moveMerchPay('"+tpx_yyyymmdd+"');\">입금예정:"+numberWithCommas(tprowslist[key].data[1])+"</a></font><br>";
                    tpCaltablehtml += "<font size='3'><a href='#' Onclick=\"javascirpt:moveMerchPay('"+tpx_yyyymmdd+"');\">입금액:"+numberWithCommas(tprowslist[key].data[2])+"</a></font>";
                    tpCaltablehtml += "</td>";
                    
                    TpWeekDayCnt++;
                    
                    if( calroopcnt==(tprowslistcnt-1))
                    {
                        for(var tpblkloopcnt2=TpTargetday ; tpblkloopcnt2 < 6; tpblkloopcnt2++)
                        {
                            tpCaltablehtml += "<td>&nbsp;</td>";
                        }
                    }
                    
                    //alert(TpWeekDayCnt);
                    ///if( (TpWeekDayCnt==0) ||   (TpWeekDayCnt==7))
                    if(TpTargetday==6)
                    {
                        tpCaltablehtml += "</tr>";
                    }
                    
                    
                    //alert(tpx_yyyymmdd);
                    
                    calroopcnt++;
               });
              
               $("#paycaltbody").append(tpCaltablehtml);
            }
            
            createCal('${payCalenderList}');
        </script>