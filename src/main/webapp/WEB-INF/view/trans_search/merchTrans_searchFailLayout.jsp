<%-- 
    Document   : trans_searchFailLayout
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>신용카드 실패내역</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">
        
                var main_trans_searchFail_layout={};
                $(document).ready(function () {

                    var tabBarId = tabbar.getActiveTab();
                    
                    main_trans_searchFail_layout = tabbar.cells(tabBarId).attachLayout("1C");
                    main_trans_searchFail_layout.cells('a').hideHeader();
                    main_trans_searchFail_layout.cells('a').attachURL("<c:url value="/trans_search/merchTrans_searchFailDetailList" />",true); 

                });
             
            
    </script>
    
    
     <!--<h1>코드관리</h1>-->
     
         <div id="Trans_searchLayout" style="width:100%;height:100%;background-color:white;"></div>
    </body>
</html>
