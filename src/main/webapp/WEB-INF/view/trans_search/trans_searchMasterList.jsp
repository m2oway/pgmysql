<%-- 
    Document   : codeMaster
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
<jsp:include page="/WEB-INF/view/common/common.jsp"/>
<html>
    <head>
        <title>신용거래조회</title>             
    </head>
    <body>
</c:if>
            
        <div class="right" id="trans_search_search" style="width:100%;">       
            <form id="trans_searchForm" method="POST" onsubmit="return false" action="<c:url value="/trans_search/trans_searchMasterList" />" modelAttribute="trans_searchFormBean"> 
                <!--
                <input type="hidden" name="page_size" value="100" >
                <input type="hidden" name="page_no" value="1" >
                -->
                <table>
                    <tr>
                        <div class="label">검색일자</div>
                        <div class="input">
                            <input type="text" name="app_start_dt" id="trans_searchForm_from_date" value ="${trans_searchFormBean.app_start_dt}" onclick="trans_search_setSens('trans_searchForm_to_date', 'max');" /> ~
                            <input type="text" name="app_end_dt" id="trans_searchForm_to_date" value="${trans_searchFormBean.app_end_dt}" onclick="trans_search_setSens('trans_searchForm_from_date', 'min');" /> </div>
                            <div class ="input"><input type="button" name="week" value="1주"/></div>
                            <div class ="input"><input type="button" name="month" value="1달"/></div>                       
                        <div class="label">결제채널</div>
                        <div class="input">
                            <select name="pay_chn_cate">
                                <option value="">전체</option>
                                <c:forEach var="code" items="${payChnCateList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <td><input type="button" name="trans_search_search" value="조회"/></td>                        
                        <td><input type="button" name="trans_search_excel" value="엑셀"/></td>
                        <td><input type="button" name="trans_search_init" value="검색조건지우기"/></td>
                    </tr>
               </table>
            </form>
            <div class="paging">
                <div id="trans_searchPaging" style="width: 50%;"></div>
                <div id="trans_searchrecinfoArea" style="width: 50%;"></div>
            </div>        
        </div>
        
        <script type="text/javascript">
            
            var trans_searchgrid={};
            var trans_searchCalendar;
            
            $(document).ready(function () {
                
                trans_searchCalendar = new dhtmlXCalendarObject(["trans_searchForm_from_date","trans_searchForm_to_date"]);    
                
                var trans_search_searchForm = document.getElementById("trans_search_search");
    
                trans_search_layout = main_trans_search_layout.cells('a').attachLayout("2E");
                     
                trans_search_layout.cells('a').hideHeader();
                trans_search_layout.cells('b').hideHeader();
                 
                trans_search_layout.cells('a').attachObject(trans_search_searchForm);
                trans_search_layout.cells('a').setHeight(40);
                
                trans_searchgrid = trans_search_layout.cells('b').attachGrid();
                
                trans_searchgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");

                var trans_searchheaders = "";
                trans_searchheaders += "<center>일자</center>,<center>결제채널</center>,<center>가맹점명</center>,<center>승인금액</center>,<center>취소금액</center>,<center>승인VAT</center>,<center>취소VAT</center>,<center>승인건</center>,<center>취소건</center>";
                trans_searchgrid.setHeader(trans_searchheaders);
                trans_searchgrid.setColAlign("center,center,center,right,right,right,right,right,right");
                trans_searchgrid.setColTypes("txt,txt,txt,edn,edn,edn,edn,edn,edn");
                trans_searchgrid.setInitWidths("100,150,150,100,100,100,100,100,100");
                trans_searchgrid.setColSorting("str,str,str,int,int,int,int,int,int");
                trans_searchgrid.setNumberFormat("0,000",3);
                trans_searchgrid.setNumberFormat("0,000",4);
                trans_searchgrid.setNumberFormat("0,000",5);
                trans_searchgrid.setNumberFormat("0,000",6);
                trans_searchgrid.setNumberFormat("0,000",7);
                trans_searchgrid.setNumberFormat("0,000",8);
                trans_searchgrid.enableColumnMove(true);
                trans_searchgrid.setSkin("dhx_skyblue");

                var trans_searchfooters = "총계,#cspan,#cspan";
                trans_searchfooters += ",<div id='trans_search_master3'>0</div>,<div id='trans_search_master4'>0</div>,<div id='trans_search_master5'>0</div>,<div id='trans_search_master6'>0</div>,<div id='trans_search_master7'>0</div>,<div id='trans_search_master8'>0</div>";
                trans_searchgrid.attachFooter(trans_searchfooters);                    

/*
                trans_searchgrid.enablePaging(true,Number($("#trans_searchForm input[name=page_size]").val()),10,"trans_searchPaging",true,"trans_searchrecinfoArea");
                trans_searchgrid.setPagingSkin("bricks");    
                
                trans_searchgrid.attachEvent("onPageChanged", function(ind,fInd,lInd){

 */ 
 /*
                    if(lInd !==0){
                        $("#trans_searchForm input[name=page_no]").val(ind);
                        trans_searchSearch();
                    }else{
                        return false;
                    }
*/                    
/*
                    if(lInd !==0){
                      $("#trans_searchForm input[name=page_no]").val(ind);      
                       trans_searchgrid.clearAll();
                        trans_searchSearch();
                    }else{
                       $("#trans_searchForm input[name=page_no]").val(1);
                        trans_searchgrid.clearAll();
                         trans_searchSearch();
                    }                    
                    
                });                    
*/
                trans_searchgrid.init();
                trans_searchgrid.parse(${ls_trans_searchMasterList}, trans_searchFooterValues,"json");

                trans_searchgrid.attachEvent("onRowDblClicked", trans_search_attach);

                //신용거래 정보 검색 이벤트
                $("#trans_searchForm input[name=trans_search_search]").click(function () {
                    
                    //$("input[name=page_no]").val("1");
                    trans_searchSearch();
                    
                   /*
                    trans_searchgrid.changePage(1);
                    return false;
                    */
                });
                
                //신용거래 정보 엑셀다운로드 이벤트
                $("#trans_searchForm input[name=trans_search_excel]").click(function () {
                    
                    trans_searchExcel();
                    
                })
                
                $("#trans_searchForm input[name=week]").click(function(){
                    trans_search_date_search("week");
                });
                    
                $("#trans_searchForm input[name=month]").click(function(){
                    trans_search_date_search("month");
                });     
                
                //검색조건 초기화
                $("#trans_searchForm input[name=trans_search_init]").click(function () {
                    
                    trans_searchMasterInit($("#trans_searchForm"));
                    
                });                
                  
            });
            
            function trans_searchFooterValues() {
                
                var trans_search_master3 = document.getElementById("trans_search_master3");
                var trans_search_master4 = document.getElementById("trans_search_master4");
                var trans_search_master5 = document.getElementById("trans_search_master5");
                var trans_search_master6 = document.getElementById("trans_search_master6");
                var trans_search_master7 = document.getElementById("trans_search_master7");
                var trans_search_master8 = document.getElementById("trans_search_master8");

                trans_search_master3.innerHTML =  putComma(sumColumn(3)) ;    
                trans_search_master4.innerHTML =  putComma(sumColumn(4)) ;
                trans_search_master5.innerHTML =  putComma(sumColumn(5)) ;
                trans_search_master6.innerHTML =  putComma(sumColumn(6)) ;
                trans_search_master7.innerHTML =  putComma(sumColumn(7)) ;
                trans_search_master8.innerHTML =  putComma(sumColumn(8)) ;

                return true;

            }
		
            function sumColumn(ind) {
                var out = 0;
                for (var i = 0; i < trans_searchgrid.getRowsNum(); i++) {
                     out += parseFloat(trans_searchgrid.cells2(i, ind).getValue());
                }

                return out;
            }

            //신용거래 상세조회 이벤트
            function trans_search_attach(rowid, col) {

                var app_dt = trans_searchgrid.getUserData(rowid, 'app_dt');
                var pay_chn_cate = trans_searchgrid.getUserData(rowid, 'pay_chn_cate');
                var onffmerch_no = trans_searchgrid.getUserData(rowid, 'onffmerch_no');
                
                main_trans_search_layout.cells('b').attachURL("<c:url value="/trans_search/trans_searchDetailList" />"+"?app_start_dt=" + app_dt + "&app_end_dt=" + app_dt + "&pay_chn_cate=" + pay_chn_cate + "&onffmerch_no=" + onffmerch_no, true);
                
            }
            
            //신용거래 정보 조회
            function trans_searchSearch() {
                
                $.ajax({
                    url: "<c:url value="/trans_search/trans_searchMasterList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#trans_searchForm").serialize(),
                    success: function (data) {

                        trans_searchgrid.clearAll();
                        trans_searchgrid.parse($.parseJSON(data),trans_searchFooterValues, "json");

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }
            
            function trans_search_setSens(id, k) {
                // update range
                if (k == "min") {
                    trans_searchCalendar.setSensitiveRange(trans_search_byId(id).value, null);
                } else {
                    trans_searchCalendar.setSensitiveRange(null, trans_search_byId(id).value);
                }
            }

            function trans_search_byId(id) {
                return document.getElementById(id);
            }          
            
            //신용거래 엑셀다운로드
            function trans_searchExcel() {

                $("#trans_searchForm").attr("action","<c:url value="/trans_search/trans_searchMasterListExcel" />");
                document.getElementById("trans_searchForm").submit();

            }       
            
            function trans_search_date_search(day){
            
                var oneDate = 1000 * 3600 * 24; 
                var now = new Date();
                var week = new Date(now.getTime() + (oneDate*-6));
                var month = new Date(now.getTime() + (oneDate*-31));
                var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                var date=new Date();

                if(day=="today"){
                    $("#trans_searchForm input[id=trans_searchForm_from_date]").val(nowTime);
                    $("#trans_searchForm input[id=trans_searchForm_to_date]").val(nowTime);
                }else if(day=="week"){
                    $("#trans_searchForm input[id=trans_searchForm_from_date]").val(weekTime);
                    $("#trans_searchForm input[id=trans_searchForm_to_date]").val(nowTime);
                }else{
                    $("#trans_searchForm input[id=trans_searchForm_from_date]").val(monthTime);
                    $("#trans_searchForm input[id=trans_searchForm_to_date]").val(nowTime);
                }
            
            }
                
            //검색조건 초기화
            function trans_searchMasterInit($form) {

                searchFormInit($form);

                trans_search_date_search("week");                    
            } 
            
            var dhxSelPopupWins=new dhtmlXWindows();
            
            //ONOFF결제ID 팝업 클릭 이벤트
            $("#trans_searchForm input[name=onfftid]").unbind("click").bind("click", function (){

                onoffTrans_searchDetailTidSelectPopup();

            });                                

            function onoffTrans_searchDetailTidSelectPopup(){
                onoffObject.onfftid_nm = $("#trans_searchForm input[name=onfftid_nm]");
                onoffObject.onfftid = $("#trans_searchForm input[name=onfftid]");
                //ONOFF결제ID 팝업
                w2 = dhxSelPopupWins.createWindow("OnofftidSelectPopUp", 20, 30, 640, 480);
                w2.setText("ONOFF결제ID 선택페이지");
                dhxSelPopupWins.window('OnofftidSelectPopUp').setModal(true);
                dhxSelPopupWins.window('OnofftidSelectPopUp').denyResize();
                w2.attachURL('<c:url value = "/popup/OnofftidSelectPopUp"/>', true);   
            }                
                
        </script>

<c:if test="${!ajaxRequest}">
    </body>
</html>
</c:if>        
