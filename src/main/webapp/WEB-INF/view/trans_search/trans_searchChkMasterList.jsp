<%-- 
    Document   : codeMaster
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
<jsp:include page="/WEB-INF/view/common/common.jsp"/>
<html>
    <head>
        <title>신용거래조회</title>             
    </head>
    <body>
</c:if>
            
        <div class="right" id="trans_searchChkMaster_search" style="width:100%;">       
            <form id="trans_searchChkMasterForm" method="POST" onsubmit="return false" action="<c:url value="/trans_search/trans_searchChkMasterList" />" modelAttribute="trans_searchChkMasterFormBean"> 
                <input type="hidden" name="page_size" value="100" >
                <input type="hidden" name="page_no" value="1" >
                <table>
                    <tr>
                        <div class="label">검색일자</div>
                        <div class="input">
                            <input type="text" name="app_start_dt" size="10" id="trans_searchChkMasterForm_from_date" value ="${trans_searchChkMasterFormBean.app_start_dt}" onclick="trans_searchChkMaster_setSens('trans_searchChkMasterForm_to_date', 'max');" /> ~
                            <input type="text" name="app_end_dt" size="10" id="trans_searchChkMasterForm_to_date" value="${trans_searchChkMasterFormBean.app_end_dt}" onclick="trans_searchChkMaster_setSens('trans_searchChkMasterForm_from_date', 'min');" /> </div>
                            <div class ="input"><input type="button" name="week" value="1주"/></div>
                            <div class ="input"><input type="button" name="month" value="1달"/></div>                       
                            <div class="label">가맹점명</div>
                            <div class="input">
                                <input type="text" name="merch_nm" size="10" value ="${trans_searchFormBean.merch_nm}" />
                            </div>        
                            <div class="label">가맹점번호</div>
                            <div class="input">
                                <input type="text" name="onffmerch_no" size="10" value ="${trans_searchFormBean.onffmerch_no}" />
                            </div>                              
                            <div class="label">결제ID명</div>
                            <div class="input">
                                <input type="text" name="onfftid_nm" size="10" value ="${trans_searchFormBean.onfftid_nm}" />
                            </div>    
                            <div class="label">결제ID</div>
                            <div class="input">
                                <input type="text" name="onfftid"  size="10" value ="${trans_searchFormBean.onfftid}" />
                            </div>  
                            <div class="label">카드번호</div>
                            <div class="input">
                                <input type="text" size="20" name="card_num" value ="${trans_searchFormBean.card_num}" />
                            </div>        
                            <div class="label">중복건수</div>
                            <div class="input">
                                <input type="text"size="5" name="chktran_cnt" value ="${trans_searchFormBean.chktran_cnt}" />
                            </div>                              

                        <td><input type="button" name="trans_searchChkMaster_search" value="조회"/></td>                        
                        <td><input type="button" name="trans_searchChkMaster_init" value="검색조건지우기"/></td>
                    </tr>
               </table>
            </form>
            <div class="paging">
                <div id="trans_searchChkMasterPaging" style="width: 50%;"></div>
                <div id="trans_searchChkMasterrecinfoArea" style="width: 50%;"></div>
            </div>                        
        </div>
        
        <script type="text/javascript">
            
            var trans_searchChkMastergrid={};
            var trans_searchChkMasterCalendar;
            
            $(document).ready(function () {
                
                trans_searchChkMasterCalendar = new dhtmlXCalendarObject(["trans_searchChkMasterForm_from_date","trans_searchChkMasterForm_to_date"]);    
                
                var trans_searchChkMaster_searchForm = document.getElementById("trans_searchChkMaster_search");
    
                trans_searchChkMaster_layout = main_trans_searchchk_layout.cells('a').attachLayout("2E");
                     
                trans_searchChkMaster_layout.cells('a').hideHeader();
                trans_searchChkMaster_layout.cells('b').hideHeader();
                 
                trans_searchChkMaster_layout.cells('a').attachObject(trans_searchChkMaster_searchForm);
                trans_searchChkMaster_layout.cells('a').setHeight(55);
                
                trans_searchChkMastergrid = trans_searchChkMaster_layout.cells('b').attachGrid();
                
                trans_searchChkMastergrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");

                var trans_searchChkMasterheaders = "";
                trans_searchChkMasterheaders += "<center>승인일</center>,<center>가맹점명</center>,<center>결제TID명</center>,<center>거래종류</center>,<center>카드번호</center>,<center>건수</center>";
                trans_searchChkMastergrid.setHeader(trans_searchChkMasterheaders);
                trans_searchChkMastergrid.setColAlign("center,center,center,center,center,right");
                trans_searchChkMastergrid.setColTypes("txt,txt,txt,txt,txt,edn");
                trans_searchChkMastergrid.setInitWidths("80,150,150,80,100,80");
                trans_searchChkMastergrid.setColSorting("str,str,str,str,str,int");
                trans_searchChkMastergrid.setNumberFormat("0,000",5);
                trans_searchChkMastergrid.enableColumnMove(true);
                trans_searchChkMastergrid.setSkin("dhx_skyblue");
        

                trans_searchChkMastergrid.enablePaging(true,Number($("#trans_searchChkMasterForm input[name=page_size]").val()),10,"trans_searchChkMasterPaging",true,"trans_searchChkMasterrecinfoArea");
                trans_searchChkMastergrid.setPagingSkin("bricks");    
                
                trans_searchChkMastergrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                
                    if(lInd !==0){
                      $("#trans_searchChkMasterForm input[name=page_no]").val(ind);      
                       trans_searchChkMastergrid.clearAll();
                        trans_searchChkMasterSearch();
                    }else{
                       $("#trans_searchChkMasterForm input[name=page_no]").val(1);
                        trans_searchChkMastergrid.clearAll();
                         trans_searchChkMasterSearch();
                    }                    
                    
                });                    

                trans_searchChkMastergrid.init();
                trans_searchChkMastergrid.parse(${ls_trans_searchMasterList}, "json");

                trans_searchChkMastergrid.attachEvent("onRowDblClicked", trans_searchChkMaster_attach);

                //신용거래 정보 검색 이벤트
                $("#trans_searchChkMasterForm input[name=trans_searchChkMaster_search]").click(function () {
                    /*
                    $("input[name=page_no]").val("1");
                    trans_searchChkMasterSearch();
                    */
                    trans_searchChkMastergrid.changePage(1);
                    return false;
                });
                
                
                $("#trans_searchChkMasterForm input[name=week]").click(function(){
                    trans_searchChkMaster_date_search("week");
                });
                    
                $("#trans_searchChkMasterForm input[name=month]").click(function(){
                    trans_searchChkMaster_date_search("month");
                });     
                
                //검색조건 초기화
                $("#trans_searchChkMasterForm input[name=trans_searchChkMaster_init]").click(function () {
                    
                    trans_searchChkMasterMasterInit($("#trans_searchChkMasterForm"));
                    
                });                
                  
            });
            
        
		

            //신용거래 상세조회 이벤트
            function trans_searchChkMaster_attach(rowid, col) {

                var app_dt = trans_searchChkMastergrid.getUserData(rowid, 'app_dt');
                var onffmerch_no = trans_searchChkMastergrid.getUserData(rowid, 'onffmerch_no');
                var onfftid = trans_searchChkMastergrid.getUserData(rowid, 'onfftid');
                var massagetype = trans_searchChkMastergrid.getUserData(rowid, 'massagetype');
                var card_num = trans_searchChkMastergrid.getUserData(rowid, 'card_num');
                
                main_trans_searchchk_layout.cells('b').attachURL("<c:url value="/trans_search/trans_searchChkDetailList" />"+"?app_dt=" + app_dt + "&onffmerch_no=" + onffmerch_no + "&onfftid=" + onfftid+ "&massagetype=" + massagetype+ "&card_num=" + card_num, true);
                
            }
            
            //신용거래 정보 조회
            function trans_searchChkMasterSearch() {
                
                $.ajax({
                    url: "<c:url value="/trans_search/trans_searchChkMasterList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#trans_searchChkMasterForm").serialize(),
                    success: function (data) {

                        //trans_searchChkMastergrid.clearAll(1);
                        trans_searchChkMastergrid.parse($.parseJSON(data), "json");

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }
            
            function trans_searchChkMaster_setSens(id, k) {
                // update range
                if (k == "min") {
                    trans_searchChkMasterCalendar.setSensitiveRange(trans_searchChkMaster_byId(id).value, null);
                } else {
                    trans_searchChkMasterCalendar.setSensitiveRange(null, trans_searchChkMaster_byId(id).value);
                }
            }

            function trans_searchChkMaster_byId(id) {
                return document.getElementById(id);
            }          
            
            //신용거래 엑셀다운로드
            function trans_searchChkMasterExcel() {

                $("#trans_searchChkMasterForm").attr("action","<c:url value="/trans_searchChkMaster/trans_searchChkMasterListExcel" />");
                document.getElementById("trans_searchChkMasterForm").submit();

            }       
            
            function trans_searchChkMaster_date_search(day){
            
                var oneDate = 1000 * 3600 * 24; 
                var now = new Date();
                var week = new Date(now.getTime() + (oneDate*-6));
                var month = new Date(now.getTime() + (oneDate*-31));
                var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                var date=new Date();

                if(day=="today"){
                    $("#trans_searchChkMasterForm input[id=trans_searchChkMasterForm_from_date]").val(nowTime);
                    $("#trans_searchChkMasterForm input[id=trans_searchChkMasterForm_to_date]").val(nowTime);
                }else if(day=="week"){
                    $("#trans_searchChkMasterForm input[id=trans_searchChkMasterForm_from_date]").val(weekTime);
                    $("#trans_searchChkMasterForm input[id=trans_searchChkMasterForm_to_date]").val(nowTime);
                }else{
                    $("#trans_searchChkMasterForm input[id=trans_searchChkMasterForm_from_date]").val(monthTime);
                    $("#trans_searchChkMasterForm input[id=trans_searchChkMasterForm_to_date]").val(nowTime);
                }
            
            }
                
            //검색조건 초기화
            function trans_searchChkMasterMasterInit($form) {

                searchFormInit($form);

                trans_searchChkMaster_date_search("week");                    
            } 
            
 
                
        </script>

<c:if test="${!ajaxRequest}">
    </body>
</html>
</c:if>        
