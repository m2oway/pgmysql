<%-- 
    Document   : trans_searchFailMasterList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>신용카드 실패거래 조회</title>             
        </head>
        <body>
</c:if>
       
    <body>
        <!--<h1>신용거래</h1>-->
        <div class="right" id="trans_searchFail_search" style="width:100%;">       
            <form id="trans_searchFailForm" method="POST" onsubmit="return false" action="<c:url value="/trans_search/trans_searchFailMasterList" />" modelAttribute="trans_searchFormBean"> 
                <input type="hidden" name="page_size" value="100" >
                <input type="hidden" name="page_no" value="1" >
                <table>
                    <tr>
                        <div class="label">검색일자</div>
                        <div class="input">
                            <input type="text" name="app_start_dt" id="trans_searchFailForm_from_date" value ="${trans_searchFormBean.app_start_dt}" onclick="setSens('trans_searchFailForm_to_date', 'max');" /> ~
                            <input type="text" name="app_end_dt" id="trans_searchFailForm_to_date" value="${trans_searchFormBean.app_end_dt}" onclick="setSens('trans_searchFailForm_from_date', 'min');" />
                        </div>
                        <div class="label">결제채널</div>
                        <div class="input">
                            <select name="pay_chn_cate">
                                <option value="">전체</option>
                                <c:forEach var="code" items="${payChnCateList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <td><input type="button" name="trans_searchFail_search" value="조회"/></td>
                        <td><input type="button" name="mcode_search" value="엑셀"/></td>
                    </tr>
               </table>
            </form>
            <div class="paging">
                <div id="trans_searchFailPaging" style="width: 50%;"></div>
                <div id="trans_searchFailrecinfoArea" style="width: 50%;"></div>
            </div>                        
        </div>
        
         <script type="text/javascript">
            
            var trans_searchFailgrid={};
            var trans_searchFailCalendar;
            
            $(document).ready(function () {
                
                trans_searchFailCalendar = new dhtmlXCalendarObject(["trans_searchFailForm_from_date","trans_searchFailForm_to_date"]);    
                
                var trans_searchFail_searchForm = document.getElementById("trans_searchFail_search");
    
                trans_searchFail_layout = main_trans_searchFail_layout.cells('a').attachLayout("2E");
                     
                trans_searchFail_layout.cells('a').hideHeader();
                trans_searchFail_layout.cells('b').hideHeader();
                 
                trans_searchFail_layout.cells('a').attachObject(trans_searchFail_searchForm);
                trans_searchFail_layout.cells('a').setHeight(55);
                
                trans_searchFailgrid = trans_searchFail_layout.cells('b').attachGrid();
                
                trans_searchFailgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                
                var trans_searchFailheaders = "";
                trans_searchFailheaders += "<center>NO</center>,<center>승인일</center>,<center>결제채널</center>,<center>승인금액</center>,<center>취소금액</center>,<center>승인VAT</center>,<center>취소VAT</center>,<center>승인건</center>,<center>취소건</center>";
                trans_searchFailgrid.setHeader(trans_searchFailheaders);
                trans_searchFailgrid.setColAlign("center,center,center,right,right,right,right,right,right");
                trans_searchFailgrid.setColTypes("txt,txt,txt,edn,edn,edn,edn,edn,edn");
                trans_searchFailgrid.setInitWidths("50,100,100,100,100,100,100,100,100");
                trans_searchFailgrid.setColSorting("str,str,str,int,int,int,int,int,int");
                trans_searchFailgrid.setNumberFormat("0,000",3);
                trans_searchFailgrid.setNumberFormat("0,000",4);
                trans_searchFailgrid.setNumberFormat("0,000",5);
                trans_searchFailgrid.setNumberFormat("0,000",6);
                trans_searchFailgrid.setNumberFormat("0,000",7);
                trans_searchFailgrid.setNumberFormat("0,000",8);
                trans_searchFailgrid.enableColumnMove(true);
                trans_searchFailgrid.setSkin("dhx_skyblue");

                var trans_searchfooters = "총계,#cspan,#cspan";
                trans_searchfooters += ",<div id='trans_searchFail_master3'>0</div>,<div id='trans_searchFail_master4'>0</div>,<div id='trans_searchFail_master5'>0</div>,<div id='trans_searchFail_master6'>0</div>,<div id='trans_searchFail_master7'>0</div>,<div id='trans_searchFail_master8'>0</div>";
                trans_searchFailgrid.attachFooter(trans_searchfooters);

                trans_searchFailgrid.enablePaging(true,Number($("#trans_searchFailForm input[name=page_size]").val()),10,"trans_searchFailPaging",true,"trans_searchFailrecinfoArea");
                trans_searchFailgrid.setPagingSkin("bricks");    
                
                trans_searchFailgrid.attachEvent("onPageChanged", function(ind,fInd,lInd){

                    if(lInd !==0){
                        $("#trans_searchFailForm input[name=page_no]").val(ind);
                        trans_searchFailSearch();
                    }else{
                        return false;
                    }
                    
                });                    

                trans_searchFailgrid.init();
                trans_searchFailgrid.parse(${ls_trans_searchFailMasterList}, trans_searchFailFooterValues,"json");

                trans_searchFailgrid.attachEvent("onRowDblClicked", trans_searchFail_attach);

                //신용거래 정보 검색 이벤트
                $("#trans_searchFailForm input[name=trans_searchFail_search]").click(function () {
                    
                    $("input[name=page_no]").val("1");
                    trans_searchFailSearch();
                    
                });
                  
            });
            
            function trans_searchFailFooterValues() {
		    var trans_searchFail_master8 = document.getElementById("trans_searchFail_master8");
		    var trans_searchFail_master3 = document.getElementById("trans_searchFail_master3");
		    var trans_searchFail_master4 = document.getElementById("trans_searchFail_master4");
		    var trans_searchFail_master5 = document.getElementById("trans_searchFail_master5");
		    var trans_searchFail_master6 = document.getElementById("trans_searchFail_master6");
		    var trans_searchFail_master7 = document.getElementById("trans_searchFail_master7");
		    
		    trans_searchFail_master8.innerHTML =  putComma(sumColumn(8)) ;
		    trans_searchFail_master3.innerHTML =  putComma(sumColumn(3)) ;
		    trans_searchFail_master4.innerHTML =  putComma(sumColumn(4)) ;
		    trans_searchFail_master5.innerHTML =  putComma(sumColumn(5)) ;
		    trans_searchFail_master6.innerHTML =  putComma(sumColumn(6)) ;
		    trans_searchFail_master7.innerHTML =  putComma(sumColumn(7)) ;
		    
		    return true;
            }
		
            function sumColumn(ind) {
                var out = 0;
                for (var i = 0; i < trans_searchFailgrid.getRowsNum(); i++) {
                     out += parseFloat(trans_searchFailgrid.cells2(i, ind).getValue());
                }

                return out;
            }

            //신용거래 상세조회 이벤트
            function trans_searchFail_attach(rowid, col) {

                var app_dt = trans_searchFailgrid.getUserData(rowid, 'app_dt');
                var pay_chn_cate = trans_searchFailgrid.getUserData(rowid, 'pay_chn_cate');
                main_trans_searchFail_layout.cells('b').attachURL("<c:url value="/trans_search/trans_searchFailDetailList" />"+"?app_dt=" + app_dt+"&pay_chn_cate="+pay_chn_cate, true);
            }
            
            //신용거래 정보 조회
            function trans_searchFailSearch() {
                
                $.ajax({
                    url: $("#trans_searchFailForm").attr("action"),
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#trans_searchFailForm").serialize(),
                    success: function (data) {

                        trans_searchFailgrid.clearAll();
                        trans_searchFailgrid.parse($.parseJSON(data),trans_searchFailFooterValues, "json");

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }
            
            function setSens(id, k) {
                // update range
                if (k == "min") {
                    trans_searchFailCalendar.setSensitiveRange(byId(id).value, null);
                } else {
                    trans_searchFailCalendar.setSensitiveRange(null, byId(id).value);
                }
            }

            function byId(id) {
                return document.getElementById(id);
            }                            
                
        </script>
    </body>
</html>
