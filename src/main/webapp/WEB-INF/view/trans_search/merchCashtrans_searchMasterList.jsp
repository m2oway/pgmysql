<%-- 
    Document   : cashtrans_searchMasterList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>현금거래조회</title>             
        </head>
        <body>
</c:if>

        <div class="right" id="cashtrans_search_search" style="width:100%;">       
            <form id="cashtrans_searchForm" method="POST" onsubmit="return false" action="<c:url value="/trans_search/merchCashtrans_searchMasterList" />" modelAttribute="trans_searchFormBean"> 
                <input type="hidden" name="page_size" value="100" >
                <input type="hidden" name="page_no" value="1" >
                <table>
                    <tr>
                        <div class="label">검색일자</div>
                        <div class="input">
                            <input type="text" name="app_start_dt" id="cashtrans_searchForm_from_date" value ="${trans_searchFormBean.app_start_dt}" onclick="setSens('cashtrans_searchForm_to_date', 'max');" /> ~
                            <input type="text" name="app_end_dt" id="cashtrans_searchForm_to_date" value="${trans_searchFormBean.app_end_dt}" onclick="setSens('cashtrans_searchForm_from_date', 'min');" />
                        </div>
                        <div class ="input"><input type="button" name="week" value="1주"/></div>
                        <div class ="input"><input type="button" name="month" value="1달"/></div>
                        <div class="label">결제채널</div>
                        <div class="input">
                            <select name="pay_chn_cate">
                                <option value="">전체</option>
                                <c:forEach var="code" items="${payChnCateList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <td><input type="button" name="cashtrans_search_search" value="조회"/></td>
                        <td><input type="button" name="cashtrans_search_excel" value="엑셀"/></td>
                        <td><input type="button" name="init" value="검색조건지우기"/></td>
                    </tr>
               </table>
            </form>
            <div class="paging">
                <div id="cashtrans_searchPaging" style="width: 50%;"></div>
                <div id="cashtrans_searchrecinfoArea" style="width: 50%;"></div>
            </div>                        
        </div>
        
         <script type="text/javascript">
            
            var cashtrans_searchgrid={};
            var cashtrans_searchCalendar;
            
            $(document).ready(function () {
                
                cashtrans_searchCalendar = new dhtmlXCalendarObject(["cashtrans_searchForm_from_date","cashtrans_searchForm_to_date"]);    
                
                var cashtrans_search_searchForm = document.getElementById("cashtrans_search_search");
    
                cashtrans_search_layout = main_cashtrans_search_layout.cells('a').attachLayout("2E");
                     
                cashtrans_search_layout.cells('a').hideHeader();
                cashtrans_search_layout.cells('b').hideHeader();
                 
                cashtrans_search_layout.cells('a').attachObject(cashtrans_search_searchForm);
                cashtrans_search_layout.cells('a').setHeight(40);
                
                cashtrans_searchgrid = cashtrans_search_layout.cells('b').attachGrid();
                
                cashtrans_searchgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                
                var trans_searchheaders = "";
                trans_searchheaders += "<center>일자</center>,<center>결제채널</center>,<center>가맹점명</center>,<center>결제ID명</center>,<center>승인금액</center>,<center>취소금액</center>,<center>승인VAT</center>,<center>취소VAT</center>,<center>승인건</center>,<center>취소건</center>";
                cashtrans_searchgrid.setHeader(trans_searchheaders);
                cashtrans_searchgrid.setColAlign("center,center,center,center,right,right,right,right,right,right");
                cashtrans_searchgrid.setColTypes("txt,txt,txt,txt,edn,edn,edn,edn,edn,edn");
                cashtrans_searchgrid.setInitWidths("100,150,150,150,100,100,100,100,100,100,100");
                cashtrans_searchgrid.setColSorting("str,str,str,str,int,int,int,int,int,int");
                cashtrans_searchgrid.setNumberFormat("0,000",4);
                cashtrans_searchgrid.setNumberFormat("0,000",5);
                cashtrans_searchgrid.setNumberFormat("0,000",6);
                cashtrans_searchgrid.setNumberFormat("0,000",7);
                cashtrans_searchgrid.setNumberFormat("0,000",8);
                cashtrans_searchgrid.setNumberFormat("0,000",9);
                cashtrans_searchgrid.enableColumnMove(true);
                cashtrans_searchgrid.setSkin("dhx_skyblue");

                var cashtrans_searchfooters = "총계,#cspan,#cspan,#cspan";
                cashtrans_searchfooters += ",<div id='cashtrans_search_master4'>0</div>,<div id='cashtrans_search_master5'>0</div>,<div id='cashtrans_search_master6'>0</div>,<div id='cashtrans_search_master7'>0</div>,<div id='cashtrans_search_master8'>0</div>,<div id='cashtrans_search_master9'>0</div>";
                cashtrans_searchgrid.attachFooter(cashtrans_searchfooters);
/*
                cashtrans_searchgrid.enablePaging(true,Number($("#cashtrans_searchForm input[name=page_size]").val()),10,"cashtrans_searchPaging",true,"cashtrans_searchrecinfoArea");
                cashtrans_searchgrid.setPagingSkin("bricks");    
              
                cashtrans_searchgrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
*/      
/*
                    if(lInd !==0){
                        $("#cashtrans_searchForm input[name=page_no]").val(ind);
                        cashtrans_searchSearch();
                    }else{
                        return false;
                    }
                    */
/*                   
                    if(lInd !==0){
                      $("#cashtrans_searchForm input[name=page_no]").val(ind);      
                       cashtrans_searchgrid.clearAll();
                        cashtrans_searchSearch();
                    }else{
                       $("#cashtrans_searchForm input[name=page_no]").val(1);
                        cashtrans_searchgrid.clearAll();
                         cashtrans_searchSearch();
                    }                          
                    
                });                    
*/
                cashtrans_searchgrid.init();
                cashtrans_searchgrid.parse(${ls_cashtrans_searchMasterList}, cashtrans_searchFooterValues,"json");

                cashtrans_searchgrid.attachEvent("onRowDblClicked", cashtrans_search_attach);

                //신용거래 정보 검색 이벤트
                $("#cashtrans_searchForm input[name=cashtrans_search_search]").click(function () {
                    /*
                    $("input[name=page_no]").val("1");
                    */
                    cashtrans_searchSearch();
                    
                   /*
                    cashtrans_searchgrid.changePage(1);
                    return false;
                    */
                });
                $("#cashtrans_searchForm input[name=week]").click(function(){
                    cash_date_search("week");
                 });

                $("#cashtrans_searchForm input[name=month]").click(function(){
                    cash_date_search("month");
                 });
                 
                //현금거래 정보 엑셀다운로드 이벤트
                $("#cashtrans_searchForm input[name=cashtrans_search_excel]").click(function () {
                    cashTrans_searchExcel();
                });    
                
                $("#cashtrans_searchForm input[name=init]").click(function () {
                    trans_searchMasterInit();
                });  
                  
            });
            
            function cashtrans_searchFooterValues() {
		    
		    var cashtrans_search_master4 = document.getElementById("cashtrans_search_master4");
		    var cashtrans_search_master5 = document.getElementById("cashtrans_search_master5");
		    var cashtrans_search_master6 = document.getElementById("cashtrans_search_master6");
		    var cashtrans_search_master7 = document.getElementById("cashtrans_search_master7");
                    var cashtrans_search_master8 = document.getElementById("cashtrans_search_master8");
                    var cashtrans_search_master9 = document.getElementById("cashtrans_search_master9");
		    
		    cashtrans_search_master4.innerHTML =  putComma(sumColumn(4)) ;
		    cashtrans_search_master5.innerHTML =  putComma(sumColumn(5)) ;
		    cashtrans_search_master6.innerHTML =  putComma(sumColumn(6)) ;
		    cashtrans_search_master7.innerHTML =  putComma(sumColumn(7)) ;
                    cashtrans_search_master8.innerHTML =  putComma(sumColumn(8)) ;
                    cashtrans_search_master9.innerHTML =  putComma(sumColumn(9)) ;
		    
		    return true;
            }
		
            function sumColumn(ind) {
                var out = 0;
                for (var i = 0; i < cashtrans_searchgrid.getRowsNum(); i++) {
                     out += parseFloat(cashtrans_searchgrid.cells2(i, ind).getValue());
                }

                return out;
            }

            //신용거래 상세조회 이벤트
            function cashtrans_search_attach(rowid, col) {

                var app_dt = cashtrans_searchgrid.getUserData(rowid, 'app_dt');
                var pay_chn_cate = cashtrans_searchgrid.getUserData(rowid, 'pay_chn_cate');
                var onffmerch_no = cashtrans_searchgrid.getUserData(rowid, 'onffmerch_no');
                var onfftid = cashtrans_searchgrid.getUserData(rowid, 'onfftid');
                main_cashtrans_search_layout.cells('b').attachURL("<c:url value="/trans_search/merchCashtrans_searchDetailList" />"+"?app_start_dt=" + app_dt+"&app_end_dt=" + app_dt+"&pay_chn_cate="+pay_chn_cate+"&onffmerch_no="+onffmerch_no+"&onfftid="+onfftid, true);
            }
            
            //신용거래 정보 조회
            function cashtrans_searchSearch() {
                
                $.ajax({
                    url: "<c:url value="/trans_search/merchCashtrans_searchMasterList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#cashtrans_searchForm").serialize(),
                    success: function (data) {

                        cashtrans_searchgrid.clearAll();
                        cashtrans_searchgrid.parse($.parseJSON(data),cashtrans_searchFooterValues, "json");

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }
            
            function setSens(id, k) {
                // update range
                if (k == "min") {
                    cashtrans_searchCalendar.setSensitiveRange(byId(id).value, null);
                } else {
                    cashtrans_searchCalendar.setSensitiveRange(null, byId(id).value);
                }
            }

            function byId(id) {
                return document.getElementById(id);
            } 
            function cash_date_search(day){
                    var oneDate = 1000 * 3600 * 24; 
                    var now = new Date();
                    var week = new Date(now.getTime() + (oneDate*-6));
                    var month = new Date(now.getTime() + (oneDate*-31));
                    var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                    var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                    var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                    var date=new Date();

                    if(day=="today"){
                        $("#cashtrans_searchForm input[id=cashtrans_searchForm_from_date]").val(nowTime);
                        $("#cashtrans_searchForm input[id=cashtrans_searchForm_to_date]").val(nowTime);
                    }else if(day=="week"){
                        $("#cashtrans_searchForm input[id=cashtrans_searchForm_from_date]").val(weekTime);
                        $("#cashtrans_searchForm input[id=cashtrans_searchForm_to_date]").val(nowTime);
                    }else{
                        $("#cashtrans_searchForm input[id=cashtrans_searchForm_from_date]").val(monthTime);
                        $("#cashtrans_searchForm input[id=cashtrans_searchForm_to_date]").val(nowTime);
                    }
                }
                
            //현금거래 엑셀다운로드
            function cashTrans_searchExcel() {

                $("#cashtrans_searchForm").attr("action","<c:url value="/trans_search/merchCashTrans_searchMasterListExcel" />");
                document.getElementById("cashtrans_searchForm").submit();

            }     
            function trans_searchCashDetailMasterInit($form) {
                searchFormInit($form);
                cash_date_search("week");                    
            } 
                
        </script>
    </body>
</html>
