<%-- 
    Document   : trans_searchFailFailDetailList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>신용카드 실패 상세조회</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">

                var trans_searchFaildetailgrid={};
                var trans_searchFaildetail_layout={};
                var trans_searchFaildetailCalendar;

                $(document).ready(function () {
                    
                    trans_searchFaildetailCalendar = new dhtmlXCalendarObject(["trans_searchFailForm_date","trans_searchFailto_date"]);    

                    var trans_searchFaildetailForm = document.getElementById("trans_searchFaildetail_search");
                    trans_searchFaildetail_layout = main_trans_searchFail_layout.cells('a').attachLayout("2E");
                    trans_searchFaildetail_layout.cells('a').hideHeader();
                    trans_searchFaildetail_layout.cells('b').hideHeader();
                    trans_searchFaildetail_layout.cells('a').attachObject(trans_searchFaildetailForm);
                    trans_searchFaildetail_layout.cells('a').setHeight(85);
                    trans_searchFaildetailgrid = trans_searchFaildetail_layout.cells('b').attachGrid();
                    trans_searchFaildetailgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");

                    var trans_searchdetailheaders = "";
                    trans_searchdetailheaders +=  "<center>NO</center>,<center>가맹점명</center>,<center>결제ID명</center>,<center>결제요청일시</center>,<center>고객명</center>";
                    trans_searchdetailheaders += ",<center>고객전화번호</center>,<center>상품명</center>,<center>거래종류</center>,<center>카드사</center>,<center>카드번호</center>";
                    trans_searchdetailheaders += ",<center>할부</center>,<center>승인금액</center>,<center>비과세금액</center>,<center>부가세</center>,<center>거래결과</center>";
                    trans_searchdetailheaders += ",<center>결제채널</center>,<center>결과코드</center>,<center>결과메시지</center>";
                    trans_searchdetailheaders += ",<center>매입사</center>,<center>결제IP</center>";                    
                    
                    trans_searchFaildetailgrid.setHeader(trans_searchdetailheaders);
                    var trans_searchdetailColAlign = "";
                    trans_searchdetailColAlign +=  "center,center,center,center,center";
                    trans_searchdetailColAlign += ",center,center,center,center,center";
                    trans_searchdetailColAlign += ",center,right,right,right,center";
                    trans_searchdetailColAlign += ",center,center,center";
                    trans_searchdetailColAlign += ",right,center";
                    trans_searchFaildetailgrid.setColAlign(trans_searchdetailColAlign);
                    var trans_searchdetailColTypes = "";
                    trans_searchdetailColTypes +=  "txt,txt,txt,txt,txt";
                    trans_searchdetailColTypes += ",txt,txt,txt,txt,txt";
                    trans_searchdetailColTypes += ",txt,edn,edn,edn,txt";
                    trans_searchdetailColTypes += ",txt,txt,txt";
                    trans_searchdetailColTypes += ",txt,txt";
                    trans_searchFaildetailgrid.setColTypes(trans_searchdetailColTypes);
                    var trans_searchdetailInitWidths = "";
                    trans_searchdetailInitWidths +=   "50,150,150,100,100";
                    trans_searchdetailInitWidths += ",100,150,80,80,120";
                    trans_searchdetailInitWidths += ",80,90,90,90,90";
                    trans_searchdetailInitWidths += ",90,90,200";
                    trans_searchdetailInitWidths += ",90,100";
                    trans_searchFaildetailgrid.setInitWidths(trans_searchdetailInitWidths);
                    var trans_searchdetailColSorting = "";
                    trans_searchdetailColSorting +=  "str,str,str,str,str";
                    trans_searchdetailColSorting += ",str,str,str,str,str";
                    trans_searchdetailColSorting += ",str,int,int,int,str";
                    trans_searchdetailColSorting += ",str,str,str";
                    trans_searchdetailColSorting += ",str,str";
                    trans_searchFaildetailgrid.setColSorting(trans_searchdetailColSorting);
                    trans_searchFaildetailgrid.setNumberFormat("0,000",11);
                    trans_searchFaildetailgrid.setNumberFormat("0,000",12);
                    trans_searchFaildetailgrid.setNumberFormat("0,000",13);                                       

                    trans_searchFaildetailgrid.enableColumnMove(true);
                    trans_searchFaildetailgrid.setSkin("dhx_skyblue");
                    trans_searchFaildetailgrid.enablePaging(true,Number($("#trans_searchFaildetailForm input[name=page_size]").val()),10,"trans_searchFaildetailPaging",true,"trans_searchFaildetailrecinfoArea");
                    trans_searchFaildetailgrid.setPagingSkin("bricks");

                    //페이징 처리
                    trans_searchFaildetailgrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        /*
                        if(lInd !==0){
                            $("#trans_searchFaildetailForm input[name=page_no]").val(ind);
                            trans_searchFaildetailSearch();
                        }else{
                            return false;
                        }
                        */
                        
                        if(lInd !==0){
                           $("#trans_searchFaildetailForm input[name=page_no]").val(ind);             
                           trans_searchFaildetailgrid.clearAll();
                           trans_searchFaildetailSearch();
                        }else{
                             $("#trans_searchFaildetailForm input[name=page_no]").val(1);
                            trans_searchFaildetailgrid.clearAll();
                             trans_searchFaildetailSearch();
                        }                            
                    });

                    trans_searchFaildetailgrid.init();
                    trans_searchFaildetailgrid.parse(${ls_trans_searchFailDetailList}, "json");

                    //상세코드검색
                    $("#trans_searchFaildetailForm input[name=trans_searchFaildetail_search]").click(function(){

                        /*
                        $("input[name=page_no]").val("1");

                        trans_searchFaildetailgrid.clearAll();

                        trans_searchFaildetailSearch();
                        */
                        trans_searchFaildetailgrid.changePage(1);
                        return false;   

                    });

                    $("#trans_searchFaildetailForm input[name=week]").click(function(){
                        trans_searchFail_date_search("week");
                    });

                    $("#trans_searchFaildetailForm input[name=month]").click(function(){
                        trans_searchFail_date_search("month");
                    });
                    //검색조건 초기화
                    $("#trans_searchFaildetailForm input[name=init]").click(function () {
                        trans_searchFailDetailMasterInit($("#trans_searchFaildetailForm"));
                    });

                    
                    //엑셀다운로드
                    $("#trans_searchFaildetailForm input[name=trans_searchFaildetail_excel]").click(function(){
                        
                        trans_searchFaildetailExcel();

                    });                    
                    

                });
                function trans_searchFail_date_search(day){
                    var oneDate = 1000 * 3600 * 24; 
                    var now = new Date();
                    var week = new Date(now.getTime() + (oneDate*-6));
                    var month = new Date(now.getTime() + (oneDate*-31));
                    var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                    var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                    var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                    var date=new Date();

                    if(day=="today"){
                        $("#trans_searchFaildetailForm input[id=trans_searchFailForm_date]").val(nowTime);
                        $("#trans_searchFaildetailForm input[id=trans_searchFailto_date]").val(nowTime);
                    }else if(day=="week"){
                        $("#trans_searchFaildetailForm input[id=trans_searchFailForm_date]").val(weekTime);
                        $("#trans_searchFaildetailForm input[id=trans_searchFailto_date]").val(nowTime);
                    }else{
                        $("#trans_searchFaildetailForm input[id=trans_searchFailForm_date]").val(monthTime);
                        $("#trans_searchFaildetailForm input[id=trans_searchFailto_date]").val(nowTime);
                    }
                }

        //코드 정보 조회
                function trans_searchFaildetailSearch() {
                
                $.ajax({
                    url: "<c:url value="/trans_search/merchTrans_searchFailDetailList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#trans_searchFaildetailForm").serialize(),
                    success: function (data) {

                        var jsonData = $.parseJSON(data);

                        trans_searchFaildetailgrid.parse(jsonData,"json");

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
                    return false;
                    
            }
            var dhxSelPopupWins=new dhtmlXWindows();
                
                function Trans_searchFail_setSens(id, k) {
                // update range
                if (k == "min") {
                    trans_searchFailCalendar.setSensitiveRange(Trans_searchFail_byId(id).value, null);
                } else {
                    trans_searchFailCalendar.setSensitiveRange(null, Trans_searchFail_byId(id).value);
                }
            }

            function Trans_searchFail_byId(id) {
                return document.getElementById(id);
            }  
                
                
                function trans_searchFaildetailExcel(){
                    $("#trans_searchFaildetailForm").attr("action","<c:url value="/trans_search/merchTrans_searchFailDetailListExcel" />");
                    document.getElementById("trans_searchFaildetailForm").submit();            
                } 
            function trans_searchFailDetailMasterInit($form) {

                searchFormInit($form);

                trans_searchFail_date_search("week");                    
            } 
                
    </script>
    <body>


        <div class="right" id="trans_searchFaildetail_search" style="width:100%;">
             <form id="trans_searchFaildetailForm" method="POST" action="<c:url value="/trans_search/merchTrans_searchFailDetailList" />" modelAttribute="trans_searchFormBean">

                <input type="hidden" name="page_size" value="100" >
                <input type="hidden" name="page_no" value="1" >
                
                    <table>
                        <tr>
                            <div class="label">거래일</div>
                            <div class="input">
                            <input type="text" size="12" name="app_start_dt" id="trans_searchFailForm_date" value ="${trans_searchFormBean.app_start_dt}" onclick="trans_searchdetail_setSens('trans_searchFaildetailForm_to_date', 'max');" /> ~
                            <input type="text"  size="12" name="app_end_dt" id="trans_searchFailto_date" value="${trans_searchFormBean.app_end_dt}" onclick="trans_searchdetail_setSens('trans_searchFailto_date', 'min');" /> </div>                            
                            <div class ="input"><input type="button" name="week" value="1주"/></div>
                            <div class ="input"><input type="button" name="month" value="1달"/></div> 
                            <!--
                            <div class="label">거래상태</div>
                            <div class="input">
                                <select name="tran_status">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${tranStatusList.tb_code_details}">
                                        <option value="${code.detail_code}" <c:if test="${code.detail_code == trans_searchFormBean.tran_status}">selected</c:if> >${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="label">거래결과</div>
                            <div class="input">
                                <select name="result_status">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${resultStatusList.tb_code_details}">
                                        <option value="${code.detail_code}" <c:if test="${code.detail_code == trans_searchFormBean.result_status}">selected</c:if> >${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>    -->                        
                            <div class="label2">결제ID명</div>
                            <div class="input">
                                <input type="text" name="onfftid_nm" value ="${trans_searchFormBean.onfftid_nm}" />
                            </div>    
                            <div class="label2">결제ID</div>
                            <div class="input">
                                <input type="text" name="onfftid" value ="${trans_searchFormBean.onfftid}" />
                            </div>                                
                            <div class="label">고객명</div>
                            <div class="input">
                                <input type="text" name="user_nm" value ="${trans_searchFormBean.user_nm}" />
                            </div>                               
                            <div class="label">고객전화번호</div>
                            <div class="input">
                                <input type="text" name="user_phone2" value ="${trans_searchFormBean.user_phone2}" />
                            </div>                                                             
                            <div class="label">상품명</div>
                            <div class="input">
                                <input type="text" name="product_nm" value ="${trans_searchFormBean.product_nm}" />
                            </div>                                                   
                            <div class="label">승인금액</div>
                            <div class="input">
                                <input type="text" name="tot_amt" value ="${trans_searchFormBean.tot_amt}" />
                            </div>                                               
                            <div class="label">카드번호</div>
                            <div class="input">
                                <input type="text" name="card_num" value ="${trans_searchFormBean.card_num}" />
                            </div>
                            <div class="label">카드사</div>
                            <div class="input">
                                <select name="iss_cd">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${issCdList.tb_code_details}">
                                        <option value="${code.detail_code}" <c:if test="${code.detail_code == trans_searchFormBean.result_status}">selected</c:if> >${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>              

                            <td><input type="button" name="trans_searchFaildetail_search" value="조회"/></td>
                            <td><input type="button" name="trans_searchFaildetail_excel" value="엑셀"/></td>
                            <td><input type="button" name="init" value="검색조건지우기"/></td>                            
                        </tr>
                    </table>
                        
            </form>

            <div class="paging">
                <div id="trans_searchFaildetailPaging" style="width: 50%;"></div>
                <div id="trans_searchFaildetailrecinfoArea" style="width: 50%;"></div>
            </div>
                                
         </div>

    </body>
</html>
