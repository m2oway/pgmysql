<%-- 
    Document   : codeDetailMaster
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>신용카드 상세조회</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">

                var trans_searchChkgrid={};
                var trans_searchChk_layout={};
                var complainMasterWindow;
                var trans_searchChkCalendar1;

                $(document).ready(function () {
                    
                    trans_searchChkCalendar1 = new dhtmlXCalendarObject(["trans_searchChkForm_from_date","trans_searchChkForm_to_date"]);    
                
                    var trans_searchChkForm = document.getElementById("trans_searchChk_search");
                    trans_searchChk_layout = main_trans_searchchk_layout.cells('b').attachLayout("2E");
                    trans_searchChk_layout.cells('a').hideHeader();
                    trans_searchChk_layout.cells('b').hideHeader();
                    trans_searchChk_layout.cells('a').attachObject(trans_searchChkForm);
                    trans_searchChk_layout.cells('a').setHeight(85);
                    trans_searchChkgrid = trans_searchChk_layout.cells('b').attachGrid();
                    trans_searchChkgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");

                    var trans_searchChkheaders = "";
                    trans_searchChkheaders +=  "<center>NO</center>,<center>가맹점명</center>,<center>결제ID명</center>,<center>고객명</center>,<center>고객전화번호</center>";
                    trans_searchChkheaders += ",<center>상품명</center>,<center>거래종류</center>,<center>카드사</center>,<center>카드번호</center>,<center>할부</center>";
                    trans_searchChkheaders += ",<center>승인금액</center>,<center>비과세금액</center>,<center>부가세</center>,<center>승인번호</center>,<center>승인일시</center>";
                    trans_searchChkheaders += ",<center>취소일</center>,<center>거래상태</center>,<center>거래결과</center>,<center>민원</center>,<center>영수증</center>";
                    trans_searchChkheaders += ",<center>취소</center>,<center>결제채널</center>,<center>결과코드</center>,<center>결과메시지</center>,<center>TID</center>";
                    trans_searchChkheaders += ",<center>가맹점번호</center>,<center>매입사</center>,<center>수수료</center>,<center>입금액</center>,<center>ONOFF수수료</center>";
                    trans_searchChkheaders += ",<center>ONOFF지급액</center>,<center>지급예정일</center>,<center>거래일련번호</center>,<center>결제IP</center>,<center>결제자ID</center>";
                    trans_searchChkgrid.setHeader(trans_searchChkheaders);
                    var trans_searchChkColAlign = "";
                    trans_searchChkColAlign +=  "center,center,center,center,center";
                    trans_searchChkColAlign += ",center,center,center,center,center";
                    trans_searchChkColAlign += ",right,right,right,center,center";
                    trans_searchChkColAlign += ",center,center,center,center,center";
                    trans_searchChkColAlign += ",center,center,center,center,center";
                    trans_searchChkColAlign += ",center,center,right,right,right";
                    trans_searchChkColAlign += ",right,center,center,center,center";
                    trans_searchChkgrid.setColAlign(trans_searchChkColAlign);
                    var trans_searchChkColTypes = "";
                    trans_searchChkColTypes +=  "txt,txt,txt,txt,txt";
                    trans_searchChkColTypes += ",txt,txt,txt,txt,txt";
                    trans_searchChkColTypes += ",edn,edn,edn,txt,txt";
                    trans_searchChkColTypes += ",txt,txt,txt,txt,txt";
                    trans_searchChkColTypes += ",txt,txt,txt,txt,txt";
                    trans_searchChkColTypes += ",txt,txt,edn,edn,edn";
                    trans_searchChkColTypes += ",edn,txt,txt,txt,txt";
                    trans_searchChkgrid.setColTypes(trans_searchChkColTypes);
                    var trans_searchChkInitWidths = "";
                    trans_searchChkInitWidths +=   "40,150,150,80,90";
                    trans_searchChkInitWidths += ",100,80,60,110,60";
                    trans_searchChkInitWidths += ",90,90,90,90,110";
                    trans_searchChkInitWidths += ",80,70,70,90,90";
                    trans_searchChkInitWidths += ",90,100,100,150,100";
                    trans_searchChkInitWidths += ",100,100,90,90,90";
                    trans_searchChkInitWidths += ",90,90,100,100,100";
                    trans_searchChkgrid.setInitWidths(trans_searchChkInitWidths);
                    var trans_searchChkColSorting = "";
                    trans_searchChkColSorting +=  "str,str,str,str,str";
                    trans_searchChkColSorting += ",str,str,str,str,str";
                    trans_searchChkColSorting += ",int,int,int,str,str";
                    trans_searchChkColSorting += ",str,str,str,str,str";
                    trans_searchChkColSorting += ",str,str,str,str,str";
                    trans_searchChkColSorting += ",str,str,int,int,int";
                    trans_searchChkColSorting += ",int,str,str,str,str";
                    trans_searchChkgrid.setColSorting(trans_searchChkColSorting);
                    trans_searchChkgrid.setNumberFormat("0,000",10);
                    trans_searchChkgrid.setNumberFormat("0,000",11);
                    trans_searchChkgrid.setNumberFormat("0,000",12);
                    trans_searchChkgrid.setNumberFormat("0,000",27);
                    trans_searchChkgrid.setNumberFormat("0,000",28);            
                    trans_searchChkgrid.setNumberFormat("0,000",29);
                    trans_searchChkgrid.setNumberFormat("0,000",30);                                
                    
                    trans_searchChkgrid.enableColumnMove(true);
                    trans_searchChkgrid.setSkin("dhx_skyblue");
                    trans_searchChkgrid.enablePaging(true,Number($("#trans_searchChkForm input[name=page_size]").val()),10,"trans_searchChkPaging",true,"trans_searchChkrecinfoArea");
                    trans_searchChkgrid.setPagingSkin("bricks");

                    <c:if test="${trans_searchFormBean.ses_user_cate == '03'}">
                    trans_searchChkgrid.setColumnHidden(18,true);  
                    trans_searchChkgrid.setColumnHidden(19,true); 
                    trans_searchChkgrid.setColumnHidden(20,true); 
                    </c:if>                            
                    

                    //페이징 처리
                    trans_searchChkgrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        if(lInd !==0){
                            $("#trans_searchChkForm input[name=page_no]").val(ind);
                            trans_searchChkgrid.clearAll();
                            trans_searchChkSearch();
                        }else{
                            $("#trans_searchChkForm input[name=page_no]").val(1);
                            trans_searchChkgrid.clearAll();
                            trans_searchChkSearch();
                        }
                    });

                    trans_searchChkgrid.init();
                    trans_searchChkgrid.parse(${ls_trans_searchDetailList}, "json");

                    <c:if test="${trans_searchFormBean.ses_user_cate == '00' ||trans_searchFormBean.ses_user_cate == '01' || trans_searchFormBean.ses_user_cate == '02'}">
                    //trans_searchChkgrid.attachEvent("onRowDblClicked", Trans_search_MgrAction);
                    </c:if>

                    //상세 검색
                    $("#trans_searchChkForm input[name=trans_searchChk_search]").unbind("click").bind("click", function (){

                        //$("input[name=page_no]").val("1");
                        //trans_searchChkSearch();
                        trans_searchChkgrid.changePage(1);
                    });
                    
                    //민원 등록
                    $("button[name=complain_insert]").unbind("click").bind("click", function (){

                        complainMasterWindow = new dhtmlXWindows();
                        w1 = complainMasterWindow.createWindow("w1", 25, 25, 700, 580);
                        w1.setText("민원 등록");
                        complainMasterWindow.window('w1').setModal(true);
                        tran_seq = $(this).attr("tran_seq");
                        w1.attachURL("<c:url value="/complain/complainMasterInsert" />" + "?tran_seq=" + tran_seq + "&complain_popup_yn=Y", true);   
                        
                        return false;

                    });                
                    
                    
                    //영수증보기
                    $("button[name=bill_view_bt]").unbind("click").bind("click", function (){

                        var tptrans_searcheq = $(this).attr("tran_seq");
                        var tpresult_cd =  $(this).attr("tran_result_cd"); 
                        if(tpresult_cd == '0000')
                        {    
                            var strUrl = "../app/appResult?tran_seq="+tptrans_searcheq+"&tran_result_cd="+tpresult_cd+"&auctioncate=1";
                            var winx = window.open(strUrl,"영수증", "width=400,height=700,scrollbars=yes,resizeable=no");
                            winx.focus();
                        }
                        
                        return false;

                    });            
                    
                    //취소버튼
                    $("button[name=cncl_view_bt]").unbind("click").bind("click", function (){

                        var tptrans_searcheq = $(this).attr("tran_seq");
                        var tporgcno =  $(this).attr("org_cno"); 
                        
                        if(confirm("승인거래를 취소하시겠습니까?"))
                        {
                            
                            $("#frm_card_cncl input[id=tran_seq]").val(tptrans_searcheq);
                            $("#frm_card_cncl input[id=org_cno]").val(tporgcno);
                            
                             $.ajax({
                                        url : "../app/mgrAction",
                                        type : "POST",
                                        async : true,
                                        dataType : "json",
                                        data: $("#frm_card_cncl").serialize(),
                                        success : function(data) {
                                            if(data.result_cd == '0000')
                                            {
                                                alert(data.result_msg);
                                                trans_searchChkgrid.clearAll();
                                                trans_searchChkSearch();
                                            }
                                            else
                                            {
                                                alert("[" +data.result_cd+ "]" + data.result_msg);
                                            }

                                        },
                                        error : function() { 
                                            alert("취소 실패");
                                        }
                                    });	            
                        }
                        
                        return false;

                    });                             
                    
                    //민원 보기
                    $("button[name=complain_search]").unbind("click").bind("click", function (){

                        complainMasterWindow = new dhtmlXWindows();
                        w1 = complainMasterWindow.createWindow("w1", 25, 25, 700, 850);
                        w1.setText("민원 보기");
                        complainMasterWindow.window('w1').setModal(true);
                        complain_seq = $(this).attr("complain_seq");
                        tran_seq = $(this).attr("tran_seq");
                        w1.attachURL("<c:url value="/complain/complainMasterUpdate" />" + "?complain_seq=" + complain_seq + "&tran_seq=" + tran_seq + "&complain_popup_yn=Y", true);   
                        
                        return false;

                    });                    
                    
                    //신용거래 정보 엑셀다운로드 이벤트
                    $("#trans_searchChkForm input[name=trans_searchChk_excel]").click(function () {

                        trans_searchDetailExcel();

                    });               
                     $("#trans_searchChkForm input[name=week]").click(function(){
                        trans_searchChk_date_search("week");
                    });

                    $("#trans_searchChkForm input[name=month]").click(function(){
                        trans_searchChk_date_search("month");
                    });
                    //검색조건 초기화
                    $("#trans_searchChkForm input[name=init]").click(function () {
                    
                        trans_searchDetailMasterInit($("#trans_searchChkForm"));

                    });  
                    
                });
                 function trans_searchChk_date_search(day){
                    var oneDate = 1000 * 3600 * 24; 
                    var now = new Date();
                    var week = new Date(now.getTime() + (oneDate*-6));
                    var month = new Date(now.getTime() + (oneDate*-31));
                    var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                    var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                    var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                    var date=new Date();

                    if(day=="today"){
                        $("#trans_searchChkForm input[id=trans_searchChkForm_from_date]").val(nowTime);
                        $("#trans_searchChkForm input[id=trans_searchChkForm_to_date]").val(nowTime);
                    }else if(day=="week"){
                        $("#trans_searchChkForm input[id=trans_searchChkForm_from_date]").val(weekTime);
                        $("#trans_searchChkForm input[id=trans_searchChkForm_to_date]").val(nowTime);
                    }else{
                        $("#trans_searchChkForm input[id=trans_searchChkForm_from_date]").val(monthTime);
                        $("#trans_searchChkForm input[id=trans_searchChkForm_to_date]").val(nowTime);
                    }
                }
                //거래 상세 정보 조회
                function trans_searchChkSearch() {
                    $.ajax({
                        url: "<c:url value="/trans_search/trans_searchChkDetailList" />",
                        type: "POST",
                        async: true,
                        dataType: "json",
                        data: $("#trans_searchChkForm").serialize(),
                        success: function (data) {

                            var jsonData = $.parseJSON(data);
                            trans_searchChkgrid.parse(jsonData,"json");

                            //민원 등록
                            $("button[name=complain_insert]").unbind("click").bind("click", function (){

                                complainMasterWindow = new dhtmlXWindows();
                                w1 = complainMasterWindow.createWindow("w1", 1, 1, 700, 500);
                                w1.setText("민원 등록");
                                complainMasterWindow.window('w1').setModal(true);
                                tran_seq = $(this).attr("tran_seq");
                                w1.attachURL("<c:url value="/complain/complainMasterInsert" />" + "?tran_seq=" + tran_seq + "&complain_popup_yn=Y", true);   

                                return false;

                            });                    

                            //민원 보기
                            $("button[name=complain_search]").unbind("click").bind("click", function (){

                                complainMasterWindow = new dhtmlXWindows();
                                w1 = complainMasterWindow.createWindow("w1", 1, 1, 700, 900);
                                w1.setText("민원 등록");
                                complainMasterWindow.window('w1').setModal(true);
                                complain_seq = $(this).attr("complain_seq");
                                tran_seq = $(this).attr("tran_seq");
                                w1.attachURL("<c:url value="/complain/complainMasterUpdate" />" + "?complain_seq=" + complain_seq + "&tran_seq=" + tran_seq + "&complain_popup_yn=Y", true);   

                                return false;

                            });      
                            

                            //영수증보기
                            $("button[name=bill_view_bt]").unbind("click").bind("click", function (){

                                var tptrans_searcheq = $(this).attr("tran_seq");
                                var tpresult_cd =  $(this).attr("tran_result_cd"); 
                                if(tpresult_cd == '0000')
                                {    
                                    var strUrl = "../app/appResult?tran_seq="+tptrans_searcheq+"&tran_result_cd="+tpresult_cd+"&auctioncate=1";
                                    var winx = window.open(strUrl,"영수증", "width=400,height=700,scrollbars=yes,resizeable=no");
                                    winx.focus();
                                }

                                return false;

                            });     
                            
                    
                            //취소버튼
                            $("button[name=cncl_view_bt]").unbind("click").bind("click", function (){

                                var tptrans_searcheq = $(this).attr("tran_seq");
                                var tporgcno =  $(this).attr("org_cno"); 

                                if(confirm("승인거래를 취소하시겠습니까?"))
                                {

                                    $("#frm_card_cncl input[id=tran_seq]").val(tptrans_searcheq);
                                    $("#frm_card_cncl input[id=org_cno]").val(tporgcno);

                                     $.ajax({
                                                url : "../app/mgrAction",
                                                type : "POST",
                                                async : true,
                                                dataType : "json",
                                                data: $("#frm_card_cncl").serialize(),
                                                success : function(data) {
                                                    if(data.result_cd == '0000')
                                                    {
                                                        alert(data.result_msg);
                                                        trans_searchChkgrid.clearAll();
                                                        trans_searchChkSearch();
                                                    }
                                                    else
                                                    {
                                                        alert("[" +data.result_cd+ "]" + data.result_msg);
                                                    }

                                                },
                                                error : function() { 
                                                    alert("취소 실패");
                                                }
                                            });	            
                                }



                                return false;

                            });                                                         


                        },
                        error: function () {
                            alert("조회 실패");
                        }
                    });
                    return false;
                }
            
                function trans_searchDetailExcel(){
                    $("#trans_searchChkForm").attr("action","<c:url value="/trans_search/trans_searchDetailListExcel" />");
                    document.getElementById("trans_searchChkForm").submit();            
                }
                
                <c:if test="${trans_searchFormBean.ses_user_cate == '00' ||trans_searchFormBean.ses_user_cate == '01' || trans_searchFormBean.ses_user_cate == '02'}">            
                        function Trans_search_MgrAction(rowId, col) 
                        {
                            var tptrans_searcheq = trans_searchChkgrid.getUserData(rowId, 'tran_seq');
                            var tpresult_cd =  trans_searchChkgrid.getUserData(rowId, 'result_cd');
                            if(tpresult_cd == '0000')
                            {    
                                var strUrl = "../app/appResult?tran_seq="+tptrans_searcheq+"&tran_result_cd="+tpresult_cd+"&auctioncate=1";
                                var winx = window.open(strUrl,"결제내역", "width=400,height=700,scrollbars=yes,resizeable=no");
                                winx.focus();
                            }
                        }
                </c:if>  
                    
                var dhxSelPopupWins=new dhtmlXWindows();
                    
                //ONOFF가맹점번호 팝업 클릭 이벤트
                $("#trans_searchChkForm input[name=merch_nm]").unbind("click").bind("click", function (){

                    onoffTrans_searchDetailMerchSelectPopup();

                });                           
                    
                function onoffTrans_searchDetailMerchSelectPopup(){
                    onoffObject.onffmerch_no = $("#trans_searchChkForm input[name=onffmerch_no]");
                    onoffObject.merch_nm = $("#trans_searchChkForm input[name=merch_nm]");
                    //ONOFF가맹점정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("가맹점 선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                }
                                  
                //ONOFF결제ID 팝업 클릭 이벤트
                $("#trans_searchChkForm input[name=onfftid_nm]").unbind("click").bind("click", function (){

                    onoffTrans_searchDetailTidSelectPopup();

                });                                
                
                function onoffTrans_searchDetailTidSelectPopup(){
                    onoffObject.onfftid_nm = $("#trans_searchChkForm input[name=onfftid_nm]");
                    onoffObject.onfftid = $("#trans_searchChkForm input[name=onfftid]");
                    //ONOFF결제ID 팝업
                    w2 = dhxSelPopupWins.createWindow("OnofftidSelectPopUp", 20, 30, 640, 480);
                    w2.setText("ONOFF결제ID 선택페이지");
                    dhxSelPopupWins.window('OnofftidSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnofftidSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnofftidSelectPopUp"/>', true);   
                }                
              
                   
                
                function trans_searchChk_setSens(id, k) {
                    // update range
                    if (k == "min") {
                        trans_searchChkCalendar1.setSensitiveRange(trans_searchChk_byId(id).value, null);
                    } else {
                        trans_searchChkCalendar1.setSensitiveRange(null, trans_searchChk_byId(id).value);
                    }
                }

                function trans_searchChk_byId(id) {
                    return document.getElementById(id);
                }
                
                function trans_searchDetailMasterInit($form) {

                    searchFormInit($form);

                    trans_searchChk_date_search("week");                    
                } 
                
            </script>

            <div class="right" id="trans_searchChk_search" style="width:100%;">
                <form id="trans_searchChkForm" method="POST" action="<c:url value="/trans_search/trans_searchChkDetailList" />" modelAttribute="trans_searchFormBean">

                    <input type="hidden" name="page_size" value="100" >
                    <input type="hidden" name="page_no" value="1" >
                    <input type="hidden" name="pay_chn_cate" value="${trans_searchFormBean.pay_chn_cate}" >
                    <table>
                        <tr><td>
                            <div class="label">승인일</div>
                            <div class="input">
                            <input type="text" size="10" name="app_start_dt" id="trans_searchChkForm_from_date" value ="${trans_searchFormBean.app_start_dt}" onclick="trans_searchChk_setSens('trans_searchChkForm_to_date', 'max');" /> ~
                            <input type="text" size="10"name="app_end_dt" id="trans_searchChkForm_to_date" value="${trans_searchFormBean.app_end_dt}" onclick="trans_searchChk_setSens('trans_searchChkForm_from_date', 'min');" /> </div>
                            <div class ="input"><input type="button" name="week" value="1주"/></div>
                            <div class ="input"><input type="button" name="month" value="1달"/></div> 
                            <div class="label">거래상태</div>
                            <div class="input">
                                <select name="tran_status">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${tranStatusList.tb_code_details}">
                                        <option value="${code.detail_code}" <c:if test="${code.detail_code == trans_searchFormBean.tran_status}">selected</c:if> >${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="label">거래결과</div>
                            <div class="input">
                                <select name="result_status">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${resultStatusList.tb_code_details}">
                                        <option value="${code.detail_code}" <c:if test="${code.detail_code == trans_searchFormBean.result_status}">selected</c:if> >${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>    
                            <div class="label">가맹점명</div>
                            <div class="input">
                                <input type="text" name="merch_nm" value ="${trans_searchFormBean.merch_nm}" />
                            </div>        
                            <div class="label">가맹점번호</div>
                            <div class="input">
                                <input type="text" name="onffmerch_no" value ="${trans_searchFormBean.onffmerch_no}" />
                            </div>                              
                            <div class="label">결제ID명</div>
                            <div class="input">
                                <input type="text" name="onfftid_nm" value ="${trans_searchFormBean.onfftid_nm}" />
                            </div>    
                            <div class="label">결제ID</div>
                            <div class="input">
                                <input type="text" name="onfftid" value ="${trans_searchFormBean.onfftid}" />
                            </div>    
                            <div class="label">고객명</div>
                            <div class="input">
                                <input type="text" name="user_nm" value ="${trans_searchFormBean.user_nm}" />
                            </div>                               
                            <div class="label">고객전화번호</div>
                            <div class="input">
                                <input type="text" size="14" name="user_phone2" value ="${trans_searchFormBean.user_phone2}" />
                            </div>                                                             
                            <div class="label">상품명</div>
                            <div class="input">
                                <input type="text" name="product_nm" value ="${trans_searchFormBean.product_nm}" />
                            </div>                                                   
                            <div class="label">승인금액</div>
                            <div class="input">
                                <input type="text" size="10" name="tot_amt" value ="${trans_searchFormBean.tot_amt}" />
                            </div>                                               
                            <div class="label">카드번호</div>
                            <div class="input">
                                <input type="text" size="20" name="card_num" value ="${trans_searchFormBean.card_num}" />
                            </div>                                                     
                            <div class="label">승인번호</div>
                            <div class="input">
                                <input type="text" size="10" name="app_no" value ="${trans_searchFormBean.app_no}" />
                            </div>   
                            <div class="label">거래종류</div>
                            <div class="input">
                                <select name="massagetype">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${massagetypeCdList.tb_code_details}">
                                        <option value="${code.detail_code}" <c:if test="${code.detail_code == trans_searchFormBean.massagetype}">selected</c:if> >${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div>                            
                            <div class="label">카드사</div>
                            <div class="input">
                                <select name="iss_cd">
                                    <option value="">전체</option>
                                    <c:forEach var="code" items="${issCdList.tb_code_details}">
                                        <option value="${code.detail_code}" <c:if test="${code.detail_code == trans_searchFormBean.result_status}">selected</c:if> >${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </div></td>              

                            <td><input type="button" name="trans_searchChk_search" value="조회"/></td>
                            <td><input type="button" name="trans_searchChk_excel" value="엑셀"/></td>
                            <td><input type="button" name="init" value="검색조건지우기"/></td>
                        </tr>
                    </table>
                        
                </form>
            <form name="frm_card_cncl" id="frm_card_cncl" method="post" >
                <input type="hidden" name="org_cno" id="org_cno" value=""><input type="hidden" name="mgr_msg" id="mgr_msg" value="" ><input type="hidden" name="EP_tr_cd" id="EP_tr_cd" value="00201000"><input type="hidden" name="mgr_txtype" id="mgr_txtype" value="40"><input type="hidden" name="tran_seq" id="tran_seq" value="">
            </form>
            <div class="paging">
                <div id="trans_searchChkPaging" style="width: 50%;"></div>
                <div id="trans_searchChkrecinfoArea" style="width: 50%;"></div>
            </div>
                                
        </div>

<c:if test="${!ajaxRequest}">
    </body>
</html>    
</c:if>