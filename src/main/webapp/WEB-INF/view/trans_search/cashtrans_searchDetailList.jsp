<%-- 
    Document   : cashtrans_searchDetailList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>현금 상세조회</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">

                var cashtrans_searchdetailgrid={};
                var cashtrans_searchdetail_layout={};
                var cashtrans_searchdetailCalendar;
                
                $(document).ready(function () {
                    
                    cashtrans_searchdetailCalendar = new dhtmlXCalendarObject(["cashtrans_searchForm_date","cashtrans_searchto_date"]);    
                
                    var cashcashtrans_searchdetailForm = document.getElementById("cashtrans_searchdetail_search");
                    cashtrans_searchdetail_layout = main_cashtrans_search_layout.cells('b').attachLayout("2E");
                    cashtrans_searchdetail_layout.cells('a').hideHeader();
                    cashtrans_searchdetail_layout.cells('b').hideHeader();
                    cashtrans_searchdetail_layout.cells('a').attachObject(cashcashtrans_searchdetailForm);
                    cashtrans_searchdetail_layout.cells('a').setHeight(80);
                    cashtrans_searchdetailgrid = cashtrans_searchdetail_layout.cells('b').attachGrid();
                    cashtrans_searchdetailgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");

                    var trans_searchdetailheaders = "";
                    trans_searchdetailheaders +=  "<center>NO</center>,<center>가맹점명</center>,<center>결제ID명</center>,<center>고객명</center>,<center>고객전화번호</center>";
                    trans_searchdetailheaders += ",<center>상품명</center>,<center>거래종류</center>,<center>인증번호</center>,<center>발행구분</center>,<center>인증구분</center>";
                    trans_searchdetailheaders += ",<center>승인금액</center>,<center>비과세금액</center>,<center>부가세</center>,<center>승인번호</center>,<center>승인일시</center>";
                    trans_searchdetailheaders += ",<center>취소일</center>,<center>거래상태</center>,<center>거래결과</center>,<center>민원</center>,<center>영수증</center>";
                    trans_searchdetailheaders += ",<center>취소</center>,<center>결제채널</center>,<center>결과코드</center>,<center>결과메시지</center>,<center>TID</center>";
                    trans_searchdetailheaders += ",<center>결제IP</center>,<center>결제자ID</center>";
                    cashtrans_searchdetailgrid.setHeader(trans_searchdetailheaders);
                    var trans_searchdetailColAlign = "";
                    trans_searchdetailColAlign +=  "center,center,center,center,center";
                    trans_searchdetailColAlign += ",center,center,center,center,center";
                    trans_searchdetailColAlign += ",right,right,right,center,center";
                    trans_searchdetailColAlign += ",center,center,center,center,center";
                    trans_searchdetailColAlign += ",center,center,center,center,center";
                    trans_searchdetailColAlign += ",center,center";
                    cashtrans_searchdetailgrid.setColAlign(trans_searchdetailColAlign);
                    var trans_searchdetailColTypes = "";
                    trans_searchdetailColTypes +=  "txt,txt,txt,txt,txt";
                    trans_searchdetailColTypes += ",txt,txt,txt,txt,txt";
                    trans_searchdetailColTypes += ",edn,edn,edn,txt,txt";
                    trans_searchdetailColTypes += ",txt,txt,txt,txt,txt";
                    trans_searchdetailColTypes += ",txt,txt,txt,txt,txt";
                    trans_searchdetailColTypes += ",txt,txt";
                    cashtrans_searchdetailgrid.setColTypes(trans_searchdetailColTypes);
                    var trans_searchdetailInitWidths = "";
                    trans_searchdetailInitWidths +=   "50,100,100,80,90";
                    trans_searchdetailInitWidths += ",150,80,100,80,80";
                    trans_searchdetailInitWidths += ",90,90,90,90,100";
                    trans_searchdetailInitWidths += ",90,90,90,100,100";
                    trans_searchdetailInitWidths += ",100,150,150,150,150";
                    trans_searchdetailInitWidths += ",150,150";
                    cashtrans_searchdetailgrid.setInitWidths(trans_searchdetailInitWidths);
                    var trans_searchdetailColSorting = "";
                    trans_searchdetailColSorting +=  "str,str,str,str,str";
                    trans_searchdetailColSorting += ",str,str,str,str,str";
                    trans_searchdetailColSorting += ",int,int,int,str,str";
                    trans_searchdetailColSorting += ",str,str,str,str,str";
                    trans_searchdetailColSorting += ",str,str,str,str,str";
                    trans_searchdetailColSorting += ",str,str";
                    cashtrans_searchdetailgrid.setColSorting(trans_searchdetailColSorting);
                    cashtrans_searchdetailgrid.setNumberFormat("0,000",10);
                    cashtrans_searchdetailgrid.setNumberFormat("0,000",11);
                    cashtrans_searchdetailgrid.setNumberFormat("0,000",12);
                    cashtrans_searchdetailgrid.enableColumnMove(true);
                    cashtrans_searchdetailgrid.setSkin("dhx_skyblue");
                    cashtrans_searchdetailgrid.enablePaging(true,Number($("#cashcashtrans_searchdetailForm input[name=page_size]").val()),10,"cashtrans_searchdetailPaging",true,"cashtrans_searchdetailrecinfoArea");
                    cashtrans_searchdetailgrid.setPagingSkin("bricks");
                    
                    cashtrans_searchdetailgrid.setColumnHidden(18,true);  

                    //페이징 처리
                    cashtrans_searchdetailgrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        /*
                        if(lInd !==0){
                            $("#cashcashtrans_searchdetailForm input[name=page_no]").val(ind);
                            cashtrans_searchdetailSearch();
                        }else{
                            return false;
                        }
                        */
                        if(lInd !==0){
                           $("#cashcashtrans_searchdetailForm input[name=page_no]").val(ind);      
                           cashtrans_searchdetailgrid.clearAll();
                             cashtrans_searchdetailSearch();
                        }else{
                            $("#cashcashtrans_searchdetailForm input[name=page_no]").val(1);
                            cashtrans_searchdetailgrid.clearAll();
                              cashtrans_searchdetailSearch();
                        }                               
                    });

                    cashtrans_searchdetailgrid.init();
                    cashtrans_searchdetailgrid.parse(${ls_cashtrans_searchDetailList}, "json");
                    
                    <c:if test="${trans_searchFormBean.ses_user_cate == '00' ||trans_searchFormBean.ses_user_cate == '01' || trans_searchFormBean.ses_user_cate == '02'}">
                    //cashtrans_searchdetailgrid.attachEvent("onRowDblClicked", Trans_search_MgrAction);
                    </c:if>      
                    //영수증보기
                    $("button[name=cashbill_view_bt]").unbind("click").bind("click", function (){

                        var tptrans_searcheq =  $(this).attr("tran_seq");
                        var tpresult_cd =  $(this).attr("tran_result_cd"); 
                        if(tpresult_cd == '0000')
                        {    
                            var strUrl = "../app/appCashResult?tran_seq="+tptrans_searcheq+"&tran_result_cd="+tpresult_cd+"&auctioncate=1";
                            var winx = window.open(strUrl,"결제내역", "width=400,height=700,scrollbars=yes,resizeable=no");
                            winx.focus();
                        }                        
                        
                        return false;

                    });                            
                        
                    //취소버튼
                    $("button[name=cncl_cash_view_bt]").unbind("click").bind("click", function (){

                        var tptrans_searcheq = $(this).attr("tran_seq");
                        var tporgcno =  $(this).attr("org_cno"); 
                        
                        if(confirm("승인거래를 취소하시겠습니까?"))
                        {
                            
                            $("#frm_cash_cncl input[id=tran_seq]").val(tptrans_searcheq);
                            $("#frm_cash_cncl input[id=org_cno]").val(tporgcno);
                            
                             $.ajax({
                                        url : "../app/mgrAction",
                                        type : "POST",
                                        async : true,
                                        dataType : "json",
                                        data: $("#frm_cash_cncl").serialize(),
                                        success : function(data) {
                                            if(data.result_cd == '0000')
                                            {
                                                alert(data.result_msg);
                                                cashtrans_searchdetailgrid.clearAll();
                                                cashtrans_searchdetailSearch();
                                            }
                                            else
                                            {
                                                alert("[" +data.result_cd+ "]" + data.result_msg);
                                            }

                                        },
                                        error : function() { 
                                            alert("취소 실패");
                                        }
                                    });	            
                        }
                        
                        return false;

                    });                       
                    
                    //상세코드검색
                    $("#cashcashtrans_searchdetailForm input[name=cashtrans_searchdetail_search]").click(function(){
                        /*
                        $("input[name=page_no]").val("1");

                        cashtrans_searchdetailgrid.clearAll();

                        cashtrans_searchdetailSearch();
                        */
                        cashtrans_searchdetailgrid.changePage(1);
                        return false;  
                    });
                    $("#cashcashtrans_searchdetailForm input[name=week]").click(function(){
                        cashtrans_search_date_search("week");
                    });

                    $("#cashcashtrans_searchdetailForm input[name=month]").click(function(){
                        cashtrans_search_date_search("month");
                    });
                    
                    //현금거래 정보 엑셀다운로드 이벤트
                    $("#cashcashtrans_searchdetailForm input[name=cashtrans_searchdetail_excel]").click(function () {

                        cashTrans_searchDetailExcel();

                    });            
                    $("#cashcashtrans_searchdetailForm input[name=init]").click(function () {
                    
                        trans_searchCashDetailMasterInit($("#cashcashtrans_searchdetailForm"));

                    }); 
                    
                });

        //코드 정보 조회
                function cashtrans_searchdetailSearch() {
                
                $.ajax({
                    url: "<c:url value="/trans_search/cashtrans_searchDetailList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#cashcashtrans_searchdetailForm").serialize(),
                    success: function (data) {

                        var jsonData = $.parseJSON(data);
                        //cashtrans_searchdetailgrid.clearAll();
                        cashtrans_searchdetailgrid.parse(jsonData,"json");
                        
                        
                            //영수증보기
                            $("button[name=cashbill_view_bt]").unbind("click").bind("click", function (){

                                var tptrans_searcheq =  $(this).attr("tran_seq");
                                var tpresult_cd =  $(this).attr("tran_result_cd"); 
                                if(tpresult_cd == '0000')
                                {    
                                    var strUrl = "../app/appCashResult?tran_seq="+tptrans_searcheq+"&tran_result_cd="+tpresult_cd+"&auctioncate=1";
                                    var winx = window.open(strUrl,"결제내역", "width=400,height=700,scrollbars=yes,resizeable=no");
                                    winx.focus();
                                }                        

                                return false;

                            });                            

                            //취소버튼
                            $("button[name=cncl_cash_view_bt]").unbind("click").bind("click", function (){

                                var tptrans_searcheq = $(this).attr("tran_seq");
                                var tporgcno =  $(this).attr("org_cno"); 

                                if(confirm("현금영수증거래를 취소하시겠습니까?"))
                                {

                                    $("#frm_cash_cncl input[id=tran_seq]").val(tptrans_searcheq);
                                    $("#frm_cash_cncl input[id=org_cno]").val(tporgcno);

                                     $.ajax({
                                                url : "../app/mgrAction",
                                                type : "POST",
                                                async : true,
                                                dataType : "json",
                                                data: $("#frm_cash_cncl").serialize(),
                                                success : function(data) {
                                                    if(data.result_cd == '0000')
                                                    {
                                                        alert(data.result_msg);
                                                        cashtrans_searchdetailgrid.clearAll();
                                                        cashtrans_searchdetailSearch();
                                                    }
                                                    else
                                                    {
                                                        alert("[" +data.result_cd+ "]" + data.result_msg);
                                                    }

                                                },
                                                error : function() { 
                                                    alert("취소 실패");
                                                }
                                            });	            
                                }

                                return false;

                            });                            
                        

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
                    return false;
            }

            var dhxSelPopupWins=new dhtmlXWindows();
                    
                //ONOFF가맹점번호 팝업 클릭 이벤트
                $("#cashcashtrans_searchdetailForm input[name=onffmerch_no]").unbind("click").bind("click", function (){

                    onoffCashMerchSelectPopup();

                });  
                
                //ONOFF가맹점번호 팝업 클릭 이벤트
                $("#cashcashtrans_searchdetailForm input[name=merch_nm]").unbind("click").bind("click", function (){

                    onoffCashMerchSelectPopup();

                });                             
                    
                function onoffCashMerchSelectPopup(){
                    onoffObject.onffmerch_no = $("#cashcashtrans_searchdetailForm input[name=onffmerch_no]");
                    onoffObject.merch_nm = $("#cashcashtrans_searchdetailForm input[name=merch_nm]");
                    //ONOFF가맹점정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("가맹점 선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                }
                               
                //ONOFF결제ID 팝업 클릭 이벤트
                $("#cashcashtrans_searchdetailForm input[name=onfftid]").unbind("click").bind("click", function (){

                    onoffCashTidSelectPopup();

                });        
                
                $("#cashcashtrans_searchdetailForm input[name=onfftid_nm]").unbind("click").bind("click", function (){

                    onoffCashTidSelectPopup();

                });                              
                
                function onoffCashTidSelectPopup(){                    
                    onoffObject.onfftid_nm = $("#cashcashtrans_searchdetailForm input[name=onfftid_nm]");
                    onoffObject.onfftid = $("#cashcashtrans_searchdetailForm input[name=onfftid]");
                    //ONOFF결제ID 팝업
                    w2 = dhxSelPopupWins.createWindow("OnofftidSelectPopUp", 20, 30, 640, 480);
                    w2.setText("ONOFF결제ID 선택페이지");
                    dhxSelPopupWins.window('OnofftidSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnofftidSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnofftidSelectPopUp"/>', true);   
                }                
              
//                function SelOnofftidInfoInput(p_onffmerch_no, p_onfftid, p_onfftid_nm){
//                    $("#cashcashtrans_searchdetailForm input[name=onfftid]").val(p_onfftid);                 
//                    $("#cashcashtrans_searchdetailForm input[name=onfftid_nm]").val(p_onfftid_nm);
//                }
                function cash_setSens(id, k) {
                    // update range
                    if (k == "min") {
                        cashtrans_searchdetailCalendar.setSensitiveRange(cash_byId(id).value, null);
                    } else {
                        cashtrans_searchdetailCalendar.setSensitiveRange(null, cash_byId(id).value);
                    }
                }

                function cash_byId(id) {
                    return document.getElementById(id);
                }
                function cashtrans_search_date_search(day){
                    var oneDate = 1000 * 3600 * 24; 
                    var now = new Date();
                    var week = new Date(now.getTime() + (oneDate*-6));
                    var month = new Date(now.getTime() + (oneDate*-31));
                    var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                    var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                    var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                    var date=new Date();

                    if(day=="today"){
                        $("#cashcashtrans_searchdetailForm input[id=cashtrans_searchForm_date]").val(nowTime);
                        $("#cashcashtrans_searchdetailForm input[id=cashtrans_searchto_date]").val(nowTime);
                    }else if(day=="week"){
                        $("#cashcashtrans_searchdetailForm input[id=cashtrans_searchForm_date]").val(weekTime);
                        $("#cashcashtrans_searchdetailForm input[id=cashtrans_searchto_date]").val(nowTime);
                    }else{
                        $("#cashcashtrans_searchdetailForm input[id=cashtrans_searchForm_date]").val(monthTime);
                        $("#cashcashtrans_searchdetailForm input[id=cashtrans_searchto_date]").val(nowTime);
                    }
                }
                

                <c:if test="${trans_searchFormBean.ses_user_cate == '00' ||trans_searchFormBean.ses_user_cate == '01' || trans_searchFormBean.ses_user_cate == '02'}">            
                        function Trans_search_MgrAction(rowId, col) 
                        {
                            var tptrans_searcheq = cashtrans_searchdetailgrid.getUserData(rowId, 'tran_seq');
                            var tpresult_cd =  cashtrans_searchdetailgrid.getUserData(rowId, 'result_cd');
                            if(tpresult_cd == '0000')
                            {    
                                var strUrl = "../app/appCashResult?tran_seq="+tptrans_searcheq+"&tran_result_cd="+tpresult_cd+"&auctioncate=1";
                                var winx = window.open(strUrl,"결제내역", "width=400,height=700,scrollbars=yes,resizeable=no");
                                winx.focus();
                            }
                        }
                </c:if>                 
                
            //현금거래 엑셀다운로드
            function cashTrans_searchDetailExcel() {

                $("#cashcashtrans_searchdetailForm").attr("action","<c:url value="/trans_search/cashTrans_searchDetailListExcel" />");
                document.getElementById("cashcashtrans_searchdetailForm").submit();

            }  
            function trans_searchCashDetailMasterInit($form) {

                searchFormInit($form);

                cashtrans_search_date_search("week");                    
            } 

        </script>

        <div class="right" id="cashtrans_searchdetail_search" style="width:100%;">
             <form id="cashcashtrans_searchdetailForm" method="POST" action="<c:url value="/trans_search/cashtrans_searchDetailList" />" modelAttribute="trans_searchFormBean">

                <input type="hidden" name="page_size" value="100" >
                <input type="hidden" name="page_no" value="1" >
                <input type="hidden" name="pay_chn_cate" value="${trans_searchFormBean.pay_chn_cate}" >
                
                <table>
                    <tr>
                        <div class="label">승인일</div>
                        <div class="input">
                        <input type="text" size="15" name="app_start_dt" id="cashtrans_searchForm_date" value ="${trans_searchFormBean.app_start_dt}" onclick="trans_searchdetail_setSens('cashtrans_searchto_date', 'max');" /> ~
                        <input type="text" size="15" name="app_end_dt" id="cashtrans_searchto_date" value="${trans_searchFormBean.app_end_dt}" onclick="trans_searchdetail_setSens('cashtrans_searchForm_date', 'min');" /> </div>
                        <div class ="input"><input type="button" name="week" value="1주"/></div>
                        <div class ="input"><input type="button" name="month" value="1달"/></div> 
                        <div class="label">거래상태</div>
                        <div class="input">
                            <select name="tran_status">
                                <option value="">전체</option>
                                <c:forEach var="code" items="${tranStatusList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == trans_searchFormBean.tran_status}">selected</c:if> >${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="label">거래결과</div>
                        <div class="input">
                            <select name="result_status">
                                <option value="">전체</option>
                                <c:forEach var="code" items="${resultStatusList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${code.detail_code == trans_searchFormBean.result_status}">selected</c:if> >${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </div>    
                        <div class="label">가맹점명</div>
                        <div class="input">
                            <input type="text" name="merch_nm" value ="${trans_searchFormBean.merch_nm}" readonly/>
                        </div>                            
                        <div class="label">가맹점ID</div>
                        <div class="input">
                            <input type="text" name="onffmerch_no" value ="${trans_searchFormBean.onffmerch_no}" readonly/>                            
                        </div>                                             
                        <div class="label">결제ID명</div>
                        <div class="input">
                            <input type="text" name="onfftid_nm" value ="${trans_searchFormBean.onfftid_nm}" readonly/>
                        </div>                            
                        <div class="label">결제ID</div>
                        <div class="input">
                            <input type="text" name="onfftid" value ="${trans_searchFormBean.onfftid}" readonly/>                            
                        </div>    
                        <div class="label">고객명</div>
                        <div class="input">
                            <input type="text" name="user_nm" value ="${trans_searchFormBean.user_nm}" />
                        </div>                               
                        <div class="label">고객전화번호</div>
                        <div class="input">
                            <input type="text" name="user_phone2" value ="${trans_searchFormBean.user_phone2}" />
                        </div>                                                             
                        <div class="label">상품명</div>
                        <div class="input">
                            <input type="text" name="product_nm" value ="${trans_searchFormBean.product_nm}" />
                        </div>                                                   
                        <div class="label">승인금액</div>
                        <div class="input">
                            <input type="text" name="tot_amt" value ="${trans_searchFormBean.tot_amt}" />
                        </div>                                               
                        <div class="label">인증번호</div>
                        <div class="input">
                            <input type="text" name="card_num" value ="${trans_searchFormBean.card_num}" />
                        </div>                                                     
                        <div class="label">승인번호</div>
                        <div class="input">
                            <input type="text" name="app_no" value ="${trans_searchFormBean.app_no}" />
                        </div>                              
                        <td>
                            <input type="button" name="cashtrans_searchdetail_search" value="조회"/>
                        </td>
                        <td>
                            <input type="button" name="cashtrans_searchdetail_excel" value="엑셀"/>
                        </td>
                        <td><input type="button" name="init" value="검색조건지우기"/></td>
                    </tr>
                </table>
                        
            </form>
            <form name="frm_cash_cncl" id="frm_cash_cncl" method="post" >
                <input type="hidden" name="org_cno" id="org_cno" value=""><input type="hidden" name="mgr_msg" id="mgr_msg" value="" ><input type="hidden" name="EP_tr_cd" id="EP_tr_cd" value="00201050"><input type="hidden" name="mgr_txtype" name="id_txtype" value="51"><input type="hidden" name="tran_seq" id="tran_seq" value="">
            </form>
            <div class="paging">
                <div id="cashtrans_searchdetailPaging" style="width: 50%;"></div>
                <div id="cashtrans_searchdetailrecinfoArea" style="width: 50%;"></div>
            </div>
                                
         </div>

    </body>
</html>
