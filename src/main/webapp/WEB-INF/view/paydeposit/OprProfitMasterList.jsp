<%-- 
    Document   : OprProfitMasterList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>영업이익 메인</title>             
        </head>
        <body>
</c:if>
       
    <body>
        <div class="right" id="OprProfitMaster_search" style="width:100%;">       
            <form id="OprProfitMasterForm" method="POST" onsubmit="return false" action="<c:url value="/paydeposit/OprProfitMasterList" />" modelAttribute="paydepositFormBean"> 
                <table>
                    <tr>
                        <td>
                            <div class="label">지급일자</div>
                            <div class="input">
                                <input type="text" name="onoffmerch_ext_pay_dt_start" id="onoffmerch_ext_pay_dt_start" value ="${paydepositFormBean.onoffmerch_ext_pay_dt_start}" onclick="" /> ~
                                <input type="text" name="onoffmerch_ext_pay_dt_end" id="onoffmerch_ext_pay_dt_end" value ="${paydepositFormBean.onoffmerch_ext_pay_dt_end}" onclick="" />
                            </div>                            
                            <div class="label">입금일자</div>
                            <div class="input">
                                <input type="text" name="deposit_start_dt" id="deposit_start_dt" value ="${paydepositFormBean.deposit_start_dt}" onclick="" /> ~
                                <input type="text" name="deposit_end_dt" id="deposit_end_dt" value ="${paydepositFormBean.deposit_end_dt}" onclick="" />
                            </div>            
                            <div class="label">지급ID</div>
                            <div class="input"><input type="text" name="onffmerch_no" value ="${paydepositFormBean.onffmerch_no}"/></div> 
                            <div class="label">지급ID명</div>
                            <div class="input"><input type="text" name="merch_nm" value ="${paydepositFormBean.merch_nm}"/></div> 
                        </td>
                        <td><input type="button" name="OprProfitMaster_search" value="조회"/></td>
                        <td><input type="button" name="OprProfitMaster_excel" value="엑셀"/></td>
                        <td><input type="button" name="init" value="검색조건삭제"/></td>
                    </tr>
               </table>
            </form>
        </div>
        <div id="OprProfitMaster" style="position: relative;width:100%;height:100%;background-color:white;"></div>
        
         <script type="text/javascript">
            
            var OprProfitMastergrid={};
            var OprProfitMasterCalendar;
            var outamtWindows={};
            var depositMetabolismWindow;
            
            $(document).ready(function () {
                
                var OprProfitMaster_searchForm = document.getElementById("OprProfitMaster_search");
                
                var tabBarId = tabbar.getActiveTab();
                OprProfitMaster_layout = tabbar.cells(tabBarId).attachLayout("2E");
                     
                OprProfitMaster_layout.cells('a').hideHeader();
                OprProfitMaster_layout.cells('b').hideHeader();
                 
                OprProfitMaster_layout.cells('a').attachObject(OprProfitMaster_searchForm);
                OprProfitMaster_layout.cells('a').setHeight(20);
                
                OprProfitMastergrid = OprProfitMaster_layout.cells('b').attachGrid();
                
                OprProfitMastergrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                
                var OprProfitMasterHeaders = "";
                OprProfitMasterHeaders += "<center>지급일</center>,<center>입금일</center>,<center>지급ID</center>,<center>지급ID명</center>";
                OprProfitMasterHeaders += ",<center>거래 대상액</center>,<center>입금예정액</center>,<center>카드수수료</center>,<center>가맹점지급액</center>,<center>가맹점수수료</center>,<center>VAT</center>,<center>대리점수수료</center>,<center>영업이익금</center>";
                
                OprProfitMastergrid.setHeader(OprProfitMasterHeaders);
                
                var OprProfitColAligns = "";
                OprProfitColAligns += "center,center,center,center";
                OprProfitColAligns += ",right,right,right,right,right,right,right,right";
                
                OprProfitMastergrid.setColAlign(OprProfitColAligns);
                
                var OprProfitColTypes = "";
                OprProfitColTypes += "txt,txt,txt,txt";
                OprProfitColTypes += ",edn,edn,edn,edn,edn,edn,edn,edn";
                
                OprProfitMastergrid.setColTypes(OprProfitColTypes);
                
                var OprProfitInitWidths = "";
                OprProfitInitWidths += "80,80,120,120";
                OprProfitInitWidths += ",100,100,100,100,100,100,100,100";
                
                OprProfitMastergrid.setInitWidths(OprProfitInitWidths);
                
                
                var OprProfitColSorting = "";
                OprProfitColSorting += "str,str,str,str";
                OprProfitColSorting += ",int,int,int,int,int,int,int,int";
                
                
                OprProfitMastergrid.setColSorting(OprProfitColSorting);
                OprProfitMastergrid.setNumberFormat("0,000",4);
                OprProfitMastergrid.setNumberFormat("0,000",5);
                OprProfitMastergrid.setNumberFormat("0,000",6);
                OprProfitMastergrid.setNumberFormat("0,000",7);
                OprProfitMastergrid.setNumberFormat("0,000",8);
                OprProfitMastergrid.setNumberFormat("0,000",9);
                OprProfitMastergrid.setNumberFormat("0,000",10);
                OprProfitMastergrid.setNumberFormat("0,000",11);
                OprProfitMastergrid.enableColumnMove(true);
                OprProfitMastergrid.setSkin("dhx_skyblue");
                
                var OprProfitMasterfooters = "총계,#cspan,#cspan,#cspan";
                OprProfitMasterfooters += ",<div id='OprProfitMaster4'>0</div>";
                OprProfitMasterfooters += ",<div id='OprProfitMaster5'>0</div>,<div id='OprProfitMaster6'>0</div>";
                OprProfitMasterfooters += ",<div id='OprProfitMaster7'>0</div>,<div id='OprProfitMaster8'>0</div>";
                OprProfitMasterfooters += ",<div id='OprProfitMaster9'>0</div>,<div id='OprProfitMaster10'>0</div>";
                OprProfitMasterfooters += ",<div id='OprProfitMaster11'>0</div>";
                
                OprProfitMasterfooters += ", , ";
                //OprProfitMasterfooters += ",,#cspan";
                
                
                OprProfitMastergrid.attachFooter(OprProfitMasterfooters);                    
                
                
                OprProfitMastergrid.init();
                //OprProfitMastergrid.attachEvent("onRowDblClicked", OprProfitMaster_attach);
                //OprProfitMastergrid.parse(${ls_OprProfitMasterList},"json");
                
                OprProfitMastergrid.parse(${ls_OprProfitMasterList}, OprProfitMasterFooterValues,"json");
                
                depositMetabolismWindow = new dhtmlXWindows();
                
                //검색조건 초기화
                $("#OprProfitMasterForm input[name=init]").click(function () {

                    OprProfitMasterInit($("#OprProfitMasterForm"));

                });  
                
                $("#OprProfitMasterForm input[name=week]").click(function(){
                    midDeposit_date_search("week");
                });

                $("#OprProfitMasterForm input[name=month]").click(function(){
                    midDeposit_date_search("month");
                });
                 dhxSelPopupWins=new dhtmlXWindows();
                
                //ONOFF지급ID명 팝업 클릭 이벤트
                $("#OprProfitMasterForm input[name=merch_nm]").unbind("click").bind("click", function (){

                    onoffMerchSalesSelectPopup();

                });  

		 function onoffMerchSalesSelectPopup(){
                    onoffObject.onffmerch_no = $("#OprProfitMasterForm input[name=onffmerch_no]");
                    onoffObject.merch_nm = $("#OprProfitMasterForm input[name=merch_nm]");
                    //ONOFF지급ID정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("ONOFF지급ID 선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                }   
                



                //검색 이벤트
                $("#OprProfitMasterForm input[name=OprProfitMaster_search]").click(function () {
                    OprProfitMasterSearch();
                });
                
                //엑셀
                $("#OprProfitMasterForm input[name=OprProfitMaster_excel]").click(function () {
                    $("#OprProfitMasterForm").attr("action","<c:url value="/paydeposit/OprProfitMasterListExcel" />");
                    document.getElementById("OprProfitMasterForm").submit();
                });                
                
                OprProfitMasterCalendar = new dhtmlXCalendarObject(["onoffmerch_ext_pay_dt_start","onoffmerch_ext_pay_dt_end","deposit_start_dt","deposit_end_dt"]);
                
                    
            });
            
            function OprProfitMasterFooterValues() {
                //OprProfitMaster19
                var OprProfitMaster4 = document.getElementById("OprProfitMaster4");
                var OprProfitMaster5 = document.getElementById("OprProfitMaster5");
                var OprProfitMaster6 = document.getElementById("OprProfitMaster6");
                var OprProfitMaster7 = document.getElementById("OprProfitMaster7");
                var OprProfitMaster8 = document.getElementById("OprProfitMaster8");
                var OprProfitMaster9 = document.getElementById("OprProfitMaster9");
                var OprProfitMaster10 = document.getElementById("OprProfitMaster10");
                var OprProfitMaster11 = document.getElementById("OprProfitMaster11");
     
                OprProfitMaster4.innerHTML =  putComma(sumColumn(4)) ;  
                OprProfitMaster5.innerHTML =  putComma(sumColumn(5)) ;    
                OprProfitMaster6.innerHTML =  putComma(sumColumn(6)) ;    
                OprProfitMaster7.innerHTML =  putComma(sumColumn(7)) ;    
                OprProfitMaster8.innerHTML =  putComma(sumColumn(8)) ;    
                OprProfitMaster9.innerHTML =  putComma(sumColumn(9)) ;    
                OprProfitMaster10.innerHTML =  putComma(sumColumn(10)) ;    
                OprProfitMaster11.innerHTML =  putComma(sumColumn(11)) ;    
                
                return true;

            }            
            
            		
            function sumColumn(ind) {
                var out = 0;
                for (var i = 0; i < OprProfitMastergrid.getRowsNum(); i++) {
                     out += parseFloat(OprProfitMastergrid.cells2(i, ind).getValue());
                }

                return out;
            }
            
            function OprProfitMasterInit($form) {
                searchFormInit($form);
                midDeposit_date_search("today");                    
            } 
            
            //검색
            function OprProfitMasterSearch() {
                
                $.ajax({
                    url: "<c:url value="/paydeposit/OprProfitMasterList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#OprProfitMasterForm").serialize(),
                    success: function (data) {
                        
                        var jsonData = $.parseJSON(data);
                        OprProfitMastergrid.clearAll();
                        //OprProfitMastergrid.parse(jsonData,"json");
                       
                       OprProfitMastergrid.parse($.parseJSON(data),OprProfitMasterFooterValues, "json");

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }

            function byId(id) {
                return document.getElementById(id);
            }              
            
            function getRowId($tag){
                var rowId = 0;

                if(OprProfitMastergrid.pagingOn){
                    rowId =Number((OprProfitMastergrid.currentPage -1) * Number(OprProfitMastergrid.rowsBufferOutSize))+Number($tag.parent().parent().index()-1);
                }else{
                    rowId = Number($tag.parent().parent().index()-1);
                }
                return rowId;
            }            
                
        </script>
    </body>
</html>
