<%-- 
    Document   : codeDetailMaster
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>입금정산 상세조회</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">

                var paydepositdetailgrid={};
                var paydepositdetail_layout={};
                
                var paydepositDetailCalendar = new dhtmlXCalendarObject(["paydepositDetail_from_date","paydepositDetail_to_date"]);    

                $(document).ready(function () {

                    var paydepositdetailForm = document.getElementById("paydepositdetail_search");

                    paydepositdetail_layout = main_paydepositMaster_layout.cells('b').attachLayout("2E");

                    paydepositdetail_layout.cells('a').hideHeader();

                    paydepositdetail_layout.cells('b').hideHeader();

                    paydepositdetail_layout.cells('a').attachObject(paydepositdetailForm);

                    paydepositdetail_layout.cells('a').setHeight(60);

                    paydepositdetailgrid = paydepositdetail_layout.cells('b').attachGrid();

                    paydepositdetailgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                

                    var paydepositdetailheaders = "";

                    paydepositdetailheaders += "<center>NO</center>,<center>결제채널</center>,<center>가맹점번호</center>,<center>거래종류</center>,<center>매입사</center>";
                    paydepositdetailheaders += ",<center>승인일</center>,<center>승인번호</center>,<center>승인금액</center>,<center>부가세</center>,<center>할부</center>";
                    paydepositdetailheaders += ",<center>매입일</center>,<center>입금예정일</center>,<center>수수료</center>,<center>입금예정금액</center>";
                    
                    paydepositdetailgrid.setHeader(paydepositdetailheaders);
                    paydepositdetailgrid.setColAlign("center,center,center,center,center,center,center,right,right,center,center,center,right,right");
                    paydepositdetailgrid.setColTypes("txt,txt,txt,txt,txt,txt,txt,edn,edn,txt,txt,txt,edn,edn");
                    paydepositdetailgrid.setInitWidths("50,150,100,100,100,100,100,100,120,100,100,100,100,100");
                    paydepositdetailgrid.setColSorting("str,str,str,str,str,str,str,int,int,str,str,str,int,int");
                    paydepositdetailgrid.setNumberFormat("0,000",7);            
                    paydepositdetailgrid.setNumberFormat("0,000",8);
                    paydepositdetailgrid.setNumberFormat("0,000",12);
                    paydepositdetailgrid.setNumberFormat("0,000",13);
                    paydepositdetailgrid.enableColumnMove(true);
                    paydepositdetailgrid.setSkin("dhx_skyblue");
                    paydepositdetailgrid.enablePaging(true,Number($("#paydepositdetailForm input[name=page_size]").val()),10,"paydepositdetailPaging",true,"paydepositdetailrecinfoArea");
                    paydepositdetailgrid.setPagingSkin("bricks");                    
//                    var paydepositdetailgridFilters = ",#select_filter,#select_filter,#text_filter,#select_filter,#text_filter,#text_filter,#numeric_filter,#numeric_filter,#select_filter,#text_filter,#text_filter,#numeric_filter,#numeric_filter";
//                    paydepositdetailgrid.attachHeader(paydepositdetailgridFilters);                    
                    paydepositdetailgrid.init();
                    paydepositdetailgrid.parse(${ls_paydepositDetailList},"json");
                    
                    //검색조건 초기화
                    $("#paydepositdetailForm input[name=init]").click(function () {
                        paydepositdetailInit($("#paydepositdetailForm"));
                    });  
                    
                    $("#paydepositdetailForm input[name=week]").click(function(){
                        paydepositdetail_date_search("week");
                    });

                    $("#paydepositdetailForm input[name=month]").click(function(){
                        paydepositdetail_date_search("month");
                    });

                    //페이징 처리
                    paydepositdetailgrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        if(lInd !==0){
                          $("#paydepositdetailForm input[name=page_no]").val(ind);             
                           paydepositdetailgrid.clearAll();
                           paydepositdetailSearch();
                        }else{
                            $("#paydepositdetailForm input[name=page_no]").val(1);
                            paydepositdetailgrid.clearAll();
                             paydepositdetailSearch();
                        }                            
                    });

                    //상세코드검색
                    $("#paydepositdetailForm input[name=paydepositdetail_search]").click(function(){
                                /*
                        $("input[name=page_no]").val("1");

                        paydepositdetailgrid.clearAll();

                        paydepositdetailSearch();
                        */
                        paydepositdetailgrid.changePage(1);

                    });
                    
                    //엑셀
                    $("#paydepositdetailForm input[name=paydepositdetail_excel]").click(function () {
                        $("#paydepositdetailForm").attr("action","<c:url value="/paydeposit/paydepositDetailListExcel" />");
                        document.getElementById("paydepositdetailForm").submit();
                    });                                    
                    
                });
                
                function paydepositdetailInit($form) {
                    searchFormInit($form);
                    paydepositdetail_date_search("week");                    
                } 
                
                function paydepositdetail_date_search(day){
                var oneDate = 1000 * 3600 * 24; 
                var now = new Date();
                var week = new Date(now.getTime() + (oneDate*-6));
                var month = new Date(now.getTime() + (oneDate*-31));
                var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                var date=new Date();
                if(day=="today"){
                    this.$("input[id=paydepositDetail_from_date]").val(nowTime);
                    this.$("input[id=paydepositDetail_to_date]").val(nowTime);
                }else if(day=="week"){
                    this.$("input[id=paydepositDetail_from_date]").val(weekTime);
                    this.$("input[id=paydepositDetail_to_date]").val(nowTime);
                }else{
                    this.$("input[id=paydepositDetail_from_date]").val(monthTime);
                    this.$("input[id=paydepositDetail_to_date]").val(nowTime);
                }
            }


                //코드 정보 조회
                function paydepositdetailSearch() {
                //alert('search !!!');
                $.ajax({
                    url: "<c:url value="/paydeposit/paydepositDetailList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#paydepositdetailForm").serialize(),
                    success: function (data) {

                        var jsonData = $.parseJSON(data);
                        //paydepositdetailgrid.clearAll();
                        paydepositdetailgrid.parse(jsonData,"json");

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
                    return false;
            }
            
            function setSens(id, k) {
                // update range
                if (k == "min") {
                    paydepositDetailCalendar.setSensitiveRange(byId(id).value, null);
                } else {
                    paydepositDetailCalendar.setSensitiveRange(null, byId(id).value);
                }
            }

            function byId(id) {
                return document.getElementById(id);
            }                      
            
    </script>
    <body>


        <div class="right" id="paydepositdetail_search" style="width:100%;">
             <form id="paydepositdetailForm" method="POST" action="<c:url value="/paydeposit/paydepositDetailList" />" modelAttribute="paydepositFormBean">

                        <input type="hidden" name="page_size" value="100" >
                        <input type="hidden" name="page_no" value="1" >
                        
                        <table>
                            <tr>
                                <td>
                                    <div class="label">승인일</div>
                                    <div class="input">
                                        <input type="text" size='15' name="sal_start_dt" id="paydepositDetail_from_date" value ="" onclick="setSens('paydepositDetail_to_date', 'max');" /> ~
                                        <input type="text" size='15' name="sal_end_dt" id="paydepositDetail_to_date" onclick="setSens('paydepositDetail_from_date', 'min');" />
                                    </div>
                                    <div class ="input"><input type="button" name="week" value="1주"/></div>
                                    <div class ="input"><input type="button" name="month" value="1달"/></div>
                                    <div class="label">입금일</div>
                                    <div class="input">
                                        <input type="text" name="deposit_dt" id="paydepositDetail_from_date" value ="${paydepositFormBean.deposit_dt}" onclick="setSens('paydepositDetail_to_date', 'max');" />
                                    </div>                                    
                                    <div class="label">결제채널</div>
                                    <div class="input">
                                        <select name="pay_type">
                                            <option value="">전체</option>
                                            <c:forEach var="code" items="${payChnCateList.tb_code_details}">
                                                <option value="${code.detail_code}">${code.code_nm}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                <div class="label">가맹점번호</div>
                                <div class="input"><input type = "text" size='10' name="merch_no" value ="${paydepositFormBean.merch_no}" /></div>                                
                                <div class="label">승인번호</div>
                                <div class="input"><input type = "text" size='10' name="aprv_no" /></div>
                                <div class="label">승인금액</div>
                                <div class="input"><input type = "text" size='10' name="tot_trx_amt" /></div>
                                <div class="label">매입사</div>
                                <div class="input">
                                    <select name="acq_cd">
                                        <option value="">전체</option>
                                        <c:forEach var="code" items="${acqCdList.tb_code_details}">
                                            <option value="${code.detail_code}">${code.code_nm}</option>
                                        </c:forEach>
                                    </select>
                                </div>                      
                            </td>                                   
                                <td >
                                    <input type="button" name="paydepositdetail_search" value="조회"/>
                                </td>
                                <td >
                                    <input type="button" name="paydepositdetail_excel" value="엑셀"/>
                                </td>
                                 <td><input type="button" name="init" value="검색조건삭제"/></td>
                            </tr>
                        </table>
             </form>

            <div class="paging">
                <div id="paydepositdetailPaging" style="width: 50%;"></div>
                <div id="paydepositdetailrecinfoArea" style="width: 50%;"></div>
            </div>

        </div>
        
    </body>
</html>
