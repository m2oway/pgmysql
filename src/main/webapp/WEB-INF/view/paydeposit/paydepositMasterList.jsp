<%-- 
    Document   : paydepositMasterList
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>지급대사 메인</title>             
        </head>
        <body>
</c:if>
       
    <body>
        <div class="right" id="paydepositMaster_search" style="width:100%;">       
            <form id="paydepositMasterForm" method="POST" onsubmit="return false" action="<c:url value="/paydeposit/paydepositMasterList" />" modelAttribute="paydepositFormBean"> 
                <table>
                    <tr>
                        <td>
                            <div class="label">승인일자</div>
                            <div class="input">
                                <input type="text" name="app_start_dt" id="paydepositappMaster_from_date" value ="${paydepositFormBean.app_start_dt}" onclick="" /> ~
                                <input type="text" name="app_end_dt" id="paydepositappMaster_to_date" value ="${paydepositFormBean.app_end_dt}" onclick="" />
                            </div>                            
                            <div class="label">정산일자</div>
                            <div class="input">
                                <input type="text" name="deposit_start_dt" id="paydepositMaster_from_date" value ="${paydepositFormBean.deposit_start_dt}" onclick="" /> ~
                                <input type="text" name="deposit_end_dt" id="paydepositMaster_to_date" value ="${paydepositFormBean.deposit_end_dt}" onclick="" />
                            </div>                   
                            <div class="label">지급ID</div>
                            <div class="input"><input type="text" name="merch_no" /></div> 
                            <div class="label">지급ID명</div>
                            <div class="input"><input type="text" name="merch_nm" /></div> 
                        </td>
                        <td><input type="button" name="paydepositMaster_search" value="조회"/></td>
                        <td><input type="button" name="paydepositMaster_excel" value="엑셀"/></td>
                        <td><input type="button" name="init" value="검색조건삭제"/></td>
                    </tr>
               </table>
            </form>
        </div>
        <div id="paydepositMaster" style="position: relative;width:100%;height:100%;background-color:white;"></div>
        
         <script type="text/javascript">
            
            var paydepositMastergrid={};
            var paydepositMasterCalendar;
            var outamtWindows={};
            var depositMetabolismWindow;
            
            $(document).ready(function () {
                
                var paydepositMaster_searchForm = document.getElementById("paydepositMaster_search");
                
                var tabBarId = tabbar.getActiveTab();
                paydepositMaster_layout = tabbar.cells(tabBarId).attachLayout("2E");
                     
                paydepositMaster_layout.cells('a').hideHeader();
                paydepositMaster_layout.cells('b').hideHeader();
                 
                paydepositMaster_layout.cells('a').attachObject(paydepositMaster_searchForm);
                paydepositMaster_layout.cells('a').setHeight(20);
                
                paydepositMastergrid = paydepositMaster_layout.cells('b').attachGrid();
                
                paydepositMastergrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                
                var paydepositMasterHeaders = "";
                paydepositMasterHeaders += "<center>정산일</center>,<center>지급ID</center>,<center>지급ID명</center>";
                paydepositMasterHeaders += ",<center>대상액 차</center>,<center>지급예정액 차</center>,<center>수수료 차</center>,<center>부가세 차</center>,<center>원천징수 차</center>,<center>승인액 차</center>,<center>취소액 차</center>";
                paydepositMasterHeaders += ",<center>거래 대상액</center>,<center>거래 지급예정액</center>,<center>거래 수수료</center>,<center>거래 부가세</center>,<center>원천징수</center>,<center>거래 승인액</center>,<center>거래 취소액</center>";
                paydepositMasterHeaders += ",<center>정산 대상액</center>,<center>정산 지급예정액</center>,<center>정산 수수료</center>,<center>정산 부가세</center>,<center>정산 원천징수</center>,<center>정산 승인액</center>,<center>정산 취소액</center>,<center>승인최소일</center>,<center>승인최대일</center>";
                
                paydepositMastergrid.setHeader(paydepositMasterHeaders);
                
                var paydepositColAligns = "";
                paydepositColAligns += "center,center,center,right,right";
                paydepositColAligns += ",right,right,right,right,right,right,right";
                paydepositColAligns += ",right,right,right,right,right,right,right";
                paydepositColAligns += ",right,right,right,right,right,center,center";
                
                paydepositMastergrid.setColAlign(paydepositColAligns);
                
                var paydepositColTypes = "";
                paydepositColTypes += "txt,txt,txt,edn,edn";
                paydepositColTypes += ",edn,edn,edn,edn,edn,edn,edn";
                paydepositColTypes += ",edn,edn,edn,edn,edn,edn,edn,edn";
                paydepositColTypes += ",edn,edn,edn,edn,txt,txt";
                
                paydepositMastergrid.setColTypes(paydepositColTypes);
                
                var paydepositInitWidths = "";
                paydepositInitWidths += "80,120,150,100,100";
                paydepositInitWidths += ",100,100,100,100,100,100,100";
                paydepositInitWidths += ",100,100,100,100,100,100,100,100";
                paydepositInitWidths += ",100,100,100,100,100,100";
                
                paydepositMastergrid.setInitWidths(paydepositInitWidths);
                
                
                var paydepositColSorting = "";
                paydepositColSorting += "str,str,str,int,int";
                paydepositColSorting += ",int,int,int,int,int,int,int";
                paydepositColSorting += ",int,int,int,int,int,int,int,int";
                paydepositColSorting += ",int,int,int,int,str,str";
                
                paydepositMastergrid.setColSorting(paydepositColSorting);
                
                paydepositMastergrid.setNumberFormat("0,000",3);
                paydepositMastergrid.setNumberFormat("0,000",4);
                paydepositMastergrid.setNumberFormat("0,000",5);
                paydepositMastergrid.setNumberFormat("0,000",6);
                paydepositMastergrid.setNumberFormat("0,000",7);
                paydepositMastergrid.setNumberFormat("0,000",8);
                paydepositMastergrid.setNumberFormat("0,000",9);
                paydepositMastergrid.setNumberFormat("0,000",10);
                paydepositMastergrid.setNumberFormat("0,000",11);
                paydepositMastergrid.setNumberFormat("0,000",12);
                paydepositMastergrid.setNumberFormat("0,000",13);
                paydepositMastergrid.setNumberFormat("0,000",14);
                paydepositMastergrid.setNumberFormat("0,000",15);
                paydepositMastergrid.setNumberFormat("0,000",16);
                paydepositMastergrid.setNumberFormat("0,000",17);
                paydepositMastergrid.setNumberFormat("0,000",18);
                paydepositMastergrid.setNumberFormat("0,000",19);
                paydepositMastergrid.setNumberFormat("0,000",20);
                paydepositMastergrid.setNumberFormat("0,000",21);
                paydepositMastergrid.setNumberFormat("0,000",22);
                paydepositMastergrid.setNumberFormat("0,000",23);
                paydepositMastergrid.enableColumnMove(true);
                paydepositMastergrid.setSkin("dhx_skyblue");
                
                var paydepositMasterfooters = "총계,#cspan,#cspan";
                paydepositMasterfooters += ",<div id='paydepositMaster3'>0</div>,<div id='paydepositMaster4'>0</div>";
                paydepositMasterfooters += ",<div id='paydepositMaster5'>0</div>,<div id='paydepositMaster6'>0</div>";
                paydepositMasterfooters += ",<div id='paydepositMaster7'>0</div>,<div id='paydepositMaster8'>0</div>";
                paydepositMasterfooters += ",<div id='paydepositMaster9'>0</div>,<div id='paydepositMaster10'>0</div>";
                paydepositMasterfooters += ",<div id='paydepositMaster11'>0</div>,<div id='paydepositMaster12'>0</div>";
                paydepositMasterfooters += ",<div id='paydepositMaster13'>0</div>,<div id='paydepositMaster14'>0</div>";
                paydepositMasterfooters += ",<div id='paydepositMaster15'>0</div>,<div id='paydepositMaster16'>0</div>";
                paydepositMasterfooters += ",<div id='paydepositMaster17'>0</div>,<div id='paydepositMaster18'>0</div>";
                paydepositMasterfooters += ",<div id='paydepositMaster19'>0</div>,<div id='paydepositMaster20'>0</div>";
                paydepositMasterfooters += ",<div id='paydepositMaster21'>0</div>,<div id='paydepositMaster22'>0</div>,<div id='paydepositMaster23'>0</div>";
                paydepositMasterfooters += ", , ";
                //paydepositMasterfooters += ",,#cspan";
                
                
                paydepositMastergrid.attachFooter(paydepositMasterfooters);                    
                
                
                paydepositMastergrid.init();
                paydepositMastergrid.attachEvent("onRowDblClicked", paydepositMaster_attach);
                //paydepositMastergrid.parse(${ls_paydepositMasterList},"json");
                
                paydepositMastergrid.parse(${ls_paydepositMasterList}, paydepositMasterFooterValues,"json");
                
                depositMetabolismWindow = new dhtmlXWindows();
                
                //검색조건 초기화
                $("#paydepositMasterForm input[name=init]").click(function () {

                    paydepositMasterInit($("#paydepositMasterForm"));

                });  
                
                $("#paydepositMasterForm input[name=week]").click(function(){
                    midDeposit_date_search("week");
                });

                $("#paydepositMasterForm input[name=month]").click(function(){
                    midDeposit_date_search("month");
                });


                //검색 이벤트
                $("#paydepositMasterForm input[name=paydepositMaster_search]").click(function () {
                    paydepositMasterSearch();
                });
                
                //엑셀
                $("#paydepositMasterForm input[name=paydepositMaster_excel]").click(function () {
                    $("#paydepositMasterForm").attr("action","<c:url value="/paydeposit/paydepositMasterListExcel" />");
                    document.getElementById("paydepositMasterForm").submit();
                });                
                
                paydepositMasterCalendar = new dhtmlXCalendarObject(["paydepositMaster_from_date","paydepositMaster_to_date","paydepositappMaster_from_date","paydepositappMaster_to_date"]);
                
                    
            });
            
            function paydepositMasterFooterValues() {
                //paydepositMaster19
                var paydepositMaster3 = document.getElementById("paydepositMaster3");
                var paydepositMaster4 = document.getElementById("paydepositMaster4");
                var paydepositMaster5 = document.getElementById("paydepositMaster5");
                var paydepositMaster6 = document.getElementById("paydepositMaster6");
                var paydepositMaster7 = document.getElementById("paydepositMaster7");
                var paydepositMaster8 = document.getElementById("paydepositMaster8");
                var paydepositMaster9 = document.getElementById("paydepositMaster9");
                var paydepositMaster10 = document.getElementById("paydepositMaster10");
                var paydepositMaster11 = document.getElementById("paydepositMaster11");
                var paydepositMaster12 = document.getElementById("paydepositMaster12");
                var paydepositMaster13 = document.getElementById("paydepositMaster13");
                var paydepositMaster14 = document.getElementById("paydepositMaster14");
                var paydepositMaster15 = document.getElementById("paydepositMaster15");
                var paydepositMaster16 = document.getElementById("paydepositMaster16");
                var paydepositMaster17 = document.getElementById("paydepositMaster17");
                var paydepositMaster18 = document.getElementById("paydepositMaster18");
                var paydepositMaster19 = document.getElementById("paydepositMaster19");
                var paydepositMaster20 = document.getElementById("paydepositMaster20");
                var paydepositMaster21 = document.getElementById("paydepositMaster21");
                var paydepositMaster22 = document.getElementById("paydepositMaster22");
                var paydepositMaster23 = document.getElementById("paydepositMaster23");
                
     
                paydepositMaster3.innerHTML =  putComma(sumColumn(3)) ;    
                paydepositMaster4.innerHTML =  putComma(sumColumn(4)) ;    
                paydepositMaster5.innerHTML =  putComma(sumColumn(5)) ;    
                paydepositMaster6.innerHTML =  putComma(sumColumn(6)) ;    
                paydepositMaster7.innerHTML =  putComma(sumColumn(7)) ;    
                paydepositMaster8.innerHTML =  putComma(sumColumn(8)) ;    
                paydepositMaster9.innerHTML =  putComma(sumColumn(9)) ;    
                paydepositMaster10.innerHTML =  putComma(sumColumn(10)) ;    
                paydepositMaster11.innerHTML =  putComma(sumColumn(11)) ;    
                paydepositMaster12.innerHTML =  putComma(sumColumn(12)) ;    
                paydepositMaster13.innerHTML =  putComma(sumColumn(13)) ;    
                paydepositMaster14.innerHTML =  putComma(sumColumn(14)) ;    
                paydepositMaster15.innerHTML =  putComma(sumColumn(15)) ;    
                paydepositMaster16.innerHTML =  putComma(sumColumn(16)) ;    
                paydepositMaster17.innerHTML =  putComma(sumColumn(17)) ;    
                paydepositMaster18.innerHTML =  putComma(sumColumn(18)) ;    
                paydepositMaster19.innerHTML =  putComma(sumColumn(19)) ;    
                paydepositMaster20.innerHTML =  putComma(sumColumn(20)) ;    
                paydepositMaster21.innerHTML =  putComma(sumColumn(21)) ;    
                paydepositMaster22.innerHTML =  putComma(sumColumn(22)) ;   
                paydepositMaster23.innerHTML =  putComma(sumColumn(23)) ;    
                
     
                return true;

            }            
            
            		
            function sumColumn(ind) {
                var out = 0;
                for (var i = 0; i < paydepositMastergrid.getRowsNum(); i++) {
                     out += parseFloat(paydepositMastergrid.cells2(i, ind).getValue());
                }

                return out;
            }
            
            function paydepositMasterInit($form) {
                searchFormInit($form);
                midDeposit_date_search("today");                    
            } 
            
            //상세조회 이벤트
            function paydepositMaster_attach(rowid, col) {

              var deposit_dt = paydepositMastergrid.getUserData(rowid, 'deposit_dt');
              var merch_no = paydepositMastergrid.getUserData(rowid, 'merch_no');
              
              main_paydepositMaster_layout.cells('b').attachURL("<c:url value="/paydeposit/paydepositDetailList" />"+"?deposit_dt=" + deposit_dt + "&merch_no="+merch_no+"&page_size=100", true);
              
            }
            
            //검색
            function paydepositMasterSearch() {
                
                $.ajax({
                    url: "<c:url value="/paydeposit/paydepositMasterList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#paydepositMasterForm").serialize(),
                    success: function (data) {
                        
                        var jsonData = $.parseJSON(data);
                        paydepositMastergrid.clearAll();
                        //paydepositMastergrid.parse(jsonData,"json");
                       
                       paydepositMastergrid.parse($.parseJSON(data),paydepositMasterFooterValues, "json");

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }
            
            function midDeposit_date_search(day){
                var oneDate = 1000 * 3600 * 24; 
                var now = new Date();
                var week = new Date(now.getTime() + (oneDate*-6));
                var month = new Date(now.getTime() + (oneDate*-31));
                var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                var yesterTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ ((now.getDate()-1)<10 ? '0'+(now.getDate()-1) : (now.getDate()-1));
                var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                var date=new Date();
                if(day=="today"){
                    this.$("input[id=paydepositMaster_from_date]").val(nowTime);
                    this.$("input[id=paydepositMaster_to_date]").val(nowTime);
                    this.$("input[id=paydepositappMaster_from_date]").val(yesterTime);
                    this.$("input[id=paydepositappMaster_to_date]").val(yesterTime);
                }else if(day=="week"){
                    this.$("input[id=paydepositMaster_from_date]").val(weekTime);
                    this.$("input[id=paydepositMaster_to_date]").val(nowTime);
                }else{
                    this.$("input[id=paydepositMaster_from_date]").val(monthTime);
                    this.$("input[id=paydepositMaster_to_date]").val(nowTime);
                }
            }
            
            function midDepositListsetSens(id, k) {
                // update range
                if (k == "min") {
                    paydepositMasterCalendar.setSensitiveRange(byId(id).value, null);
                } else {
                    paydepositMasterCalendar.setSensitiveRange(null, byId(id).value);
                }
            }

            function byId(id) {
                return document.getElementById(id);
            }              
            
            function getRowId($tag){
                var rowId = 0;

                if(paydepositMastergrid.pagingOn){
                    rowId =Number((paydepositMastergrid.currentPage -1) * Number(paydepositMastergrid.rowsBufferOutSize))+Number($tag.parent().parent().index()-1);
                }else{
                    rowId = Number($tag.parent().parent().index()-1);
                }
                return rowId;
            }            
                
        </script>
    </body>
</html>
