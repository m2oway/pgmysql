<%-- 
    Document   : dframtDetailList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>보류 정보 관리</title>
        </head>
        <body>
</c:if>

            <div class="searchForm1row" id="dframtDetailSearch" style="display:none;width:100%;height:100%;background-color:white;">
                <form id="dframtDetailForm" method="POST" action="<c:url value="/dframt/dframtDetailList" />" modelAttribute="dfrAmtFormBean">
                    <input type="hidden" name="page_size" value="100" >
                    <input type="hidden" name="page_no" value="1" >                    
                    
                    <div class="label">검색일</div>
                    <div class="input">
                        <input type="text" name="gen_dt_start" size="15" id="dframtDetailList_gen_start_dt" value="${dfrAmtFormBean.gen_dt_start}" onclick="dframtDetailsetSens('dframtDetailList_gen_end_dt', 'max');" />~
                        <input type="text" name="gen_dt_end" size="15" id="dframtDetailList_gen_end_dt" value="${dfrAmtFormBean.gen_dt_end}" onclick="dframtDetailsetSens('dframtDetailList_gen_start_dt', 'min');" />
                    </div>
                    
                    <div class ="input"><input type="button" name="week" value="1주"/></div>
                    <div class ="input"><input type="button" name="month" value="1달"/></div>


                    <div class="label2">지급ID</div>
                    <div class="input">
                        <input type="text" name="onffmerch_no" value ="${dfrAmtFormBean.onffmerch_no}" readonly/>
                    </div> 
                    <div class="label">지급ID명</div>
                    <div class="input">
                        <input type="text" name="merch_nm" value ="${dfrAmtFormBean.merch_nm}" />
                    </div>               

                    <div class="label">생성사유</div>
                    <div class="input">
                        <select name="gen_reson">
                            <option value="">전체</option>
                            <c:forEach var="code" items="${DfrGenResonList.tb_code_details}">
                                <option value="${code.detail_code}">${code.code_nm}</option>
                            </c:forEach>
                        </select>
                    </div>                        

                    <div class="input">
                        <input type="button" name="search" value="검색"/>
                        <input type="button" name="init" value="초기화"/>
                        <input type="button" name="excel" value="엑셀"/>
                    </div>

                </form>
                    <div class="paging">
                       <div id="dframtdetailPaging" style="width: 50%;"></div>
                       <div id="dframtdetailrecinfoArea" style="width: 50%;"></div>
                   </div>                        
            </div>

            <div id="dframtDetailList" style="width:100%;height:100%;background-color:white;"></div>
            
            <script type="text/javascript">
                
                var dframtDetailGrid;
                var dframtDetailListCalendar;
                var dhxSelPopupWins=new dhtmlXWindows();
                
                $(document).ready(function () {          
                    
                    dframtDetailListCalendar = new dhtmlXCalendarObject(["dframtDetailList_gen_start_dt","dframtDetailList_gen_end_dt"]);

                    var dframtDetailSearch = document.getElementById("dframtDetailSearch");  
                    dframtDetailLayout = main_dframt_layout.cells('b').attachLayout("2E");
                    dframtDetailLayout.cells('a').hideHeader();
                    dframtDetailLayout.cells('b').hideHeader();
                    dframtDetailLayout.cells('a').attachObject(dframtDetailSearch);
                    dframtDetailLayout.cells('a').setHeight(65);
                    dframtDetailGrid = dframtDetailLayout.cells('b').attachGrid();
                    dframtDetailGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var dframtDetailGridheaders = "";
                    dframtDetailGridheaders += "<center>NO</center>,<center>보류 발생일</center>,<center>지급ID명</center>,<center>지급ID</center>";
                    dframtDetailGridheaders += ",<center>발생보류금액</center>,<center>보류확정액</center>,<center>보류해제액</center>,<center>생성사유</center>";
                    dframtDetailGrid.setHeader(dframtDetailGridheaders);
                    dframtDetailGrid.setColAlign("center,center,center,center,right,right,center,center");
                    dframtDetailGrid.setColTypes("txt,txt,txt,txt,edn,edn,txt,txt");
                    dframtDetailGrid.setInitWidths("50,150,150,150,100,100,100,100");
                    dframtDetailGrid.setColSorting("str,str,str,str,int,int,str,str");
                    dframtDetailGrid.setNumberFormat("0,000",4,".",",");
                    dframtDetailGrid.setNumberFormat("0,000",5,".",",");
                    dframtDetailGrid.enableColumnMove(true);
                    dframtDetailGrid.setSkin("dhx_skyblue");
                    dframtDetailGrid.enablePaging(true,Number($("#dframtDetailForm input[name=page_size]").val()),10,"dframtdetailPaging",true,"dframtdetailrecinfoArea");
                    dframtDetailGrid.setPagingSkin("bricks");                    
                    
                    dframtDetailGrid.init();
                    dframtDetailGrid.parse(${ls_dframtDetailList}, "json");
                    dframtDetailGrid.attachEvent("onRowDblClicked", doUpdateFormDframt);
                    
                    
                    //페이징 처리
                    dframtDetailGrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                        if(lInd !==0){
                             $("#dframtDetailForm input[name=page_no]").val(ind);
                            dframtDetailGrid.clearAll();
                            doSearchDframtDetail();
                        }else{
                             $("#dframtDetailForm input[name=page_no]").val(1);
                            dframtDetailGrid.clearAll();
                            doSearchDframtDetail();
                        }                        
                    });
                    
                    
                    //검색조건 초기화
                    $("#dframtDetailForm input[name=init]").click(function () {
                        dframtDetailInit($("#dframtDetailForm"));
                    });  
                    
                    $("#dframtDetailForm input[name=week]").click(function(){
                        dframtDetail_date_search("week");
                    });

                    $("#dframtDetailForm input[name=month]").click(function(){
                        dframtDetail_date_search("month");
                    });
                    
                    $("#dframtDetailForm input[name=search]").click(function(){
                        //doSearchDframtDetail();
                        dframtDetailGrid.changePage(1);
                    });                  
                    
                    $("#dframtDetailForm input[name=excel]").click(function(){
                        $("#dframtDetailForm").attr("action","<c:url value="/dframt/dframtDetailListExcel" />");
                        document.getElementById("dframtDetailForm").submit();
                    });                           
                    
                    //버튼 추가
                    $("#dframtDetailForm .searchButton").button({
                        icons: {
                            primary: "ui-icon-search"
                        }
                    })
                    .unbind("click")
                    .bind("click",function(e){
                        //보류 정보 조회 이벤트
                        dframtDetailGrid.changePage(1);
                        return false;
                    });                  
                   
                });
                
                
                
                 //지급ID번호 팝업 클릭 이벤트
                $("#dframtDetailForm input[name=onffmerch_no]").unbind("click").bind("click", function (){

                    dframtDetailonoffMerchSelectPopup();

                });                          
                
                function dframtDetailInit($form) {
                    searchFormInit($form);
                    dframtDetail_date_search("week");                    
                } 

                 function dframtDetailonoffMerchSelectPopup(){
                    onoffObject.onffmerch_no = $("#dframtDetailForm input[name=onffmerch_no]");
                    onoffObject.merch_nm = $("#dframtDetailForm input[name=merch_nm]");
                    //지급ID정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("가맹점 선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                }
                
                function dframtDetail_date_search(day){
                    var oneDate = 1000 * 3600 * 24; 
                    var now = new Date();
                    var week = new Date(now.getTime() + (oneDate*-6));
                    var month = new Date(now.getTime() + (oneDate*-31));
                    var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                    var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                    var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                    var date=new Date();
                    if(day=="today"){
                        this.$("input[id=dframtDetailList_gen_start_dt]").val(nowTime);
                        this.$("input[id=dframtDetailList_gen_end_dt]").val(nowTime);
                    }else if(day=="week"){
                        this.$("input[id=dframtDetailList_gen_start_dt]").val(weekTime);
                        this.$("input[id=dframtDetailList_gen_end_dt]").val(nowTime);
                    }else{
                        this.$("input[id=dframtDetailList_gen_start_dt]").val(monthTime);
                        this.$("input[id=dframtDetailList_gen_end_dt]").val(nowTime);
                    }
                }
                
                function dframtDetailsetSens(id, k) {
                    // update range
                    if (k == "min") {
                        dframtDetailListCalendar.setSensitiveRange(byId(id).value, null);
                    } else {
                        dframtDetailListCalendar.setSensitiveRange(null, byId(id).value);
                    }
                }
			
                function byId(id) {
                    return document.getElementById(id);
                }                
                
                //보류 상세 목록 조회
                function doSearchDframtDetail(){
                    $.ajax({
                        url : "<c:url value="/dframt/dframtDetailList" />",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#dframtDetailForm").serialize(),
                        success : function(data) {
                            dframtDetailGrid.parse(jQuery.parseJSON(data), "json");
                        },
                        error : function() {
                            alert("조회 실패");
                        }
                    });
                }      
                
                //보류 수정
                function doUpdateFormDframt(rowid){
                    
                    var dfr_seq = dframtDetailGrid.getUserData(rowid, 'dfr_seq');
                    w1 = dframtMasterWindow.createWindow("w1", 100, 0, 900, 700);
                    w1.setText("보류 수정");
                    dframtMasterWindow.window('w1').setModal(true);
                    w1.attachURL('<c:url value="/dframt/dframtMasterUpdate" />' + '?dfr_seq='+dfr_seq,true);                       
                    
                }                
                
            </script>
            
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>
