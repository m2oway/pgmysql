<%-- 
    Document   : dframtMasterInsert
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>보류금 등록</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">
                
                var dframtMasterInsertCalendar;
                var merchpay_popup_yn = '${dfrAmtFormBean.merchpay_popup_yn}';
                var dhxSelPopupWins;
                
                $(document).ready(function () {
                    
                    dhxSelPopupWins = new dhtmlXWindows(); 
                    
                    //보류 등록 이벤트
                    $("#dframtMasterFormInsert input[name=insert]").click(function(){
                       doInsertDframtMaster(); 
                    });              
                    
                    //보류 등록 닫기
                    $("#dframtMasterFormInsert input[name=close]").click(function(){
                        
                        if(merchpay_popup_yn === "Y"){
                            merchpayMetabolismWindow.window("outamtListWindow").close();
                        }else{
                            dframtMasterWindow.window("w1").close();
                        }

                    });                      
                    
                    dframtMasterInsertCalendar = new dhtmlXCalendarObject(["dframtMasterInsert_exp_pay_dt", "dframtMasterInsert_gen_dt"]);
                    
                });
                
                //ONOFF지급ID번호 팝업 클릭 이벤트
                $("#dframtMasterFormInsert input[name=merch_nm]").unbind("click").bind("click", function (){
                    
                    dframtonoffMerchSelectPopup();

                });                           
                
                function dframtonoffMerchSelectPopup(){
                    onoffObject.onffmerch_no = $("#dframtMasterFormInsert input[name=onffmerch_no]");
                    onoffObject.merch_nm = $("#dframtMasterFormInsert input[name=merch_nm]");
                    //ONOFF지급ID정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("지급ID 선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                }                

                //보류금 등록
                function doInsertDframtMaster(){
                    
                    if($("#dframtMasterFormInsert input[name=onffmerch_no]").val().trim().length === 0){
                        alert("지급ID을 선택하여 주십시오.");
                        $("#dframtMasterFormInsert input[name=onffmerch_no]").focus();
                        return false;
                    }
                    
                    if($("#dframtMasterFormInsert input[name=gen_dfr_amt]").val().trim().length === 0){
                        alert("보류금액을 입력하여 주십시오.");
                        $("#dframtMasterFormInsert input[name=gen_dfr_amt]").focus();
                        return false;
                    }

                    if($("#dframtMasterFormInsert input[name=gen_dt]").val().trim().length === 0){
                        alert("보류발생일을 입력하여 주십시오.");
                        $("#dframtMasterFormInsert input[name=gen_dt]").focus();
                        return false;
                    }               
                    /*
                    if($("#dframtMasterFormInsert input[name=exp_pay_dt]").val().trim().length === 0){
                        alert("지급예정일을 입력하여 주십시오.");
                        $("#dframtMasterFormInsert input[name=exp_pay_dt]").focus();
                        return false;
                    }                            
                    */
                    if(!window.confirm("등록하시겠습니까?")){
                        return false;
                    }

                    $.ajax({
                        url : $("#dframtMasterFormInsert").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#dframtMasterFormInsert").serialize(),
                        success : function(data) {
                            alert("등록 완료");
                            if(merchpay_popup_yn === "Y"){
                                merchpayMasterSearch();
                                merchpayDetailGrid.clearAll();
                                merchpayMasterWindow.window("w1").close();
                            }else{
                                dframtSearch();
                                dframtMasterWindow.window("w1").close();
                            }                            
                            

                        },
                        error : function() { 
                                alert("등록 실패");
                        }
                    });	      
        
                }                   

            </script>

            <form id="dframtMasterFormInsert" method="POST" action="<c:url value="/dframt/dframtMasterInsert" />" modelAttribute="dframtFormBean">
                <table class="gridtable" height="100%" width="100%">
                    <tr>
                        <td class="headcol">지급ID</td>
                        <td>
                            <input type="hidden" name="onffmerch_no"  value="" />
                            <input name="merch_nm" value="" size="60" />
                        </td>
                        <td class="headcol">사유</td>
                        <td>
                            <select name="gen_reson">
                                <c:forEach var="code" items="${dfrGenResonList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>                                
                        </td>                        
                    </tr>                    
                    <tr>
                        <td class="headcol">보류금</td>
                        <td><input name="gen_dfr_amt" onkeyup="javascript:InpuOnlyNumber2(this);" onblur="javascript:checkLength(this,20);" /></td>
                        <td class="headcol">보류발생일</td>
                        <td><input name="gen_dt" id="dframtMasterInsert_gen_dt" value="" /></td>
                    </tr>
                    <!--
                    <tr>
                        <td class="headcol">지급예정일</td>
                        <td><input name="exp_pay_dt" id="dframtMasterInsert_exp_pay_dt" value="" /></td>
                        <td class="headcol"></td>
                        <td></td>                        
                    </tr>
                    -->
                    <tr>
                        <td class="headcol">메모</td>
                        <td colspan="3">
                            <textarea name="memo" cols="70" onblur="javascript:checkLength(this,500);"></textarea>
                        </td>
                    </tr> 
                    <tr>
                        <td colspan="4">
                            <input type="button" name="insert" value="등록"/>
                            <input type="button" name="close" value="닫기"/>
                        </td>
                    </tr>
                </table>
            </form>

<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

