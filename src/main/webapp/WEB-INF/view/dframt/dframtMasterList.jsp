<%-- 
    Document   : 
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${!ajaxRequest}">
<jsp:include page="/WEB-INF/view/common/common.jsp"/>
<html>
    <head>
        <title>보류금조회</title>             
    </head>
    <body>
</c:if>
            
        <div class="right" id="dframt_search" style="width:100%;">       
            <form id="dframtForm" method="POST" onsubmit="return false" action="<c:url value="/dframt/dframtMasterList" />" modelAttribute="dfrAmtMainFormBean"> 
                <input type="hidden" name="page_size" value="100" >
                <input type="hidden" name="page_no" value="1" >
                <table>
                    <tr>
                            <div class="label">지급ID명</div>
                            <div class="input">
                                <input type="text" name="merch_nm" value ="${dfrAmtMainFormBean.merch_nm}" />
                            </div>        
                            <div class="label">지급ID</div>
                            <div class="input">
                                <input type="text" name="onffmerch_no" value ="${dfrAmtMainFormBean.onffmerch_no}" />
                            </div>                              
                        <td><input type="button" name="dframt_search_bt" value="조회"/></td>                        
                        <td><input type="button" name="dframt_excel_bt" value="엑셀"/></td>
                        <td><input type="button" name="dframt_init_bt" value="검색조건지우기"/></td>
                        <div class="action">
                            <button class="addButton">추가</button>
                        </div>                              
                    </tr>
               </table>                            
            </form>
                            
            <div class="paging">
                <div id="dframtPaging" style="width: 50%;"></div>
                <div id="dframtrecinfoArea" style="width: 50%;"></div>
            </div>                        
        </div>
        
        <script type="text/javascript">
            
            var dframtgrid={};
            var dframtCalendar;
            var dframtMasterWindow;
            
            $(document).ready(function () {

                dframtMasterWindow = new dhtmlXWindows();
                
                var dframt_searchForm = document.getElementById("dframt_search");
    
                dframt_layout = main_dframt_layout.cells('a').attachLayout("2E");
                     
                dframt_layout.cells('a').hideHeader();
                dframt_layout.cells('b').hideHeader();
                 
                dframt_layout.cells('a').attachObject(dframt_searchForm);
                dframt_layout.cells('a').setHeight(55);
                
                dframtgrid = dframt_layout.cells('b').attachGrid();
                
                dframtgrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");

                var dframtheaders = "";
                dframtheaders += "<center>NO</center>,<center>지급ID명</center>,<center>지급ID</center>,<center>보류생성액</center>,<center>보류확정액</center>,<center>보류해제액</center>,<center>보류요청잔액</center>,<center>보류확정잔액</center>";
                dframtgrid.setHeader(dframtheaders);
                dframtgrid.setColAlign("center,center,center,right,right,right,right,right");
                dframtgrid.setColTypes("txt,txt,txt,edn,edn,edn,edn,edn");
                dframtgrid.setInitWidths("70,150,150,90,90,90,90,90");
                dframtgrid.setColSorting("str,str,str,int,int,int,int,int");
                dframtgrid.setNumberFormat("0,000",3);
                dframtgrid.setNumberFormat("0,000",4);
                dframtgrid.setNumberFormat("0,000",5);
                dframtgrid.setNumberFormat("0,000",6);
                dframtgrid.setNumberFormat("0,000",7);
                dframtgrid.enableColumnMove(true);
                dframtgrid.setSkin("dhx_skyblue");

                dframtgrid.enablePaging(true,Number($("#dframtForm input[name=page_size]").val()),10,"dframtPaging",true,"dframtrecinfoArea");
                dframtgrid.setPagingSkin("bricks");    
                
                dframtgrid.attachEvent("onPageChanged", function(ind,fInd,lInd){
                    if(lInd !==0){
                      $("#dframtForm input[name=page_no]").val(ind);      
                       dframtgrid.clearAll();
                        dframtSearch();
                    }else{
                       $("#dframtForm input[name=page_no]").val(1);
                        dframtgrid.clearAll();
                         dframtSearch();
                    }                    
                    
                });                    

                dframtgrid.init();
                dframtgrid.parse(${ls_dframtMasterList}, "json");

                dframtgrid.attachEvent("onRowDblClicked", dframt_attach);

                //신용거래 정보 검색 이벤트
                $("#dframtForm input[name=dframt_search_bt]").click(function () {
                    
                    dframtgrid.changePage(1);
                    return false;
                });
                
                 $("#dframtForm input[name=onffmerch_no]").unbind("click").bind("click", function (){

                    onoffMerchSecuritySelectPopup();

                });
                
                 function onoffMerchSecuritySelectPopup(){
                    onoffObject.onffmerch_no = $("#dframtForm input[name=onffmerch_no]");
                    onoffObject.merch_nm = $("#dframtForm input[name=merch_nm]");
                    //ONOFF지급ID정보 팝업
                    w1 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w1.setText("지급ID번호 선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w1.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                }
                
                
                //신용거래 정보 엑셀다운로드 이벤트
                $("#dframtForm input[name=dframt_excel_bt]").click(function () {
                    
                    dframtExcel();
                    
                })
                
                //검색조건 초기화
                $("#dframtForm input[name=dframt_init_bt]").click(function () {
                    
                    dframtMasterInit($("#dframtForm"));
                    
                });                
                
                $("#dframtForm .addButton").button({
                    icons: {
                        primary: "ui-icon-plusthick"
                    },
                    text: false
                })
                .unbind("click")
                .bind("click",function(e){
                    //등록 이벤트
                    doInsertFormDframtMaster();
                    return false;
                });                    
                  
            });
            
            //보류 등록
            function doInsertFormDframtMaster(){

                w1 = dframtMasterWindow.createWindow("w1", 150, 150, 850, 250);
                w1.setText("보류 등록");
                dframtMasterWindow.window('w1').setModal(true);
                onffmerch_no = '${dfrAmtMainFormBean.onffmerch_no}';
                w1.attachURL("<c:url value="/dframt/dframtMasterInsert" />" + '?onffmerch_no='+onffmerch_no, true);   

            }                    

            //신용거래 상세조회 이벤트
            function dframt_attach(rowid, col) {
                var onffmerch_no = dframtgrid.getUserData(rowid, 'onffmerch_no');
                main_dframt_layout.cells('b').attachURL("<c:url value="/dframt/dframtDetailList" />"+"?onffmerch_no=" + onffmerch_no, true);
                
            }
            
            //신용거래 정보 조회
            function dframtSearch() {
                
                $.ajax({
                    url: "<c:url value="/dframt/dframtMasterList" />",
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#dframtForm").serialize(),
                    success: function (data) {
                        dframtgrid.clearAll();
                        dframtgrid.parse($.parseJSON(data), "json");

                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }
            

            
            //신용거래 엑셀다운로드
            function dframtExcel() {

                $("#dframtForm").attr("action","<c:url value="/dframt/dframtMasterListExcel" />");
                document.getElementById("dframtForm").submit();

            }       
                
            //검색조건 초기화
            function dframtMasterInit($form) {

                searchFormInit($form);

            } 
            
        </script>

<c:if test="${!ajaxRequest}">
    </body>
</html>
</c:if>        
