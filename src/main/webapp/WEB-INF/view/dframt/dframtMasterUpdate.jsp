<%-- 
    Document   : merchdframtMasterUpdate
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>보류 해제, 확정</title>
        </head>
        <body>
</c:if>
            <div style="overflow: auto;height: 600px;">
            <form id="dframtMasterFormUpdate" method="POST" action="<c:url value="/dframt/dframtMasterUpdate" />" modelAttribute="dframtFormBean">
                <input type="hidden" name="dfr_seq" value="${ls_dframtDetailList[0].dfr_seq}" />
                <input type="hidden" name="onffmerch_no" value="${ls_dframtDetailList[0].onffmerch_no}" />
                <input type="hidden" name="gen_dfr_amt" value="${ls_dframtDetailList[0].gen_dfr_amt}" />
                <table class="gridtable" height="40%" width="100%">
                    <tr>
                        <td class="headcol">지급ID</td>
                        <td>${ls_dframtDetailList[0].merch_nm}</td>
                        <td class="headcol">지급예정일</td>
                        <td>${ls_dframtDetailList[0].exp_pay_dt}</td>
                    </tr>
                    <tr>
                        <td class="headcol">보류생성금</td>
                        <td>${ls_dframtDetailList[0].gen_dfr_amt}</td>
                        <td class="headcol">보류확정액</td>
                        <td>${ls_dframtDetailList[0].dec_dfr_amt}</td>
                    </tr>
                    <tr>
                        <td class="headcol">보류해제액</td>
                        <td>${ls_dframtDetailList[0].rel_dfr_amt}</td>
                        <td class="headcol">보류생성일</td>
                        <td>${ls_dframtDetailList[0].gen_dt}</td>                        
                    </tr>                    
                    <tr>
                        <td class="headcol">사유</td>
                        <td>
                            <select name="gen_reson">
                                <c:forEach var="code" items="${dfrGenResonList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${ls_dframtDetailList[0].gen_reson == code.detail_code}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td class="headcol"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="headcol">메모</td>
                        <td colspan="3">
                            <textarea name="memo" cols="80">${ls_dframtDetailList[0].memo}</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <input type="button" name="update" value="수정"/>
                            <input type="button" name="delete" value="삭제"/>
                        </td>
                    </tr>
                </table>
            </form>
                
            <c:if test="${ls_dframtDetailList[0].gen_dfr_amt != ls_dframtDetailList[0].dec_dfr_amt}">
                        
                <form id="dframtDetailFormInsert" method="POST" action="<c:url value="/dframt/dframtDetailInsert" />" modelAttribute="dframtFormBean">
                    <input type="hidden" name="dfr_seq" value="${ls_dframtDetailList[0].dfr_seq}" />
                    <input type="hidden" name="onffmerch_no" value="${ls_dframtDetailList[0].onffmerch_no}" />
                    <table class="gridtable" height="30%" width="100%">
                        <tr>
                            <td class="headcol">보류확정 사유</td>
                            <td>
                                <select name="dfr_dec_reson">
                                    <c:forEach var="code" items="${dfrDecResonList.tb_code_details}">
                                        <option value="${code.detail_code}">${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td class="headcol">보류확정금액</td>
                            <td><input name="dfr_dec_amt" onkeyup="javascript:InpuOnlyNumber(this);" onblur="javascript:checkLength(this,20);" /></td>
                        </tr>
                        <tr>
                            <td class="headcol">보류확정일</td>
                            <td><input name="dfr_dec_dt" id="dframtDetailFormInsertDfr_dec_dt"/></td>
                            <td class="headcol">지급일</td>
                            <td><input name="exp_pay_dt" id="dframtDetailFormInsertExp_pay_dt"/></td>
                        </tr>
                        <tr>
                            <td class="headcol">메모</td>
                            <td colspan="3">
                                <textarea name="memo" cols="80"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4"><input type="button" name="insert" value="보류확정"/></td>
                        </tr>
                    </table>

                </form>
                
            </c:if>

            <div id="dframtMasterFormUpdateList" style="position: relative;width:800px;height:145px;background-color:white;"></div>
            <button id="dfr_dec_Delete">삭제</button>
            
            <c:if test="${ls_dframtDetailList[0].gen_dfr_amt != ls_dframtDetailList[0].rel_dfr_amt}">
                        
                <form id="dframtRelFormInsert" method="POST" action="<c:url value="/dframt/dframtRelInsert" />" modelAttribute="dframtFormBean">
                    <input type="hidden" name="dfr_seq" value="${ls_dframtDetailList[0].dfr_seq}" />
                    <input type="hidden" name="onffmerch_no" value="${ls_dframtDetailList[0].onffmerch_no}" />
                    <table class="gridtable" height="30%" width="100%">
                        <tr>
                            <td class="headcol">보류해제 사유</td>
                            <td>
                                <select name="dfr_rel_reson">
                                    <c:forEach var="code" items="${dfrRelResonList.tb_code_details}">
                                        <option value="${code.detail_code}">${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td class="headcol">보류해제금액</td>
                            <td><input name="dfr_rel_amt" onkeyup="javascript:InpuOnlyNumber(this);" onblur="javascript:checkLength(this,20);" /></td>
                        </tr>
                        <tr>
                            <td class="headcol">보류해제일</td>
                            <td><input name="dfr_rel_dt" id="dframtRelFormInsertDfr_rel_dt"/></td>
                            <td class="headcol">지급일</td>
                            <td><input name="exp_pay_dt" id="dframtRelFormInsertExp_pay_dt"/></td>
                        </tr>
                        <tr>
                            <td class="headcol">메모</td>
                            <td colspan="3">
                                <textarea name="memo" cols="80"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4"><input type="button" name="insert" value="보류해제"/></td>
                        </tr>
                    </table>

                </form>
                
            </c:if>                
            
            <div id="dframtRelFormUpdateList" style="position: relative;width:800px;height:145px;background-color:white;"></div>
            <button id="dfr_rel_Delete">삭제</button>
        </div>
            
            <script type="text/javascript">
                
                var dframtMasterUpdateGrid;
                var dframtMasterUpdateCalendar;
                var dframtRelUpdateGrid;
                
                $(document).ready(function () {
                    
                    dframtMasterUpdateCalendar = new dhtmlXCalendarObject(["dframtDetailFormInsertDfr_dec_dt","dframtDetailFormInsertExp_pay_dt","dframtRelFormInsertDfr_rel_dt","dframtRelFormInsertExp_pay_dt"]);
                    
                    dframtMasterUpdateGrid = new dhtmlXGridObject('dframtMasterFormUpdateList');
                    dframtMasterUpdateGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var dframtMasterUpdateGridheaders = "";
                    dframtMasterUpdateGridheaders += "<center>NO</center>,<center>선택</center>,<center>보류확정일자</center>,<center>보류확정사유</center>,<center>보류확정금액</center>,<center>메모</center>";
                    dframtMasterUpdateGrid.setHeader(dframtMasterUpdateGridheaders);
                    dframtMasterUpdateGrid.setColAlign("center,center,center,center,right,center");
                    dframtMasterUpdateGrid.setColTypes("txt,ch,txt,txt,edn,txt");
                    dframtMasterUpdateGrid.setInitWidths("50,50,150,150,100,300");
                    dframtMasterUpdateGrid.setColSorting("str,str,str,str,int,str");
                    dframtMasterUpdateGrid.setNumberFormat("0,000",4,".",",");
                    dframtMasterUpdateGrid.enableColumnMove(true);
                    dframtMasterUpdateGrid.setSkin("dhx_skyblue");
                    dframtMasterUpdateGrid.init();
                    dframtMasterUpdateGrid.parse(${ls_drfDecList}, "json");             
                    
                    dframtRelUpdateGrid = new dhtmlXGridObject('dframtRelFormUpdateList');
                    dframtRelUpdateGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var dframtRelUpdateGridheaders = "";
                    dframtRelUpdateGridheaders += "<center>NO</center>,<center>선택</center>,<center>보류해제일자</center>,<center>보류해제사유</center>,<center>보류해제금액</center>,<center>메모</center>";
                    dframtRelUpdateGrid.setHeader(dframtRelUpdateGridheaders);
                    dframtRelUpdateGrid.setColAlign("center,center,center,center,right,center");
                    dframtRelUpdateGrid.setColTypes("txt,ch,txt,txt,edn,txt");
                    dframtRelUpdateGrid.setInitWidths("50,50,150,150,100,300");
                    dframtRelUpdateGrid.setColSorting("str,str,str,str,int,str");
                    dframtRelUpdateGrid.setNumberFormat("0,000",4,".",",");
                    dframtRelUpdateGrid.enableColumnMove(true);
                    dframtRelUpdateGrid.setSkin("dhx_skyblue");
                    dframtRelUpdateGrid.init();
                    dframtRelUpdateGrid.parse(${ls_drfRelList}, "json");                           
                    
                    //보류금 수정 이벤트
                    $("#dframtMasterFormUpdate input[name=update]").click(function(){
                       doUpdateDframtMaster(); 
                    });
                    
                    //보류금 삭제 이벤트
                    $("#dframtMasterFormUpdate input[name=delete]").click(function(){
                       doDeleteDframtMaster(); 
                    });                    
                    
                    //보류금 확정 이벤트
                    $("#dframtDetailFormInsert input[name=insert]").click(function(){
                       doInsertDframtDetail(); 
                    });              
                    
                    //보류금 등록 닫기
                    $("#dframtMasterFormUpdate input[name=close]").click(function(){
                        dframtMasterWindow.window("w1").close();
                    });
                    
                    //보류확정 삭제 이벤트
                    $("#dfr_dec_Delete").click(function(){
                       doDeleteDframtDetail(); 
                    });                                        
                    
                    //보류해제 등록 이벤트
                    $("#dframtRelFormInsert input[name=insert]").click(function(){
                       doInsertDframtRel(); 
                    });                                  
                    
                    //보류해제 삭제 이벤트
                    $("#dfr_rel_Delete").click(function(){
                       doDeleteDframtRel(); 
                    });                                  
                    
                });

                //보류금 수정
                function doUpdateDframtMaster(){
                    
                    if(!window.confirm("수정하시겠습니까?")){
                        return;
                    }
                    
                    $.ajax({
                        url : "<c:url value="/dframt/dframtMasterUpdate" />",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#dframtMasterFormUpdate").serialize(),
                        success : function(data) {
                            alert("수정 완료");
                            dframtgrid.clearAll();
                            dframtSearch();
                            dframtDetailGrid.clearAll();
                            dframtMasterWindow.window("w1").close();
                        },
                        error : function() { 
                                alert("수정 실패");
                        }
                    });	      
        
                }           
                
                //보류금 삭제
                function doDeleteDframtMaster(){
                    
                    if(${ls_dframtDetailList[0].dec_dfr_amt > 0}){
                        alert("보류확정된 보류금이 존재합니다.");
                        return;
                    }
                    
                    var pay_sche_seq = '${ls_dframtDetailList[0].pay_sche_seq}';
                    if(pay_sche_seq.length > 0){
                        alert("지급확정된 보류금입니다.");
                        return;
                    }
                    
                    if(!window.confirm("삭제하시겠습니까?")){
                        return;
                    }
                    
                    $.ajax({
                        url : "<c:url value="/dframt/dframtMasterDelete" />",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#dframtMasterFormUpdate").serialize(),
                        success : function(data) {
                            alert("삭제 완료");                            
                            dframtgrid.clearAll();
                            dframtSearch();
                            dframtDetailGrid.clearAll();
                            dframtMasterWindow.window("w1").close();
                        },
                        error : function() { 
                                alert("삭제 실패");
                        }
                    });	      
        
                }                        
                
                //보류금 확정 등록
                function doInsertDframtDetail(){
                    
                    if($("#dframtDetailFormInsert input[name=dfr_dec_amt]").val().trim().length === 0){
                        alert("보류확정금액을 입력하여 주십시오.");
                        $("#dframtDetailFormInsert input[name=dfr_dec_amt]").focus();
                        return false;
                    }
                    
                    if($("#dframtDetailFormInsert input[name=dfr_dec_dt]").val().trim().length === 0){
                        alert("보류확정일을 입력하여 주십시오.");
                        $("#dframtDetailFormInsert input[name=dfr_dec_dt]").focus();
                        return false;
                    }      
                    
                    if($("#dframtDetailFormInsert input[name=exp_pay_dt]").val().trim().length === 0){
                        alert("지급일을 입력하여 주십시오.");
                        $("#dframtDetailFormInsert input[name=exp_pay_dt]").focus();
                        return false;
                    }                          
                    
                    if(!window.confirm("보류확정 하시겠습니까?")){
                        return;
                    }
                    
                    $.ajax({
                        url : "<c:url value="/dframt/dframtDetailInsert" />",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#dframtDetailFormInsert").serialize(),
                        success : function(data) {
                            alert("보류확정 완료");
                            dframtgrid.clearAll();
                            dframtSearch();
                            dframtDetailGrid.clearAll();
                            doSearchDframtDetail();
                            dframtMasterWindow.window("w1").close();
                        },
                        error : function() { 
                                alert("보류확정 실패");
                        }
                    });	      
        
                }            
                
                //보류확정 정보 삭제
                function doDeleteDframtDetail(){

                    var rel_dfr_amt = '${ls_dframtDetailList[0].rel_dfr_amt}';
                    
                    if(rel_dfr_amt > 0){
                        alert("보류해제 금액이 존재합니다.");
                        return;
                    }

                    var rowId = dframtMasterUpdateGrid.getCheckedRows(1);
                    
                    if(rowId.length < 1){
                        alert("삭제할 보류확정 내역을 체크하십시오.");
                        return;
                    }
                    
                    var rowIds = rowId.split(",");
                    
                    var ckeckFlag = true;
                    
                    for(i=0;i<rowIds.length;i++){
                        if(ckeckFlag){
                            if(dframtMasterUpdateGrid.getUserData(Number(rowIds[i]), 'pay_sche_seq') !== 'null'){
                                ckeckFlag = false;
                            }
                        }
                    }
                    
                    if(!ckeckFlag){
                        alert("지급확정된 내역이 체크 되었습니다.");
                        return;
                    }
                    
                    var dfr_dec_amt = 0;
                    var dec_dfr_seqs = '';
                    
                    for(i=0;i<rowIds.length;i++){
                        dfr_dec_amt += Number(dframtMasterUpdateGrid.getUserData(Number(rowIds[i]), 'dfr_dec_amt'));
                        
                        if(i == 0){
                            dec_dfr_seqs += dframtMasterUpdateGrid.getUserData(Number(rowIds[i]), 'dec_dfr_seq');
                        }else{
                            dec_dfr_seqs += ','+dframtMasterUpdateGrid.getUserData(Number(rowIds[i]), 'dec_dfr_seq');
                        }
                        
                    }
                    
                    var dfr_seq = '${ls_dframtDetailList[0].dfr_seq}';
                    
                    var onffmerch_no = dframtMasterUpdateGrid.getUserData(Number(rowIds[i]), 'onffmerch_no')
                    
                    if(!window.confirm("삭제하시겠습니까?")){
                        return;
                    }

                    $.ajax({
                        url : "<c:url value="/dframt/dframtDetailDelete" />",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: {dec_dfr_seqs : dec_dfr_seqs
                               ,dfr_dec_amt : dfr_dec_amt
                               ,onffmerch_no : onffmerch_no
                               ,dfr_seq : dfr_seq},
                        success : function(data) {
                            alert("삭제 완료");         
                            dframtgrid.clearAll();
                            dframtSearch();
                            dframtDetailGrid.clearAll();
                            doSearchDframtDetail();
                            dframtMasterWindow.window("w1").close();
                        },
                        error : function(jqXHR, textStatus, errorThrown) {
                            alert("삭제 실패 = " + errorThrown);

                        }
                    });
                }           
                
                //보류해제 등록
                function doInsertDframtRel(){
                
                    if($("#dframtRelFormInsert input[name=dfr_rel_amt]").val().trim().length === 0){
                        alert("보류해제금액을 입력하여 주십시오.");
                        $("#dframtRelFormInsert input[name=dfr_rel_amt]").focus();
                        return false;
                    }
                    
                    var dec_bal_amt = ${ls_dframtDetailList[0].dec_dfr_amt} - ${ls_dframtDetailList[0].rel_dfr_amt};
                    
                    if($("#dframtRelFormInsert input[name=dfr_rel_amt]").val() > dec_bal_amt){
                        alert("보류해제금액이 보류확정잔액보다 큽니다. ");
                        return false;
                    }
                    
                    if($("#dframtRelFormInsert input[name=dfr_rel_dt]").val().trim().length === 0){
                        alert("보류해제일을 입력하여 주십시오.");
                        $("#dframtRelFormInsert input[name=dfr_rel_dt]").focus();
                        return false;
                    }      
                    
                    if($("#dframtRelFormInsert input[name=exp_pay_dt]").val().trim().length === 0){
                        alert("지급일을 입력하여 주십시오.");
                        $("#dframtRelFormInsert input[name=exp_pay_dt]").focus();
                        return false;
                    }                          
                    
                    if(!window.confirm("보류해제 하시겠습니까?")){
                        return;
                    }
                    
                    $.ajax({
                        url : "<c:url value="/dframt/dframtRelInsert" />",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#dframtRelFormInsert").serialize(),
                        success : function(data) {
                            alert("보류해제 완료");
                            dframtgrid.clearAll();
                            dframtSearch();
                            dframtDetailGrid.clearAll();
                            doSearchDframtDetail();
                            dframtMasterWindow.window("w1").close();
                        },
                        error : function() { 
                                alert("보류해제 실패");
                        }
                    });	      
        
                }            
                
                //보류해제 삭제
                function doDeleteDframtRel(){

                    var rowId = dframtRelUpdateGrid.getCheckedRows(1);
                    
                    if(rowId.length < 1){
                        alert("삭제할 보류해제 내역을 체크하십시오.");
                        return;
                    }
                    
                    var rowIds = rowId.split(",");
                    
                    var ckeckFlag = true;
                    
                    for(i=0;i<rowIds.length;i++){
                        if(ckeckFlag){
                            if(dframtRelUpdateGrid.getUserData(Number(rowIds[i]), 'pay_sche_seq') !== 'null'){
                                ckeckFlag = false;
                            }
                        }
                    }                
                    
                    if(!ckeckFlag){
                        alert("지급확정된 내역이 체크되었습니다.");
                        return;
                    }
                    
                    var dfr_rel_amt = 0;
                    var dfr_rel_seqs = '';
                    
                    for(i=0;i<rowIds.length;i++){
                        dfr_rel_amt += Number(dframtRelUpdateGrid.getUserData(Number(rowIds[i]), 'dfr_rel_amt'));
                        
                        if(i == 0){
                            dfr_rel_seqs += dframtRelUpdateGrid.getUserData(Number(rowIds[i]), 'dfr_rel_seq');
                        }else{
                            dfr_rel_seqs += ','+dframtRelUpdateGrid.getUserData(Number(rowIds[i]), 'dfr_rel_seq');
                        }
                        
                    }
                    
                    var dfr_seq = '${ls_dframtDetailList[0].dfr_seq}';
                    
                    var onffmerch_no = dframtRelUpdateGrid.getUserData(Number(rowIds[i]), 'onffmerch_no')
                    
                    if(!window.confirm("삭제하시겠습니까?")){
                        return;
                    }

                    $.ajax({
                        url : "<c:url value="/dframt/dframtRelDelete" />",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: {dfr_rel_seqs : dfr_rel_seqs
                               ,dfr_rel_amt : dfr_rel_amt
                               ,onffmerch_no : onffmerch_no
                               ,dfr_seq : dfr_seq},
                        success : function(data) {
                            alert("삭제 완료");         
                            dframtgrid.clearAll();
                            dframtSearch();
                            dframtDetailGrid.clearAll();
                            doSearchDframtDetail();
                            dframtMasterWindow.window("w1").close();
                        },
                        error : function(jqXHR, textStatus, errorThrown) {
                            alert("삭제 실패 = " + errorThrown);

                        }
                    });
                }                       

            </script>                
                
                
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

