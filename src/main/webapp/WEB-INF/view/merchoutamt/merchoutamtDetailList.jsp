<%-- 
    Document   : merchoutamtDetailList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>미수 정보 관리</title>
        </head>
        <body>
</c:if>

            <div class="searchForm1row" id="merchoutamtDetailSearch" style="display:none;width:100%;height:100%;background-color:white;">
                
                <form id="merchoutamtDetailForm" method="POST" action="<c:url value="/merchoutamt/merchoutamtDetailList" />" modelAttribute="merchoutamtFormBean">
                    
                    <input type="hidden" name="page_size" value="100" >
                    <input type="hidden" name="page_no" value="1" >
                    
                    <div class="label">검색일</div>
                    <div class="input">
                        <input type="text" name="gen_start_dt" id="merchoutamtDetailList_gen_start_dt" value="${merchoutamtFormBean.gen_start_dt}" onclick="merchoutamtDetailsetSens('merchoutamtDetailList_gen_end_dt', 'max');" />~
                        <input type="text" name="gen_end_dt" id="merchoutamtDetailList_gen_end_dt" value="${merchoutamtFormBean.gen_end_dt}" onclick="merchoutamtDetailsetSens('merchoutamtDetailList_gen_start_dt', 'min');" />
                    </div>
                    
                    <div class ="input"><input type="button" name="week" value="1주"/></div>
                    <div class ="input"><input type="button" name="month" value="1달"/></div>


                    <div class="label2">지급ID</div>
                    <div class="input">
                        <input type="text" name="onffmerch_no" value ="${merchoutamtFormBean.onffmerch_no}" readonly />
                    </div> 
                    <div class="label">지급ID명</div>
                    <div class="input">
                        <input type="text" name="merch_nm" value ="${merchoutamtFormBean.merch_nm}" readonly />
                    </div>               

                    <div class="label">상계상태</div>
                    <div class="input">
                        <select name="outamt_stat">
                            <option value="">전체</option>
                            <c:forEach var="code" items="${outamtStatList.tb_code_details}">
                                <option value="${code.detail_code}"  <c:if test="${code.detail_code == merchoutamtFormBean.outamt_stat}">selected</c:if> >${code.code_nm}</option>
                            </c:forEach>
                        </select>
                    </div>                        

                    <div class="input">
                        <input type="button" name="search" value="검색"/>
                        <input type="button" name="init" value="초기화"/>
                        <input type="button" name="excel" value="엑셀"/>
                    </div>

                </form>
                    
                <div class="paging">
                    <div id="merchoutamtDetailPaging" style="width: 50%;"></div>
                    <div id="merchoutamtDetailrecinfoArea" style="width: 50%;"></div>
                </div>                                     
                        
            </div>
            
            <div id="merchoutamtDetailList" style="width:100%;height:100%;background-color:white;"></div>
            
            <script type="text/javascript">
                
                var merchoutamtDetailGrid;
                var merchoutamtDetailListCalendar;
                var dhxSelPopupWins=new dhtmlXWindows();
                
                $(document).ready(function () {          
                    
                    merchoutamtDetailListCalendar = new dhtmlXCalendarObject(["merchoutamtDetailList_gen_start_dt","merchoutamtDetailList_gen_end_dt"]);

                    var merchoutamtDetailSearch = document.getElementById("merchoutamtDetailSearch");  
                    merchoutamtDetailLayout = merchoutamtLayout.cells('b').attachLayout("2E");
                    merchoutamtDetailLayout.cells('a').hideHeader();
                    merchoutamtDetailLayout.cells('b').hideHeader();
                    merchoutamtDetailLayout.cells('a').attachObject(merchoutamtDetailSearch);
                    merchoutamtDetailLayout.cells('a').setHeight(65);
                    merchoutamtDetailGrid = merchoutamtDetailLayout.cells('b').attachGrid();
                    merchoutamtDetailGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var merchoutamtDetailGridheaders = "";
                    merchoutamtDetailGridheaders += "<center>NO</center>,<center>미수 발생일</center>,<center>지급ID</center>,<center>지급ID명</center>";
                    merchoutamtDetailGridheaders += ",<center>발생미수금액</center>,<center>미수잔액</center>,<center>사유</center>,<center>상계상태</center>";
                    merchoutamtDetailGrid.setHeader(merchoutamtDetailGridheaders);
                    merchoutamtDetailGrid.setColAlign("center,center,center,center,right,right,center,center");
                    merchoutamtDetailGrid.setColTypes("txt,txt,txt,txt,edn,edn,txt,txt");
                    merchoutamtDetailGrid.setInitWidths("50,150,150,150,100,100,100,100");
                    merchoutamtDetailGrid.setColSorting("str,str,str,str,int,int,str,str");
                    merchoutamtDetailGrid.setNumberFormat("0,000",4,".",",");
                    merchoutamtDetailGrid.setNumberFormat("0,000",5,".",",");
                    merchoutamtDetailGrid.enableColumnMove(true);
                    merchoutamtDetailGrid.setSkin("dhx_skyblue");
                    
                    var merchoutamtDetailGridFilters = ",#text_filter,#select_filter,#select_filter,#numeric_filter,#numeric_filter,#select_filter,#select_filter";
                    merchoutamtDetailGrid.attachHeader(merchoutamtDetailGridFilters);                    

                    merchoutamtDetailGrid.enablePaging(true,Number($("#merchoutamtDetailForm input[name=page_size]").val()),10,"merchoutamtDetailPaging",true,"merchoutamtDetailrecinfoArea");
                    merchoutamtDetailGrid.setPagingSkin("bricks");    

                    merchoutamtDetailGrid.attachEvent("onPageChanged", function(ind,fInd,lInd){

                        if(lInd !==0){
                          $("#merchoutamtDetailForm input[name=page_no]").val(ind);      
                            merchoutamtDetailGrid.clearAll();
                            doSearchMerchOutamtDetail();
                        }else{
                           $("#merchoutamtDetailForm input[name=page_no]").val(1);
                            merchoutamtDetailGrid.clearAll();
                            doSearchMerchOutamtDetail();
                        }                    

                    });                                            
                    
                    merchoutamtDetailGrid.init();
                    merchoutamtDetailGrid.parse(${merchoutamtDetailList}, "json");
                    merchoutamtDetailGrid.attachEvent("onRowDblClicked", doUpdateFormMerchOutamt);
                    
                    //검색조건 초기화
                    $("#merchoutamtDetailForm input[name=init]").click(function () {
                        merchoutamtDetailInit($("#merchoutamtDetailForm"));
                    });  
                    
                    $("#merchoutamtDetailForm input[name=week]").click(function(){
                        merchoutamtDetail_date_search("week");
                    });

                    $("#merchoutamtDetailForm input[name=month]").click(function(){
                        merchoutamtDetail_date_search("month");
                    });
                    
                    $("#merchoutamtDetailForm input[name=search]").click(function(){
                        doSearchMerchOutamtDetail();
                    });                  
                    
                    $("#merchoutamtDetailForm input[name=excel]").click(function(){
                        $("#merchoutamtDetailForm").attr("action","<c:url value="/merchoutamt/merchoutamtDetailListExcel" />");
                        document.getElementById("merchoutamtDetailForm").submit();
                    });                           
                    
                    //버튼 추가
                    $("#merchoutamtDetailForm .searchButton").button({
                        icons: {
                            primary: "ui-icon-search"
                        }
                    })
                    .unbind("click")
                    .bind("click",function(e){
                        //미수 정보 조회 이벤트
                        doSearchMerchOutamtDetail();
                        return false;
                    });                   
                   
                });
                
                 //지급ID번호 팝업 클릭 이벤트
                $("#merchoutamtDetailForm input[name=onffmerch_no]").unbind("click").bind("click", function (){

                    merchoutamtDetailonoffMerchSelectPopup();

                });                                         
                
                
                 //지급ID번호 팝업 클릭 이벤트
                $("#merchoutamtDetailForm input[name=merch_nm]").unbind("click").bind("click", function (){

                    merchoutamtDetailonoffMerchSelectPopup();

                });                          
                
                function merchoutamtDetailInit($form) {
                    searchFormInit($form);
                    //merchoutamtDetail_date_search("week");                    
                } 

                 function merchoutamtDetailonoffMerchSelectPopup(){
                    onoffObject.onffmerch_no = $("#merchoutamtDetailForm input[name=onffmerch_no]");
                    onoffObject.merch_nm = $("#merchoutamtDetailForm input[name=merch_nm]");
                    //지급ID정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("지급ID 선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                }
                
                function merchoutamtDetail_date_search(day){
                    var oneDate = 1000 * 3600 * 24; 
                    var now = new Date();
                    var week = new Date(now.getTime() + (oneDate*-6));
                    var month = new Date(now.getTime() + (oneDate*-31));
                    var nowTime = now.getFullYear() +"-"+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +"-"+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
                    var weekTime = week.getFullYear() +"-"+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +"-"+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
                    var monthTime = month.getFullYear() +"-"+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +"-"+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

                    var date=new Date();
                    if(day=="today"){
                        this.$("input[id=merchoutamtDetailList_gen_start_dt]").val(nowTime);
                        this.$("input[id=merchoutamtDetailList_gen_end_dt]").val(nowTime);
                    }else if(day=="week"){
                        this.$("input[id=merchoutamtDetailList_gen_start_dt]").val(weekTime);
                        this.$("input[id=merchoutamtDetailList_gen_end_dt]").val(nowTime);
                    }else{
                        this.$("input[id=merchoutamtDetailList_gen_start_dt]").val(monthTime);
                        this.$("input[id=merchoutamtDetailList_gen_end_dt]").val(nowTime);
                    }
                }
                
                function merchoutamtDetailsetSens(id, k) {
                    // update range
                    if (k == "min") {
                        merchoutamtDetailListCalendar.setSensitiveRange(byId(id).value, null);
                    } else {
                        merchoutamtDetailListCalendar.setSensitiveRange(null, byId(id).value);
                    }
                }
			
                function byId(id) {
                    return document.getElementById(id);
                }                
                
                //미수 상세 목록 조회
                function doSearchMerchOutamtDetail(){
                    $.ajax({
                        url : "<c:url value="/merchoutamt/merchoutamtDetailList" />",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#merchoutamtDetailForm").serialize(),
                        success : function(data) {
                            merchoutamtDetailGrid.clearAll();
                            merchoutamtDetailGrid.parse(jQuery.parseJSON(data), "json");
                        },
                        error : function() {
                            alert("조회 실패");
                        }
                    });
                }      
                
                //미수 상계 조회
                function doUpdateFormMerchOutamt(rowid){
                    
                    var outamt_seq = merchoutamtDetailGrid.getUserData(rowid, 'outamt_seq');
                    w1 = merchoutamtMasterWindow.createWindow("w1", 100, 100, 900, 600);
                    w1.setText("미수 상계");
                    merchoutamtMasterWindow.window('w1').setModal(true);
                    w1.attachURL('<c:url value="/merchoutamt/merchoutamtMasterUpdate" />' + '?outamt_seq='+outamt_seq,true);                       
                    
                }             
                
            </script>
            
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>
