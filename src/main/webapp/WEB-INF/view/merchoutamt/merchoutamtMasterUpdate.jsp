<%-- 
    Document   : merchmerchoutamtMasterUpdate
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>미수 상계</title>
        </head>
        <body>
</c:if>

            <form id="merchoutamtMasterFormUpdate" method="POST" action="<c:url value="/merchoutamt/merchoutamtMasterUpdate" />" modelAttribute="merchoutamtFormBean">
                <input type="hidden" name="outamt_seq" value="${ls_merchoutamtDetailList[0].outamt_seq}" />
                <table class="gridtable" height="40%" width="100%">
                    <tr>
                        <td class="headcol">지급ID</td>
                        <td>${ls_merchoutamtDetailList[0].merch_nm}</td>
                        <td class="headcol">미수생성일</td>
                        <td>${ls_merchoutamtDetailList[0].gen_dt}</td>                        
                    </tr>
                    <tr>
                        <td class="headcol">미수금 생성금</td>
                        <td>${ls_merchoutamtDetailList[0].out_amt}</td>
                        <td class="headcol">미수잔액</td>
                        <td>${ls_merchoutamtDetailList[0].cur_outamt}</td>
                    </tr>
                    <tr>
                        <td class="headcol">사유</td>
                        <td>
                            <select name="gen_reson">
                                <c:forEach var="code" items="${insResonList.tb_code_details}">
                                    <option value="${code.detail_code}" <c:if test="${ls_merchoutamtDetailList[0].gen_reson == code.detail_code}">selected</c:if>>${code.code_nm}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td class="headcol"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="headcol">메모</td>
                        <td colspan="3">
                            <textarea name="memo" cols="80">${ls_merchoutamtDetailList[0].memo}</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <input type="button" name="update" value="수정"/>
                            <input type="button" name="delete" value="삭제"/>
                        </td>
                    </tr>
                </table>
            </form>
                
            <c:if test="${ls_merchoutamtDetailList[0].outamt_stat == '0'}">

                <form id="merchoutamtDetailFormInsert" method="POST" action="<c:url value="/merchoutamt/merchoutamtDetailInsert" />" modelAttribute="merchoutamtFormBean">
                    <input type="hidden" name="outamt_seq" value="${ls_merchoutamtDetailList[0].outamt_seq}" />
                    <table class="gridtable" height="30%" width="100%">
                        <tr>
                            <td class="headcol">상계방법</td>
                            <td>
                                <select name="setoff_cate">
                                    <c:forEach var="code" items="${setoffCateList.tb_code_details}">
                                        <option value="${code.detail_code}">${code.code_nm}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td class="headcol">상계금액</td>
                            <td><input name="setoff_amt" onkeyup="javascript:InpuOnlyNumber(this);" onblur="javascript:checkLength(this,20);" /></td>
                        </tr>
                        <tr>
                            <td class="headcol">상계일</td>
                            <td><input name="setoff_date" id="merchoutamtDetailFormInsertSetoff_date"/></td>
                            <td class="headcol"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="headcol">메모</td>
                            <td colspan="3">
                                <textarea name="memo" cols="80"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4"><input type="button" name="insert" value="상계처리"/></td>
                        </tr>
                    </table>

                </form>

            </c:if>

            <div id="merchoutamtMasterFormUpdateList" style="position: relative;width:800px;height:145px;background-color:white;"></div>
            <button id="setoffDelete">삭제</button>

            <script type="text/javascript">
                
                var merchoutamtMasterUpdateGrid;
                var merchoutamtMasterUpdateCalendar;
                
                $(document).ready(function () {
                    
                    merchoutamtMasterUpdateCalendar = new dhtmlXCalendarObject(['merchoutamtDetailFormInsertSetoff_date',"merchoutamtMasterUpdate_exp_pay_dt","merchoutamtMasterUpdate_gen_dt"]);
                    
                    merchoutamtMasterUpdateGrid = new dhtmlXGridObject('merchoutamtMasterFormUpdateList');
                    merchoutamtMasterUpdateGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var merchoutamtMasterUpdateGridheaders = "";
                    merchoutamtMasterUpdateGridheaders += "<center>NO</center>,<center>선택</center>,<center>상계일자</center>,<center>상계방법</center>,<center>상계금액</center>,<center>메모</center>";
                    merchoutamtMasterUpdateGrid.setHeader(merchoutamtMasterUpdateGridheaders);
                    merchoutamtMasterUpdateGrid.setColAlign("center,center,center,center,right,center");
                    merchoutamtMasterUpdateGrid.setColTypes("txt,ch,txt,txt,edn,txt");
                    merchoutamtMasterUpdateGrid.setInitWidths("50,50,150,150,100,150");
                    merchoutamtMasterUpdateGrid.setColSorting("str,str,str,str,int,str");
                    merchoutamtMasterUpdateGrid.setNumberFormat("0,000",4,".",",");
                    merchoutamtMasterUpdateGrid.enableColumnMove(true);
                    merchoutamtMasterUpdateGrid.setSkin("dhx_skyblue");
                    merchoutamtMasterUpdateGrid.init();
                    merchoutamtMasterUpdateGrid.parse(${ls_merchsetoffinfoList}, "json");             
                    
                    //미수금 수정 이벤트
                    $("#merchoutamtMasterFormUpdate input[name=update]").click(function(){
                       doUpdateMerchOutamtMaster(); 
                    });
                    
                    //미수금 삭제 이벤트
                    $("#merchoutamtMasterFormUpdate input[name=delete]").click(function(){
                       doDeleteMerchOutamtMaster(); 
                    });                    
                    
                    //미수금 상계 이벤트
                    $("#merchoutamtDetailFormInsert input[name=insert]").click(function(){
                       doInsertMerchOutamtDetail(); 
                    });              
                    
                    //미수금 등록 닫기
                    $("#merchoutamtMasterFormUpdate input[name=close]").click(function(){
                        merchoutamtMasterWindow.window("w1").close();
                    });
                    
                    //상계 삭제 이벤트
                    $("#setoffDelete").click(function(){
                       doDeleteMerchSetoff(); 
                    });                                        
                    
                });

                //미수금 수정
                function doUpdateMerchOutamtMaster(){
                    
                    if(!window.confirm("수정하시겠습니까?")){
                        return;
                    }
                    
                    $.ajax({
                        url : $("#merchoutamtMasterFormUpdate").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#merchoutamtMasterFormUpdate").serialize(),
                        success : function(data) {
                            alert("수정 완료");
                            doSearchMerchOutamtMaster();
                            merchoutamtDetailGrid.clearAll();
                            merchoutamtMasterWindow.window("w1").close();
                        },
                        error : function() { 
                                alert("수정 실패");
                        }
                    });	      
        
                }           
                
                //미수금 상계 등록
                function doInsertMerchOutamtDetail(){
                    
                    if($("#merchoutamtDetailFormInsert input[name=setoff_amt]").val().trim().length === 0){
                        alert("상계금액을 입력하여 주십시오.");
                        $("#merchoutamtDetailFormInsert input[name=setoff_amt]").val("");
                        $("#merchoutamtDetailFormInsert input[name=setoff_amt]").focus();
                        return false;
                    }
                    
                    if($("#merchoutamtDetailFormInsert input[name=setoff_date]").val().trim().length === 0){
                        alert("상계일을 입력하여 주십시오.");
                        $("#merchoutamtDetailFormInsert input[name=setoff_date]").val("");
                        $("#merchoutamtDetailFormInsert input[name=setoff_date]").focus();
                        return false;
                    }                    
                    
                    if(!window.confirm("상계하시겠습니까?")){
                        return;
                    }
                    
                    $.ajax({
                        url : $("#merchoutamtDetailFormInsert").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#merchoutamtDetailFormInsert").serialize(),
                        success : function(data) {
                            alert("상계 완료");
                            doSearchMerchOutamtMaster();
                            merchoutamtDetailGrid.clearAll();
                            merchoutamtMasterWindow.window("w1").close();
                        },
                        error : function() { 
                                alert("상계 실패");
                        }
                    });	      
        
                }            
                
                //미수금 삭제
                function doDeleteMerchOutamtMaster(){
                    
                    if(${ls_merchoutamtDetailList[0].out_amt} !== ${ls_merchoutamtDetailList[0].cur_outamt}){
                        alert("상계내역이 있습니다.");
                        return;
                    }
                    
                    var pay_sche_seq = '${ls_merchoutamtDetailList[0].pay_sche_seq}';
                    
                    if(pay_sche_seq.length > 0){
                        alert("지급확정된 미수금입니다.");
                        return;
                    }
                    
                    if(!window.confirm("삭제하시겠습니까?")){
                        return;
                    }
                    
                    $.ajax({
                        url : "<c:url value="/merchoutamt/merchoutamtMasterDelete" />",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#merchoutamtMasterFormUpdate").serialize(),
                        success : function(data) {
                            alert("삭제 완료");                            
                            doSearchMerchOutamtMaster();
                            merchoutamtDetailGrid.clearAll();
                            merchoutamtMasterWindow.window("w1").close();
                        },
                        error : function() { 
                                alert("삭제 실패");
                        }
                    });	      
        
                }        
                
                //상계 정보 삭제
                function doDeleteMerchSetoff(){

                    var rowId = merchoutamtMasterUpdateGrid.getCheckedRows(1);
                    
                    if(rowId.length < 1){
                        alert("삭제할 상계내역을 체크하십시오.");
                        return;
                    }
                    
                    var rowIds = rowId.split(",");
                    
                    var ckeckFlag = true;
                    
                    for(i=0;i<rowIds.length;i++){
                        if(ckeckFlag){
                            if(merchoutamtMasterUpdateGrid.getUserData(Number(rowIds[i]), 'pay_sche_seq') !== 'null'){
                                ckeckFlag = false;
                            }
                        }
                    }
                    
                    if(!ckeckFlag){
                        alert("지급확정된 상계내역입니다.");
                        return;
                    }
                    
                    var setoff_amt = 0;
                    
                    for(i=0;i<rowIds.length;i++){
                        setoff_amt += Number(merchoutamtMasterUpdateGrid.getUserData(Number(rowIds[i]), 'setoff_amt'));
                    }
                    
                    var outamt_stat = '${ls_merchoutamtDetailList[0].outamt_stat}';
                    var outamt_seq = '${ls_merchoutamtDetailList[0].outamt_seq}';
                    
                    if(!window.confirm("삭제하시겠습니까?")){
                        return;
                    }

                    $.ajax({
                        url : "<c:url value="/merchoutamt/merchoutamtSetoffDelete" />",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: {setoff_seqs : rowId
                               ,setoff_amt : setoff_amt
                               ,outamt_stat : outamt_stat
                               ,outamt_seq : outamt_seq},
                        success : function(data) {
                            alert("삭제 완료");                            
                            doSearchMerchOutamtMaster();
                            merchoutamtDetailGrid.clearAll();
                            merchoutamtMasterWindow.window("w1").close();
                        },
                        error : function(jqXHR, textStatus, errorThrown) {
                            alert("삭제 실패 = " + errorThrown);

                        }
                    });
                }                

            </script>                
                
                
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

