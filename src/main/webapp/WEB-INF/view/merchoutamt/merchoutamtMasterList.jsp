<%-- 
    Document   : merchoutamtMasterList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>가맹점 미수 정보 관리</title>
        </head>
        <body>
</c:if>

            <div class="searchForm1row" id="merchoutamtMasterSearch" style="display:none;width:100%;height:100%;background-color:white;">
                
                <form id="merchoutamtMasterForm" method="POST" action="<c:url value="/merchoutamt/merchoutamtMasterList" />" modelAttribute="merchoutamtFormBean">
                    
                    <input type="hidden" name="page_size" value="100" >
                    <input type="hidden" name="page_no" value="1" >
                    
                    <div class="label2">지급ID</div>
                    <div class="input">
                        <input type="text" name="onffmerch_no" value ="${merchoutamtFormBean.onffmerch_no}" readonly/>
                    </div> 
                    <div class="label">지급ID명</div>
                    <div class="input">
                        <input type="text" name="merch_nm" value ="${merchoutamtFormBean.merch_nm}" readonly/>
                    </div>                        

                    <div class="input">
                        <input type="button" name="search" value="검색"/>
                        <input type="button" name="init" value="초기화"/>
                        <input type="button" name="excel" value="엑셀"/>
                    </div>
                    
                    <div class="action">
                        <button class="addButton">추가</button>
                    </div>                                                                    

                </form>
                    
                <div class="paging">
                    <div id="merchoutamtMasterPaging" style="width: 50%;"></div>
                    <div id="merchoutamtMasterrecinfoArea" style="width: 50%;"></div>
                </div>                          
                        
            </div>
            
            <div id="merchoutamtMasterList" style="width:100%;height:100%;background-color:white;"></div>
            
            <script type="text/javascript">
                
                var merchoutamtMasterGrid;
                var merchoutamtMasterWindow;
                var merchoutamtMasterListCalendar;
                var dhxSelPopupWins=new dhtmlXWindows();
                
                $(document).ready(function () {          

                    merchoutamtMasterListCalendar = new dhtmlXCalendarObject(["merchoutamtMasterList_gen_start_dt","merchoutamtMasterList_gen_end_dt"]);

                    var merchoutamtSearch = document.getElementById("merchoutamtMasterSearch");            
                    merchoutamtLayout = tabbar.cells("7-2").attachLayout("2E");
                    merchoutamtLayout.cells('a').hideHeader();
                    merchoutamtLayout.cells('b').hideHeader();
                    
                    merchoutamtMasterLayout = merchoutamtLayout.cells('a').attachLayout("2E");
                    merchoutamtMasterLayout.cells('a').hideHeader();
                    merchoutamtMasterLayout.cells('b').hideHeader();
                    merchoutamtMasterLayout.cells('a').attachObject(merchoutamtSearch);
                    merchoutamtMasterLayout.cells('a').setHeight(65);
                    merchoutamtMasterGrid = merchoutamtMasterLayout.cells('b').attachGrid();
                    merchoutamtMasterGrid.setImagePath("<c:url value="/resources/images/dhx/dhtmlxGrid/" />");
                    var merchoutamtMasterGridheaders = "";
                    merchoutamtMasterGridheaders += "<center>NO</center>,<center>지급ID</center>,<center>지급ID명</center>,<center>미수금액</center>,<center>미수잔액</center>";
                    merchoutamtMasterGrid.setHeader(merchoutamtMasterGridheaders);
                    merchoutamtMasterGrid.setColAlign("center,center,center,right,right");
                    merchoutamtMasterGrid.setColTypes("txt,txt,txt,edn,edn");
                    merchoutamtMasterGrid.setInitWidths("50,150,200,150,150");
                    merchoutamtMasterGrid.setColSorting("str,str,str,int,int");
                    var merchoutamtMasterGridFooters = "총계,#cspan,#cspan,<div id='merchoutamtMasterGrid3'>0</div>,<div id='merchoutamtMasterGrid4'>0</div>";
                    merchoutamtMasterGrid.attachFooter(merchoutamtMasterGridFooters);
                    merchoutamtMasterGrid.setNumberFormat("0,000",3,".",",");
                    merchoutamtMasterGrid.setNumberFormat("0,000",4,".",",");
                    merchoutamtMasterGrid.enableColumnMove(true);
                    merchoutamtMasterGrid.setSkin("dhx_skyblue");
                    
                    merchoutamtMasterGrid.enablePaging(true,Number($("#merchoutamtMasterForm input[name=page_size]").val()),10,"merchoutamtMasterPaging",true,"merchoutamtMasterrecinfoArea");
                    merchoutamtMasterGrid.setPagingSkin("bricks");    

                    merchoutamtMasterGrid.attachEvent("onPageChanged", function(ind,fInd,lInd){

                        if(lInd !==0){
                          $("#merchoutamtMasterForm input[name=page_no]").val(ind);      
                            merchoutamtMasterGrid.clearAll();
                            doSearchMerchOutamtMaster();
                        }else{
                           $("#merchoutamtMasterForm input[name=page_no]").val(1);
                            merchoutamtMasterGrid.clearAll();
                            doSearchMerchOutamtMaster();
                        }                    

                    });                     
                    
                    merchoutamtMasterGrid.init();
                    merchoutamtMasterGrid.parse(${merchoutamtMasterList}, merchoutamtMasterGridFooterValues, "json");
                    merchoutamtMasterGrid.attachEvent("onRowDblClicked", doSearchFormMerchOutamtDetail);
                    
                    merchoutamtMasterWindow = new dhtmlXWindows();
                    merchoutamtMasterWindow.attachViewportTo("merchoutamtMasterList");
                    
                    //검색조건 초기화
                    $("#merchoutamtMasterForm input[name=init]").click(function () {
                        merchoutamtMasterInit($("#merchoutamtMasterForm"));
                    });  
                    
                    $("#merchoutamtMasterForm input[name=search]").click(function(){
                        doSearchMerchOutamtMaster();
                    });                    

                    $("#merchoutamtMasterForm input[name=excel]").click(function(){
                        $("#merchoutamtMasterForm").attr("action","<c:url value="/merchoutamt/merchoutamtMasterListExcel" />");
                        document.getElementById("merchoutamtMasterForm").submit();
                    });  
                    
                    $("#merchoutamtMasterForm .addButton").button({
                        icons: {
                            primary: "ui-icon-plusthick"
                        },
                        text: false
                    })
                    .unbind("click")
                    .bind("click",function(e){
                        //미수 등록 이벤트
                       doInsertFormMerchOutamtMaster();
                       return false;
                    });                    
                    
                });
                
                //초기화
                function merchoutamtMasterInit($form) {
                    searchFormInit($form);          
                } 
                
                //미수금 정보 조회
                function doSearchMerchOutamtMaster(){
                    
                    $.ajax({
                        url : "<c:url value="/merchoutamt/merchoutamtMasterList" />",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#merchoutamtMasterForm").serialize(),
                        success : function(data) {
                            
                            merchoutamtMasterGrid.clearAll();
                            var jsonData = $.parseJSON(data);
                            merchoutamtMasterGrid.parse(jsonData,merchoutamtMasterGridFooterValues,"json");
                            
                        },
                        error : function() {
                            alert("조회 실패");
                        }
                    });
                }      
                
                //미수 상세 목록 조회
                function doSearchFormMerchOutamtDetail(rowid){
                    var onffmerch_no = merchoutamtMasterGrid.getUserData(rowid, 'onffmerch_no');
                    merchoutamtLayout.cells('b').attachURL("<c:url value="/merchoutamt/merchoutamtDetailList" />" + "?onffmerch_no="+onffmerch_no,true);
                }
                
                function merchoutamtMasterGridFooterValues() {
                    
                    var merchoutamtMasterGrid3 = document.getElementById("merchoutamtMasterGrid3");
                    var merchoutamtMasterGrid4 = document.getElementById("merchoutamtMasterGrid4");
                    merchoutamtMasterGrid3.innerHTML = putComma(sumColumn(3));
                    merchoutamtMasterGrid4.innerHTML = putComma(sumColumn(4));

                    return true;
                }                
                
                function sumColumn(ind) {
                    
                    var out = 0;
                    for (var i = 0; i < merchoutamtMasterGrid.getRowsNum(); i++) {
                         out += parseFloat(merchoutamtMasterGrid.cells2(i, ind).getValue());
                    }

                    return out;
                    
                }                
                
                //지급ID번호 팝업 클릭 이벤트
                $("#merchoutamtMasterForm input[name=merch_nm]").unbind("click").bind("click", function (){
                    
                    merchoutamtonoffMerchSelectPopup();

                });           
                
                //지급ID번호 팝업 클릭 이벤트
                $("#merchoutamtMasterForm input[name=merch_nm]").unbind("click").bind("click", function (){
                    
                    merchoutamtonoffMerchSelectPopup();

                });                            
                
                function merchoutamtonoffMerchSelectPopup(){
                    onoffObject.onffmerch_no = $("#merchoutamtMasterForm input[name=onffmerch_no]");
                    onoffObject.merch_nm = $("#merchoutamtMasterForm input[name=merch_nm]");
                    //지급ID정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("지급ID 선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                }
                
                //미수 등록
                function doInsertFormMerchOutamtMaster(){

                    w1 = merchoutamtMasterWindow.createWindow("w1", 150, 150, 850, 250);
                    w1.setText("미수 등록");
                    merchoutamtMasterWindow.window('w1').setModal(true);
                    onffmerch_no = '${merchoutamtFormBean.onffmerch_no}';
                    w1.attachURL("<c:url value="/merchoutamt/merchoutamtMasterInsert" />" + '?onffmerch_no='+onffmerch_no, true);   

                }                         
                
            </script>
            
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>
