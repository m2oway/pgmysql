<%-- 
    Document   : merchoutamtMasterInsert
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>미수금 등록</title>
        </head>
        <body>
</c:if>

            <script type="text/javascript">
                
                var merchoutamtMasterInsertCalendar;
                
                $(document).ready(function () {
                    
                    //미수 등록 이벤트
                    $("#merchoutamtMasterFormInsert input[name=insert]").click(function(){
                       doInsertMerchOutamtMaster(); 
                    });              
                    
                    //미수 등록 닫기
                    $("#merchoutamtMasterFormInsert input[name=close]").click(function(){
                        
                        merchoutamtMasterWindow.window("w1").close();

                    });                      
                    
                    merchoutamtMasterInsertCalendar = new dhtmlXCalendarObject(["merchoutamtMasterInsert_exp_pay_dt", "merchoutamtMasterInsert_gen_dt"]);
                    
                });
                
                //ONOFF지급ID번호 팝업 클릭 이벤트
                $("#merchoutamtMasterFormInsert input[name=merch_nm]").unbind("click").bind("click", function (){
                    
                    merchoutamtonoffMerchSelectPopup();

                });                           
                
                function merchoutamtonoffMerchSelectPopup(){
                    onoffObject.onffmerch_no = $("#merchoutamtMasterFormInsert input[name=onffmerch_no]");
                    onoffObject.merch_nm = $("#merchoutamtMasterFormInsert input[name=merch_nm]");
                    //ONOFF지급ID정보 팝업
                    w2 = dhxSelPopupWins.createWindow("OnoffmerchSelectPopUp", 20, 30, 640, 480);
                    w2.setText("지급ID 선택페이지");
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').setModal(true);
                    dhxSelPopupWins.window('OnoffmerchSelectPopUp').denyResize();
                    w2.attachURL('<c:url value = "/popup/OnoffmerchSelectPopUp"/>', true);   
                }                

                //미수금 등록
                function doInsertMerchOutamtMaster(){
                    
                    if($("#merchoutamtMasterFormInsert input[name=onffmerch_no]").val().trim().length === 0){
                        alert("지급ID을 선택하여 주십시오.");
                        $("#merchoutamtMasterFormInsert input[name=onffmerch_no]").focus();
                        return false;
                    }
                    
                    if($("#merchoutamtMasterFormInsert input[name=out_amt]").val().trim().length === 0){
                        alert("미수금액을 입력하여 주십시오.");
                        $("#merchoutamtMasterFormInsert input[name=out_amt]").focus();
                        return false;
                    }

                    if($("#merchoutamtMasterFormInsert input[name=gen_dt]").val().trim().length === 0){
                        alert("미수발생일을 입력하여 주십시오.");
                        $("#merchoutamtMasterFormInsert input[name=gen_dt]").focus();
                        return false;
                    }                                
                    
                    if(!window.confirm("등록하시겠습니까?")){
                        return false;
                    }

                    $.ajax({
                        url : $("#merchoutamtMasterFormInsert").attr("action"),
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#merchoutamtMasterFormInsert").serialize(),
                        success : function(data) {
                            alert("등록 완료");
                            merchoutamtMasterGrid.clearAll();
                            doSearchMerchOutamtMaster();
                            merchoutamtMasterWindow.window("w1").close();                            
                        },
                        error : function() { 
                                alert("등록 실패");
                        }
                    });	      
        
                }                   

            </script>

            <form id="merchoutamtMasterFormInsert" method="POST" action="<c:url value="/merchoutamt/merchoutamtMasterInsert" />" modelAttribute="merchoutamtFormBean">
                <table class="gridtable" height="100%" width="100%">
                    <tr>
                        <td class="headcol">지급ID</td>
                        <td>
                            <input type="hidden" name="onffmerch_no" value="" />
                            <input name="merch_nm" value="" readonly="readonly" />
                        </td>
                        <td class="headcol">사유</td>
                        <td>
                            <select name="gen_reson">
                                <c:forEach var="code" items="${insResonList.tb_code_details}">
                                    <option value="${code.detail_code}">${code.code_nm}</option>
                                </c:forEach>
                            </select>                                
                        </td>                        
                    </tr>                    
                    <tr>
                        <td class="headcol">미수금</td>
                        <td><input name="out_amt" onkeyup="javascript:InpuOnlyNumber2(this);" onblur="javascript:checkLength(this,20);" /></td>
                        <td class="headcol">미수발생일</td>
                        <td><input name="gen_dt" id="merchoutamtMasterInsert_gen_dt" value="${merchoutamtFormBean.gen_dt}" /></td>
                    </tr>             
                    <tr>
                        <td class="headcol">메모</td>
                        <td colspan="3">
                            <textarea name="memo" cols="100" onblur="javascript:checkLength(this,500);"></textarea>
                        </td>
                    </tr> 
                    <tr>
                        <td colspan="4">
                            <input type="button" name="insert" value="등록"/>
                            <input type="button" name="close" value="닫기"/>
                        </td>
                    </tr>
                </table>
            </form>

<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

