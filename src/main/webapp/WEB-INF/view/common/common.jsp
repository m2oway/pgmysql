<%--
  Created by IntelliJ IDEA.
  User: hoyeongheo
  Date: 2014-10-28
  Time: 오후 7:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<link href="<c:url value="/resources/css/form.css" />" rel="stylesheet"  type="text/css" />

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/smoothness/jquery-ui-1.8.17.custom.css"  />" />
<script type="text/javascript" src="<c:url value="/resources/javascript/jquery-1.6.2.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/javascript/jquery-ui-1.8.17.custom.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/javascript/jqueryform/2.8/jquery.form.js" />"></script>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dhx/dhtmlxlayout.css" />"  />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dhx/dhtmlxlayout_dhx_skyblue.css" />"  />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dhx/dhtmlxmenu_dhx_skyblue.css" />"  />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dhx/dhtmlxtabbar.css" />"  />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dhx/dhtmlxlayout.css" />"  />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dhx/dhtmlxlayout_dhx_skyblue.css" />"  />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dhx/dhtmlxgrid.css" />"  />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dhx/dhtmlxgrid_dhx_skyblue.css" />"  />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dhx/dhtmlxaccordion_dhx_skyblue.css" />"  />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dhx/dhtmlxcalendar.css" />"  />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dhx/dhtmlxcalendar_dhx_skyblue.css" />"  />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dhx/dhx.css" />"  />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dhx/dhtmlxgrid_pgn_bricks.css" />"  />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dhx/dhtmlxwindows.css" />"  />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dhx/dhtmlxwindows_dhx_skyblue.css" />"  />

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dhx/dhtmlxtoolbar_dhx_skyblue.css" />"  />

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/main/settlementPaymentCss.css" />"  />

<script src="<c:url value="/resources/javascript/common.js" />"></script>
<script src="<c:url value="/resources/javascript/dhx/dhtmlxcommon.js" />"></script>
<script src="<c:url value="/resources/javascript/dhx/dhtmlxcontainer.js" />"></script>
<script src="<c:url value="/resources/javascript/dhx/dhtmlxlayout.js" />"></script>
<script src="<c:url value="/resources/javascript/dhx/dhtmlxmenu.js" />"></script>
<script src="<c:url value="/resources/javascript/dhx/dhtmlxmenu_ext.js" />"></script>

<script src="<c:url value="/resources/javascript/dhx/dhtmlxtabbar.js" />"></script>
<script src="<c:url value="/resources/javascript/dhx/dhtmlxtoolbar.js" />"></script>

<script src="<c:url value="/resources/javascript/dhx/dhtmlxgrid.js" />"></script>
<script src="<c:url value="/resources/javascript/dhx/dhtmlxgridcell.js" />"></script>
<script src="<c:url value="/resources/javascript/dhx/dhtmlxgrid_export.js" />"></script>
<script src="<c:url value="/resources/javascript/dhx/dhtmlxgrid_pgn.js" />"></script>
<script src="<c:url value="/resources/javascript/dhx/dhtmlxgrid_excell_link.js" />"></script>
<script src="<c:url value="/resources/javascript/dhx/dhtmlxgrid_excell_dhxcalendar.js" />"></script>
<script src="<c:url value="/resources/javascript/dhx/dhtmlxgrid_filter.js" />"></script>
<script src="<c:url value="/resources/javascript/dhx/dhtmlxgrid_srnd.js" />"></script>
<script src="<c:url value="/resources/javascript/dhx/dhtmlxgrid_json.js" />"></script>
<script src="<c:url value="/resources/javascript/dhx/dhtmlxgrid_mcol.js" />"></script>
<script src="<c:url value="/resources/javascript/dhx/dhtmlxgrid_group.js" />"></script>
<script src="<c:url value="/resources/javascript/dhx/dhtmlxgrid_splt.js" />"></script>
<script src="<c:url value="/resources/javascript/dhx/dhtmlxgrid_excell_sub_row.js" />"></script>
<script src="<c:url value="/resources/javascript/dhx/dhtmlxform.js" />"></script>
<script src="<c:url value="/resources/javascript/dhx/dhtmlxaccordion.js" />"></script>
<script src="<c:url value="/resources/javascript/dhx/dhtmlxcalendar.js" />"></script>
<script src="<c:url value="/resources/javascript/dhx/dhtmlxwindows.js" />"></script>
