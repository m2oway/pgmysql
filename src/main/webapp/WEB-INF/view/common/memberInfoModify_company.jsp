<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>회원정보 수정</title>
		<%@include file="/WEB-INF/view/include/head_resource.jsp" %>
	</head>
	<body>
		<div id="container">
			<!--topWarapper-->
			<%@include file="/WEB-INF/view/include/topWrapper.jsp" %>
			
			<div id="center">
				<input type="hidden" id="result" value="${result}"/><!-- 수정처리 결과 (성공:success) -->
				<div id="body">
					<div class="body2">
						<span>회원정보 수정</span>
						<div class="ne">
							<div class="ne1">
								<form:form id="form" action="/common/memberInfoModify_company" method="post" modelAttribute="memberJoinFormBean">
<%-- 								<form:form id="form" action="/common/memberJoinForm" method="post" modelAttribute="memberJoinFormBean"> --%>
									<div class="ne1">
										<div>
											<span class="r_ao4">아이디(ID)</span>
											<input type="hidden" name="ip_user_id" id="ip_user_id" value="${companyMemberInfo.login_id}" />
											&nbsp;<span class="r_ao4">&nbsp;:&nbsp;<c:out value="${companyMemberInfo.login_id}"/></span>
										</div>
										<div class="m_1">
											<span class="r_ao4">담당자명(실명)</span>
											<form:input path="ip_user_name" class="add_in4" value="${companyMemberInfo.dev_name}" />
										</div>
										<div class="m_1">
											<span class="r_ao4">닉네임</span>
											<form:input path="ip_nick_name" class="add_in4" id="ip_nick_name" value="${companyMemberInfo.user_nick_name}" />
											<span class="com_bt"><a id="writeString" class="textLink2" href="<c:url value="/common/userNickNameChk" />">중복확인<h5>중복확인</h5></a></span><span id="duplicate_nickName"></span>
											<form:hidden path="hd_user_nickName" id="hd_user_nickName" value="${companyMemberInfo.user_nick_name}"/>
											<form:hidden path="hd_user_nickName_cnt" id="hd_user_nickName_cnt"/>
										</div>
										<div class="sex">
											<span>성별</span>
											<input type="radio" name="rb_sex" id="rb_sex1" value="M" <c:if test="${companyMemberInfo.sex_dvsn eq 'M'}">checked='checked'</c:if>/>남&nbsp;&nbsp;
											<input type="radio" name="rb_sex" id="rb_sex2" value="W" <c:if test="${companyMemberInfo.sex_dvsn eq 'W'}">checked='checked'</c:if>/>여
<%-- 											<form:radiobutton path="rb_sex" name="rb_sex" id="rb_sex1" value="M" label="남" <c:if test="${companyMemberInfo.sex_dvsn eq 'M'}">checked="checked"</c:if> /> --%>
<%-- 											<form:radiobutton path="rb_sex" name="rb_sex" id="rb_sex2" value="W" label="여" <c:if test="${companyMemberInfo.sex_dvsn eq 'W'}">checked="checked"</c:if> /> --%>
										</div>
										<div class="m_1">
											<span class="r_ao4">업체명</span>
											<form:input path="ip_company_name" id="ip_company_name" class="add_in4" value="" />
										</div>
										<div class="m_1">
											<span class="r_ao4">사업자등록 번호</span>
											<form:input path="ip_Business_number" id="ip_Business_number" class="add_in4" value="" />
										</div>
<!-- 										<div class="m_1"> -->
<%-- 											<span class="r_ao4">비밀번호 확인</span> --%>
<%-- 											<form:password path="ip_password" id="ip_password" class="add_in4" /> --%>
<!-- 										</div> -->
										<div class="m_1">
											<ul>
												<li class="m_t">
													<span class="r_ao5">연락처</span>
												</li>
												<li class="line"></li>
												<li class="m_1">
													<span class="r_ao4">회사 전화번호</span>
													<select name="sl_tel_number1" id="sl_tel_number1" class="add_in4">
														<option value="02" <c:if test="${companyMemberInfo.dev_tel_no1 eq '02'}">selected='selected'</c:if>>02</option>
														<option value="031" <c:if test="${companyMemberInfo.dev_tel_no1 eq '031'}">selected='selected'</c:if>>031</option>
														<option value="032" <c:if test="${companyMemberInfo.dev_tel_no1 eq '032'}">selected='selected'</c:if>>032</option>
														<option value="033" <c:if test="${companyMemberInfo.dev_tel_no1 eq '033'}">selected='selected'</c:if>>033</option>
														<option value="041" <c:if test="${companyMemberInfo.dev_tel_no1 eq '041'}">selected='selected'</c:if>>041</option>
														<option value="042" <c:if test="${companyMemberInfo.dev_tel_no1 eq '042'}">selected='selected'</c:if>>042</option>
														<option value="043" <c:if test="${companyMemberInfo.dev_tel_no1 eq '043'}">selected='selected'</c:if>>043</option>
														<option value="051" <c:if test="${companyMemberInfo.dev_tel_no1 eq '051'}">selected='selected'</c:if>>051</option>
														<option value="052" <c:if test="${companyMemberInfo.dev_tel_no1 eq '052'}">selected='selected'</c:if>>052</option>
														<option value="053" <c:if test="${companyMemberInfo.dev_tel_no1 eq '053'}">selected='selected'</c:if>>053</option>
														<option value="054" <c:if test="${companyMemberInfo.dev_tel_no1 eq '054'}">selected='selected'</c:if>>054</option>
														<option value="055" <c:if test="${companyMemberInfo.dev_tel_no1 eq '055'}">selected='selected'</c:if>>055</option>
														<option value="061" <c:if test="${companyMemberInfo.dev_tel_no1 eq '061'}">selected='selected'</c:if>>061</option>
														<option value="062" <c:if test="${companyMemberInfo.dev_tel_no1 eq '062'}">selected='selected'</c:if>>062</option>
														<option value="063" <c:if test="${companyMemberInfo.dev_tel_no1 eq '063'}">selected='selected'</c:if>>063</option>
														<option value="064" <c:if test="${companyMemberInfo.dev_tel_no1 eq '064'}">selected='selected'</c:if>>064</option>
														<option value="070" <c:if test="${companyMemberInfo.dev_tel_no1 eq '070'}">selected='selected'</c:if>>070</option>
													</select>
													<span>-</span>
													<form:input path="ip_tel_number2" id="ip_tel_number2" class="add_in4" maxlength="4" value="${companyMemberInfo.dev_tel_no2}" />
													<span>-</span>
													<form:input path="ip_tel_number3" id="ip_tel_number3" class="add_in4" maxlength="4" value="${companyMemberInfo.dev_tel_no3}" />
												</li>
												<li class="m_1">
													<span class="r_ao4">휴대전화번호</span>
													<form:hidden path="dev_mobl_no" id="dev_mobl_no" value="${companyMemberInfo.dev_mobl_no}"/>
													<select name="sl_mobile_number1" id="sl_mobile_number1" class="add_in4">
														<option value="010" <c:if test="${companyMemberInfo.dev_mobl_no1 eq '010'}">selected='selected'</c:if>>010</option>
														<option value="011" <c:if test="${companyMemberInfo.dev_mobl_no1 eq '011'}">selected='selected'</c:if>>011</option>
														<option value="016" <c:if test="${companyMemberInfo.dev_mobl_no1 eq '016'}">selected='selected'</c:if>>016</option>
														<option value="017" <c:if test="${companyMemberInfo.dev_mobl_no1 eq '017'}">selected='selected'</c:if>>017</option>
														<option value="018" <c:if test="${companyMemberInfo.dev_mobl_no1 eq '018'}">selected='selected'</c:if>>018</option>
														<option value="019" <c:if test="${companyMemberInfo.dev_mobl_no1 eq '019'}">selected='selected'</c:if>>019</option>
													</select>
													<span>-</span>
													<form:input path="ip_mobile_number2" id="ip_mobile_number2" class="add_in4" maxlength="4" value="${companyMemberInfo.dev_mobl_no2}" />
													<span>-</span>
													<form:input path="ip_mobile_number3" id="ip_mobile_number3" class="add_in4" maxlength="4" value="${companyMemberInfo.dev_mobl_no3}" />
													<form:hidden path="hd_mobile_cert" id="hd_mobile_cert"/>
													<span class="com_bt" id="com_bt_mobile">휴대폰 인증<h5>휴대폰 인증</h5></span>
												</li>
												<li class="m_2">
													<span class="r_ao4">e-mail</span>
													<form:input path="ip_email1" id="ip_email1" class="add_in4" value="${companyMemberInfo.dev_email1_id}" />
													<span>@</span>
													<form:select path="sl_email2" id="sl_email2" class="add_in4">
														<form:option value="" label="--선택--" />
														<form:option value="naver.com" label="naver.com" />
														<form:option value="daum.net" label="daum.net" />
														<form:option value="nate.com" label="nate.com" />
														<form:option value="gmail.com" label="gmail.com" />
													</form:select>
													<form:checkbox path="ck_direct_email" id="ck_direct_email" value="direct" label="직접입력" />
													<form:input path="ip_direct_email" id="ip_direct_email" class="add_in4" value="${companyMemberInfo.dev_email1_domain}" />
												</li>
												<li class="m_t">
													<span class="r_ao5">회사 주소</span>
												</li>
												<li class="line"></li>
												<li class="m_1">
													<span class="r_ao4">우편번호</span>
													<form:hidden path="zipcode_id" id="zipcode_id" value="${companyMemberInfo.dev_zipcode_id}" />
													<form:input path="ip_post_number1" id="ip_post_number1" class="add_in4" value="${companyMemberInfo.zipcode1}" readOnly="readOnly" />
													<span>-</span>
													<form:input path="ip_post_number2" id="ip_post_number2" class="add_in4" value="${companyMemberInfo.zipcode2}" readOnly="readOnly" />
													<span class="com_bt-3" id="com_bt-3"><h5>찾아보기</h5></span>
												</li>
												<li class="m_1">
													<form:input path="ip_address" id="ip_address" class="add_in4" value="${companyMemberInfo.zip_addr}" readOnly="readOnly" />
												</li> 
												<li class="m_1">
													<span class="r_ao4">상세주소</span>
													<form:input path="ip_detail_address" id="ip_detail_address" class="add_in4" value="${companyMemberInfo.dev_addr_detl}" />
												</li>
											</ul>
										</div>
										
									</div>
									
									<div id="stap_bt">
										<div id="on_submit" class="stap_btnext"><h5>확인</h5></div>
									</div>
								</form:form>
								
							</div>
						</div>
					</div>
				</div>
			
			</div>
			
			<!--btmWarpper-->
			<%@include file="/WEB-INF/view/include/btmWrapper.jsp" %>
			<%@include file="/WEB-INF/view/popup/addresspop.jsp" %>
			<%@include file="/WEB-INF/view/include/mobileCertificationPop.jsp" %>
			
		</div>
		
<script>
	$("#ip_birthday").datepicker({dateFormat:"yy-mm-dd"});
	
	$("#ip_direct_email").hide();
	
	$(document).ready(function(){
		var email2 = $("#ip_direct_email").val();
		$("#sl_email2").val($("#ip_direct_email").val());
		
		if($("#sl_email2").val() == email2){
			$("#ip_direct_email").val("");
		}else{
			$("#ck_direct_email").attr("checked", true);
			$("#ip_direct_email").show();
			$("#ip_direct_email").val(email2);
// 			$("#sl_email2").val("");
			$("#sl_email2").attr("disabled", true);
		}
		
		if($("#result").val() == "success"){
			alert("회원정보가 수정되었습니다.");
			$("#result").val("");
			location.href="<c:url value='/'/>";	//'IT Players' 메인화면으로 이동
		}
	});
	
	//닉네임 중복 확인
	$("a.textLink2").click(function(){
		
		if($.trim($("#ip_nick_name").val()) == ""){
			alert("닉네임을 입력하십시오.");
			$("#ip_nick_name").focus();
			$("#duplicate_nickName").html("");
			return false;
		}
		
		if($.trim($("#ip_nick_name").val()) == $.trim($("#hd_user_nickName").val())){
			alert("현재 회원님께서 사용중이신 닉네임 입니다.");
			return false;
		}
		
		var link = $(this);
		$("#ip_nick_name").val($.trim($("#ip_nick_name").val()));
		$("#hd_user_nickName").val($.trim($("#ip_nick_name").val()));
		
		$.ajax({
			url: link.attr("href")
			,type: "POST"
			,data: $('#form').serialize()
			,dataType: "json"
			,success: function(data){
					$("#hd_user_nickName_cnt").val(data);
					if(data == 0){
						$("#duplicate_nickName").html("사용할 수 있는 닉네임 입니다.");
					}else{
						$("#duplicate_nickName").html("현재 사용중인 닉네임 입니다.");
					}
				}
			,error: function(xhr){
					alert(xhr.status);
				}
		});
		return false;
	});
	
	//이메일 직접입력
	$("#ck_direct_email").click(function(){
		if($("#ck_direct_email:checked").val() == "direct"){	//활성화 (직접입력)
			$("#ip_direct_email").show();
			$("#sl_email2").val("");
			$("#sl_email2").attr("disabled", true);
		}else{
			$("#ip_direct_email").hide();
			$("#ip_direct_email").val("");
			$("#sl_email2").attr("disabled", false);
		}
	});
	
	//정보 수정
	$("#on_submit").click(function(){
			
		if($.trim($("#ip_user_name").val()) == ""){
			alert("이름(실명)을 입력하십시오.");
			$("#ip_user_name").focus();
			return false;
		}
		
		if($.trim($("#ip_nick_name").val()) == ""){
			alert("닉네임을 입력하십시오.");
			$("#ip_nick_name").focus();
			return false;
		}
		
		if($.trim($("#ip_nick_name").val()) != $.trim($("#hd_user_nickName").val())){
			if($("#hd_user_nickName").val() == "" || $("#hd_user_nickName").val() != $("#ip_nick_name").val() || $("#hd_user_nickName_cnt").val() == ""){
				alert("닉네임 중복확인을 하십시오.");
				$("#ip_nick_name").focus();
				return false;
			}	
			if($("#hd_user_nickName_cnt").val() > 0){
				alert("현재 사용중인 닉네임 입니다. 다시 입력 하십시오.");
				$("#ip_nick_name").focus();
				return false;
			}
		}
		
		if($("input:radio[name=rb_sex]:checked").val() == undefined){
			alert("성별을 선택하십시오.");
			return false;
		}
		
		if($.trim($("#ip_company_name").val()) == ""){
			alert("업체명을 입력하십시오.");
			$("#ip_company_name").focus();
			return false;
		}
		
		if($.trim($("#ip_Business_number").val()) == ""){
			alert("사업자등록 번호를 입력하십시오.");
			$("#ip_Business_number").focus();
			return false;
		}
		
		if($.trim($("#ip_tel_number2").val()) == "" || $.trim($("#ip_tel_number3").val()) == ""){
			alert("전화번호를 입력하십시오.");
			return false;
		}
		if(isNaN($("#ip_tel_number2").val()) || isNaN($("#ip_tel_number3").val())){
			alert("전화번호엔 숫자값만 입력하십시오.");
			return false;
		}
		
		if($.trim($("#ip_mobile_number2").val()) == "" || $.trim($("#ip_mobile_number3").val()) == ""){
			alert("휴대전화번호를 입력하십시오.");
			return false;
		}
		if(isNaN($("#ip_mobile_number2").val()) || isNaN($("#ip_mobile_number3").val())){
			alert("휴대전화번호엔 숫자값만 입력하십시오.");
			return false;
		}
		//기존에 등록된(인증받은) 휴대폰 번호와 입력된 휴대폰 번호가 다를경우
		if($("#dev_mobl_no").val() != $("#sl_mobile_number1").val() + "-" + $.trim($("#ip_mobile_number2").val()) + "-" + $.trim($("#ip_mobile_number3").val())){
			if($("#hd_mobile_cert").val() == "" || $("#hd_mobile_cert").val() != "true"){
				alert("휴대전화 인증을 받으십시오.");
				return false;
			}
		}
		
		if($.trim($("#ip_email1").val()) == ""){
			alert("이메일을 입력하십시오.");
			$("#ip_email1").focus();
			return false;
		}
		if($("#sl_email2").val() == "" && $("#ck_direct_email:checked").val() == undefined){
			alert("이메일을 입력하십시오.");
			return false;
		}
		if($.trim($("#ip_direct_email").val()) == "" && $("#ck_direct_email:checked").val() == "direct"){
			alert("이메일을 입력하십시오.");
			return false;
		}
		
		if($("#ip_post_number1").val() == "" || $("#ip_post_number2").val() == ""){
			alert("우편번호를 입력하십시오.");
			return false;
		}
		if($("#ip_address").val() == ""){
			alert("주소를 입력하십시오.");
			return false;
		}
			
// 		alert($("#form").serialize());
			
		if(confirm("회원정보 수정을 하시겠습니까?")){
			
			$("#form").submit();
			return false;
			
		}
	});
</script>

</body>
</html>