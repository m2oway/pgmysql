<%@page contentType="text/html; charset=euc-kr" %>
<%
	java.util.Properties prop = (java.util.Properties) session.getAttribute("LoginInfo");
	
	String __ses_empId__	= "";
	String __ses_empName__	= "";
	String __ses_adminYn__	= "";
	String __ses_deptCd__	= "";
	String __ses_deptName__	= "";
	String __ses_userRghtId__ = "";
	String __ses_comemp_dept_id__ = "";

	if(prop != null){
		__ses_empId__	 = prop.getProperty("__ses_empId__");
	    __ses_empName__	 = prop.getProperty("__ses_empName__");
	    __ses_adminYn__	 = prop.getProperty("__ses_adminYn__");
	    __ses_deptCd__	 = prop.getProperty("__ses_deptCd__");
	    __ses_deptName__ = prop.getProperty("__ses_deptName__");
	    __ses_userRghtId__ = prop.getProperty("__ses_userRghtId__");
	    
		prop.setProperty("__ses_empId__",	 __ses_empId__);
	    prop.setProperty("__ses_empName__",  __ses_empName__);
	    prop.setProperty("__ses_adminYn__",  __ses_adminYn__);
		prop.setProperty("__ses_deptCd__",	 __ses_deptCd__);
	    prop.setProperty("__ses_deptName__", __ses_deptName__);
	    prop.setProperty("__ses_userRghtId__", __ses_userRghtId__);
	    
    }else{%>
		<script type="text/javascript" src="/js/common.js"></script>
		<script>
			var iUrl	= "/common/sessionScreen.do";
			var map		= new iutil.HashMap();
			var args	= new Object();
			var iWidth	= 600;
			var iHeight	= 280;
			var iScoll	= "Y"; // default:no
	
			rtnVal = parent.iwindow.ModalDialog(iUrl, args, map, iWidth, iHeight, iScoll);
// 			location.href="login/loginForm.do";
		</script>
<%	}%>