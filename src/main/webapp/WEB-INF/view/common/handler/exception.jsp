<%@ page contentType="text/html; charset=euc-kr" %>
<!-- start : title -->
<!-- end : title -->
<%
	java.lang.Exception e = (java.lang.Exception)request.getAttribute("exception");
	String exMsg = "";
	String urlAddr = "";
	
	if(e != null){
		exMsg = e.getMessage();
	}else{
		if(request.getAttribute("exMsg") != null){
			exMsg = java.net.URLDecoder.decode((String)request.getAttribute("exMsg"));
			urlAddr = java.net.URLDecoder.decode((String)request.getAttribute("urlAddr"));
		}else{
			exMsg = "시스템 에러입니다.";
		}
	}
%>
<!-- start : 상단 탑메뉴 -->
<%-- 	<%@ include file="../include/subtopmenu.jsp"%> --%>
<!-- end : 상단 탑메뉴 -->
<!-- start : 서브 컨텐츠 -->

<input type="Hidden" name="frameYn" value="">
<span id="ctgCondition"></span>
<br style="line-height:15px;" />
<table width="1024" border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td width="200" valign="top" style="padding:0 0 0 10px;">
<!-- start : 왼쪽 메뉴 영역 -->		
<%-- 		<%@ include file="../include/left/left.jsp"%> --%>
<!-- end : 왼쪽 메뉴 영역 -->	<!-- end : 왼쪽 메뉴 영역 -->		</td>
        <td width="814" valign="top">

    <table width="786" border="0" height="537px" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center" valign="middle">


<table width="600" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="18" height="18"><img src="/images/common/error_cor_tl.gif" /></td>
        <td background="/images/common/error_cor_t.gif"></td>
        <td width="18"><img src="/images/common/error_cor_tr.gif" /></td>
    </tr>
    <tr>
        <td background="/images/common/error_cor_l.gif"></td>
        <td class="cor"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="right"><img src="/images/sub/error_img03.gif" /></td>
            </tr>
            <tr>
                <td style="padding:0 38px 0 38px;">
				
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
	                    <tr>
	                        <td width="12" height="12"><img src="/images/common/error_cor_tl_s.gif" /></td>
	                        <td background="/images/common/error_cor_t_s.gif"></td>
	                        <td width="12"><img src="/images/common/error_cor_tr_s.gif" /></td>
	                    </tr>
	                    <tr>
	                        <td background="/images/common/error_cor_l_s.gif"></td>
	                        <td align="center" bgcolor="#FFFFFF" style="padding:10px 0 10px 0;">
								<div style="overflow:auto; width: 100%;height: 51;scrolling-x:yes;">
								<table width="80%" border="0" cellspacing="0" cellpadding="0">
		                            <tr>
		                                <td><%=exMsg%></td>
		                            </tr>
								</table>
								</div>
							</td>
	                        <td background="/images/common/error_cor_r_s.gif"></td>
	                    </tr>
	                    <tr>
	                        <td height="12"><img src="/images/common/error_cor_bl_s.gif" /></td>
	                        <td background="/images/common/error_cor_b_s.gif"></td>
	                        <td><img src="/images/common/error_cor_br_s.gif" /></td>
	                    </tr>
	                </table>
				</td>
            </tr>
			<tr>
				<td align="center">
					<table width="80%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="center"><br>관리자에게 문의바랍니다.<br>Tel : 000-000-0000</td>
						</tr>
						<tr>
							<td align="center" style="padding:10px 0 0 0;">
<!-- 								<img src="/images/btn/btn_prev.gif" hspace="1" border="0" style="cursor:pointer" onclick="history.back()" /> -->
								<img src="/images/btn/btn_main.gif" hspace="1" border="0" style="cursor:pointer" onclick="location.href='<%=urlAddr%>'" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
        </table></td>
        <td background="/images/common/error_cor_r.gif"></td>
    </tr>
    <tr>
        <td height="18"><img src="/images/common/error_cor_bl.gif" /></td>
        <td background="/images/common/error_cor_b.gif"></td>
        <td><img src="/images/common/error_cor_br.gif" /></td>
    </tr>
</table>
<br><br><br>
                   
            </td>
        </tr>
    </table>
		</td>
    </tr>
</table>

<!-- end : 서브 컨텐츠 -->
<iframe name="submitFrame" frameborder="1" marginheight="0" marginwidth="0" style="display:none" width="1005" height="800"></iframe>
<!-- start : copyright -->
<!-- end : copyright  -->
</body>
</html>