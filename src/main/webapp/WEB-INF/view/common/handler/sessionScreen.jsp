<%@ page contentType="text/html; charset=euc-kr" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>프리미엄 에셋 Agent Total Information System</title> 
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr">
<link href="/css/css.css" rel="stylesheet" type="text/css"/>
<base target=_self/>
</head>
<body MS_POSITIONING="GridLayout" leftmargin="0" topmargin="0" class="top">
<form name="frm" method="post" target="modalTarget" runat="server" enctype="multipart/form-data">
<!-- start : 서브 컨텐츠 -->
<table width="600" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="18" height="18"><img src="/images/common/error_cor_tl.gif" /></td>
        <td background="/images/common/error_cor_t.gif"></td>
        <td width="18"><img src="/images/common/error_cor_tr.gif" /></td>
    </tr>
    <tr>
        <td background="/images/common/error_cor_l.gif"></td>
        <td class="cor"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="right"><img src="/images/sub/error_img02.gif" /></td>
            </tr>
            <tr>
                <td style="padding:0 38px 0 38px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="12" height="12"><img src="/images/common/error_cor_tl_s.gif" /></td>
                        <td background="/images/common/error_cor_t_s.gif"></td>
                        <td width="12"><img src="/images/common/error_cor_tr_s.gif" /></td>
                    </tr>
                    <tr>
                        <td background="/images/common/error_cor_l_s.gif"></td>
                        <td align="center" bgcolor="#FFFFFF" style="padding:10px 0 10px 0;"><table width="80%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center">시간초과로 인해 접속이 끊어졌습니다.<br />
다시 로그인 하신후 사용하시기 바랍니다.
</td>
                            </tr>
                            <tr>
                                <td align="center" style="padding:10px 0 0 0;"><img src="/images/btn/btn_close.gif" hspace="1" border="0" style="cursor:pointer" onclick="window.close();" /></td>
                            </tr>
                        </table></td>
                        <td background="/images/common/error_cor_r_s.gif"></td>
                    </tr>
                    <tr>
                        <td height="12"><img src="/images/common/error_cor_bl_s.gif" /></td>
                        <td background="/images/common/error_cor_b_s.gif"></td>
                        <td><img src="/images/common/error_cor_br_s.gif" /></td>
                    </tr>
                </table></td>
            </tr>
        </table></td>
        <td background="/images/common/error_cor_r.gif"></td>
    </tr>
    <tr>
        <td height="18"><img src="/images/common/error_cor_bl.gif" /></td>
        <td background="/images/common/error_cor_b.gif"></td>
        <td><img src="/images/common/error_cor_br.gif" /></td>
    </tr>
</table>
</body>
</html>