<%@page contentType="text/html; charset=euc-kr" %>
<form name="frm">
<%
	java.util.Properties prop = (java.util.Properties) session.getAttribute("LoginInfo");
	
	String __ses_empId__	= "";
	String __ses_empName__	= "";
	String __ses_adminYn__	= "";
	String __ses_deptCd__	= "";
	String __ses_deptName__	= "";
	
	String url = request.getParameter("url");
	String menuId[] = url.split("=");
	if(menuId != null){
		if(menuId.length > 1){%>
			<input type="hidden" name="menuId" value="<%=menuId[1]%>">
<%		}
	}
	String[] treeIdx = request.getParameterValues("treeIdx");
	if(treeIdx != null){
		for(int i=0 ; i < treeIdx.length ; i++){
//			System.out.println("2▶▶▶▶▶▶▶" + treeIdx[i]);%>
			<input type="hidden" name="treeIdx" value="<%=treeIdx[i]%>">
<%		}
	}
	
	if(prop != null){
		__ses_empId__	 = prop.getProperty("__ses_empId__");
	    __ses_empName__	 = prop.getProperty("__ses_empName__");
	    __ses_adminYn__	 = prop.getProperty("__ses_adminYn__");
	    __ses_deptCd__	 = prop.getProperty("__ses_deptCd__");
	    __ses_deptName__ = prop.getProperty("__ses_deptName__");

		prop.setProperty("__ses_empId__",	 __ses_empId__);
	    prop.setProperty("__ses_empName__",  __ses_empName__);
	    prop.setProperty("__ses_adminYn__",  __ses_adminYn__);
	    prop.setProperty("__ses_deptCd__",	 __ses_deptCd__);
	    prop.setProperty("__ses_deptName__", __ses_deptName__);
		
		//response.sendRedirect(url + "&treeIdx=" + treeIdx + "&treeIdx=" + treeIdx);	%>
		<script>
			frm.action = "<%=menuId[0]%>";
 			frm.target = "_self";
  			frm.submit();
 		</script>
<%		return;
    }else{%>
		<script type="text/javascript" src="/js/common.js"></script>
		<script>
		<%if(!"/login/loginForm.do".equals(url)){%>
			var iUrl	= "/common/sessionScreen.do";
			var map		= new iutil.HashMap();
			var args	= new Object();
			var iWidth	= 600;
			var iHeight	= 280;
			var iScoll	= "Y"; // default:no
	
			rtnVal = parent.iwindow.ModalDialog(iUrl, args, map, iWidth, iHeight, iScoll);
			//alert("세션이 끊겼습니다.\n다시 로그인해주세요.");
		<%}%>
			location.href="/login/loginForm.do";
		</script>
<%	}%>
</form>