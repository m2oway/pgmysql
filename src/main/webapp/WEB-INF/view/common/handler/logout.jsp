<meta http-equiv="Refresh" content="1;url=/login/loginForm.do">
<%@page contentType="text/html; charset=euc-kr" %>
<%@ page import="java.util.*"%>
<%
	/**
	 * 업 무 명 : 로그아룻
	 * 작 성 자 : 김재호(ktinybird@nate.com)
	 * 작 성 일 : 2010/04/25
	 * 내    용 : 시스템의 로그아웃 처리를 한다.
	 **/
	 
	HttpSession _session = request.getSession(false);
	_session.removeAttribute("LoginInfo");
	_session.invalidate();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">



<html xmlns="http://www.w3.org/1999/xhtml">

	<script language="javascript">window.name = "modalTarget";</script>
	<head>
		<!-- title>한국전력기술(주)____Global Top 5 Power EPC Leader. "..."..."..."..."</title--> 
		<title>Accounts Receivable Settlement Service</title> 
		<base target=_self>
		<script><!--
		//
			function logout(){
				alert("정상적으로 로그아웃 처리되었습니다.");
//				frm.action = "/login/main.do";
//				frm.target = "modalTarget";
//				frm.submit();
				window.close();
			}
		//
		--></script>
	</head>

	<body MS_POSITIONING="GridLayout">
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<center><h1>정상적으로 로그아웃 되었습니다.</h1></center>
		<form name="frm" method="post" target="modalTarget" runat="server">
			<input type="hidden" name="loginActionYn" value="Y">
		</form>
	</body>
</html>