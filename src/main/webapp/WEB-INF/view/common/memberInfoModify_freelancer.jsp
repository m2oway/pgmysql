<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>회원정보 수정</title>
		<%@include file="/WEB-INF/view/include/head_resource.jsp" %>
	</head>
	<body>
		<div id="container">
			<!--topWarapper-->
			<%@include file="/WEB-INF/view/include/topWrapper.jsp" %>
			
			<div id="center">
<%-- 				<div>====[ ${error} ]===</div> --%>
				<input type="hidden" id="result" value="${result}"/><!-- 수정처리 결과 (성공:success) -->
				<div id="body">
					<div class="body2">
						<span>회원정보 수정</span>
						<div class="ne">
							<form:form id="form" action="/common/memberInfoModify_freelancer" method="post" modelAttribute="memberJoinFormBean">
								<div class="member-body-1">
									<div class="member-box2">
										<ul>
											<li class="member-nameinput">
												<span class="member-name">아이디(ID)</span>
												&nbsp;<span class="r_ao4">&nbsp;:&nbsp;${freeMemberInfo.login_id}</span>
											</li>
											<li class="member-nameinput">
												<span class="member-name">이름(실명)</span>
												<input class="member-inputtext" type="text" name="ip_user_name" id="ip_user_name" value="${freeMemberInfo.dev_name}" alt="name"/>
											</li>
											<li class="member-nameinput">
												<span class="member-name">닉네임</span>
												<input class="member-inputtext" type="text" name="ip_nick_name" id="ip_nick_name" value="${freeMemberInfo.user_nick_name}" alt="name2"/>
												<span class="member-name-bt"><a id="writeString" class="textLink2" href="<c:url value="/common/userNickNameChk" />">중복확인</a></span>
												<span id="duplicate_nickName"></span>
												<input type="hidden" name="hd_user_nickName" id="hd_user_nickName" value="${freeMemberInfo.user_nick_name}"/>
												<input type="hidden" name="hd_user_nickName_cnt" id="hd_user_nickName_cnt"/>
											</li>
											<li class="member-nameinput">
												<span class="member-name">성별</span>
												<input class="member-radio" type="radio" name="rb_sex" id="rb_sex1" value="10" <c:if test="${freeMemberInfo.sex_dvsn eq '10'}">checked='checked'</c:if> alt="남"/>
												<span class="member-name-s">남</span>
												<input class="member-radio" type="radio" name="rb_sex" id="rb_sex2" value="20" <c:if test="${freeMemberInfo.sex_dvsn eq '20'}">checked='checked'</c:if> alt="여"/>
												<span class="member-name-s">여</span>
											</li>
											<li class="member-nameinput">
												<span class="member-name">생년월일</span>
												<input class="member-inputtext" type="text" name="ip_birthday" id="ip_birthday" value="${freeMemberInfo.dev_brth_date}" readOnly="readOnly" alt="birthday"/>
<%-- 												<span class="member-calendar-bt">달력</span> --%>
												<input class="member-radio" type="radio" name="rb_calendar_dvsn" id="rb_calendar_dvsn1" value="solar" <c:if test="${freeMemberInfo.dev_brth_dvsn eq 'solar'}">checked='checked'</c:if> alt="남"/>
												<span class="member-name-s">양력</span>
												<input class="member-radio" type="radio" name="rb_calendar_dvsn" id="rb_calendar_dvsn2" value="lunar" <c:if test="${freeMemberInfo.dev_brth_dvsn eq 'lunar'}">checked='checked'</c:if> alt="여"/>
												<span class="member-name-s">음력</span>
											</li>
<!-- 											<li class="member-nameinput"> -->
<%-- 												<span class="member-name">비밀번호</span> --%>
<!-- 												<input class="member-inputtext" type="text" alt="pw"/> -->
<!-- 											</li> -->
<!-- 											<li class="member-nameinput"> -->
<%-- 												<span class="member-name">비밀번호 확인</span> --%>
<!-- 												<input class="member-inputtext" type="text" alt="pw1"/> -->
<!-- 											</li> -->
										</ul>
									</div>
								</div>
								<div class="member-sub-title"><span>*</span>연락처</div>
								<div class="member-body-1">
									<div class="member-box2">
										<ul>
											<li class="member-nameinput">
												<span class="member-name">전화번호</span>
												<select name="sl_tel_number1" id="sl_tel_number1" class="member-phone-select">
													<option value="02" <c:if test="${freeMemberInfo.dev_tel_no1 eq '02'}">selected='selected'</c:if>>02</option>
													<option value="031" <c:if test="${freeMemberInfo.dev_tel_no1 eq '031'}">selected='selected'</c:if>>031</option>
													<option value="032" <c:if test="${freeMemberInfo.dev_tel_no1 eq '032'}">selected='selected'</c:if>>032</option>
													<option value="033" <c:if test="${freeMemberInfo.dev_tel_no1 eq '033'}">selected='selected'</c:if>>033</option>
													<option value="041" <c:if test="${freeMemberInfo.dev_tel_no1 eq '041'}">selected='selected'</c:if>>041</option>
													<option value="042" <c:if test="${freeMemberInfo.dev_tel_no1 eq '042'}">selected='selected'</c:if>>042</option>
													<option value="043" <c:if test="${freeMemberInfo.dev_tel_no1 eq '043'}">selected='selected'</c:if>>043</option>
													<option value="051" <c:if test="${freeMemberInfo.dev_tel_no1 eq '051'}">selected='selected'</c:if>>051</option>
													<option value="052" <c:if test="${freeMemberInfo.dev_tel_no1 eq '052'}">selected='selected'</c:if>>052</option>
													<option value="053" <c:if test="${freeMemberInfo.dev_tel_no1 eq '053'}">selected='selected'</c:if>>053</option>
													<option value="054" <c:if test="${freeMemberInfo.dev_tel_no1 eq '054'}">selected='selected'</c:if>>054</option>
													<option value="055" <c:if test="${freeMemberInfo.dev_tel_no1 eq '055'}">selected='selected'</c:if>>055</option>
													<option value="061" <c:if test="${freeMemberInfo.dev_tel_no1 eq '061'}">selected='selected'</c:if>>061</option>
													<option value="062" <c:if test="${freeMemberInfo.dev_tel_no1 eq '062'}">selected='selected'</c:if>>062</option>
													<option value="063" <c:if test="${freeMemberInfo.dev_tel_no1 eq '063'}">selected='selected'</c:if>>063</option>
													<option value="064" <c:if test="${freeMemberInfo.dev_tel_no1 eq '064'}">selected='selected'</c:if>>064</option>
													<option value="070" <c:if test="${freeMemberInfo.dev_tel_no1 eq '070'}">selected='selected'</c:if>>070</option>
												</select>
												<span class="member-minus">-</span>
												<input type="text" name="ip_tel_number2" id="ip_tel_number2" class="member-phonetext" maxlength="4" value="${freeMemberInfo.dev_tel_no2}" alt="phone-number"/>
												<span class="member-minus">-</span>
												<input type="text" name="ip_tel_number3" id="ip_tel_number3" class="member-phonetext" maxlength="4" value="${freeMemberInfo.dev_tel_no3}" alt="phone-number"/>
											</li>
											<li class="member-nameinput">
												<span class="member-name">휴대전화번호</span>
												<input type="hidden" name="dev_mobl_no" id="dev_mobl_no" value="${freeMemberInfo.dev_mobl_no}"/>
												<select name="sl_mobile_number1" id="sl_mobile_number1" class="member-phone-select">
													<option value="010" <c:if test="${freeMemberInfo.dev_mobl_no1 eq '010'}">selected='selected'</c:if>>010</option>
													<option value="011" <c:if test="${freeMemberInfo.dev_mobl_no1 eq '011'}">selected='selected'</c:if>>011</option>
													<option value="016" <c:if test="${freeMemberInfo.dev_mobl_no1 eq '016'}">selected='selected'</c:if>>016</option>
													<option value="017" <c:if test="${freeMemberInfo.dev_mobl_no1 eq '017'}">selected='selected'</c:if>>017</option>
													<option value="018" <c:if test="${freeMemberInfo.dev_mobl_no1 eq '018'}">selected='selected'</c:if>>018</option>
													<option value="019" <c:if test="${freeMemberInfo.dev_mobl_no1 eq '019'}">selected='selected'</c:if>>019</option>
												</select>
												<span class="member-minus">-</span>
												<input type="text" name="ip_mobile_number2" id="ip_mobile_number2" class="member-phonetext" maxlength="4" value="${freeMemberInfo.dev_mobl_no2}" alt="휴대전화번호"/>
												<span class="member-minus">-</span>
												<input type="text" name="ip_mobile_number3" id="ip_mobile_number3" class="member-phonetext" maxlength="4" value="${freeMemberInfo.dev_mobl_no3}" alt="휴대전화번호"/>
												<input type="hidden" name="hd_mobile_cert" id="hd_mobile_cert"/>
												<span class="member-name-bt phone" id="com_bt_mobile">휴대폰인증</span>
											</li>
											<li class="member-nameinput">
												<span class="member-name">E-mail</span>
												<input type="text" name="ip_email1" id="ip_email1" class="member-inputtext" value="${freeMemberInfo.dev_email1_id}" alt="E-mail"/>
												<span class="member-minus">@</span>
												<select name="sl_email2" id="sl_email2" class="member-e-mail-select">
													<option value="">--선택--</option>
													<option value="naver.com">naver.com</option>
													<option value="daum.net">daum.net</option>
													<option value="nate.com">nate.com</option>
													<option value="gmail.com">gmail.com</option>
												</select>
												<input type="checkbox" name="ck_direct_email" id="ck_direct_email" value="direct" alt="직접입력" />
												<span class="member-name-s">직접입력</span>
												<input type="text" name="ip_direct_email" id="ip_direct_email" class="member-inputtext" value="${freeMemberInfo.dev_email1_domain}"/>
											</li>
										</ul>
									</div>
								</div>
								<div class="member-sub-title"><span>*</span>주소</div>
								<div class="member-body-2">
									<div class="member-box2">
										<ul>
											<li class="member-nameinput">
												<span class="member-name">우편번호</span>
												<input type="hidden" name="zipcode_id" id="zipcode_id" value="${freeMemberInfo.dev_zipcode_id}"/>
												<input type="text" name="ip_post_number1" id="ip_post_number1" class="member-post-input" value="${freeMemberInfo.zipcode1}" readOnly="readOnly" alt="우편번호"/>
												<span class="member-minus">-</span>
												<input type="text" name="ip_post_number2" id="ip_post_number2" class="member-post-input" value="${freeMemberInfo.zipcode2}" readOnly="readOnly" alt="우편번호"/>
												<span class="member-calendar-bt" id="com_bt-3">검색</span>
											</li>
											<li class="member-nameinput">
												<span class="member-name">상세주소</span>
												<input type="text" name="ip_address" id="ip_address" class="member-address" value="${freeMemberInfo.zip_addr}" readOnly="readOnly" alt="상세주소"/>
											</li>
											<li class="member-nameinput">
												<input type="text" name="ip_detail_address" id="ip_detail_address" class="member-address1" value="${freeMemberInfo.dev_addr_detl}" alt="상세주소"/>
											</li>
										</ul>
									</div>
								</div>
								<div class="member-stap_bt">
									<span id="on_submit" class="member-stap_btnext">확인</span>
								</div>
							</form:form>
						</div>
					</div>
				</div>
			
			</div>
			
			<!--btmWarpper-->
			<%@include file="/WEB-INF/view/include/btmWrapper.jsp" %>
			<%@include file="/WEB-INF/view/popup/addresspop.jsp" %>
			<%@include file="/WEB-INF/view/include/mobileCertificationPop.jsp" %>
			
		</div>
		
<script>
	$.datepicker.regional['ko'] = {
		monthNames: ['1월(JAN)','2월(FEB)','3월(MAR)','4월(APR)','5월(MAY)','6월(JUN)','7월(JUL)','8월(AUG)','9월(SEP)','10월(OCT)','11월(NOV)','12월(DEC)'],
		monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
		dayNames: ['일','월','화','수','목','금','토'],
		dayNamesShort: ['일','월','화','수','목','금','토'],
		dayNamesMin: ['일','월','화','수','목','금','토'],
		weekHeader: 'Wk',
		dateFormat: 'yy-mm-dd',
		firstDay: 0,
		isRTL: false};
	$.datepicker.setDefaults($.datepicker.regional['ko']);
	
	$('#ip_birthday').datepicker({
		showOn:"button"
		,showAnim:"drop"	//달력보기 애니메이션(Drop)
		,dateFormat:"yy-mm-dd"	//가져올 날짜 형식
		,buttonImage:"../../../resources/images/calendar-bt.png"	//달력버튼 이미지URL
//			,buttonImage:"images/calendar.gif"
		,buttonImageOnly:true
		,changeMonth:true	//월 Select Box
		,changeYear:true	//년 Select Box
		,showMonthAfterYear:true 	//년도 표시가 왼쪽으로 나도로록 처리
	});
	
	$("#ip_direct_email").hide();
	
	$(document).ready(function(){
		var email2 = $("#ip_direct_email").val();
		$("#sl_email2").val($("#ip_direct_email").val());
		
		if($("#sl_email2").val() == email2){
			$("#ip_direct_email").val("");
		}else{
// 			$("#ck_direct_email").click();
			$("#ck_direct_email").attr("checked", true);
			$("#ip_direct_email").show();
			$("#ip_direct_email").val(email2);
// 			$("#sl_email2").val("");
			$("#sl_email2").attr("disabled", true);
		}
		
		if($("#result").val() == "success"){
			alert("회원정보가 수정되었습니다.");
			$("#result").val("");
			location.href="<c:url value='/'/>";	//'IT Players' 메인화면으로 이동
		}
	});
	
	//닉네임 중복 확인
	$("a.textLink2").click(function(){
		
		if($.trim($("#ip_nick_name").val()) == ""){
			alert("닉네임을 입력하십시오.");
			$("#ip_nick_name").focus();
			$("#duplicate_nickName").html("");
			return false;
		}
		
		if($.trim($("#ip_nick_name").val()) == $.trim($("#hd_user_nickName").val())){
			alert("현재 회원님께서 사용중이신 닉네임 입니다.");
			return false;
		}
		
		var link = $(this);
		$("#ip_nick_name").val($.trim($("#ip_nick_name").val()));
		$("#hd_user_nickName").val($.trim($("#ip_nick_name").val()));
		
		$.ajax({
			url: link.attr("href")
			,type: "POST"
			,data: $('#form').serialize()
			,dataType: "json"
			,success: function(data){
					$("#hd_user_nickName_cnt").val(data);
					if(data == 0){
						$("#duplicate_nickName").html("사용할 수 있는 닉네임 입니다.");
					}else{
						$("#duplicate_nickName").html("현재 사용중인 닉네임 입니다.");
					}
				}
			,error: function(xhr){
					alert(xhr.status);
				}
		});
		return false;
	});
	
	//이메일 직접입력
	$("#ck_direct_email").click(function(){
		if($("#ck_direct_email:checked").val() == "direct"){	//활성화 (직접입력)
			$("#ip_direct_email").show();
			$("#sl_email2").val("");
			$("#sl_email2").attr("disabled", true);
		}else{
			$("#ip_direct_email").hide();
			$("#ip_direct_email").val("");
			$("#sl_email2").attr("disabled", false);
		}
	});
	
	//정보 수정
	$("#on_submit").click(function(){
			
		if($.trim($("#ip_user_name").val()) == ""){
			alert("이름(실명)을 입력하십시오.");
			$("#ip_user_name").focus();
			return false;
		}
		
		if($.trim($("#ip_nick_name").val()) == ""){
			alert("닉네임을 입력하십시오.");
			$("#ip_nick_name").focus();
			return false;
		}
		
		if($.trim($("#ip_nick_name").val()) != $.trim($("#hd_user_nickName").val())){
			if($("#hd_user_nickName").val() == "" || $("#hd_user_nickName").val() != $("#ip_nick_name").val() || $("#hd_user_nickName_cnt").val() == ""){
				alert("닉네임 중복확인을 하십시오.");
				$("#ip_nick_name").focus();
				return false;
			}	
			if($("#hd_user_nickName_cnt").val() > 0){
				alert("현재 사용중인 닉네임 입니다. 다시 입력 하십시오.");
				$("#ip_nick_name").focus();
				return false;
			}
		}
		
		if($("input:radio[name=rb_sex]:checked").val() == undefined){
			alert("성별을 선택하십시오.");
			return false;
		}
		
		if($("#ip_birthday").val() == ""){
			alert("생년월일을 입력하십시오.");
			return false;
		}
		
		if($("input:radio[name=rb_calendar_dvsn]:checked").val() == undefined){
			alert("생년월일 양력,음력 구분을 선택하십시오.");
			return false;
		}
		
// 		if($("#ip_password").val() == ""){
// 			alert("정보변경 본인확인을 위한 기존 비밀번호를 입력하십시오.");
// 			$("#ip_password").focus();
// 			return false;
// 		}
		
		if($.trim($("#ip_tel_number2").val()) == "" || $.trim($("#ip_tel_number3").val()) == ""){
			alert("전화번호를 입력하십시오.");
			return false;
		}
		if(isNaN($("#ip_tel_number2").val()) || isNaN($("#ip_tel_number3").val())){
			alert("전화번호엔 숫자값만 입력하십시오.");
			return false;
		}
		
		if($.trim($("#ip_mobile_number2").val()) == "" || $.trim($("#ip_mobile_number3").val()) == ""){
			alert("휴대전화번호를 입력하십시오.");
			return false;
		}
		if(isNaN($("#ip_mobile_number2").val()) || isNaN($("#ip_mobile_number3").val())){
			alert("휴대전화번호엔 숫자값만 입력하십시오.");
			return false;
		}
		//기존에 등록된(인증받은) 휴대폰 번호와 입력된 휴대폰 번호가 다를경우
		if($("#dev_mobl_no").val() != $("#sl_mobile_number1").val() + "-" + $.trim($("#ip_mobile_number2").val()) + "-" + $.trim($("#ip_mobile_number3").val())){
			if($("#hd_mobile_cert").val() == "" || $("#hd_mobile_cert").val() != "true"){
				alert("휴대전화 인증을 받으십시오.");
				return false;
			}
		}
		
		if($.trim($("#ip_email1").val()) == ""){
			alert("이메일을 입력하십시오.");
			$("#ip_email1").focus();
			return false;
		}
		if($("#sl_email2").val() == "" && $("#ck_direct_email:checked").val() == undefined){
			alert("이메일을 입력하십시오.");
			return false;
		}
		if($.trim($("#ip_direct_email").val()) == "" && $("#ck_direct_email:checked").val() == "direct"){
			alert("이메일을 입력하십시오.");
			return false;
		}
		
		if($("#ip_post_number1").val() == "" || $("#ip_post_number2").val() == ""){
			alert("우편번호를 입력하십시오.");
			return false;
		}
		if($("#ip_address").val() == ""){
			alert("주소를 입력하십시오.");
			return false;
		}
			
// 		alert($("#form").serialize());
			
		if(confirm("회원정보 수정을 하시겠습니까?")){
			
			//데이터를 Bean으로 전송
// 			$.post($(this).attr("action"), $("#form").serialize(), function(html) {
// 				$("#form").replaceWith(html);
// 			});
			$("#form").submit();
			return false;
			
		}
	});
	
// 			$(document).ready(function(){
// 				$("#rb_user_dvsn1").click(function(){ 	//프리랜서 (개인) 회원가입 폼
// 					$(".ne1").load("<c:url value="/common/memberJoinForm_freelancer" />",function(response, status, xhr) {
// 						  if (status == "error") {
// 							    var msg = "Sorry but there was an error: ";
// 							    $("#error").html(msg + xhr.status + " " + xhr.statusText);
// 						  }
// 					return false;
// 					});
// 				});
				
// 				$("#rb_user_dvsn2").click(function(){	//기업 회원가입 폼
// 					$(".ne1").load("<c:url value="/common/memberJoinForm_company" />",function(response, status, xhr) {
// 						  if (status == "error") {
// 							    var msg = "Sorry but there was an error: ";
// 							    $("#error").html(msg + xhr.status + " " + xhr.statusText);
// 						  }
// 					return false;
// 					});
// 				});
				
// 				$("#rb_user_dvsn1").click();
// 			});			
</script>

</body>
</html>