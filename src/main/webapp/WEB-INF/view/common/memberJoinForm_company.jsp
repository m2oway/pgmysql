<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<form:form id="form" action="/common/memberJoinForm" method="post" modelAttribute="memberJoinFormBean">
	<input type="hidden" name="rb_user_dvsn" value="company"/>
	<div class="ne1">
	
		<div class="member-body-1">
			<div class="member-box2">
				<ul>
					<li class="member-nameinput">
						<span class="member-name">아이디(ID)</span>
						<input class="member-inputtext" type="text" name="ip_user_id" id="ip_user_id" alt="id"/>
						<span class="member-subname">*12자 이내</span>
						<span class="member-name-bt"><a id="writeString" class="textLink" href="<c:url value="/common/userIdChk" />">중복확인</a></span>
						<span id="duplicate"></span>
						<input type="hidden" name="hd_user_id" id="hd_user_id" />
						<input type="hidden" name="hd_user_id_cnt" id="hd_user_id_cnt"/>
					</li>
					<li class="member-nameinput">
						<span class="member-name">담당자명(실명)</span>
						<input class="member-inputtext" type="text" name="ip_user_name" id="ip_user_name" alt="name"/>
					</li>
					<li class="member-nameinput">
						<span class="member-name">닉네임</span>
						<input class="member-inputtext" type="text" name="ip_nick_name" id="ip_nick_name" alt="name2"/>
						<span class="member-name-bt"><a id="writeString" class="textLink2" href="<c:url value="/common/userNickNameChk" />">중복확인</a></span>
						<span id="duplicate_nickName"></span>
						<input type="hidden" name="hd_user_nickName" id="hd_user_nickName" />
						<input type="hidden" name="hd_user_nickName_cnt" id="hd_user_nickName_cnt"/>
					</li>
					<li class="member-nameinput">
						<span class="member-name">성별</span>
						<input class="member-radio" type="radio" name="rb_sex" id="rb_sex1" value="10" alt="남"/>
						<span class="member-name-s">남</span>
						<input class="member-radio" type="radio" name="rb_sex" id="rb_sex2" value="20" alt="여"/>
						<span class="member-name-s">여</span>
					</li>
					<li class="member-nameinput">
						<span class="member-name">생년월일</span>
						<input class="member-inputtext" type="text" name="ip_birthday" id="ip_birthday" readOnly="readOnly" alt="birthday"/>
<%-- 						<span class="member-calendar-bt">달력</span> --%>
						<input class="member-radio" type="radio" name="rb_calendar_dvsn" id="rb_calendar_dvsn1" value="solar"/>
						<span class="member-name-s">양력</span>
						<input class="member-radio" type="radio" name="rb_calendar_dvsn" id="rb_calendar_dvsn2" value="lunar"/>
						<span class="member-name-s">음력</span>
					</li>
					<li class="member-nameinput">
						<span class="member-name">사업자 등록번호</span>
						<input class="member-inputtext-com" type="text" name="ip_Business_number" id="ip_Business_number" alt="사업자 등록번호"/> 
						<span class="member-subname">*16자 이내</span>
					</li>
					<li class="member-nameinput">
						<span class="member-name">업체명</span>
						<input class="member-inputtext-com" type="text" name="ip_company_name" id="ip_company_name" alt="업체명"/>
					</li>
					<li class="member-nameinput">
						<span class="member-name">비밀번호</span>
						<input class="member-inputtext" type="password" name="ip_password" id="ip_password" alt="pw"/>
					</li>
					<li class="member-nameinput">
						<span class="member-name">비밀번호 확인</span>
						<input class="member-inputtext" type="password" name="ip_password_confirm" id="ip_password_confirm" alt="pw1"/>
					</li>
				</ul>
			</div>
		</div>
		<div class="member-sub-title"><span>*</span>연락처</div>
		<div class="member-body-1">
			<div class="member-box2">
				<ul>
					<li class="member-nameinput">
						<span class="member-name">전화번호</span>
						<select class="member-phone-select" name="sl_tel_number1" id="sl_tel_number1">
							<option value="02">02</option>
							<option value="031">031</option>
							<option value="032">032</option>
							<option value="033">033</option>
							<option value="041">041</option>
							<option value="042">042</option>
							<option value="043">043</option>
							<option value="051">051</option>
							<option value="052">052</option>
							<option value="053">053</option>
							<option value="054">054</option>
							<option value="055">055</option>
							<option value="061">061</option>
							<option value="062">062</option>
							<option value="063">063</option>
							<option value="064">064</option>
							<option value="070">070</option>
						</select>
						<span class="member-minus">-</span>
						<input class="member-phonetext" type="text" name="ip_tel_number2" id="ip_tel_number2" maxlength="4" alt="phone-number"/>
						<span class="member-minus">-</span>
						<input class="member-phonetext" type="text" name="ip_tel_number3" id="ip_tel_number3" maxlength="4" alt="phone-number"/>
					</li>
					<li class="member-nameinput">
						<span class="member-name">휴대전화번호</span>
						<select class="member-phone-select" name="sl_mobile_number1" id="sl_mobile_number1">
							<option value="010">010</option>
							<option value="011">011</option>
							<option value="016">016</option>
							<option value="017">017</option>
							<option value="018">018</option>
							<option value="019">019</option>
						</select>
						<span class="member-minus">-</span>
						<input class="member-phonetext" type="text" name="ip_mobile_number2" id="ip_mobile_number2" maxlength="4" alt="휴대전화번호"/>
						<span class="member-minus">-</span>
						<input class="member-phonetext" type="text" name="ip_mobile_number3" id="ip_mobile_number3" maxlength="4" alt="휴대전화번호"/>
						<input type="hidden" name="hd_mobile_cert" id="hd_mobile_cert"/>
						<span class="member-name-bt phone" id="com_bt_mobile">휴대폰인증</span>
					</li>
					<li class="member-nameinput">
						<span class="member-name">E-mail</span>
						<input class="member-inputtext" type="text" name="ip_email1" id="ip_email1" alt="E-mail"/>
						<span class="member-minus">@</span>
						<select class="member-e-mail-select" name="sl_email2" id="sl_email2">
							<option value="">--선택--</option>
							<option value="naver.com">naver.com</option>
							<option value="daum.net">daum.net</option>
							<option value="nate.com">nate.com</option>
							<option value="gmail.com">gmail.com</option>
						</select>
						<input type="checkbox" name="ck_direct_email" id="ck_direct_email" value="direct" alt="직접입력" />
						<span class="member-name-s">직접입력</span>
						<input class="member-inputtext" type="text" name="ip_direct_email" id="ip_direct_email" />
					</li>
				</ul>
			</div>
		</div>
		<div class="member-sub-title"><span>*</span>주소</div>
		<div class="member-body-2">
			<div class="member-box2">
				<ul>
					<li class="member-nameinput">
						<span class="member-name">우편번호</span>
						<input type="hidden" name="zipcode_id" id="zipcode_id"/>
						<input class="member-post-input" type="text" name="ip_post_number1" id="ip_post_number1" readOnly="readOnly" alt="우편번호"/>
						<span class="member-minus">-</span>
						<input class="member-post-input" type="text" name="ip_post_number2" id="ip_post_number2" readOnly="readOnly" alt="우편번호"/>
						<span class="member-calendar-bt" id="com_bt-3">검색</span>
					</li>
					<li class="member-nameinput">
						<span class="member-name">상세주소</span>
						<input class="member-address" type="text" name="ip_address" id="ip_address" readOnly="readOnly" alt="상세주소"/>
					</li>
					<li class="member-nameinput">
						<input class="member-address1" type="text" name="ip_detail_address" id="ip_detail_address" alt="상세주소"/>
					</li>
				</ul>
			</div>
		</div>
		
	</div>
	
	<div class="member-stap_bt">
		<span id="on_submit" class="member-stap_btnext">확인</span>
	</div>
</form:form>
<%@include file="/WEB-INF/view/popup/addresspop.jsp" %>
<%@include file="/WEB-INF/view/include/mobileCertificationPop.jsp" %>

		<script>
			$.datepicker.regional['ko'] = {
				monthNames: ['1월(JAN)','2월(FEB)','3월(MAR)','4월(APR)','5월(MAY)','6월(JUN)','7월(JUL)','8월(AUG)','9월(SEP)','10월(OCT)','11월(NOV)','12월(DEC)'],
				monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
				dayNames: ['일','월','화','수','목','금','토'],
				dayNamesShort: ['일','월','화','수','목','금','토'],
				dayNamesMin: ['일','월','화','수','목','금','토'],
				weekHeader: 'Wk',
				dateFormat: 'yy-mm-dd',
				firstDay: 0,
				isRTL: false};
			$.datepicker.setDefaults($.datepicker.regional['ko']);
			
			$('#ip_birthday').datepicker({
				showOn:"button"
				,showAnim:"drop"	//달력보기 애니메이션(Drop)
				,dateFormat:"yy-mm-dd"	//가져올 날짜 형식
				,buttonImage:"../../../resources/images/calendar-bt.png"	//달력버튼 이미지URL
// 				,buttonImage:"images/calendar.gif"
				,buttonImageOnly:true
				,changeMonth:true	//월 Select Box
				,changeYear:true	//년 Select Box
				,showMonthAfterYear:true 	//년도 표시가 왼쪽으로 나도로록 처리
			});

			$("#ip_direct_email").hide();
			
			$(".tli ul li:eq(0)").click(function(){
				alert("dFDFs");
			});
			// Tabs
			$('#tabs').tabs();
// 			$( "#ip_birthday" ).datepicker();
			
			//아이디 중복 확인
			$("a.textLink").click(function(){
				
				if($.trim($("#ip_user_id").val()) == ""){
					alert("아이디를 입력하십시오.");
					$("#ip_user_id").focus();
					$("#duplicate").html("");
					return false;
				}
				
				var link = $(this);
				$('#ip_user_id').val($.trim($('#ip_user_id').val()));
				$('#hd_user_id').val($.trim($('#ip_user_id').val()));
				
				$.ajax({
					url: link.attr("href")
					,type: "POST"
					,data: $('#form').serialize()
					,dataType: "json"
					,success: function(data) { 
									$("#hd_user_id_cnt").val(data);
									if(data == 0){
										$("#duplicate").html("사용할 수 있는 아이디 입니다.");
									}else{
										$("#duplicate").html("현재 사용중인 아이디 입니다.");
									}
						   	  }
					,error: function(xhr) {
									alert(xhr.status);
							  }
					});
				return false;
			});
			
			//닉네임 중복 확인
			$("a.textLink2").click(function(){
				
				if($.trim($("#ip_nick_name").val()) == ""){
					alert("닉네임을 입력하십시오.");
					$("#ip_nick_name").focus();
					$("#duplicate_nickName").html("");
					return false;
				}
				
				var link = $(this);
				$("#ip_nick_name").val($.trim($("#ip_nick_name").val()));
				$("#hd_user_nickName").val($.trim($("#ip_nick_name").val()));
				
				$.ajax({
					url: link.attr("href")
					,type: "POST"
					,data: $('#form').serialize()
					,dataType: "json"
					,success: function(data){
							$("#hd_user_nickName_cnt").val(data);
							if(data == 0){
								$("#duplicate_nickName").html("사용할 수 있는 닉네임 입니다.");
							}else{
								$("#duplicate_nickName").html("현재 사용중인 닉네임 입니다.");
							}
						}
					,error: function(xhr){
							alert(xhr.status);
						}
				});
				return false;
			});
			
			//이메일 직접입력
			$("#ck_direct_email").click(function(){
				if($("#ck_direct_email:checked").val() == "direct"){	//활성화 (직접입력)
					$("#ip_direct_email").show();
					$("#sl_email2").val("");
					$("#sl_email2").attr("disabled", true);
				}else{
					$("#ip_direct_email").hide();
					$("#ip_direct_email").val("");
					$("#sl_email2").attr("disabled", false);
				}
			});
			
			//회원가입
			$("#on_submit").click(function(){
				
				if($("input:radio[name=rb_user_dvsn]:checked").val() == undefined){
					alert("가입자 구분을 선택하십시오.");
					return false;
				}
					
				if($.trim($("#ip_user_id").val()) == ""){
					alert("아이디를 입력하십시오.");
					$("#ip_user_id").focus();
					$("#duplicate").html("");
					return false;
				}
				if($("#hd_user_id").val() == "" || $("#hd_user_id").val() != $("#ip_user_id").val() || $("#hd_user_id_cnt").val() == ""){
					alert("아이디 중복확인을 하십시오.");
					$("#ip_user_id").focus();
					return false;
				}
				//아이디 조회 카운트값으로 비교해야됨, DB 테이블에 등록된것인지 아닌지('0'보다 크면 등록 불가)
				if($("#hd_user_id_cnt").val() > 0){
					alert("현재 사용중인 아이디 입니다. 다시 입력 하십시오.");
					$("#ip_user_id").focus();
					return false;
				}
				
				if($.trim($("#ip_user_name").val()) == ""){
					alert("담당자명(실명)을 입력하십시오.");
					$("#ip_user_name").focus();
					return false;
				}
				
				if($.trim($("#ip_nick_name").val()) == ""){
					alert("닉네임을 입력하십시오.");
					$("#ip_nick_name").focus();
					return false;
				}
				
				if($("input:radio[name=rb_sex]:checked").val() == undefined){
					alert("성별을 선택하십시오.");
					return false;
				}
				
				if($("#ip_birthday").val() == ""){
					alert("생년월일을 입력하십시오.");
					return false;
				}
				
				if($("input:radio[name=rb_calendar_dvsn]:checked").val() == undefined){
					alert("생년월일 양력,음력 구분을 선택하십시오.");
					return false;
				}
				
				if($.trim($("#ip_company_name").val()) == ""){
					alert("업체명을 입력하십시오.");
					$("#ip_company_name").focus();
					return false;
				}
				
				if($.trim($("#ip_Business_number").val()) == ""){
					alert("사업자등록 번호를 입력하십시오.");
					return false;
				}
				if(isNaN($("#ip_Business_number").val())){
					alert("사업자등록 번호엔 숫자값만 입력하십시오.");
					return false;
				}
				
				if($.trim($("#ip_password").val()) == ""){
					alert("비밀번호를 입력하십시오.");
					$("#ip_password").val("");
					$("#ip_password").focus();
					return false;
				}
				if($.trim($("#ip_password_confirm").val()) == ""){
					alert("비밀번호 확인을 입력하십시오.");
					$("#ip_password_confirm").val("");
					$("#ip_password_confirm").focus();
					return false;
				}
				if($("#ip_password").val() != $("#ip_password_confirm").val()){
					alert("비밀번호와 비밀번호 확인 값이 틀립니다.");
					$("#ip_password").val("");
					$("#ip_password_confirm").val("");
					$("#ip_password").focus();
					return false;
				}
				
				if($.trim($("#ip_tel_number2").val()) == "" || $.trim($("#ip_tel_number3").val()) == ""){
					alert("전화번호를 입력하십시오.");
					return false;
				}
				if(isNaN($("#ip_tel_number2").val()) || isNaN($("#ip_tel_number3").val())){
					alert("전화번호엔 숫자값만 입력하십시오.");
					return false;
				}
				
				if($.trim($("#ip_mobile_number2").val()) == "" || $.trim($("#ip_mobile_number3").val()) == ""){
					alert("휴대전화번호를 입력하십시오.");
					return false;
				}
				if(isNaN($("#ip_mobile_number2").val()) || isNaN($("#ip_mobile_number3").val())){
					alert("휴대전화번호엔 숫자값만 입력하십시오.");
					return false;
				}
				if($("#hd_mobile_cert").val() == "" || $("#hd_mobile_cert").val() != "true"){
					alert("휴대전화 인증을 받으십시오.");
					return false;
				}
				
				if($.trim($("#ip_email1").val()) == ""){
					alert("이메일을 입력하십시오.");
					$("#ip_email1").focus();
					return false;
				}
				if($("#sl_email2").val() == "" && $("#ck_direct_email:checked").val() == undefined){
					alert("이메일을 입력하십시오.");
					return false;
				}
				if($.trim($("#ip_direct_email").val()) == "" && $("#ck_direct_email:checked").val() == "direct"){
					alert("이메일을 입력하십시오.");
					return false;
				}
				
				if($("#ip_post_number1").val() == "" || $("#ip_post_number2").val() == ""){
					alert("우편번호를 입력하십시오.");
					return false;
				}
				if($("#ip_address").val() == ""){
					alert("주소를 입력하십시오.");
					return false;
				}

//				alert($("#form").serialize());
					
				if(confirm("회원가입 하시겠습니까?")){
					//데이터를 Bean으로 전송
					$.post($(this).attr("action"), $("#form").serialize(), function(html) {
						$("#form").replaceWith(html);
					});
					return false;
				}
			});
			
		</script>

