<%--
  Created by IntelliJ IDEA.
  User: hoyeongheo
  Date: 2014-10-28
  Time: 오후 7:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/onoff.css" />"  />  
        <style>
            #paycalyyyymmtitle{left:10px;top:10px;width:200px;height:30px;font-size: 20px;background-color:#ffffff;}
        </style> 
            <style>
                html, body {
                    height: 100%;
                }                
                .logout_btn {
                            -webkit-border-radius: 28;
                            -moz-border-radius: 28;
                            border-radius: 28px;
                            font-family: Arial;
                            color: #ffffff;
                            font-size: 12px;
                            background: #add5f0;
                            padding: 10px 20px 10px 20px;
                            text-decoration: none;
                      }

                      .logout_btn:hover {
                            background: #3cb0fd;
                            background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
                            background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
                            background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
                            background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
                            background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
                            text-decoration: none;
                      }                    
                   .manualbt {
	-moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
	box-shadow:inset 0px 1px 0px 0px #ffffff;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #dfdfdf) );
	background:-moz-linear-gradient( center top, #ededed 5%, #dfdfdf 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#dfdfdf');
	background-color:#ededed;
	-webkit-border-top-left-radius:6px;
	-moz-border-radius-topleft:6px;
	border-top-left-radius:6px;
	-webkit-border-top-right-radius:6px;
	-moz-border-radius-topright:6px;
	border-top-right-radius:6px;
	-webkit-border-bottom-right-radius:6px;
	-moz-border-radius-bottomright:6px;
	border-bottom-right-radius:6px;
	-webkit-border-bottom-left-radius:6px;
	-moz-border-radius-bottomleft:6px;
	border-bottom-left-radius:6px;
	text-indent:0;
	border:1px solid #dcdcdc;
	display:inline-block;
	color:#777777;
	font-family:arial;
	font-size:15px;
	font-weight:bold;
	font-style:normal;
	height:45px;
	line-height:45px;
	width:64px;
	text-decoration:none;
	text-align:center;
	text-shadow:1px 1px 0px #ffffff;
}
.manualbt:hover {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #dfdfdf), color-stop(1, #ededed) );
	background:-moz-linear-gradient( center top, #dfdfdf 5%, #ededed 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#dfdfdf', endColorstr='#ededed');
	background-color:#dfdfdf;
}.manualbt:active {
	position:relative;
	top:1px;
}                    


.CSSTableGenerator {
	margin:0px;padding:0px;margin-left:5px;margin-right:5px;
	width:99%;
	box-shadow: 10px 10px 5px #888888;
	border:1px solid #000000;
	-moz-border-radius-bottomleft:0px;
	-webkit-border-bottom-left-radius:0px;
	border-bottom-left-radius:0px;
	
	-moz-border-radius-bottomright:0px;
	-webkit-border-bottom-right-radius:0px;
	border-bottom-right-radius:0px;
	
	-moz-border-radius-topright:0px;
	-webkit-border-top-right-radius:0px;
	border-top-right-radius:0px;
	
	-moz-border-radius-topleft:0px;
	-webkit-border-top-left-radius:0px;
	border-top-left-radius:0px;
}.CSSTableGenerator table{
    border-collapse: collapse;
        border-spacing: 0;
	width:100%;
	margin:1px;padding:3px;
}.CSSTableGenerator tr:last-child td:last-child {
	-moz-border-radius-bottomright:0px;
	-webkit-border-bottom-right-radius:0px;
	border-bottom-right-radius:0px;
}
.CSSTableGenerator table tr:first-child td:first-child {
	-moz-border-radius-topleft:0px;
	-webkit-border-top-left-radius:0px;
	border-top-left-radius:0px;
}
.CSSTableGenerator table tr:first-child td:last-child {
	-moz-border-radius-topright:0px;
	-webkit-border-top-right-radius:0px;
	border-top-right-radius:0px;
}.CSSTableGenerator tr:last-child td:first-child{
	-moz-border-radius-bottomleft:0px;
	-webkit-border-bottom-left-radius:0px;
	border-bottom-left-radius:0px;
}.CSSTableGenerator tr:hover td{
	
}
.CSSTableGenerator tr:nth-child(odd){ background-color:#ffffff; }
.CSSTableGenerator tr:nth-child(even)    { background-color:#ffffff; }.CSSTableGenerator td{
	vertical-align:middle;
	
	
	border:1px solid #000000;
	border-width:0px 1px 1px 0px;
	text-align:left;
	padding:7px;
	font-size:12px;
	font-family:Arial;
	font-weight:normal;
	color:#000000;
}.CSSTableGenerator tr:last-child td{
	border-width:0px 1px 0px 0px;
}.CSSTableGenerator tr td:last-child{
	border-width:0px 0px 1px 0px;
}.CSSTableGenerator tr:last-child td:last-child{
	border-width:0px 0px 0px 0px;
}
.CSSTableGenerator tr:first-child td{
		background:-o-linear-gradient(bottom, #81bdf9 5%, #81bdf9 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #81bdf9), color-stop(1, #81bdf9) );
	background:-moz-linear-gradient( center top, #81bdf9 5%, #81bdf9 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#81bdf9", endColorstr="#81bdf9");	background: -o-linear-gradient(top,#81bdf9,81bdf9);

	background-color:#81bdf9;
	border:0px solid #000000;
	text-align:center;
	border-width:0px 0px 1px 1px;
	font-size:14px;
	font-family:Arial;
	font-weight:bold;
	color:#000000;
}
.CSSTableGenerator tr:first-child:hover td{
	background:-o-linear-gradient(bottom, #81bdf9 5%, #81bdf9 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #81bdf9), color-stop(1, #81bdf9) );
	background:-moz-linear-gradient( center top, #81bdf9 5%, #81bdf9 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#81bdf9", endColorstr="#81bdf9");	background: -o-linear-gradient(top,#81bdf9,81bdf9);

	background-color:#81bdf9;
}
.CSSTableGenerator tr:first-child td:first-child{
	border-width:0px 0px 1px 0px;
}
.CSSTableGenerator tr:first-child td:last-child{
	border-width:0px 0px 1px 1px;
}

.titleBg {
    background-image: url(../resources/images/title_bg.gif);
    background-repeat: no-repeat;
    background-position: left top;
    padding: 0 0 0 15px;
}

.title {
    background-image: url(../resources/images/title_sleft.gif);
    background-repeat: no-repeat;
    background-position: left top;
    font-size: 16px;
    font-weight: bold;
    color: #222222;
    line-height: 1.3em;
    padding: 0 0 2px 12px;
}

.basicfotter{
    position: relative;
    min-width: 1340px;
    height: 81px;
    /*background-color: #7393ae;*/
    background-color: #ffffff;
}
            </style>   
    </head>
<body >
<table width="100%" height="100%" border="0" >
    <tr>
        <td width="100%" height="317px" style="background:url('../resources/images/S_main_picbg_tko.jpg') 0 0 no-repeat;" valign="top">
<!--            
<div class="loginInfo">
    <div class="logo">
            <img src="../resources/images/onoffmark.jpg" width="146" height="65"/>
    </div>
    <div style="padding:10px 10px 10px 10px;vertical-align:middle;height: 60px;text-align:right;vertical-align: middle;"><button id='logoutbtn' class="logout_btn">logout</button></div>

</div>-->
 <!--<img src="../resources/images/S_main_picbg.jpg" width="1199" height="317"/>-->
 <div style="padding:10px 10px 10px 10px;vertical-align:middle;height: 60px;text-align:right;vertical-align: middle;"><button id='logoutbtn' class="logout_btn">logout</button></div>
        </td>
    </tr>
    <tr>
        <td width="100%" height="30px">
<!--메뉴 -->
<div class="settlementPaymentMenu">
    <div id="menuObj"></div>
</div>   
        </td>
    </tr>
    <tr>
         <td valign="top">
<div id="contentsmain" style="width:100%;height:82%;min-height:400px;" >