<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>회원가입</title>
		<%@include file="/WEB-INF/view/include/head_resource.jsp" %>
	</head>
	<body>
		<div id="container">
			<!--topWarapper-->
			<%@include file="/WEB-INF/view/include/topWrapper.jsp" %>
			<div class="member-center">
				<div class="member-title-img"></div>
				<!-- ----------수행기술 등록 부분 Start--------------------------------- -->
<!-- 				<div class="ne"> -->
<%-- 					<span class="yn">플젝 수행 분야: </span> --%>
<!-- 					[<input type="text" name="proj_exec_fild_code" id="proj_exec_fild_code" class="add_in4"/> -->
<!-- 					<input type="text" name="proj_exec_fild_name" id="proj_exec_fild_name" class="add_in4"/>]<br/> -->
<%-- 					<span class="yn">플젝 수행 역할: </span> --%>
<!-- 					[<input type="text" name="proj_exec_role_code" id="proj_exec_role_code" class="add_in4"/> -->
<!-- 					<input type="text" name="proj_exec_role_name" id="proj_exec_role_name" class="add_in4"/>]&nbsp; -->
<!-- 					<div id="on_projExec" class="stap_btnext"><h5>수행분야</h5></div> -->
<!-- 				</div> -->
				<!-- ----------수행기술 등록 부분 End----------------------------------- -->
				
				<!-- ----------수행업무 등록 부분 Start--------------------------------- -->
<!-- 				<div class="ne"> -->
<%-- 					<span class="yn">수행 업무: </span> --%>
<!-- 					[<input type="text" name="exec_job_code" id="exec_job_code" class="add_in4"/> -->
<!-- 					<input type="text" name="exec_job_name" id="exec_job_name" class="add_in4"/>]<br/> -->
<!-- 					======= -->
<!-- 					[산업분류ID<input type="text" name="hd_idst_clss_id" id="hd_idst_clss_id" />]&nbsp;&nbsp;  -->
<!-- 					<input type="text" name="ip_jobDuty_name" id="ip_jobDuty_name" class="add_in4" readonly="readonly" /> -->
<!-- 					<input type="text" name="hd_jobDuty_id" id="hd_jobDuty_id" /> -->
<%-- 					{{<span class="com_bt-1" id="on_executeJob"><h5>찾아보기</h5></span>}} --%>
<!-- 				</div> -->
				<!-- ----------수행업무 등록 부분 End----------------------------------- -->
				<div class="member-comradio">
					<input class="member-radio" type="radio" name="rb_user_dvsn" id="rb_user_dvsn1" value="freelancer"/>
					<span class="member-name-s">개인</span>&nbsp;&nbsp;&nbsp;
					<input class="member-radio" type="radio" name="rb_user_dvsn" id="rb_user_dvsn2" value="company"/>
					<span class="member-name-s">기업</span>
<!-- 					회원정보 수정 임시 -->
					&nbsp;&nbsp;&nbsp;&nbsp;
					<span class="yn">[개인<a href="/common/member_userPw_confirmation?memb_dvsn=free">[프리랜서 회원 정보수정]</a>정보수정]&nbsp;
					[기업<a href="/common/member_userPw_confirmation?memb_dvsn=company">[기업 회원 정보수정]</a>정보수정]</span>
				</div>
				<div class="ne1">
					<!--ajax import -->
				</div>
			</div>
			
			<!--btmWarpper-->
			<%@include file="/WEB-INF/view/include/btmWrapper.jsp" %>
			<%@include file="/WEB-INF/view/popup/addresspop.jsp" %>
			
			<!-- ----------수행기술 등록 부분 Start--------------------------------- -->
			<%//@include file="/WEB-INF/view/popup/projectExecutePop.jsp" %>
			<!-- ----------수행기술 등록 부분 End----------------------------------- -->
			
		</div>
		<script>
			$(document).ready(function(){
				$("#rb_user_dvsn1").click(function(){ 	//프리랜서 (개인) 회원가입 폼
					$(".ne1").load("<c:url value="/common/memberJoinForm_freelancer" />",function(response, status, xhr) {
						  if (status == "error") {
							    var msg = "Sorry but there was an error: ";
							    $("#error").html(msg + xhr.status + " " + xhr.statusText);
						  }
					return false;
					});
				});
				
				$("#rb_user_dvsn2").click(function(){	//기업 회원가입 폼
					$(".ne1").load("<c:url value="/common/memberJoinForm_company" />",function(response, status, xhr) {
						  if (status == "error") {
							    var msg = "Sorry but there was an error: ";
							    $("#error").html(msg + xhr.status + " " + xhr.statusText);
						  }
					return false;
					});
				});
				
				$("#rb_user_dvsn1").click();
			});
			
			//수행기술 등록 부분 Start===================================================
			//수행기술 팝업 Ajax
			$("#on_projExec").click(function(){
				$.ajax({ 
					url: "/popup/projectExecutePop"
					, type :"GET"
//						, data   :formId.serialize()
					, dataType: "text"
					, success: function(data) {
// 								$("body").append(data.replace("proj_exec_fild_code","1111"));
								$("body").append(data);
							   }
				    , error: function(xhr) { 
				    			alert(xhr.status);
				    		 }
				    });
				return false;
			});
			//수행기술 등록 부분 End=====================================================
			
			//수행업무 등록 부분 Start===================================================
			//수행업무 팝업 Ajax
			$("#on_executeJob").click(function(){
				
				if($.trim($("#hd_idst_clss_id").val()) == ""){
					alert("발주사(산업분류)를 먼저 선택하십시오.");
					return false;
				}else{
					$.ajax({
						url: "/popup/executeJobPop"
						, type: "GET"
						, dataType: "text"
						, success: function(data) {
								$("body").append(data);
						}
				    	, error: function(xhr) { 
				    			alert(xhr.status);
				    	}
					});
					return false;
				}
			});
			//수행업무 등록 부분 End=====================================================
		</script>
</body>
</html>
