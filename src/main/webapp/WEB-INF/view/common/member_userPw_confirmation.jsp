<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>회원정보 수정</title>
		<%@include file="/WEB-INF/view/include/head_resource.jsp" %>
	</head>
	<body>
		<div id="container">
			<!--topWarapper-->
			<%@include file="/WEB-INF/view/include/topWrapper.jsp" %>
			<div id="center">
			
				<div class="compop-center">
					<input type="hidden" name="result" id="result" value="${result}" />
					<form id="form" action="/common/member_userPw_confirmation" method="post">
						<input type="hidden" name="memb_dvsn" id="memb_dvsn" value="${memberJoin.memb_dvsn}" />
						<div class="compop-title-box">
							<ul>
								<li class="compop-list-title">
									<span class="compop-title-img"></span>
								</li>
								<li class="compop-list-title">
									<span class="compop-title">비밀번호 확인</span>
								</li>
								<li class="clean"></li>
							</ul>
						</div>
						<div class="compop-title-line"></div>
						<div class="compop-name-title-box">
							<ul>
								<li class="compop-search-name-block">
									<span class="compop-name1">*회원정보 수정시 보안을 위해 비밀번호를 입력해주시기 바랍니다.</span>
								</li>
							</ul>
						</div>
						<div class="compop-body-1">
							<ul>
								<li class="compop-list-input">
									<span class="compop-name">아이디(ID)</span>
									<input class="compop-input-idpw" type="text" name="ip_user_id" id="ip_user_id" value="${memberJoin.ip_user_id}" readonly="readonly" alt="id" />
								</li>
								<li class="compop-list-input1">
									<span class="compop-name">비밀번호(PW)</span>
									<input class="compop-input-idpw" type="password" name="ip_password" id="ip_password" alt="pw" />
								</li>
							</ul>
						</div>
						<div class="compop-stap_bt">
							<span class="compop-stap_btnext" id="on_submit">확인</span>
						</div>
					</form>
				</div>
				<div class="clean"></div>			
<!-- ---------------------------------------------------------------------------------------------- -->
<!-- 				<div id="body"> -->
<!-- 					<div class="body2"> -->
<%-- 						<span>회원정보 수정</span> --%>
<!-- 						<div class="ne"> -->
<!-- 							<div class="ne1"> -->
<%-- 								<input type="hidden" name="result" id="result" value="${result}" /> --%>
<%-- <%-- 								<input type="hidden" name="member_dvsn" id="member_dvsn" value="${memberJoin.memb_dvsn}" /> --%>
<%-- 								<form id="form" action="/common/member_userPw_confirmation" method="post"> --%>
<%-- 									<input type="hidden" name="memb_dvsn" id="memb_dvsn" value="${memberJoin.memb_dvsn}" /> --%>
<!-- 									<div class="ne1"> -->
<!-- 										<div class="m_1"> -->
<%-- 											<span class="r_ao4">아이디</span> --%>
<%-- 											<input type="text" name="ip_user_id" id="ip_user_id" value="${memberJoin.ip_user_id}" class="add_in4" readonly="readonly" /> --%>
<!-- 										</div> -->
<!-- 										<div class="m_1"> -->
<%-- 											<span class="r_ao4">비밀번호</span> --%>
<!-- 											<input type="password" name="ip_password" id="ip_password" class="add_in4" /> -->
<!-- 										</div> -->
<!-- 										<div id="stap_bt"> -->
<!-- 											<div id="on_submit" class="stap_btnext"><h5>확인</h5></div> -->
<!-- 										</div> -->
<!-- 									</div> -->
<%-- 								</form> --%>
<!-- 							</div> -->
<!-- 						</div> -->
<!-- 					</div> -->
<!-- 				</div> -->
<!-- ---------------------------------------------------------------------------------------------- -->
			</div>
			<!--btmWarpper-->
			<%@include file="/WEB-INF/view/include/btmWrapper.jsp" %>
		</div>
		
<script>

	$(document).ready(function(){
		$("#ip_password").focus();
		
		if($("#result").val() == "match"){	//비밀번호 일치
			$("#result").val("");
			if($("#memb_dvsn").val() == "free"){
				location.href="<c:url value='/common/memberInfoModify_freelancer?user_login_id="+$("#ip_user_id").val()+"'/>";
			}else if($("#memb_dvsn").val() == "company"){
				location.href="<c:url value='/common/memberInfoModify_company?user_login_id="+$("#ip_user_id").val()+"'/>";
			}
		}else if($("#result").val() == "inconsistency"){	//비밀번호 불일치
			$("#result").val("");
			alert("비밀번호가 일치하지 않습니다.");
// 			location.href="<c:url value='/'/>";
		}
	});

	$("#ip_password").keypress(function(event){
		var code = (event.keyCode ? event.keyCode : event.which);
		if(code===13) { // Enter keycode
			$("#on_submit").click();
	    }
	});

	$("#on_submit").unbind("click").click(function(){
		if($("#ip_password").val() == ""){
			alert("비밀번호를 입력하십시오.");
			$("#ip_password").focus();
			return false;
		}
		
		$("#form").submit();
		
	});

</script>
		
	</body>
</html>