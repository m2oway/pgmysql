<%--
  Created by IntelliJ IDEA.
  User: hoyeongheo
  Date: 2014-10-28
  Time: 오후 5:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=10"/>
<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
<html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <title>tkon</title>
            <style>
                .logout_btn {
                            -webkit-border-radius: 28;
                            -moz-border-radius: 28;
                            border-radius: 28px;
                            font-family: Arial;
                            color: #ffffff;
                            font-size: 12px;
                            background: #add5f0;
                            padding: 10px 20px 10px 20px;
                            text-decoration: none;
                      }

                      .logout_btn:hover {
                            background: #3cb0fd;
                            background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
                            background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
                            background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
                            background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
                            background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
                            text-decoration: none;
                      }                    
                   .manualbt {
	-moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
	box-shadow:inset 0px 1px 0px 0px #ffffff;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #dfdfdf) );
	background:-moz-linear-gradient( center top, #ededed 5%, #dfdfdf 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#dfdfdf');
	background-color:#ededed;
	-webkit-border-top-left-radius:6px;
	-moz-border-radius-topleft:6px;
	border-top-left-radius:6px;
	-webkit-border-top-right-radius:6px;
	-moz-border-radius-topright:6px;
	border-top-right-radius:6px;
	-webkit-border-bottom-right-radius:6px;
	-moz-border-radius-bottomright:6px;
	border-bottom-right-radius:6px;
	-webkit-border-bottom-left-radius:6px;
	-moz-border-radius-bottomleft:6px;
	border-bottom-left-radius:6px;
	text-indent:0;
	border:1px solid #dcdcdc;
	display:inline-block;
	color:#777777;
	font-family:arial;
	font-size:15px;
	font-weight:bold;
	font-style:normal;
	height:45px;
	line-height:45px;
	width:64px;
	text-decoration:none;
	text-align:center;
	text-shadow:1px 1px 0px #ffffff;
}
.manualbt:hover {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #dfdfdf), color-stop(1, #ededed) );
	background:-moz-linear-gradient( center top, #dfdfdf 5%, #ededed 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#dfdfdf', endColorstr='#ededed');
	background-color:#dfdfdf;
}.manualbt:active {
	position:relative;
	top:1px;
}                    
            </style>
        </head>
        <body>
</c:if>
            <div class="loginInfo">
                <div class="logo">
                    <!--
                   <img src="../resources/images/okpay.png" width="146" height="65"/>
                    -->
                    <img src="../resources/images/tkomark.png" width="146" height="65"/>
                </div>
               <div style="padding:10px 10px 10px 10px;vertical-align:middle;height: 60px;text-align:right;vertical-align: middle;"><a href="<c:url value="/manualtool.jsp"/>" ><B>수동tool</B></a>&nbsp;&nbsp;&nbsp;<a href="<c:url value="/resources/images/usermanual.hwp"/>" class="manualbt">메뉴얼</a>&nbsp;<button id='logoutbtn' class="logout_btn">logout</button></div>
            </div>

            <!--메뉴 -->
            <div class="settlementPaymentMenu">
                <div id="menuObj"></div>
            </div>

            <div id="vubMain"  class="settlementPaymentContent">
                <div id="main_tabbar" class="settlementPaymentContent_Tabbar"></div>
            </div>

            <div class="fotter"><!--<img src="../resources/images/onoffbottom2.jpg" width="1200" height="77"/>--></div>

            <script type="text/javascript">

                var tabbar;
                var menu;

                $(document).ready(function(){

                    var height=0;
                    var minusHight =(Number($(".settlementPaymentMenu").css("height").replace(/px/g,"")) + Number($(".loginInfo").css("height").replace(/px/g,""))+ Number($(".fotter").css("height").replace(/px/g,"")));


                    menu = new dhtmlXMenuObject("menuObj");
                    menu.setIconsPath("<c:url value="/resources/images/dhx/dhtmlxMenu/" />");
                    
                    var user_cate = '${siteSessionObj.user_cate}';
                    
                    if(user_cate === '00'){//시스템관리자
                        
                        menu.loadXML("<c:url value="/resources/menu/dhx_menu_links.xml" />");
                        
                    }else if(user_cate === '01'){//정산관리자
                        
                        menu.loadXML("<c:url value="/resources/menu/dhx_menu_links_merch.xml" />");
                        
                    }else if(user_cate === '02'){//결제관리자
                        
                        menu.loadXML("<c:url value="/resources/menu/dhx_menu_links_term.xml" />");
                        
                    }else if(user_cate === '03'){//대리점관리자
                        
                        menu.loadXML("<c:url value="/resources/menu/dhx_menu_links_agent.xml" />");
                        
                    }
                    
                    tabbar = new dhtmlXTabBar("main_tabbar", "top");
                    tabbar.setSkin("dhx_skyblue");
                    tabbar.setImagePath("<c:url value="/resources/images/dhx/dhtmlxTabbar/" />");
                    tabbar.enableTabCloseButton(true);
                    tabbar.enableAutoReSize(true);

                    mainReSize();

                    $(window).resize(function() {
                        mainReSize();
                    });

                    $("#logoutbtn").click(function(){
                        location.href = "${pageContext.request.contextPath}/login/logout";
                    });
                    
                    if(user_cate === '01' )
                    {
                      //tabbar_add('11','정산달력','/paycalender/merchpayCalenderList',true);
                      tabbar_add('4-1','신용카드승인','/app/appselReady',true);
                    }
         
                    if(user_cate === '02')
                    {
                      tabbar_add('4-1','신용카드승인','/app/appselReady',true);
                    }
                    
                    if(user_cate === '00' )
                    {
                        tabbar_add('11','정산달력','/paycalender/payCalenderList',true);
                    }



                });

                function mainReSize(){

                    var menuHeight = Number($(".settlementPaymentMenu").css("height").replace(/px/g,""));

                    var loginInfoHeight = Number($(".loginInfo").css("height").replace(/px/g,""));

                    var fotter = Number($(".fotter").css("height").replace(/px/g,""));

                    var min_height =  Number($(".settlementPaymentContent").css("min-height").replace(/px/g,""));

                    var calHeight = $(window).height() -(menuHeight+loginInfoHeight+fotter);

                    if(calHeight >= min_height){

                        $('.settlementPaymentContent').css('height' ,  calHeight);

                    }
                }

                function tabbar_add(id,name,url,args) {
                    var contextPathUrl = "${pageContext.request.contextPath}" + url;
                    var tabId = String(tabbar.getAllTabs());
                    var tabId_arr = tabId.split(",");
                    var tabSize = tabId_arr.length;
                    var flag = false;

                    if(tabSize > 0){
                        for(var i=0;i<tabSize;i++){
                            if(tabId_arr[i] === id){
                                flag = true;
                            }
                        }
                    }

                    if(flag) {
                        if (id === tabId){
                            alert("현재 선택되어 있는 탭입니다.");
                        }
                        tabbar.setTabActive(id);
                    }else{
                        //tabbar.clearAll();
                        tabbar.addTab(id, name, "*");
                        tabbar.setTabActive(id);
                        tabbar.cells(id).attachURL(contextPathUrl,args);
                    }
                }

            </script>
            
<c:if test="${!ajaxRequest}">
        </body>
    </html>
</c:if>
