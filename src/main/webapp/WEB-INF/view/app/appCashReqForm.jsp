<%-- 
    Document   : appList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    Date today=new Date();
    SimpleDateFormat formater = new SimpleDateFormat("yyyy");       
    String strCurYear = formater.format(today);
%>
<html>
<head>
<title>okpay 현금영수증 승인</title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<!--
<link href="../css/style.css" rel="stylesheet" type="text/css">
-->
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/main/settlementPaymentCss.css" />"  />
<style type="text/css">
.pop-layer {display:none; position: absolute; top: 50%; left: 50%; width: 650px; height:auto;  background-color:#fff; border: 5px solid #3571B5; z-index: 10;} 
.pop-layer .pop-container {padding: 20px 25px;}
.pop-layer p.ctxt {color: #666; line-height: 25px;}
.pop-layer .btn-r {width: 100%; margin:10px 0 20px; padding-top: 10px; border-top: 1px solid #DDD; text-align:right;}
a.cbtn {display:inline-block; height:25px; padding:0 14px 0; border:1px solid #304a8a; background-color:#3f5a9d; font-size:13px; color:#fff; line-height:25px;}
a.cbtn:hover {border: 1px solid #091940; background-color:#1f326a; color:#fff;}
</style>
<script type="text/javascript" src="<c:url value="/resources/javascript/jquery-1.6.2.min.js" />"></script>
<script src="<c:url value="/resources/javascript/common.js" />"></script>
<script type="text/javascript">
    <c:if test="${result_code!='0'}">
        alert("${result_msg}");
        window.close();
    </c:if>
        
     function NulltoZero(p_param)
     {
         if(p_param =='')
         {
             return '0';
         }
         else
         {
             return p_param;
         }
     }        
    /* 입력 자동 Setting */
    function f_init(){

        var frm_pay = document.frm_pay;

        var today = new Date();
        var year  = today.getFullYear();
        var month = today.getMonth() + 1;
        var date  = today.getDate();
        var time  = today.getTime();

        if(parseInt(month) < 10) {
            month = "0" + month;
        }

        if(parseInt(date) < 10) {
            date = "0" + date;
        }

    }


    var bRetVal = true;
    function f_submit() {
        var frm_pay = document.frm_pay;


        /*  주문정보 확인 */
        /*
        if( !frm_pay.EP_order_no.value ) {
            alert("가맹점주문번호를 입력하세요!!");
            frm_pay.EP_order_no.focus();
            return;
        }
        */
        
        /*   고객명 확인 */
        if( !frm_pay.EP_user_nm.value ) {
            alert("고객명을 입력하세요!!");
            frm_pay.EP_user_nm.focus();
            return;
        }
        
        
        /*   고객전화번호 확인 */
        if( !frm_pay.EP_user_phone2.value ) {
            alert("고객전화번호를 입력하세요!!");
            frm_pay.EP_user_phone2.focus();
            return;
        }
        
        /*   고객상품명 확인 */
        if( !frm_pay.EP_product_nm.value ) {
            alert("상품명을 입력하세요!!");
            frm_pay.EP_product_nm.focus();
            return;
        }        
          
        document.all["EP_tot_amt"].value =  NulltoZero(document.all["EP_tot_amt"].value).replace(/,/g, '');
        document.all["EP_service_amt"].value =  NulltoZero(document.all["EP_service_amt"].value).replace(/,/g, '');
        document.all["EP_vat"].value =  NulltoZero(document.all["EP_vat"].value).replace(/,/g, ''); 
        
        var appAmt =  NulltoZero(document.all["EP_tot_amt"].value).replace(/,/g, '');
        var svcAmt =  NulltoZero(document.all["EP_service_amt"].value).replace(/,/g, '');
        var vatAmt =  NulltoZero(document.all["EP_vat"].value).replace(/,/g, ''); 
 
        

        /*  금액 확인 */
        if( !frm_pay.EP_tot_amt.value || frm_pay.EP_tot_amt.value=='0') {
            alert("총거래금액을 입력하세요!!");
            frm_pay.EP_tot_amt.focus();
            return;
        }
        if( !frm_pay.EP_service_amt.value ) {
            alert("봉사료를 입력하세요!!");
            frm_pay.EP_service_amt.focus();
            return;
        }
        if( !frm_pay.EP_vat.value ) {
            alert("부가세를 입력하세요!!");
            frm_pay.EP_vat.focus();
            return;
        }    
        
        

        if(  appAmt < (parseInt(svcAmt)+parseInt(vatAmt))   )
        {
            alert("봉사료,서비스료가 잘못되었습니다.");
            return;
        }
        
        
        /* 현금영수증 발급 구분에 따라 처리 */            
        /* 현금영수증 발행용도 */
        if( frm_pay.EP_issue_type.value == "01" ) {
        	/* 개인 */
    	    if( frm_pay.EP_auth_type.value == "02" ) {
    	        
    	        if( frm_pay.EP_auth_value.value.length != 13 ) {
    	            alert("주민등록번호를 입력하세요!!");
                    frm_pay.EP_auth_value.focus();
                    return;
    	        }
            }
            else if( frm_pay.EP_auth_type.value == "03" ) {
            	
            	if( frm_pay.EP_auth_value.value.length < 10 ) {
    	            alert("휴대폰번호를 입력하세요!!");
                    frm_pay.EP_auth_value.focus();
                    return;
    	        }
            }
            else {
            	alert("소득공제용은 인증구분을 주민등록번호, 휴대폰번호를 선택하세요.!!");
                frm_pay.EP_cash_auth_type.focus();
                return;
            }
        }
        else {
        	/* 법인 */
        	if( frm_pay.EP_auth_value.value.length != 10 ) {
    	        alert("사업자번호를 입력하세요!!");
                frm_pay.EP_auth_value.focus();
                return;
    	    }
        }        
    
        //bRetVal = true;
        //if ( bRetVal ) frm_pay.submit();
        if ( bRetVal )
        {            bRetVal = false;
                        progress_open("progresss");
                        $.ajax({
                    url : "../app/appCashHiddenAction",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#frm_pay").serialize(),
                        success : function(data) {
                            if(data.result_cd == '0000')
                            {
                                alert(data.result_msg);
                                forminit();
                                bRetVal = true;
                                 progress_close("progresss");
                                //window.close();
                            }
                            else
                            {
                                alert("[" +data.result_cd+ "]" + data.result_msg);
                                bRetVal = true;
                                 progress_close("progresss");
                                //window.close();
                            }
                            
                        },
                        error : function() { 
                            alert("승인 실패");
                            bRetVal = true;
                             progress_close("progresss");
                        }
           });	      
        }
        else
        {
            alert("결제 진행중입니다.");
        }        
    }
    
    
    function forminit()
    {
        var frm_pay = document.frm_pay;       
        /*   고객명 확인 */
        frm_pay.EP_user_nm.value = ""; 
        
        
        /*   고객전화번호 확인 */
        frm_pay.EP_user_phone2.value = ""; 
        
        
        /*   고객상품명 확인 */
        frm_pay.EP_product_nm.value = ""; 
        
        /*  금액 확인 */
        frm_pay.EP_tot_amt.value= "";        
        frm_pay.EP_service_amt.value= "";
        frm_pay.EP_vat.value = "";

        frm_pay.EP_issue_type.value = "01";
        frm_pay.EP_auth_type.value = "";
        frm_pay.EP_auth_value.value="";
            
    }
    
    
    function calVat()
    {
        var varTaxFlag =  NulltoZero(document.all["tax_flag"].value).replace(/,/g, '');
        var appAmt = NulltoZero(document.all["EP_tot_amt"].value).replace(/,/g, '');
        var svcAmt = NulltoZero(document.all["EP_service_amt"].value).replace(/,/g, '');
        //과세
        if(varTaxFlag == '00')
        {
            var cal_vat_amt = Math.floor( parseInt(appAmt)*(1/11)) ;
            var cal_svc_amt=  0;
            
            document.all["EP_tot_amt"].value = appAmt;
            document.all["EP_service_amt"].value = 0;
            document.all["EP_vat"].value = cal_vat_amt;
        }
        //비과세
        else  if(varTaxFlag == '01')
        {
            var cal_vat_amt = Math.floor( parseInt(appAmt)*(1/11)) ;
            var cal_svc_amt=  0;
            
            document.all["EP_tot_amt"].value = appAmt;
            document.all["EP_service_amt"].value = appAmt;
            document.all["EP_vat"].value = 0;
        }   
    }
    
    function changeSvc()
    {
        var svcAmt = NulltoZero(document.all["EP_service_amt"].value).replace(/,/g, '');
        var appAmt = NulltoZero(document.all["EP_tot_amt"].value).replace(/,/g, '');
        var cal_vat_amt = Math.floor( (parseInt(appAmt)-parseInt(svcAmt))*(1/11)) ;
        
        document.all["EP_vat"].value = cal_vat_amt;
          
    }
    
    function IssueTypeChange()
    {
        var tpisstype = document.all["EP_issue_type"].value;
        
        if(tpisstype == '03')
        {
            document.all["EP_auth_type"].value = '03';
            document.all["EP_auth_value"].value = '0100001234';
            document.all["EP_auth_value"].readOnly = true;
        }
        else if(tpisstype == '02')
        {
            document.all["EP_auth_type"].value='04';
           document.all["EP_auth_value"].value = '';
            document.all["EP_auth_value"].readOnly = false;
        }
        else if(tpisstype == '01')
        {
            document.all["EP_auth_type"].value='03';
           document.all["EP_auth_value"].value = '';
            document.all["EP_auth_value"].readOnly = false;
        }        
        else
        {
            document.all["EP_auth_type"].value = '';
            document.all["EP_auth_value"].value = '';
            document.all["EP_auth_value"].readOnly = false;
        }
    }
    
    function ChkIssueTypeChange()
    {
        var tpisstype = document.all["EP_issue_type"].value;
        var tpauthtype = document.all["EP_auth_type"].value;
        if(tpisstype == '03')
        {
            if(tpauthtype != '03')
            {                
                document.all["EP_auth_type"].value = '03';
                alert('자진발급시 인증구분은 휴대폰번호만 가능합니다.');
            }            
        }
  
    }
	function progress_open(el){

		var temp = $('#' + el);		//레이어의 id를 temp변수에 저장
                temp.fadeIn();	//bg 클래스가 없으면 일반레이어로 실행한다.
		
		// 화면의 중앙에 레이어를 띄운다.
		if (temp.outerHeight() < $(document).height() ) temp.css('margin-top', '-'+temp.outerHeight()/2+'px');
		else temp.css('top', '0px');
		if (temp.outerWidth() < $(document).width() ) temp.css('margin-left', '-'+temp.outerWidth()/2+'px');
		else temp.css('left', '0px');
                
	}	
        
        function progress_close(el){
            var temp = $('#' + el);		//레이어의 id를 temp변수에 저장
            temp.fadeOut();		//'닫기'버튼을 클릭하면 레이어가 사라진다.
	}
        
        
</script>
</head>
<body onload="f_init();">
<!--
    <form name="frm_pay" method="post" action="../easypay_request_bak.jsp">
-->
<form name="frm_pay" id="frm_pay" method="post" action="../app/appCashHiddenAction">
    <!-- 사업자 과세(00:과세) 비과세(01) 구분 -->
 <input type="hidden" name="tax_flag" value="${onfftid_info.tax_flag}">   
 <input type="hidden" name="param_onffmerch_no" value="${onfftid_info.onffmerch_no}">   
 <input type="hidden" name="param_onfftid" value="${onfftid_info.onfftid}">    
    
<!-- 거래구분(수정불가) -->
<input type="hidden" name="EP_tr_cd" value="00201050">
<!-- 결제수단(수정불가) -->
<input type="hidden" name="EP_pay_type" value="cash">
<!-- 요청구분(수정불가) -->
<input type="hidden" name="EP_req_type" value="issue">
<!--가맹점 주문번호 -->
<input type="hidden" name="EP_order_no" value="${orderseq}" >
<!--하위가맹점사용여부 0:미사용 1:사용-->
<input  type="hidden" name="EP_sub_mall_yn" value="1"/>
<!--하위가맹점사업자번호-->
<input  type="hidden" name="EP_sub_mall_buss" value="${onfftid_info.biz_no}"/>
<table border="1" width="100%" cellpadding="10" cellspacing="0">
<tr>
    <td>
    <table class="gridtable" height="100%" width="100%">
    <tr>
        <td height="30" bgcolor="#FFFFFF" colspan="4"><b>주문 정보</b>(*필수)</td>
    </tr>  
    <tr height="25">
    	<td class="headcol" width="80">&nbsp;상점명</td>
        <td bgcolor="#FFFFFF"  colspan="3">&nbsp;<input type="text" name="onff_merch_nm" value="${onfftid_info.comp_nm}" size="30" readonly>
        </td>
    </tr>     
    <tr >
        <td class="headcol"  >&nbsp;*고객명</td>
        <td bgcolor="#FFFFFF" >&nbsp;<input type="text" name="EP_user_nm" size="30" ></td>
        <td class="headcol" >&nbsp;*고객전화번호(- 제외입력)</td>
        <td bgcolor="#FFFFFF">&nbsp;<input type="text" name="EP_user_phone2" size="30" onkeyup="javascript:InpuOnlyNumber(this);"></td>        
    </tr>            
    <tr>
        <td class="headcol" >&nbsp;*상품명</td>
        <td bgcolor="#FFFFFF" colspan="3">&nbsp;<input type="text" name="EP_product_nm" size="40" ></td>
    </tr>
    <tr>
        <td height="30" bgcolor="#FFFFFF" colspan="4"><b>현금영수증정보</b>(*필수)</td>
    </tr>    
    <tr>        
        <td class="headcol" >&nbsp;*발행용도</td>
        <td bgcolor="#FFFFFF" >&nbsp;<select name="EP_issue_type" onchange="javascript:IssueTypeChange();" >
            <option value="01" selected>소득공제용</option>
            <option value="02">지출증빙용</option>            
            <option value="03">자진발급</option>            
        </select></td>
        <td class="headcol" >&nbsp;*인증구분</td>
        <td bgcolor="#FFFFFF">&nbsp;<select name="EP_auth_type" onchange="javascript:ChkIssueTypeChange();">
            <option value="" >선택</option>
            <option value="02">주민등록번호</option>
            <option value="03" selected>휴대폰번호</option>
            <option value="04">사업자번호</option>
        </select></td>
    </tr>
    <tr >
        <td class="headcol" >&nbsp;*인증번호</td>
        <td bgcolor="#FFFFFF" colspan="3">&nbsp;<input type="text" name="EP_auth_value" value="" size="20" maxlength="13" onkeyup="javascript:InpuOnlyNumber(this);">&nbsp;'-'없이 인증구분 해당값 입력</td>      
    </tr>        
    <tr>
        <td class="headcol" >&nbsp;*거래총금액</td>
        <td bgcolor="#FFFFFF" colspan="3"><input type="text" name="EP_tot_amt" maxlength="15" size="30"  onkeyup="javascript:amtToHan($(this),$('#frm_pay span[name=insert_amt_han]'),Number($(this).attr('maxlength')));" onblur="javascript:calVat();" >&nbsp;<span name="insert_amt_han"></span><span>원</span></td>
    </tr>
    <tr>    
        <td class="headcol" >*봉사료</td>
        <td bgcolor="#FFFFFF"  colspan="3"><input type="text" name="EP_service_amt" size="30" onblur="javascript:changeSvc();" onkeyup="javascript:InpuOnlyNumber(this);"></td>
    </tr>
    <tr>        
        <td class="headcol" >*부가세</td>
        <td bgcolor="#FFFFFF" colspan="3"><input type="text" name="EP_vat" size="30" onkeyup="javascript:InpuOnlyNumber(this);"></td>
    </tr>      
   <tr>
       <td colspan="4" height="30" align="center" bgcolor="#FFFFFF"><input type="button" value="현금영수증 승인" class="input_D" style="cursor:hand;font-size:12pt; font-weight:bold;" onclick="javascript:f_submit();"></td>
    </tr>
    </table>
    </td>
</tr>
</table>
</form>
<div id="progresss" class="pop-layer">
    <div class="pop-container">
          <img src="<c:url value="/resources/images/loders.gif"/>" width="220" height="19" border="0" alt="">
    </div>
</div>        
</body>
</html>
