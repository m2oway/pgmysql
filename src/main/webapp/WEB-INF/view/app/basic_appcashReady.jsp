<%-- 
    Document   : appList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="/WEB-INF/view/common/basic_common.jsp"/>
<%@include file="/WEB-INF/view/common/basic_comm_header.jsp"%>
<%
    String strCurDate = com.onoffkorea.system.common.util.Util.getTodayDate();
    String strTpDefaultEndDt = "20991231";
%>
            <script type="text/javascript">
                var regobj = new RegExp('-',"gi");
                var dhxSelPopupWins;
		dhxSelPopupWins = new dhtmlXWindows();
                               
                $(document).ready(function () {
                    
                  
                //applistd 정보 등록 이벤트
                $("#appRedyForm input[name=startapp]").click(function(){
                      reqAppStart(); 
                     });              
                    
                });
                
                function reqAppStart()
                {
                   var strUrl = "../app/appCashReqForm?param_onffmerch_no="+$("#appRedyForm select[name=onffmerch_no]").val()+"&param_onfftid="+$("#appRedyForm select[name=onfftid]").val();
                   var winx2 = window.open(strUrl,"현금영수증승인", "width=800,height=450,scrollbars=yes,resizeable=no");
                   winx2.focus();
                }
                
                
            </script>
            <form id="appRedyForm" method="POST" action="" >
                <table class="gridtable" width="100%">                    
                    <tr>
                        <td align='left' colspan='4'>- 현금영수증결제요청</td>                        
                    </tr>
                    <tr>                        
                        <td class="headcol" width="20%">지급ID</td>
                        <td > <select name="onffmerch_no" >
                               ${onffmerch_info}
                            </select> </td>
                        <td class="headcol">UID선택*</td>
                        <td>
                            <select name="onfftid" >
                               ${onfftid_info}
                            </select> 
                        </td>                        
                    </tr>
                    <tr>
                        <td colspan="4" align="center"><input type="button" name="startapp" value="현금영수증 승인" style="font-size:12pt; font-weight:bold;"/></td>
                    </tr>
                </table>
                </form>  
<%@include file="/WEB-INF/view/common/basic_comm_tail.jsp"%>  
<script type="text/javascript">

                var tabbar;
                var menu;

                $(document).ready(function(){

                    var height=0;
                    var minusHight =(Number($(".settlementPaymentMenu").css("height").replace(/px/g,"")) + Number($(".loginInfo").css("height").replace(/px/g,""))+ Number($(".fotter").css("height").replace(/px/g,"")));


                    menu = new dhtmlXMenuObject("menuObj");
                    menu.setIconsPath("<c:url value="/resources/images/dhx/dhtmlxMenu/" />");
                    
                    var user_cate = '${siteSessionObj.user_cate}';
                    
                    //정산관리자, 결제관리자
                   if(user_cate === '01' || user_cate === '02'){
                        menu.loadXML("<c:url value="/resources/menu/dhx_menu_links_basicterm.xml" />");
                        
                    }
                    
                    $("#logoutbtn").click(function(){
                        location.href = "${pageContext.request.contextPath}/login/logout";
                    });
                    

                });
                
                
                function basicmenu_move(moveaddr)
                {
                    location.href = "${pageContext.request.contextPath}"+ moveaddr;
                }

            </script>
