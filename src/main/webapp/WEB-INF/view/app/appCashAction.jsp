<%-- 
    Document   : appList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    Date today=new Date();
    SimpleDateFormat formater = new SimpleDateFormat("yyyy");       
    String strCurYear = formater.format(today);
%>
<html>
<head>
<title>okpay 카드결제</title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<!--
<link href="../css/style.css" rel="stylesheet" type="text/css">
-->
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/main/settlementPaymentCss.css" />"  />
</head>
<body >
<form name="frm_pay" id="frm_pay" method="post" action="../app/appCashResult">
    <input type="hidden" name="tran_seq" value="${TRAN_SEQ}"/>
    <input type="hidden" name="tran_result_cd" value="${RESULT_CD}"/>
    <input type="hidden" name="tran_result_msg" value="${RESULT_MSG}"/>
</form>
<script type="text/javascript">
    <c:if test="${RESULT_CD!='0000'}">
        //alert("${RESULT_MSG}");
        //window.close();
        document.getElementById("frm_pay").submit();
    </c:if>
    <c:if test="${RESULT_CD =='0000'}">
        //alert("${RESULT_MSG}");
       // alert("${TRAN_SEQ}");
       //document.frm_pay.submit();
       document.getElementById("frm_pay").submit();
    </c:if>
</script>
</body>
</html>
