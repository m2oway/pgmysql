<%-- 
    Document   : appList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    Date today=new Date();
    SimpleDateFormat formater = new SimpleDateFormat("yyyy");       
    String strCurYear = formater.format(today);
%>
<html>
<head>
<title>okpay 카드결제</title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<!--
<link href="../css/style.css" rel="stylesheet" type="text/css">
-->
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/main/settlementPaymentCss.css" />"  />
<style type="text/css">
.pop-layer {display:none; position: absolute; top: 50%; left: 50%; width: 650px; height:auto;  background-color:#fff; border: 5px solid #3571B5; z-index: 10;} 
.pop-layer .pop-container {padding: 20px 25px;}
.pop-layer p.ctxt {color: #666; line-height: 25px;}
.pop-layer .btn-r {width: 100%; margin:10px 0 20px; padding-top: 10px; border-top: 1px solid #DDD; text-align:right;}
a.cbtn {display:inline-block; height:25px; padding:0 14px 0; border:1px solid #304a8a; background-color:#3f5a9d; font-size:13px; color:#fff; line-height:25px;}
a.cbtn:hover {border: 1px solid #091940; background-color:#1f326a; color:#fff;}
</style>
<script type="text/javascript" src="<c:url value="/resources/javascript/jquery-1.6.2.min.js" />"></script>
<script src="<c:url value="/resources/javascript/common.js" />"></script>
<script type="text/javascript">
    <c:if test="${result_code!='0'}">
        alert("${result_msg}");
        window.close();
    </c:if>

     function NulltoZero(p_param)
     {
         if(p_param =='')
         {
             return '0';
         }
         else
         {
             return p_param;
         }
     }
     
    /* 입력 자동 Setting */
    function f_init(){

        var frm_pay = document.frm_pay;

        var today = new Date();
        var year  = today.getFullYear();
        var month = today.getMonth() + 1;
        var date  = today.getDate();
        var time  = today.getTime();

        if(parseInt(month) < 10) {
            month = "0" + month;
        }

        if(parseInt(date) < 10) {
            date = "0" + date;
        }

    }


    var bRetVal = true;

    function f_submit() {
        var frm_pay = document.frm_pay;
        //var bRetVal = false;


        /*  주문정보 확인 */
        /*
        if( !frm_pay.EP_order_no.value ) {
            alert("가맹점주문번호를 입력하세요!!");
            frm_pay.EP_order_no.focus();
            return;
        }
        */
       
        frm_pay.EP_product_amt.value = parseInt(NulltoZero(frm_pay.EP_product_amt.value).replace(/,/g, ''));
        frm_pay.EP_com_tax_amt.value = parseInt(NulltoZero(frm_pay.EP_com_tax_amt.value).replace(/,/g, ''));
        frm_pay.EP_com_free_amt.value = parseInt(NulltoZero(frm_pay.EP_com_free_amt.value).replace(/,/g, ''));
        frm_pay.EP_com_vat_amt.value = parseInt(NulltoZero(frm_pay.EP_com_vat_amt.value).replace(/,/g, ''));            
       
        var tp_totamt = parseInt(NulltoZero(frm_pay.EP_product_amt.value).replace(/,/g, ''));
        var tp_taxamt = parseInt(NulltoZero(frm_pay.EP_com_tax_amt.value).replace(/,/g, ''));
        var tp_free_amt = parseInt(NulltoZero(frm_pay.EP_com_free_amt.value).replace(/,/g, ''));
        var tp_vat_amt = parseInt(NulltoZero(frm_pay.EP_com_vat_amt.value).replace(/,/g, ''));       
       //tmpappchkamt.replace(/,/g, '')

        if( !frm_pay.EP_product_amt.value ) {
            alert("결제금액을 입력하세요!!");
            frm_pay.EP_product_amt.focus();
            return;
        }
        
        if( !frm_pay.EP_product_nm.value ) {
            alert("삼품명을 입력하세요!!");
            frm_pay.EP_product_nm.focus();
            return;
        }        
        
        if( !frm_pay.EP_user_phone2.value ) {
            alert("고객휴대폰을 입력하세요!!");
            frm_pay.EP_user_phone2.focus();
            return;
        }    
        
        if( !frm_pay.EP_user_nm.value ) {
            alert("고객명을 입력하세요!!");
            frm_pay.EP_user_nm.focus();
            return;
        }                  

        /* 결제금액 설정 */
        frm_pay.EP_tot_amt.value = frm_pay.EP_card_amt.value = frm_pay.EP_product_amt.value;

        frm_pay.EP_card_no.value = frm_pay.card_no1.value + frm_pay.card_no2.value + frm_pay.card_no3.value + frm_pay.card_no4.value;
        if ( frm_pay.EP_card_no.value.length < 13 )
         {
            alert("신용카드번호를 입력하세요.!!");
            frm_pay.card_no1.focus();
            return;
        }
        if( !frm_pay.expire_yy.value ) {
            alert("유효기간(년) 선택하세요.!!");
            frm_pay.expire_yy.focus();
            return;
        }

        if( !frm_pay.expire_mm.value ) {
            alert("유효기간(월) 선택하세요.!!");
            frm_pay.expire_mm.focus();
            return;
        }
        frm_pay.EP_expire_date.value = frm_pay.expire_yy.value.substring(2, 4) + frm_pay.expire_mm.value;
        


        if(tp_totamt < 0 )
        {
            alert("결제금액이 잘못되었습니다.");
            frm_pay.EP_product_amt.focus();
            return;
        }
        
        if(tp_taxamt < 0 )
        {
            alert("과세승인금액이 잘못되었습니다.");
            frm_pay.EP_com_tax_amt.focus();
            return;
        }
        
        if( tp_free_amt < 0 )
        {
            alert("비과세승인금액이 잘못되었습니다.");
            frm_pay.EP_com_free_amt.focus();
            return;
        }
        
        if(tp_vat_amt < 0)
        {
            alert("부가세가 잘못되었습니다.");
            frm_pay.EP_com_vat_amt.focus();
            return;
        }
        
        var chkamt = tp_taxamt+tp_free_amt+tp_vat_amt;
        if(chkamt != tp_totamt)
        {
            alert('과세승인금액,비과세승인금액,부가세금액의 합이 결제금액과 일치하지 않습니다.');
            return;
        }

    <c:if test="${onfftid_info.cert_type == '0'}">
             /* 카드구분에 따라 처리 */
            if( frm_pay.EP_card_user_type.value == "0" ) {
            	/* 개인 */
        	    if( frm_pay.EP_password.value.length != 2 ) {
        	        alert("비밀번호를 입력하세요.!!");
                    frm_pay.EP_password.focus();
                    return;
                }
                if( frm_pay.EP_auth_value.value.length != 6 ) {
        	        alert("주민등록증 생년월일을 입력 하세요.!!");
                    frm_pay.EP_auth_value.focus();
                    return;
                }
            }
            else {
            	/* 법인 */
            	frm_pay.EP_password.value = "  ";
            	if( frm_pay.EP_auth_value.value.length != 10 ) {
        	        alert("사업자등록번호를 입력하세요.!!");
                    frm_pay.EP_auth_value.focus();
                    return;
                }
            }
    </c:if>



        if( frm_pay.EP_card_amt.value < 50000 ) {
            frm_pay.EP_install_period.value = "00";
            frm_pay.EP_noint.value = "00";
        }
                
        //bRetVal = true;
        //if ( bRetVal ) frm_pay.submit();        
        if ( bRetVal ) 
        {
            bRetVal = false;
            progress_open("progresss");
            $.ajax({
                    url : "../app/appHiddenReqAction",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#frm_pay").serialize(),
                        success : function(data) {
                            if(data.result_cd == '0000')
                            {
                                alert(data.result_msg);
                                forminit();
                                bRetVal = true;
                                progress_close("progresss");
                                //window.close();
                            }
                            else
                            {
                                alert("[" +data.result_cd+ "]" + data.result_msg);
                                bRetVal = true;
                                progress_close("progresss");
                                //window.close();
                            }
                            
                        },
                        error : function() { 
                            alert("결제 실패");
                            bRetVal = true;
                            progress_close("progresss");
                        }
           });	      
        } 
        else
        {
            alert("결제 진행중입니다.");
        }
        
        
    }
    
    function forminit()
    {
        var frm_pay = document.frm_pay;

        frm_pay.EP_product_amt.value="";        
        frm_pay.EP_product_nm.value="";
        frm_pay.EP_user_phone2.value="";        
        frm_pay.EP_user_nm.value="";

        /* 결제금액 설정 */
        frm_pay.EP_tot_amt.value ="" 
        frm_pay.EP_card_amt.value ="" 
        frm_pay.EP_product_amt.value="";

        frm_pay.EP_card_no.value = "";
        frm_pay.card_no1.value = "";
        frm_pay.card_no2.value = "";
        frm_pay.card_no3.value = "";
        frm_pay.card_no4.value = "";
 
        frm_pay.expire_yy.value = "";
        frm_pay.expire_mm.value= "";
        
        frm_pay.EP_expire_date.value = "";
        
        frm_pay.EP_product_amt.value="";
        frm_pay.EP_com_tax_amt.value="";
        frm_pay.EP_com_free_amt.value="";
        frm_pay.EP_com_vat_amt.value="";
        
    <c:if test="${onfftid_info.cert_type == '0'}">
        frm_pay.EP_password.value = "";
        frm_pay.EP_auth_value.value="";
        frm_pay.EP_card_user_type.value = "0";
    </c:if>

        frm_pay.EP_install_period.value = "00";
        frm_pay.EP_noint.value = "00";

                
    }
    
    
    function gotoCardMove(sElmt1, sElmt2)
    {
        if(document.all[sElmt1].value.length == 4)
        {
            document.all[sElmt2].focus();
        }
    }
    
    function calVat()
    {
        var varTaxFlag =  NulltoZero(document.all["tax_flag"].value);
        var appAmt = NulltoZero(document.all["EP_product_amt"].value).replace(/,/g, '');
		//alert(varTaxFlag);
		//alert(appAmt);
        //과세
        if(varTaxFlag == '00')
        {
            var cal_vat_amt = Math.floor( parseInt(appAmt)*(1/11)) ;
            
            var cal_tax_amt = appAmt - cal_vat_amt;
            
            var cal_free_amt=  0;
            
            document.all["EP_com_tax_amt"].value = cal_tax_amt;
            document.all["EP_com_free_amt"].value = cal_free_amt
            document.all["EP_com_vat_amt"].value = cal_vat_amt
        }
        //비과세
        else  if(varTaxFlag == '01')
        {
            var cal_tax_amt = 0;
            var cal_free_amt= appAmt;
            var cal_vat_amt = 0;
            
            document.all["EP_com_tax_amt"].value = cal_tax_amt;
            document.all["EP_com_free_amt"].value = cal_free_amt
            document.all["EP_com_vat_amt"].value = cal_vat_amt
        }
            
    }
        
    function changeSvc()
    {
        var svcAmt = NulltoZero(document.all["EP_com_free_amt"].value).replace(/,/g, '');//바과세 승인금액
        var appAmt = NulltoZero(document.all["EP_product_amt"].value).replace(/,/g, '');
        
        var cal_vat_amt = Math.floor( (parseInt(appAmt)-parseInt(svcAmt))*(1/11)) ;
        
        document.all["EP_com_tax_amt"].value = (parseInt(appAmt)-parseInt(svcAmt)-cal_vat_amt);
        
        document.all["EP_com_vat_amt"].value = cal_vat_amt;         
    }
    
	function layer_open(el){

		var temp = $('#' + el);		//레이어의 id를 temp변수에 저장
		var bg = temp.prev().hasClass('bg');	//dimmed 레이어를 감지하기 위한 boolean 변수

		if(bg){
			$('.layer').fadeIn();
		}else{
			temp.fadeIn();	//bg 클래스가 없으면 일반레이어로 실행한다.
		}

		// 화면의 중앙에 레이어를 띄운다.
		if (temp.outerHeight() < $(document).height() ) temp.css('margin-top', '-'+temp.outerHeight()/2+'px');
		else temp.css('top', '0px');
		if (temp.outerWidth() < $(document).width() ) temp.css('margin-left', '-'+temp.outerWidth()/2+'px');
		else temp.css('left', '0px');
                
                
                document.i_frame.location.replace('../freeinstallment.jsp');

		temp.find('a.cbtn').click(function(e){
			if(bg){
				$('.layer').fadeOut();
			}else{
				temp.fadeOut();		//'닫기'버튼을 클릭하면 레이어가 사라진다.
			}
			e.preventDefault();
		});

		$('.layer .bg').click(function(e){
			$('.layer').fadeOut();
			e.preventDefault();
		});

	}	
        
        
        
	function progress_open(el){

		var temp = $('#' + el);		//레이어의 id를 temp변수에 저장
                temp.fadeIn();	//bg 클래스가 없으면 일반레이어로 실행한다.
		
		// 화면의 중앙에 레이어를 띄운다.
		if (temp.outerHeight() < $(document).height() ) temp.css('margin-top', '-'+temp.outerHeight()/2+'px');
		else temp.css('top', '0px');
		if (temp.outerWidth() < $(document).width() ) temp.css('margin-left', '-'+temp.outerWidth()/2+'px');
		else temp.css('left', '0px');
                
	}	
        
        function progress_close(el){
            var temp = $('#' + el);		//레이어의 id를 temp변수에 저장
            temp.fadeOut();		//'닫기'버튼을 클릭하면 레이어가 사라진다.
	}
        
        

</script>
</head>
<body onload="f_init();">
<!--
    <form name="frm_pay" method="post" action="../easypay_request_bak.jsp">
-->
<form name="frm_pay" id="frm_pay" method="post" action="../app/appAction">
    <!-- 사업자 과세(00:과세) 비과세(01) 구분 -->
 <input type="hidden" name="tax_flag" value="${onfftid_info.tax_flag}">   
 <input type="hidden" name="param_onffmerch_no" value="${onfftid_info.onffmerch_no}">   
 <input type="hidden" name="param_onfftid" value="${onfftid_info.onfftid}">    
    
<!-- 거래구분(수정불가) -->
<input type="hidden" name="EP_tr_cd" value="00101000">
<!-- 결제수단(수정불가) -->
<input type="hidden" name="EP_pay_type" value="card">

<!-- 결제총금액 -->
<input type="hidden" name="EP_tot_amt" value="">
<!-- 통화코드 : 00(원), 01(달러)-->
<input type="hidden" name="EP_currency" value="00">
<!-- 에스크로여부(수정불가) -->
<input type="hidden" name="EP_escrow_yn" value="N">
<!-- 복합결제여부(수정불가) -->
<input type="hidden" name="EP_complex_yn" value="N">

<!-- 카드결제종류(수정불가) -->
<input type="hidden" name="EP_req_type" value="0">
<!-- 신용카드 결제금액 -->
<input type="hidden" name="EP_card_amt" value="">
<!-- 신용카드 WCC(수정불가) -->
<input type="hidden" name="EP_wcc" value="@">
<!-- 신용카드번호 -->
<input type="hidden" name="EP_card_no" value="">
<!-- 유효기간 -->
<input type="hidden" name="EP_expire_date" value="">
<!-- 가맹점 필드 -->
<input type="hidden" name="EP_user_define1" value="">
<input type="hidden" name="EP_user_define2" value="">
<input type="hidden" name="EP_user_define3" value="">
<input type="hidden" name="EP_user_define4" value="">
<input type="hidden" name="EP_user_define5" value="">
<input type="hidden" name="EP_user_define6" value="">

<input type="hidden" name="EP_card_txtype" value="20"><!-- 처리종류 20 승인-->
<!-- 무이자 : 00 일반 02 무이자-->
<input type="hidden" name="EP_noint" value="00">
<input type="hidden" name="EP_user_type" value="" ><!-- 사용자구분 1: 일반 2:회원-->
<input type="hidden" name="EP_user_id" value="" ><!-- 고객ID-->
<input type="hidden" name="EP_user_phone1" value="" ><!--전화번호-->
<input type="hidden" name="EP_user_addr" value="" ><!-- 고객주소-->
<input type="hidden" name="EP_product_type" value="0" ><!-- 상품구분 0:실물 1:컨텐츠 -->
    <!--과세구분: ""일반,"TG01" 복합과세--> 
     <input type="hidden" name="EP_tax_flg" value="TG01" >
     <!--가맹점 주문번호 -->
     <input type="hidden" name="EP_order_no" value="${orderseq}" >

<table border="1" width="100%" cellpadding="10" cellspacing="0">
<tr>
    <td>
    <table class="gridtable" height="100%" width="100%">
    <tr>
        <td bgcolor="#FFFFFF" colspan="4">&nbsp;<b>주문정보</b>(*필수)</td>
    </tr>        
    <tr height="25">
    	<td class="headcol" width="80">&nbsp;상점명</td>
        <td bgcolor="#FFFFFF"  colspan="3"><input type="text" name="onff_merch_nm" value="${onfftid_info.comp_nm}" size="30" >
        </td>
    </tr>    
    <tr>
        <td class="headcol"  >&nbsp;*고객명</td>
        <td bgcolor="#FFFFFF" >&nbsp;<input type="text" name="EP_user_nm" size="30" ></td>
        <td class="headcol" >&nbsp;*고객전화번호(- 제외입력)</td>
        <td bgcolor="#FFFFFF">&nbsp;<input type="text" name="EP_user_phone2" size="30" onkeyup="javascript:InpuOnlyNumber(this);"></td>
    </tr>
    <tr>

        <td class="headcol" >&nbsp;*상품명</td>
        <td bgcolor="#FFFFFF" colspan="3">&nbsp;<input type="text" name="EP_product_nm" size="40" ></td>
    </tr>        
    <tr>
        <td height="30" bgcolor="#FFFFFF" colspan="4"><b>결제정보</b>(*필수)&nbsp;&nbsp;&nbsp;<input type="button" name="freeinstallbtn" value="무이자 정보 보기" onclick="layer_open('layer1');return false;"></td>
    </tr>    
    <tr height="25">
    	<td class="headcol" >&nbsp;*카드번호</td>
        <td bgcolor="#FFFFFF"  >&nbsp;<input type="text" name="card_no1" value="" size="5" maxlength="4" onKeyUp="javascript:gotoCardMove('card_no1','card_no2');" >
        &nbsp;<input type="text" name="card_no2" value="" size="5" maxlength="4" class="input_F"  onKeyUp="javascript:gotoCardMove('card_no2','card_no3');" >
        &nbsp;<input type="text" name="card_no3" value="" size="5" maxlength="4" class="input_F"  onKeyUp="javascript:gotoCardMove('card_no3','card_no4');" >
        &nbsp;<input type="text" name="card_no4" value="" size="5" maxlength="4" class="input_F"  onKeyUp="javascript:gotoCardMove('card_no4','expire_yy');">
        </td>
        <td class="headcol" >&nbsp;*유효기간</td>
        <td bgcolor="#FFFFFF" >&nbsp;<select name="expire_yy" >
        	<option value="" selected>선택</option>
<%
    int intCurYear = Integer.parseInt(strCurYear);
    
    for (int ix=0 ; ix < 15 ; ix++)
    {
%>
                <option value="<%=intCurYear+ix%>"><%=intCurYear+ix%></option>
<%
    }
%>
        </select>&nbsp;년
        &nbsp;<select name="expire_mm" >
            <option value="" selected>선택</option>
            <option value="01">01</option>
            <option value="02">02</option>
            <option value="03">03</option>
            <option value="04">04</option>
            <option value="05">05</option>
            <option value="06">06</option>
            <option value="07">07</option>
            <option value="08">08</option>
            <option value="09">09</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
        </select>&nbsp;월
        </td>
    </tr>
    <tr height="25">
        <td class="headcol" >&nbsp;*할부개월</td>
        <td bgcolor="#FFFFFF" colspan="3">&nbsp;<select name="EP_install_period" >
            <option value="00" selected>일시불</option>
            <option value="02">2개월</option>
            <option value="03">3개월</option>
            <option value="04">4개월</option>
            <option value="05">5개월</option>
            <option value="06">6개월</option>
            <option value="07">7개월</option>
            <option value="08">8개월</option>
            <option value="09">9개월</option>
            <option value="10">10개월</option>
            <option value="11">11개월</option>
            <option value="12">12개월</option>
        </select></td>
    </tr>
    <c:if test="${onfftid_info.cert_type == '1'}">
    <input type="hidden" name="EP_cert_type" value="1"/><!-- 인증구분 0:인증 1:비인증-->
    <!-- 카드구분 : 0 개인 1 법인-->
    <input type="hidden" name="EP_card_user_type" value="" />
    <input type="hidden" name="EP_password" value="" /><!--비밀번호**(앞2자리)비밀번호-->
    <input type="hidden" name="EP_auth_value" value="" /><!-- 주민등록증 생년월일(앞6자리), 사업자번호(10자리) -->
    </c:if>
 <c:if test="${onfftid_info.cert_type == '0'}">
    <tr height="25">    
        <td class="headcol">&nbsp;카드구분</td>
        <td bgcolor="#FFFFFF"  >&nbsp;<select name="EP_card_user_type" class="input_A">
            <option value="0" selected>개인</option>
            <option value="1">법인</option>
        </select><input type="hidden" name="EP_cert_type" value="0"/><!-- 인증구분 0:인증 1:비인증--></td>
        <td class="headcol">&nbsp;비밀번호</td>
        <td bgcolor="#FFFFFF" >&nbsp;<input type="password" name="EP_password" value="" size="4" maxlength="2" class="input_A">&nbsp;**(앞2자리)</td>
    </tr>
    <tr height="25">
        <td class="headcol">&nbsp;주민(사업자)등록번호</td>
        <td bgcolor="#FFFFFF" colspan="3">&nbsp;<input type="password" name="EP_auth_value" value="" size="15" class="input_A">&nbsp;주민등록번호 생년월일(앞6자리), 사업자번호(10자리)</td>
    </tr>    
</c:if>
    <tr>
        <td class="headcol" >&nbsp;*결제금액</td>
        <td bgcolor="#FFFFFF" colspan="3"><input type="text" name="EP_product_amt"  maxlength="15" size="30" onkeyup="javascript:amtToHan($(this),$('#frm_pay span[name=insert_amt_han]'),Number($(this).attr('maxlength')));" onblur="javascript:calVat();" onkeyup="javascript:InpuOnlyNumber(this);">&nbsp;<span name="insert_amt_han"></span><span>원</span></td>
    </tr>
    <tr>    
        <td class="headcol" >과세승인금액</td>
        <td bgcolor="#FFFFFF"  colspan="3"><input type="text" name="EP_com_tax_amt" size="30" onkeyup="javascript:InpuOnlyNumber(this);"></td>
    </tr>
    <tr>        
        <td class="headcol" >비과세승인금액</td>
        <td bgcolor="#FFFFFF" colspan="3"><input type="text" name="EP_com_free_amt" size="30" onblur="javascript:changeSvc();" onkeyup="javascript:InpuOnlyNumber(this);"></td>
    </tr>    
    <tr>        
        <td class="headcol" >부가세</td>
        <td bgcolor="#FFFFFF" colspan="3"><input type="text" name="EP_com_vat_amt" size="30" onkeyup="javascript:InpuOnlyNumber(this);" ></td>
    </tr>       
   <tr>
       <td colspan="4" height="30" align="center" bgcolor="#FFFFFF"><input type="button" value="카드 승인" class="input_D" style="cursor:hand;font-size:12pt; font-weight:bold;" onclick="javascript:f_submit();" ></td>
    </tr>      
    </table>
    </td>
</tr>
</table>
</form>
<div id="layer1" class="pop-layer">
    <div class="pop-container">
        <div class="pop-conts">
            <!--content //-->
            <iframe name="i_frame" src="#" width="600" height="300" marginwidth="0" marginheight="0" frameborder="no" scrolling="auto" ></iframe>
            <div class="btn-r">
                <a href="#" class="cbtn">Close</a>
            </div>
        <!--// content-->
        </div>
    </div>
</div>
<div id="progresss" class="pop-layer">
    <div class="pop-container">
          <img src="<c:url value="/resources/images/loders.gif"/>" width="220" height="19" border="0" alt="">
    </div>
</div>        

</body>
</html>
