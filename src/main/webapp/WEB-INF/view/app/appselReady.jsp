<%-- 
    Document   : appList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!ajaxRequest}">
    <jsp:include page="/WEB-INF/view/common/common.jsp"/>
    <html>
        <head>
            <title>결제</title>
        </head>
        <body>
</c:if>
<%
    String strCurDate = com.onoffkorea.system.common.util.Util.getTodayDate();
    String strTpDefaultEndDt = "20991231";
%>
            <script type="text/javascript">
                var regobj = new RegExp('-',"gi");
                var dhxSelPopupWins;
		dhxSelPopupWins = new dhtmlXWindows();
                               
                $(document).ready(function () {
                    
                  
                //applistd 정보 등록 이벤트
                $("#appCardRedyForm input[name=startcardapp]").click(function(){
                      reqCardAppStart(); 
                     });              
                    
                });
                
                function reqCardAppStart()
                {
                   var strUrl = "../app/appReqForm?param_onffmerch_no="+$("#appCardRedyForm select[name=onffmerch_no]").val()+"&param_onfftid="+$("#appCardRedyForm select[name=onfftid]").val();
                   var winx = window.open(strUrl,"카드결제", "width=800,height=520,scrollbars=yes,resizeable=no");
                   winx.focus();
                }
                
                
            </script>
            <form id="appCardRedyForm" method="POST" action="" >
                <table class="gridtable" height="100%" width="100%">                    
                    <tr>
                        <td align='left' colspan='4'>- 승인결제요청</td>                        
                    </tr>
                    <tr>                        
                        <td class="headcol" width="20%">지급ID</td>
                        <td > <select name="onffmerch_no" >
                               ${onffmerch_info}
                            </select> </td>
                        <td class="headcol">UID선택*</td>
                        <td>
                            <select name="onfftid" >
                               ${onfftid_info}
                            </select> 
                        </td>                        
                    </tr>
                    <tr>
                        <td colspan="4" align="center"><input type="button" name="startcardapp" value="카드 승인" style="font-size:12pt; font-weight:bold;"/></td>
                    </tr>
                </table>
                </form>  
<c:if test="${!ajaxRequest}">    
        </body>
    </html>
</c:if>

