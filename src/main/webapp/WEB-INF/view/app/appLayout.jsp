<%-- 
    Document   : transMaster
    Created on : 2014. 11. 18, 오전 11:21:02
    Author     : Administrator
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/view/common/common.jsp"/>

<c:if test="${!ajaxRequest}">
    <html>
        <head>
            <title>결제요청</title>
        </head>
        <body>
</c:if>

    <script type="text/javascript">
        
                var main_app_layout={};
                $(document).ready(function () {

                    var tabBarId = tabbar.getActiveTab();

                    main_app_layout = tabbar.cells(tabBarId).attachLayout("2E");

                    //main_app_layout = new dhtmlXLayoutObject("AppLayout", "2E", "dhx_skyblue");

                    main_app_layout.cells('a').hideHeader();
                    main_app_layout.cells('b').hideHeader();
                    
                    main_app_layout.cells('a').setHeight(160);
                    
                    main_app_layout.cells('a').attachURL("<c:url value="/app/appselReady" />",true);           
                    //main_app_layout.cells('b').attachURL("<c:url value="/app/appDetailList" />",true);  
            });
             
            
    </script>
         <div id="AppLayout" style="width:100%;height:100%;background-color:white;"></div>
    </body>
</html>
