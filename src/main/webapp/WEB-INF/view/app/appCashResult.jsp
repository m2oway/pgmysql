<%-- 
    Document   : appList
    Created on : 2014. 10. 29, 10:28:34
    Author     : Administrator
--%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<title>okpay 현금영수증 결제 결과</title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/main/settlementPaymentCss.css" />"  />
<script type="text/javascript" src="<c:url value="/resources/javascript/jquery-1.6.2.min.js" />"></script>
<script type="text/javascript">
    window.resizeTo(400,740);
<c:if test="${tran_result_cd =='0000'}">    
    function SendEmail()
    {
         var tpcnclmsg = document.frm_cash_reuslt_pop.recvmailaddr.value;
        if(tpcnclmsg == '')
        {
            alert('메일주소를 입력하시기 바랍니다.');
            document.frm_cash_reuslt_pop.recvmailaddr.focus();
            return;
        }
        
        if(confirm("메일을 발송하시겠습니까?"))
        {
             $.ajax({
                        url : "../app/cashSendMail",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#frm_cash_reuslt_pop").serialize(),
                        success : function(data) {
                            if(data.result_cd == '0000')
                            {
                                alert(data.result_msg);
                                window.close();
                            }
                            else
                            {
                                alert("[" +data.result_cd+ "]" + data.result_msg);
                                window.close();
                            }
                            
                        },
                        error : function() { 
                            alert("메일발송 실패");
                        }
                    });	            
        }
    }
 </c:if>     
  <c:if test="${appFormBean.user_cate == '00' ||appFormBean.user_cate == '01' || appFormBean.user_cate == '02'}">         
      <c:if test="${tran_info_obj.tran_status == '00'}">    
    function cnclCash()
    {
        var tpcnclmsg = document.frm_cash_reuslt_pop.mgr_msg.value;
        if(tpcnclmsg == '')
        {
            alert('취소사유를 입력하시기 바랍니다.');
            document.frm_cash_reuslt_pop.mgr_msg.focus();
            return;
        }
        
        if(confirm("현금영수증발급을 취소하시겠습니까?"))
        {
             $.ajax({
                        url : "../app/mgrAction",
                        type : "POST",
                        async : true,
                        dataType : "json",
                        data: $("#frm_cash_reuslt_pop").serialize(),
                        success : function(data) {
                            if(data.result_cd == '0000')
                            {
                                alert(data.result_msg);
                                window.close();
                            }
                            else
                            {
                                alert("[" +data.result_cd+ "]" + data.result_msg);
                                window.close();
                            }
                            
                        },
                        error : function() { 
                            alert("취소 실패");
                        }
                    });	            
        }
    }
        </c:if>   
</c:if>    
</script>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<c:if test="${tran_result_cd =='0000'}">
<form name="frm_cash_reuslt_pop" id="frm_cash_reuslt_pop" method="post" >
<div id="divMsg" ondragstart="return false" onselectstart="return false" oncontextmenu="return false">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td height="5"></td>
	</tr>
	<tr>
		<td width="100%" valign="middle" align="center" height="30">
		<div id="divAD" style="width:330" align="left" style="font-size:8pt">
			배경까지 인쇄하셔야 아래의 영수증을 보실 수 있습니다.</br>
			■ 설정 : 도구&gt인터넷옵션&gt고급&gt인쇄(배경 및 이미지인쇄 선택)
		</div>
		</td>
	</tr>
	<tr>
		<td width="100%" valign="middle" align="center">
		<!--
	    /***********************************************************************
	            영수증
	    ************************************************************************/
		-->
		<table width="330" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td height="5" colspan="3"></td>
			</tr>
			<tr>
                            <td width="25"></td>
				<td width="280" align="center" valign="middle">
				<table border="0" width="280" align="center" cellpadding="0" cellspacing="1" bgcolor="#7893C8">
					<tr>
						<td width="100%" align="center" valign="middle" colspan="11" bgcolor="#FFFFFF">
						<table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#FFFFFF">
							<tr>
								<td align="center" height="35" colspan=2><u><font size="2">현 금 영 수 증</font></u></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="50%" bgcolor="#FFFFFF" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">일련번호</td>
							</tr>
							<tr>
								<td align="center" height="17">${tran_info_obj.tran_seq}</td>
							</tr>
						</table>
						</td>
						<td width="50%" bgcolor="#FFFFFF" >
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">발급종류</td>
							</tr>
                                                        <tr>
								<td align="center" height="17">${tran_info_obj.wcc_nm}</td>
							</tr>                                                        
						</table>
						</td>
						<td width="50%" align="center" valign="middle"></td>
					</tr>
					<tr>
						<td width="50%" bgcolor="#FFFFFF" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">인증번호종류</td>
							</tr>
                                                        <tr>
								<td align="center" height="17">${tran_info_obj.card_cate_nm}</td>
							</tr>
                                                        
						</table>
						</td>
						<td width="50%" bgcolor="#FFFFFF" >
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">인증번호</td>
							</tr>
							<tr>
								<td align="center" height="17">${tran_info_obj.card_num}</td>
							</tr>
						</table>
						</td>
						<td width="50%" align="center" valign="middle"></td>
					</tr>
					<tr>
						<td width="50%" bgcolor="#FFFFFF" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">유효기간</td>
							</tr>
							<tr>
								<td align="center" height="17"></td>
							</tr>
						</table>
						</td>
						<td width="50%" bgcolor="#FFFFFF" >
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">거래유형</td>
							</tr>
                                                        <c:if test="${tran_info_obj.massagetype =='10'}">
							<tr>
								<td align="center" height="17">발급</td>
							</tr>
                                                        </c:if>                                                        
                                                        <c:if test="${tran_info_obj.massagetype !='10'}">
							<tr>
								<td align="center" height="17">취소</td>
							</tr>
                                                        </c:if>                                      
						</table>
						</td>
						<td width="50%" align="center" valign="middle"></td>
					</tr>
					<tr>
						<td width="50%" bgcolor="#FFFFFF" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">거래일시</td>
							</tr>
                                                        <c:if test="${tran_info_obj.massagetype =='10'}">                                                        
							<tr>
                                                            <td align="center" height="17">${tran_info_obj.app_dt}&nbsp;${tran_info_obj.app_tm}</td>
							</tr>
                                                        </c:if>  
                                                        <c:if test="${tran_info_obj.massagetype !='10'}">
                                                            <td align="center" height="17">&nbsp;</td>
                                                        </c:if>         
						</table>
						</td>
						<td width="50%" bgcolor="#FFFFFF" >
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">취소일시</td>
							</tr>
                                                        <c:if test="${tran_info_obj.massagetype =='10'}">                                                        
							<tr>
                                                            <td align="center" height="17">&nbsp;</td>
							</tr>
                                                        </c:if>  
                                                        <c:if test="${tran_info_obj.massagetype !='10'}">
                                                            <td align="center" height="17">${tran_info_obj.app_dt}&nbsp;${tran_info_obj.app_tm}</td>
                                                        </c:if>
						</table>
						</td>
                                                <td width="50%" align="center" valign="middle"></td>
					</tr>
					
					<tr>
						<td width="30%" bgcolor="#DBE4F7">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left">품명</td>
							</tr>
						</table>
						</td>
						<td width="20%" bgcolor="#DBE4F7">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>&nbsp;</td>
							</tr>
						</table>
						</td>
						<td width="20%" bgcolor="#DBE4F7">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>&nbsp;</td>
							</tr>
						</table>
						</td>                                                
					</tr>

					<tr>
						<!--상품명-->
						<td width="30%" bgcolor="#FFFFFF" rowspan="4">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left">${tran_info_obj.product_nm}</td>
							</tr>
						</table>
						</td>
						<td width="20%" bgcolor="#FFFFFF">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							bgcolor="#DBE4F7">
							<tr>
								<td align="left">과세금액<br><font style='font-size:7pt;font-family:돋움;'>(Amount)</font></td>
							</tr>
						</table>
						</td>

						<td width="6%" bgcolor="#FFFFFF">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="right" width="6%">${ViewTaxTargetAmt}</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="20%" bgcolor="#FFFFFF">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							bgcolor="#DBE4F7">
							<tr>
								<td align="left">봉사료<br>
								<font style='font-size:7pt;font-family:돋움;'>(Service)</font></td>
							</tr>
						</table>
						</td>

						<td width="6%" bgcolor="#FFFFFF">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="right" width="6%">${ViewSvcAmt}</td>
							</tr>
						</table>
						</td>
					</tr>                                        
					<tr>
						<td width="20%" bgcolor="#FFFFFF">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							bgcolor="#DBE4F7">
							<tr>
								<td align="left">세금 <br>
								<font style='font-size:7pt;font-family:돋움;'>(TAXES)</font></td>
							</tr>
						</table>
						</td>

						<td width="6%" bgcolor="#FFFFFF">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="right" width="6%">${ViewTaxAmt}</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="20%" bgcolor="#FFFFFF">
						<table width="100%" border="0" cellspacing="0" cellpadding="0"
							bgcolor="#DBE4F7">
							<tr>
								<td align="left">합계 <br>
								<font style='font-size:7pt;font-family:돋움;'>(TOTALS)</font></td>
							</tr>
						</table>
						</td>

						<td width="6%" bgcolor="#FFFFFF">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="right" width="6%">${ViewTotAmt}</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="50%" bgcolor="#FFFFFF" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">승인번호/고유번호</td>
							</tr>
							<tr>
								<td align="center" height="17">${tran_info_obj.app_no}</td>
							</tr>
						</table>
						</td>
						<td width="50%" bgcolor="#FFFFFF" >
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">고객명</td>
							</tr>
							<tr>
								<td align="center" height="17">${tran_info_obj.user_nm}</td>
							</tr>
						</table>
						</td>
					</tr>
					<!-- CTL 관련 추가 -->                                        
                                        <tr>
                                            <td width="50%" bgcolor="#9bcdff" colspan="3" align="center"><b>공급자 정보</b></td>
					<tr>                                        
					<tr>
						<td width="50%" bgcolor="#FFFFFF" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">공급자 상호<br>
								</td>
							</tr>
							<tr>
								<td align="center" height="17">${tran_info_obj.comp_nm}</td>
							</tr>
						</table>
						</td>
						<td width="50%" bgcolor="#FFFFFF" >
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">사업자등록번호</td>
							</tr>
							<tr>
								<td align="center" height="17">${tran_info_obj.biz_no}</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="50%" bgcolor="#FFFFFF" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">대표자명<br>
								</td>
							</tr>
							<tr>
								<td align="center" height="17">${tran_info_obj.comp_ceo_nm}</td>
							</tr>
						</table>
						</td>
						<td width="50%" bgcolor="#FFFFFF" >
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">이용문의</td>
							</tr>
							<tr>
								<td align="center" height="17">${tran_info_obj.comp_tel1}</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="100%" bgcolor="#FFFFFF" colspan="9">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">사업장주소</td>
							</tr>
							<tr>
                                                            <td align="center" height="17">${tran_info_obj.addr_1}&nbsp;${tran_info_obj.addr_2}</td>
							</tr>
						</table>
						</td>
					</tr>	
                                        <tr>
                                            <td width="50%" bgcolor="#9bcdff" colspan="3" align="center"><b>가맹점 정보</b></td>
					<tr>                                        
					<tr>
						<td width="50%" bgcolor="#FFFFFF" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">가맹점명</td>
							</tr>
							<tr>
	
								<td align="center" height="17">${onffCompInfo.comp_nm}</td>
					
							</tr>
						</table>
						</td>
						<td width="50%" bgcolor="#FFFFFF" >
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">사업자등록번호</td>
							</tr>
							<tr>
	
								<td align="center" height="17">${ViewOnffBizNo}</td>
					
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="50%" bgcolor="#FFFFFF" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">대표자</td>
							</tr>
							<tr>
	
								<td align="center" height="17">${onffCompInfo.comp_ceo_nm}</td>
					
							</tr>
						</table>
						</td>                                            
						<td width="50%" bgcolor="#FFFFFF" colspan="2">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">승인관련문의</td>
							</tr>
							<tr>
								<td align="center" height="17">${onffCompInfo.comp_tel1}</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="100%" bgcolor="#FFFFFF" colspan="3">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7">가맹점주소</td>
							</tr>
							<tr>
								<td align="center" height="17">${onffCompInfo.addr_1}<br>${onffCompInfo.addr_2}</td>
							</tr>
						</table>
						</td>
					</tr>                                        
                                      <tr>
                                            <td width="330" height="30" align="center" valign="middle" colspan="3"><table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                                <td align="center" bgcolor="#DBE4F7" width="100">발송 메일주소</td>
                                                                                <td align="center" bgcolor="#FFFFFF" ><input type="text" name="recvmailaddr" size="30" ><input type="hidden" name="tran_seq" value="${tran_info_obj.tran_seq}"></td>
                                                                        </tr>
                                                                </table></td>
                                        </tr>                                           
    <c:if test="${appFormBean.user_cate == '00' ||appFormBean.user_cate == '01' || appFormBean.user_cate == '02'}">  
         <c:if test="${tran_info_obj.tran_status == '00'}">             
             <!--
                                        <tr>
                                            <td width="330" height="30" align="center" valign="middle" colspan="3"><table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                                <td align="center" bgcolor="#DBE4F7"  width="100" >취소사유</td>
                                                                                <td align="center" bgcolor="#FFFFFF" ><input type="text" name="mgr_msg" size="30" ></td>
                                                                        </tr>
                                                                </table></td>
                                        </tr>   
             -->
         </c:if>                                        
   </c:if>                                        
				</table><input type="hidden" name="org_cno" value="${tran_info_obj.pg_seq}"><input type="hidden" name="EP_tr_cd" value="00201050"><input type="hidden" name="mgr_txtype" value="51">
				<!--border end--></td>
				<td width="25"></td>
			</tr>
			<tr>
				<td width="330" height="5" align="center" valign="middle" colspan="3"></td>
			</tr>    
			<tr>
                            <td width="330" height="18" align="center" valign="middle" colspan="3"><input type="button" value="메일발송" onclick="javascript:SendEmail();" />&nbsp;<input type="button" value="인쇄" onclick="window.print();" />&nbsp;<input type="button" value="닫기" onclick="window.close()" /></td>
                        </tr>
<!--                        
    <c:if test="${appFormBean.user_cate == '00' ||appFormBean.user_cate == '01' || appFormBean.user_cate == '02'}">         
			<tr>
                            <td width="330" height="18" align="center" valign="middle" colspan="3">
<c:if test="${tran_info_obj.tran_status == '00'}">  
                                <input type="button" value="취소" onclick="javascript:cnclCash();" />&nbsp;
         </c:if>                                  
                                <input type="button" value="메일발송" onclick="javascript:SendEmail();" />&nbsp;<input type="button" value="인쇄" onclick="window.print();" />&nbsp;<input type="button" value="닫기" onclick="window.close()" /></td>
			</tr>
   </c:if>  
    <c:if test="${appFormBean.user_cate != '00' && appFormBean.user_cate != '01' && appFormBean.user_cate != '02'}">                                    
			<tr>
                            <td width="330" height="18" align="center" valign="middle" colspan="3"><input type="button" value="메일발송" onclick="javascript:SendEmail();" />&nbsp;<input type="button" value="인쇄" onclick="window.print();" />&nbsp;<input type="button" value="닫기" onclick="window.close()" /></td>
			</tr>
   </c:if>     
-->
                </table>
		<!--영수증 end-->
		</td>
	</tr>
</table>
</div>
</form>
</c:if>
<c:if test="${tran_result_cd !='0000'}">
<form name="frm_pay" method="post" >
<div id="divMsg" ondragstart="return false" onselectstart="return false" oncontextmenu="return false">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr>
		<td height="5"></td>
	</tr>
	<tr>
		<td width="100%" valign="middle" align="center">
		<!--
	    /***********************************************************************
	            영수증
	    ************************************************************************/
		-->
		<table width="330" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td height="5" colspan="3"></td>
			</tr>
			<tr>
                            <td width="25"></td>
				<td width="280" align="center" valign="middle">
				<table border="0" width="280" align="center" cellpadding="0" cellspacing="1" bgcolor="#7893C8">
					<tr>
						<td width="100%" align="center" valign="middle" colspan="11" bgcolor="#FFFFFF">
						<table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#FFFFFF">
							<tr>
								<td align="center" height="35" colspan=2><u><font size="2">카 드 승 인 결 과</font></u></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="100%" bgcolor="#FFFFFF" >
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7" align="center">오류코드</td>
							</tr>
							<tr>
								<td align="center" height="17">${tran_result_cd}</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td width="100%" bgcolor="#FFFFFF" >
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="left" bgcolor="#DBE4F7" align="center">오류내용</td>
							</tr>
							<tr>
								<td align="center" height="17">${tran_result_msg}</td>
							</tr>
						</table>
						</td>
						<td width="50%" align="center" valign="middle"></td>
					</tr>
				</table>
				<!--border end--></td>
				<td width="25"></td>
			</tr>
			<tr>
				<td width="330" height="5" align="center" valign="middle" colspan="3"></td>
			</tr>                        
			<tr>
                            <td width="330" height="18" align="center" valign="middle" colspan="3"><input type="button" value="close" onclick="window.close()" /></td>
			</tr>
		</table>
		<!--영수증 end-->
		</td>
	</tr>
</table>
</div>
</form>    
</c:if>
</body>
</html>
