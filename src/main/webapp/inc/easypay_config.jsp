<%@ page contentType="text/html; charset=euc-kr" %>
<%
	request.setCharacterEncoding("euc-kr");
	
	// ============================================================================== 
	// =   PAGE : 결제 정보 환경 설정 PAGE                                     		= 
	// = -------------------------------------------------------------------------- = 
	// =   Copyright (c)  2010   KICC Inc.   All Rights Reserved.                   = 
	// ============================================================================== 
	
	
	// ============================================================================== 
	// =   01. 지불 데이터 셋업 (업체에 맞게 수정)                             		= 
	// = -------------------------------------------------------------------------- = 
	// = ※ 주의 ※                                                           		= 
	// =  cert_file 변수 설정                                                 		= 
	// = pg_cert.pem 파일이 있는 디렉토리의  절대 경로 설정                         = 
	// =                                                                            = 
	// =  log_dir 변수 설정                                                   		=
	// = log 디렉토리 설정                                                    		=
	// =  log_level 변수 설정                                                 		=
	// = log 레벨 설정                                                        		=
	// = -------------------------------------------------------------------------- =
	
	//String g_cert_file  = "/user/pgweb/jspdoc/WEB-INF/client-cert";
        //String g_cert_file  = "/usr/local/tomcat/webapp/takeon/WEB-INF/client-cert";
        String g_cert_file  = "G:/ide_works/netbeans80/takeon/src/main/webapp/WEB-INF/cert";
	//String g_log_dir    = "/user/pgweb/jspdoc/test/SohnTest/easypay70_npi_jsp/log";
        //String g_log_dir    = "/usr/local/tomcat/webapp/takeon/WEB-INF/log";
        String g_log_dir    = "C:/easypay/log";
	int g_log_level  = 1;
	
	// ============================================================================== /
	// =   02. 쇼핑몰 지불 정보 설정                                                = /
	// = -------------------------------------------------------------------------- = /		
	String g_gw_url    = "testgw.easypay.co.kr";  // Gateway URL ( test )
	//String g_gw_url    = "gw.easypay.co.kr";     // Gateway URL ( real )
	String g_gw_port   = "80";                  // 포트번호(변경불가) /
	
	String g_mall_id   = "T5102001";             // 리얼 반영시 KICC에 발급된 mall_id 사용
        //String g_mall_id   = "05523591";             // 리얼 반영시 KICC에 발급된 mall_id 사용
	String g_mall_name = "테스트 상점";    
	// ============================================================================= /
%>
