/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//숫자만 입력

var tabbar ={};
var onoffObject={};
onoffObject.onfftid="";                 
onoffObject.onfftid_nm="";
onoffObject.onffmerch_no ="";
onoffObject.merch_nm=""; 

onoffObject.comp_nm=""; 
onoffObject.comp_seq=""; 
onoffObject.biz_no=""; 
onoffObject.comp_cate_nm=""; 
onoffObject.corp_no=""; 
onoffObject.comp_ceo_nm=""; 
onoffObject.comp_tel1=""; 
onoffObject.comp_tel2=""; 
onoffObject.zip_cd=""; 
onoffObject.addr=""; 
onoffObject.addr_1=""; 
onoffObject.addr_2=""; 
onoffObject.biz_cate=""; 
onoffObject.biz_type=""; 
onoffObject.tax_flag=""; 
onoffObject.agent_seq ="";
onoffObject.agent_nm=""; 

onoffObject.other_nm=""; 
onoffObject.other_zip_cd=""; 
onoffObject.other_addr_1=""; 
onoffObject.other_addr_2=""; 

onoffObject.other_tel1=""; 
onoffObject.other_tel2=""; 

function InpuOnlyNumber(obj)
{
	var word = obj.value;
	var str = "1234567890.";
	for (i=0;i< word.length;i++){
		if(str.indexOf(word.charAt(i)) < 0){
			alert("숫자 조합만 가능합니다..");
			obj.value="";
			obj.focus();
			return false;
		}
	}
}

function InpuOnlyNumber2(obj)
{
	var word = obj.value;
	var str = "1234567890.,-";
	for (i=0;i< word.length;i++){
		if(str.indexOf(word.charAt(i)) < 0){
			alert("숫자 조합만 가능합니다..");
			obj.value="";
			obj.focus();
			return false;
		}
	}
}


function InpuOnlyNumber_Jquery(obj)
{
	var word = obj.val();
	var str = "1234567890.";
	for (i=0;i< word.length;i++){
		if(str.indexOf(word.charAt(i)) < 0){
			alert("숫자 조합만 가능합니다..");
			obj.attr('value','');
			obj.focus();
			return false;
		}
	}
}

//길이첵크
function checkLength(obj, param_maxlength)
{
	var word = obj.value;
	if(param_maxlength < obj.value.length)
	{
		alert("입력가능 길이를 초과하였습니다.");
		obj.focus();
		return false;
	}
}

function checkLength_Jquery(obj, param_maxlength)
{
	var word = obj.val();
	if(param_maxlength < word.length)
	{
		alert("입력가능 길이를 초과하였습니다.");
		obj.focus();
		return false;
	}
}

function date_search(day,sep, $from_date, $to_date){
	var oneDate = 1000 * 3600 * 24;
	var now = new Date();
	var week = new Date(now.getTime() + (oneDate*-6));
	var month = new Date(now.getTime() + (oneDate*-31));
	var nowTime = now.getFullYear() +sep+ ((now.getMonth()+1)<10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1)) +sep+ (now.getDate()<10 ? '0'+now.getDate() : now.getDate());
	var weekTime = week.getFullYear() +sep+ ((week.getMonth()+1)<10 ? '0'+(week.getMonth()+1) : (week.getMonth()+1)) +sep+ (week.getDate()<10 ? '0'+week.getDate() : week.getDate());
	var monthTime = month.getFullYear() +sep+ ((month.getMonth()+1)<10 ? '0'+(month.getMonth()+1) : (month.getMonth()+1)) +sep+ (month.getDate()<10 ? '0'+month.getDate() : month.getDate());

	var date=new Date();

	if(day=="today"){
		$from_date.val(nowTime);
		$to_date.val(nowTime);
	}else if(day=="week"){
		$from_date.val(weekTime);
		$to_date.val(nowTime);
	}else{
		$from_date.val(monthTime);
		$to_date.val(nowTime);
	}
}

function isValidDate(param) {
	try
	{
		param = param.replace( /[^(0-9)]/g,'');

		// 자리수가 맞지않을때
		if( isNaN(param) || param.length!=8 ) {
			return false;
		}

		var year = Number(param.substring(0, 4));
		var month = Number(param.substring(4, 6));
		var day = Number(param.substring(6, 8));

		var dd = day / 0;


		if( month<1 || month>12 ) {
			return false;
		}

		var maxDaysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
		var maxDay = maxDaysInMonth[month-1];

		// 윤년 체크
		if( month==2 && ( year%4==0 && year%100!=0 || year%400==0 ) ) {
			maxDay = 29;
		}

		if( day<=0 || day>maxDay ) {
			return false;
		}
		return true;

	} catch (err) {
		return false;
	}
}


function dhxwindowCenterAlgin(dhtmlWindows){

	var location={};

	location.x = ($(window).width() - Number(dhtmlWindows.width))/2

	location.y = ($(window).height()- Number(dhtmlWindows.height))/2

	return location;
}


//숫자만 입력되게.
function numberOnly(event) {
	event = event || window.event;
	var keyID = (event.which) ? event.which : event.keyCode;
	if( ( keyID >=48 && keyID <= 57 ) || ( keyID >=96 && keyID <= 105 ) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39 )
	{
		return;
	}
	else
	{
		return false;
	}
}


/*
 * 숫자를 한글 금액으로 변환
 */

Number.prototype.read = function() {
	if (this == 0) return '영';
	var phonemic = ['','일','이','삼','사','오','육','칠','팔','구'];
	var unit = ['','','십','백','천','만','십만','백만','천만','억','십억','백억','천억','조','십조','백조'];
	var ret = '';
	var part = new Array();
	var p;
	for (var x=0; x<String(this).length; x++) part[x] = String(this).substring(x,x+1);

	for (var i=0, cnt = String(this).length; cnt > 0; --cnt,++i) {
		p = phonemic[part[i]];
		p+= (p) ? (cnt>4 && phonemic[part[i+1]]) ? unit[cnt].substring(0,1) : unit[cnt] : '';
		ret+= p;
	}
	return ret;
}

String.prototype.comma = function() {
	if( this != null && this != '' ) {
		var orgenal={};
		orgenal.org = this.replace(/[^0-9]/g, '');
		orgenal.change = orgenal.org.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
		return orgenal;
	}
}

/*
 *	소숫점 2자리까지 표시
 */
Math.roundPrecision = function(val, precision){
	var p = this.pow(10, precision);
	return this.round(val * p) / p;
}




/**
 *	콤마설정.
 */
function putComma(input) {
	var num = input;

	if (num < 0) {
		num *= -1;
		var minus = true;
	}else{
		var minus = false;
	}

	var dotPos = (num+"").split(".");
	var dotU = dotPos[0];
	var dotD = dotPos[1];
	var commaFlag = dotU.length%3;

	if(commaFlag) {
		var out = dotU.substring(0, commaFlag);
		if (dotU.length > 3) out += ",";
	}
	else var out = "";

	for (var i=commaFlag; i < dotU.length; i+=3) {
		out += dotU.substring(i, i+3);
		if( i < dotU.length-3) out += ",";
	}

	if(minus) out = "-" + out;
	if(dotD) return out + "." + dotD;
	else return out;
}

/*
 * isNumber
 * 오직 숫자로만 이루어져 있는지 체크 한다.(음수,소수점 허용)
 *
 * @param   string
 * @return  boolean
 */

var inumber     = new Object();

inumber.isNumber = function(num)
{
	var validPattern  = /^[\+-]?\d+[.]?\d*$/;
	return  validPattern.test(num);
}

Number.prototype.vubIsNumber = function() {
	var validPattern  = /^[\+-]?\d+[.]?\d*$/;
	return  validPattern.test(num);
}


function amtToHan($formTag,$toTag,max_length){
	var value="";

	if($formTag.val().length ===1){
		if(Number($formTag.val()) === 0 ){
			$(this).val("");
		}
	}else if($formTag.length ===0){
		$toTag.text("");
	}

	if($formTag.val().replace(/[^0-9]/g,"").length > Number(max_length) ){

		value= $formTag.val().replace(/[^0-9]/g,"").substring(0,10);

	}else{
		value= $formTag.val();
	}

	numberOnly();

	var amt  = value.comma();

	$formTag.val(amt.change);

	$toTag.text(Number(amt.org).read())

}



function BankDepositButtonAction($form,obj){

	$.ajax({
		url : obj.url,
		type : "POST",
		async : true,
		dataType : "json",
		data: $form.serialize(),
		success : function(data) {
			if(data ==="success") {

				obj.closeWindowObj.close();

				alert(obj.successText);

				obj.clearGrid.clearAll();

				obj.searchFunction();

			}else{
				alert(obj.failText);
			}
		},
		error : function() {
			alert(obj.failText);
		}
	});
}
 function SelOnofftidInfoInput(p_onffmerch_no, p_onfftid, p_onfftid_nm){
       
    (onoffObject.onfftid).val("");
    (onoffObject.onfftid_nm).val("");
    (onoffObject.onfftid).val(p_onfftid);                 
    (onoffObject.onfftid_nm).val(p_onfftid_nm);    
    
    onoffObject.onfftid ="";
    onoffObject.onfftid_nm="";
}
function SelOnoffmerchInfoInput(p_onffmerch_no, p_merch_nm){
  
    (onoffObject.onffmerch_no).val("");
    (onoffObject.merch_nm).val("");
    
    (onoffObject.onffmerch_no).val(p_onffmerch_no);                 
    (onoffObject.merch_nm).val(p_merch_nm);    
    
    onoffObject.onffmerch_no = "";
    onoffObject.merch_nm="";                 
}
function SelCompaynyInfoInput(gridRawObject){
//            alert("(onoffObject.comp_seq) : "+(onoffObject.comp_seq).get(0).tagNme);
//            alert("comp_nm : "+gridRawObject.get("comp_nm"));
//            (onoffObject.comp_nm).text(gridRawObject.get("comp_nm"));
            nullCheck((onoffObject.comp_seq),gridRawObject.get("comp_seq"));            
            nullCheck((onoffObject.comp_nm),gridRawObject.get("comp_nm"));            
            nullCheck((onoffObject.biz_no),gridRawObject.get("biz_no"));            
            nullCheck((onoffObject.comp_cate_nm),gridRawObject.get("comp_cate_nm")); 
            nullCheck((onoffObject.corp_no),gridRawObject.get("corp_no"));
            nullCheck((onoffObject.comp_ceo_nm),gridRawObject.get("comp_ceo_nm"));
            nullCheck((onoffObject.comp_tel1),gridRawObject.get("comp_tel1"));
            nullCheck((onoffObject.comp_tel2),gridRawObject.get("comp_tel2"));
            nullCheck((onoffObject.addr),gridRawObject.get("addr_1") + gridRawObject.get("addr_2"));
            nullCheck((onoffObject.zip_cd),gridRawObject.get("zip_cd"));
            nullCheck((onoffObject.addr_1),gridRawObject.get("addr_1"));
            nullCheck((onoffObject.addr_2),gridRawObject.get("addr_2"));
            nullCheck((onoffObject.biz_cate),gridRawObject.get("biz_cate"));
            nullCheck((onoffObject.biz_type),gridRawObject.get("biz_type"));
            nullCheck((onoffObject.tax_flag),gridRawObject.get("tax_flag"));
            
            nullCheck((onoffObject.other_nm),gridRawObject.get("comp_nm"));
            nullCheck((onoffObject.other_zip_cd),gridRawObject.get("zip_cd"));
            nullCheck((onoffObject.other_addr_1),gridRawObject.get("addr_1"));
            nullCheck((onoffObject.other_addr_2),gridRawObject.get("addr_2"));      
            nullCheck((onoffObject.other_tel1),gridRawObject.get("comp_tel1"));
            nullCheck((onoffObject.other_tel2),gridRawObject.get("comp_tel2"));      
            

//    alert("comp_nmhtml  : "+onoffObject.comp_nm.parent().html());
//    (onoffObject.comp_nm).val(p_comp_nm);                 
//    (onoffObject.comp_seq).val(p_comp_seq);    
//    (onoffObject.biz_no).val(p_biz_no);    
//    (onoffObject.comp_cate_nm).val(p_comp_cate_nm);    
//    (onoffObject.corp_no).val(p_corp_no);    
//    (onoffObject.comp_ceo_nm).val(p_comp_ceo_nm);    
//    (onoffObject.comp_tel1).val(p_comp_tel1);    
//    (onoffObject.comp_tel2).val(p_comp_tel2);    
//    (onoffObject.zip_cd).val(p_zip_cd);    
//    (onoffObject.addr_1).val(p_addr_1);    
//    (onoffObject.addr_2).val(p_addr_2);    
//    (onoffObject.biz_cate).val(p_biz_cate);    
//    (onoffObject.biz_type).val(p_biz_type);    
//    (onoffObject.tax_flag).val(p_tax_flag);            
}
function SelAgentInfoInput(p_agent_seq,p_agent_nm){
    
    (onoffObject.agent_seq).val("");
    (onoffObject.agent_nm).val("");
    (onoffObject.agent_seq).val(p_agent_seq);
    (onoffObject.agent_nm).val(p_agent_nm);
    
    onoffObject.agent_seq ="";
    onoffObject.agent_nm="";                 
}
function nullCheck($value,parm){
    
//    alert("tagName : "+$value.attr("tag"));
    
    if($value!==''){
        if(parm !=='' || parm !=="undefined"){
            
            switch($value.prop("tagName")){
                case"SPAN":
                    $value.text(parm);
                    break;
                case "INPUT":
                    $value.val(parm);
                    break;
            }
        }
    }
    
    
}
function searchFormInit($form){
    $form.find("input").each(function(){
        switch($(this).attr("type")){
            case "text":
                $(this).val("");
                break;
        } 
    });

    $form.find("select").each(function(){
        $(this).find("option:eq(0)").attr("selected","selected");
    });
}

var Common = {

        // 날짜목록 (년)
	yyList : function (y, s, e) {
		day = new Date();
                var year = "";
		if (typeof y == "undefined") {
			var yy = day.getFullYear();
		} else if (y == "") {
			var yy = day.getFullYear();
		} else {
			var yy = parseInt(y);
		}

		for (var i = (e ? e : day.getFullYear() + 1); i >= (s ? s : 2009); i--) {
			year += "<option value='" + i + "'" + (i == yy ? " selected" : "") + ">" + i + "년</option>";
		}
                
                return year;
	},

	// 날짜목록 (월)
	mmList : function (m) {
		day = new Date();
                var month = "";
		if (typeof m == "undefined") {
			var mm = day.getMonth() + 1;
		} else if (m == "") {
			var mm = day.getMonth() + 1;
		} else {
			var mm = (m.substr(0, 1) == "0") ? parseInt(m.substr(1, m.length)) : parseInt(m);
		}

		for (var i = 1; i <= 12; i++) {
			var n = (i < 10 ? "0" : "") + i;

			month += "<option value='" + n + "'" + (i == mm ? " selected" : "") + ">" + n + "월</option>";
		}
                return month;
	}
}