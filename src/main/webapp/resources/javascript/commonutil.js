/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//숫자만 입력
function InpuOnlyNumber_util(obj) 
{
    var word = obj.value;
    var str = "1234567890.";
    for (i=0;i< word.length;i++){
        if(str.indexOf(word.charAt(i)) < 0){
            alert("숫자 조합만 가능합니다..");
            obj.value="";
            obj.focus();
            return false;
        }
    }
}

function InpuOnlyNumber_Jquery(obj) 
{
    var word = obj.val();
    var str = "1234567890.";
    for (i=0;i< word.length;i++){
        if(str.indexOf(word.charAt(i)) < 0){
            alert("숫자 조합만 가능합니다..");
            obj.attr('value','');
            obj.focus();
            return false;
        }
    }
}
//길이첵크
function checkLength(obj, param_maxlength)
{
    var word = obj.value;
    if(param_maxlength < obj.value.length)
    {
        alert("입력가능 길이를 초과하였습니다.");
        obj.focus();
        return false;
    }
}

function checkLength_Jquery(obj, param_maxlength)
{
    var word = obj.val();
    if(param_maxlength < word.length)
    {
        alert("입력가능 길이를 초과하였습니다.");
        obj.focus();
        return false;
    }
}