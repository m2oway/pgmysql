<%-- 
    Document   : settle_process
    Created on : 2017. 6. 7, 오후 2:57:31
    Author     : MoonBong
--%>
<%@page import="com.onoffkorea.system.common.util.Util"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.commons.fileupload.FileItem"%>
<%@page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@page import="java.util.*" %>
<%@page import="java.sql.*" %>
<%@page import="javax.sql.*" %>
<%@page import="javax.naming.*"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="com.onoffkorea.system.app.Vo.KiccPgVo"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%!

	/**
	 * 빈 문자열 체크
	 *
	 * @param arg 문자열
	 * @return boolean
	 *
	 */
		public boolean isEmpty(String arg) {
			if (arg == null) {
				return true;
			} else if (arg.equals("")) {
				return true;
			}

			return false;
		}
    /**
     * 주어진 길이(iLength)만큼 주어진 문자(cPadder)를 strSource의 왼쪽에 붙혀서 보내준다.
     * ex) lpad("abc", 5, '^') ==> "^^abc"
     *     lpad("abcdefghi", 5, '^') ==> "abcde"
     *     lpad(null, 5, '^') ==> "^^^^^"
     *
     * @param strSource
     * @param iLength
     * @param cPadder
     */
    public String lpad(String strSource, int iLength, char cPadder) {
        StringBuffer sbBuffer = null;

        if (!isEmpty(strSource)) 
        {
            int iByteSize = strSource.getBytes().length;
            if (iByteSize > iLength) {
                return strSource.substring(0, iLength);
            }
            else if (iByteSize == iLength) {
                return strSource;
            }
            else {
                int iPadLength = iLength - iByteSize;
                sbBuffer = new StringBuffer();
                for (int j = 0; j < iPadLength; j++) {
                    sbBuffer.append(cPadder);
                }
                sbBuffer.append(strSource);
                return sbBuffer.toString();
            }
        }
        sbBuffer = new StringBuffer();
        for (int j = 0; j < iLength; j++) {
            sbBuffer.append(cPadder);
        }

        return sbBuffer.toString();
    }
    
    /**
     * 주어진 길이(iLength)만큼 주어진 문자(cPadder)를 strSource의 오른쪽에 붙혀서 보내준다.
     * ex) lpad("abc", 5, '^') ==> "^^abc"
     *     lpad("abcdefghi", 5, '^') ==> "abcde"
     *     lpad(null, 5, '^') ==> "^^^^^"
     *
     * @param strSource
     * @param iLength
     * @param cPadder
     */
    public String Rpad(String strSource, int iLength, char cPadder) {
        StringBuffer sbBuffer = null;

        if (!isEmpty(strSource)) 
        {
            int iByteSize = strSource.getBytes().length;
            if (iByteSize > iLength) 
            {
                //return strSource.substring(0, iLength);
            	return new String(strSource.getBytes(),0,iLength);
            }
            else if (iByteSize == iLength) 
            {
                return strSource;
            }
            else 
            {
                int iPadLength = iLength - iByteSize;
                sbBuffer = new StringBuffer();
                sbBuffer.append(strSource);
                for (int j = 0; j < iPadLength; j++) 
                {
                    sbBuffer.append(cPadder);
                }
                
                return sbBuffer.toString();
            }
        }

       	sbBuffer = new StringBuffer();
        for (int j = 0; j < iLength; j++) 
        {
        	sbBuffer.append(cPadder);
        }
        
        return sbBuffer.toString();
    }    
    /**
     * 파라미터 체크 메소드
     */
    public String getNullToSpace(String param) {
        return (param == null) ? "" : param.trim();
    }

    public String getNullToZero(String param) {
        return (param == null) ? "0" : param.trim();
    }
        
    
    public void closeResultSet(ResultSet rs)
    {
        try
        {
            if ( rs != null ) rs.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    
    public void closePstmt(PreparedStatement psmt)
    {
        try
        {
            if ( psmt != null ) psmt.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }    
    
    public void closeConnection(Connection con)
    {
        try
        {
            if ( con != null ) con.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }       
    
    public void closeFileOutputStream(FileOutputStream os)
    {
        try
        {
            if ( os != null ) os.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }        
    
    public void closeInputStream(InputStream in)
    {
        try
        {
            if ( in != null ) in.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }         
    
    
    public void closeFileInputStream(FileInputStream in)
    {
        try
        {
            if ( in != null ) in.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }       
        
        
    public void closeBufferedReader(BufferedReader in)
    {
        try
        {
            if ( in != null ) in.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }           
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>입금내역파일처리</title>
    </head>
    <body>
<%
    final Logger logger = Logger.getLogger("settle_process.jsp");
    Date today=new Date();
    SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmss"); 
    
    String strCurDate = formater.format(today);
        
    FileOutputStream os=null;
    InputStream in=null;
            
    FileInputStream fis = null;
    BufferedReader br = null;
    
    Connection conn = null;
    PreparedStatement pstmt = null;
    PreparedStatement pstmt2 = null;
    ResultSet rs = null;
    ResultSet rs2 = null;
   
    String filename=null;
    
    String filename2=null;
   
    try
    {
       //DB Connection을 가져온다.
        Context initContext = new InitialContext();                                            //2
        DataSource ds = (DataSource) initContext.lookup("java:comp/env/jdbc/okpay");     //3
        conn = ds.getConnection();                                                             //4
        conn.setAutoCommit(false);
        
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        
        //file을 save한다.
        if(isMultipart)
        {
            DiskFileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);

            List<FileItem> items = upload.parseRequest(request);

            for( FileItem item : items)
            {
                if(item.isFormField())
                {
                    String name=item.getFieldName();

                    String value=item.getString("utf-8");
                    out.println("요청파라미터:"+name+"="+value+"<br/>");
                }
                else
                {
                    String name=item.getFieldName();
                    String fileName=item.getName();
                    String contentType=item.getContentType();
                    long size=item.getSize();
                    
                    out.println("요청파라미터 : " + name +"<br/>" );
                    out.println("파일명 : "+ fileName +"<br/>");
                    out.println("내용형식 : "+ contentType +"<br/>");
                    out.println("파일크기 : "+ size +"<br/>");

                    //filesave
                    //filename="c:\\uploadfile\\okpay\\" + strCurDate +fileName;
                    filename="/usr/local/tomcat7/uploadfile/okpay/" + strCurDate +fileName;
                    
                    filename2 = strCurDate +fileName;
                    out.println("file저장 : "+filename+"<br/>");
                    request.setAttribute("filename", filename);
                    os=new FileOutputStream(filename);
                    in=item.getInputStream();
                    byte[] buffer=new byte[512];
                    
                    int len=-1;
                    
                    while((len=in.read(buffer)) !=-1)
                    {
                        os.write(buffer,0,len);
                    }
                }            
            }

        }   

        closeFileOutputStream(os);
        closeInputStream(in);
                
        //file 정보저장을 위한 일련번호를 채번
        String strTpFileSeq = null;
        String strFileSeqSql = "select seq_tb_acq_resultfile.nextval fileseq from dual";
        pstmt = conn.prepareStatement(strFileSeqSql);
        
        rs = pstmt.executeQuery();
        
        rs.next();
        //fileseq일련번호
        strTpFileSeq = rs.getString("fileseq");
        
        closeResultSet(rs);
        closePstmt(pstmt);
        
        //file 정보를 입력한다.
        StringBuffer sb = new StringBuffer();
            
        sb.append("INSERT INTO TB_ACQ_RESULTFILE (FILE_SEQ,FILE_CATE,FILE_NM,WORK_DT,STEP,INS_USER ,MOD_USER) ");
        sb.append(" VALUES ( ?,?,?,?,?,?,? )");
            
        pstmt = conn.prepareStatement(sb.toString());
            
        pstmt.setInt(1, Integer.parseInt(strTpFileSeq));
        pstmt.setString(2, "KSNETPG_BAL");
        pstmt.setString(3, filename2);
        pstmt.setString(4, strCurDate);
        pstmt.setString(5, "00");
        pstmt.setString(6, "1");
        pstmt.setString(7, "1");
            
        int intDayFileCnt = pstmt.executeUpdate();
        
        closePstmt(pstmt);
        
        //저장된file을 읽어 처리한다.
            fis = new FileInputStream(filename);
            String sLine = "";
            
            int readCount =0 ;
            
            br = new BufferedReader(new InputStreamReader(fis));
            while( (sLine = br.readLine()) != null )
            {
                readCount++;
                //System.out.println("SLINE : ["+sLine+"]");
                
                if(readCount > 1)
                {
                    String[] arrStrLineInfo = sLine.split(",", -1);                    
                    
                    String strCol1	=	(Util.nullToString(arrStrLineInfo[0]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//결제구분   
                    String strCol2	=	(Util.nullToString(arrStrLineInfo[1]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//상점ID     
                    String strCol3	=	(Util.nullToString(arrStrLineInfo[2]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//기타정산   
                    String strCol4	=	(Util.nullToString(arrStrLineInfo[3]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//결재수단   
                    String strCol5	=	(Util.nullToString(arrStrLineInfo[4]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//기관명     
                    String strCol6	=	(Util.nullToString(arrStrLineInfo[5]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//건수       
                    String strCol7	=	(Util.nullToString(arrStrLineInfo[6]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//금액       
                    String strCol8	=	(Util.nullToString(arrStrLineInfo[7]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//수수료     
                    String strCol9	=	(Util.nullToString(arrStrLineInfo[8]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//부가세     
                    String strCol10	=	(Util.nullToString(arrStrLineInfo[9]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//상점입금액 
                    String strCol11	=	(Util.nullToString(arrStrLineInfo[10]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//매입요청일 
                    String strCol12	=	(Util.nullToString(arrStrLineInfo[11]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//매입완료일 
                    String strCol13	=	(Util.nullToString(arrStrLineInfo[12]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//지급예정일 
                    String strCol14	=	(Util.nullToString(arrStrLineInfo[13]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//실지급일   
                    String strCol15	=	(Util.nullToString(arrStrLineInfo[14]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//승인일자   
                    String strCol16	=	(Util.nullToString(arrStrLineInfo[15]).trim()).replaceAll("=\"", "").replaceAll("\"", "");	//통화구분   
                    
                    logger.debug("strCol1  :["+strCol1 +"]");
                    logger.debug("strCol2  :["+strCol2 +"]");
                    logger.debug("strCol3  :["+strCol3 +"]");
                    logger.debug("strCol4  :["+strCol4 +"]");
                    logger.debug("strCol5  :["+strCol5 +"]");
                    logger.debug("strCol6  :["+strCol6 +"]");
                    logger.debug("strCol7  :["+strCol7 +"]");
                    logger.debug("strCol8  :["+strCol8 +"]");
                    logger.debug("strCol9  :["+strCol9 +"]");
                    logger.debug("strCol10 :["+strCol10+"]");
                    logger.debug("strCol11 :["+strCol11+"]");
                    logger.debug("strCol12 :["+strCol12+"]");
                    logger.debug("strCol13 :["+strCol13+"]");
                    logger.debug("strCol14 :["+strCol14+"]");
                    logger.debug("strCol15 :["+strCol15+"]");
                    logger.debug("strCol16 :["+strCol16+"]");
                    
                    //수신된 정보로 가맹점 정보를 검색
                    StringBuffer sb_onffTidInfo = new StringBuffer();

                    sb_onffTidInfo.append(" select m.onfftid, m.onffmerch_no, m.pay_chn_cate, n.terminal_no, n.merch_no ");
                    sb_onffTidInfo.append(" from TB_ONFFTID_MST m, ");
                    sb_onffTidInfo.append("       ( ");
                    sb_onffTidInfo.append("           SELECT b.pay_mtd_seq, a.start_dt, a.end_dt, b.APP_ISS_CD, b.TID_MTD, b.TERMINAL_NO,b.PAY_MTD,b.MERCH_NO  ");
                    sb_onffTidInfo.append("            FROM TB_PAY_MTD a ");
                    sb_onffTidInfo.append("                      ,TB_PAY_MTD_DETAIL b ");
                    sb_onffTidInfo.append("            WHERE  ");
                    sb_onffTidInfo.append("            a.PAY_MTD_SEQ = b.PAY_MTD_SEQ ");
                    sb_onffTidInfo.append("            and b.DEL_FLAG  = 'N' ");
                    sb_onffTidInfo.append("            and b.USE_FLAG = 'Y' ");
                    sb_onffTidInfo.append("            and b.DEL_FLAG  = 'N' ");
                    sb_onffTidInfo.append("            and b.USE_FLAG = 'Y' ");
                    sb_onffTidInfo.append("            and a.start_dt <= TO_CHAR(sysdate, 'YYYYMMDD') ");
                    sb_onffTidInfo.append("            and a.end_dt  >= TO_CHAR(sysdate, 'YYYYMMDD') ");
                    sb_onffTidInfo.append("       ) n ");
                    sb_onffTidInfo.append(" where  ");
                    sb_onffTidInfo.append(" m.PAY_MTD_SEQ = n.pay_mtd_seq        ");
                    sb_onffTidInfo.append(" and m.del_flag = 'N'  ");
                    sb_onffTidInfo.append(" and n.TERMINAL_NO = ? ");

                    pstmt = conn.prepareStatement(sb_onffTidInfo.toString());

                    pstmt.setString(1, strCol1);

                    rs = pstmt.executeQuery();

                    String strOnffTid = "";
                    String strOnffMerchNo = "";
                    String strPayChnCate = "";

                    if(rs != null && rs.next())
                    {
                        strOnffTid = getNullToSpace(rs.getString(1));
                        strOnffMerchNo = getNullToSpace(rs.getString(2));
                        strPayChnCate =  getNullToSpace(rs.getString(3));
                    }
                    
                    logger.debug("strOnffTid : " + strOnffTid);
                    logger.debug("strOnffMerchNo : " + strOnffMerchNo);
                    logger.debug("strPayChnCate : " + strPayChnCate);

                    closeResultSet(rs);
                    closePstmt(pstmt);
                    
                    String strTpCms="";
                    //승인이 가맹점정보가 있을시 수수료 정보를 가져온다.
                    //if(!strOnffTid.equals("") && !strOnffMerchNo.equals("") && r_noti_type.equals("10"))
                    if(!strOnffTid.equals("") && !strOnffMerchNo.equals(""))
                    {
                        //승인취소를 입금액으로 확인하기 위해 
                        String strTpPayAmt = Util.nullToInt(strCol10);
                        
                        int intTpPayAmt = Integer.parseInt(strTpPayAmt);
                        
                        //승인일 경우
                        if(intTpPayAmt >= 0)
                        {     
                                String sql  = " INSERT INTO TB_ACQ_RESULT_DEATIL "
                                + " ( "
                                    + "  FILE_SEQ,FILE_NM,PAY_TYPE,RECORD_SEQ,RECORD_CL_CD \n" 
                                    + " ,ORG_RECORD_CL_CD,MERCH_NO,CRNCY_CD,CRNCY_IDX,SAL_DT \n" 
                                    + " ,RCPT_DT,ORG_SAL_DT,ACQ_DT,CARD_NUM,INSTALLMENT \n" 
                                    + " ,TOT_TRX_AMT,CARDFM_RET_RSN_CD,VAN_RET_RSN_CD,ISS_CD,ORG_ISS_CD \n" 
                                    + " ,ACQ_CD,ORG_ACQ_CD,FILLER,FILLER2,APRV_NO \n" 
                                    + " ,SHOP_POS_NO,VAT_AMT,CMMS_AMT,PAY_AMT,PG_SEQ \n" 
                                    + " ,ORDER_SEQ,FILLER3,PAY_DT,PROC_FLAG \n" 
                                    + " ,INS_USER,MOD_USER \n" 
                                + " ) "
                               + " VALUES  "
                               + " ( ?, ?, ?, ? ,?"                           //  1  2  3  4 5
                               + " , ?, ?, ?, ? ,? "                          //  6  7  8  9 10 
                               + " , ?, ?, ?, ? ,? "                          //  11 12 13 14 15 
                               + " , ?, ?, ?, F_GET_ISS_CD('KSNETPG', ? ) ,? " //  16 17 18 19 20 
                               + " , F_GET_ISS_CD('KSNETPG', ? ), ?, ?, ? ,? " //  21 22 23 24 25 
                               + " , ?, ?, ?, ? ,? "                          // 26 27 28 29 30 
                               + " , ?, ?, ?, ? "                             //  31 32 33 34
                               + " , ?, ? "                             //  35 36 
                               + " ) "
                               ;
                            
                            pstmt = conn.prepareStatement(sql);
                            
                            pstmt.setString( 1, strCurDate+lpad(strTpFileSeq,6,'0'));
                            pstmt.setString( 2, filename2);
                            pstmt.setString( 3, "001");
                            pstmt.setString( 4, lpad(String.valueOf(readCount),10,'0') );
                            
                            pstmt.setString( 5, "60");

                             String strTpSaledd = Util.nullToString(strCol15).trim();
                             String strTpOrgSaledd = Util.nullToString(strCol15).trim();

                             pstmt.setString(6, "A");        
                             pstmt.setString(7, Util.nullToString(strCol2).trim());
                             pstmt.setString(8, null);
                             pstmt.setString(9, null);
                             pstmt.setString(10, strCol15);


                             String strTpCloseDt = strCol11;
                             String strTpAcqDt =  strCol12;

                             pstmt.setString(11, strTpCloseDt);//접수일자
                             pstmt.setString(12, strCol15);//원거래일
                             pstmt.setString(13, strTpAcqDt);//매입일자
                             pstmt.setString(14, "");//카드번호
                             pstmt.setString(15, "00");

                             String strOrgSaleAmt = Util.nullToInt(strCol7);
                             pstmt.setLong(16, Long.parseLong(strOrgSaleAmt));

                             pstmt.setString(17, null);
                             pstmt.setString(18, null);

                             String strTpAcqcd = null;

                             pstmt.setString(19, strTpAcqcd);
                             pstmt.setString(20, strTpAcqcd);
                             pstmt.setString(21, strTpAcqcd);
                             pstmt.setString(22, strTpAcqcd);
                             pstmt.setString(23, strTpAcqcd);
                             pstmt.setString(24, null);            

                             String strTpAppNo = "";
                             if(!strTpAppNo.equals(""))
                             {
                                 pstmt.setString(25, strTpAppNo);
                             }
                             else
                             {
                                 pstmt.setString(25, null);
                             }
                             pstmt.setString(26, null);            

                             //pstmt.setString(27, tr.getVatamt());
                             String strOrgVatAmt = Util.nullToInt(strCol9);
                             pstmt.setLong(27, Long.parseLong(strOrgVatAmt));            

                             //pstmt.setString(28, tr.getCardFee());
                             String strOrgCardFee = Util.nullToInt(strCol8);
                             pstmt.setLong(28, Long.parseLong(strOrgCardFee));            

                             //pstmt.setString(29, tr.getPayamt());
                             String strOrgPayamt = Util.nullToInt(strCol11);
                             pstmt.setLong(29, Long.parseLong(strOrgPayamt));            

                             pstmt.setString(30, "");
                             pstmt.setString(31, "");
                             pstmt.setString(32, null );
                             pstmt.setString(33, Util.nullToSpaceString(strCol13).trim());
                             pstmt.setString(34, "N");
                             pstmt.setString(35, "1");
                             pstmt.setString(36, "1");
                             
                             pstmt.executeUpdate();
                             closePstmt(pstmt); 
                        }//승인일경우 end if
                        //취소일경우
                        else if(strCol5.equals("취소"))
                        {
                            
                               String sql  = " INSERT INTO TB_ACQ_RESULT_DEATIL "
                                + " ( "
                                    + "  FILE_SEQ,FILE_NM,PAY_TYPE,RECORD_SEQ,RECORD_CL_CD \n" 
                                    + " ,ORG_RECORD_CL_CD,MERCH_NO,CRNCY_CD,CRNCY_IDX,SAL_DT \n" 
                                    + " ,RCPT_DT,ORG_SAL_DT,ACQ_DT,CARD_NUM,INSTALLMENT \n" 
                                    + " ,TOT_TRX_AMT,CARDFM_RET_RSN_CD,VAN_RET_RSN_CD,ISS_CD,ORG_ISS_CD \n" 
                                    + " ,ACQ_CD,ORG_ACQ_CD,FILLER,FILLER2,APRV_NO \n" 
                                    + " ,SHOP_POS_NO,VAT_AMT,CMMS_AMT,PAY_AMT,PG_SEQ \n" 
                                    + " ,ORDER_SEQ,FILLER3,PAY_DT,PROC_FLAG \n" 
                                    + " ,INS_USER,MOD_USER \n" 
                                + " ) "
                               + " VALUES  "
                               + " ( ?, ?, ?, ? ,?"                           //  1  2  3  4 5
                               + " , ?, ?, ?, ? ,? "                          //  6  7  8  9 10 
                               + " , ?, ?, ?, ? ,? "                          //  11 12 13 14 15 
                               + " , ?, ?, ?, F_GET_ISS_CD('KSNETPG', ? ) ,? " //  16 17 18 19 20 
                               + " , F_GET_ISS_CD('KSNETPG', ? ), ?, ?, ? ,? " //  21 22 23 24 25 
                               + " , ?, ?, ?, ? ,? "                          // 26 27 28 29 30 
                               + " , ?, ?, ?, ? "                             //  31 32 33 34
                               + " , ?, ? "                             //  35 36 
                               + " ) "
                               ;
                            
                            pstmt = conn.prepareStatement(sql);
                            
                            pstmt.setString( 1, strCurDate+lpad(strTpFileSeq,6,'0'));
                            pstmt.setString( 2, filename2);
                            pstmt.setString( 3, "001");
                            pstmt.setString( 4, lpad(String.valueOf(readCount),10,'0') );
                            
                            pstmt.setString( 5, "67");

                             String strTpSaledd = Util.nullToString(strCol15).trim();
                             String strTpOrgSaledd = Util.nullToString(strCol15).trim();

                             pstmt.setString(6, "C");        
                             pstmt.setString(7, Util.nullToString(strCol2).trim());
                             pstmt.setString(8, null);
                             pstmt.setString(9, null);
                             pstmt.setString(10, strCol15);


                             String strTpCloseDt = strCol11;
                             String strTpAcqDt =  strCol12;

                             pstmt.setString(11, strTpCloseDt);//접수일자
                             pstmt.setString(12, strCol15);//원거래일
                             pstmt.setString(13, strTpAcqDt);//매입일자
                             pstmt.setString(14, "");//카드번호
                             pstmt.setString(15, "00");

                             String strOrgSaleAmt = Util.nullToInt(strCol7);
                             pstmt.setLong(16, Long.parseLong(strOrgSaleAmt));

                             pstmt.setString(17, null);
                             pstmt.setString(18, null);

                             String strTpAcqcd = null;

                             pstmt.setString(19, strTpAcqcd);
                             pstmt.setString(20, strTpAcqcd);
                             pstmt.setString(21, strTpAcqcd);
                             pstmt.setString(22, strTpAcqcd);
                             pstmt.setString(23, strTpAcqcd);
                             pstmt.setString(24, null);            

                             String strTpAppNo = "";
                             if(!strTpAppNo.equals(""))
                             {
                                 pstmt.setString(25, strTpAppNo);
                             }
                             else
                             {
                                 pstmt.setString(25, null);
                             }
                             pstmt.setString(26, null);            

                             //pstmt.setString(27, tr.getVatamt());
                             String strOrgVatAmt = Util.nullToInt(strCol9);
                             pstmt.setLong(27, Long.parseLong(strOrgVatAmt));            

                             //pstmt.setString(28, tr.getCardFee());
                             String strOrgCardFee = Util.nullToInt(strCol8);
                             pstmt.setLong(28, Long.parseLong(strOrgCardFee));            

                             //pstmt.setString(29, tr.getPayamt());
                             String strOrgPayamt = Util.nullToInt(strCol11);
                             pstmt.setLong(29, Long.parseLong(strOrgPayamt));            

                             pstmt.setString(30, "");
                             pstmt.setString(31, "");
                             pstmt.setString(32, null );
                             pstmt.setString(33, Util.nullToSpaceString(strCol13).trim());
                             pstmt.setString(34, "N");
                             pstmt.setString(35, "1");
                             pstmt.setString(36, "1");
                            
                             pstmt.executeUpdate();
                             closePstmt(pstmt); 
                        }//취소일경우 endif                            
                    } 
                    //가맹점정보 미등록
                    else
                    {
                        out.println("pg가맹점 미등록 : " + strCol2 + "<br/>");
                    }//가맹점 정보 미등록 endif
                 
                }//rowcount ifend
            }// row loop end
            
            //
            
            conn.commit();
            
            
            //정산 프로시져 돌림
            
             out.println("<b>입금처리완료!!!</b><br/>");
    }
    catch(Exception ex)
    {
        ex.printStackTrace();
        conn.rollback();
        
        out.println("<b>파일처리오류!!!</b><br/>");
    }
    finally
    {
        closeFileOutputStream(os);
        closeInputStream(in);
        
        closeFileInputStream(fis);
        closeBufferedReader(br);
        
        closeResultSet(rs);
        closeResultSet(rs2);
        closePstmt(pstmt);
        closePstmt(pstmt2);
        closeConnection(conn);
    }

%>        
    </body>
</html>
