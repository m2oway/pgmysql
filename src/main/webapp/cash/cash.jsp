<%@ page contentType="text/html; charset=euc-kr" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="../inc/easypay_config.jsp" %><!-- ' 환경설정 파일 include -->
<html>
<head>	
<title>KICC EASYPAY7.0 SAMPLE</title>
<meta name="robots" content="noindex, nofollow">
<meta http-equiv="content-type" content="text/html; charset=euc-kr">
<link href="../css/style.css" rel="stylesheet" type="text/css">
<script language="javascript" src="../js/default.js" type="text/javascript"></script>
<script type="text/javascript">

    /* 입력 자동 Setting */
    function f_init(){
        var frm_pay = document.frm_pay;
        
        var today = new Date();
        var year  = today.getFullYear();
        var month = today.getMonth() + 1;
        var date  = today.getDate();
        var time  = today.getTime();
        
        if(parseInt(month) < 10) {
            month = "0" + month;
        }

        if(parseInt(date) < 10) {
            date = "0" + date;
        }
        
        frm_pay.EP_order_no.value = "" + year + month + date + time;   //가맹점주문번호
        frm_pay.EP_user_id.value = "USER_" + time;                           //고객ID
        frm_pay.EP_user_nm.value = "길라임";        
    }
    
    function f_submit() {
        var frm_pay = document.frm_pay;
        
        var bRetVal = false;
        
        /*  주문번호 확인 */
        if( !frm_pay.EP_order_no.value ) {
            alert("가맹점주문번호를 입력하세요!!");
            frm_pay.EP_order_no.focus();
            return;
        }
        /*  금액 확인 */
        if( !frm_pay.EP_tot_amt.value ) {
            alert("총거래금액을 입력하세요!!");
            frm_pay.EP_tot_amt.focus();
            return;
        }
        if( !frm_pay.EP_service_amt.value ) {
            alert("봉사료를 입력하세요!!");
            frm_pay.EP_service_amt.focus();
            return;
        }
        if( !frm_pay.EP_vat.value ) {
            alert("부가세를 입력하세요!!");
            frm_pay.EP_vat.focus();
            return;
        }        
        
        /* 현금영수증 발급 구분에 따라 처리 */            
        /* 현금영수증 발행용도 */
        if( frm_pay.EP_issue_type.value == "01" ) {
        	/* 개인 */
    	    if( frm_pay.EP_auth_type.value == "02" ) {
    	        
    	        if( frm_pay.EP_auth_value.value.length != 13 ) {
    	            alert("주민등록번호를 입력하세요!!");
                    frm_pay.EP_auth_value.focus();
                    return;
    	        }
            }
            else if( frm_pay.EP_auth_type.value == "03" ) {
            	
            	if( frm_pay.EP_auth_value.value.length < 10 ) {
    	            alert("휴대폰번호를 입력하세요!!");
                    frm_pay.EP_auth_value.focus();
                    return;
    	        }
            }
            else {
            	alert("소득공제용은 인증구분을 주민등록번호, 휴대폰번호를 선택하세요.!!");
                frm_pay.EP_cash_auth_type.focus();
                return;
            }
        }
        else {
        	/* 법인 */
        	if( frm_pay.EP_auth_value.value.length != 10 ) {
    	        alert("사업자번호를 입력하세요!!");
                frm_pay.EP_auth_value.focus();
                return;
    	    }
        }
        /* 하위 가맹점 사용 확인 */
        if( frm_pay.EP_sub_mall_yn.value == "1" ) {
            if( frm_pay.EP_sub_mall_buss.value.length != 10 ) {
                alert("하위가맹점 사업자번호를 입력하세요!!");
                frm_pay.EP_auth_value.focus();
                return;
    	    }
        }
        
        bRetVal = true;
        if ( bRetVal ) frm_pay.submit();
    }
</script>
</head>
<body onload="f_init();">
<form name="frm_pay" method="post" action="../cash_easypay_request.jsp">
	
<!-- 거래구분(수정불가) -->
<input type="hidden" name="EP_tr_cd" value="00201050">
<!-- 결제수단(수정불가) -->
<input type="hidden" name="EP_pay_type" value="cash">
<!-- 요청구분(수정불가) -->
<input type="hidden" name="EP_req_type" value="issue">

<table border="0" width="910" cellpadding="10" cellspacing="0">
<tr>
    <td>
    <!-- title start -->
	<table border="0" width="900" cellpadding="0" cellspacing="0">
	<tr>
		<td height="30" bgcolor="#FFFFFF" align="left">&nbsp;<img src="../img/arow3.gif" border="0" align="absmiddle">&nbsp;현금영수증 > <b>발행</td>
	</tr>
	<tr>
		<td height="2" bgcolor="#2D4677"></td>
	</tr>
	</table>
	<!-- title end -->
    
    <!-- cash start -->
    <table border="0" width="900" cellpadding="0" cellspacing="0">
    <tr>
        <td height="30" bgcolor="#FFFFFF">&nbsp;<img src="../img/arow2.gif" border="0" align="absmiddle">&nbsp;<b>현금영수증정보</b>(*필수)</td>
    </tr>
    </table>

    <table border="0" width="900" cellpadding="0" cellspacing="1" bgcolor="#DCDCDC">
    <!-- 거래금액 및 부가세 부분은 가맹점에서 직접 계산해야 함 -->
    <tr height="25">        
        <td bgcolor="#EDEDED" width="150">&nbsp;*총거래금액</td>
        <td bgcolor="#FFFFFF" width="300" colspan="3">&nbsp;<input type="text" name="EP_tot_amt" value="1000" size="20" maxlength="13" class="input_F">
        </td>
    </tr>
    <tr height="25">        
        <td bgcolor="#EDEDED" width="150">&nbsp;*봉사료</td>
        <td bgcolor="#FFFFFF" width="300" colspan="3">&nbsp;<input type="text" name="EP_service_amt" value="0" size="20" maxlength="13" class="input_F">
        </td>
    </tr>
    <tr height="25">        
        <td bgcolor="#EDEDED" width="150">&nbsp;*부가세</td>
        <td bgcolor="#FFFFFF" width="300" colspan="3">&nbsp;<input type="text" name="EP_vat" value="100" size="20" maxlength="13" class="input_F">
        </td>
    </tr>
    <tr height="25">        
        <td bgcolor="#EDEDED" width="150">&nbsp;*발행용도</td>
        <td bgcolor="#FFFFFF" width="300">&nbsp;<select name="EP_issue_type" class="input_F">
        	<option value="01" selected>소득공제용</option>
            <option value="02">지출증빙용</option>            
        </select></td>
        <td bgcolor="#EDEDED" width="150">&nbsp;*인증구분</td>
        <td bgcolor="#FFFFFF" width="300">&nbsp;<select name="EP_auth_type" class="input_F">
            <option value="" selected>선택</option>
            <option value="02">주민등록번호</option>
            <option value="03">휴대폰번호</option>
            <option value="04">사업자번호</option>
        </select></td>
    </tr>
    <tr height="25">
        <td bgcolor="#EDEDED" width="150">&nbsp;*인증번호</td>
        <td bgcolor="#FFFFFF" width="300" colspan="3">&nbsp;<input type="text" name="EP_auth_value" value="" size="20" maxlength="13" class="input_F">&nbsp;'-'없이 입력
        </td>      
    </tr>
    <tr height="25">        
        <td bgcolor="#EDEDED" width="150">&nbsp;*하위가맹점사용여부</td>
        <td bgcolor="#FFFFFF" width="300">&nbsp;<select name="EP_sub_mall_yn" class="input_F">
        	<option value="" selected>선택</option>
            <option value="0" selected>미사용</option>
            <option value="1">사용</option>            
        </select></td>
        <td bgcolor="#EDEDED" width="150">&nbsp;하위가맹점사업자번호</td>
        <td bgcolor="#FFFFFF" width="300" colspan="3">&nbsp;<input type="text" name="EP_sub_mall_buss" value="" size="20" maxlength="13" class="input_A">&nbsp;'-'없이 입력
        </td>
    </tr>    
    </table>
    <!-- cash end -->
   
    <!-- order start -->
    <table border="0" width="900" cellpadding="0" cellspacing="0">
    <tr>
        <td height="30" bgcolor="#FFFFFF">&nbsp;<img src="../img/arow2.gif" border="0" align="absmiddle">&nbsp;<b>주문정보</b>(*필수)</td>
    </tr>
    </table>
    <table border="0" width="900" cellpadding="0" cellspacing="1" bgcolor="#DCDCDC">
    <tr height="25">
        <td bgcolor="#EDEDED" width="150">&nbsp;*주문번호</td>
        <td bgcolor="#FFFFFF" width="300" colspan="3">&nbsp;<input type="text" name="EP_order_no" size="50" class="input_F"></td>        
    </tr>
    <tr height="25">
        <td bgcolor="#EDEDED" width="150">&nbsp;고객ID</td>
        <td bgcolor="#FFFFFF" width="300">&nbsp;<input type="text" name="EP_user_id" size="50" class="input_A"></td>
        <td bgcolor="#EDEDED" width="150">&nbsp;고객명</td>
        <td bgcolor="#FFFFFF" width="300">&nbsp;<input type="text" name="EP_user_nm" size="50" class="input_A"></td>
    </tr>
    </table>
    <!-- order Data END -->
    
    
    <table border="0" width="900" cellpadding="0" cellspacing="0">
    <tr>
        <td height="30" align="center" bgcolor="#FFFFFF"><input type="button" value="발 행" class="input_D" style="cursor:hand;" onclick="javascript:f_submit();"></td>
    </tr>
    </table>
    </td>
</tr>
</table>
</form>
</body>
</html>