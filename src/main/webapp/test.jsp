<%-- 
    Document   : test.jsp
    Created on : 2015. 1. 7, 오후 4:53:48
    Author     : Administrator
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
	<title>Minimal init</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dhx/dhtmlxwindows.css" />"  />
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/dhx/dhtmlxwindows_dhx_skyblue.css" />"  />
        <script src="<c:url value="/resources/javascript/dhx/dhtmlxcommon.js" />"></script>
        <script src="<c:url value="/resources/javascript/dhx/dhtmlxwindows.js" />"></script>
        <script src="<c:url value="/resources/javascript/dhx/dhtmlxcontainer.js" />"></script>
	<script>
		
		var dhxWins
		function doOnLoad() {
			dhxWins = new dhtmlXWindows();
			dhxWins.attachViewportTo("winVP");
		}
		
		var idPrefix = 1;
		function createWindow() {
			var p = 0;
			dhxWins.forEachWindow(function(){p++;});
			if (p>5) {
				alert("Too many windows");
				return;
			}
			var id = "userWin"+(idPrefix++);
			//
			var w = Number(document.getElementById("win_w").value);
			var h = Number(document.getElementById("win_h").value);
			var x = Number(document.getElementById("win_x").value);
			var y = Number(document.getElementById("win_y").value);
			//
			dhxWins.createWindow(id, x, y, w, h);
			dhxWins.window(id).setText(document.getElementById("win_t").value);
			// dhxWins.window(id).keepInViewport(true);
			//
			document.getElementById("win_x").value = x+8;
			document.getElementById("win_y").value = y+6;
		}
		
		function doOnUnload() {
			if (dhxWins != null && dhxWins.unload != null) {
				dhxWins.unload();
				dhxWins = null;
			}
		}
		
	</script>
</head>
<body onload="doOnLoad();" onunload="doOnUnload();">
	<div>
		<table>
			<tr>
				<td>Input Position (x,y)</td>
				<td><input id="win_x" type="text" style="width: 30px;" value="20"> <input id="win_y" type="text" style="width: 30px;" value="30"></td>
			</tr>
			<tr>
				<td>Input Size (width, height)</td>
				<td><input id="win_w" type="text" style="width: 30px;" value="320"> <input id="win_h" type="text" style="width: 30px;" value="200"></td>
			</tr>
			<tr>
				<td>Input Header Text</td>
				<td><input id="win_t" type="text" style="width: 150px;" value="New dhtmlxWindow"></td>
			</tr>
			<tr>
				<td colspan="2" align="center" style="padding-top: 10px;"><input type="button" value="Create Window" onclick="createWindow();"></td>
			</tr>
		</table>
	</div>
	<div id="winVP" style="position: relative; height: 500px; border: #cecece 1px solid; margin: 10px;"></div>
</body>
</html>
