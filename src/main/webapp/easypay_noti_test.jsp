<%@ page import="java.util.*" %>
<%@ page import="java.sql.*" %>
<%@ page import="javax.sql.*" %>
<%@ page import="javax.naming.*"%>
<%@ page import="org.apache.log4j.Logger"%>
<%@ page import="com.onoffkorea.system.app.Vo.KiccPgVo"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=euc-kr" %>
<%!
    /**
     * 파라미터 체크 메소드
     */
    public String getNullToSpace(String param) {
        return (param == null) ? "" : param.trim();
    }

    public String getNullToZero(String param) {
        return (param == null) ? "0" : param.trim();
    }
        
    
    public void closeResultSet(ResultSet rs)
    {
        try
        {
            if ( rs != null ) rs.close();
        }
        catch(Exception ex)
        {
        }
    }
    
    public void closePstmt(PreparedStatement psmt)
    {
        try
        {
            if ( psmt != null ) psmt.close();
        }
        catch(Exception ex)
        {
        }
    }    
    
    public void closeConnection(Connection con)
    {
        try
        {
            if ( con != null ) con.close();
        }
        catch(Exception ex)
        {
        }
    }       
%>
<%
    request.setCharacterEncoding("EUC-KR");

    final char DELI_US = 0x1f;
    final String RESULT_SUCCESS = "0000";
    final String RESULT_FAIL 	= "5001";
    
    final Logger logger = Logger.getLogger("easypay_noti_onoffkorea.jsp");

    Connection conn = null;
    PreparedStatement pstmt = null;
    PreparedStatement pstmt2 = null;
    ResultSet rs = null;
    ResultSet rs2 = null;

    /* -------------------------------------------------------------------------- */
    /* ::: 노티수신                                                               */
    /* -------------------------------------------------------------------------- */
    String result_msg = "";

    String r_res_cd         = getNullToSpace(request.getParameter( "res_cd"         ));  // 응답코드
    String r_res_msg        = getNullToSpace(request.getParameter( "res_msg"        ));  // 응답 메시지    
    String r_cno            = getNullToSpace(request.getParameter( "cno"            ));  // PG거래번호
    String r_memb_id        = getNullToSpace(request.getParameter( "memb_id"        ));  // 가맹점 ID
    String r_amount         = getNullToZero(request.getParameter( "amount"         ));  // 총 결제금액
    String r_order_no       = getNullToSpace(request.getParameter( "order_no"       ));  // 주문번호
    String r_noti_type      = getNullToSpace(request.getParameter( "noti_type"      ));  // 노티구분
    String r_auth_no        = getNullToSpace(request.getParameter( "auth_no"        ));  // 승인번호
    String r_tran_date      = getNullToSpace(request.getParameter( "tran_date"      ));  // 승인일시
    String r_card_no        = getNullToSpace(request.getParameter( "card_no"        ));  // 카드번호
    String r_issuer_cd      = getNullToSpace(request.getParameter( "issuer_cd"      ));  // 발급사코드
    String r_issuer_nm      = getNullToSpace(request.getParameter( "issuer_nm"      ));  // 발급사명
    String r_acquirer_cd    = getNullToSpace(request.getParameter( "acquirer_cd"    ));  // 매입사코드
    String r_acquirer_nm    = getNullToSpace(request.getParameter( "acquirer_nm"    ));  // 매입사명
    String r_install_period = getNullToSpace(request.getParameter( "install_period" ));  // 할부개월
    String r_noint          = getNullToSpace(request.getParameter( "noint"          ));  // 무이자여부
    String r_bank_cd        = getNullToSpace(request.getParameter( "bank_cd"        ));  // 은행코드
    String r_bank_nm        = getNullToSpace(request.getParameter( "bank_nm"        ));  // 은행명
    String r_account_no     = getNullToSpace(request.getParameter( "account_no"     ));  // 계좌번호
    String r_deposit_nm     = getNullToSpace(request.getParameter( "deposit_nm"     ));  // 입금자명
    String r_expire_date    = getNullToSpace(request.getParameter( "expire_date"    ));  // 계좌사용만료일
    String r_cash_res_cd    = getNullToSpace(request.getParameter( "cash_res_cd"    ));  // 현금영수증 결과코드
    String r_cash_res_msg   = getNullToSpace(request.getParameter( "cash_res_msg"   ));  // 현금영수증 결과메시지
    String r_cash_auth_no   = getNullToSpace(request.getParameter( "cash_auth_no"   ));  // 현금영수증 승인번호
    String r_cash_tran_date = getNullToSpace(request.getParameter( "cash_tran_date" ));  // 현금영수증 승인일시
    String r_cp_cd          = getNullToSpace(request.getParameter( "cp_cd"          ));  // 포인트사
    String r_used_pnt       = getNullToSpace(request.getParameter( "used_pnt"       ));  // 사용포인트
    String r_remain_pnt     = getNullToSpace(request.getParameter( "remain_pnt"     ));  // 잔여한도
    String r_pay_pnt        = getNullToSpace(request.getParameter( "pay_pnt"        ));  // 할인/발생포인트 
    String r_accrue_pnt     = getNullToSpace(request.getParameter( "accrue_pnt"     ));  // 누적포인트
    String r_escrow_yn      = getNullToSpace(request.getParameter( "escrow_yn"      ));  // 에스크로 사용유무
    String r_canc_date      = getNullToSpace(request.getParameter( "canc_date"      ));  // 취소일시
    String r_canc_acq_date  = getNullToSpace(request.getParameter( "canc_acq_date"  ));  // 매입취소일시
    String r_refund_date    = getNullToSpace(request.getParameter( "refund_date"    ));  // 환불예정일시
    String r_pay_type       = getNullToSpace(request.getParameter( "pay_type"       ));  // 결제수단
    String r_auth_cno       = getNullToSpace(request.getParameter( "auth_cno"       ));  // 인증거래번호
    String r_tlf_sno        = getNullToSpace(request.getParameter( "tlf_sno"        ));  // 채번거래번호
    String r_account_type   = getNullToSpace(request.getParameter( "account_type"   ));  // 채번계좌 타입 US AN 1 (V-일반형, F-고정형)
    String r_reserve1       = getNullToSpace(request.getParameter( "reserve1"       ));  // 가맹점 필드1
    String r_reserve2       = getNullToSpace(request.getParameter( "reserve2"       ));  // 가맹점 필드2
    String r_reserve3       = getNullToSpace(request.getParameter( "reserve3"       ));  // 가맹점 필드3
    String r_reserve4       = getNullToSpace(request.getParameter( "reserve4"       ));  // 가맹점 필드4
    String r_reserve5       = getNullToSpace(request.getParameter( "reserve5"       ));  // 가맹점 필드5
    String r_reserve6       = getNullToSpace(request.getParameter( "reserve6"       ));  // 가맹점 필드6
    String r_user_id       = getNullToSpace(request.getParameter( "user_id"       ));  // 가맹점 필드6
    String r_user_nm       = getNullToSpace(request.getParameter( "user_nm"       ));  // 가맹점 필드6
    
    StringBuffer sb_param = new StringBuffer();
    sb_param.append("res_cd	:["		+ r_res_cd		+ "]");
    sb_param.append("res_msg:["		+ r_res_msg	+ "]");	
    sb_param.append("cno:["			+ r_cno		+ "]");		 
    sb_param.append("memb_id:["		+ r_memb_id	+ "]");		 
    sb_param.append("amount:["		+ r_amount		+ "]");	 
    sb_param.append("order_no:["		+ r_order_no	+ "]");		 
    sb_param.append("noti_type:["		+ r_noti_type	+ "]");		 
    sb_param.append("auth_no:["		+ r_auth_no	+ "]");		 
    sb_param.append("tran_date:["		+ r_tran_date	+ "]");		 
    sb_param.append("card_no:["		+ r_card_no	+ "]");		 
    sb_param.append("issuer_cd:["		+ r_issuer_cd	+ "]");	
    sb_param.append("issuer_nm:["	+ r_issuer_nm	+ "]");		
    sb_param.append("acquirer_cd:["	+ r_acquirer_cd	+ "]");	 
    sb_param.append("acquirer_nm	:["	+ r_acquirer_nm	+ "]");
    sb_param.append("install_period:["	+ r_install_period	+ "]");	 
    sb_param.append("noint:["		+ r_noint		+ "]");		
    sb_param.append("bank_cd:["		+ r_bank_cd	+ "]");		 
    sb_param.append("bank_nm:["		+ r_bank_nm	+ "]");		 
    sb_param.append("account_no:["	+ r_account_no	+ "]");		
    sb_param.append("deposit_nm:["	+ r_deposit_nm	+ "]");		
    sb_param.append("expire_date:["	+ r_expire_date	+ "]");		
    sb_param.append("cash_res_cd:["	+ r_cash_res_cd	+ "]");	 
    sb_param.append("cash_res_msg:["	+ r_cash_res_msg+ "]");		 
    sb_param.append("cash_auth_no:["	+ r_cash_auth_no+ "]");		 
    sb_param.append("cash_tran_date:["	+ r_cash_tran_date+ "]");		 
    sb_param.append("cp_cd:["		+ r_cp_cd+ "]");			 
    sb_param.append("used_pnt:["		+ r_used_pnt+ "]");			 
    sb_param.append("remain_pnt:["	+ r_remain_pnt+ "]");			
    sb_param.append("pay_pnt:["		+ r_pay_pnt+ "]");			 
    sb_param.append("accrue_pnt:["	+ r_accrue_pnt+ "]");			
    sb_param.append("escrow_yn:["	+ r_escrow_yn+ "]");			
    sb_param.append("canc_date:["	+ r_canc_date+ "]");			
    sb_param.append("canc_acq_date:["	+ r_canc_acq_date+ "]");		 
    sb_param.append("refund_date:["	+ r_refund_date+ "]");			
    sb_param.append("pay_type:["		+ r_pay_type+ "]");			 
    sb_param.append("auth_cno:["		+ r_auth_cno+ "]");			 
    sb_param.append("tlf_sno:["		+ r_tlf_sno+ "]");		
    sb_param.append("account_type:["	+ r_account_type	+ "]");	 
    sb_param.append("reserve1:["		+ r_reserve1+ "]");			 
    sb_param.append("reserve2:["		+ r_reserve2+ "]");			 
    sb_param.append("reserve3:["		+ r_reserve3+ "]");			 
    sb_param.append("reserve4:["		+ r_reserve4+ "]");			 
    sb_param.append("reserve5:["		+ r_reserve5+ "]");			 
    sb_param.append("reserve6:["		+ r_reserve6+ "]");			 
    sb_param.append("user_id:["		+ r_user_id	+ "]");		 
    sb_param.append("user_nm:["		+ r_user_nm+ "]");
    
    logger.trace("----------noti param---------------");
    logger.trace(sb_param.toString());
    logger.trace("-------------------------");
    
    if ( r_res_cd.equals("0000") )
    {
        Date today=new Date();
        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmss");       
        String strCurYear = formater.format(today);
        
        try 
        {
            //DB Connection을 가져온다.
            Context initContext = new InitialContext();                                            //2
            DataSource ds = (DataSource) initContext.lookup("java:comp/env/jdbc/okpay");     //3
            conn = ds.getConnection();                                                             //4
            conn.setAutoCommit(false);
            
            logger.debug("-- strTpNotiseq gen--");
            
            //noti수신일련번호를 만든다..
            String strTpNotiseq = null;
            String sb_TpNotiSql = "select concat(DATE_FORMAT(now(),'%Y%m%d%H%i%S'), LPAD(f_get_sequence('TB_KICC_NOTI'),8,'0'))";
            pstmt = conn.prepareStatement(sb_TpNotiSql);
            rs = pstmt.executeQuery();
            if(rs != null && rs.next())
            {
               strTpNotiseq = rs.getString(1);
            }
            
            closeResultSet(rs);
            closePstmt(pstmt);
            
            logger.debug("-- noti info insert start--");
            
            //noti수신정보를 무조건 저장한다.
            StringBuffer sb_notiinf_ins_sql = new StringBuffer();
            sb_notiinf_ins_sql.append(" insert into TB_KICC_NOTI ");
            sb_notiinf_ins_sql.append(" ( ");
            sb_notiinf_ins_sql.append(" 	noti_seq, ");			
            sb_notiinf_ins_sql.append(" 	res_cd, ");			
            sb_notiinf_ins_sql.append(" 	res_msg, ");		
            sb_notiinf_ins_sql.append(" 	cno, ");			
            sb_notiinf_ins_sql.append(" 	memb_id, ");		
            sb_notiinf_ins_sql.append(" 	amount, ");			
            sb_notiinf_ins_sql.append(" 	order_no, ");		
            sb_notiinf_ins_sql.append(" 	noti_type, ");		
            sb_notiinf_ins_sql.append(" 	auth_no, ");			
            sb_notiinf_ins_sql.append(" 	tran_date, ");		
            sb_notiinf_ins_sql.append(" 	card_no, ");			
            sb_notiinf_ins_sql.append(" 	issuer_cd, ");		
            sb_notiinf_ins_sql.append(" 	issuer_nm, ");		
            sb_notiinf_ins_sql.append(" 	acquirer_cd, ");		
            sb_notiinf_ins_sql.append(" 	acquirer_nm, ");		
            sb_notiinf_ins_sql.append(" 	install_period, ");		
            sb_notiinf_ins_sql.append(" 	noint, ");			
            sb_notiinf_ins_sql.append(" 	bank_cd, ");			
            sb_notiinf_ins_sql.append(" 	bank_nm, ");		
            sb_notiinf_ins_sql.append(" 	account_no, ");		
            sb_notiinf_ins_sql.append(" 	deposit_nm, ");		
            sb_notiinf_ins_sql.append(" 	expire_date, ");		
            sb_notiinf_ins_sql.append(" 	cash_res_cd, ");		
            sb_notiinf_ins_sql.append(" 	cash_res_msg, ");	
            sb_notiinf_ins_sql.append(" 	cash_auth_no, ");		
            sb_notiinf_ins_sql.append(" 	cash_tran_date, ");	
            sb_notiinf_ins_sql.append(" 	cp_cd, ");			
            sb_notiinf_ins_sql.append(" 	used_pnt, ");		
            sb_notiinf_ins_sql.append(" 	remain_pnt, ");		
            sb_notiinf_ins_sql.append(" 	pay_pnt, ");			
            sb_notiinf_ins_sql.append(" 	accrue_pnt, ");		
            sb_notiinf_ins_sql.append(" 	escrow_yn, ");	
            sb_notiinf_ins_sql.append(" 	canc_date, ");		
            sb_notiinf_ins_sql.append(" 	canc_acq_date, ");	
            sb_notiinf_ins_sql.append(" 	refund_date, ");		
            sb_notiinf_ins_sql.append(" 	pay_type, ");		
            sb_notiinf_ins_sql.append(" 	auth_cno, ");		
            sb_notiinf_ins_sql.append(" 	tlf_sno, ");			
            sb_notiinf_ins_sql.append(" 	account_type, ");		
            sb_notiinf_ins_sql.append(" 	reserve1, ");		
            sb_notiinf_ins_sql.append(" 	reserve2, ");		
            sb_notiinf_ins_sql.append(" 	reserve3, ");		
            sb_notiinf_ins_sql.append(" 	reserve4, ");		
            sb_notiinf_ins_sql.append(" 	reserve5, ");		
            sb_notiinf_ins_sql.append(" 	reserve6, ");		
            sb_notiinf_ins_sql.append(" 	user_id, ");			
            sb_notiinf_ins_sql.append(" 	user_nm, ");		
            sb_notiinf_ins_sql.append(" 	INS_USER, ");             
            sb_notiinf_ins_sql.append(" 	MOD_USER ");        
            sb_notiinf_ins_sql.append(" ) values ( ");
            sb_notiinf_ins_sql.append(" 	?, ");				//noti_seq		
            sb_notiinf_ins_sql.append(" 	?, ");				//res_cd			
            sb_notiinf_ins_sql.append(" 	?, ");				//res_msg		
            sb_notiinf_ins_sql.append(" 	?, ");				//cno			
            sb_notiinf_ins_sql.append(" 	?, ");				//memb_id		
            sb_notiinf_ins_sql.append(" 	?, ");				//amount			
            sb_notiinf_ins_sql.append(" 	?, ");				//order_no		
            sb_notiinf_ins_sql.append(" 	?, ");				//noti_type		
            sb_notiinf_ins_sql.append(" 	?, ");				//auth_no			
            sb_notiinf_ins_sql.append(" 	?, ");				//tran_date		
            sb_notiinf_ins_sql.append(" 	?, ");				//card_no			
            sb_notiinf_ins_sql.append(" 	?, ");				//issuer_cd		
            sb_notiinf_ins_sql.append(" 	?, ");				//issuer_nm		
            sb_notiinf_ins_sql.append(" 	?, ");				//acquirer_cd		
            sb_notiinf_ins_sql.append(" 	?, ");				//acquirer_nm		
            sb_notiinf_ins_sql.append(" 	?, ");				//install_period		
            sb_notiinf_ins_sql.append(" 	?, ");				//noint			
            sb_notiinf_ins_sql.append(" 	?, ");				//bank_cd			
            sb_notiinf_ins_sql.append(" 	?, ");				//bank_nm		
            sb_notiinf_ins_sql.append(" 	?, ");				//account_no		
            sb_notiinf_ins_sql.append(" 	?, ");				//deposit_nm		
            sb_notiinf_ins_sql.append(" 	?, ");				//expire_date		
            sb_notiinf_ins_sql.append(" 	?, ");				//cash_res_cd		
            sb_notiinf_ins_sql.append(" 	?, ");				//cash_res_msg	
            sb_notiinf_ins_sql.append(" 	?, ");				//cash_auth_no		
            sb_notiinf_ins_sql.append(" 	?, ");				//cash_tran_date	
            sb_notiinf_ins_sql.append(" 	?, ");				//cp_cd			
            sb_notiinf_ins_sql.append(" 	?, ");				//used_pnt		
            sb_notiinf_ins_sql.append(" 	?, ");				//remain_pnt	
            sb_notiinf_ins_sql.append(" 	?, ");				//pay_pnt			
            sb_notiinf_ins_sql.append(" 	?, ");				//accrue_pnt	
            sb_notiinf_ins_sql.append(" 	?, ");				//escrow_yn	
            sb_notiinf_ins_sql.append(" 	?, ");				//canc_date		
            sb_notiinf_ins_sql.append(" 	?, ");				//canc_acq_date	
            sb_notiinf_ins_sql.append(" 	?, ");				//refund_date		
            sb_notiinf_ins_sql.append(" 	?, ");				//pay_type		
            sb_notiinf_ins_sql.append(" 	?, ");				//auth_cno		
            sb_notiinf_ins_sql.append(" 	?, ");				//tlf_sno			
            sb_notiinf_ins_sql.append(" 	?, ");				//account_type		
            sb_notiinf_ins_sql.append(" 	?, ");				//reserve1		
            sb_notiinf_ins_sql.append(" 	?, ");				//reserve2		
            sb_notiinf_ins_sql.append(" 	?, ");				//reserve3		
            sb_notiinf_ins_sql.append(" 	?, ");				//reserve4		
            sb_notiinf_ins_sql.append(" 	?, ");				//reserve5		
            sb_notiinf_ins_sql.append(" 	?, ");				//reserve6		
            sb_notiinf_ins_sql.append(" 	?, ");				//user_id			
            sb_notiinf_ins_sql.append(" 	?, ");				//user_nm		
            sb_notiinf_ins_sql.append(" 	?, ");				//INS_USER             
            sb_notiinf_ins_sql.append(" 	? ");				//MOD_USER           
            sb_notiinf_ins_sql.append(" ) ");
            
            pstmt = conn.prepareStatement(sb_notiinf_ins_sql.toString());
            pstmt.setString(1		,strTpNotiseq); //noti_seq		
            pstmt.setString(2		,r_res_cd		); //res_cd			
            pstmt.setString(3		,r_res_msg		); //res_msg		
            pstmt.setString(4		,r_cno		); //cno			
            pstmt.setString(5		,r_memb_id	); //memb_id		
            pstmt.setString(6		,r_amount		); //amount			
            pstmt.setString(7		,r_order_no		); //order_no		
            pstmt.setString(8		,r_noti_type		); //noti_type		
            pstmt.setString(9		,r_auth_no		); //auth_no			
            pstmt.setString(10	,r_tran_date	); //tran_date		
            pstmt.setString(11	,r_card_no		); //card_no			
            pstmt.setString(12	,r_issuer_cd	); //issuer_cd		
            pstmt.setString(13	,r_issuer_nm	); //issuer_nm		
            pstmt.setString(14	,r_acquirer_cd	); //acquirer_cd		
            pstmt.setString(15	,r_acquirer_nm	); //acquirer_nm		
            pstmt.setString(16	,r_install_period	); //install_period		
            pstmt.setString(17	,r_noint		); //noint			
            pstmt.setString(18	,r_bank_cd		); //bank_cd			
            pstmt.setString(19	,r_bank_nm	); //bank_nm		
            pstmt.setString(20	,r_account_no	); //account_no		
            pstmt.setString(21	,r_deposit_nm	); //deposit_nm		
            pstmt.setString(22	,r_expire_date	); //expire_date		
            pstmt.setString(23	,r_cash_res_cd	); //cash_res_cd		
            pstmt.setString(24	,r_cash_res_msg);//cash_res_msg	
            pstmt.setString(25	,r_cash_auth_no	); //cash_auth_no		
            pstmt.setString(26	,r_cash_tran_date); //cash_tran_date	
            pstmt.setString(27	,r_cp_cd		); //cp_cd			
            pstmt.setString(28	,r_used_pnt	); //used_pnt		
            pstmt.setString(29	,r_remain_pnt	); //remain_pnt	
            pstmt.setString(30	,r_pay_pnt		); //pay_pnt			
            pstmt.setString(31	,r_accrue_pnt	); //accrue_pnt	
            pstmt.setString(32	,r_escrow_yn	); //escrow_yn	
            pstmt.setString(33	,r_canc_date	); //canc_date		
            pstmt.setString(34	,r_canc_acq_date); //canc_acq_date	
            pstmt.setString(35	,r_refund_date	); //refund_date		
            pstmt.setString(36	,r_pay_type	); //pay_type		
            pstmt.setString(37	,r_auth_cno	); //auth_cno		
            pstmt.setString(38	,r_tlf_sno		); //tlf_sno			
            pstmt.setString(39	,r_account_type	); //account_type		
            pstmt.setString(40	,r_reserve1		); //reserve1		
            pstmt.setString(41	,r_reserve2		); //reserve2		
            pstmt.setString(42	,r_reserve3		); //reserve3		
            pstmt.setString(43	,r_reserve4		); //reserve4		
            pstmt.setString(44	,r_reserve5		); //reserve5		
            pstmt.setString(45	,r_reserve6		); //reserve6		
            pstmt.setString(46	,r_user_id		); //user_id			
            pstmt.setString(47	,r_user_nm		); //user_nm		
            pstmt.setString(48	,"0"); //INS_USER             
            pstmt.setString(49	,"0"); //MOD_USER   
            
            int intNotiIns = pstmt.executeUpdate();
            
            conn.commit();    
            
            closePstmt(pstmt);

            logger.debug("-- onoffmerchinfo search --");
            
            //수신된 정보로 가맹점 정보를 검색
            StringBuffer sb_onffTidInfo = new StringBuffer();

            sb_onffTidInfo.append(" select m.onfftid, m.onffmerch_no, m.pay_chn_cate, n.terminal_no, n.merch_no ");
            sb_onffTidInfo.append(" from TB_ONFFTID_MST m, ");
            sb_onffTidInfo.append("       ( ");
            sb_onffTidInfo.append("           SELECT b.pay_mtd_seq, a.start_dt, a.end_dt, b.APP_ISS_CD, b.TID_MTD, b.TERMINAL_NO,b.PAY_MTD,b.MERCH_NO  ");
            sb_onffTidInfo.append("            FROM TB_PAY_MTD a ");
            sb_onffTidInfo.append("                      ,TB_PAY_MTD_DETAIL b ");
            sb_onffTidInfo.append("            WHERE  ");
            sb_onffTidInfo.append("            a.PAY_MTD_SEQ = b.PAY_MTD_SEQ ");
            sb_onffTidInfo.append("            and b.DEL_FLAG  = 'N' ");
            sb_onffTidInfo.append("            and b.USE_FLAG = 'Y' ");
            sb_onffTidInfo.append("            and b.DEL_FLAG  = 'N' ");
            sb_onffTidInfo.append("            and b.USE_FLAG = 'Y' ");
            sb_onffTidInfo.append("            and a.start_dt <= DATE_FORMAT(now(),'%Y%m%d') ");
            sb_onffTidInfo.append("            and a.end_dt  >= DATE_FORMAT(now(),'%Y%m%d') ");
            sb_onffTidInfo.append("       ) n ");
            sb_onffTidInfo.append(" where  ");
            sb_onffTidInfo.append(" m.PAY_MTD_SEQ = n.pay_mtd_seq        ");
            sb_onffTidInfo.append(" and m.del_flag = 'N'  ");
            sb_onffTidInfo.append(" and n.TERMINAL_NO = ? ");

            pstmt = conn.prepareStatement(sb_onffTidInfo.toString());

            pstmt.setString(1, r_memb_id);

            rs = pstmt.executeQuery();

            String strOnffTid = "";
            String strOnffMerchNo = "";
            String strPayChnCate = "";

            if(rs != null && rs.next())
            {
                strOnffTid = getNullToSpace(rs.getString(1));
                strOnffMerchNo = getNullToSpace(rs.getString(2));
                strPayChnCate =  getNullToSpace(rs.getString(3));
            }
            
            closeResultSet(rs);
            closePstmt(pstmt);
           
            logger.trace("strOnffTid : "+ strOnffTid);
            logger.trace("strOnffMerchNo : "+ strOnffMerchNo);
            logger.trace("strPayChnCate : " + strPayChnCate);
            
            String strTpCms = "";
            
            //승인이 가맹점정보가 있을시 수수료 정보를 가져온다.
            if(!strOnffTid.equals("") && !strOnffMerchNo.equals("") && r_noti_type.equals("10"))
            {
                logger.debug("-- onofftidinfo commisioninfo search --");
                //가맹점 정보에 대한 수수료 정보를 가져온다.
                StringBuffer sb_onffcms = new StringBuffer();
                sb_onffcms.append(" SELECT ONFFTID_CMS_SEQ, ONFFTID, COMMISSION, START_DT, END_DT ");
                sb_onffcms.append("  FROM TB_ONFFTID_COMMISSION ");
                sb_onffcms.append("  WHERE ONFFTID = ? ");
                sb_onffcms.append("  and DEL_FLAG  = 'N' ");
                sb_onffcms.append("  and USE_FLAG = 'Y' ");
                sb_onffcms.append("  and START_DT <= ? ");
                sb_onffcms.append("  and END_DT >= ? ");

                String strTpAppDt = r_tran_date.substring(0,8);

                pstmt = conn.prepareStatement(sb_onffcms.toString());

                pstmt.setString(1, strOnffTid);
                pstmt.setString(2, strTpAppDt);
                pstmt.setString(3, strTpAppDt);
                rs = pstmt.executeQuery();  

                if(rs != null && rs.next())
                {
                    strTpCms = rs.getString(3);
                }
                closeResultSet(rs);
                closePstmt(pstmt);
                
                logger.trace("strTpCms : " + strTpCms);
            }            
            
            //가맹점번호가 없을경우
            if(strOnffTid.equals("") || strOnffMerchNo.equals(""))
            {
                logger.debug("-- onoff merch, tild info null --");
                //noti처리상태 update
                StringBuffer sb_noti_mod_sql = new StringBuffer();
                sb_noti_mod_sql.append(" UPDATE TB_KICC_NOTI SET ");
                sb_noti_mod_sql.append(" proc_flag=? ");
                sb_noti_mod_sql.append(" ,mod_dt = DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                sb_noti_mod_sql.append(" ,mod_user= '0' ");                
                sb_noti_mod_sql.append(" WHERE noti_seq = ? ");
                pstmt = conn.prepareStatement(sb_noti_mod_sql.toString());
                pstmt.setString(1, "50");
                pstmt.setString(2, strTpNotiseq);
                int int_mod_notiinfo = pstmt.executeUpdate();  
                
                closeResultSet(rs);
                closePstmt(pstmt);
            }
            //승인이면서 수수료 정보가 없는경우
            else if( r_noti_type.equals("10") && strTpCms.equals(""))
            {
                logger.debug("-- onoff tid cms info null --");
                //noti처리상태 update
                StringBuffer sb_noti_mod_sql = new StringBuffer();
                sb_noti_mod_sql.append(" UPDATE TB_KICC_NOTI SET ");
                sb_noti_mod_sql.append(" proc_flag=? ");
                sb_noti_mod_sql.append(" ,mod_dt = DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                sb_noti_mod_sql.append(" ,mod_user= '0' ");                
                sb_noti_mod_sql.append(" WHERE noti_seq = ? ");
                pstmt = conn.prepareStatement(sb_noti_mod_sql.toString());
                pstmt.setString(1, "50");
                pstmt.setString(2, strTpNotiseq);
                int int_mod_notiinfo = pstmt.executeUpdate();  
                
                closeResultSet(rs);
                closePstmt(pstmt);
            }
            else
            {
                 logger.debug("-- onoff info check sucess --");
                //승인 경우
                if(r_noti_type.equals("10"))
                {
                    logger.debug("-- notiinfo app --");
                    //신용카드일 경우
                    if(r_pay_type.equals("11"))
                    {
                        logger.debug("-- notiinfo app card --");
                        
                        //이미 입력거래가 있는경우
                        StringBuffer sbTranChkSql = new StringBuffer();
                        sbTranChkSql.append(" select count(1) cnt ");
                        sbTranChkSql.append(" from VW_TRAN_CARDPG ");
                        sbTranChkSql.append(" where pg_seq = ? ");
                        sbTranChkSql.append(" and TID = ? ");
                        sbTranChkSql.append(" and massagetype='10' ");

                        pstmt = conn.prepareStatement(sbTranChkSql.toString());

                        pstmt.setString(1, r_cno);
                        pstmt.setString(2, r_memb_id);

                        rs = pstmt.executeQuery();

                        int int_appChk_cnt = 0;

                        if(rs != null && rs.next())
                        {
                            int_appChk_cnt = rs.getInt("cnt");
                        }

                        closeResultSet(rs);
                        closePstmt(pstmt);                    
                        logger.debug("-- tran cnt chkeck --");
                        String strNotResult = null;
                        //접수된 승인건이 없는 경우
                        if(int_appChk_cnt == 0)
                        {
                            logger.debug("-- tran cnt chkeck ok  --");
                            logger.debug("-- isscd transger  --");
                            //발급사코드 변환
                            StringBuffer sb_isscd = new StringBuffer();
                            sb_isscd.append(" select main_code, detail_code, code_nm detailcode_nm ");
                            sb_isscd.append(" from TB_CODE_DETAIL ");
                            sb_isscd.append(" where main_code = 'ISS_CD' ");
                            sb_isscd.append(" and use_flag = 'Y' ");
                            sb_isscd.append(" and del_flag='N' ");
                            sb_isscd.append(" and detail_code = f_get_iss_cd('KICCPG', ?) ");
                            
                            pstmt = conn.prepareStatement(sb_isscd.toString());
                            pstmt.setString(1, r_issuer_cd);

                            rs = pstmt.executeQuery();                            

                            String strOnffIssCd = null;
                            String strOnffIssNm = null;

                            if(rs != null && rs.next())
                            {
                                strOnffIssCd = rs.getString(2);
                                strOnffIssNm = rs.getString(3);
                            }    
                            else
                            {
                                strOnffIssCd = "091";
                                strOnffIssNm = "기타카드";
                            }
                            closeResultSet(rs);
                            closePstmt(pstmt);
                            
                            logger.debug("-- acqcd transger  --");
                            //매입사코드변환
                            StringBuffer sb_acqcd = new StringBuffer();
                            sb_acqcd.append(" select main_code, detail_code, code_nm detailcode_nm ");
                            sb_acqcd.append(" from TB_CODE_DETAIL ");
                            sb_acqcd.append(" where main_code = 'ISS_CD' ");
                            sb_acqcd.append(" and use_flag = 'Y' ");
                            sb_acqcd.append(" and del_flag='N' ");
                            sb_acqcd.append(" and detail_code = f_get_iss_cd('KICCPG', ?) ");
                            
                            pstmt = conn.prepareStatement(sb_acqcd.toString());
                            pstmt.setString(1, r_acquirer_cd);
                            
                            rs = pstmt.executeQuery(); 
                            
                            String strOnffAcqCd = null;
                            String strOnffAcqNm = null;

                            if(rs != null && rs.next())
                            {
                                strOnffAcqCd = rs.getString(2);
                                strOnffAcqNm = rs.getString(3);
                            }
                            else
                            {
                                strOnffAcqCd = "091";
                                strOnffAcqNm = "기타카드";
                            }
                            closeResultSet(rs);
                            closePstmt(pstmt);

                            logger.debug("-- isscd transger  --");
                            //수수료를 계산한다.
                            double dbl_TpCms = Double.parseDouble(strTpCms)/Double.parseDouble("100");
                            double dbl_TpApp_mat = Double.parseDouble(r_amount);
                            double dbl_onfftid_cms = Math.floor(dbl_TpCms * dbl_TpApp_mat);
                            double dbl_onfftid_payamt = dbl_TpApp_mat - dbl_onfftid_cms;


                            logger.debug("-- strTpTranseq create  --");
                            //거래일련번호를 생성한다.
                            String strTpTranseq = null;
                            String sb_TpTranseqSql = "select concat(DATE_FORMAT(now(),'%Y%m%d%H%i%S'), LPAD(f_get_sequence('TRANSEQ'),8,'0'))";
                            pstmt = conn.prepareStatement(sb_TpTranseqSql);
                            rs = pstmt.executeQuery();
                            if(rs != null && rs.next())
                            {
                                strTpTranseq = rs.getString(1);
                            }                

                            closeResultSet(rs);
                            closePstmt(pstmt);
                            logger.debug("strTpTranseq : "+strTpTranseq);
                            logger.debug("-- traninfo insert  --");
                            //해당거래정보를 insert 한다.
                            StringBuffer sb_instraninfo = new StringBuffer();
                            sb_instraninfo.append(" insert ");
                            sb_instraninfo.append(" into TB_TRAN_CARDPG_"+r_tran_date.substring(2, 6) + " ( ");
                            sb_instraninfo.append(" TRAN_SEQ			 ");
                            sb_instraninfo.append(" ,MASSAGETYPE		 ");
                            sb_instraninfo.append(" ,CARD_NUM			 ");
                            sb_instraninfo.append(" ,APP_DT				 ");
                            sb_instraninfo.append(" ,APP_TM				 ");
                            sb_instraninfo.append(" ,APP_NO				 ");
                            sb_instraninfo.append(" ,TOT_AMT			 ");	
                            sb_instraninfo.append(" ,PG_SEQ				 ");
                            sb_instraninfo.append(" ,PAY_CHN_CATE		 ");	
                            sb_instraninfo.append(" ,TID				 ");	
                            sb_instraninfo.append(" ,ONFFTID			 ");	
                            sb_instraninfo.append(" ,WCC				 ");	
                            sb_instraninfo.append(" ,EXPIRE_DT			 ");
                            sb_instraninfo.append(" ,CARD_CATE			 ");
                            sb_instraninfo.append(" ,ORDER_SEQ			 ");
                            sb_instraninfo.append(" ,TRAN_DT			 ");
                            sb_instraninfo.append(" ,TRAN_TM			 ");	
                            sb_instraninfo.append(" ,TRAN_CATE			 ");
                            sb_instraninfo.append(" ,FREE_INST_FLAG		 ");
                            sb_instraninfo.append(" ,TAX_AMT			 ");	
                            sb_instraninfo.append(" ,SVC_AMT			 ");	
                            sb_instraninfo.append(" ,CURRENCY			 ");
                            sb_instraninfo.append(" ,INSTALLMENT		 ");	
                            sb_instraninfo.append(" ,ISS_CD				 ");
                            sb_instraninfo.append(" ,ISS_NM				 ");
                            sb_instraninfo.append(" ,APP_ISS_CD			 ");
                            sb_instraninfo.append(" ,APP_ISS_NM			 ");
                            sb_instraninfo.append(" ,ACQ_CD				 ");
                            sb_instraninfo.append(" ,ACQ_NM			 ");
                            sb_instraninfo.append(" ,APP_ACQ_CD			 ");
                            sb_instraninfo.append(" ,APP_ACQ_NM			 ");
                            sb_instraninfo.append(" ,MERCH_NO			 ");
                            sb_instraninfo.append(" ,ORG_MERCH_NO		 ");
                            sb_instraninfo.append(" ,RESULT_CD			 ");
                            sb_instraninfo.append(" ,RESULT_MSG			 ");
                            sb_instraninfo.append(" ,ORG_APP_DD			 ");
                            sb_instraninfo.append(" ,ORG_APP_NO			 ");
                            sb_instraninfo.append(" ,CNCL_REASON		 ");	
                            sb_instraninfo.append(" ,TRAN_STATUS		 ");	
                            sb_instraninfo.append(" ,RESULT_STATUS		 ");
                            sb_instraninfo.append(" ,TRAN_STEP			 ");
                            sb_instraninfo.append(" ,CNCL_DT			 ");	
                            sb_instraninfo.append(" ,ACQ_DT				 ");
                            sb_instraninfo.append(" ,ACQ_RESULT_CD		 ");
                            sb_instraninfo.append(" ,HOLDOFF_DT			 ");
                            sb_instraninfo.append(" ,HOLDON_DT			 ");
                            sb_instraninfo.append(" ,PAY_DT				 ");
                            sb_instraninfo.append(" ,PAY_AMT			 ");	
                            sb_instraninfo.append(" ,COMMISION			 ");
                            sb_instraninfo.append(" ,ONOFFTID_PAY_AMT	 ");	
                            sb_instraninfo.append(" ,ONOFFTID_COMMISION	 ");
                            sb_instraninfo.append(" ,CLIENT_IP			 ");
                            sb_instraninfo.append(" ,USER_TYPE			 ");
                            sb_instraninfo.append(" ,MEMB_USER_NO		 ");
                            sb_instraninfo.append(" ,USER_ID				 ");
                            sb_instraninfo.append(" ,USER_NM			 ");
                            sb_instraninfo.append(" ,USER_MAIL			 ");
                            sb_instraninfo.append(" ,USER_PHONE1		 ");
                            sb_instraninfo.append(" ,USER_PHONE2		 ");
                            sb_instraninfo.append(" ,USER_ADDR			 ");
                            sb_instraninfo.append(" ,PRODUCT_TYPE		 ");
                            sb_instraninfo.append(" ,PRODUCT_NM		 ");
                            sb_instraninfo.append(" ,FILLER				 ");
                            sb_instraninfo.append(" ,FILLER2				 ");
                            sb_instraninfo.append(" ,TERM_FILLER1		 ");
                            sb_instraninfo.append(" ,TERM_FILLER2		 ");	
                            sb_instraninfo.append(" ,TERM_FILLER3		 ");	
                            sb_instraninfo.append(" ,TERM_FILLER4		 ");	
                            sb_instraninfo.append(" ,TERM_FILLER5		 ");	
                            sb_instraninfo.append(" ,TERM_FILLER6		 ");	
                            sb_instraninfo.append(" ,TERM_FILLER7		 ");	
                            sb_instraninfo.append(" ,TERM_FILLER8		 ");	
                            sb_instraninfo.append(" ,TERM_FILLER9		 ");	
                            sb_instraninfo.append(" ,TERM_FILLER10		 ");
                            sb_instraninfo.append(" ,TRAD_CHK_FLAG		 ");
                            sb_instraninfo.append(" ,ACC_CHK_FLAG		 ");
                            sb_instraninfo.append(" ,INS_DT				 ");
                            sb_instraninfo.append(" ,MOD_DT				 ");
                            sb_instraninfo.append(" ,INS_USER			 ");	
                            sb_instraninfo.append(" ,MOD_USER			 ");
                            sb_instraninfo.append(" ,ORG_PG_SEQ			 ");
                            sb_instraninfo.append(" ,ONFFMERCH_NO	 ");
                            sb_instraninfo.append(" ,auth_pw ");
                            sb_instraninfo.append(" ,auth_value ");
                            sb_instraninfo.append(" ,app_card_num ");
                            sb_instraninfo.append(" )  ");
                            sb_instraninfo.append(" values ( ");
                            sb_instraninfo.append("  ? ");//tran_seq			
                            sb_instraninfo.append(" ,? ");//massagetype			
                            sb_instraninfo.append(" ,? ");//card_num			
                            sb_instraninfo.append(" ,? ");//app_dt			
                            sb_instraninfo.append(" ,? ");//app_tm			
                            sb_instraninfo.append(" ,? ");//app_no			
                            sb_instraninfo.append(" ,? ");//tot_amt			
                            sb_instraninfo.append(" ,? ");//pg_seq			
                            sb_instraninfo.append(" ,? ");//pay_chn_cate		
                            sb_instraninfo.append(" ,? ");//tid				
                            sb_instraninfo.append(" ,? ");//onfftid			
                            sb_instraninfo.append(" ,? ");//wcc				
                            sb_instraninfo.append(" ,? ");//expire_dt			
                            sb_instraninfo.append(" ,? ");//card_cate			
                            sb_instraninfo.append(" ,? ");//order_seq			
                            sb_instraninfo.append(" ,? ");//tran_dt			
                            sb_instraninfo.append(" ,? ");//tran_tm			
                            sb_instraninfo.append(" ,? ");//tran_cate			
                            sb_instraninfo.append(" ,? ");//free_inst_flag		
                            sb_instraninfo.append(" ,? ");//tax_amt			
                            sb_instraninfo.append(" ,? ");//svc_amt			
                            sb_instraninfo.append(" ,? ");//currency			
                            sb_instraninfo.append(" ,? ");//installment			
                            sb_instraninfo.append(" ,? ");//iss_cd			
                            sb_instraninfo.append(" ,? ");//iss_nm			
                            sb_instraninfo.append(" ,? ");//app_iss_cd			
                            sb_instraninfo.append(" ,? ");//app_iss_nm			
                            sb_instraninfo.append(" ,? ");//acq_cd			
                            sb_instraninfo.append(" ,? ");//acq_nm			
                            sb_instraninfo.append(" ,? ");//app_acq_cd			
                            sb_instraninfo.append(" ,? ");//app_acq_nm			
                            sb_instraninfo.append(" ,? ");//merch_no			
                            sb_instraninfo.append(" ,? ");//org_merch_no		
                            sb_instraninfo.append(" ,? ");//result_cd			
                            sb_instraninfo.append(" ,? ");//result_msg			
                            sb_instraninfo.append(" ,? ");//org_app_dd			
                            sb_instraninfo.append(" ,? ");//org_app_no			
                            sb_instraninfo.append(" ,? ");//cncl_reason			
                            sb_instraninfo.append(" ,? ");//tran_status			
                            sb_instraninfo.append(" ,? ");//result_status		
                            sb_instraninfo.append(" ,? ");//tran_step			
                            sb_instraninfo.append(" ,? ");//cncl_dt			
                            sb_instraninfo.append(" ,? ");//acq_dt			
                            sb_instraninfo.append(" ,? ");//acq_result_cd
                            sb_instraninfo.append(" ,? ");//holdoff_dt
                            sb_instraninfo.append(" ,? ");//holdon_dt
                            sb_instraninfo.append(" ,? ");//pay_dt
                            sb_instraninfo.append(" ,? ");//pay_amt
                            sb_instraninfo.append(" ,? ");//commision
                            sb_instraninfo.append(" ,? ");//onofftid_pay_amt
                            sb_instraninfo.append(" ,? ");//onofftid_commision
                            sb_instraninfo.append(" ,? ");//client_ip
                            sb_instraninfo.append(" ,? ");//user_type
                            sb_instraninfo.append(" ,? ");//memb_user_no
                            sb_instraninfo.append(" ,? ");//user_id
                            sb_instraninfo.append(" ,? ");//user_nm
                            sb_instraninfo.append(" ,? ");//user_mail
                            sb_instraninfo.append(" ,? ");//user_phone1
                            sb_instraninfo.append(" ,? ");//user_phone2
                            sb_instraninfo.append(" ,? ");//user_addr
                            sb_instraninfo.append(" ,? ");//product_type
                            sb_instraninfo.append(" ,? ");//product_nm
                            sb_instraninfo.append(" ,? ");//filler
                            sb_instraninfo.append(" ,? ");//filler2
                            sb_instraninfo.append(" ,? ");//term_filler1
                            sb_instraninfo.append(" ,? ");//term_filler2
                            sb_instraninfo.append(" ,? ");//term_filler3
                            sb_instraninfo.append(" ,? ");//term_filler4
                            sb_instraninfo.append(" ,? ");//term_filler5
                            sb_instraninfo.append(" ,? ");//term_filler6
                            sb_instraninfo.append(" ,? ");//term_filler7
                            sb_instraninfo.append(" ,? ");//term_filler8
                            sb_instraninfo.append(" ,? ");//term_filler9
                            sb_instraninfo.append(" ,? ");//term_filler10
                            sb_instraninfo.append(" ,? ");//trad_chk_flag
                            sb_instraninfo.append(" ,? ");//acc_chk_flag
                            sb_instraninfo.append("  , DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                            sb_instraninfo.append("  , DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                            sb_instraninfo.append(" ,? ");//ins_user			
                            sb_instraninfo.append(" ,? ");//mod_user			
                            sb_instraninfo.append(" ,? ");//org_pg_seq			
                            sb_instraninfo.append(" ,? ");//onffmerch_no		
                            sb_instraninfo.append(" ,? ");//auth_pw		  
                            sb_instraninfo.append(" ,? ");//auth_value		  
                            sb_instraninfo.append(" ,? ");//app_card_num	
                            sb_instraninfo.append(" ) ");          

                            pstmt = conn.prepareStatement(sb_instraninfo.toString());

                            pstmt.setString(1,strTpTranseq);//tran_seq				, jdbcType=VARCHAR} 
                            pstmt.setString(2,"10");//massagetype			, jdbcType=VARCHAR} 
                            pstmt.setString(3,r_card_no);//card_num			, jdbcType=VARCHAR} 
                            pstmt.setString(4,r_tran_date.substring(0, 8));//app_dt				, jdbcType=VARCHAR}
                            pstmt.setString(5,r_tran_date.substring(8, 14));//app_tm				 , jdbcType=VARCHAR}
                            pstmt.setString(6,r_auth_no);//app_no				, jdbcType=VARCHAR}
                            pstmt.setString(7,r_amount);//tot_amt				 , jdbcType=INTEGER }
                            pstmt.setString(8,r_cno);//pg_seq				, jdbcType=VARCHAR}
                            pstmt.setString(9,strPayChnCate);//pay_chn_cate			 , jdbcType=VARCHAR}
                            pstmt.setString(10,r_memb_id);//tid					, jdbcType=VARCHAR}
                            pstmt.setString(11,strOnffTid);//onfftid				, jdbcType=VARCHAR}
                            pstmt.setString(12,null);//wcc					, jdbcType=VARCHAR}
                            pstmt.setString(13,"0000");//expire_dt			, jdbcType=VARCHAR}
                            pstmt.setString(14,null);//card_cate			  , jdbcType=VARCHAR}
                            pstmt.setString(15,r_order_no);//order_seq			  , jdbcType=VARCHAR}
                            pstmt.setString(16,r_tran_date.substring(0, 8));//tran_dt				, jdbcType=VARCHAR}
                            pstmt.setString(17,r_tran_date.substring(8, 14));//tran_tm				, jdbcType=VARCHAR}
                            //pstmt.setString(18,r_noti_type);//tran_cate			  , jdbcType=VARCHAR}
                            pstmt.setString(18,"20");//tran_cate			  , jdbcType=VARCHAR}
                            pstmt.setString(19,r_noint);//free_inst_flag		    , jdbcType=VARCHAR}
                            pstmt.setDouble(20,0);//tax_amt				, jdbcType=INTEGER }
                            pstmt.setDouble(21,0);//svc_amt				, jdbcType=INTEGER }
                            pstmt.setString(22,null);//currency			 , jdbcType=VARCHAR}
                            pstmt.setString(23,r_install_period);//installment			   , jdbcType=VARCHAR}
                            pstmt.setString(24,strOnffIssCd);//iss_cd				, jdbcType=VARCHAR}
                            pstmt.setString(25,strOnffIssNm);//iss_nm				 , jdbcType=VARCHAR}
                            pstmt.setString(26,r_issuer_cd);//app_iss_cd			 , jdbcType=VARCHAR}
                            pstmt.setString(27,r_issuer_nm);//app_iss_nm			 , jdbcType=VARCHAR}
                            pstmt.setString(28,strOnffAcqCd);//acq_cd				, jdbcType=VARCHAR}
                            pstmt.setString(29,strOnffAcqNm);//acq_nm				, jdbcType=VARCHAR}
                            pstmt.setString(30,r_acquirer_cd);//app_acq_cd			  , jdbcType=VARCHAR}
                            pstmt.setString(31,r_acquirer_nm);//app_acq_nm			   , jdbcType=VARCHAR}
                            pstmt.setString(32,r_memb_id);//merch_no			  , jdbcType=VARCHAR}
                            pstmt.setString(33,r_memb_id);//org_merch_no			  , jdbcType=VARCHAR}
                            pstmt.setString(34,r_res_cd);//result_cd			 , jdbcType=VARCHAR}
                            pstmt.setString(35,r_res_msg);//result_msg			   , jdbcType=VARCHAR}
                            pstmt.setString(36,null);//org_app_dd			  , jdbcType=VARCHAR}
                            pstmt.setString(37,null);//org_app_no			   , jdbcType=VARCHAR}
                            pstmt.setString(38,null);//cncl_reason			, jdbcType=VARCHAR}
                            pstmt.setString(39,"00");//tran_status			   , jdbcType=VARCHAR}
                            pstmt.setString(40,"00");//result_status		     , jdbcType=VARCHAR}
                            pstmt.setString(41,"00");//tran_step			 , jdbcType=VARCHAR}
                            pstmt.setString(42,null);//cncl_dt				, jdbcType=VARCHAR}
                            pstmt.setString(43,null);//acq_dt				, jdbcType=VARCHAR}
                            pstmt.setString(44,null);//acq_result_cd		     , jdbcType=VARCHAR}
                            pstmt.setString(45,null);//holdoff_dt			  , jdbcType=VARCHAR}
                            pstmt.setString(46,null);//holdon_dt			  , jdbcType=VARCHAR}
                            pstmt.setString(47,null);//pay_dt				, jdbcType=VARCHAR}
                            pstmt.setString(48,null);//pay_amt				, jdbcType=INTEGER }
                            pstmt.setString(49,null);//commision			  , jdbcType=INTEGER }
                            pstmt.setDouble(50,dbl_onfftid_payamt);//onofftid_pay_amt		, jdbcType=INTEGER }
                            pstmt.setDouble(51,dbl_onfftid_cms);//onofftid_commision		, jdbcType=INTEGER }
                            pstmt.setString(52,null);//client_ip				, jdbcType=VARCHAR}
                            pstmt.setString(53,null);//user_type			, jdbcType=VARCHAR}
                            pstmt.setString(54,null);//memb_user_no		, jdbcType=VARCHAR}
                            pstmt.setString(55,r_user_id);//user_id				, jdbcType=VARCHAR}
                            pstmt.setString(56,r_user_nm);//user_nm				, jdbcType=VARCHAR}
                            pstmt.setString(57,null);//user_mail			    , jdbcType=VARCHAR}
                            pstmt.setString(58,null);//user_phone1			 , jdbcType=VARCHAR}
                            pstmt.setString(59,null);//user_phone2			 , jdbcType=VARCHAR}
                            pstmt.setString(60,null);//user_addr			, jdbcType=VARCHAR}
                            pstmt.setString(61,null);//product_type		  , jdbcType=VARCHAR}
                            pstmt.setString(62,null);//product_nm			 , jdbcType=VARCHAR}
                            pstmt.setString(63,null);//filler				   , jdbcType=VARCHAR}
                            pstmt.setString(64,null);//filler2				   , jdbcType=VARCHAR}
                            pstmt.setString(65,r_reserve1);//term_filler1			, jdbcType=VARCHAR}
                            pstmt.setString(66,r_reserve2);//term_filler2			, jdbcType=VARCHAR}
                            pstmt.setString(67,r_reserve3);//term_filler3			, jdbcType=VARCHAR}
                            pstmt.setString(68,r_reserve4);//term_filler4			, jdbcType=VARCHAR}
                            pstmt.setString(69,r_reserve5);//term_filler5			, jdbcType=VARCHAR}
                            pstmt.setString(70,r_reserve6);//term_filler6				, jdbcType=VARCHAR}
                            pstmt.setString(71,null);//term_filler7			  , jdbcType=VARCHAR}
                            pstmt.setString(72,null);//term_filler8			, jdbcType=VARCHAR}
                            pstmt.setString(73,null);//term_filler9			, jdbcType=VARCHAR}
                            pstmt.setString(74,null);//term_filler10			, jdbcType=VARCHAR}
                            pstmt.setString(75,"0");//trad_chk_flag		  , jdbcType=VARCHAR}
                            pstmt.setString(76,"0");//acc_chk_flag			, jdbcType=VARCHAR}
                            pstmt.setString(77,"0");//ins_user				     , jdbcType=VARCHAR}
                            pstmt.setString(78,"0");//mod_user			, jdbcType=VARCHAR}
                            pstmt.setString(79,null);//org_pg_seq			, jdbcType=VARCHAR}
                            pstmt.setString(80,strOnffMerchNo);//onffmerch_no		  , jdbcType=VARCHAR}
                            pstmt.setString(81,null);//auth_pw		  , jdbcType=VARCHAR}
                            pstmt.setString(82,null);//auth_value		  , jdbcType=VARCHAR}   
                            pstmt.setString(83,r_card_no);//app_card_num		  , jdbcType=VARCHAR}                

                            int int_insResult = pstmt.executeUpdate();                               
                            logger.debug("-- traninfo insert end  --");
                            closeResultSet(rs);
                            closePstmt(pstmt);

                            strNotResult = "10";//처리완료
                        }
                        //접수된 승인건이 있는 경우
                        else
                        {
                            logger.debug("-- traninfo dual!!  --");
                            strNotResult = "20";//기존재건 존재
                        }

                        logger.debug("-- notiinfo update!!  --");
                        //noti처리상태 update
                        StringBuffer sb_noti_mod_sql = new StringBuffer();
                        sb_noti_mod_sql.append(" UPDATE TB_KICC_NOTI SET ");
                        sb_noti_mod_sql.append(" proc_flag=? ");
                        sb_noti_mod_sql.append(" ,mod_dt = DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                        sb_noti_mod_sql.append(" ,mod_user= '0' ");                
                        sb_noti_mod_sql.append(" WHERE noti_seq = ? ");

                        pstmt = conn.prepareStatement(sb_noti_mod_sql.toString());

                        pstmt.setString(1, strNotResult);
                        pstmt.setString(2, strTpNotiseq);

                        int int_mod_notiinfo = pstmt.executeUpdate();
                    }
                }
                //취소일경우
                else if(r_noti_type.equals("20"))
                {
                     logger.debug("-- noti cncl  --");
                    //카드취소일 경우
                    if(r_pay_type.equals("11"))
                    {
                        logger.debug("-- noti cncl card  --");
                        
                        //이미 입력거래가 있는경우
                        StringBuffer sbTranChkSql = new StringBuffer();
                        sbTranChkSql.append(" select count(1) cnt ");
                        sbTranChkSql.append(" from VW_TRAN_CARDPG ");
                        sbTranChkSql.append(" where pg_seq = ? ");
                        sbTranChkSql.append(" and TID = ? ");
                        sbTranChkSql.append(" and massagetype='40' ");

                        pstmt = conn.prepareStatement(sbTranChkSql.toString());

                        pstmt.setString(1, r_cno);
                        pstmt.setString(2, r_memb_id);

                        rs = pstmt.executeQuery();

                        int int_CnclChk_cnt = 0;

                        if(rs != null && rs.next())
                        {
                            int_CnclChk_cnt = rs.getInt("cnt");
                        }

                        closeResultSet(rs);
                        closePstmt(pstmt);                    
                       logger.debug("-- tran cnt chkeck --");
                        String strNotResult = null;
                        //접수된 승인건이 없는 경우
                        if(int_CnclChk_cnt == 0)
                        {
                            logger.debug("-- tran cnt chkeck ok  --");
                            //원거래를 가져온다.
                            StringBuffer sb_orgtraninfoSql = new StringBuffer();

                            sb_orgtraninfoSql.append(" select  b.comp_nm, b.biz_no, b.corp_no, b.comp_ceo_nm, b.comp_tel1, b.tax_flag ,b.merch_nm,  b.app_chk_amt, b.addr_1, b.addr_2 ");
                            sb_orgtraninfoSql.append("         ,a.tran_seq,a.massagetype,a.card_num ");
                            sb_orgtraninfoSql.append("         ,a.app_dt ");
                            sb_orgtraninfoSql.append("         ,a.app_tm         ");
                            sb_orgtraninfoSql.append("         ,a.app_no,a.tot_amt,a.pg_seq,a.tid ");
                            sb_orgtraninfoSql.append("         ,a.pay_chn_cate,a.onfftid,a.onffmerch_no,a.wcc ");
                            sb_orgtraninfoSql.append("         ,a.expire_dt,a.card_cate,a.order_seq ");
                            sb_orgtraninfoSql.append("         ,a.tran_dt,a.tran_tm,a.tran_cate,a.free_inst_flag ");
                            sb_orgtraninfoSql.append("         ,a.tax_amt,a.svc_amt,a.currency ");
                            sb_orgtraninfoSql.append("         ,a.installment,a.iss_cd,a.iss_nm ");
                            sb_orgtraninfoSql.append("         ,a.app_iss_cd,a.app_iss_nm,a.acq_cd ");
                            sb_orgtraninfoSql.append("         ,a.acq_nm,a.app_acq_cd,a.app_acq_nm ");
                            sb_orgtraninfoSql.append("         ,a.merch_no,a.org_merch_no,a.result_cd,a.result_msg ");
                            sb_orgtraninfoSql.append("         ,a.tran_status,a.result_status ");
                            sb_orgtraninfoSql.append("         ,a.org_app_dd,a.org_app_no,a.cncl_reason ");
                            sb_orgtraninfoSql.append("         ,a.tran_step,a.cncl_dt,a.acq_dt,a.acq_result_cd ");
                            sb_orgtraninfoSql.append("         ,a.holdoff_dt,a.holdon_dt,a.pay_dt,a.pay_amt ");
                            sb_orgtraninfoSql.append("         ,a.commision,a.client_ip,a.user_type ");
                            sb_orgtraninfoSql.append("         ,a.memb_user_no,a.user_id,a.user_nm ");
                            sb_orgtraninfoSql.append("         ,a.user_mail,a.user_phone1,a.user_phone2 ");
                            sb_orgtraninfoSql.append("         ,a.user_addr,a.product_type,a.product_nm ");
                            sb_orgtraninfoSql.append("         ,a.filler,a.filler2,a.term_filler1,a.term_filler2 ");
                            sb_orgtraninfoSql.append("         ,a.term_filler3,a.term_filler4,a.term_filler5 ");
                            sb_orgtraninfoSql.append("         ,a.term_filler6,a.term_filler7,a.term_filler8 ");
                            sb_orgtraninfoSql.append("         ,a.term_filler9,a.term_filler10,a.trad_chk_flag ");
                            sb_orgtraninfoSql.append("         ,a.acc_chk_flag,a.ins_dt,a.mod_dt,a.ins_user,a.mod_user,a.onofftid_pay_amt,a.onofftid_commision,a.tb_nm, a.app_card_num	 ");
                            sb_orgtraninfoSql.append(" from VW_TRAN_CARDPG a ");
                            sb_orgtraninfoSql.append("          ,( ");
                            sb_orgtraninfoSql.append("               SELECT x1.comp_nm, x1.BIZ_NO, x1.CORP_NO, x1.COMP_CEO_NM, x1.COMP_TEL1, x1.addr_1, x1.addr_2, x1.TAX_FLAG , x.ONFFMERCH_NO, x.MERCH_NM,  x.APP_CHK_AMT ");
                            sb_orgtraninfoSql.append("                FROM  TB_ONFFMERCH_MST x, ");
                            sb_orgtraninfoSql.append("                          TB_COMPANY x1 ");
                            sb_orgtraninfoSql.append("                WHERE                  ");
                            sb_orgtraninfoSql.append("                          x.del_flag = 'N' ");
                            sb_orgtraninfoSql.append("                        AND x.SVC_STAT = '00' ");
                            sb_orgtraninfoSql.append("                        AND x1.del_flag = 'N' ");
                            sb_orgtraninfoSql.append("                        AND x1.use_flag = 'Y'                 ");
                            sb_orgtraninfoSql.append("                        AND  X.COMP_SEQ = x1.comp_seq ");
                            sb_orgtraninfoSql.append("          ) b ");
                            sb_orgtraninfoSql.append(" where ");
                            sb_orgtraninfoSql.append("      a.ONFFMERCH_NO = b.ONFFMERCH_NO ");
                            sb_orgtraninfoSql.append(" and       ");
                            sb_orgtraninfoSql.append("     a.pg_seq =? ");
                            sb_orgtraninfoSql.append(" and    a.massagetype='10' ");
                            sb_orgtraninfoSql.append(" and    a.result_status='00' ");

                            pstmt2 = conn.prepareStatement(sb_orgtraninfoSql.toString());
                            pstmt2.setString(1, r_cno);
                            rs2 = pstmt2.executeQuery();
                            
                            //원거래정보
                            //원거래정보가 있는 경우
                            if(rs2 != null && rs2.next())
                            {
                                logger.debug("-- org tran  exist  --");
                                logger.debug("-- strTpTranseq create  --");
                                //거래일련번호를 생성한다.
                                String strTpTranseq = null;
                                String sb_TpTranseqSql = "select concat(DATE_FORMAT(now(),'%Y%m%d%H%i%S'), LPAD(f_get_sequence('TRANSEQ'),8,'0'))";
                                pstmt = conn.prepareStatement(sb_TpTranseqSql);
                                rs = pstmt.executeQuery();
                                if(rs != null && rs.next())
                                {
                                    strTpTranseq = rs.getString(1);
                                }                

                                closeResultSet(rs);
                                closePstmt(pstmt);
                                logger.debug("strTpTranseq : "+strTpTranseq);
                                logger.debug("-- traninfo insert  --");
                                //취소정보를 insert한다.
                                StringBuffer sb_instraninfo = new StringBuffer();
                                sb_instraninfo.append(" insert ");
                                sb_instraninfo.append(" into TB_TRAN_CARDPG_"+r_canc_date.substring(2, 6) + " ( ");
                                sb_instraninfo.append(" TRAN_SEQ			 ");
                                sb_instraninfo.append(" ,MASSAGETYPE		 ");
                                sb_instraninfo.append(" ,CARD_NUM			 ");
                                sb_instraninfo.append(" ,APP_DT				 ");
                                sb_instraninfo.append(" ,APP_TM				 ");
                                sb_instraninfo.append(" ,APP_NO				 ");
                                sb_instraninfo.append(" ,TOT_AMT			 ");	
                                sb_instraninfo.append(" ,PG_SEQ				 ");
                                sb_instraninfo.append(" ,PAY_CHN_CATE		 ");	
                                sb_instraninfo.append(" ,TID				 ");	
                                sb_instraninfo.append(" ,ONFFTID			 ");	
                                sb_instraninfo.append(" ,WCC				 ");	
                                sb_instraninfo.append(" ,EXPIRE_DT			 ");
                                sb_instraninfo.append(" ,CARD_CATE			 ");
                                sb_instraninfo.append(" ,ORDER_SEQ			 ");
                                sb_instraninfo.append(" ,TRAN_DT			 ");
                                sb_instraninfo.append(" ,TRAN_TM			 ");	
                                sb_instraninfo.append(" ,TRAN_CATE			 ");
                                sb_instraninfo.append(" ,FREE_INST_FLAG		 ");
                                sb_instraninfo.append(" ,TAX_AMT			 ");	
                                sb_instraninfo.append(" ,SVC_AMT			 ");	
                                sb_instraninfo.append(" ,CURRENCY			 ");
                                sb_instraninfo.append(" ,INSTALLMENT		 ");	
                                sb_instraninfo.append(" ,ISS_CD				 ");
                                sb_instraninfo.append(" ,ISS_NM				 ");
                                sb_instraninfo.append(" ,APP_ISS_CD			 ");
                                sb_instraninfo.append(" ,APP_ISS_NM			 ");
                                sb_instraninfo.append(" ,ACQ_CD				 ");
                                sb_instraninfo.append(" ,ACQ_NM			 ");
                                sb_instraninfo.append(" ,APP_ACQ_CD			 ");
                                sb_instraninfo.append(" ,APP_ACQ_NM			 ");
                                sb_instraninfo.append(" ,MERCH_NO			 ");
                                sb_instraninfo.append(" ,ORG_MERCH_NO		 ");
                                sb_instraninfo.append(" ,RESULT_CD			 ");
                                sb_instraninfo.append(" ,RESULT_MSG			 ");
                                sb_instraninfo.append(" ,ORG_APP_DD			 ");
                                sb_instraninfo.append(" ,ORG_APP_NO			 ");
                                sb_instraninfo.append(" ,CNCL_REASON		 ");	
                                sb_instraninfo.append(" ,TRAN_STATUS		 ");	
                                sb_instraninfo.append(" ,RESULT_STATUS		 ");
                                sb_instraninfo.append(" ,TRAN_STEP			 ");
                                sb_instraninfo.append(" ,CNCL_DT			 ");	
                                sb_instraninfo.append(" ,ACQ_DT				 ");
                                sb_instraninfo.append(" ,ACQ_RESULT_CD		 ");
                                sb_instraninfo.append(" ,HOLDOFF_DT			 ");
                                sb_instraninfo.append(" ,HOLDON_DT			 ");
                                sb_instraninfo.append(" ,PAY_DT				 ");
                                sb_instraninfo.append(" ,PAY_AMT			 ");	
                                sb_instraninfo.append(" ,COMMISION			 ");
                                sb_instraninfo.append(" ,ONOFFTID_PAY_AMT	 ");	
                                sb_instraninfo.append(" ,ONOFFTID_COMMISION	 ");
                                sb_instraninfo.append(" ,CLIENT_IP			 ");
                                sb_instraninfo.append(" ,USER_TYPE			 ");
                                sb_instraninfo.append(" ,MEMB_USER_NO		 ");
                                sb_instraninfo.append(" ,USER_ID				 ");
                                sb_instraninfo.append(" ,USER_NM			 ");
                                sb_instraninfo.append(" ,USER_MAIL			 ");
                                sb_instraninfo.append(" ,USER_PHONE1		 ");
                                sb_instraninfo.append(" ,USER_PHONE2		 ");
                                sb_instraninfo.append(" ,USER_ADDR			 ");
                                sb_instraninfo.append(" ,PRODUCT_TYPE		 ");
                                sb_instraninfo.append(" ,PRODUCT_NM		 ");
                                sb_instraninfo.append(" ,FILLER				 ");
                                sb_instraninfo.append(" ,FILLER2				 ");
                                sb_instraninfo.append(" ,TERM_FILLER1		 ");
                                sb_instraninfo.append(" ,TERM_FILLER2		 ");	
                                sb_instraninfo.append(" ,TERM_FILLER3		 ");	
                                sb_instraninfo.append(" ,TERM_FILLER4		 ");	
                                sb_instraninfo.append(" ,TERM_FILLER5		 ");	
                                sb_instraninfo.append(" ,TERM_FILLER6		 ");	
                                sb_instraninfo.append(" ,TERM_FILLER7		 ");	
                                sb_instraninfo.append(" ,TERM_FILLER8		 ");	
                                sb_instraninfo.append(" ,TERM_FILLER9		 ");	
                                sb_instraninfo.append(" ,TERM_FILLER10		 ");
                                sb_instraninfo.append(" ,TRAD_CHK_FLAG		 ");
                                sb_instraninfo.append(" ,ACC_CHK_FLAG		 ");
                                sb_instraninfo.append(" ,INS_DT				 ");
                                sb_instraninfo.append(" ,MOD_DT				 ");
                                sb_instraninfo.append(" ,INS_USER			 ");	
                                sb_instraninfo.append(" ,MOD_USER			 ");
                                sb_instraninfo.append(" ,ORG_PG_SEQ			 ");
                                sb_instraninfo.append(" ,ONFFMERCH_NO	 ");
                                sb_instraninfo.append(" ,auth_pw ");
                                sb_instraninfo.append(" ,auth_value ");
                                sb_instraninfo.append(" ,app_card_num ");
                                sb_instraninfo.append(" )  ");
                                sb_instraninfo.append(" values ( ");
                                sb_instraninfo.append("  ? ");//tran_seq			
                                sb_instraninfo.append(" ,? ");//massagetype			
                                sb_instraninfo.append(" ,? ");//card_num			
                                sb_instraninfo.append(" ,? ");//app_dt			
                                sb_instraninfo.append(" ,? ");//app_tm			
                                sb_instraninfo.append(" ,? ");//app_no			
                                sb_instraninfo.append(" ,? ");//tot_amt			
                                sb_instraninfo.append(" ,? ");//pg_seq			
                                sb_instraninfo.append(" ,? ");//pay_chn_cate		
                                sb_instraninfo.append(" ,? ");//tid				
                                sb_instraninfo.append(" ,? ");//onfftid			
                                sb_instraninfo.append(" ,? ");//wcc				
                                sb_instraninfo.append(" ,? ");//expire_dt			
                                sb_instraninfo.append(" ,? ");//card_cate			
                                sb_instraninfo.append(" ,? ");//order_seq			
                                sb_instraninfo.append(" ,? ");//tran_dt			
                                sb_instraninfo.append(" ,? ");//tran_tm			
                                sb_instraninfo.append(" ,? ");//tran_cate			
                                sb_instraninfo.append(" ,? ");//free_inst_flag		
                                sb_instraninfo.append(" ,? ");//tax_amt			
                                sb_instraninfo.append(" ,? ");//svc_amt			
                                sb_instraninfo.append(" ,? ");//currency			
                                sb_instraninfo.append(" ,? ");//installment			
                                sb_instraninfo.append(" ,? ");//iss_cd			
                                sb_instraninfo.append(" ,? ");//iss_nm			
                                sb_instraninfo.append(" ,? ");//app_iss_cd			
                                sb_instraninfo.append(" ,? ");//app_iss_nm			
                                sb_instraninfo.append(" ,? ");//acq_cd			
                                sb_instraninfo.append(" ,? ");//acq_nm			
                                sb_instraninfo.append(" ,? ");//app_acq_cd			
                                sb_instraninfo.append(" ,? ");//app_acq_nm			
                                sb_instraninfo.append(" ,? ");//merch_no			
                                sb_instraninfo.append(" ,? ");//org_merch_no		
                                sb_instraninfo.append(" ,? ");//result_cd			
                                sb_instraninfo.append(" ,? ");//result_msg			
                                sb_instraninfo.append(" ,? ");//org_app_dd			
                                sb_instraninfo.append(" ,? ");//org_app_no			
                                sb_instraninfo.append(" ,? ");//cncl_reason			
                                sb_instraninfo.append(" ,? ");//tran_status			
                                sb_instraninfo.append(" ,? ");//result_status		
                                sb_instraninfo.append(" ,? ");//tran_step			
                                sb_instraninfo.append(" ,? ");//cncl_dt			
                                sb_instraninfo.append(" ,? ");//acq_dt			
                                sb_instraninfo.append(" ,? ");//acq_result_cd
                                sb_instraninfo.append(" ,? ");//holdoff_dt
                                sb_instraninfo.append(" ,? ");//holdon_dt
                                sb_instraninfo.append(" ,? ");//pay_dt
                                sb_instraninfo.append(" ,? ");//pay_amt
                                sb_instraninfo.append(" ,? ");//commision
                                sb_instraninfo.append(" ,? ");//onofftid_pay_amt
                                sb_instraninfo.append(" ,? ");//onofftid_commision
                                sb_instraninfo.append(" ,? ");//client_ip
                                sb_instraninfo.append(" ,? ");//user_type
                                sb_instraninfo.append(" ,? ");//memb_user_no
                                sb_instraninfo.append(" ,? ");//user_id
                                sb_instraninfo.append(" ,? ");//user_nm
                                sb_instraninfo.append(" ,? ");//user_mail
                                sb_instraninfo.append(" ,? ");//user_phone1
                                sb_instraninfo.append(" ,? ");//user_phone2
                                sb_instraninfo.append(" ,? ");//user_addr
                                sb_instraninfo.append(" ,? ");//product_type
                                sb_instraninfo.append(" ,? ");//product_nm
                                sb_instraninfo.append(" ,? ");//filler
                                sb_instraninfo.append(" ,? ");//filler2
                                sb_instraninfo.append(" ,? ");//term_filler1
                                sb_instraninfo.append(" ,? ");//term_filler2
                                sb_instraninfo.append(" ,? ");//term_filler3
                                sb_instraninfo.append(" ,? ");//term_filler4
                                sb_instraninfo.append(" ,? ");//term_filler5
                                sb_instraninfo.append(" ,? ");//term_filler6
                                sb_instraninfo.append(" ,? ");//term_filler7
                                sb_instraninfo.append(" ,? ");//term_filler8
                                sb_instraninfo.append(" ,? ");//term_filler9
                                sb_instraninfo.append(" ,? ");//term_filler10
                                sb_instraninfo.append(" ,? ");//trad_chk_flag
                                sb_instraninfo.append(" ,? ");//acc_chk_flag
                                sb_instraninfo.append("  , DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                                sb_instraninfo.append("  , DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                                sb_instraninfo.append(" ,? ");//ins_user			
                                sb_instraninfo.append(" ,? ");//mod_user			
                                sb_instraninfo.append(" ,? ");//org_pg_seq			
                                sb_instraninfo.append(" ,? ");//onffmerch_no		
                                sb_instraninfo.append(" ,? ");//auth_pw		  
                                sb_instraninfo.append(" ,? ");//auth_value		  
                                sb_instraninfo.append(" ,? ");//app_card_num	
                                sb_instraninfo.append(" ) ");          
                                
                                //System.out.println("sb_instraninfo.toString() : " + sb_instraninfo.toString());

                                pstmt = conn.prepareStatement(sb_instraninfo.toString());

                                //당일취소,익일취소 구분
                                String strTpOrgAppDt = rs2.getString("app_dt");
                                String strTpCnclAppDt = r_canc_date.substring(0, 8);

                                //당일취소
                                if(strTpCnclAppDt.equals(strTpOrgAppDt))
                                {                            
                                    pstmt.setString(4,r_canc_date.substring(0, 8));//app_dt				, jdbcType=VARCHAR}
                                    pstmt.setString(5,r_canc_date.substring(8, 14));//app_tm				 , jdbcType=VARCHAR}

                                    pstmt.setString(39,"10");//tran_status 당일취소 셋팅                            
                                    pstmt.setString(76,"3");//acc_chk_flag 매입비대상 셋팅
                                }
                                //익일취소
                                else
                                {
                                    pstmt.setString(4,r_canc_date.substring(0, 8));//app_dt				, jdbcType=VARCHAR}
                                    pstmt.setString(5,r_canc_date.substring(8, 14));//app_tm				 , jdbcType=VARCHAR}

                                    pstmt.setString(39,"20");//tran_status 익일취소(매입취소) 셋팅
                                    pstmt.setString(76,"0");//acc_chk_flag 매입비대상 셋팅
                                }
                                pstmt.setString(1,strTpTranseq);//tran_seq				, jdbcType=VARCHAR} 
                                pstmt.setString(2,"40");//massagetype			, jdbcType=VARCHAR} 
                                pstmt.setString(3,rs2.getString("card_num"));//card_num			, jdbcType=VARCHAR} 

                                pstmt.setString(6,null);//app_no				, jdbcType=VARCHAR}
                                pstmt.setString(7,rs2.getString("tot_amt"));//tot_amt				 , jdbcType=INTEGER }
                                pstmt.setString(8,r_cno);//pg_seq				, jdbcType=VARCHAR}
                                pstmt.setString(9,rs2.getString("pay_chn_cate"));//pay_chn_cate			 , jdbcType=VARCHAR}
                                pstmt.setString(10,r_memb_id);//tid					, jdbcType=VARCHAR}
                                pstmt.setString(11,rs2.getString("onfftid"));//onfftid				, jdbcType=VARCHAR}
                                pstmt.setString(12,rs2.getString("wcc"));//wcc					, jdbcType=VARCHAR}
                                pstmt.setString(13,rs2.getString("expire_dt"));//expire_dt			, jdbcType=VARCHAR}
                                pstmt.setString(14,rs2.getString("card_cate"));//card_cate			  , jdbcType=VARCHAR}
                                pstmt.setString(15,r_order_no);//order_seq			  , jdbcType=VARCHAR}
                                pstmt.setString(16,r_canc_date.substring(0, 8));//tran_dt				, jdbcType=VARCHAR}
                                pstmt.setString(17,r_canc_date.substring(8, 14));//tran_tm				, jdbcType=VARCHAR}
                                pstmt.setString(18,"40");//tran_cate			  , jdbcType=VARCHAR}
                                pstmt.setString(19,rs2.getString("free_inst_flag"));//free_inst_flag		    , jdbcType=VARCHAR}
                                pstmt.setDouble(20,rs2.getDouble("tax_amt"));//tax_amt				, jdbcType=INTEGER }
                                pstmt.setDouble(21,rs2.getDouble("svc_amt"));//svc_amt				, jdbcType=INTEGER }
                                pstmt.setString(22,rs2.getString("currency"));//currency			 , jdbcType=VARCHAR}
                                pstmt.setString(23,rs2.getString("installment"));//installment			   , jdbcType=VARCHAR}
                                /*
                                pstmt.setString(24,rs2.getString("iss_cd"));//iss_cd				, jdbcType=VARCHAR}
                                pstmt.setString(25,rs2.getString("iss_nm"));//iss_nm				 , jdbcType=VARCHAR}
                                pstmt.setString(26,rs2.getString("app_iss_cd"));//app_iss_cd			 , jdbcType=VARCHAR}
                                pstmt.setString(27,rs2.getString("app_iss_nm"));//app_iss_nm			 , jdbcType=VARCHAR}
                                pstmt.setString(28,rs2.getString("acq_cd"));//acq_cd				, jdbcType=VARCHAR}
                                pstmt.setString(29,rs2.getString("acq_nm"));//acq_nm				, jdbcType=VARCHAR}
                                pstmt.setString(30,rs2.getString("app_acq_cd"));//app_acq_cd			  , jdbcType=VARCHAR}
                                pstmt.setString(31,rs2.getString("app_acq_nm"));//app_acq_nm			   , jdbcType=VARCHAR}
                                */
                                pstmt.setString(24,null);//iss_cd				, jdbcType=VARCHAR}
                                pstmt.setString(25,null);//iss_nm				 , jdbcType=VARCHAR}
                                pstmt.setString(26,null);//app_iss_cd			 , jdbcType=VARCHAR}
                                pstmt.setString(27,null);//app_iss_nm			 , jdbcType=VARCHAR}
                                pstmt.setString(28,null);//acq_cd				, jdbcType=VARCHAR}
                                pstmt.setString(29,null);//acq_nm				, jdbcType=VARCHAR}
                                pstmt.setString(30,null);//app_acq_cd			  , jdbcType=VARCHAR}
                                pstmt.setString(31,null);//app_acq_nm			   , jdbcType=VARCHAR}
                                
                                pstmt.setString(32,rs2.getString("merch_no"));//merch_no			  , jdbcType=VARCHAR}
                                pstmt.setString(33,rs2.getString("org_merch_no"));//org_merch_no			  , jdbcType=VARCHAR}
                                pstmt.setString(34,r_res_cd);//result_cd			 , jdbcType=VARCHAR}
                                pstmt.setString(35,r_res_msg);//result_msg			   , jdbcType=VARCHAR}
                                pstmt.setString(36,rs2.getString("app_dt"));//org_app_dd			  , jdbcType=VARCHAR}
                                pstmt.setString(37,rs2.getString("app_no"));//org_app_no			   , jdbcType=VARCHAR}
                                pstmt.setString(38,"noti취소통지");//cncl_reason			, jdbcType=VARCHAR}
                                pstmt.setString(40,"00");//result_status		     , jdbcType=VARCHAR}
                                pstmt.setString(41,"00");//tran_step			 , jdbcType=VARCHAR}
                                pstmt.setString(42,r_canc_date.substring(0, 8));//cncl_dt				, jdbcType=VARCHAR}
                                pstmt.setString(43,null);//acq_dt				, jdbcType=VARCHAR}
                                pstmt.setString(44,null);//acq_result_cd		     , jdbcType=VARCHAR}
                                pstmt.setString(45,null);//holdoff_dt			  , jdbcType=VARCHAR}
                                pstmt.setString(46,null);//holdon_dt			  , jdbcType=VARCHAR}
                                pstmt.setString(47,null);//pay_dt				, jdbcType=VARCHAR}
                                pstmt.setString(48,null);//pay_amt				, jdbcType=INTEGER }
                                pstmt.setString(49,null);//commision			  , jdbcType=INTEGER }
                                pstmt.setString(50,rs2.getString("onofftid_pay_amt"));//onofftid_pay_amt		, jdbcType=INTEGER }
                                pstmt.setString(51,rs2.getString("onofftid_commision"));//onofftid_commision		, jdbcType=INTEGER }
                                pstmt.setString(52,null);//client_ip				, jdbcType=VARCHAR}
                                pstmt.setString(53,rs2.getString("user_type"));//user_type			, jdbcType=VARCHAR}
                                pstmt.setString(54,rs2.getString("memb_user_no"));//memb_user_no		, jdbcType=VARCHAR}
                                pstmt.setString(55,rs2.getString("user_id"));//user_id				, jdbcType=VARCHAR}
                                pstmt.setString(56,rs2.getString("user_nm"));//user_nm				, jdbcType=VARCHAR}
                                pstmt.setString(57,rs2.getString("user_mail"));//user_mail			    , jdbcType=VARCHAR}
                                pstmt.setString(58,rs2.getString("user_phone1"));//user_phone1			 , jdbcType=VARCHAR}
                                pstmt.setString(59,rs2.getString("user_phone2"));//user_phone2			 , jdbcType=VARCHAR}
                                pstmt.setString(60,rs2.getString("user_addr"));//user_addr			, jdbcType=VARCHAR}
                                pstmt.setString(61,rs2.getString("product_type"));//product_type		  , jdbcType=VARCHAR}
                                pstmt.setString(62,rs2.getString("product_nm"));//product_nm			 , jdbcType=VARCHAR}
                                pstmt.setString(63,null);//filler				   , jdbcType=VARCHAR}
                                pstmt.setString(64,null);//filler2				   , jdbcType=VARCHAR}
                                pstmt.setString(65,r_reserve1);//term_filler1			, jdbcType=VARCHAR}
                                pstmt.setString(66,r_reserve2);//term_filler2			, jdbcType=VARCHAR}
                                pstmt.setString(67,r_reserve3);//term_filler3			, jdbcType=VARCHAR}
                                pstmt.setString(68,r_reserve4);//term_filler4			, jdbcType=VARCHAR}
                                pstmt.setString(69,r_reserve5);//term_filler5			, jdbcType=VARCHAR}
                                pstmt.setString(70,r_reserve6);//term_filler6				, jdbcType=VARCHAR}
                                pstmt.setString(71,null);//term_filler7			  , jdbcType=VARCHAR}
                                pstmt.setString(72,null);//term_filler8			, jdbcType=VARCHAR}
                                pstmt.setString(73,null);//term_filler9			, jdbcType=VARCHAR}
                                pstmt.setString(74,null);//term_filler10			, jdbcType=VARCHAR}
                                pstmt.setString(75,"0");//trad_chk_flag		  , jdbcType=VARCHAR}
                                pstmt.setString(77,"0");//ins_user				     , jdbcType=VARCHAR}
                                pstmt.setString(78,"0");//mod_user			, jdbcType=VARCHAR}
                                pstmt.setString(79,rs2.getString("pg_seq"));//org_pg_seq			, jdbcType=VARCHAR}
                                pstmt.setString(80,rs2.getString("onffmerch_no"));//onffmerch_no		  , jdbcType=VARCHAR}
                                pstmt.setString(81,null);//auth_pw		  , jdbcType=VARCHAR}
                                pstmt.setString(82,null);//auth_value		  , jdbcType=VARCHAR}   
                                pstmt.setString(83,rs2.getString("app_card_num"));//app_card_num		  , jdbcType=VARCHAR}                
                                int int_insResult = pstmt.executeUpdate();                               

                                closeResultSet(rs);
                                closePstmt(pstmt);
                                logger.debug("--org traninfo update  --");
                                //원거래를 update한다.
                                StringBuffer sb_orgtran_mod_sql = new StringBuffer();
                                sb_orgtran_mod_sql.append(" update " + rs2.getString("tb_nm") + " ");
                                sb_orgtran_mod_sql.append(" set      cncl_reason = ? ");
                                sb_orgtran_mod_sql.append("              ,tran_status= ? ");
                                sb_orgtran_mod_sql.append("              ,cncl_dt = ? ");
                                sb_orgtran_mod_sql.append("              ,acc_chk_flag = ? ");
                                sb_orgtran_mod_sql.append("               ,mod_dt = DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                                sb_orgtran_mod_sql.append("               ,mod_user= '0' ");
                                sb_orgtran_mod_sql.append("          where 1=1 ");
                                sb_orgtran_mod_sql.append("            and tran_seq = ? ");

                                pstmt = conn.prepareStatement(sb_orgtran_mod_sql.toString());

                                pstmt.setString(1, "noti취소통지");

                                //당일취소
                                if(strTpCnclAppDt.equals(strTpOrgAppDt))
                                {
                                   pstmt.setString(2, "10");//당일취소
                                   pstmt.setString(4, "3");//매입비대상   
                                }
                                else
                                {
                                    pstmt.setString(2, "20");//매입취소
                                    pstmt.setString(4, rs2.getString("acc_chk_flag"));
                                }                        
                                pstmt.setString(3, r_canc_date.substring(0, 8));                        
                                pstmt.setString(5, rs2.getString("tran_seq"));

                                int int_mod_orginfo = pstmt.executeUpdate();  

                                closeResultSet(rs);
                                closePstmt(pstmt);
                                
                                strNotResult = "10";

                            } 
                            //원거래정보가 없는 경우
                            else
                            {
                                logger.debug("--org traninfo not exist  --");
                                //원거래가 없어 insert안함.
                                strNotResult = "30";
                            }
                        }
                        //이미 취소거래가 있는경우
                        else
                        {
                            logger.debug("-- cncl traninfo exist  --");
                            strNotResult = "20";
                        }
                        //noti처리상태 update
                        StringBuffer sb_noti_mod_sql = new StringBuffer();
                        sb_noti_mod_sql.append(" UPDATE TB_KICC_NOTI SET ");
                        sb_noti_mod_sql.append(" proc_flag=? ");
                        sb_noti_mod_sql.append(" ,mod_dt = DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                        sb_noti_mod_sql.append(" ,mod_user= '0' ");                
                        sb_noti_mod_sql.append(" WHERE noti_seq = ? ");
                        pstmt = conn.prepareStatement(sb_noti_mod_sql.toString());
                        pstmt.setString(1, strNotResult);
                        pstmt.setString(2, strTpNotiseq);
                        int int_mod_notiinfo = pstmt.executeUpdate();
                    }
                }
            }
            result_msg = "res_cd=" + RESULT_SUCCESS + DELI_US + "res_msg=SUCCESS";
            conn.commit();

          } catch ( SQLException e ) {
            //e.printStackTrace();
            conn.rollback();
            logger.error(e);
            
            result_msg = "res_cd=" + RESULT_FAIL + DELI_US + "res_msg=FAIL";

          } catch ( Exception e ) {
            //e.printStackTrace();
            conn.rollback();
            logger.error(e);
            result_msg = "res_cd=" + RESULT_FAIL + DELI_US + "res_msg=FAIL";
          } 
          finally 
          {
              closeResultSet(rs);
              closeResultSet(rs2);
              closePstmt(pstmt);
              closePstmt(pstmt2);
              closeConnection(conn);
          }
    }
    

    //if ( r_res_cd.equals("0000") )
    //{
	    /* ---------------------------------------------------------------------- */
	    /* ::: 가맹점 DB 처리                                                     */
	    /* ---------------------------------------------------------------------- */
	    /* DB처리 성공 시 : res_cd=0000, 실패 시 : res_cd=5001                    */
	    /* ---------------------------------------------------------------------- */
		//if DB처리 성공 시
		//{
		//	result_msg = "res_cd=" + RESULT_SUCCESS + DELI_US + "res_msg=SUCCESS";
		//}	
		//else
		//{
    	//	result_msg = "res_cd=" + RESULT_FAIL + DELI_US + "res_msg=FAIL";
		//}
    //}
  
    /* -------------------------------------------------------------------------- */
    /* ::: 노티 처리결과 처리                                                     */
    /* -------------------------------------------------------------------------- */
    out.println( result_msg );
           
%>