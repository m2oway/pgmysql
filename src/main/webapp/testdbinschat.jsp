<%@ page language="java" contentType="text/html; charset=euc-kr" %>
<%@ page import = "java.sql.*" %> 
<%
    Connection con = null;
    ResultSet rs = null;
    Statement  stmt = null;
    
    PreparedStatement pstmt = null;
    
    try
    {
        String strTestNm = request.getParameter( "testmsg"         );
        
        Class.forName("com.mysql.jdbc.Driver"); 
        
//        con = DriverManager.getConnection("jdbc:mysql://10.100.50.32:3306/okpay","okpay","okpay!");         
//        con = DriverManager.getConnection("jdbc:mysql://10.100.50.54:3306/okpay","okpay","okpay!");
        con = DriverManager.getConnection("jdbc:mysql://192.168.0.241:3306/okpay","okpay","okpay!");
        
        
        String strInsTest = "insert into TB_USER_BAK (USER_SEQ, USER_NM) values(?,?)";
        
        pstmt = con.prepareStatement(strInsTest);
        pstmt.setString(1, "1");
        pstmt.setString(2, strTestNm);
        
        pstmt.executeUpdate();
        
        con.commit();
        
    }
    catch(Exception ex)
    {
        out.println(ex.getMessage());
        con.rollback();
    }
    finally
    {
        if (rs!=null) { 
            try {
               rs.close();
            } catch (SQLException e) {
            }
         }

         if(stmt!=null) {
            try {
               stmt.close();
            } catch (SQLException e) {
            }
         }
         
         if(pstmt!=null) {
            try {
               pstmt.close();
            } catch (SQLException e) {
            }
         }         

         if(con!=null) {
            try {
               con.close();
            } catch (SQLException e) {
            }
         }
    }
   
           
%>