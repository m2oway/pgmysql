<%-- 
    Document   : test.jsp
    Created on : 2015. 1. 7, 오후 4:53:48
    Author     : Administrator
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<meta http-equiv="X-UA-Compatible" content="IE=10"/>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>okpay</title>
        <style>
            table.logintable {
                font-family: verdana,arial,sans-serif;
                font-size:11px;
                color:#FFF;
                background-color:#666;

                border: solid 1px;
                border-color: #CCC;
                border-collapse: collapse;
            }	
            table.logintable td
            {
                border: solid 1px;
                border-color: #CCC;
            }

            table.titletable {
                font-family: verdana,arial,sans-serif;
                font-size:13px;
                color:#ff2005;
            }							

            table.gridtable {
                font-family: verdana,arial,sans-serif;
                font-size:13px;
                color:#333333;
                border: solid 1px;
                border-color: #CCC;
                border-collapse: collapse;
            }	

            table.gridtable td {
                border-left:solid 1px;
                border-left-color: #CCC;
                padding: 8px;
                background-color: #ffffff;
            }	

            table.gridtable td.headcol {
                border-width: 1px;
                padding: 8px;
                border-style: solid;
                border-color: #CCC;
                text-align:center;

            }		

            table.bbs {
                font-family: verdana,arial,sans-serif;
                font-size:11px;
                color:#333333;
                border-top:solid 1px;
                border-top-color: #CCC;
                border-collapse: collapse;
            }	

            table.maintable
            {
                margin:0;
                padding:0;

            }

            table.maintable td.leftcol
            {
                /*
                padding-left:30px;
                */
                padding-left:0px;
            }

            #divpop{position:absolute;left:10px;top:10px;width:430px;height:538px;z-index:10;background-color:#eee;}        
        </style>
    </head>
    <body style="margin:0px;padding:0px;" >
                                                                <table width="100%" class="titletable">
                                    <tr>
                                        <td valign="bottom" align="left"><img src="resources/images/muiza_tile.jpg" width="240" height="32"></td>
                                        <td  valign="bottom" align="right">[4/01~4/30일까지 무이자 안내]</td>
                                    </tr>
                                </table>                        
                                <table width="100%" class="gridtable">
                                    <tr>
                                        <td class="headcol">무이자카드</td>
                                        <td class="headcol">슬림할부개월</td>
                                        <td class="headcol">이벤트 비고 참조 공지</td>
                                    </tr>
                                    <tr>
                                        <td style="line-height:1.5;">삼 성(2~5개월)<br>국 민(2~5개월)<br>하 나(2~5개월)<br>신 한(2~5개월)<br>롯 데(2~5개월)<br>현 대(2~3개월)<br>비 씨(2~5개월)<br>농 협(2~5개월)<br><br><br></td>
                                        <td style="line-height:1.5;">삼 성 6,10,12개월<br>롯 데 6,10 개월<br>하 나 6,10 개월<br>국 민 6,10 개월<br><br><br><br><br><br><br></td>
                                        <td style="line-height:1.5;vertical-align:top;​">06개월 - 1회차 고객부담<br>10개월  - 1,2 회차 고객부담<br>12개월  - 1,2,3 회차 고객부담<br><br><font style="color:#ff2005;">* 법인,체크,선불,기프트카드는 제외<br>* 하나카드(하나SK카드 및 구외환카드 포함)<br>*  5만원이상 결제시 적용<br>* 대학등록금 공공기관 및 세외수입 제외</font></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td> 
                                        <td>&nbsp;</td>
                                        <td></td>
                                    </tr>
                                </table>

    </body>
</html>