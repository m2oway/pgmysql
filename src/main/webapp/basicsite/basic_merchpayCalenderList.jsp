<%-- 
    Document   : test.jsp
    Created on : 2015. 1. 7, 오후 4:53:48
    Author     : Administrator
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
            <jsp:include page="/WEB-INF/view/common/common.jsp"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>okpay</title>
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/onoff.css" />"  />        
        <script type="text/javascript" src="<c:url value="/resources/javascript/jquery-1.6.2.min.js" />"></script>
        <script type="text/javascript" src="<c:url value="/resources/javascript/jquery-ui-1.8.17.custom.min.js" />"></script>
        <script type="text/javascript" src="<c:url value="/resources/javascript/jqueryform/2.8/jquery.form.js" />"></script>
        <style>
            #paycalyyyymmtitle{left:10px;top:10px;width:200px;height:30px;font-size: 20px;background-color:#ffffff;}
        </style> 
            <style>
                html, body {
                    height: 100%;
                }                
                .logout_btn {
                            -webkit-border-radius: 28;
                            -moz-border-radius: 28;
                            border-radius: 28px;
                            font-family: Arial;
                            color: #ffffff;
                            font-size: 12px;
                            background: #add5f0;
                            padding: 10px 20px 10px 20px;
                            text-decoration: none;
                      }

                      .logout_btn:hover {
                            background: #3cb0fd;
                            background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
                            background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
                            background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
                            background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
                            background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
                            text-decoration: none;
                      }                    
                   .manualbt {
	-moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
	box-shadow:inset 0px 1px 0px 0px #ffffff;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #dfdfdf) );
	background:-moz-linear-gradient( center top, #ededed 5%, #dfdfdf 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#dfdfdf');
	background-color:#ededed;
	-webkit-border-top-left-radius:6px;
	-moz-border-radius-topleft:6px;
	border-top-left-radius:6px;
	-webkit-border-top-right-radius:6px;
	-moz-border-radius-topright:6px;
	border-top-right-radius:6px;
	-webkit-border-bottom-right-radius:6px;
	-moz-border-radius-bottomright:6px;
	border-bottom-right-radius:6px;
	-webkit-border-bottom-left-radius:6px;
	-moz-border-radius-bottomleft:6px;
	border-bottom-left-radius:6px;
	text-indent:0;
	border:1px solid #dcdcdc;
	display:inline-block;
	color:#777777;
	font-family:arial;
	font-size:15px;
	font-weight:bold;
	font-style:normal;
	height:45px;
	line-height:45px;
	width:64px;
	text-decoration:none;
	text-align:center;
	text-shadow:1px 1px 0px #ffffff;
}
.manualbt:hover {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #dfdfdf), color-stop(1, #ededed) );
	background:-moz-linear-gradient( center top, #dfdfdf 5%, #ededed 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#dfdfdf', endColorstr='#ededed');
	background-color:#dfdfdf;
}.manualbt:active {
	position:relative;
	top:1px;
}                    
            </style>        
<body >
<div class="loginInfo">
    <div class="logo">
            <img src="../resources/images/okpay.png" width="146" height="65"/>
    </div>
    <div style="padding:10px 10px 10px 10px;vertical-align:middle;height: 60px;text-align:right;vertical-align: middle;"><button id='logoutbtn' class="logout_btn">logout</button></div>
</div>
<!--메뉴 -->
<div class="settlementPaymentMenu">
    <div id="menuObj"></div>
</div>   
<div id="contentsmain" style="width:100%;height:82%;min-height:400px;" >
<form name="paycalcurform" id="paycalcurform" action="<c:url value="/paycalender/merchpayCalenderList" />">
    <input type="hidden" name="paycalyyyymm" id="paycalyyyymm">
</form>
<table border="1" bodercolor='blue' cellpadding="0" cellspacing="0" width="100%" height=100%">
<tr>
    <td align="center" height="10%" >
        <table border="0" bodercolor='blue' cellpadding="0" cellspacing="0" width="100%" height=100%">
            <tr>
                <td align="right"><font size='5'><a href="#" Onclick="javascirpt:moveMonth('-');">◁</a></font></td>
                <td id='paycalyyyymmtitle' align="center">년월</td>
                <td align="left"><font size='5'><a href="#" Onclick="javascirpt:moveMonth('+');">▷</a></font></td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td>
        <table border="1" width="100%" height=100%">
            <thead> 
                <tr> 
                    <th height="7%" align="center"><font size='5'>일</font></td>
                    <th align="center"><font size='5'>월</font></td>
                    <th align="center"><font size='5'>화</font></td>
                    <th align="center"><font size='5'>수</font></td>
                    <th align="center"><font size='5'>목</font></td>
                    <th align="center"><font size='5'>금</font></td>
                    <th align="center"><font size='5'>토</font></td>
                </tr>
            </thead>
            <tbody id="paycaltbody" name="paycaltbody">
                
            </tbody>
        </table>

    </td>
</tr>
</table>
</div>
<div class="fotter"><!--<img src="../resources/images/onoffbottom2.jpg" width="1200" height="77"/>--></div>
</body>
</html>
            <script type="text/javascript">

                var tabbar;
                var menu;

                $(document).ready(function(){

                    var height=0;
                    var minusHight =(Number($(".settlementPaymentMenu").css("height").replace(/px/g,"")) + Number($(".loginInfo").css("height").replace(/px/g,""))+ Number($(".fotter").css("height").replace(/px/g,"")));


                    menu = new dhtmlXMenuObject("menuObj");
                    menu.setIconsPath("<c:url value="/resources/images/dhx/dhtmlxMenu/" />");
                    
                    var user_cate = '${siteSessionObj.user_cate}';
                    
                    //정산관리자, 결제관리자
                   if(user_cate === '01' || user_cate === '02'){
                        menu.loadXML("<c:url value="/resources/menu/dhx_menu_links_basicterm.xml" />");
                        
                    }
                    
                    $("#logoutbtn").click(function(){
                        location.href = "${pageContext.request.contextPath}/login/logout";
                    });
                    

                });

            </script>
<script type="text/javascript">
            function numberWithCommas(x) {
                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }
            
            
            function moveMerchPay(mvtargetdt)
            {
                tabbar_add('7-1','가맹점 지급관리','/merchpay/merchMerchpayMasterList?exp_pay_start_dt='+mvtargetdt+'&exp_pay_end_dt='+mvtargetdt,true);
            }
            
            function moveMonth(movemonth)
            {   
                var strMovTargetMonth = $("#paycalyyyymm").val();
                
                var tpx_yyyy = strMovTargetMonth.slice(0,4);
                var tpx_mm = strMovTargetMonth.slice(4,6);
                
                var TpTargetday_cur = new Date(tpx_yyyy+"-"+tpx_mm+"-"+"01");
                var TpTargetday_first = new Date(tpx_yyyy+"-"+tpx_mm+"-"+"01");
                var TpTargetday_last = new Date(TpTargetday_cur.getFullYear(),TpTargetday_cur.getMonth()+1,""); 
                
                //익월
                if(movemonth=="+")
                {
                    //날자계산
                    var tplastDay = new Date ( TpTargetday_last.setDate( TpTargetday_last.getDate() + 1 ) );       
                    var tplastDayyyy = tplastDay.getFullYear();
                    var tplastDamonth = tplastDay.getMonth()+1;
                    if(tplastDamonth < 10)
                    {
                        tplastDamonth = "0"+tplastDamonth; 
                    }
                    $("#paycalyyyymm").val(tplastDayyyy+""+tplastDamonth);
                }
                //전월
                else
                {
                    var tppreMonth = new Date ( TpTargetday_first.setDate( TpTargetday_first.getDate() - 1 ) );
                    var preMonthyyyy = tppreMonth.getFullYear();
                    var preMonthmonth = tppreMonth.getMonth()+1;
                    if(preMonthmonth < 10)
                    {
                        preMonthmonth = "0"+preMonthmonth; 
                    }
                    $("#paycalyyyymm").val(preMonthyyyy+""+preMonthmonth);                    
                }  
                
                $.ajax({
                    url: $("#paycalcurform").attr("action"),
                    type: "POST",
                    async: true,
                    dataType: "json",
                    data: $("#paycalcurform").serialize(),
                    success: function (data) {
                        createCal(data);
                    },
                    error: function () {
                        alert("조회 실패");
                    }
                });
            }
            
            
            function createCal(jasondata)
            {
               var tp_jsonobj = JSON.parse(jasondata);
               var tprowslist = tp_jsonobj.rows;
               var tprowslistcnt = tprowslist.length
               var tbody = document.getElementById("paycaltbody");
               while(tbody.rows.length > 0){
                      tbody.deleteRow(0);
               }
               
               var week = new Array('일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일');
               var calroopcnt = 0;
               var tpCaltablehtml = "";
               
               $.each(tprowslist, function(key)
               { 
                    var tpx_yyyymmdd="";
                    tpx_yyyymmdd = tprowslist[key].data[0]; 
                    var tpx_yyyy = tpx_yyyymmdd.slice(0,4);
                    var tpx_mm = tpx_yyyymmdd.slice(4,6);
                    var tpx_dd = tpx_yyyymmdd.slice(6,8);
       
                    var TpTargetday = new Date(tpx_yyyy+"-"+tpx_mm+"-"+tpx_dd).getDay();
                    var TpWeekDayCnt =  calroopcnt%7;
                    
                    if(calroopcnt==0 && TpTargetday!=0)
                    {
                         $("#paycalyyyymmtitle").text(tpx_yyyy+"년 " + tpx_mm + "월");
                         $("#paycalyyyymm").val(tpx_yyyy+""+tpx_mm);
                         
                        tpCaltablehtml+= "<tr>";
                        
                        for(var tpblkloopcnt=0 ; tpblkloopcnt < TpTargetday; tpblkloopcnt++)
                        {
                            tpCaltablehtml += "<td>&nbsp;</td>";
                            TpWeekDayCnt++;
                        }                        
                    }
                    else if(calroopcnt==0 && TpTargetday ==0)
                    {
                         $("#paycalyyyymmtitle").text(tpx_yyyy+"년 " + tpx_mm + "월");
                         $("#paycalyyyymm").val(tpx_yyyy+""+tpx_mm);
                         
                        tpCaltablehtml+= "<tr>";
                    }
                    
                    tpCaltablehtml += "<td valign='top' align='left' width='14%'>";
                    tpCaltablehtml += "<font size='5'>" + tpx_dd + "</font><br>";
                    tpCaltablehtml += "<font size='3'><a href='#' Onclick=\"javascirpt:moveMerchPay('"+tpx_yyyymmdd+"');\">입금예정:"+numberWithCommas(tprowslist[key].data[1])+"</a></font><br>";
                    tpCaltablehtml += "<font size='3'><a href='#' Onclick=\"javascirpt:moveMerchPay('"+tpx_yyyymmdd+"');\">입금액:"+numberWithCommas(tprowslist[key].data[2])+"</a></font>";
                    tpCaltablehtml += "</td>";
                    
                    TpWeekDayCnt++;
                    
                    if( calroopcnt==(tprowslistcnt-1))
                    {
                        for(var tpblkloopcnt2=TpTargetday ; tpblkloopcnt2 < 6; tpblkloopcnt2++)
                        {
                            tpCaltablehtml += "<td>&nbsp;</td>";
                        }
                    }
                    
                    //alert(TpWeekDayCnt);
                    ///if( (TpWeekDayCnt==0) ||   (TpWeekDayCnt==7))
                    if(TpTargetday==6)
                    {
                        tpCaltablehtml += "</tr>";
                    }
                    
                    
                    //alert(tpx_yyyymmdd);
                    
                    calroopcnt++;
               });
              
               $("#paycaltbody").append(tpCaltablehtml);
            }
            
            createCal('${payCalenderList}');
        </script>