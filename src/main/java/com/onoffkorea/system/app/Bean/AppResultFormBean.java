/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.app.Bean;

import com.onoffkorea.system.common.util.CommonSortableListPagingForm;

/**
 *
 * @author Administrator
 */
public class AppResultFormBean extends CommonSortableListPagingForm{

    /* -------------------------------------------------------------------------- */
    /* ::: 처리구분 설정                                                          */
    /* -------------------------------------------------------------------------- */
    private String  TRAN_CD_NOR_PAYMENT;
    private String  TRAN_CD_NOR_MGR;    

    private String  tr_cd;             // [필수]요청구분
    private String  pay_type;          // [필수]결제수단

    /* -------------------------------------------------------------------------- */
    /* ::: 신용카드 정보 설정                                                     */
    /* -------------------------------------------------------------------------- */
    private String  card_amt;           // [필수]신용카드 결제금액
    private String  card_txtype;        // [필수]처리종류
    private String  req_type;           // [필수]카드결제종류
    private String  cert_type;          // [필수]인증여부
    private String  wcc;                // [필수]wcc
    private String  card_no;            // [필수]카드번호
    private String  expire_date;        // [필수]유효기간
    private String  install_period;     // [필수]할부개월
    private String  noint;              // [필수]무이자여부
    private String  card_user_type;     // [선택]카드구분 : 인증여부에 따라 필수
    private String  password;           // [선택]비밀번호 : 인증여부에 따라 필수
    private String  auth_value;         // [선택]인증값   : 인증여부에 따라 필수
    private String  cavv;               // [선택]CAVV  : 카드결제종류에 따라 필수
    private String  xid;                // [선택]XID   : 카드결제종류에 따라 필수
    private String  eci;                // [선택]ECI   : 카드결제종류에 따라 필수
    private String  kvp_cardcode;       // [선택]KVP_CARDCODE   : 카드결제종류에 따라 필수
    private String  kvp_sessionkey;     // [선택]KVP_SESSIONKEY : 카드결제종류에 따라 필수
    private String  kvp_encdata;        // [선택]KVP_ENCDATA    : 카드결제종류에 따라 필수
    private String  req_cno;            // [선택]은련카드 인증거래번호
    private String  kvp_pgid;           // [선택]은련카드 PGID

    private String  kvp_sessionkey_enc;
    private String  kvp_encdata_enc;

    /* -------------------------------------------------------------------------- */
    /* ::: 계좌이체 정보 설정                                                     */
    /* -------------------------------------------------------------------------- */
    private String  acnt_amt;          // [필수]계좌이체 결제금액
    private String  acnt_txtype;       // [필수]계좌이체 처리종류
    private String  kftc_signdata;     // [필수]전자지갑 암호문

    /* -------------------------------------------------------------------------- */
    /* ::: 무통장입금 정보 설정                                                   */
    /* -------------------------------------------------------------------------- */
    private String  vacct_amt;          // [필수]무통장입금 결제금액
    private String  vacct_txtype;       // [필수]무통장입금 처리종류
    private String  bank_cd;            // [필수]은행코드
    private String  vacct_expr_date;    // [필수]입금만료일
    private String  vacct_expr_time;    // [필수]입금만료시간

    /* -------------------------------------------------------------------------- */
    /* ::: 현금영수증 정보 설정                                                   */
    /* -------------------------------------------------------------------------- */
    private String  cash_yn;          // [필수]현금영수증 발행여부
    private String  cash_issue_type;  // [선택]현금영수증발행용도
    private String  cash_auth_type;   // [선택]인증구분
    private String  cash_auth_value;  // [선택]인증번호

    /* -------------------------------------------------------------------------- */
    /* ::: 휴대폰결제 정보 설정                                                   */
    /* -------------------------------------------------------------------------- */
    private String  mob_amt;        // [필수]휴대폰 결제금액
    private String  mob_txtype;     // [필수]휴대폰 처리종류
    private String  mobile_cd;      // [필수]이통사코드
    private String  mobile_no;      // [필수]휴대폰번호
    private String  jumin_no;       // [필수]휴대폰 가입자 주민등록번호
    private String  mob_auth_id;    // [필수]인증고유번호(승인 시 필수)
    private String  mob_billid;     // [필수]인증번호(승인 시 필수)
    private String  mob_cno;        // [필수]PG거래번호(승인 시 필수)

    /* -------------------------------------------------------------------------- */
    /* ::: 포인트 결제 정보 설정                                                  */
    /* -------------------------------------------------------------------------- */
    private String  pnt_amt;      // [필수]포인트 결제금액
    private String  pnt_txtype;   // [필수]포인트 처리종류
    private String  pnt_cp_cd;    // [필수]포인트 서비스사 코드
    private String  pnt_idno;     // [필수]포인트 ID(카드)번호
    private String  pnt_pwd;      // [옵션]포인트 비밀번호

    /* -------------------------------------------------------------------------- */
    /* ::: 결제 주문 정보 설정                                                    */
    /* -------------------------------------------------------------------------- */
    private String  user_type;       // [선택]사용자구분구분[1:일반,2:회원]
    private String  order_no;        // [필수]주문번호
    private String  memb_user_no;    // [선택]가맹점 고객일련번호
    private String  user_id;         // [선택]고객 ID
    private String  user_nm;         // [필수]고객명
    private String  user_mail;       // [필수]고객 E-mail
    private String  user_phone1;     // [필수]가맹점 고객 연락처1
    private String  user_phone2;     // [선택]가맹점 고객 연락처2
    private String  user_addr;       // [선택]가맹점 고객 주소
    private String  product_type;    // [필수]상품정보구분[0:실물,1:컨텐츠]
    private String  product_nm;      // [필수]상품명
    private String  product_amt;     // [필수]상품금액
    private String  user_define1;    // [선택]가맹점필드1
    private String  user_define2;    // [선택]가맹점필드2
    private String  user_define3;    // [선택]가맹점필드3
    private String  user_define4;    // [선택]가맹점필드4
    private String  user_define5;    // [선택]가맹점필드5
    private String  user_define6;    // [선택]가맹점필드6

    /* -------------------------------------------------------------------------- */
    /* ::: 기타정보 설정                                                          */
    /* -------------------------------------------------------------------------- */
    private String  client_ip;       // [필수]결제고객 IP
    private String  tot_amt;         // [필수]결제 총 금액
    private String  currency;        // [필수]통화코드
    private String  escrow_yn;       // [필수]에스크로여부
    private String  complex_yn;      // [필수]복합결제여부
    private String  tax_flg;         // [필수]과세구분 플래그(TG01:복합과세 승인거래)
    private String  com_tax_amt;     // [필수]과세 승인 금액(복합과세 거래 시 필수)
    private String  com_free_amt;    // [필수]비과세 승인 금액(복합과세 거래 시 필수)
    private String  com_vat_amt;     // [필수]부가세 금액(복합과세 거래 시 필수)


    /* -------------------------------------------------------------------------- */
    /* ::: 변경관리 정보 설정                                                     */
    /* -------------------------------------------------------------------------- */
    private String  mgr_txtype;       // [필수]거래구분
    private String  mgr_subtype;      // [선택]변경세부구분
    private String  org_cno;          // [필수]원거래고유번호
    private String  mgr_amt;          // [선택]부분취소/환불요청 금액
    private String  mgr_tax_flg;      // [필수]과세구분 플래그(TG01:복합과세 변경거래)
    private String  mgr_tax_amt;      // [필수]과세부분취소 금액(복합과세 변경 시 필수)
    private String  mgr_free_amt;     // [필수]비과세부분취소 금액(복합과세 변경 시 필수)
    private String  mgr_vat_amt;      // [필수]부가세 부분취소금액(복합과세 변경 시 필수)
    private String  mgr_bank_cd;      // [선택]환불계좌 은행코드
    private String  mgr_account;      // [선택]환불계좌 번호
    private String  mgr_depositor;    // [선택]환불계좌 예금주명
    private String  mgr_socno;        // [선택]환불계좌 주민번호
    private String  mgr_telno;        // [선택]환불고객 연락처
    private String  deli_cd;          // [선택]배송구분[자가:DE01,택배:DE02]
    private String  deli_corp_cd;     // [선택]택배사코드
    private String  deli_invoice;     // [선택]운송장 번호
    private String  deli_rcv_nm;      // [선택]수령인 이름
    private String  deli_rcv_tel;     // [선택]수령인 연락처
    private String  req_id;           // [선택]가맹점 관리자 로그인 아이디
    private String  mgr_msg;          // [선택]변경 사유

    /* -------------------------------------------------------------------------- */
    /* ::: 전문                                                                   */
    /* -------------------------------------------------------------------------- */
    private String  mgr_data;         // 변경정보
    private String  tx_req_data;      // 요청전문

    /* -------------------------------------------------------------------------- */
    /* ::: 결제 결과                                                              */
    /* -------------------------------------------------------------------------- */
    private String  bDBProc;         
    private String  res_cd;          
    private String  res_msg;         
    private String  r_order_no;      
    private String  r_complex_yn;    
    private String  r_msg_type;       //거래구분
    private String  r_noti_type;	    //노티구분
    private String  r_cno;            //PG거래번호
    private String  r_amount;         //총 결제금액
    private String  r_auth_no;        //승인번호
    private String  r_tran_date;      //거래일시
    private String  r_pnt_auth_no;    //포인트 승인 번호
    private String  r_pnt_tran_date;  //포인트 승인 일시
    private String  r_cpon_auth_no;   //쿠폰 승인 번호
    private String  r_cpon_tran_date; //쿠폰 승인 일시
    private String  r_card_no;        //카드번호
    private String  r_issuer_cd;      //발급사코드
    private String  r_issuer_nm;      //발급사명
    private String  r_acquirer_cd;    //매입사코드
    private String  r_acquirer_nm;    //매입사명
    private String  r_install_period; //할부개월
    private String  r_noint;          //무이자여부
    private String  r_bank_cd;        //은행코드
    private String  r_bank_nm;        //은행명
    private String  r_account_no;     //계좌번호
    private String  r_deposit_nm;     //입금자명
    private String  r_expire_date;    //계좌사용 만료일
    private String  r_cash_res_cd;    //현금영수증 결과코드
    private String  r_cash_res_msg;   //현금영수증 결과메세지
    private String  r_cash_auth_no;   //현금영수증 승인번호
    private String  r_cash_tran_date; //현금영수증 승인일시
    private String  r_auth_id;        //PhoneID
    private String  r_billid;         //인증번호
    private String  r_mobile_no;      //휴대폰번호
    private String  r_ars_no;         //ARS 전화번호
    private String  r_cp_cd;          //포인트사
    private String  r_used_pnt;       //사용포인트
    private String  r_remain_pnt;     //잔여한도
    private String  r_pay_pnt;        //할인/발생포인트
    private String  r_accrue_pnt;     //누적포인트
    private String  r_remain_cpon;    //쿠폰잔액
    private String  r_used_cpon;      //쿠폰 사용금액
    private String  r_mall_nm;        //제휴사명칭
    private String  r_escrow_yn;	    //에스크로 사용유무
    private String  r_canc_acq_data;  //매입취소일시
    private String  r_canc_date;      //취소일시
    private String  r_refund_date;    //환불예정일시

    public String getKvp_sessionkey_enc() {
        return kvp_sessionkey_enc;
    }

    public void setKvp_sessionkey_enc(String kvp_sessionkey_enc) {
        this.kvp_sessionkey_enc = kvp_sessionkey_enc;
    }

    public String getKvp_encdata_enc() {
        return kvp_encdata_enc;
    }

    public void setKvp_encdata_enc(String kvp_encdata_enc) {
        this.kvp_encdata_enc = kvp_encdata_enc;
    }

    
    
    public String getTRAN_CD_NOR_PAYMENT() {
        return TRAN_CD_NOR_PAYMENT;
    }

    public void setTRAN_CD_NOR_PAYMENT(String TRAN_CD_NOR_PAYMENT) {
        this.TRAN_CD_NOR_PAYMENT = TRAN_CD_NOR_PAYMENT;
    }

    public String getTRAN_CD_NOR_MGR() {
        return TRAN_CD_NOR_MGR;
    }

    public void setTRAN_CD_NOR_MGR(String TRAN_CD_NOR_MGR) {
        this.TRAN_CD_NOR_MGR = TRAN_CD_NOR_MGR;
    }

    public String getTr_cd() {
        return tr_cd;
    }

    public void setTr_cd(String tr_cd) {
        this.tr_cd = tr_cd;
    }

    public String getPay_type() {
        return pay_type;
    }

    public void setPay_type(String pay_type) {
        this.pay_type = pay_type;
    }

    public String getCard_amt() {
        return card_amt;
    }

    public void setCard_amt(String card_amt) {
        this.card_amt = card_amt;
    }

    public String getCard_txtype() {
        return card_txtype;
    }

    public void setCard_txtype(String card_txtype) {
        this.card_txtype = card_txtype;
    }

    public String getReq_type() {
        return req_type;
    }

    public void setReq_type(String req_type) {
        this.req_type = req_type;
    }

    public String getCert_type() {
        return cert_type;
    }

    public void setCert_type(String cert_type) {
        this.cert_type = cert_type;
    }

    public String getWcc() {
        return wcc;
    }

    public void setWcc(String wcc) {
        this.wcc = wcc;
    }

    public String getCard_no() {
        return card_no;
    }

    public void setCard_no(String card_no) {
        this.card_no = card_no;
    }

    public String getExpire_date() {
        return expire_date;
    }

    public void setExpire_date(String expire_date) {
        this.expire_date = expire_date;
    }

    public String getInstall_period() {
        return install_period;
    }

    public void setInstall_period(String install_period) {
        this.install_period = install_period;
    }

    public String getNoint() {
        return noint;
    }

    public void setNoint(String noint) {
        this.noint = noint;
    }

    public String getCard_user_type() {
        return card_user_type;
    }

    public void setCard_user_type(String card_user_type) {
        this.card_user_type = card_user_type;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuth_value() {
        return auth_value;
    }

    public void setAuth_value(String auth_value) {
        this.auth_value = auth_value;
    }

    public String getCavv() {
        return cavv;
    }

    public void setCavv(String cavv) {
        this.cavv = cavv;
    }

    public String getXid() {
        return xid;
    }

    public void setXid(String xid) {
        this.xid = xid;
    }

    public String getEci() {
        return eci;
    }

    public void setEci(String eci) {
        this.eci = eci;
    }

    public String getKvp_cardcode() {
        return kvp_cardcode;
    }

    public void setKvp_cardcode(String kvp_cardcode) {
        this.kvp_cardcode = kvp_cardcode;
    }

    public String getKvp_sessionkey() {
        return kvp_sessionkey;
    }

    public void setKvp_sessionkey(String kvp_sessionkey) {
        this.kvp_sessionkey = kvp_sessionkey;
    }

    public String getKvp_encdata() {
        return kvp_encdata;
    }

    public void setKvp_encdata(String kvp_encdata) {
        this.kvp_encdata = kvp_encdata;
    }

    public String getReq_cno() {
        return req_cno;
    }

    public void setReq_cno(String req_cno) {
        this.req_cno = req_cno;
    }

    public String getKvp_pgid() {
        return kvp_pgid;
    }

    public void setKvp_pgid(String kvp_pgid) {
        this.kvp_pgid = kvp_pgid;
    }

    public String getAcnt_amt() {
        return acnt_amt;
    }

    public void setAcnt_amt(String acnt_amt) {
        this.acnt_amt = acnt_amt;
    }

    public String getAcnt_txtype() {
        return acnt_txtype;
    }

    public void setAcnt_txtype(String acnt_txtype) {
        this.acnt_txtype = acnt_txtype;
    }

    public String getKftc_signdata() {
        return kftc_signdata;
    }

    public void setKftc_signdata(String kftc_signdata) {
        this.kftc_signdata = kftc_signdata;
    }

    public String getVacct_amt() {
        return vacct_amt;
    }

    public void setVacct_amt(String vacct_amt) {
        this.vacct_amt = vacct_amt;
    }

    public String getVacct_txtype() {
        return vacct_txtype;
    }

    public void setVacct_txtype(String vacct_txtype) {
        this.vacct_txtype = vacct_txtype;
    }

    public String getBank_cd() {
        return bank_cd;
    }

    public void setBank_cd(String bank_cd) {
        this.bank_cd = bank_cd;
    }

    public String getVacct_expr_date() {
        return vacct_expr_date;
    }

    public void setVacct_expr_date(String vacct_expr_date) {
        this.vacct_expr_date = vacct_expr_date;
    }

    public String getVacct_expr_time() {
        return vacct_expr_time;
    }

    public void setVacct_expr_time(String vacct_expr_time) {
        this.vacct_expr_time = vacct_expr_time;
    }

    public String getCash_yn() {
        return cash_yn;
    }

    public void setCash_yn(String cash_yn) {
        this.cash_yn = cash_yn;
    }

    public String getCash_issue_type() {
        return cash_issue_type;
    }

    public void setCash_issue_type(String cash_issue_type) {
        this.cash_issue_type = cash_issue_type;
    }

    public String getCash_auth_type() {
        return cash_auth_type;
    }

    public void setCash_auth_type(String cash_auth_type) {
        this.cash_auth_type = cash_auth_type;
    }

    public String getCash_auth_value() {
        return cash_auth_value;
    }

    public void setCash_auth_value(String cash_auth_value) {
        this.cash_auth_value = cash_auth_value;
    }

    public String getMob_amt() {
        return mob_amt;
    }

    public void setMob_amt(String mob_amt) {
        this.mob_amt = mob_amt;
    }

    public String getMob_txtype() {
        return mob_txtype;
    }

    public void setMob_txtype(String mob_txtype) {
        this.mob_txtype = mob_txtype;
    }

    public String getMobile_cd() {
        return mobile_cd;
    }

    public void setMobile_cd(String mobile_cd) {
        this.mobile_cd = mobile_cd;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getJumin_no() {
        return jumin_no;
    }

    public void setJumin_no(String jumin_no) {
        this.jumin_no = jumin_no;
    }

    public String getMob_auth_id() {
        return mob_auth_id;
    }

    public void setMob_auth_id(String mob_auth_id) {
        this.mob_auth_id = mob_auth_id;
    }

    public String getMob_billid() {
        return mob_billid;
    }

    public void setMob_billid(String mob_billid) {
        this.mob_billid = mob_billid;
    }

    public String getMob_cno() {
        return mob_cno;
    }

    public void setMob_cno(String mob_cno) {
        this.mob_cno = mob_cno;
    }

    public String getPnt_amt() {
        return pnt_amt;
    }

    public void setPnt_amt(String pnt_amt) {
        this.pnt_amt = pnt_amt;
    }

    public String getPnt_txtype() {
        return pnt_txtype;
    }

    public void setPnt_txtype(String pnt_txtype) {
        this.pnt_txtype = pnt_txtype;
    }

    public String getPnt_cp_cd() {
        return pnt_cp_cd;
    }

    public void setPnt_cp_cd(String pnt_cp_cd) {
        this.pnt_cp_cd = pnt_cp_cd;
    }

    public String getPnt_idno() {
        return pnt_idno;
    }

    public void setPnt_idno(String pnt_idno) {
        this.pnt_idno = pnt_idno;
    }

    public String getPnt_pwd() {
        return pnt_pwd;
    }

    public void setPnt_pwd(String pnt_pwd) {
        this.pnt_pwd = pnt_pwd;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getMemb_user_no() {
        return memb_user_no;
    }

    public void setMemb_user_no(String memb_user_no) {
        this.memb_user_no = memb_user_no;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_nm() {
        return user_nm;
    }

    public void setUser_nm(String user_nm) {
        this.user_nm = user_nm;
    }

    public String getUser_mail() {
        return user_mail;
    }

    public void setUser_mail(String user_mail) {
        this.user_mail = user_mail;
    }

    public String getUser_phone1() {
        return user_phone1;
    }

    public void setUser_phone1(String user_phone1) {
        this.user_phone1 = user_phone1;
    }

    public String getUser_phone2() {
        return user_phone2;
    }

    public void setUser_phone2(String user_phone2) {
        this.user_phone2 = user_phone2;
    }

    public String getUser_addr() {
        return user_addr;
    }

    public void setUser_addr(String user_addr) {
        this.user_addr = user_addr;
    }

    public String getProduct_type() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public String getProduct_nm() {
        return product_nm;
    }

    public void setProduct_nm(String product_nm) {
        this.product_nm = product_nm;
    }

    public String getProduct_amt() {
        return product_amt;
    }

    public void setProduct_amt(String product_amt) {
        this.product_amt = product_amt;
    }

    public String getUser_define1() {
        return user_define1;
    }

    public void setUser_define1(String user_define1) {
        this.user_define1 = user_define1;
    }

    public String getUser_define2() {
        return user_define2;
    }

    public void setUser_define2(String user_define2) {
        this.user_define2 = user_define2;
    }

    public String getUser_define3() {
        return user_define3;
    }

    public void setUser_define3(String user_define3) {
        this.user_define3 = user_define3;
    }

    public String getUser_define4() {
        return user_define4;
    }

    public void setUser_define4(String user_define4) {
        this.user_define4 = user_define4;
    }

    public String getUser_define5() {
        return user_define5;
    }

    public void setUser_define5(String user_define5) {
        this.user_define5 = user_define5;
    }

    public String getUser_define6() {
        return user_define6;
    }

    public void setUser_define6(String user_define6) {
        this.user_define6 = user_define6;
    }

    public String getClient_ip() {
        return client_ip;
    }

    public void setClient_ip(String client_ip) {
        this.client_ip = client_ip;
    }

    public String getTot_amt() {
        return tot_amt;
    }

    public void setTot_amt(String tot_amt) {
        this.tot_amt = tot_amt;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getEscrow_yn() {
        return escrow_yn;
    }

    public void setEscrow_yn(String escrow_yn) {
        this.escrow_yn = escrow_yn;
    }

    public String getComplex_yn() {
        return complex_yn;
    }

    public void setComplex_yn(String complex_yn) {
        this.complex_yn = complex_yn;
    }

    public String getTax_flg() {
        return tax_flg;
    }

    public void setTax_flg(String tax_flg) {
        this.tax_flg = tax_flg;
    }

    public String getCom_tax_amt() {
        return com_tax_amt;
    }

    public void setCom_tax_amt(String com_tax_amt) {
        this.com_tax_amt = com_tax_amt;
    }

    public String getCom_free_amt() {
        return com_free_amt;
    }

    public void setCom_free_amt(String com_free_amt) {
        this.com_free_amt = com_free_amt;
    }

    public String getCom_vat_amt() {
        return com_vat_amt;
    }

    public void setCom_vat_amt(String com_vat_amt) {
        this.com_vat_amt = com_vat_amt;
    }

    public String getMgr_txtype() {
        return mgr_txtype;
    }

    public void setMgr_txtype(String mgr_txtype) {
        this.mgr_txtype = mgr_txtype;
    }

    public String getMgr_subtype() {
        return mgr_subtype;
    }

    public void setMgr_subtype(String mgr_subtype) {
        this.mgr_subtype = mgr_subtype;
    }

    public String getOrg_cno() {
        return org_cno;
    }

    public void setOrg_cno(String org_cno) {
        this.org_cno = org_cno;
    }

    public String getMgr_amt() {
        return mgr_amt;
    }

    public void setMgr_amt(String mgr_amt) {
        this.mgr_amt = mgr_amt;
    }

    public String getMgr_tax_flg() {
        return mgr_tax_flg;
    }

    public void setMgr_tax_flg(String mgr_tax_flg) {
        this.mgr_tax_flg = mgr_tax_flg;
    }

    public String getMgr_tax_amt() {
        return mgr_tax_amt;
    }

    public void setMgr_tax_amt(String mgr_tax_amt) {
        this.mgr_tax_amt = mgr_tax_amt;
    }

    public String getMgr_free_amt() {
        return mgr_free_amt;
    }

    public void setMgr_free_amt(String mgr_free_amt) {
        this.mgr_free_amt = mgr_free_amt;
    }

    public String getMgr_vat_amt() {
        return mgr_vat_amt;
    }

    public void setMgr_vat_amt(String mgr_vat_amt) {
        this.mgr_vat_amt = mgr_vat_amt;
    }

    public String getMgr_bank_cd() {
        return mgr_bank_cd;
    }

    public void setMgr_bank_cd(String mgr_bank_cd) {
        this.mgr_bank_cd = mgr_bank_cd;
    }

    public String getMgr_account() {
        return mgr_account;
    }

    public void setMgr_account(String mgr_account) {
        this.mgr_account = mgr_account;
    }

    public String getMgr_depositor() {
        return mgr_depositor;
    }

    public void setMgr_depositor(String mgr_depositor) {
        this.mgr_depositor = mgr_depositor;
    }

    public String getMgr_socno() {
        return mgr_socno;
    }

    public void setMgr_socno(String mgr_socno) {
        this.mgr_socno = mgr_socno;
    }

    public String getMgr_telno() {
        return mgr_telno;
    }

    public void setMgr_telno(String mgr_telno) {
        this.mgr_telno = mgr_telno;
    }

    public String getDeli_cd() {
        return deli_cd;
    }

    public void setDeli_cd(String deli_cd) {
        this.deli_cd = deli_cd;
    }

    public String getDeli_corp_cd() {
        return deli_corp_cd;
    }

    public void setDeli_corp_cd(String deli_corp_cd) {
        this.deli_corp_cd = deli_corp_cd;
    }

    public String getDeli_invoice() {
        return deli_invoice;
    }

    public void setDeli_invoice(String deli_invoice) {
        this.deli_invoice = deli_invoice;
    }

    public String getDeli_rcv_nm() {
        return deli_rcv_nm;
    }

    public void setDeli_rcv_nm(String deli_rcv_nm) {
        this.deli_rcv_nm = deli_rcv_nm;
    }

    public String getDeli_rcv_tel() {
        return deli_rcv_tel;
    }

    public void setDeli_rcv_tel(String deli_rcv_tel) {
        this.deli_rcv_tel = deli_rcv_tel;
    }

    public String getReq_id() {
        return req_id;
    }

    public void setReq_id(String req_id) {
        this.req_id = req_id;
    }

    public String getMgr_msg() {
        return mgr_msg;
    }

    public void setMgr_msg(String mgr_msg) {
        this.mgr_msg = mgr_msg;
    }

    public String getMgr_data() {
        return mgr_data;
    }

    public void setMgr_data(String mgr_data) {
        this.mgr_data = mgr_data;
    }

    public String getTx_req_data() {
        return tx_req_data;
    }

    public void setTx_req_data(String tx_req_data) {
        this.tx_req_data = tx_req_data;
    }

    public String getbDBProc() {
        return bDBProc;
    }

    public void setbDBProc(String bDBProc) {
        this.bDBProc = bDBProc;
    }

    public String getRes_cd() {
        return res_cd;
    }

    public void setRes_cd(String res_cd) {
        this.res_cd = res_cd;
    }

    public String getRes_msg() {
        return res_msg;
    }

    public void setRes_msg(String res_msg) {
        this.res_msg = res_msg;
    }

    public String getR_order_no() {
        return r_order_no;
    }

    public void setR_order_no(String r_order_no) {
        this.r_order_no = r_order_no;
    }

    public String getR_complex_yn() {
        return r_complex_yn;
    }

    public void setR_complex_yn(String r_complex_yn) {
        this.r_complex_yn = r_complex_yn;
    }

    public String getR_msg_type() {
        return r_msg_type;
    }

    public void setR_msg_type(String r_msg_type) {
        this.r_msg_type = r_msg_type;
    }

    public String getR_noti_type() {
        return r_noti_type;
    }

    public void setR_noti_type(String r_noti_type) {
        this.r_noti_type = r_noti_type;
    }

    public String getR_cno() {
        return r_cno;
    }

    public void setR_cno(String r_cno) {
        this.r_cno = r_cno;
    }

    public String getR_amount() {
        return r_amount;
    }

    public void setR_amount(String r_amount) {
        this.r_amount = r_amount;
    }

    public String getR_auth_no() {
        return r_auth_no;
    }

    public void setR_auth_no(String r_auth_no) {
        this.r_auth_no = r_auth_no;
    }

    public String getR_tran_date() {
        return r_tran_date;
    }

    public void setR_tran_date(String r_tran_date) {
        this.r_tran_date = r_tran_date;
    }

    public String getR_pnt_auth_no() {
        return r_pnt_auth_no;
    }

    public void setR_pnt_auth_no(String r_pnt_auth_no) {
        this.r_pnt_auth_no = r_pnt_auth_no;
    }

    public String getR_pnt_tran_date() {
        return r_pnt_tran_date;
    }

    public void setR_pnt_tran_date(String r_pnt_tran_date) {
        this.r_pnt_tran_date = r_pnt_tran_date;
    }

    public String getR_cpon_auth_no() {
        return r_cpon_auth_no;
    }

    public void setR_cpon_auth_no(String r_cpon_auth_no) {
        this.r_cpon_auth_no = r_cpon_auth_no;
    }

    public String getR_cpon_tran_date() {
        return r_cpon_tran_date;
    }

    public void setR_cpon_tran_date(String r_cpon_tran_date) {
        this.r_cpon_tran_date = r_cpon_tran_date;
    }

    public String getR_card_no() {
        return r_card_no;
    }

    public void setR_card_no(String r_card_no) {
        this.r_card_no = r_card_no;
    }

    public String getR_issuer_cd() {
        return r_issuer_cd;
    }

    public void setR_issuer_cd(String r_issuer_cd) {
        this.r_issuer_cd = r_issuer_cd;
    }

    public String getR_issuer_nm() {
        return r_issuer_nm;
    }

    public void setR_issuer_nm(String r_issuer_nm) {
        this.r_issuer_nm = r_issuer_nm;
    }

    public String getR_acquirer_cd() {
        return r_acquirer_cd;
    }

    public void setR_acquirer_cd(String r_acquirer_cd) {
        this.r_acquirer_cd = r_acquirer_cd;
    }

    public String getR_acquirer_nm() {
        return r_acquirer_nm;
    }

    public void setR_acquirer_nm(String r_acquirer_nm) {
        this.r_acquirer_nm = r_acquirer_nm;
    }

    public String getR_install_period() {
        return r_install_period;
    }

    public void setR_install_period(String r_install_period) {
        this.r_install_period = r_install_period;
    }

    public String getR_noint() {
        return r_noint;
    }

    public void setR_noint(String r_noint) {
        this.r_noint = r_noint;
    }

    public String getR_bank_cd() {
        return r_bank_cd;
    }

    public void setR_bank_cd(String r_bank_cd) {
        this.r_bank_cd = r_bank_cd;
    }

    public String getR_bank_nm() {
        return r_bank_nm;
    }

    public void setR_bank_nm(String r_bank_nm) {
        this.r_bank_nm = r_bank_nm;
    }

    public String getR_account_no() {
        return r_account_no;
    }

    public void setR_account_no(String r_account_no) {
        this.r_account_no = r_account_no;
    }

    public String getR_deposit_nm() {
        return r_deposit_nm;
    }

    public void setR_deposit_nm(String r_deposit_nm) {
        this.r_deposit_nm = r_deposit_nm;
    }

    public String getR_expire_date() {
        return r_expire_date;
    }

    public void setR_expire_date(String r_expire_date) {
        this.r_expire_date = r_expire_date;
    }

    public String getR_cash_res_cd() {
        return r_cash_res_cd;
    }

    public void setR_cash_res_cd(String r_cash_res_cd) {
        this.r_cash_res_cd = r_cash_res_cd;
    }

    public String getR_cash_res_msg() {
        return r_cash_res_msg;
    }

    public void setR_cash_res_msg(String r_cash_res_msg) {
        this.r_cash_res_msg = r_cash_res_msg;
    }

    public String getR_cash_auth_no() {
        return r_cash_auth_no;
    }

    public void setR_cash_auth_no(String r_cash_auth_no) {
        this.r_cash_auth_no = r_cash_auth_no;
    }

    public String getR_cash_tran_date() {
        return r_cash_tran_date;
    }

    public void setR_cash_tran_date(String r_cash_tran_date) {
        this.r_cash_tran_date = r_cash_tran_date;
    }

    public String getR_auth_id() {
        return r_auth_id;
    }

    public void setR_auth_id(String r_auth_id) {
        this.r_auth_id = r_auth_id;
    }

    public String getR_billid() {
        return r_billid;
    }

    public void setR_billid(String r_billid) {
        this.r_billid = r_billid;
    }

    public String getR_mobile_no() {
        return r_mobile_no;
    }

    public void setR_mobile_no(String r_mobile_no) {
        this.r_mobile_no = r_mobile_no;
    }

    public String getR_ars_no() {
        return r_ars_no;
    }

    public void setR_ars_no(String r_ars_no) {
        this.r_ars_no = r_ars_no;
    }

    public String getR_cp_cd() {
        return r_cp_cd;
    }

    public void setR_cp_cd(String r_cp_cd) {
        this.r_cp_cd = r_cp_cd;
    }

    public String getR_used_pnt() {
        return r_used_pnt;
    }

    public void setR_used_pnt(String r_used_pnt) {
        this.r_used_pnt = r_used_pnt;
    }

    public String getR_remain_pnt() {
        return r_remain_pnt;
    }

    public void setR_remain_pnt(String r_remain_pnt) {
        this.r_remain_pnt = r_remain_pnt;
    }

    public String getR_pay_pnt() {
        return r_pay_pnt;
    }

    public void setR_pay_pnt(String r_pay_pnt) {
        this.r_pay_pnt = r_pay_pnt;
    }

    public String getR_accrue_pnt() {
        return r_accrue_pnt;
    }

    public void setR_accrue_pnt(String r_accrue_pnt) {
        this.r_accrue_pnt = r_accrue_pnt;
    }

    public String getR_remain_cpon() {
        return r_remain_cpon;
    }

    public void setR_remain_cpon(String r_remain_cpon) {
        this.r_remain_cpon = r_remain_cpon;
    }

    public String getR_used_cpon() {
        return r_used_cpon;
    }

    public void setR_used_cpon(String r_used_cpon) {
        this.r_used_cpon = r_used_cpon;
    }

    public String getR_mall_nm() {
        return r_mall_nm;
    }

    public void setR_mall_nm(String r_mall_nm) {
        this.r_mall_nm = r_mall_nm;
    }

    public String getR_escrow_yn() {
        return r_escrow_yn;
    }

    public void setR_escrow_yn(String r_escrow_yn) {
        this.r_escrow_yn = r_escrow_yn;
    }

    public String getR_canc_acq_data() {
        return r_canc_acq_data;
    }

    public void setR_canc_acq_data(String r_canc_acq_data) {
        this.r_canc_acq_data = r_canc_acq_data;
    }

    public String getR_canc_date() {
        return r_canc_date;
    }

    public void setR_canc_date(String r_canc_date) {
        this.r_canc_date = r_canc_date;
    }

    public String getR_refund_date() {
        return r_refund_date;
    }

    public void setR_refund_date(String r_refund_date) {
        this.r_refund_date = r_refund_date;
    }
    
}
