/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.app.Bean;

import com.onoffkorea.system.app.Vo.AllatPgVo;
import com.onoffkorea.system.app.Vo.KcpPgVo;
import com.onoffkorea.system.app.Vo.KiccPgVo;
import com.onoffkorea.system.common.util.CommonSortableListPagingForm;

/**
 *
 * @author Administrator
 */
public class AppFormBean extends CommonSortableListPagingForm{

    private KiccPgVo ReqKiccPgVo;
    
    private KiccPgVo ResKiccPgVo;
    
    private KcpPgVo ReqKcpPgVo;
    
    private KcpPgVo ResKcpPgVo;
    
    private AllatPgVo ReqAllatPgVo;
    private AllatPgVo ResAllatPgVo;
    
    private String pay_ch_cate;//결제채널
    
    private String mtd_cate;//무이자 구분
    
    private String onfftid;
    
    private String onffmerch_no;
    
    private String insTableNm;
    
    private String param_appAmt;    
    
    private String param_onfftid;
    
    private String param_onffmerch_no;
    
    private String chkCardBean;
        
    private String pay_mtd_seq;
    private String app_iss_cd;
    
    private String org_app_dt;
    
    private String param_card_cd;
    private String param_app_cmp_cd;
    
    private String tran_seq;
    private String tran_result_cd;
    private String tran_result_msg;
    
    private String recvmailaddr;
    
    private String chkcnt;
    private String chkpgseq;
    private String chkpgtid;

    public String getChkpgseq() {
        return chkpgseq;
    }

    public void setChkpgseq(String chkpgseq) {
        this.chkpgseq = chkpgseq;
    }

    public String getChkpgtid() {
        return chkpgtid;
    }

    public void setChkpgtid(String chkpgtid) {
        this.chkpgtid = chkpgtid;
    }
    

    public String getRecvmailaddr() {
        return recvmailaddr;
    }

    public void setRecvmailaddr(String recvmailaddr) {
        this.recvmailaddr = recvmailaddr;
    }
    
    

    public String getTran_seq() {
        return tran_seq;
    }

    public void setTran_seq(String tran_seq) {
        this.tran_seq = tran_seq;
    }

    public String getTran_result_cd() {
        return tran_result_cd;
    }

    public void setTran_result_cd(String tran_result_cd) {
        this.tran_result_cd = tran_result_cd;
    }

    public String getTran_result_msg() {
        return tran_result_msg;
    }

    public void setTran_result_msg(String tran_result_msg) {
        this.tran_result_msg = tran_result_msg;
    }
    
    

    public String getParam_app_cmp_cd() {
        return param_app_cmp_cd;
    }

    public void setParam_app_cmp_cd(String param_app_cmp_cds) {
        this.param_app_cmp_cd = param_app_cmp_cds;
    }

    public String getParam_card_cd() {
        return param_card_cd;
    }

    public void setParam_card_cd(String param_card_cd) {
        this.param_card_cd = param_card_cd;
    }


    public String getOrg_app_dt() {
        return org_app_dt;
    }

    public void setOrg_app_dt(String org_app_dt) {
        this.org_app_dt = org_app_dt;
    }
    
    public String getPay_mtd_seq() {
        return pay_mtd_seq;
    }

    public void setPay_mtd_seq(String pay_mtd_seq) {
        this.pay_mtd_seq = pay_mtd_seq;
    }

    public String getApp_iss_cd() {
        return app_iss_cd;
    }

    public void setApp_iss_cd(String app_iss_cd) {
        this.app_iss_cd = app_iss_cd;
    }
    

    public String getChkCardBean() {
        return chkCardBean;
    }

    public void setChkCardBean(String chkCardBean) {
        this.chkCardBean = chkCardBean;
    }
    
    

    public String getParam_appAmt() {
        return param_appAmt;
    }

    public void setParam_appAmt(String param_appAmt) {
        this.param_appAmt = param_appAmt;
    }
    
    public String getParam_onfftid() {
        return param_onfftid;
    }

    public void setParam_onfftid(String param_onfftid) {
        this.param_onfftid = param_onfftid;
    }

    public String getParam_onffmerch_no() {
        return param_onffmerch_no;
    }

    public void setParam_onffmerch_no(String param_onffmerch_no) {
        this.param_onffmerch_no = param_onffmerch_no;
    }
    
    
    

    public String getInsTableNm() {
        return insTableNm;
    }

    public void setInsTableNm(String insTableNm) {
        this.insTableNm = insTableNm;
    }

    public String getOnffmerch_no() {
        return onffmerch_no;
    }

    public void setOnffmerch_no(String onffmerch_no) {
        this.onffmerch_no = onffmerch_no;
    }
    
    

    public String getOnfftid() {
        return onfftid;
    }

    public void setOnfftid(String onfftid) {
        this.onfftid = onfftid;
    }
    

    public String getPay_ch_cate() {
        return pay_ch_cate;
    }

    public void setPay_ch_cate(String pay_ch_cate) {
        this.pay_ch_cate = pay_ch_cate;
    }

    public String getMtd_cate() {
        return mtd_cate;
    }

    public void setMtd_cate(String mtd_cate) {
        this.mtd_cate = mtd_cate;
    }

    public KiccPgVo getReqKiccPgVo() {
        return ReqKiccPgVo;
    }

    public void setReqKiccPgVo(KiccPgVo ReqKiccPgVo) {
        this.ReqKiccPgVo = ReqKiccPgVo;
    }

    public KiccPgVo getResKiccPgVo() {
        return ResKiccPgVo;
    }

    public void setResKiccPgVo(KiccPgVo ResKiccPgVo) {
        this.ResKiccPgVo = ResKiccPgVo;
    }

    public KcpPgVo getReqKcpPgVo() {
        return ReqKcpPgVo;
    }

    public void setReqKcpPgVo(KcpPgVo ReqKcpPgVo) {
        this.ReqKcpPgVo = ReqKcpPgVo;
    }

    public KcpPgVo getResKcpPgVo() {
        return ResKcpPgVo;
    }

    public void setResKcpPgVo(KcpPgVo ResKcpPgVo) {
        this.ResKcpPgVo = ResKcpPgVo;
    }

    public AllatPgVo getReqAllatPgVo() {
        return ReqAllatPgVo;
    }

    public void setReqAllatPgVo(AllatPgVo ReqAllatPgVo) {
        this.ReqAllatPgVo = ReqAllatPgVo;
    }

    public AllatPgVo getResAllatPgVo() {
        return ResAllatPgVo;
    }

    public void setResAllatPgVo(AllatPgVo ResAllatPgVo) {
        this.ResAllatPgVo = ResAllatPgVo;
    }    
    
    @Override
    public String toString() {
        return "AppFormBean{" + "ReqKiccPgVo=" + ReqKiccPgVo + ", ResKiccPgVo=" + ResKiccPgVo + ", ReqKcpPgVo=" + ReqKcpPgVo + ", ResKcpPgVo=" + ResKcpPgVo + ", pay_ch_cate=" + pay_ch_cate + ", mtd_cate=" + mtd_cate + ", onfftid=" + onfftid + ", onffmerch_no=" + onffmerch_no + ", insTableNm=" + insTableNm + ", param_appAmt=" + param_appAmt + ", param_onfftid=" + param_onfftid + ", param_onffmerch_no=" + param_onffmerch_no + ", chkCardBean=" + chkCardBean + ", pay_mtd_seq=" + pay_mtd_seq + ", app_iss_cd=" + app_iss_cd + ", org_app_dt=" + org_app_dt + ", param_card_cd=" + param_card_cd + ", param_app_cmp_cd=" + param_app_cmp_cd + ", tran_seq=" + tran_seq + ", tran_result_cd=" + tran_result_cd + ", tran_result_msg=" + tran_result_msg + ", recvmailaddr=" + recvmailaddr + '}';
    }
    
    
    
}
