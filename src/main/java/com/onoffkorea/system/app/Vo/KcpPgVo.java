/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.app.Vo;

/**
 *
 * @author Administrator
 */
public class KcpPgVo 
{
    private String      pay_method ; // 결제 방법
    private String     ordr_idxx  ; // 주문 번호
    private String     good_name  ; // 상품 정보
    private String     good_mny   ; // 결제 금액
    
    private String tax_flag;//복합과세구분
    private String comm_tax_mny;//과세
    private String comm_vat_mny;//부가세
    private String comm_free_mny;//비과세
            
    private String     buyr_name  ; // 주문자 이름
    private String     buyr_mail  ; // 주문자 E-Mail
    private String     buyr_tel1  ; // 주문자 전화번호
    private String     buyr_tel2  ; // 주문자 휴대폰번호
    private String     req_tx     ; // 요청 종류
    private String     currency   ; // 화폐단위 (WON/USD)
    private String     quotaopt   ; // 할부개월수 옵션
    private String     mod_type     ; // 변경TYPE(승인취소시 필요)
    private String     mod_desc     ; // 변경사유
    private String     panc_mod_mny ;  // 부분취소 금액
    private String     panc_rem_mny ;  // 부분취소 가능 금액
    private String     mod_tax_mny  ; // 공급가 부분 취소 요청 금액
    private String     mod_vat_mny  ; // 부과세 부분 취소 요청 금액
    private String     mod_free_mny ; // 비과세 부분 취소 요청 금액
    private String     tran_cd    ;                                                 // 트랜잭션 코드
    private String     bSucc      ;                                                 // DB 작업 성공 여부
    private String     res_cd     ;                                                 // 결과코드
    private String     res_msg    ;                                                 // 결과메시지
    private String     tno        ;                                                 // 거래번호
    private String     amount          ;  // 결제 금액
    private String     card_pay_method ;  // 카드 결제 방법
    private String     card_cd         ;                                             // 카드 코드(발급사)
    private String     card_name       ;                                             // 카드명(발급사)
    private String     acqu_cd;        //매입사코드
    private String     acqu_name;      //매입사명
    private String     app_time        ;                                             // 승인시간
    private String     app_no          ;                                             // 승인번호
    private String     noinf           ;                                             // 무이자여부
    private String     quota           ;                                             // 할부개월   
    
    
    private String card_no;//카드번호
    private String card_expiry;//유효기간
    private String cardpwd;//카드암호
    private String cardauth;//인증번호
    private String cust_ip;//
    private String escw_mod;//
    
    private String g_conf_home_dir;
    private String g_conf_key_dir;
    private String g_conf_log_dir;
    
    private String g_conf_log_level;
    private String g_conf_gw_port;
    private String g_conf_tx_mode;
    
    private String g_conf_site_cd;
    private String g_conf_site_key;
    private String g_conf_site_name;
    private String g_conf_gw_url;    
    
    private String g_conf_user_type;
    
    private String tx_cd;
    private String card_tx_type;
    
    private String org_cno;
    
    //현금영수증 관련 정보
    // 현금영수증 정보
    private String user_type;
    private String trad_time;
    private String tr_code;
    private String id_info;
    private String amt_tot;
    private String amt_sup;
    private String amt_svc;
    private String amt_tax;
    private String pay_type;

    // 주문 정보
    private String comment;

    // 가맹점 정보
    private String corp_type;

    // 입점몰인 경우 판매상점 DATA 전문 생성
    private String corp_tax_type;   
    private String corp_tax_no;
    private String corp_sell_tax_no;
    private String corp_nm;
    private String corp_owner_nm;
    private String corp_addr;
    private String corp_telno;

    public String getG_conf_user_type() {
        return g_conf_user_type;
    }

    public void setG_conf_user_type(String g_conf_user_type) {
        this.g_conf_user_type = g_conf_user_type;
    }
    
    
    

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getTrad_time() {
        return trad_time;
    }

    public void setTrad_time(String trad_time) {
        this.trad_time = trad_time;
    }

    public String getTr_code() {
        return tr_code;
    }

    public void setTr_code(String tr_code) {
        this.tr_code = tr_code;
    }

    public String getId_info() {
        return id_info;
    }

    public void setId_info(String id_info) {
        this.id_info = id_info;
    }

    public String getAmt_tot() {
        return amt_tot;
    }

    public void setAmt_tot(String amt_tot) {
        this.amt_tot = amt_tot;
    }

    public String getAmt_sup() {
        return amt_sup;
    }

    public void setAmt_sup(String amt_sup) {
        this.amt_sup = amt_sup;
    }

    public String getAmt_svc() {
        return amt_svc;
    }

    public void setAmt_svc(String amt_svc) {
        this.amt_svc = amt_svc;
    }

    public String getAmt_tax() {
        return amt_tax;
    }

    public void setAmt_tax(String amt_tax) {
        this.amt_tax = amt_tax;
    }

    public String getPay_type() {
        return pay_type;
    }

    public void setPay_type(String pay_type) {
        this.pay_type = pay_type;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCorp_type() {
        return corp_type;
    }

    public void setCorp_type(String corp_type) {
        this.corp_type = corp_type;
    }

    public String getCorp_tax_type() {
        return corp_tax_type;
    }

    public void setCorp_tax_type(String corp_tax_type) {
        this.corp_tax_type = corp_tax_type;
    }

    public String getCorp_tax_no() {
        return corp_tax_no;
    }

    public void setCorp_tax_no(String corp_tax_no) {
        this.corp_tax_no = corp_tax_no;
    }

    public String getCorp_sell_tax_no() {
        return corp_sell_tax_no;
    }

    public void setCorp_sell_tax_no(String corp_sell_tax_no) {
        this.corp_sell_tax_no = corp_sell_tax_no;
    }

    public String getCorp_nm() {
        return corp_nm;
    }

    public void setCorp_nm(String corp_nm) {
        this.corp_nm = corp_nm;
    }

    public String getCorp_owner_nm() {
        return corp_owner_nm;
    }

    public void setCorp_owner_nm(String corp_owner_nm) {
        this.corp_owner_nm = corp_owner_nm;
    }

    public String getCorp_addr() {
        return corp_addr;
    }

    public void setCorp_addr(String corp_addr) {
        this.corp_addr = corp_addr;
    }

    public String getCorp_telno() {
        return corp_telno;
    }

    public void setCorp_telno(String corp_telno) {
        this.corp_telno = corp_telno;
    }
    
    
    public String getOrg_cno() {
        return org_cno;
    }

    public void setOrg_cno(String org_cno) {
        this.org_cno = org_cno;
    }
    
    public String getAcqu_cd() {
        return acqu_cd;
    }

    public void setAcqu_cd(String acqu_cd) {
        this.acqu_cd = acqu_cd;
    }

    public String getAcqu_name() {
        return acqu_name;
    }

    public void setAcqu_name(String acqu_name) {
        this.acqu_name = acqu_name;
    }
    
    

    public String getTax_flag() {
        return tax_flag;
    }

    public void setTax_flag(String tax_flag) {
        this.tax_flag = tax_flag;
    }

    public String getComm_tax_mny() {
        return comm_tax_mny;
    }

    public void setComm_tax_mny(String comm_tax_mny) {
        this.comm_tax_mny = comm_tax_mny;
    }

    public String getComm_vat_mny() {
        return comm_vat_mny;
    }

    public void setComm_vat_mny(String comm_vat_mny) {
        this.comm_vat_mny = comm_vat_mny;
    }

    public String getComm_free_mny() {
        return comm_free_mny;
    }

    public void setComm_free_mny(String comm_free_mny) {
        this.comm_free_mny = comm_free_mny;
    }
    
    public String getCard_tx_type() {
        return card_tx_type;
    }

    public void setCard_tx_type(String card_tx_type) {
        this.card_tx_type = card_tx_type;
    }
    
    

    public String getCard_no() {
        return card_no;
    }

    public void setCard_no(String card_no) {
        this.card_no = card_no;
    }

    public String getCard_expiry() {
        return card_expiry;
    }

    public void setCard_expiry(String card_expiry) {
        this.card_expiry = card_expiry;
    }

    public String getCust_ip() {
        return cust_ip;
    }

    public void setCust_ip(String cust_ip) {
        this.cust_ip = cust_ip;
    }

    public String getEscw_mod() {
        return escw_mod;
    }

    public void setEscw_mod(String escw_mod) {
        this.escw_mod = escw_mod;
    }

    
    
    
    public String getTx_cd() {
        return tx_cd;
    }

    public void setTx_cd(String tx_cd) {
        this.tx_cd = tx_cd;
    }
    
    

    public String getCardpwd() {
        return cardpwd;
    }

    public void setCardpwd(String cardpwd) {
        this.cardpwd = cardpwd;
    }

    public String getCardauth() {
        return cardauth;
    }

    public void setCardauth(String cardauth) {
        this.cardauth = cardauth;
    }
    
    
    

    public String getPay_method() {
        return pay_method;
    }

    public void setPay_method(String pay_method) {
        this.pay_method = pay_method;
    }

    public String getOrdr_idxx() {
        return ordr_idxx;
    }

    public void setOrdr_idxx(String ordr_idxx) {
        this.ordr_idxx = ordr_idxx;
    }

    public String getGood_name() {
        return good_name;
    }

    public void setGood_name(String good_name) {
        this.good_name = good_name;
    }

    public String getGood_mny() {
        return good_mny;
    }

    public void setGood_mny(String good_mny) {
        this.good_mny = good_mny;
    }

    public String getBuyr_name() {
        return buyr_name;
    }

    public void setBuyr_name(String buyr_name) {
        this.buyr_name = buyr_name;
    }

    public String getBuyr_mail() {
        return buyr_mail;
    }

    public void setBuyr_mail(String buyr_mail) {
        this.buyr_mail = buyr_mail;
    }

    public String getBuyr_tel1() {
        return buyr_tel1;
    }

    public void setBuyr_tel1(String buyr_tel1) {
        this.buyr_tel1 = buyr_tel1;
    }

    public String getBuyr_tel2() {
        return buyr_tel2;
    }

    public void setBuyr_tel2(String buyr_tel2) {
        this.buyr_tel2 = buyr_tel2;
    }

    public String getReq_tx() {
        return req_tx;
    }

    public void setReq_tx(String req_tx) {
        this.req_tx = req_tx;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getQuotaopt() {
        return quotaopt;
    }

    public void setQuotaopt(String quotaopt) {
        this.quotaopt = quotaopt;
    }

    public String getMod_type() {
        return mod_type;
    }

    public void setMod_type(String mod_type) {
        this.mod_type = mod_type;
    }

    public String getMod_desc() {
        return mod_desc;
    }

    public void setMod_desc(String mod_desc) {
        this.mod_desc = mod_desc;
    }

    public String getPanc_mod_mny() {
        return panc_mod_mny;
    }

    public void setPanc_mod_mny(String panc_mod_mny) {
        this.panc_mod_mny = panc_mod_mny;
    }

    public String getPanc_rem_mny() {
        return panc_rem_mny;
    }

    public void setPanc_rem_mny(String panc_rem_mny) {
        this.panc_rem_mny = panc_rem_mny;
    }

    public String getMod_tax_mny() {
        return mod_tax_mny;
    }

    public void setMod_tax_mny(String mod_tax_mny) {
        this.mod_tax_mny = mod_tax_mny;
    }

    public String getMod_vat_mny() {
        return mod_vat_mny;
    }

    public void setMod_vat_mny(String mod_vat_mny) {
        this.mod_vat_mny = mod_vat_mny;
    }

    public String getMod_free_mny() {
        return mod_free_mny;
    }

    public void setMod_free_mny(String mod_free_mny) {
        this.mod_free_mny = mod_free_mny;
    }

    public String getTran_cd() {
        return tran_cd;
    }

    public void setTran_cd(String tran_cd) {
        this.tran_cd = tran_cd;
    }

    public String getbSucc() {
        return bSucc;
    }

    public void setbSucc(String bSucc) {
        this.bSucc = bSucc;
    }

    public String getRes_cd() {
        return res_cd;
    }

    public void setRes_cd(String res_cd) {
        this.res_cd = res_cd;
    }

    public String getRes_msg() {
        return res_msg;
    }

    public void setRes_msg(String res_msg) {
        this.res_msg = res_msg;
    }

    public String getTno() {
        return tno;
    }

    public void setTno(String tno) {
        this.tno = tno;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCard_pay_method() {
        return card_pay_method;
    }

    public void setCard_pay_method(String card_pay_method) {
        this.card_pay_method = card_pay_method;
    }

    public String getCard_cd() {
        return card_cd;
    }

    public void setCard_cd(String card_cd) {
        this.card_cd = card_cd;
    }

    public String getCard_name() {
        return card_name;
    }

    public void setCard_name(String card_name) {
        this.card_name = card_name;
    }

    public String getApp_time() {
        return app_time;
    }

    public void setApp_time(String app_time) {
        this.app_time = app_time;
    }

    public String getApp_no() {
        return app_no;
    }

    public void setApp_no(String app_no) {
        this.app_no = app_no;
    }

    public String getNoinf() {
        return noinf;
    }

    public void setNoinf(String noinf) {
        this.noinf = noinf;
    }

    public String getQuota() {
        return quota;
    }

    public void setQuota(String quota) {
        this.quota = quota;
    }

    public String getG_conf_home_dir() {
        return g_conf_home_dir;
    }

    public void setG_conf_home_dir(String g_conf_home_dir) {
        this.g_conf_home_dir = g_conf_home_dir;
    }

    public String getG_conf_key_dir() {
        return g_conf_key_dir;
    }

    public void setG_conf_key_dir(String g_conf_key_dir) {
        this.g_conf_key_dir = g_conf_key_dir;
    }

    public String getG_conf_log_dir() {
        return g_conf_log_dir;
    }

    public void setG_conf_log_dir(String g_conf_log_dir) {
        this.g_conf_log_dir = g_conf_log_dir;
    }

    public String getG_conf_log_level() {
        return g_conf_log_level;
    }

    public void setG_conf_log_level(String g_conf_log_level) {
        this.g_conf_log_level = g_conf_log_level;
    }

    public String getG_conf_gw_port() {
        return g_conf_gw_port;
    }

    public void setG_conf_gw_port(String g_conf_gw_port) {
        this.g_conf_gw_port = g_conf_gw_port;
    }

    public String getG_conf_tx_mode() {
        return g_conf_tx_mode;
    }

    public void setG_conf_tx_mode(String g_conf_tx_mode) {
        this.g_conf_tx_mode = g_conf_tx_mode;
    }

    public String getG_conf_site_cd() {
        return g_conf_site_cd;
    }

    public void setG_conf_site_cd(String g_conf_site_cd) {
        this.g_conf_site_cd = g_conf_site_cd;
    }

    public String getG_conf_site_key() {
        return g_conf_site_key;
    }

    public void setG_conf_site_key(String g_conf_site_key) {
        this.g_conf_site_key = g_conf_site_key;
    }

    public String getG_conf_site_name() {
        return g_conf_site_name;
    }

    public void setG_conf_site_name(String g_conf_site_name) {
        this.g_conf_site_name = g_conf_site_name;
    }

    public String getG_conf_gw_url() {
        return g_conf_gw_url;
    }

    public void setG_conf_gw_url(String g_conf_gw_url) {
        this.g_conf_gw_url = g_conf_gw_url;
    }

    @Override
    public String toString() {
        return "KcpPgVo{" + "pay_method=" + pay_method + ", ordr_idxx=" + ordr_idxx + ", good_name=" + good_name + ", good_mny=" + good_mny + ", tax_flag=" + tax_flag + ", comm_tax_mny=" + comm_tax_mny + ", comm_vat_mny=" + comm_vat_mny + ", comm_free_mny=" + comm_free_mny + ", buyr_name=" + buyr_name + ", buyr_mail=" + buyr_mail + ", buyr_tel1=" + buyr_tel1 + ", buyr_tel2=" + buyr_tel2 + ", req_tx=" + req_tx + ", currency=" + currency + ", quotaopt=" + quotaopt + ", mod_type=" + mod_type + ", mod_desc=" + mod_desc + ", panc_mod_mny=" + panc_mod_mny + ", panc_rem_mny=" + panc_rem_mny + ", mod_tax_mny=" + mod_tax_mny + ", mod_vat_mny=" + mod_vat_mny + ", mod_free_mny=" + mod_free_mny + ", tran_cd=" + tran_cd + ", bSucc=" + bSucc + ", res_cd=" + res_cd + ", res_msg=" + res_msg + ", tno=" + tno + ", amount=" + amount + ", card_pay_method=" + card_pay_method + ", card_cd=" + card_cd + ", card_name=" + card_name + ", acqu_cd=" + acqu_cd + ", acqu_name=" + acqu_name + ", app_time=" + app_time + ", app_no=" + app_no + ", noinf=" + noinf + ", quota=" + quota + ", card_no=" + card_no + ", card_expiry=" + card_expiry + ", cardpwd=" + cardpwd + ", cardauth=" + cardauth + ", cust_ip=" + cust_ip + ", escw_mod=" + escw_mod + ", g_conf_home_dir=" + g_conf_home_dir + ", g_conf_key_dir=" + g_conf_key_dir + ", g_conf_log_dir=" + g_conf_log_dir + ", g_conf_log_level=" + g_conf_log_level + ", g_conf_gw_port=" + g_conf_gw_port + ", g_conf_tx_mode=" + g_conf_tx_mode + ", g_conf_site_cd=" + g_conf_site_cd + ", g_conf_site_key=" + g_conf_site_key + ", g_conf_site_name=" + g_conf_site_name + ", g_conf_gw_url=" + g_conf_gw_url + ", tx_cd=" + tx_cd + ", card_tx_type=" + card_tx_type + ", org_cno=" + org_cno + ", user_type=" + user_type + ", trad_time=" + trad_time + ", tr_code=" + tr_code + ", id_info=" + id_info + ", amt_tot=" + amt_tot + ", amt_sup=" + amt_sup + ", amt_svc=" + amt_svc + ", amt_tax=" + amt_tax + ", pay_type=" + pay_type + ", comment=" + comment + ", corp_type=" + corp_type + ", corp_tax_type=" + corp_tax_type + ", corp_tax_no=" + corp_tax_no + ", corp_sell_tax_no=" + corp_sell_tax_no + ", corp_nm=" + corp_nm + ", corp_owner_nm=" + corp_owner_nm + ", corp_addr=" + corp_addr + ", corp_telno=" + corp_telno + '}';
    }
    
}
