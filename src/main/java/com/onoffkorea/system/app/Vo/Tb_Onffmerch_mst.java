/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.app.Vo;

/**
 *
 * @author Administrators
 */
public class Tb_Onffmerch_mst {
    private String onfftid	; 
    private String comp_seq;
    private String comp_seq_nm;
    private String merch_nm;
    private String tel_1	;
    private String tel_2	;
    private String zip_cd	;
    private String addr_1	;
    private String addr_2	;
    private String svc_stat;
    private String svc_stat_nm;
    private String merch_cate;
    private String merch_cate_nm;
    private String biz_cate;
    private String biz_cate_nm;
    private String biz_type;
    private String biz_type_nm;
    private String tax_flag;
    private String tax_flag_nm;
    private String close_dt;
    private String close_user;
    private String close_reson;
    private String bal_period;
    private String pay_dt_1;
    private String pay_dt_2;
    private String bank_cd;
    private String bank_cd_nm;
    private String acc_no;
    private String agent_commission;
    private String org_seq;
    private String org_seq_nm;
    private String agent_seq;
    private String agent_seq_nm;
    private String ins_dt;
    private String mod_dt;
    private String ins_user;
    private String mod_user;
    private String status;
    private String status_nm;
    private String del_flag;
    private String del_flag_nm;

    public String getOnfftid() {
        return onfftid;
    }

    public void setOnfftid(String onfftid) {
        this.onfftid = onfftid;
    }

    public String getComp_seq() {
        return comp_seq;
    }

    public void setComp_seq(String comp_seq) {
        this.comp_seq = comp_seq;
    }

    public String getComp_seq_nm() {
        return comp_seq_nm;
    }

    public void setComp_seq_nm(String comp_seq_nm) {
        this.comp_seq_nm = comp_seq_nm;
    }

    public String getMerch_nm() {
        return merch_nm;
    }

    public void setMerch_nm(String merch_nm) {
        this.merch_nm = merch_nm;
    }

    public String getTel_1() {
        return tel_1;
    }

    public void setTel_1(String tel_1) {
        this.tel_1 = tel_1;
    }

    public String getTel_2() {
        return tel_2;
    }

    public void setTel_2(String tel_2) {
        this.tel_2 = tel_2;
    }

    public String getZip_cd() {
        return zip_cd;
    }

    public void setZip_cd(String zip_cd) {
        this.zip_cd = zip_cd;
    }

    public String getAddr_1() {
        return addr_1;
    }

    public void setAddr_1(String addr_1) {
        this.addr_1 = addr_1;
    }

    public String getAddr_2() {
        return addr_2;
    }

    public void setAddr_2(String addr_2) {
        this.addr_2 = addr_2;
    }

    public String getSvc_stat() {
        return svc_stat;
    }

    public void setSvc_stat(String svc_stat) {
        this.svc_stat = svc_stat;
    }

    public String getSvc_stat_nm() {
        return svc_stat_nm;
    }

    public void setSvc_stat_nm(String svc_stat_nm) {
        this.svc_stat_nm = svc_stat_nm;
    }

    public String getMerch_cate() {
        return merch_cate;
    }

    public void setMerch_cate(String merch_cate) {
        this.merch_cate = merch_cate;
    }

    public String getMerch_cate_nm() {
        return merch_cate_nm;
    }

    public void setMerch_cate_nm(String merch_cate_nm) {
        this.merch_cate_nm = merch_cate_nm;
    }

    public String getBiz_cate() {
        return biz_cate;
    }

    public void setBiz_cate(String biz_cate) {
        this.biz_cate = biz_cate;
    }

    public String getBiz_cate_nm() {
        return biz_cate_nm;
    }

    public void setBiz_cate_nm(String biz_cate_nm) {
        this.biz_cate_nm = biz_cate_nm;
    }

    public String getBiz_type() {
        return biz_type;
    }

    public void setBiz_type(String biz_type) {
        this.biz_type = biz_type;
    }

    public String getBiz_type_nm() {
        return biz_type_nm;
    }

    public void setBiz_type_nm(String biz_type_nm) {
        this.biz_type_nm = biz_type_nm;
    }

    public String getTax_flag() {
        return tax_flag;
    }

    public void setTax_flag(String tax_flag) {
        this.tax_flag = tax_flag;
    }

    public String getTax_flag_nm() {
        return tax_flag_nm;
    }

    public void setTax_flag_nm(String tax_flag_nm) {
        this.tax_flag_nm = tax_flag_nm;
    }

    public String getClose_dt() {
        return close_dt;
    }

    public void setClose_dt(String close_dt) {
        this.close_dt = close_dt;
    }

    public String getClose_user() {
        return close_user;
    }

    public void setClose_user(String close_user) {
        this.close_user = close_user;
    }

    public String getClose_reson() {
        return close_reson;
    }

    public void setClose_reson(String close_reson) {
        this.close_reson = close_reson;
    }

    public String getBal_period() {
        return bal_period;
    }

    public void setBal_period(String bal_period) {
        this.bal_period = bal_period;
    }

    public String getPay_dt_1() {
        return pay_dt_1;
    }

    public void setPay_dt_1(String pay_dt_1) {
        this.pay_dt_1 = pay_dt_1;
    }

    public String getPay_dt_2() {
        return pay_dt_2;
    }

    public void setPay_dt_2(String pay_dt_2) {
        this.pay_dt_2 = pay_dt_2;
    }

    public String getBank_cd() {
        return bank_cd;
    }

    public void setBank_cd(String bank_cd) {
        this.bank_cd = bank_cd;
    }

    public String getBank_cd_nm() {
        return bank_cd_nm;
    }

    public void setBank_cd_nm(String bank_cd_nm) {
        this.bank_cd_nm = bank_cd_nm;
    }

    public String getAcc_no() {
        return acc_no;
    }

    public void setAcc_no(String acc_no) {
        this.acc_no = acc_no;
    }

    public String getAgent_commission() {
        return agent_commission;
    }

    public void setAgent_commission(String agent_commission) {
        this.agent_commission = agent_commission;
    }

    public String getOrg_seq() {
        return org_seq;
    }

    public void setOrg_seq(String org_seq) {
        this.org_seq = org_seq;
    }

    public String getOrg_seq_nm() {
        return org_seq_nm;
    }

    public void setOrg_seq_nm(String org_seq_nm) {
        this.org_seq_nm = org_seq_nm;
    }

    public String getAgent_seq() {
        return agent_seq;
    }

    public void setAgent_seq(String agent_seq) {
        this.agent_seq = agent_seq;
    }

    public String getAgent_seq_nm() {
        return agent_seq_nm;
    }

    public void setAgent_seq_nm(String agent_seq_nm) {
        this.agent_seq_nm = agent_seq_nm;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus_nm() {
        return status_nm;
    }

    public void setStatus_nm(String status_nm) {
        this.status_nm = status_nm;
    }

    public String getDel_flag() {
        return del_flag;
    }

    public void setDel_flag(String del_flag) {
        this.del_flag = del_flag;
    }

    public String getDel_flag_nm() {
        return del_flag_nm;
    }

    public void setDel_flag_nm(String del_flag_nm) {
        this.del_flag_nm = del_flag_nm;
    }

    

}
