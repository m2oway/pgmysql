/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.app.Vo;

/**
 *
 * @author Administrator
 */
public class Tb_Pay_Chn {
private String onfftid       ;
private String onfftid_nm    ;
private String pay_chn_cate  ;
private String pay_chn_cate_nm;
private String pay_mtd_seq   ;
private String mtd_cate      ;
private String mtd_cate_nm;
private String commission    ;
private String start_dt      ;
private String end_dt        ;
private String status        ;
private String del_flag      ;
private String memo          ;
private String pay_mtd_no;
private String ins_dt        ;
private String mod_dt        ;
private String ins_user      ;
private String mod_user      ;

    public String getOnfftid() {
        return onfftid;
    }

    public void setOnfftid(String onfftid) {
        this.onfftid = onfftid;
    }

    public String getOnfftid_nm() {
        return onfftid_nm;
    }

    public void setOnfftid_nm(String onfftid_nm) {
        this.onfftid_nm = onfftid_nm;
    }

    public String getPay_chn_cate() {
        return pay_chn_cate;
    }

    public void setPay_chn_cate(String pay_chn_cate) {
        this.pay_chn_cate = pay_chn_cate;
    }

    public String getPay_chn_cate_nm() {
        return pay_chn_cate_nm;
    }

    public void setPay_chn_cate_nm(String pay_chn_cate_nm) {
        this.pay_chn_cate_nm = pay_chn_cate_nm;
    }

    public String getPay_mtd_seq() {
        return pay_mtd_seq;
    }

    public void setPay_mtd_seq(String pay_mtd_seq) {
        this.pay_mtd_seq = pay_mtd_seq;
    }

    public String getMtd_cate() {
        return mtd_cate;
    }

    public void setMtd_cate(String mtd_cate) {
        this.mtd_cate = mtd_cate;
    }

    public String getMtd_cate_nm() {
        return mtd_cate_nm;
    }

    public void setMtd_cate_nm(String mtd_cate_nm) {
        this.mtd_cate_nm = mtd_cate_nm;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getStart_dt() {
        return start_dt;
    }

    public void setStart_dt(String start_dt) {
        this.start_dt = start_dt;
    }

    public String getEnd_dt() {
        return end_dt;
    }

    public void setEnd_dt(String end_dt) {
        this.end_dt = end_dt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDel_flag() {
        return del_flag;
    }

    public void setDel_flag(String del_flag) {
        this.del_flag = del_flag;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getPay_mtd_no() {
        return pay_mtd_no;
    }

    public void setPay_mtd_no(String pay_mtd_no) {
        this.pay_mtd_no = pay_mtd_no;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }



}
