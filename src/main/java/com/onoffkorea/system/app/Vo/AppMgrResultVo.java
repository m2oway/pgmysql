/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.app.Vo;

/**
 *
 * @author MoonbongChoi
 */
public class AppMgrResultVo {
    
    private String result_cd;
    private String result_msg;

    public String getResult_cd() {
        return result_cd;
    }

    public void setResult_cd(String result_cd) {
        this.result_cd = result_cd;
    }

    public String getResult_msg() {
        return result_msg;
    }

    public void setResult_msg(String result_msg) {
        this.result_msg = result_msg;
    }
    
    
    
}
