/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.app.Vo;

/**
 *
 * @author MoonbongChoi
 */
public class AppTerminalInfoVo {
    private String pay_mtd_seq;
    private String start_dt;	
    private String end_dt;
    private String app_iss_cd;
    private String tid_mtd;
    private String terminal_no;
    private String pay_mtd;
    private String merch_no;
    private String terminal_pwd;

    public String getTerminal_pwd() {
        return terminal_pwd;
    }

    public void setTerminal_pwd(String terminal_pwd) {
        this.terminal_pwd = terminal_pwd;
    }
    
    

    public String getPay_mtd_seq() {
        return pay_mtd_seq;
    }

    public void setPay_mtd_seq(String pay_mtd_seq) {
        this.pay_mtd_seq = pay_mtd_seq;
    }

    public String getStart_dt() {
        return start_dt;
    }

    public void setStart_dt(String start_dt) {
        this.start_dt = start_dt;
    }

    public String getEnd_dt() {
        return end_dt;
    }

    public void setEnd_dt(String end_dt) {
        this.end_dt = end_dt;
    }

    public String getApp_iss_cd() {
        return app_iss_cd;
    }

    public void setApp_iss_cd(String app_iss_cd) {
        this.app_iss_cd = app_iss_cd;
    }

    public String getTid_mtd() {
        return tid_mtd;
    }

    public void setTid_mtd(String tid_mtd) {
        this.tid_mtd = tid_mtd;
    }

    public String getTerminal_no() {
        return terminal_no;
    }

    public void setTerminal_no(String terminal_no) {
        this.terminal_no = terminal_no;
    }

    public String getPay_mtd() {
        return pay_mtd;
    }

    public void setPay_mtd(String pay_mtd) {
        this.pay_mtd = pay_mtd;
    }

    public String getMerch_no() {
        return merch_no;
    }

    public void setMerch_no(String merch_no) {
        this.merch_no = merch_no;
    }

    @Override
    public String toString() {
        return "AppTerminalInfoVo{" + "pay_mtd_seq=" + pay_mtd_seq + ", start_dt=" + start_dt + ", end_dt=" + end_dt + ", app_iss_cd=" + app_iss_cd + ", tid_mtd=" + tid_mtd + ", terminal_no=" + terminal_no + ", pay_mtd=" + pay_mtd + ", merch_no=" + merch_no + ", terminal_pwd=" + terminal_pwd + '}';
    }


}
