/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.app.Vo;

/**
 *
 * @author MoonbongChoi
 */
public class AppOnffTidInfoVo {
    
    private String comp_nm;
    private String onffmerch_no;
    private String merch_nm;
    private String onfftid_cms_seq;
    private String onfftid;			
    private String onfftid_nm;
    private String tax_flag;
    private String commission;		
    private String start_dt;			
    private String end_dt;
    private String cert_type;
    private String pay_mtd_seq;
    
    private String pay_chn_cate;
    private String biz_no;
    private String corp_no;
    
    private String appreq_chk_amt;

    public String getAppreq_chk_amt() {
        return appreq_chk_amt;
    }

    public void setAppreq_chk_amt(String appreq_chk_amt) {
        this.appreq_chk_amt = appreq_chk_amt;
    }
    
    

    public String getPay_chn_cate() {
        return pay_chn_cate;
    }

    public void setPay_chn_cate(String pay_chn_cate) {
        this.pay_chn_cate = pay_chn_cate;
    }

    public String getBiz_no() {
        return biz_no;
    }

    public void setBiz_no(String biz_no) {
        this.biz_no = biz_no;
    }

    public String getCorp_no() {
        return corp_no;
    }

    public void setCorp_no(String corp_no) {
        this.corp_no = corp_no;
    }
    
    

    public String getPay_mtd_seq() {
        return pay_mtd_seq;
    }

    public void setPay_mtd_seq(String pay_mtd_seq) {
        this.pay_mtd_seq = pay_mtd_seq;
    }
    
    

    public String getComp_nm() {
        return comp_nm;
    }

    public void setComp_nm(String comp_nm) {
        this.comp_nm = comp_nm;
    }
    
    public String getCert_type() {
        return cert_type;
    }

    public void setCert_type(String cert_type) {
        this.cert_type = cert_type;
    }
    
    public String getOnffmerch_no() {
        return onffmerch_no;
    }

    public void setOnffmerch_no(String onffmerch_no) {
        this.onffmerch_no = onffmerch_no;
    }

    public String getMerch_nm() {
        return merch_nm;
    }

    public void setMerch_nm(String merch_nm) {
        this.merch_nm = merch_nm;
    }

    public String getOnfftid_cms_seq() {
        return onfftid_cms_seq;
    }

    public void setOnfftid_cms_seq(String onfftid_cms_seq) {
        this.onfftid_cms_seq = onfftid_cms_seq;
    }

    public String getOnfftid() {
        return onfftid;
    }

    public void setOnfftid(String onfftid) {
        this.onfftid = onfftid;
    }

    public String getOnfftid_nm() {
        return onfftid_nm;
    }

    public void setOnfftid_nm(String onfftid_nm) {
        this.onfftid_nm = onfftid_nm;
    }

    public String getTax_flag() {
        return tax_flag;
    }

    public void setTax_flag(String tax_flag) {
        this.tax_flag = tax_flag;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getStart_dt() {
        return start_dt;
    }

    public void setStart_dt(String start_dt) {
        this.start_dt = start_dt;
    }

    public String getEnd_dt() {
        return end_dt;
    }

    public void setEnd_dt(String end_dt) {
        this.end_dt = end_dt;
    }

    @Override
    public String toString() {
        return "AppOnffTidInfoVo{" + "comp_nm=" + comp_nm + ", onffmerch_no=" + onffmerch_no + ", merch_nm=" + merch_nm + ", onfftid_cms_seq=" + onfftid_cms_seq + ", onfftid=" + onfftid + ", onfftid_nm=" + onfftid_nm + ", tax_flag=" + tax_flag + ", commission=" + commission + ", start_dt=" + start_dt + ", end_dt=" + end_dt + ", cert_type=" + cert_type + ", pay_mtd_seq=" + pay_mtd_seq + '}';
    }


    
}
