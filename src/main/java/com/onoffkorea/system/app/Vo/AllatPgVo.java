/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.app.Vo;

/**
 *
 * @author Administrator
 */
public class AllatPgVo {

    private String allat_shop_id		;//상점 ID                        
    private String allat_order_no		;//주문번호                       
    private String allat_amt		;//승인금액                       
    private String allat_card_no		;//카드번호                       
    private String allat_cardvalid_ym	;//카드 유효기간                  
    private String allat_passwd_no		;//카드비밀번호                   
    private String allat_sell_mm		;//할부개월값                     
    private String allat_business_type	;//결제자카드종류                 
    private String allat_registry_no	;//주민번호                       
    private String allat_biz_no		;//사업자번호                     
    private String allat_shop_member_id	;//회원 ID                        
    private String allat_product_cd		;//상품코드                       
    private String allat_product_nm		;//상품명                         
    private String allat_cardcert_yn	;//카드인증여부                   
    private String allat_zerofee_yn		;//일반/무이자할부 사용여부       
    private String allat_buyer_nm		;//결제자성명                     
    private String allat_recp_name		;//수취인성명                     
    private String allat_recp_addr		;//수취인주소                     
    private String allat_user_ip		;//결제자 IP                      
    private String allat_email_addr		;//결제자 이메일 주소             
    private String allat_bonus_yn		;//보너스포인트 사용여부          
    private String allat_gender		;//구매자 성별                    
    private String allat_birth_ymd		;//구매자 생년월일                
    private String allat_cost_amt		;//결제금액에 대한 원 공급가 금액 
    private String allat_pay_type		;//결제방식                       
    private String allat_test_yn		;//테스트여부                     
    private String allat_opt_pin		;//올앳참조필드                   
    private String allat_opt_mod		;//올앳참조필드                   
    private String reply_cd			;//결과코드                       
    private String reply_msg		;//결과메세지                     
    private String order_no			;//주문번호                       
    private String amt			;//승인금액                       
    private String pay_type			;//지불수단                       
    private String approval_ymdhms		;//승인일시                       
    private String seq_no			;//거래일련번호                   
    private String approval_no		;//승인번호                       
    private String card_id			;//카드ID                         
    private String card_nm			;//카드명                         
    private String sell_mm			;//할부개월                       
    private String zerofee_yn		;//무이자여부                     
    private String cert_yn			;//인증여부                       
    private String contract_yn		;//직가맹여부      
    
    private String     buyr_tel1  ; // 주문자 전화번호
    private String     buyr_tel2  ; // 주문자 휴대폰번호
    
    //취소관련 응답 파라미터
    private String sCancelYMDHMS;//취소일시
    private String sPartCancelFlag;//취소구분 
    private String sRemainAmt;//잔액
    private String sPayType; //거래방식구분  
    
    //현금영수증 요청관련
    
    private String allat_apply_ymdhms  ;
    private String allat_cert_no       ;
    private String allat_supply_amt    ;
    private String allat_vat_amt       ;
    private String allat_receipt_type  ;
    private String allat_seq_no        ;
    private String allat_reg_business_no;
    private String allat_buyer_ip      ;

    public String getAllat_apply_ymdhms() {
        return allat_apply_ymdhms;
    }

    public void setAllat_apply_ymdhms(String allat_apply_ymdhms) {
        this.allat_apply_ymdhms = allat_apply_ymdhms;
    }

    public String getAllat_cert_no() {
        return allat_cert_no;
    }

    public void setAllat_cert_no(String allat_cert_no) {
        this.allat_cert_no = allat_cert_no;
    }

    public String getAllat_supply_amt() {
        return allat_supply_amt;
    }

    public void setAllat_supply_amt(String allat_supply_amt) {
        this.allat_supply_amt = allat_supply_amt;
    }

    public String getAllat_vat_amt() {
        return allat_vat_amt;
    }

    public void setAllat_vat_amt(String allat_vat_amt) {
        this.allat_vat_amt = allat_vat_amt;
    }

    public String getAllat_receipt_type() {
        return allat_receipt_type;
    }

    public void setAllat_receipt_type(String allat_receipt_type) {
        this.allat_receipt_type = allat_receipt_type;
    }

    public String getAllat_seq_no() {
        return allat_seq_no;
    }

    public void setAllat_seq_no(String allat_seq_no) {
        this.allat_seq_no = allat_seq_no;
    }

    public String getAllat_reg_business_no() {
        return allat_reg_business_no;
    }

    public void setAllat_reg_business_no(String allat_reg_business_no) {
        this.allat_reg_business_no = allat_reg_business_no;
    }

    public String getAllat_buyer_ip() {
        return allat_buyer_ip;
    }

    public void setAllat_buyer_ip(String allat_buyer_ip) {
        this.allat_buyer_ip = allat_buyer_ip;
    }
    
    
    

    public String getsCancelYMDHMS() {
        return sCancelYMDHMS;
    }

    public void setsCancelYMDHMS(String sCancelYMDHMS) {
        this.sCancelYMDHMS = sCancelYMDHMS;
    }

    public String getsPartCancelFlag() {
        return sPartCancelFlag;
    }

    public void setsPartCancelFlag(String sPartCancelFlag) {
        this.sPartCancelFlag = sPartCancelFlag;
    }

    public String getsRemainAmt() {
        return sRemainAmt;
    }

    public void setsRemainAmt(String sRemainAmt) {
        this.sRemainAmt = sRemainAmt;
    }

    public String getsPayType() {
        return sPayType;
    }

    public void setsPayType(String sPayType) {
        this.sPayType = sPayType;
    }
        
    public String getBuyr_tel1() {
        return buyr_tel1;
    }

    public void setBuyr_tel1(String buyr_tel1) {
        this.buyr_tel1 = buyr_tel1;
    }

    public String getBuyr_tel2() {
        return buyr_tel2;
    }

    public void setBuyr_tel2(String buyr_tel2) {
        this.buyr_tel2 = buyr_tel2;
    }
        
    public String getAllat_shop_id() {
        return allat_shop_id;
    }

    public void setAllat_shop_id(String allat_shop_id) {
        this.allat_shop_id = allat_shop_id;
    }

    public String getAllat_order_no() {
        return allat_order_no;
    }

    public void setAllat_order_no(String allat_order_no) {
        this.allat_order_no = allat_order_no;
    }

    public String getAllat_amt() {
        return allat_amt;
    }

    public void setAllat_amt(String allat_amt) {
        this.allat_amt = allat_amt;
    }

    public String getAllat_card_no() {
        return allat_card_no;
    }

    public void setAllat_card_no(String allat_card_no) {
        this.allat_card_no = allat_card_no;
    }

    public String getAllat_cardvalid_ym() {
        return allat_cardvalid_ym;
    }

    public void setAllat_cardvalid_ym(String allat_cardvalid_ym) {
        this.allat_cardvalid_ym = allat_cardvalid_ym;
    }

    public String getAllat_passwd_no() {
        return allat_passwd_no;
    }

    public void setAllat_passwd_no(String allat_passwd_no) {
        this.allat_passwd_no = allat_passwd_no;
    }

    public String getAllat_sell_mm() {
        return allat_sell_mm;
    }

    public void setAllat_sell_mm(String allat_sell_mm) {
        this.allat_sell_mm = allat_sell_mm;
    }

    public String getAllat_business_type() {
        return allat_business_type;
    }

    public void setAllat_business_type(String allat_business_type) {
        this.allat_business_type = allat_business_type;
    }

    public String getAllat_registry_no() {
        return allat_registry_no;
    }

    public void setAllat_registry_no(String allat_registry_no) {
        this.allat_registry_no = allat_registry_no;
    }

    public String getAllat_biz_no() {
        return allat_biz_no;
    }

    public void setAllat_biz_no(String allat_biz_no) {
        this.allat_biz_no = allat_biz_no;
    }

    public String getAllat_shop_member_id() {
        return allat_shop_member_id;
    }

    public void setAllat_shop_member_id(String allat_shop_member_id) {
        this.allat_shop_member_id = allat_shop_member_id;
    }

    public String getAllat_product_cd() {
        return allat_product_cd;
    }

    public void setAllat_product_cd(String allat_product_cd) {
        this.allat_product_cd = allat_product_cd;
    }

    public String getAllat_product_nm() {
        return allat_product_nm;
    }

    public void setAllat_product_nm(String allat_product_nm) {
        this.allat_product_nm = allat_product_nm;
    }

    public String getAllat_cardcert_yn() {
        return allat_cardcert_yn;
    }

    public void setAllat_cardcert_yn(String allat_cardcert_yn) {
        this.allat_cardcert_yn = allat_cardcert_yn;
    }

    public String getAllat_zerofee_yn() {
        return allat_zerofee_yn;
    }

    public void setAllat_zerofee_yn(String allat_zerofee_yn) {
        this.allat_zerofee_yn = allat_zerofee_yn;
    }

    public String getAllat_buyer_nm() {
        return allat_buyer_nm;
    }

    public void setAllat_buyer_nm(String allat_buyer_nm) {
        this.allat_buyer_nm = allat_buyer_nm;
    }

    public String getAllat_recp_name() {
        return allat_recp_name;
    }

    public void setAllat_recp_name(String allat_recp_name) {
        this.allat_recp_name = allat_recp_name;
    }

    public String getAllat_recp_addr() {
        return allat_recp_addr;
    }

    public void setAllat_recp_addr(String allat_recp_addr) {
        this.allat_recp_addr = allat_recp_addr;
    }

    public String getAllat_user_ip() {
        return allat_user_ip;
    }

    public void setAllat_user_ip(String allat_user_ip) {
        this.allat_user_ip = allat_user_ip;
    }

    public String getAllat_email_addr() {
        return allat_email_addr;
    }

    public void setAllat_email_addr(String allat_email_addr) {
        this.allat_email_addr = allat_email_addr;
    }

    public String getAllat_bonus_yn() {
        return allat_bonus_yn;
    }

    public void setAllat_bonus_yn(String allat_bonus_yn) {
        this.allat_bonus_yn = allat_bonus_yn;
    }

    public String getAllat_gender() {
        return allat_gender;
    }

    public void setAllat_gender(String allat_gender) {
        this.allat_gender = allat_gender;
    }

    public String getAllat_birth_ymd() {
        return allat_birth_ymd;
    }

    public void setAllat_birth_ymd(String allat_birth_ymd) {
        this.allat_birth_ymd = allat_birth_ymd;
    }

    public String getAllat_cost_amt() {
        return allat_cost_amt;
    }

    public void setAllat_cost_amt(String allat_cost_amt) {
        this.allat_cost_amt = allat_cost_amt;
    }

    public String getAllat_pay_type() {
        return allat_pay_type;
    }

    public void setAllat_pay_type(String allat_pay_type) {
        this.allat_pay_type = allat_pay_type;
    }

    public String getAllat_test_yn() {
        return allat_test_yn;
    }

    public void setAllat_test_yn(String allat_test_yn) {
        this.allat_test_yn = allat_test_yn;
    }

    public String getAllat_opt_pin() {
        return allat_opt_pin;
    }

    public void setAllat_opt_pin(String allat_opt_pin) {
        this.allat_opt_pin = allat_opt_pin;
    }

    public String getAllat_opt_mod() {
        return allat_opt_mod;
    }

    public void setAllat_opt_mod(String allat_opt_mod) {
        this.allat_opt_mod = allat_opt_mod;
    }

    public String getReply_cd() {
        return reply_cd;
    }

    public void setReply_cd(String reply_cd) {
        this.reply_cd = reply_cd;
    }

    public String getReply_msg() {
        return reply_msg;
    }

    public void setReply_msg(String reply_msg) {
        this.reply_msg = reply_msg;
    }

    public String getOrder_no() {
        return order_no;
    }

    public void setOrder_no(String order_no) {
        this.order_no = order_no;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getPay_type() {
        return pay_type;
    }

    public void setPay_type(String pay_type) {
        this.pay_type = pay_type;
    }

    public String getApproval_ymdhms() {
        return approval_ymdhms;
    }

    public void setApproval_ymdhms(String approval_ymdhms) {
        this.approval_ymdhms = approval_ymdhms;
    }

    public String getSeq_no() {
        return seq_no;
    }

    public void setSeq_no(String seq_no) {
        this.seq_no = seq_no;
    }

    public String getApproval_no() {
        return approval_no;
    }

    public void setApproval_no(String approval_no) {
        this.approval_no = approval_no;
    }

    public String getCard_id() {
        return card_id;
    }

    public void setCard_id(String card_id) {
        this.card_id = card_id;
    }

    public String getCard_nm() {
        return card_nm;
    }

    public void setCard_nm(String card_nm) {
        this.card_nm = card_nm;
    }

    public String getSell_mm() {
        return sell_mm;
    }

    public void setSell_mm(String sell_mm) {
        this.sell_mm = sell_mm;
    }

    public String getZerofee_yn() {
        return zerofee_yn;
    }

    public void setZerofee_yn(String zerofee_yn) {
        this.zerofee_yn = zerofee_yn;
    }

    public String getCert_yn() {
        return cert_yn;
    }

    public void setCert_yn(String cert_yn) {
        this.cert_yn = cert_yn;
    }

    public String getContract_yn() {
        return contract_yn;
    }

    public void setContract_yn(String contract_yn) {
        this.contract_yn = contract_yn;
    }

    @Override
    public String toString() {
        return "AllatPgVo{" + "allat_shop_id=" + allat_shop_id + ", allat_order_no=" + allat_order_no + ", allat_amt=" + allat_amt + ", allat_card_no=" + allat_card_no + ", allat_cardvalid_ym=" + allat_cardvalid_ym + ", allat_passwd_no=" + allat_passwd_no + ", allat_sell_mm=" + allat_sell_mm + ", allat_business_type=" + allat_business_type + ", allat_registry_no=" + allat_registry_no + ", allat_biz_no=" + allat_biz_no + ", allat_shop_member_id=" + allat_shop_member_id + ", allat_product_cd=" + allat_product_cd + ", allat_product_nm=" + allat_product_nm + ", allat_cardcert_yn=" + allat_cardcert_yn + ", allat_zerofee_yn=" + allat_zerofee_yn + ", allat_buyer_nm=" + allat_buyer_nm + ", allat_recp_name=" + allat_recp_name + ", allat_recp_addr=" + allat_recp_addr + ", allat_user_ip=" + allat_user_ip + ", allat_email_addr=" + allat_email_addr + ", allat_bonus_yn=" + allat_bonus_yn + ", allat_gender=" + allat_gender + ", allat_birth_ymd=" + allat_birth_ymd + ", allat_cost_amt=" + allat_cost_amt + ", allat_pay_type=" + allat_pay_type + ", allat_test_yn=" + allat_test_yn + ", allat_opt_pin=" + allat_opt_pin + ", allat_opt_mod=" + allat_opt_mod + ", reply_cd=" + reply_cd + ", reply_msg=" + reply_msg + ", order_no=" + order_no + ", amt=" + amt + ", pay_type=" + pay_type + ", approval_ymdhms=" + approval_ymdhms + ", seq_no=" + seq_no + ", approval_no=" + approval_no + ", card_id=" + card_id + ", card_nm=" + card_nm + ", sell_mm=" + sell_mm + ", zerofee_yn=" + zerofee_yn + ", cert_yn=" + cert_yn + ", contract_yn=" + contract_yn + '}';
    }
    
}
