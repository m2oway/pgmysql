/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.app.Controller;

import com.onoffkorea.system.app.Bean.AppFormBean;
import com.onoffkorea.system.app.Service.AppService;
import com.onoffkorea.system.app.Vo.*;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.company.Vo.Tb_Company;
import com.onoffkorea.system.mail.service.EmailService;
import com.onoffkorea.system.mail.vo.EmailVO;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import common.web.servlet.BaseSpringMultiActionController;
import java.net.URLEncoder;
import java.text.NumberFormat;
import java.util.Hashtable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/app/**")
public class AppController  extends BaseSpringMultiActionController{
        
    protected Log logger = LogFactory.getLog(getClass());

    public String getNullToSpace(String param) {
        return (param == null) ? "" : param.trim();
    }    
    

    public String getNullToZero(String param) {
        return (param == null) ? "0" : param.trim();
    }    
        
    
    @Autowired
    private CommonService commonService;
    
    @Autowired
    private AppService appService;
    
    
    @Autowired
    private EmailService emailService;
    
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }    

    //레이아웃
    @RequestMapping(method= RequestMethod.GET,value = "/appLayout")
    public String appLayout(@Valid AppFormBean appFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        return null;
        
    }
    
    
    //결제TID 선택
    @RequestMapping(method= RequestMethod.GET,value = "/basic_appselReady")
    public String basic_appselReady(@Valid AppFormBean appFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        HttpSession session = request.getSession();
        
        SiteSession sessionobj = (SiteSession)session.getAttribute("siteSessionObj");
        
        logger.trace("----------------------------------------------------------------");
        logger.trace("AppControll appselReady getOnfftid session val : " + sessionobj.getSes_onfftid());
        logger.trace("AppControll appselReady getOnffmerch_no session val : " + sessionobj.getSes_onffmerch_no());
        logger.trace("AppControll appselReady getUser_cate session val : " + sessionobj.getSes_user_cate());      
        logger.trace("----------------------------------------------------------------");
          
        appFormBean.setOnfftid(sessionobj.getSes_onfftid());
        appFormBean.setOnffmerch_no(sessionobj.getSes_onffmerch_no());
        appFormBean.setUser_cate(sessionobj.getSes_user_cate());
        appFormBean.setUser_seq(sessionobj.getUser_seq());
        
        Hashtable ht = appService.appselReady(appFormBean);
        
        logger.trace("----------------------------------------------------------------");
        logger.trace("AppControll appselReady onfftid_info  result: " + (String)ht.get("onfftid_info"));
        logger.trace("AppControll appselReady onffmerch_info resulrt : " + (String)ht.get("onffmerch_info"));
        logger.trace("----------------------------------------------------------------");
        
        model.addAttribute("onfftid_info",(String)ht.get("onfftid_info"));
        model.addAttribute("onffmerch_info",(String)ht.get("onffmerch_info"));            
        
        return null;
        
    }    
    
    //결제TID 선택
    @RequestMapping(method= RequestMethod.GET,value = "/appselReady")
    public String appselReady(@Valid AppFormBean appFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        HttpSession session = request.getSession();
        
        SiteSession sessionobj = (SiteSession)session.getAttribute("siteSessionObj");
        
        logger.trace("----------------------------------------------------------------");
        logger.trace("AppControll appselReady getOnfftid session val : " + sessionobj.getSes_onfftid());
        logger.trace("AppControll appselReady getOnffmerch_no session val : " + sessionobj.getSes_onffmerch_no());
        logger.trace("AppControll appselReady getUser_cate session val : " + sessionobj.getSes_user_cate());      
        logger.trace("----------------------------------------------------------------");
          
        appFormBean.setOnfftid(sessionobj.getSes_onfftid());
        appFormBean.setOnffmerch_no(sessionobj.getSes_onffmerch_no());
        appFormBean.setUser_cate(sessionobj.getSes_user_cate());
        appFormBean.setUser_seq(sessionobj.getUser_seq());
        
        Hashtable ht = appService.appselReady(appFormBean);
        
        logger.trace("----------------------------------------------------------------");
        logger.trace("AppControll appselReady onfftid_info  result: " + (String)ht.get("onfftid_info"));
        logger.trace("AppControll appselReady onffmerch_info resulrt : " + (String)ht.get("onffmerch_info"));
        logger.trace("----------------------------------------------------------------");
        
        model.addAttribute("onfftid_info",(String)ht.get("onfftid_info"));
        model.addAttribute("onffmerch_info",(String)ht.get("onffmerch_info"));            
        
        return null;
        
    }
    
    //결제내역 입력
    @RequestMapping(method= RequestMethod.GET,value = "/appReqForm")
    public String appReqForm(@Valid AppFormBean appFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        
        HttpSession session = request.getSession();
        
        SiteSession sessionobj = (SiteSession)session.getAttribute("siteSessionObj");
        
        logger.trace("----------------------------------------------------------------");
        logger.trace("AppControll appReqForm getOnfftid session val : " + sessionobj.getSes_onfftid());
        logger.trace("AppControll appReqForm getOnffmerch_no session val : " + sessionobj.getSes_onffmerch_no());
        logger.trace("AppControll appReqForm getUser_cate session val : " + sessionobj.getSes_user_cate());      
        
        logger.trace("AppControll appReqForm appFormBean.getParam_onffmerch_no val : " + appFormBean.getParam_onffmerch_no());      
        logger.trace("AppControll appReqForm appFormBean.getParam_onfftid val : " + appFormBean.getParam_onfftid());      

        String strUserCate = sessionobj.getSes_user_cate();

        appFormBean.setOnfftid(sessionobj.getSes_onfftid());
        appFormBean.setOnffmerch_no(sessionobj.getSes_onffmerch_no());
        appFormBean.setUser_cate(sessionobj.getSes_user_cate());  
        appFormBean.setUser_seq(sessionobj.getUser_seq());
        
        Hashtable ht = appService.appReqForm(appFormBean);
        
        AppOnffTidInfoVo onfftid_info = (AppOnffTidInfoVo)ht.get("onfftid_info");
        //String strOrderSeq = (String)ht.get("order_seq");
        
        model.addAttribute("onfftid_info",onfftid_info);
        //model.addAttribute("orderseq",strOrderSeq);
        
        logger.trace("AppControll appReqForm (String)ht.get(result_code)d val : " + (String)ht.get("result_code"));   
        
        logger.trace("----------------------------------------------------------------");
        
        model.addAttribute("result_code",(String)ht.get("result_code"));
        model.addAttribute("result_msg",(String)ht.get("result_msg"));

        return null;        
    }
    
//결제요청 
    @RequestMapping(method= RequestMethod.POST,value = "/appHiddenReqAction")
    @ResponseBody
    public AppMgrResultVo appHiddenReqAction(@Valid AppFormBean appFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        AppMgrResultVo resultobj = new AppMgrResultVo();
        
        String strTpResult = null;
        String strTpResultMsg = null;
        
        HttpSession session = request.getSession();
        
        SiteSession sessionobj = (SiteSession)session.getAttribute("siteSessionObj");

        String strUserCate = sessionobj.getSes_user_cate();

        appFormBean.setOnfftid(sessionobj.getSes_onfftid());
        appFormBean.setOnffmerch_no(sessionobj.getSes_onffmerch_no());
        appFormBean.setUser_cate(sessionobj.getSes_user_cate());     
        appFormBean.setUser_seq(sessionobj.getUser_seq());
        
        
            /* -------------------------------------------------------------------------- */
            /* ::: 처리구분 설정                                                          */
            /* -------------------------------------------------------------------------- */
            KiccPgVo kiccpgparam = new KiccPgVo();
        
            kiccpgparam.setTRAN_CD_NOR_PAYMENT("00101000");   // 승인(일반, 에스크로)
            kiccpgparam.setTRAN_CD_NOR_MGR("00201000");   // 변경(일반, 에스크로)
            

            kiccpgparam.setTr_cd            (getNullToSpace(request.getParameter("EP_tr_cd")));           // [필수]요청구분
            kiccpgparam.setPay_type         (getNullToSpace(request.getParameter("EP_pay_type")));        // [필수]결제수단

            /* -------------------------------------------------------------------------- */
            /* ::: 신용카드 정보 설정                                                     */
            /* -------------------------------------------------------------------------- */
            kiccpgparam.setCard_amt          (getNullToSpace(request.getParameter("EP_card_amt")));        // [필수]신용카드 결제금액
            kiccpgparam.setCard_txtype       (getNullToSpace(request.getParameter("EP_card_txtype")));     // [필수]처리종류
            kiccpgparam.setReq_type          (getNullToSpace(request.getParameter("EP_req_type")));        // [필수]카드결제종류
            kiccpgparam.setCert_type         (getNullToSpace(request.getParameter("EP_cert_type")));       // [필수]인증여부
            kiccpgparam.setWcc               (getNullToSpace(request.getParameter("EP_wcc")));             // [필수]wcc
            kiccpgparam.setCard_no           (getNullToSpace(request.getParameter("EP_card_no")));         // [필수]카드번호
            kiccpgparam.setExpire_date       (getNullToSpace(request.getParameter("EP_expire_date")));     // [필수]유효기간
            kiccpgparam.setInstall_period    (getNullToSpace(request.getParameter("EP_install_period")));  // [필수]할부개월
            kiccpgparam.setNoint             (getNullToSpace(request.getParameter("EP_noint")));           // [필수]무이자여부
            kiccpgparam.setCard_user_type    (getNullToSpace(request.getParameter("EP_card_user_type")));  // [선택]카드구분 : 인증여부에 따라 필수
            kiccpgparam.setPassword          (getNullToSpace(request.getParameter("EP_password")));        // [선택]비밀번호 : 인증여부에 따라 필수
            kiccpgparam.setAuth_value        (getNullToSpace(request.getParameter("EP_auth_value")));      // [선택]인증값   : 인증여부에 따라 필수
            kiccpgparam.setCavv              (getNullToSpace(request.getParameter("EP_cavv")));            // [선택]CAVV  : 카드결제종류에 따라 필수
            kiccpgparam.setXid               (getNullToSpace(request.getParameter("EP_xid")));             // [선택]XID   : 카드결제종류에 따라 필수
            kiccpgparam.setEci               (getNullToSpace(request.getParameter("EP_eci")));             // [선택]ECI   : 카드결제종류에 따라 필수
            kiccpgparam.setKvp_cardcode      (getNullToSpace(request.getParameter("EP_kvp_cardcode")));    // [선택]KVP_CARDCODE   : 카드결제종류에 따라 필수
            kiccpgparam.setKvp_sessionkey    (getNullToSpace(request.getParameter("EP_kvp_sessionkey")));  // [선택]KVP_SESSIONKEY : 카드결제종류에 따라 필수
            kiccpgparam.setKvp_encdata       (getNullToSpace(request.getParameter("EP_kvp_encdata")));     // [선택]KVP_ENCDATA    : 카드결제종류에 따라 필수
            kiccpgparam.setReq_cno           (getNullToSpace(request.getParameter("EP_cno")));             // [선택]은련카드 인증거래번호
            kiccpgparam.setKvp_pgid          (getNullToSpace(request.getParameter("EP_kvp_pgid")));        // [선택]은련카드 PGID

            kiccpgparam.setKvp_encdata_enc(URLEncoder.encode( getNullToSpace(request.getParameter("EP_kvp_sessionkey")) ));
            kiccpgparam.setKvp_encdata_enc(URLEncoder.encode( getNullToSpace(request.getParameter("EP_kvp_encdata")) ));

            /* -------------------------------------------------------------------------- */
            /* ::: 결제 주문 정보 설정                                                    */
            /* -------------------------------------------------------------------------- */
            kiccpgparam.setUser_type        (getNullToSpace(request.getParameter("EP_user_type")));        // [선택]사용자구분구분[1:일반,2:회원]
            kiccpgparam.setOrder_no         (getNullToSpace(request.getParameter("EP_order_no")));         // [필수]주문번호
            kiccpgparam.setMemb_user_no     (getNullToSpace(request.getParameter("EP_memb_user_no")));     // [선택]가맹점 고객일련번호
            kiccpgparam.setUser_id          (getNullToSpace(request.getParameter("EP_user_id")));          // [선택]고객 ID
            kiccpgparam.setUser_nm          (getNullToSpace(request.getParameter("EP_user_nm")));          // [필수]고객명
            kiccpgparam.setUser_mail        (getNullToSpace(request.getParameter("EP_user_mail")));        // [필수]고객 E-mail
            kiccpgparam.setUser_phone1      (getNullToSpace(request.getParameter("EP_user_phone1")));      // [필수]가맹점 고객 연락처1
            kiccpgparam.setUser_phone2      (getNullToSpace(request.getParameter("EP_user_phone2")));      // [선택]가맹점 고객 연락처2
            kiccpgparam.setUser_addr        (getNullToSpace(request.getParameter("EP_user_addr")));        // [선택]가맹점 고객 주소
            kiccpgparam.setProduct_type     (getNullToSpace(request.getParameter("EP_product_type")));     // [필수]상품정보구분[0:실물,1:컨텐츠]
            kiccpgparam.setProduct_nm       (getNullToSpace(request.getParameter("EP_product_nm")));       // [필수]상품명
            kiccpgparam.setProduct_amt      (getNullToSpace(request.getParameter("EP_product_amt")));      // [필수]상품금액
            kiccpgparam.setUser_define1     (getNullToSpace(request.getParameter("EP_user_define1")));     // [선택]가맹점필드1
            kiccpgparam.setUser_define2     (getNullToSpace(request.getParameter("EP_user_define2")));     // [선택]가맹점필드2
            kiccpgparam.setUser_define3     (getNullToSpace(request.getParameter("EP_user_define3")));     // [선택]가맹점필드3
            kiccpgparam.setUser_define4     (getNullToSpace(request.getParameter("EP_user_define4")));     // [선택]가맹점필드4
            kiccpgparam.setUser_define5     (getNullToSpace(request.getParameter("EP_user_define5")));     // [선택]가맹점필드5
            kiccpgparam.setUser_define6     (getNullToSpace(request.getParameter("EP_user_define6")));     // [선택]가맹점필드6

            /* -------------------------------------------------------------------------- */
            /* ::: 기타정보 설정                                                          */
            /* -------------------------------------------------------------------------- */
            kiccpgparam.setClient_ip        (request.getRemoteAddr());                                     // [필수]결제고객 IP
            kiccpgparam.setTot_amt          (getNullToSpace(request.getParameter("EP_tot_amt")));          // [필수]결제 총 금액
            kiccpgparam.setCurrency         (getNullToSpace(request.getParameter("EP_currency")));         // [필수]통화코드
            kiccpgparam.setEscrow_yn        (getNullToSpace(request.getParameter("EP_escrow_yn")));        // [필수]에스크로여부
            kiccpgparam.setComplex_yn       (getNullToSpace(request.getParameter("EP_complex_yn")));       // [필수]복합결제여부
            kiccpgparam.setTax_flg          (getNullToSpace(request.getParameter("EP_tax_flg")));          // [필수]과세구분 플래그(TG01:복합과세 승인거래)
            kiccpgparam.setCom_tax_amt      (getNullToSpace(request.getParameter("EP_com_tax_amt")));      // [필수]과세 승인 금액(복합과세 거래 시 필수)
            kiccpgparam.setCom_free_amt     (getNullToSpace(request.getParameter("EP_com_free_amt")));     // [필수]비과세 승인 금액(복합과세 거래 시 필수)
            kiccpgparam.setCom_vat_amt      (getNullToSpace(request.getParameter("EP_com_vat_amt")));      // [필수]부가세 금액(복합과세 거래 시 필수)

            appFormBean.setReqKiccPgVo(kiccpgparam);
            
            //kcp 정보를 설정한다.
            KcpPgVo kcppgparam = new KcpPgVo();
            
            kcppgparam.setCard_pay_method("SSL");//SSL고정
            kcppgparam.setPay_method("CARD");//결제방법card고정
            kcppgparam.setOrdr_idxx(getNullToSpace(request.getParameter("EP_order_no")));//주문번호
            kcppgparam.setGood_name (getNullToSpace(request.getParameter("EP_product_nm")));
            kcppgparam.setGood_mny  (getNullToSpace(request.getParameter("EP_tot_amt")));//결제금액
            
            kcppgparam.setTax_flag("TG03");//복합과세 구분값
            kcppgparam.setComm_free_mny(getNullToSpace(request.getParameter("EP_com_free_amt")));//비과세금액
            kcppgparam.setComm_tax_mny(getNullToSpace(request.getParameter("EP_com_tax_amt")));//과세
            kcppgparam.setComm_vat_mny(getNullToSpace(request.getParameter("EP_com_vat_amt")));//부가세
            
            kcppgparam.setBuyr_name (getNullToSpace(request.getParameter("EP_user_nm")));//주문자이름
            kcppgparam.setBuyr_mail (getNullToSpace(request.getParameter("EP_user_mail")));//주문자email
            kcppgparam.setBuyr_tel1 (getNullToSpace(request.getParameter("EP_user_phone1")));//주문자전화
            kcppgparam.setBuyr_tel2 (getNullToSpace(request.getParameter("EP_user_phone2")));//주문자휴대전화
            kcppgparam.setReq_tx    ("pay");//요청종류 승인시 pay로 고정
            kcppgparam.setCurrency  ("410");//화패단위원화고정
            
            kcppgparam.setQuota(getNullToSpace(request.getParameter("EP_install_period")));//할부개월수
            kcppgparam.setCard_no(getNullToSpace(request.getParameter("EP_card_no")));//카드번호
            kcppgparam.setCard_expiry(getNullToSpace(request.getParameter("EP_expire_date")));//유효기간

            String strTpCardAuth = getNullToSpace(request.getParameter("EP_auth_value"));//인증값
            String strTpCardPwd = getNullToSpace(request.getParameter("EP_password"));//암호
            
            if(!strTpCardAuth.equals("") && !strTpCardPwd.equals(""))
            {
                kcppgparam.setCard_tx_type("11121000");//인증
                kcppgparam.setCardauth(strTpCardAuth);
                kcppgparam.setCardpwd(strTpCardPwd);
            }
            else
            {
                kcppgparam.setCard_tx_type("11111000");//비인증
            }

            
            kcppgparam.setTx_cd("00100000");//카드승인시 고정
            kcppgparam.setEscw_mod("N");//에스크로모드
            kcppgparam.setCust_ip(request.getRemoteAddr());            
            
            //kcppgparam.setMod_type     ("");//변경type
            //kcppgparam.setMod_desc     ("");//변경사유
            //kcppgparam.setPanc_mod_mny("");//부분취소금액
            //kcppgparam.setPanc_rem_mny("");//부분취소가능금액
            //kcppgparam.setMod_tax_mny  ("");//공급가 부분취소 요청금액
            //kcppgparam.setMod_vat_mny  ("");//부과세 부분 취소 요청금액
            //kcppgparam.setMod_free_mny ("");//비과세 부분 취소 요청금액
            //kcppgparam.setTran_cd   ("");//트랜젝션코드
            //kcppgparam.setbSucc("");//DB 작업성공여부
            //kcppgparam.setRes_cd    ("");//결과코드
            //kcppgparam.setRes_msg   ("");//결과메세지
            //kcppgparam.setTno       ("");//거래번호
            //kcppgparam.setAmount          ("");//결제금액
            //kcppgparam.setCard_pay_method ("");//카드결제방법
            //kcppgparam.setCard_cd         ("");//카드코드
            //kcppgparam.setCard_name       ("");//카드명
            //kcppgparam.setApp_time        ("");//승인시간
            //kcppgparam.setApp_no          ("");//승인번호
            //kcppgparam.setNoinf           ("");//무이자여부
            //kcppgparam.setQuota           ("");//할부개월
            
            
            appFormBean.setReqKcpPgVo(kcppgparam);
            
            //AllAt 정보를 셋팅한다.
            AllatPgVo allatparam = new AllatPgVo();
            
            //allatparam.setAllat_shop_id		("");//상점 ID                         
            allatparam.setAllat_order_no	(getNullToSpace(request.getParameter("EP_order_no")));//주문번호                        
            allatparam.setAllat_amt		(getNullToSpace(request.getParameter("EP_tot_amt")));//승인금액                        
            allatparam.setAllat_card_no		(getNullToSpace(request.getParameter("EP_card_no")));//카드번호                        
            allatparam.setAllat_cardvalid_ym	(getNullToSpace(request.getParameter("EP_expire_date")));//카드 유효기간                   
            allatparam.setAllat_passwd_no	(getNullToSpace(request.getParameter("EP_password")));//카드비밀번호                    
            allatparam.setAllat_sell_mm		(getNullToSpace(request.getParameter("EP_install_period")));//할부개월값  
            
            String strTpAllatCardCertType = getNullToSpace(request.getParameter("EP_cert_type"));//인증여부
            
            //카드인증일 경우
            if(strTpAllatCardCertType.equals("0"))
            {
                allatparam.setAllat_cardcert_yn	("Y");//카드인증여부      
                
                String strTpAllatCardType = getNullToSpace(request.getParameter("EP_card_user_type"));//암호
                
                //법인일경우
                if(strTpAllatCardType.equals("1"))
                {
                    allatparam.setAllat_business_type("1");//결제자카드종류                  
                    allatparam.setAllat_registry_no	("");//주민번호                        
                    allatparam.setAllat_biz_no		(getNullToSpace(request.getParameter("EP_auth_value")));//사업자번호      
                }
                //개인일 경우
                else if (strTpAllatCardType.equals("0"))
                {
                    allatparam.setAllat_business_type("0");//결제자카드종류                  
                    allatparam.setAllat_registry_no	(getNullToSpace(request.getParameter("EP_auth_value")));//주민번호                        
                    allatparam.setAllat_biz_no		("");//사업자번호                                    
                }
            }
            //비인증일 경우
            else
            {
                allatparam.setAllat_cardcert_yn	("N");//카드인증여부      
            }         
            
  
            //allatparam.setAllat_shop_member_id	("engplusapp");//회원 ID                         
            allatparam.setAllat_shop_member_id	(getNullToSpace(request.getParameter("EP_user_phone2")));//회원 ID에 전화번호 입력                         
            allatparam.setAllat_product_cd	("eng");//상품코드                        
            allatparam.setAllat_product_nm	(getNullToSpace(request.getParameter("EP_product_nm")));//상품명                          
            allatparam.setAllat_zerofee_yn	("N");//일반/무이자할부 사용여부        
            allatparam.setAllat_buyer_nm	(getNullToSpace(request.getParameter("EP_user_nm")));//결제자성명                      
            allatparam.setAllat_recp_name	(getNullToSpace(request.getParameter("EP_user_nm")));//수취인성명                      
            allatparam.setAllat_recp_addr	("address");//수취인주소                      
            allatparam.setAllat_user_ip		(request.getRemoteAddr());//결제자 IP                       
            allatparam.setAllat_email_addr	(getNullToSpace(request.getParameter("EP_user_mail")));//결제자 이메일 주소              
            allatparam.setAllat_bonus_yn	("N");//보너스포인트 사용여부           
            allatparam.setAllat_gender		("");//구매자 성별                     
            allatparam.setAllat_birth_ymd	("");//구매자 생년월일                 
            allatparam.setAllat_cost_amt	("");//결제금액에 대한 원 공급가 금액  
            allatparam.setAllat_pay_type	("NOR");//결제방식                        
            allatparam.setAllat_test_yn		("Y");//테스트여부(test:Y, 서비스: N)
            allatparam.setAllat_opt_pin		("NOUSE");//올앳참조필드                    
            allatparam.setAllat_opt_mod		("APP");//올앳참조필드  
            /*
            allatparam.setReply_cd		("");//결과코드                        
            allatparam.setReply_msg		("");//결과메세지                      
            allatparam.setOrder_no		("");//주문번호                        
            allatparam.setAmt			("");//승인금액                        
            allatparam.setPay_type		("");//지불수단                        
            allatparam.setApproval_ymdhms	("");//승인일시                        
            allatparam.setSeq_no		("");//거래일련번호                    
            allatparam.setApproval_no		("");//승인번호                        
            allatparam.setCard_id		("");//카드ID                          
            allatparam.setCard_nm		("");//카드명                          
            allatparam.setSell_mm		("");//할부개월                        
            allatparam.setZerofee_yn		("");//무이자여부                      
            allatparam.setCert_yn		("");//인증여부                        
            allatparam.setContract_yn		("");//직가맹여부
            */
            
            //기타 필요정보셋팅
            allatparam.setBuyr_tel1(getNullToSpace(request.getParameter("EP_user_phone1")));//주문자 전화
            allatparam.setBuyr_tel2(getNullToSpace(request.getParameter("EP_user_phone2")));//주문자 휴대전화
            
            appFormBean.setReqAllatPgVo(allatparam);
            
            
            //기본검증 및 결제필요정보를 가져온다.
            Hashtable ht = appService.appReqAction(appFormBean);
            
            strTpResult = (String)ht.get("RESULT_CD");
            strTpResultMsg = (String)ht.get("RESULT_MSG");
            
            //가맹점 정보
            AppOnffTidInfoVo appOnffTidInfo  = (AppOnffTidInfoVo)ht.get("onfftidInfo");
            
            //terminal정보
            AppTerminalInfoVo appTerminfo = (AppTerminalInfoVo)ht.get("appTidObj");            
            
            if(strTpResult == null || !strTpResult.equals("0000"))
            {
                resultobj.setResult_cd(strTpResult);
                resultobj.setResult_msg(strTpResultMsg);
                
                String strErrTid = null;
                if(appTerminfo != null)
                {
                    strErrTid = appTerminfo.getTerminal_no();
                    
                    if(strErrTid == null)
                    {
                        strErrTid = "XXXXXXX";
                    }
                }
                else
                {
                    strErrTid = "XXXXXXX";
                }
                
                appService.appErrCardTranSave(appFormBean, strErrTid, strTpResult, strTpResultMsg);                

                return resultobj;
            }
            
            
            //결제를 요청한다.(tid 종류에 따라)
            String strTidMtd= appTerminfo.getTid_mtd();
            
            logger.debug("strTidMtd : " + strTidMtd);
            
            Hashtable ht_appresult;
            //KICC PG일 경우
            if(strTidMtd.equals("KICCPG"))
            {
               logger.trace("----------------------------------------------------------------");
               logger.trace("AppControll appReqAction KICCPG.cert_file val : " + new String(getMessage("KICCPG.cert_file")));
               logger.trace("AppControll appReqAction KICCPG.cert_file val : " + new String(getMessage("KICCPG.g_log_dir")));
               logger.trace("AppControll appReqAction KICCPG.cert_file val : " + new String(getMessage("KICCPG.g_gw_url")));
               logger.trace("AppControll appReqAction KICCPG.cert_file val : " + new String(getMessage("KICCPG.g_gw_port")));
               logger.trace("----------------------------------------------------------------");                
                
               kiccpgparam.setKiccpg_cert_file( new String(getMessage("KICCPG.cert_file")));
               kiccpgparam.setKiccpg_g_log_dir( new String(getMessage("KICCPG.g_log_dir")));
               kiccpgparam.setKiccpg_g_gw_url( new String(getMessage("KICCPG.g_gw_url")));
               kiccpgparam.setKiccpg_g_gw_port( new String(getMessage("KICCPG.g_gw_port")));
               
               ht_appresult = appService.appKiccReqAction(appFormBean, appOnffTidInfo, appTerminfo);
               
                resultobj.setResult_cd((String)ht_appresult.get("RESULT_CD"));
                resultobj.setResult_msg((String)ht_appresult.get("RESULT_MSG"));
            }
            else if(strTidMtd.equals("KCPPG"))
            {
               logger.trace("----------------------------------------------------------------");
               logger.trace("AppControll appReqAction KCPPG.cert_file val : " + new String(getMessage("KICCPG.cert_file")));
               logger.trace("AppControll appReqAction KCPPG.cert_file val : " + new String(getMessage("KICCPG.g_log_dir")));
               logger.trace("AppControll appReqAction KCPPG.cert_file val : " + new String(getMessage("KICCPG.g_gw_url")));
               logger.trace("AppControll appReqAction KCPPG.cert_file val : " + new String(getMessage("KICCPG.g_gw_port")));
               logger.trace("----------------------------------------------------------------");                
               
               kcppgparam.setG_conf_home_dir(new String(getMessage("g_conf_home_dir")));
               kcppgparam.setG_conf_key_dir(new String(getMessage("g_conf_key_dir")));
               kcppgparam.setG_conf_log_dir(new String(getMessage("g_conf_log_dir")));
               kcppgparam.setG_conf_log_level(new String(getMessage("g_conf_log_level")));
               kcppgparam.setG_conf_gw_port(new String(getMessage("g_conf_gw_port")));
               kcppgparam.setG_conf_tx_mode(new String(getMessage("g_conf_tx_mode")));
               kcppgparam.setG_conf_site_name(new String(getMessage("g_conf_site_name")));
               kcppgparam.setG_conf_gw_url(new String(getMessage("g_conf_gw_url")));
                       
               ht_appresult = appService.appKcpReqAction(appFormBean, appOnffTidInfo, appTerminfo);
               
                resultobj.setResult_cd((String)ht_appresult.get("RESULT_CD"));
                resultobj.setResult_msg((String)ht_appresult.get("RESULT_MSG"));
            }
            else if(strTidMtd.equals("ALLATPG"))
            {
               String strTestFlag =  com.onoffkorea.system.common.util.Util.nullToString(new String(getMessage("ALLAT_TESTFLAG")));
               
               logger.debug("strTestFlag : " + strTestFlag);
                
               if(strTestFlag.equals("N"))
               {
                   allatparam.setAllat_test_yn("N"); 
               }
               else
               {
                   allatparam.setAllat_test_yn("Y"); 
               }
               
               logger.debug("allatparam.getAllat_test_yn : " + allatparam.getAllat_test_yn());  
                
               ht_appresult = appService.appAllatReqAction(appFormBean, appOnffTidInfo, appTerminfo);
               
                resultobj.setResult_cd((String)ht_appresult.get("RESULT_CD"));
                resultobj.setResult_msg((String)ht_appresult.get("RESULT_MSG"));
            }            
            else
            {
                //throw new Exception("지원하지 않는 결제사 입니다.");
                resultobj.setResult_cd("OF03");
                resultobj.setResult_msg("지원하지 않는 결제사 입니다.");
            }
            
        return resultobj;
        
    }    
    
    //결제요청 
    @RequestMapping(method= RequestMethod.POST,value = "/appAction")
    public String appReqAction(@Valid AppFormBean appFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
    
        String strTpResult = null;
        String strTpResultMsg = null;
        
        HttpSession session = request.getSession();
        
        SiteSession sessionobj = (SiteSession)session.getAttribute("siteSessionObj");

        String strUserCate = sessionobj.getSes_user_cate();

        appFormBean.setOnfftid(sessionobj.getSes_onfftid());
        appFormBean.setOnffmerch_no(sessionobj.getSes_onffmerch_no());
        appFormBean.setUser_cate(sessionobj.getSes_user_cate());     
        appFormBean.setUser_seq(sessionobj.getUser_seq());
        
        
            /* -------------------------------------------------------------------------- */
            /* ::: 처리구분 설정                                                          */
            /* -------------------------------------------------------------------------- */
            KiccPgVo kiccpgparam = new KiccPgVo();
        
            kiccpgparam.setTRAN_CD_NOR_PAYMENT("00101000");   // 승인(일반, 에스크로)
            kiccpgparam.setTRAN_CD_NOR_MGR("00201000");   // 변경(일반, 에스크로)
            

            kiccpgparam.setTr_cd            (getNullToSpace(request.getParameter("EP_tr_cd")));           // [필수]요청구분
            kiccpgparam.setPay_type         (getNullToSpace(request.getParameter("EP_pay_type")));        // [필수]결제수단

            /* -------------------------------------------------------------------------- */
            /* ::: 신용카드 정보 설정                                                     */
            /* -------------------------------------------------------------------------- */
            kiccpgparam.setCard_amt          (getNullToSpace(request.getParameter("EP_card_amt")));        // [필수]신용카드 결제금액
            kiccpgparam.setCard_txtype       (getNullToSpace(request.getParameter("EP_card_txtype")));     // [필수]처리종류
            kiccpgparam.setReq_type          (getNullToSpace(request.getParameter("EP_req_type")));        // [필수]카드결제종류
            kiccpgparam.setCert_type         (getNullToSpace(request.getParameter("EP_cert_type")));       // [필수]인증여부
            kiccpgparam.setWcc               (getNullToSpace(request.getParameter("EP_wcc")));             // [필수]wcc
            kiccpgparam.setCard_no           (getNullToSpace(request.getParameter("EP_card_no")));         // [필수]카드번호
            kiccpgparam.setExpire_date       (getNullToSpace(request.getParameter("EP_expire_date")));     // [필수]유효기간
            kiccpgparam.setInstall_period    (getNullToSpace(request.getParameter("EP_install_period")));  // [필수]할부개월
            kiccpgparam.setNoint             (getNullToSpace(request.getParameter("EP_noint")));           // [필수]무이자여부
            kiccpgparam.setCard_user_type    (getNullToSpace(request.getParameter("EP_card_user_type")));  // [선택]카드구분 : 인증여부에 따라 필수
            kiccpgparam.setPassword          (getNullToSpace(request.getParameter("EP_password")));        // [선택]비밀번호 : 인증여부에 따라 필수
            kiccpgparam.setAuth_value        (getNullToSpace(request.getParameter("EP_auth_value")));      // [선택]인증값   : 인증여부에 따라 필수
            kiccpgparam.setCavv              (getNullToSpace(request.getParameter("EP_cavv")));            // [선택]CAVV  : 카드결제종류에 따라 필수
            kiccpgparam.setXid               (getNullToSpace(request.getParameter("EP_xid")));             // [선택]XID   : 카드결제종류에 따라 필수
            kiccpgparam.setEci               (getNullToSpace(request.getParameter("EP_eci")));             // [선택]ECI   : 카드결제종류에 따라 필수
            kiccpgparam.setKvp_cardcode      (getNullToSpace(request.getParameter("EP_kvp_cardcode")));    // [선택]KVP_CARDCODE   : 카드결제종류에 따라 필수
            kiccpgparam.setKvp_sessionkey    (getNullToSpace(request.getParameter("EP_kvp_sessionkey")));  // [선택]KVP_SESSIONKEY : 카드결제종류에 따라 필수
            kiccpgparam.setKvp_encdata       (getNullToSpace(request.getParameter("EP_kvp_encdata")));     // [선택]KVP_ENCDATA    : 카드결제종류에 따라 필수
            kiccpgparam.setReq_cno           (getNullToSpace(request.getParameter("EP_cno")));             // [선택]은련카드 인증거래번호
            kiccpgparam.setKvp_pgid          (getNullToSpace(request.getParameter("EP_kvp_pgid")));        // [선택]은련카드 PGID

            kiccpgparam.setKvp_encdata_enc(URLEncoder.encode( getNullToSpace(request.getParameter("EP_kvp_sessionkey")) ));
            kiccpgparam.setKvp_encdata_enc(URLEncoder.encode( getNullToSpace(request.getParameter("EP_kvp_encdata")) ));

            /* -------------------------------------------------------------------------- */
            /* ::: 계좌이체 정보 설정                                                     */
            /* -------------------------------------------------------------------------- */
            /*
            kiccpgparam.setAcnt_amt          (getNullToSpace(request.getParameter("EP_acnt_amt")));        // [필수]계좌이체 결제금액
            kiccpgparam.setAcnt_txtype       (getNullToSpace(request.getParameter("EP_acnt_txtype")));     // [필수]계좌이체 처리종류
            kiccpgparam.setKftc_signdata     (getNullToSpace(request.getParameter("hd_pi")));              // [필수]전자지갑 암호문
            */
            /* -------------------------------------------------------------------------- */
            /* ::: 무통장입금 정보 설정                                                   */
            /* -------------------------------------------------------------------------- */
            /*
            kiccpgparam.setVacct_amt         (getNullToSpace(request.getParameter("EP_vacct_amt")));       // [필수]무통장입금 결제금액
            kiccpgparam.setVacct_txtype      (getNullToSpace(request.getParameter("EP_vacct_txtype")));    // [필수]무통장입금 처리종류
            kiccpgparam.setBank_cd           (getNullToSpace(request.getParameter("EP_bank_cd")));         // [필수]은행코드
            kiccpgparam.setVacct_expr_date   (getNullToSpace(request.getParameter("EP_expire_date")));     // [필수]입금만료일
            kiccpgparam.setVacct_expr_time   (getNullToSpace(request.getParameter("EP_expire_time")));     // [필수]입금만료시간
            */
            /* -------------------------------------------------------------------------- */
            /* ::: 현금영수증 정보 설정                                                   */
            /* -------------------------------------------------------------------------- */
            /*
            kiccpgparam.setCash_yn           (getNullToSpace(request.getParameter("EP_cash_yn")));         // [필수]현금영수증 발행여부
            kiccpgparam.setCash_issue_type   (getNullToSpace(request.getParameter("EP_cash_issue_type"))); // [선택]현금영수증발행용도
            kiccpgparam.setCash_auth_type    (getNullToSpace(request.getParameter("EP_cash_auth_type")));  // [선택]인증구분
            kiccpgparam.setCash_auth_value   (getNullToSpace(request.getParameter("EP_cash_auth_value"))); // [선택]인증번호
            */
            /* -------------------------------------------------------------------------- */
            /* ::: 휴대폰결제 정보 설정                                                   */
            /* -------------------------------------------------------------------------- */
            /*
            kiccpgparam.setMob_amt           (getNullToSpace(request.getParameter("EP_mob_amt")));         // [필수]휴대폰 결제금액
            kiccpgparam.setMob_txtype        (getNullToSpace(request.getParameter("EP_mob_txtype")));      // [필수]휴대폰 처리종류
            kiccpgparam.setMobile_cd         (getNullToSpace(request.getParameter("EP_mobile_cd")));       // [필수]이통사코드
            kiccpgparam.setMobile_no         (getNullToSpace(request.getParameter("EP_mobile_no")));       // [필수]휴대폰번호
            kiccpgparam.setJumin_no          (getNullToSpace(request.getParameter("EP_jumin_no")));        // [필수]휴대폰 가입자 주민등록번호
            kiccpgparam.setMob_auth_id       (getNullToSpace(request.getParameter("EP_mob_auth_id")));     // [필수]인증고유번호(승인 시 필수)
            kiccpgparam.setMob_billid        (getNullToSpace(request.getParameter("EP_mob_billid")));      // [필수]인증번호(승인 시 필수)
            kiccpgparam.setMob_cno           (getNullToSpace(request.getParameter("EP_mob_cno")));         // [필수]PG거래번호(승인 시 필수)
            */
            /* -------------------------------------------------------------------------- */
            /* ::: 포인트 결제 정보 설정                                                  */
            /* -------------------------------------------------------------------------- */
            /*
            kiccpgparam.setPnt_amt           (getNullToSpace(request.getParameter("EP_pnt_amt")));         // [필수]포인트 결제금액
            kiccpgparam.setPnt_txtype        (getNullToSpace(request.getParameter("EP_pnt_txtype")));      // [필수]포인트 처리종류
            kiccpgparam.setPnt_cp_cd         (getNullToSpace(request.getParameter("EP_pnt_cp_cd")));       // [필수]포인트 서비스사 코드
            kiccpgparam.setPnt_idno          (getNullToSpace(request.getParameter("EP_pnt_idno")));        // [필수]포인트 ID(카드)번호
            kiccpgparam.setPnt_pwd           (getNullToSpace(request.getParameter("EP_pnt_pwd")));         // [옵션]포인트 비밀번호
            */
            
            /* -------------------------------------------------------------------------- */
            /* ::: 결제 주문 정보 설정                                                    */
            /* -------------------------------------------------------------------------- */
            kiccpgparam.setUser_type        (getNullToSpace(request.getParameter("EP_user_type")));        // [선택]사용자구분구분[1:일반,2:회원]
            kiccpgparam.setOrder_no         (getNullToSpace(request.getParameter("EP_order_no")));         // [필수]주문번호
            kiccpgparam.setMemb_user_no     (getNullToSpace(request.getParameter("EP_memb_user_no")));     // [선택]가맹점 고객일련번호
            kiccpgparam.setUser_id          (getNullToSpace(request.getParameter("EP_user_id")));          // [선택]고객 ID
            kiccpgparam.setUser_nm          (getNullToSpace(request.getParameter("EP_user_nm")));          // [필수]고객명
            kiccpgparam.setUser_mail        (getNullToSpace(request.getParameter("EP_user_mail")));        // [필수]고객 E-mail
            kiccpgparam.setUser_phone1      (getNullToSpace(request.getParameter("EP_user_phone1")));      // [필수]가맹점 고객 연락처1
            kiccpgparam.setUser_phone2      (getNullToSpace(request.getParameter("EP_user_phone2")));      // [선택]가맹점 고객 연락처2
            kiccpgparam.setUser_addr        (getNullToSpace(request.getParameter("EP_user_addr")));        // [선택]가맹점 고객 주소
            kiccpgparam.setProduct_type     (getNullToSpace(request.getParameter("EP_product_type")));     // [필수]상품정보구분[0:실물,1:컨텐츠]
            kiccpgparam.setProduct_nm       (getNullToSpace(request.getParameter("EP_product_nm")));       // [필수]상품명
            kiccpgparam.setProduct_amt      (getNullToSpace(request.getParameter("EP_product_amt")));      // [필수]상품금액
            kiccpgparam.setUser_define1     (getNullToSpace(request.getParameter("EP_user_define1")));     // [선택]가맹점필드1
            kiccpgparam.setUser_define2     (getNullToSpace(request.getParameter("EP_user_define2")));     // [선택]가맹점필드2
            kiccpgparam.setUser_define3     (getNullToSpace(request.getParameter("EP_user_define3")));     // [선택]가맹점필드3
            kiccpgparam.setUser_define4     (getNullToSpace(request.getParameter("EP_user_define4")));     // [선택]가맹점필드4
            kiccpgparam.setUser_define5     (getNullToSpace(request.getParameter("EP_user_define5")));     // [선택]가맹점필드5
            kiccpgparam.setUser_define6     (getNullToSpace(request.getParameter("EP_user_define6")));     // [선택]가맹점필드6

            /* -------------------------------------------------------------------------- */
            /* ::: 기타정보 설정                                                          */
            /* -------------------------------------------------------------------------- */
            kiccpgparam.setClient_ip        (request.getRemoteAddr());                                     // [필수]결제고객 IP
            kiccpgparam.setTot_amt          (getNullToSpace(request.getParameter("EP_tot_amt")));          // [필수]결제 총 금액
            kiccpgparam.setCurrency         (getNullToSpace(request.getParameter("EP_currency")));         // [필수]통화코드
            kiccpgparam.setEscrow_yn        (getNullToSpace(request.getParameter("EP_escrow_yn")));        // [필수]에스크로여부
            kiccpgparam.setComplex_yn       (getNullToSpace(request.getParameter("EP_complex_yn")));       // [필수]복합결제여부
            kiccpgparam.setTax_flg          (getNullToSpace(request.getParameter("EP_tax_flg")));          // [필수]과세구분 플래그(TG01:복합과세 승인거래)
            kiccpgparam.setCom_tax_amt      (getNullToSpace(request.getParameter("EP_com_tax_amt")));      // [필수]과세 승인 금액(복합과세 거래 시 필수)
            kiccpgparam.setCom_free_amt     (getNullToSpace(request.getParameter("EP_com_free_amt")));     // [필수]비과세 승인 금액(복합과세 거래 시 필수)
            kiccpgparam.setCom_vat_amt      (getNullToSpace(request.getParameter("EP_com_vat_amt")));      // [필수]부가세 금액(복합과세 거래 시 필수)


            /* -------------------------------------------------------------------------- */
            /* ::: 변경관리 정보 설정                                                     */
            /* -------------------------------------------------------------------------- */
/*            
            kiccpgparam.setMgr_txtype       (getNullToSpace(request.getParameter("mgr_txtype")));          // [필수]거래구분
            kiccpgparam.setMgr_subtype      (getNullToSpace(request.getParameter("mgr_subtype")));         // [선택]변경세부구분
            kiccpgparam.setOrg_cno          (getNullToSpace(request.getParameter("org_cno")));             // [필수]원거래고유번호
            kiccpgparam.setMgr_amt          (getNullToSpace(request.getParameter("mgr_amt")));             // [선택]부분취소/환불요청 금액
            kiccpgparam.setMgr_tax_flg      (getNullToSpace(request.getParameter("mgr_tax_flg")));         // [필수]과세구분 플래그(TG01:복합과세 변경거래)
            kiccpgparam.setMgr_tax_amt      (getNullToSpace(request.getParameter("mgr_tax_amt")));         // [필수]과세부분취소 금액(복합과세 변경 시 필수)
            kiccpgparam.setMgr_free_amt     (getNullToSpace(request.getParameter("mgr_free_amt")));        // [필수]비과세부분취소 금액(복합과세 변경 시 필수)
            kiccpgparam.setMgr_vat_amt      (getNullToSpace(request.getParameter("mgr_vat_amt")));         // [필수]부가세 부분취소금액(복합과세 변경 시 필수)
            kiccpgparam.setMgr_bank_cd      (getNullToSpace(request.getParameter("mgr_bank_cd")));         // [선택]환불계좌 은행코드
            kiccpgparam.setMgr_account      (getNullToSpace(request.getParameter("mgr_account")));         // [선택]환불계좌 번호
            kiccpgparam.setMgr_depositor    (getNullToSpace(request.getParameter("mgr_depositor")));       // [선택]환불계좌 예금주명
            kiccpgparam.setMgr_socno        (getNullToSpace(request.getParameter("mgr_socno")));           // [선택]환불계좌 주민번호
            kiccpgparam.setMgr_telno        (getNullToSpace(request.getParameter("mgr_telno")));           // [선택]환불고객 연락처
            kiccpgparam.setDeli_cd          (getNullToSpace(request.getParameter("deli_cd")));             // [선택]배송구분[자가:DE01,택배:DE02]
            kiccpgparam.setDeli_corp_cd     (getNullToSpace(request.getParameter("deli_corp_cd")));        // [선택]택배사코드
            kiccpgparam.setDeli_invoice     (getNullToSpace(request.getParameter("deli_invoice")));        // [선택]운송장 번호
            kiccpgparam.setDeli_rcv_nm      (getNullToSpace(request.getParameter("deli_rcv_nm")));         // [선택]수령인 이름
            kiccpgparam.setDeli_rcv_tel     (getNullToSpace(request.getParameter("deli_rcv_tel")));        // [선택]수령인 연락처
            kiccpgparam.setReq_id           (getNullToSpace(request.getParameter("req_id")));              // [선택]가맹점 관리자 로그인 아이디
            kiccpgparam.setMgr_msg          (getNullToSpace(request.getParameter("mgr_msg")));             // [선택]변경 사유
*/        
            appFormBean.setReqKiccPgVo(kiccpgparam);

            //기본검증 및 결제필요정보를 가져온다.
            Hashtable ht = appService.appReqAction(appFormBean);
            
            strTpResult = (String)ht.get("RESULT_CD");
            strTpResultMsg = (String)ht.get("RESULT_MSG");

            
            //가맹점 정보
            AppOnffTidInfoVo appOnffTidInfo  = (AppOnffTidInfoVo)ht.get("onfftidInfo");
            
            //terminal정보
            AppTerminalInfoVo appTerminfo = (AppTerminalInfoVo)ht.get("appTidObj");
            
            
            if(strTpResult == null || !strTpResult.equals("0000"))
            {
                model.addAttribute("RESULT_CD",strTpResult);
                model.addAttribute("RESULT_MSG",strTpResultMsg);
                
                String strErrTid = null;
                if(appTerminfo != null)
                {
                    strErrTid = appTerminfo.getTerminal_no();
                    
                    if(strErrTid == null)
                    {
                        strErrTid = "XXXXXXX";
                    }
                }
                else
                {
                    strErrTid = "XXXXXXX";
                }
                
                appService.appErrCardTranSave(appFormBean, strErrTid, strTpResult, strTpResultMsg);
                
                return null;
            }
            
            //결제를 요청한다.(tid 종류에 따라)
            String strTidMtd= appTerminfo.getTid_mtd();
            
            Hashtable ht_appresult;
            //KICC PG일 경우
            if(strTidMtd.equals("KICCPG"))
            {
               logger.trace("----------------------------------------------------------------");
               logger.trace("AppControll appReqAction KICCPG.cert_file val : " + new String(getMessage("KICCPG.cert_file")));
               logger.trace("AppControll appReqAction KICCPG.cert_file val : " + new String(getMessage("KICCPG.g_log_dir")));
               logger.trace("AppControll appReqAction KICCPG.cert_file val : " + new String(getMessage("KICCPG.g_gw_url")));
               logger.trace("AppControll appReqAction KICCPG.cert_file val : " + new String(getMessage("KICCPG.g_gw_port")));
               logger.trace("----------------------------------------------------------------");                
                
               kiccpgparam.setKiccpg_cert_file( new String(getMessage("KICCPG.cert_file")));
               kiccpgparam.setKiccpg_g_log_dir( new String(getMessage("KICCPG.g_log_dir")));
               kiccpgparam.setKiccpg_g_gw_url( new String(getMessage("KICCPG.g_gw_url")));
               kiccpgparam.setKiccpg_g_gw_port( new String(getMessage("KICCPG.g_gw_port")));
               
               ht_appresult = appService.appKiccReqAction(appFormBean, appOnffTidInfo, appTerminfo);
               
               model.addAttribute("RESULT_CD",(String)ht_appresult.get("RESULT_CD"));
               model.addAttribute("RESULT_MSG",(String)ht_appresult.get("RESULT_MSG"));
               model.addAttribute("TRAN_SEQ", (String)ht_appresult.get("TRAN_SEQ"));
            }
            else
            {
                //throw new Exception("지원하지 않는 결제사 입니다.");
                model.addAttribute("RESULT_CD","OF03");
                model.addAttribute("RESULT_MSG","지원하지 않는 결제사 입니다.");
                model.addAttribute("TRAN_SEQ", "");
            }
            
        return null;
        
    }
    
    
    //결과페이지
    @RequestMapping(method= RequestMethod.GET,value = "/appResult")
    public String appResult(@Valid AppFormBean appFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        Hashtable ht_appresult;
        HttpSession session = request.getSession();
        SiteSession sessionobj = (SiteSession)session.getAttribute("siteSessionObj");
        String strUserCate = sessionobj.getSes_user_cate();

        appFormBean.setOnfftid(sessionobj.getSes_onfftid());
        appFormBean.setOnffmerch_no(sessionobj.getSes_onffmerch_no());
        appFormBean.setUser_cate(sessionobj.getSes_user_cate()); 
        appFormBean.setUser_seq(sessionobj.getUser_seq());
        
        if(appFormBean.getTran_seq() != null || appFormBean.getTran_seq().equals("") )
        {   
            ht_appresult = appService.appResult(appFormBean);
            
            Tb_Tran_CardPg tran_info_obj =(Tb_Tran_CardPg)ht_appresult.get("tran_info_obj");
            model.addAttribute("tran_info_obj", tran_info_obj);
            
            Tb_Company tbOnffCompInfo =(Tb_Company)ht_appresult.get("onffCompInfo");
            
            logger.trace(tbOnffCompInfo.getComp_nm());
            
            model.addAttribute("onffCompInfo", tbOnffCompInfo);
            
            String strTpCardNo = tran_info_obj.getCard_num();
            String strInputCardNo = "";
            if(!strTpCardNo.equals(""))
            {
		strInputCardNo = strTpCardNo.substring(0, 8) + "****" + strTpCardNo.substring(12 , strTpCardNo.length()); 
            }
            
             model.addAttribute("ViewCardNo", strInputCardNo);
             
            String strTpOnffBizNo = tbOnffCompInfo.getBiz_no();
            strTpOnffBizNo = strTpOnffBizNo.substring(0, 6) +  "****" ;
            
             model.addAttribute("ViewOnffBizNo", strTpOnffBizNo);
             
             NumberFormat nf = NumberFormat.getInstance();
             
             String strViewAmt = "";
             String strViewSvcAmt = "";
             
             String strTaxAmt = com.onoffkorea.system.common.util.Util.nullToInt((String)tran_info_obj.getTax_amt());
             String strTotAmt = com.onoffkorea.system.common.util.Util.nullToInt((String)tran_info_obj.getTot_amt());
             String strSvcAmt = com.onoffkorea.system.common.util.Util.nullToInt((String)tran_info_obj.getSvc_amt());
             logger.trace(" ctrl tran_info_obj.getSvc_amt() : "+ tran_info_obj.getSvc_amt());
             logger.trace(" ctrl strSvcAmt : "+ strSvcAmt);
             
             strViewAmt = nf.format( Integer.parseInt(strTotAmt) - Integer.parseInt(strSvcAmt) - Integer.parseInt(strTaxAmt));//과세승인금액
             strTaxAmt = nf.format( Integer.parseInt(strTaxAmt) );//부가세
             strTotAmt = nf.format( Integer.parseInt(strTotAmt) );//총금액
             strViewSvcAmt = nf.format( Integer.parseInt(strSvcAmt) );//비과세금액
             logger.trace(" ctrl strSvcAmt : "+ strSvcAmt);
             
             model.addAttribute("ViewSvcAmt", strViewSvcAmt);//비과세승인금액
             model.addAttribute("ViewAmt", strViewAmt);//과세승인금액
             model.addAttribute("ViewTotAmt", strTotAmt);//총금액
             model.addAttribute("ViewTaxAmt", strTaxAmt);//vat
        }
        
        model.addAttribute("tran_result_cd", appFormBean.getTran_result_cd());
        model.addAttribute("tran_result_msg", appFormBean.getTran_result_msg());
        
        return null;        
    }    
    
    //결과페이지
    @RequestMapping(method= RequestMethod.POST,value = "/appResult")
    public String appResultForm(@Valid AppFormBean appFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        Hashtable ht_appresult;
        HttpSession session = request.getSession();
        SiteSession sessionobj = (SiteSession)session.getAttribute("siteSessionObj");
        String strUserCate = sessionobj.getSes_user_cate();

        appFormBean.setOnfftid(sessionobj.getSes_onfftid());
        appFormBean.setOnffmerch_no(sessionobj.getSes_onffmerch_no());
        appFormBean.setUser_cate(sessionobj.getSes_user_cate());         
        appFormBean.setUser_seq(sessionobj.getUser_seq());
        
        if(appFormBean.getTran_seq() != null || appFormBean.getTran_seq().equals("") )
        {   
            if(appFormBean.getTran_result_cd() != null && appFormBean.getTran_result_cd().equals("0000"))
            {
                ht_appresult = appService.appResult(appFormBean);
                Tb_Tran_CardPg tran_info_obj =(Tb_Tran_CardPg)ht_appresult.get("tran_info_obj");
                model.addAttribute("tran_info_obj", tran_info_obj);
                
                Tb_Company tbOnffCompInfo =(Tb_Company)ht_appresult.get("onffCompInfo");
                
                logger.trace(tbOnffCompInfo.getComp_nm());
                model.addAttribute("onffCompInfo", tbOnffCompInfo);

                String strTpCardNo = tran_info_obj.getCard_num();
                String strInputCardNo = "";
                if(!strTpCardNo.equals(""))
                {
                    strInputCardNo = strTpCardNo.substring(0, 8) + "****" + strTpCardNo.substring(12 , strTpCardNo.length()); 
                }

                 model.addAttribute("ViewCardNo", strInputCardNo);
                 
                String strTpOnffBizNo = tbOnffCompInfo.getBiz_no();
                strTpOnffBizNo = strTpOnffBizNo.substring(0, 6) +  "****" ;

                 model.addAttribute("ViewOnffBizNo", strTpOnffBizNo);                 

                 NumberFormat nf = NumberFormat.getInstance();

                String strViewAmt = "";
                String strViewSvcAmt = "";

                String strTaxAmt = com.onoffkorea.system.common.util.Util.nullToInt((String)tran_info_obj.getTax_amt());
                String strTotAmt = com.onoffkorea.system.common.util.Util.nullToInt((String)tran_info_obj.getTot_amt());
                String strSvcAmt = com.onoffkorea.system.common.util.Util.nullToInt((String)tran_info_obj.getSvc_amt());
                logger.trace(" ctrl tran_info_obj.getSvc_amt() : "+ tran_info_obj.getSvc_amt());


                strViewAmt = nf.format( Integer.parseInt(strTotAmt) - Integer.parseInt(strSvcAmt) - Integer.parseInt(strTaxAmt));//과세승인금액
                strTaxAmt = nf.format( Integer.parseInt(strTaxAmt) );//부가세
                strTotAmt = nf.format( Integer.parseInt(strTotAmt) );//총금액
                strViewSvcAmt = nf.format( Integer.parseInt(strSvcAmt) );//비과세금액

                model.addAttribute("ViewSvcAmt", strViewSvcAmt);//비과세승인금액
                model.addAttribute("ViewAmt", strViewAmt);//과세승인금액
                model.addAttribute("ViewTotAmt", strTotAmt);//총금액
                model.addAttribute("ViewTaxAmt", strTaxAmt);//vat
            }
        }
        
        model.addAttribute("tran_result_cd", appFormBean.getTran_result_cd());
        model.addAttribute("tran_result_msg", appFormBean.getTran_result_msg());
        
        return null;        
    }
    

    //결제TID 선택
    @RequestMapping(method= RequestMethod.GET,value = "/appcashReady")
    public String appcashReady(@Valid AppFormBean appFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        HttpSession session = request.getSession();
        
        SiteSession sessionobj = (SiteSession)session.getAttribute("siteSessionObj");
        
        logger.trace("----------------------------------------------------------------");
        logger.trace("AppControll appselReady getOnfftid session val : " + sessionobj.getSes_onfftid());
        logger.trace("AppControll appselReady getOnffmerch_no session val : " + sessionobj.getSes_onffmerch_no());
        logger.trace("AppControll appselReady getUser_cate session val : " + sessionobj.getSes_user_cate());      
        logger.trace("----------------------------------------------------------------");
          
        appFormBean.setOnfftid(sessionobj.getSes_onfftid());
        appFormBean.setOnffmerch_no(sessionobj.getSes_onffmerch_no());
        appFormBean.setUser_cate(sessionobj.getSes_user_cate());
        appFormBean.setUser_seq(sessionobj.getUser_seq());
        
        Hashtable ht = appService.appselReady(appFormBean);
        
        logger.trace("----------------------------------------------------------------");
        logger.trace("AppControll appselReady onfftid_info  result: " + (String)ht.get("onfftid_info"));
        logger.trace("AppControll appselReady onffmerch_info resulrt : " + (String)ht.get("onffmerch_info"));
        logger.trace("----------------------------------------------------------------");
        
        model.addAttribute("onfftid_info",(String)ht.get("onfftid_info"));
        model.addAttribute("onffmerch_info",(String)ht.get("onffmerch_info"));            
        
        return null;
        
    }
    
    

    //결제TID 선택
    @RequestMapping(method= RequestMethod.GET,value = "/basic_appcashReady")
    public String basic_appcashReady(@Valid AppFormBean appFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        HttpSession session = request.getSession();
        
        SiteSession sessionobj = (SiteSession)session.getAttribute("siteSessionObj");
        
        logger.trace("----------------------------------------------------------------");
        logger.trace("AppControll appselReady getOnfftid session val : " + sessionobj.getSes_onfftid());
        logger.trace("AppControll appselReady getOnffmerch_no session val : " + sessionobj.getSes_onffmerch_no());
        logger.trace("AppControll appselReady getUser_cate session val : " + sessionobj.getSes_user_cate());      
        logger.trace("----------------------------------------------------------------");
          
        appFormBean.setOnfftid(sessionobj.getSes_onfftid());
        appFormBean.setOnffmerch_no(sessionobj.getSes_onffmerch_no());
        appFormBean.setUser_cate(sessionobj.getSes_user_cate());
        appFormBean.setUser_seq(sessionobj.getUser_seq());
        
        Hashtable ht = appService.appselReady(appFormBean);
        
        logger.trace("----------------------------------------------------------------");
        logger.trace("AppControll appselReady onfftid_info  result: " + (String)ht.get("onfftid_info"));
        logger.trace("AppControll appselReady onffmerch_info resulrt : " + (String)ht.get("onffmerch_info"));
        logger.trace("----------------------------------------------------------------");
        
        model.addAttribute("onfftid_info",(String)ht.get("onfftid_info"));
        model.addAttribute("onffmerch_info",(String)ht.get("onffmerch_info"));            
        
        return null;
        
    }    
    
    
    //결제내역 입력
    @RequestMapping(method= RequestMethod.GET,value = "/appCashReqForm")
    public String appCashReqForm(@Valid AppFormBean appFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        
        HttpSession session = request.getSession();
        
        SiteSession sessionobj = (SiteSession)session.getAttribute("siteSessionObj");
        
        logger.trace("----------------------------------------------------------------");
        logger.trace("AppControll appReqForm getOnfftid session val : " + sessionobj.getSes_onfftid());
        logger.trace("AppControll appReqForm getOnffmerch_no session val : " + sessionobj.getSes_onffmerch_no());
        logger.trace("AppControll appReqForm getUser_cate session val : " + sessionobj.getSes_user_cate());      
        
        logger.trace("AppControll appReqForm appFormBean.getParam_onffmerch_no val : " + appFormBean.getParam_onffmerch_no());      
        logger.trace("AppControll appReqForm appFormBean.getParam_onfftid val : " + appFormBean.getParam_onfftid());      

        String strUserCate = sessionobj.getSes_user_cate();

        appFormBean.setOnfftid(sessionobj.getSes_onfftid());
        appFormBean.setOnffmerch_no(sessionobj.getSes_onffmerch_no());
        appFormBean.setUser_cate(sessionobj.getSes_user_cate());   
        appFormBean.setUser_seq(sessionobj.getUser_seq());
        
        Hashtable ht = appService.appReqForm(appFormBean);
        
        AppOnffTidInfoVo onfftid_info = (AppOnffTidInfoVo)ht.get("onfftid_info");
        //String strOrderSeq = (String)ht.get("order_seq");
        
        model.addAttribute("onfftid_info",onfftid_info);
        //model.addAttribute("orderseq",strOrderSeq);
        
        logger.trace("AppControll appReqForm (String)ht.get(result_code)d val : " + (String)ht.get("result_code"));   
        
        logger.trace("----------------------------------------------------------------");
        
        model.addAttribute("result_code",(String)ht.get("result_code"));
        model.addAttribute("result_msg",(String)ht.get("result_msg"));

        return null;        
    }
    

    //결제요청 
    @RequestMapping(method= RequestMethod.POST,value = "/appCashHiddenAction")
    @ResponseBody
    public AppMgrResultVo appCashHiddenReqAction(@Valid AppFormBean appFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        AppMgrResultVo resultobj = new AppMgrResultVo();
        
        String strTpResult = null;
        String strTpResultMsg = null;
        

        HttpSession session = request.getSession();
        
        SiteSession sessionobj = (SiteSession)session.getAttribute("siteSessionObj");

        String strUserCate = sessionobj.getSes_user_cate();

        appFormBean.setOnfftid(sessionobj.getSes_onfftid());
        appFormBean.setOnffmerch_no(sessionobj.getSes_onffmerch_no());
        appFormBean.setUser_cate(sessionobj.getSes_user_cate());    
        appFormBean.setUser_seq(sessionobj.getUser_seq());
        
        
            /* -------------------------------------------------------------------------- */
            /* ::: 처리구분 설정                                                          */
            /* -------------------------------------------------------------------------- */
            KiccPgVo kiccpgparam = new KiccPgVo();
        
            kiccpgparam.setISSUE("issue");//발행
            kiccpgparam.setCANCL("cancel");//취소
        
            kiccpgparam.setTr_cd            (getNullToSpace(request.getParameter("EP_tr_cd")));           // [필수]요청구분
            kiccpgparam.setPay_type         (getNullToSpace(request.getParameter("EP_pay_type")));        // [필수]결제수단
            kiccpgparam.setReq_type         (getNullToSpace(request.getParameter("EP_req_type")));       // 요청타입


            /* -------------------------------------------------------------------------- */
            /* ::: 현금영수증 정보 설정                                                   */
            /* -------------------------------------------------------------------------- */
            kiccpgparam.setCash_issue_type   (getNullToSpace(request.getParameter("EP_issue_type"))); // [필수]현금영수증발행용도
            kiccpgparam.setCash_auth_type    (getNullToSpace(request.getParameter("EP_auth_type")));  // [필수]인증구분
            kiccpgparam.setCash_auth_value   (getNullToSpace(request.getParameter("EP_auth_value"))); // [필수]인증번호
            
            kiccpgparam.setSub_mall_yn(getNullToSpace(request.getParameter("EP_sub_mall_yn")));  // [필수]하위가맹점사용여부
            kiccpgparam.setSub_mall_buss(getNullToSpace(request.getParameter("EP_sub_mall_buss"))); // [필수]하위가맹점사업자번호
            
            kiccpgparam.setTot_amt          (getNullToZero(request.getParameter("EP_tot_amt")));          // [필수]결제 총 금액
            kiccpgparam.setCom_free_amt     (getNullToZero(request.getParameter("EP_service_amt")));     // [필수]비과세 승인 금액(봉사료)
            kiccpgparam.setCom_vat_amt      (getNullToZero(request.getParameter("EP_vat")));      // [필수]부가세 금액
            
            
            /* -------------------------------------------------------------------------- */
            /* ::: 결제 주문 정보 설정                                                    */
            /* -------------------------------------------------------------------------- */
            kiccpgparam.setUser_type        (getNullToSpace(request.getParameter("EP_user_type")));        // [선택]사용자구분구분[1:일반,2:회원]
            kiccpgparam.setOrder_no         (getNullToSpace(request.getParameter("EP_order_no")));         // [필수]주문번호
            kiccpgparam.setMemb_user_no     (getNullToSpace(request.getParameter("EP_memb_user_no")));     // [선택]가맹점 고객일련번호
            kiccpgparam.setUser_id          (getNullToSpace(request.getParameter("EP_user_id")));          // [선택]고객 ID
            kiccpgparam.setUser_nm          (getNullToSpace(request.getParameter("EP_user_nm")));          // [필수]고객명
            kiccpgparam.setUser_mail        (getNullToSpace(request.getParameter("EP_user_mail")));        // [필수]고객 E-mail
            kiccpgparam.setUser_phone1      (getNullToSpace(request.getParameter("EP_user_phone1")));      // [필수]가맹점 고객 연락처1
            kiccpgparam.setUser_phone2      (getNullToSpace(request.getParameter("EP_user_phone2")));      // [선택]가맹점 고객 연락처2
            kiccpgparam.setUser_addr        (getNullToSpace(request.getParameter("EP_user_addr")));        // [선택]가맹점 고객 주소
            kiccpgparam.setProduct_type     (getNullToSpace(request.getParameter("EP_product_type")));     // [필수]상품정보구분[0:실물,1:컨텐츠]
            kiccpgparam.setProduct_nm       (getNullToSpace(request.getParameter("EP_product_nm")));       // [필수]상품명
            kiccpgparam.setProduct_amt      (getNullToSpace(request.getParameter("EP_product_amt")));      // [필수]상품금액
            kiccpgparam.setUser_define1     (getNullToSpace(request.getParameter("EP_user_define1")));     // [선택]가맹점필드1
            kiccpgparam.setUser_define2     (getNullToSpace(request.getParameter("EP_user_define2")));     // [선택]가맹점필드2
            kiccpgparam.setUser_define3     (getNullToSpace(request.getParameter("EP_user_define3")));     // [선택]가맹점필드3
            kiccpgparam.setUser_define4     (getNullToSpace(request.getParameter("EP_user_define4")));     // [선택]가맹점필드4
            kiccpgparam.setUser_define5     (getNullToSpace(request.getParameter("EP_user_define5")));     // [선택]가맹점필드5
            kiccpgparam.setUser_define6     (getNullToSpace(request.getParameter("EP_user_define6")));     // [선택]가맹점필드6

            /* -------------------------------------------------------------------------- */
            /* ::: 기타정보 설정                                                          */
            /* -------------------------------------------------------------------------- */
            kiccpgparam.setClient_ip        (request.getRemoteAddr());                                     // [필수]결제고객 IP
            
            logger.trace("----------------------------------------------------------------");
            logger.trace("AppControll appCashReqAction getRemoteAddr val : " + request.getRemoteAddr());
            logger.trace("AppControll appCashReqAction getTot_amt val : " + kiccpgparam.getTot_amt());
            logger.trace("AppControll appCashReqAction getCom_free_amt val : " + kiccpgparam.getCom_free_amt() );
            logger.trace("AppControll appCashReqAction getCom_vat_amt val : " + kiccpgparam.getCom_vat_amt() );
            
            logger.trace("AppControll appCashReqAction getCash_issue_type val : " + kiccpgparam.getCash_issue_type() );
            logger.trace("AppControll appCashReqAction getCash_auth_type val : " + kiccpgparam.getCash_auth_type());
            logger.trace("AppControll appCashReqAction getCash_auth_value val : " + kiccpgparam.getCash_auth_value());
            
            
            logger.trace("AppControll appCashReqAction getSub_mall_buss val : " + kiccpgparam.getSub_mall_yn());
            logger.trace("AppControll appCashReqAction getSub_mall_buss val : " + kiccpgparam.getSub_mall_buss());
            logger.trace("----------------------------------------------------------------");           
            

            appFormBean.setReqKiccPgVo(kiccpgparam);
            
            
            //kcp 정보를 설정한다.
            KcpPgVo kcppgparam = new KcpPgVo();
          
            //kcppgparam.setPay_method("CARD");//결제방법card고정
            kcppgparam.setOrdr_idxx(getNullToSpace(request.getParameter("EP_order_no")));//주문번호
            kcppgparam.setPay_type("PAXX");
            
            kcppgparam.setReq_tx("pay");
            
            //사업자정보
            kcppgparam.setCorp_type("1");//사업장구분 입점몰 판매로 고정
            kcppgparam.setCorp_tax_type("TG01");//과세로 고정
            kcppgparam.setCorp_tax_no(getNullToSpace(request.getParameter("EP_sub_mall_buss")));//발행사업자번호
            kcppgparam.setCorp_sell_tax_no(getNullToSpace(request.getParameter("EP_sub_mall_buss")));//발행사업자번호
            
            //인증정보
            String strTpTmpTrcd = getNullToSpace(request.getParameter("EP_issue_type"));//(01 소득공제, 02 지출증빙, 03 자진발급)
            if(strTpTmpTrcd.equals("01"))
            {
                kcppgparam.setTr_code("0");//발행구분
                kcppgparam.setId_info(getNullToSpace(request.getParameter("EP_auth_value")));//인증값
            }
            else if(strTpTmpTrcd.equals("02"))
            {
                kcppgparam.setTr_code("1");//발행구분
                kcppgparam.setId_info(getNullToSpace(request.getParameter("EP_auth_value")));//인증값
            }
            else  if(strTpTmpTrcd.equals("03"))
            {
                kcppgparam.setTr_code("0");//발행구분
                kcppgparam.setId_info(getNullToSpace(request.getParameter("EP_auth_value")));//인증값
            }
            
            
            //금액정보
            kcppgparam.setAmt_tot(getNullToZero(request.getParameter("EP_tot_amt")));
            long lntpsupamt = Long.parseLong(getNullToZero(request.getParameter("EP_tot_amt")))-Long.parseLong(getNullToZero(request.getParameter("EP_service_amt")))-Long.parseLong(getNullToZero(request.getParameter("EP_vat")));
            kcppgparam.setAmt_sup(String.valueOf(lntpsupamt));
            kcppgparam.setAmt_svc(getNullToZero(request.getParameter("EP_service_amt")));
            kcppgparam.setAmt_tax(getNullToZero(request.getParameter("EP_vat")));
            
            //상품정보
            kcppgparam.setGood_name (getNullToSpace(request.getParameter("EP_product_nm")));
            kcppgparam.setGood_mny  (getNullToSpace(request.getParameter("EP_tot_amt")));//결제금액
            kcppgparam.setBuyr_name (getNullToSpace(request.getParameter("EP_user_nm")));//주문자이름
            kcppgparam.setBuyr_mail (getNullToSpace(request.getParameter("EP_user_mail")));//주문자email
            kcppgparam.setBuyr_tel1 (getNullToSpace(request.getParameter("EP_user_phone2")));//주문자전화
            kcppgparam.setBuyr_tel2 (getNullToSpace(request.getParameter("EP_user_phone2")));//주문자휴대전화
             
            
            kcppgparam.setTx_cd("07010000");//현금영수증 승인
            kcppgparam.setCust_ip(request.getRemoteAddr());            
            
            appFormBean.setReqKcpPgVo(kcppgparam);
            
            //allat 정보를 설정한다.
            AllatPgVo allatparam = new AllatPgVo();            
            //allatparam.setAllat_shop_id       ();
            //allatparam.setAllat_apply_ymdhms  ();//거래요청일자
            allatparam.setAllat_shop_member_id(getNullToSpace(request.getParameter("EP_user_nm")));//고객ID
            allatparam.setAllat_cert_no       (getNullToSpace(request.getParameter("EP_auth_value")));//인증번호
            allatparam.setAllat_supply_amt    (String.valueOf(lntpsupamt));//공급가액
            allatparam.setAllat_vat_amt       (getNullToZero(request.getParameter("EP_vat")));//VAT
            allatparam.setAllat_receipt_type  ("NBANK");//현금영수증구분(최대6자):계좌이체(ABANK),무통장(NBANK)
            allatparam.setAllat_product_nm    (getNullToSpace(request.getParameter("EP_product_nm")));
            allatparam.setAllat_reg_business_no(getNullToSpace(request.getParameter("EP_sub_mall_buss")));
            allatparam.setAllat_buyer_ip      (request.getRemoteAddr());
            //allatparam.setAllat_test_yn       ("");
            
            appFormBean.setReqAllatPgVo(allatparam);
            
            //기본검증 및 결제필요정보를 가져온다.
            Hashtable ht = appService.appReqCashAction(appFormBean);
            
            strTpResult = (String)ht.get("RESULT_CD");
            strTpResultMsg = (String)ht.get("RESULT_MSG");
            
            //가맹점 정보
            AppOnffTidInfoVo appOnffTidInfo  = (AppOnffTidInfoVo)ht.get("onfftidInfo");
            
            //terminal정보
            AppTerminalInfoVo appTerminfo = (AppTerminalInfoVo)ht.get("appTidObj");            
            
            if(strTpResult == null || !strTpResult.equals("0000"))
            {
                //model.addAttribute("RESULT_CD",strTpResult);
                //model.addAttribute("RESULT_CD",strTpResultMsg);
                //return null;
                resultobj.setResult_cd(strTpResult);
                resultobj.setResult_msg(strTpResultMsg);
                
                String strErrTid = null;
                if(appTerminfo != null)
                {
                    strErrTid = appTerminfo.getTerminal_no();
                    
                    if(strErrTid == null)
                    {
                        strErrTid = "XXXXXXX";
                    }
                }
                else
                {
                    strErrTid = "XXXXXXX";
                }
                
                appService.appErrCashTranSave(appFormBean, strErrTid, strTpResult, strTpResultMsg);
                
                return resultobj;
            }
            
            //결제를 요청한다.(tid 종류에 따라)
            String strTidMtd= appTerminfo.getTid_mtd();
            
            logger.trace("strTidMtd : " + strTidMtd);
            
            Hashtable ht_appresult;
            //KICC PG일 경우
            if(strTidMtd.equals("KICCPG"))
            {
               logger.trace("---------------------------KICC CASH-------------------------------------");
               logger.trace("AppControll appReqAction KICCPG.cert_file val : " + new String(getMessage("KICCPG.cert_file")));
               logger.trace("AppControll appReqAction KICCPG.cert_file val : " + new String(getMessage("KICCPG.g_log_dir")));
               logger.trace("AppControll appReqAction KICCPG.cert_file val : " + new String(getMessage("KICCPG.g_gw_url")));
               logger.trace("AppControll appReqAction KICCPG.cert_file val : " + new String(getMessage("KICCPG.g_gw_port")));
               logger.trace("----------------------------------------------------------------");                
                
               kiccpgparam.setKiccpg_cert_file( new String(getMessage("KICCPG.cert_file")));
               kiccpgparam.setKiccpg_g_log_dir( new String(getMessage("KICCPG.g_log_dir")));
               kiccpgparam.setKiccpg_g_gw_url( new String(getMessage("KICCPG.g_gw_url")));
               kiccpgparam.setKiccpg_g_gw_port( new String(getMessage("KICCPG.g_gw_port")));
               
               ht_appresult = appService.appKiccCashReqAction(appFormBean, appOnffTidInfo, appTerminfo);
               
               /*
               model.addAttribute("RESULT_CD",(String)ht_appresult.get("RESULT_CD"));
               model.addAttribute("RESULT_MSG",(String)ht_appresult.get("RESULT_MSG"));
               model.addAttribute("TRAN_SEQ", (String)ht_appresult.get("TRAN_SEQ"));
               */
                resultobj.setResult_cd((String)ht_appresult.get("RESULT_CD"));
                resultobj.setResult_msg((String)ht_appresult.get("RESULT_MSG"));

            }
            else if(strTidMtd.equals("KCPPG"))
            {
                logger.trace("-------------------------------KCP CASH---------------------------------");
               logger.trace("AppControll appReqAction KCPPG.cert_file val : " + new String(getMessage("KICCPG.cert_file")));
               logger.trace("AppControll appReqAction KCPPG.cert_file val : " + new String(getMessage("KICCPG.g_log_dir")));
               logger.trace("AppControll appReqAction KCPPG.cert_file val : " + new String(getMessage("KICCPG.g_gw_url")));
               logger.trace("AppControll appReqAction KCPPG.cert_file val : " + new String(getMessage("KICCPG.g_gw_port")));
               logger.trace("----------------------------------------------------------------");                
               
               kcppgparam.setG_conf_home_dir(new String(getMessage("g_conf_home_dir")));
               kcppgparam.setG_conf_key_dir(new String(getMessage("g_conf_key_dir")));
               kcppgparam.setG_conf_log_dir(new String(getMessage("g_conf_log_dir")));
               kcppgparam.setG_conf_log_level(new String(getMessage("g_conf_log_level")));
               kcppgparam.setG_conf_gw_port(new String(getMessage("g_conf_gw_port")));
               kcppgparam.setG_conf_tx_mode(new String(getMessage("g_conf_tx_mode")));
               kcppgparam.setG_conf_site_name(new String(getMessage("g_conf_site_name")));
               kcppgparam.setG_conf_gw_url(new String(getMessage("g_conf_gw_url")));
               kcppgparam.setG_conf_user_type(new String(getMessage("g_conf_user_type")));
               
               ht_appresult = appService.appKcpCashReqAction(appFormBean, appOnffTidInfo, appTerminfo);
               
                resultobj.setResult_cd((String)ht_appresult.get("RESULT_CD"));
                resultobj.setResult_msg((String)ht_appresult.get("RESULT_MSG"));
            }
            else if(strTidMtd.equals("ALLATPG"))
            {
               logger.trace("-------------------------------alltat CASH---------------------------------");
               
               String strTestFlag =  com.onoffkorea.system.common.util.Util.nullToString(new String(getMessage("ALLAT_TESTFLAG")));
               
               logger.debug("strTestFlag : " + strTestFlag);
                
               if(strTestFlag.equals("N"))
               {
                   allatparam.setAllat_test_yn("N"); 
               }
               else
               {
                   allatparam.setAllat_test_yn("Y"); 
               }               
               
               logger.debug("allatparam.getAllat_test_yn : " + allatparam.getAllat_test_yn());  
               
               ht_appresult = appService.appAllatCashReqAction(appFormBean, appOnffTidInfo, appTerminfo);
               
                resultobj.setResult_cd((String)ht_appresult.get("RESULT_CD"));
                resultobj.setResult_msg((String)ht_appresult.get("RESULT_MSG"));
            }            
            else
            {
                //throw new Exception("지원하지 않는 결제사 입니다.");
                /*
                model.addAttribute("RESULT_CD","OF03");
                model.addAttribute("RESULT_MSG","지원하지 않는 결제사 입니다.");
                model.addAttribute("TRAN_SEQ", "");
                */
                
                resultobj.setResult_cd("OF03");
                resultobj.setResult_msg("지원하지 않는 결제사 입니다.");
            }
            
        return resultobj;
        
    }        
    
    //결제요청 
    @RequestMapping(method= RequestMethod.POST,value = "/appCashAction")
    public String appCashReqAction(@Valid AppFormBean appFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        String strTpResult = null;
        String strTpResultMsg = null;
        
        HttpSession session = request.getSession();
        
        SiteSession sessionobj = (SiteSession)session.getAttribute("siteSessionObj");

        String strUserCate = sessionobj.getSes_user_cate();

        appFormBean.setOnfftid(sessionobj.getSes_onfftid());
        appFormBean.setOnffmerch_no(sessionobj.getSes_onffmerch_no());
        appFormBean.setUser_cate(sessionobj.getSes_user_cate());    
        appFormBean.setUser_seq(sessionobj.getUser_seq());
        
        
            /* -------------------------------------------------------------------------- */
            /* ::: 처리구분 설정                                                          */
            /* -------------------------------------------------------------------------- */
            KiccPgVo kiccpgparam = new KiccPgVo();
        
            kiccpgparam.setISSUE("issue");//발행
            kiccpgparam.setCANCL("cancel");//취소
        
            kiccpgparam.setTr_cd            (getNullToSpace(request.getParameter("EP_tr_cd")));           // [필수]요청구분
            kiccpgparam.setPay_type         (getNullToSpace(request.getParameter("EP_pay_type")));        // [필수]결제수단
            kiccpgparam.setReq_type         ( getNullToSpace(request.getParameter("EP_req_type")));       // 요청타입


            /* -------------------------------------------------------------------------- */
            /* ::: 현금영수증 정보 설정                                                   */
            /* -------------------------------------------------------------------------- */
            kiccpgparam.setCash_issue_type   (getNullToSpace(request.getParameter("EP_issue_type"))); // [필수]현금영수증발행용도
            kiccpgparam.setCash_auth_type    (getNullToSpace(request.getParameter("EP_auth_type")));  // [필수]인증구분
            kiccpgparam.setCash_auth_value   (getNullToSpace(request.getParameter("EP_auth_value"))); // [필수]인증번호
            
            kiccpgparam.setSub_mall_yn(getNullToSpace(request.getParameter("EP_sub_mall_yn")));  // [필수]하위가맹점사용여부
            kiccpgparam.setSub_mall_buss(getNullToSpace(request.getParameter("EP_sub_mall_buss"))); // [필수]하위가맹점사업자번호
            
            kiccpgparam.setTot_amt          (getNullToZero(request.getParameter("EP_tot_amt")));          // [필수]결제 총 금액
            kiccpgparam.setCom_free_amt     (getNullToZero(request.getParameter("EP_service_amt")));     // [필수]비과세 승인 금액(봉사료)
            kiccpgparam.setCom_vat_amt      (getNullToZero(request.getParameter("EP_vat")));      // [필수]부가세 금액
            
            
            /* -------------------------------------------------------------------------- */
            /* ::: 결제 주문 정보 설정                                                    */
            /* -------------------------------------------------------------------------- */
            kiccpgparam.setUser_type        (getNullToSpace(request.getParameter("EP_user_type")));        // [선택]사용자구분구분[1:일반,2:회원]
            kiccpgparam.setOrder_no         (getNullToSpace(request.getParameter("EP_order_no")));         // [필수]주문번호
            kiccpgparam.setMemb_user_no     (getNullToSpace(request.getParameter("EP_memb_user_no")));     // [선택]가맹점 고객일련번호
            kiccpgparam.setUser_id          (getNullToSpace(request.getParameter("EP_user_id")));          // [선택]고객 ID
            kiccpgparam.setUser_nm          (getNullToSpace(request.getParameter("EP_user_nm")));          // [필수]고객명
            kiccpgparam.setUser_mail        (getNullToSpace(request.getParameter("EP_user_mail")));        // [필수]고객 E-mail
            kiccpgparam.setUser_phone1      (getNullToSpace(request.getParameter("EP_user_phone1")));      // [필수]가맹점 고객 연락처1
            kiccpgparam.setUser_phone2      (getNullToSpace(request.getParameter("EP_user_phone2")));      // [선택]가맹점 고객 연락처2
            kiccpgparam.setUser_addr        (getNullToSpace(request.getParameter("EP_user_addr")));        // [선택]가맹점 고객 주소
            kiccpgparam.setProduct_type     (getNullToSpace(request.getParameter("EP_product_type")));     // [필수]상품정보구분[0:실물,1:컨텐츠]
            kiccpgparam.setProduct_nm       (getNullToSpace(request.getParameter("EP_product_nm")));       // [필수]상품명
            kiccpgparam.setProduct_amt      (getNullToSpace(request.getParameter("EP_product_amt")));      // [필수]상품금액
            kiccpgparam.setUser_define1     (getNullToSpace(request.getParameter("EP_user_define1")));     // [선택]가맹점필드1
            kiccpgparam.setUser_define2     (getNullToSpace(request.getParameter("EP_user_define2")));     // [선택]가맹점필드2
            kiccpgparam.setUser_define3     (getNullToSpace(request.getParameter("EP_user_define3")));     // [선택]가맹점필드3
            kiccpgparam.setUser_define4     (getNullToSpace(request.getParameter("EP_user_define4")));     // [선택]가맹점필드4
            kiccpgparam.setUser_define5     (getNullToSpace(request.getParameter("EP_user_define5")));     // [선택]가맹점필드5
            kiccpgparam.setUser_define6     (getNullToSpace(request.getParameter("EP_user_define6")));     // [선택]가맹점필드6

            /* -------------------------------------------------------------------------- */
            /* ::: 기타정보 설정                                                          */
            /* -------------------------------------------------------------------------- */
            kiccpgparam.setClient_ip        (request.getRemoteAddr());                                     // [필수]결제고객 IP
            
            logger.trace("----------------------------------------------------------------");
            logger.trace("AppControll appCashReqAction getRemoteAddr val : " + request.getRemoteAddr());
            logger.trace("AppControll appCashReqAction getTot_amt val : " + kiccpgparam.getTot_amt());
            logger.trace("AppControll appCashReqAction getCom_free_amt val : " + kiccpgparam.getCom_free_amt() );
            logger.trace("AppControll appCashReqAction getCom_vat_amt val : " + kiccpgparam.getCom_vat_amt() );
            
            logger.trace("AppControll appCashReqAction getCash_issue_type val : " + kiccpgparam.getCash_issue_type() );
            logger.trace("AppControll appCashReqAction getCash_auth_type val : " + kiccpgparam.getCash_auth_type());
            logger.trace("AppControll appCashReqAction getCash_auth_value val : " + kiccpgparam.getCash_auth_value());
            
            
            logger.trace("AppControll appCashReqAction getSub_mall_buss val : " + kiccpgparam.getSub_mall_yn());
            logger.trace("AppControll appCashReqAction getSub_mall_buss val : " + kiccpgparam.getSub_mall_buss());
            logger.trace("----------------------------------------------------------------");           
            

            appFormBean.setReqKiccPgVo(kiccpgparam);

            //기본검증 및 결제필요정보를 가져온다.
            Hashtable ht = appService.appReqCashAction(appFormBean);
            
            strTpResult = (String)ht.get("RESULT_CD");
            strTpResultMsg = (String)ht.get("RESULT_MSG");
            
            //가맹점 정보
            AppOnffTidInfoVo appOnffTidInfo  = (AppOnffTidInfoVo)ht.get("onfftidInfo");
            
            //terminal정보
            AppTerminalInfoVo appTerminfo = (AppTerminalInfoVo)ht.get("appTidObj");
            
            if(strTpResult == null || !strTpResult.equals("0000"))
            {
                model.addAttribute("RESULT_CD",strTpResult);
                model.addAttribute("RESULT_MSG",strTpResultMsg);
                
                String strErrTid = null;
                if(appTerminfo != null)
                {
                    strErrTid = appTerminfo.getTerminal_no();
                    
                    if(strErrTid == null)
                    {
                        strErrTid = "XXXXXXX";
                    }
                }
                else
                {
                    strErrTid = "XXXXXXX";
                }
                
                appService.appErrCashTranSave(appFormBean, strErrTid, strTpResult, strTpResultMsg);
                
                return null;
            }
            

            
            //결제를 요청한다.(tid 종류에 따라)
            String strTidMtd= appTerminfo.getTid_mtd();
            
            Hashtable ht_appresult;
            //KICC PG일 경우
            if(strTidMtd.equals("KICCPG"))
            {
               logger.trace("----------------------------------------------------------------");
               logger.trace("AppControll appReqAction KICCPG.cert_file val : " + new String(getMessage("KICCPG.cert_file")));
               logger.trace("AppControll appReqAction KICCPG.cert_file val : " + new String(getMessage("KICCPG.g_log_dir")));
               logger.trace("AppControll appReqAction KICCPG.cert_file val : " + new String(getMessage("KICCPG.g_gw_url")));
               logger.trace("AppControll appReqAction KICCPG.cert_file val : " + new String(getMessage("KICCPG.g_gw_port")));
               logger.trace("----------------------------------------------------------------");                
                
               kiccpgparam.setKiccpg_cert_file( new String(getMessage("KICCPG.cert_file")));
               kiccpgparam.setKiccpg_g_log_dir( new String(getMessage("KICCPG.g_log_dir")));
               kiccpgparam.setKiccpg_g_gw_url( new String(getMessage("KICCPG.g_gw_url")));
               kiccpgparam.setKiccpg_g_gw_port( new String(getMessage("KICCPG.g_gw_port")));
               
               ht_appresult = appService.appKiccCashReqAction(appFormBean, appOnffTidInfo, appTerminfo);
               
               model.addAttribute("RESULT_CD",(String)ht_appresult.get("RESULT_CD"));
               model.addAttribute("RESULT_MSG",(String)ht_appresult.get("RESULT_MSG"));
               model.addAttribute("TRAN_SEQ", (String)ht_appresult.get("TRAN_SEQ"));
            }
            else
            {
                //throw new Exception("지원하지 않는 결제사 입니다.");
                model.addAttribute("RESULT_CD","9003");
                model.addAttribute("RESULT_MSG","지원하지 않는 결제사 입니다.");
                model.addAttribute("TRAN_SEQ", "");
            }
            
        return null;
        
    }    
    
        
    //결과페이지
    @RequestMapping(method= RequestMethod.GET,value = "/appCashResult")
    public String appCashResult(@Valid AppFormBean appFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        Hashtable ht_appresult;
        
        HttpSession session = request.getSession();
        SiteSession sessionobj = (SiteSession)session.getAttribute("siteSessionObj");
        String strUserCate = sessionobj.getSes_user_cate();

        appFormBean.setOnfftid(sessionobj.getSes_onfftid());
        appFormBean.setOnffmerch_no(sessionobj.getSes_onffmerch_no());
        appFormBean.setUser_cate(sessionobj.getSes_user_cate()); 
        appFormBean.setUser_seq(sessionobj.getUser_seq());
        
        
        if(appFormBean.getTran_seq() != null || appFormBean.getTran_seq().equals("") )
        {               
            ht_appresult = appService.appCashResult(appFormBean);
            
            Tb_Tran_CardPg tran_info_obj =(Tb_Tran_CardPg)ht_appresult.get("tran_info_obj");
            model.addAttribute("tran_info_obj", tran_info_obj);
            
            Tb_Company tbOnffCompInfo =(Tb_Company)ht_appresult.get("onffCompInfo");
            model.addAttribute("onffCompInfo", tbOnffCompInfo);
             
             NumberFormat nf = NumberFormat.getInstance();
             
             String strViewTotAmt = "";
             String strViewSvcAmt = "";
             String strViewTaxAmt = "";
             
             String strTaxAmt = com.onoffkorea.system.common.util.Util.nullToInt((String)tran_info_obj.getTax_amt());
             String strTotAmt = com.onoffkorea.system.common.util.Util.nullToInt((String)tran_info_obj.getTot_amt());
             String strSvcAmt = com.onoffkorea.system.common.util.Util.nullToInt((String)tran_info_obj.getSvc_amt());
             
             String strTaxTargetAmt = String.valueOf(Integer.parseInt(strTotAmt) -  Integer.parseInt(strSvcAmt) -  Integer.parseInt(strTaxAmt));

             strViewTotAmt = nf.format( Integer.parseInt(strTotAmt) );
             strViewSvcAmt = nf.format( Integer.parseInt(strSvcAmt) );
             strViewTaxAmt = nf.format( Integer.parseInt(strTaxAmt) );
             strTaxTargetAmt = nf.format( Integer.parseInt(strTaxTargetAmt) );
             

             //model.addAttribute("ViewTotAmt", strTotAmt);
             //model.addAttribute("ViewTaxAmt", strTaxAmt);
             model.addAttribute("ViewTotAmt", strViewTotAmt);
             model.addAttribute("ViewTaxAmt", strViewTaxAmt);
             model.addAttribute("ViewSvcAmt", strViewSvcAmt);
             model.addAttribute("ViewTaxTargetAmt", strTaxTargetAmt); 
             
             String strTpOnffBizNo = tbOnffCompInfo.getBiz_no();
             strTpOnffBizNo = strTpOnffBizNo.substring(0, 6) +  "****" ;
            
             model.addAttribute("ViewOnffBizNo", strTpOnffBizNo);
        }
        
        model.addAttribute("tran_result_cd", appFormBean.getTran_result_cd());
        model.addAttribute("tran_result_msg", appFormBean.getTran_result_msg());
        
        return null;        
    }    
    
    //결과페이지
    @RequestMapping(method= RequestMethod.POST,value = "/appCashResult")
    public String appCashResultForm(@Valid AppFormBean appFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        Hashtable ht_appresult;
        
        HttpSession session = request.getSession();
        SiteSession sessionobj = (SiteSession)session.getAttribute("siteSessionObj");
        String strUserCate = sessionobj.getSes_user_cate();

        appFormBean.setOnfftid(sessionobj.getSes_onfftid());
        appFormBean.setOnffmerch_no(sessionobj.getSes_onffmerch_no());
        appFormBean.setUser_cate(sessionobj.getSes_user_cate()); 
        appFormBean.setUser_seq(sessionobj.getUser_seq());        
        
        if(appFormBean.getTran_seq() != null || appFormBean.getTran_seq().equals("") )
        {   
            ht_appresult = appService.appCashResult(appFormBean);
            Tb_Tran_CardPg tran_info_obj =(Tb_Tran_CardPg)ht_appresult.get("tran_info_obj");
            model.addAttribute("tran_info_obj", tran_info_obj);
            
            Tb_Company tbOnffCompInfo =(Tb_Company)ht_appresult.get("onffCompInfo");
            model.addAttribute("onffCompInfo", tbOnffCompInfo);
             
             NumberFormat nf = NumberFormat.getInstance();
             
             String strViewTotAmt = "";
             String strViewSvcAmt = "";
             String strViewTaxAmt = "";
             
             String strTaxAmt = com.onoffkorea.system.common.util.Util.nullToInt((String)tran_info_obj.getTax_amt());
             String strTotAmt = com.onoffkorea.system.common.util.Util.nullToInt((String)tran_info_obj.getTot_amt());
             String strSvcAmt = com.onoffkorea.system.common.util.Util.nullToInt((String)tran_info_obj.getSvc_amt());
             
             String strTaxTargetAmt = String.valueOf(Integer.parseInt(strTotAmt) -  Integer.parseInt(strSvcAmt)-  Integer.parseInt(strTaxAmt));

             strViewTotAmt = nf.format( Integer.parseInt(strTotAmt) );
             strViewSvcAmt = nf.format( Integer.parseInt(strSvcAmt) );
             strViewTaxAmt = nf.format( Integer.parseInt(strTaxAmt) );
             strTaxTargetAmt = nf.format( Integer.parseInt(strTaxTargetAmt) );

             model.addAttribute("ViewTotAmt", strTotAmt);
             model.addAttribute("ViewTaxAmt", strTaxAmt);
             model.addAttribute("ViewSvcAmt", strViewSvcAmt);
             model.addAttribute("ViewTaxTargetAmt", strTaxTargetAmt);     
             
             String strTpOnffBizNo = tbOnffCompInfo.getBiz_no();
             strTpOnffBizNo = strTpOnffBizNo.substring(0, 6) +  "****" ;
            
             model.addAttribute("ViewOnffBizNo", strTpOnffBizNo);             
        }
        
        model.addAttribute("tran_result_cd", appFormBean.getTran_result_cd());
        model.addAttribute("tran_result_msg", appFormBean.getTran_result_msg());
        
        return null;        
    }
    
   
    
    //취소요청 
    @RequestMapping(method= RequestMethod.POST,value = "/mgrAction")
    @ResponseBody
    public AppMgrResultVo appMgrAction(@Valid AppFormBean appFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        AppMgrResultVo resultobj = new AppMgrResultVo();
        String strTpResult = null;
        String strTpResultMsg = null;
        
        HttpSession session = request.getSession();
        
        SiteSession sessionobj = (SiteSession)session.getAttribute("siteSessionObj");

        String strUserCate = sessionobj.getSes_user_cate();

        appFormBean.setOnfftid(sessionobj.getSes_onfftid());
        appFormBean.setOnffmerch_no(sessionobj.getSes_onffmerch_no());
        appFormBean.setUser_cate(sessionobj.getSes_user_cate());  
        appFormBean.setUser_seq(sessionobj.getUser_seq());        
        
        appFormBean.setUser_id(sessionobj.getUser_id());
        
        //kicc 정보 설정
        /* -------------------------------------------------------------------------- */
        /* ::: 처리구분 설정                                                          */
        /* -------------------------------------------------------------------------- */
        KiccPgVo kiccpgparam = new KiccPgVo();
        kiccpgparam.setTRAN_CD_NOR_MGR("00201000");   // 변경(일반, 에스크로)
            

        /* -------------------------------------------------------------------------- */
        /* ::: 기타정보 설정                                                          */
        /* -------------------------------------------------------------------------- */
        kiccpgparam.setClient_ip        (request.getRemoteAddr());                     // [필수]결제고객 IP

        /* -------------------------------------------------------------------------- */
        /* ::: 변경관리 정보 설정                                                     */
        /* -------------------------------------------------------------------------- */
        kiccpgparam.setTr_cd            (getNullToSpace(request.getParameter("EP_tr_cd")));
        kiccpgparam.setMgr_txtype       (getNullToSpace(request.getParameter("mgr_txtype")));          // [필수]거래구분
        kiccpgparam.setMgr_subtype      (getNullToSpace(request.getParameter("mgr_subtype")));         // [선택]변경세부구분
        kiccpgparam.setOrg_cno          (getNullToSpace(request.getParameter("org_cno")));             // [필수]원거래고유번호
        kiccpgparam.setMgr_amt          (getNullToSpace(request.getParameter("mgr_amt")));             // [선택]부분취소/환불요청 금액
        kiccpgparam.setReq_id           (getNullToSpace(request.getParameter("req_id")));              // [선택]가맹점 관리자 로그인 아이디
        kiccpgparam.setMgr_msg          (getNullToSpace(request.getParameter("mgr_msg")));             // [선택]변경 사유
        
        logger.trace("----------------------------------------------------------------");
        logger.trace("AppControll appMgrAction kiccpgparam.setClient_ip val : " + kiccpgparam.getClient_ip() );
        logger.trace("AppControll appMgrAction kiccpgparam.getMgr_txtype val : " + kiccpgparam.getMgr_txtype() );
        logger.trace("AppControll appMgrAction kiccpgparam.getMgr_subtype val : " + kiccpgparam.getMgr_subtype());
        logger.trace("AppControll appMgrAction kiccpgparam.getOrg_cno val : " + kiccpgparam.getOrg_cno());
        logger.trace("AppControll appMgrAction kiccpgparam.getMgr_amt val : " + kiccpgparam.getMgr_amt());
        logger.trace("AppControll appMgrAction kiccpgparam.getReq_id val : " + kiccpgparam.getReq_id());
        logger.trace("AppControll appMgrAction kiccpgparam.getMgr_msg val : " + kiccpgparam.getMgr_msg());
        logger.trace("----------------------------------------------------------------");           

        appFormBean.setReqKiccPgVo(kiccpgparam);
        //kcp정보 셋팅
        KcpPgVo kcppgparam = new KcpPgVo();
            
        kcppgparam.setCard_pay_method("SSL");//SSL고정
        kcppgparam.setPay_method("mod");//결제방법card고정
        //kcppgparam.setOrdr_idxx(getNullToSpace(request.getParameter("EP_order_no")));//주문번호
        kcppgparam.setTx_cd("00200000");//취소 고정
        kcppgparam.setOrg_cno(getNullToSpace(request.getParameter("org_cno")));
        kcppgparam.setCust_ip(request.getRemoteAddr());            
        kcppgparam.setMod_type     ("STSC");//변경type
        kcppgparam.setMod_desc     ("customer request");//변경사유
        //kcppgparam.setPanc_mod_mny("");//부분취소금액
        //kcppgparam.setPanc_rem_mny("");//부분취소가능금액
        //kcppgparam.setMod_tax_mny  ("");//공급가 부분취소 요청금액
        //kcppgparam.setMod_vat_mny  ("");//부과세 부분 취소 요청금액
        //kcppgparam.setMod_free_mny ("");//비과세 부분 취소 요청금액
        //kcppgparam.setTran_cd   ("");//트랜젝션코드
        //kcppgparam.setbSucc("");//DB 작업성공여부
        //kcppgparam.setRes_cd    ("");//결과코드
        //kcppgparam.setRes_msg   ("");//결과메세지
        //kcppgparam.setTno       ("");//거래번호
        //kcppgparam.setAmount          ("");//결제금액
        //kcppgparam.setCard_pay_method ("");//카드결제방법
        //kcppgparam.setCard_cd         ("");//카드코드
        //kcppgparam.setCard_name       ("");//카드명
        //kcppgparam.setApp_time        ("");//승인시간
        //kcppgparam.setApp_no          ("");//승인번호
        //kcppgparam.setNoinf           ("");//무이자여부
        //kcppgparam.setQuota           ("");//할부개월
        
        appFormBean.setReqKcpPgVo(kcppgparam);
        
        
        //allat정보를 셋팅한다.
        AllatPgVo allatparam = new AllatPgVo();
        allatparam.setAllat_user_ip(request.getRemoteAddr());
        appFormBean.setReqAllatPgVo(allatparam);
        
        //기본검증 및 결제필요정보를 가져온다.
        Hashtable ht = appService.appMgrAction(appFormBean);
            
        strTpResult = (String)ht.get("RESULT_CD");
        strTpResultMsg = (String)ht.get("RESULT_MSG");
        
        String strTran_Gubun = (String)ht.get("TRAN_GUBUN");
        String strTidMtd = (String)ht.get("TIDMTD");
        
        Tb_Tran_CardPg tranInfo = null;
        
        if(strTran_Gubun.equals("CARD"))
        {
            tranInfo = (Tb_Tran_CardPg)ht.get("TransCardInfo");
        }
        else if(strTran_Gubun.equals("CASH"))
        {
            tranInfo = (Tb_Tran_CardPg)ht.get("TransCashInfo");
        }        
            
        if(strTpResult == null || !strTpResult.equals("0000"))
        {
            resultobj.setResult_cd(strTpResult);
            resultobj.setResult_msg(strTpResultMsg);
            
            appService.mgrErrTranSave(appFormBean, strTran_Gubun, tranInfo, strTpResult, strTpResultMsg);
            
            return resultobj;
        }
            
        logger.trace("----------------------------------------------------------------");
        logger.trace("AppControll appReqAction strTran_Gubun val : " + strTran_Gubun);
        logger.trace("AppControll appReqAction strTidMtd val : " + strTidMtd);
        logger.trace("----------------------------------------------------------------");
        
        Hashtable ht_appresult;
        //KICC PG일 경우
        if(strTidMtd.equals("KICCPG"))
        {
            logger.trace("----------------------------------------------------------------");
            logger.trace("AppControll appReqAction KICCPG.cert_file val : " + new String(getMessage("KICCPG.cert_file")));
            logger.trace("AppControll appReqAction KICCPG.cert_file val : " + new String(getMessage("KICCPG.g_log_dir")));
            logger.trace("AppControll appReqAction KICCPG.cert_file val : " + new String(getMessage("KICCPG.g_gw_url")));
            logger.trace("AppControll appReqAction KICCPG.cert_file val : " + new String(getMessage("KICCPG.g_gw_port")));
            logger.trace("----------------------------------------------------------------");                
                
            kiccpgparam.setKiccpg_cert_file( new String(getMessage("KICCPG.cert_file")));
            kiccpgparam.setKiccpg_g_log_dir( new String(getMessage("KICCPG.g_log_dir")));
            kiccpgparam.setKiccpg_g_gw_url( new String(getMessage("KICCPG.g_gw_url")));
            kiccpgparam.setKiccpg_g_gw_port( new String(getMessage("KICCPG.g_gw_port")));
               
            ht_appresult = appService.appMgrKiccAction(appFormBean,tranInfo);

            resultobj.setResult_cd((String)ht_appresult.get("RESULT_CD"));
            resultobj.setResult_msg((String)ht_appresult.get("RESULT_MSG"));
        }
        else if(strTidMtd.equals("KCPPG"))
        {
            logger.trace("----------------------------------------------------------------");
            logger.trace("AppControll appMgrAction KCPPG.cert_file val : " + new String(getMessage("KICCPG.cert_file")));
            logger.trace("AppControll appMgrAction KCPPG.cert_file val : " + new String(getMessage("KICCPG.g_log_dir")));
            logger.trace("AppControll appMgrAction KCPPG.cert_file val : " + new String(getMessage("KICCPG.g_gw_url")));
            logger.trace("AppControll appMgrAction KCPPG.cert_file val : " + new String(getMessage("KICCPG.g_gw_port")));
            logger.trace("----------------------------------------------------------------");                
               
            kcppgparam.setG_conf_home_dir(new String(getMessage("g_conf_home_dir")));
            kcppgparam.setG_conf_key_dir(new String(getMessage("g_conf_key_dir")));
            kcppgparam.setG_conf_log_dir(new String(getMessage("g_conf_log_dir")));
            kcppgparam.setG_conf_log_level(new String(getMessage("g_conf_log_level")));
            kcppgparam.setG_conf_gw_port(new String(getMessage("g_conf_gw_port")));
            kcppgparam.setG_conf_tx_mode(new String(getMessage("g_conf_tx_mode")));
            kcppgparam.setG_conf_site_name(new String(getMessage("g_conf_site_name")));
            kcppgparam.setG_conf_gw_url(new String(getMessage("g_conf_gw_url")));
               
            ht_appresult = appService.appMgrKcpAction(appFormBean,tranInfo);

            resultobj.setResult_cd((String)ht_appresult.get("RESULT_CD"));
            resultobj.setResult_msg((String)ht_appresult.get("RESULT_MSG"));            
        }
        else if(strTidMtd.equals("ALLATPG"))
        {
            String strTestFlag =  com.onoffkorea.system.common.util.Util.nullToString(new String(getMessage("ALLAT_TESTFLAG")));
               
            logger.debug("strTestFlag : " + strTestFlag);
                
            if(strTestFlag.equals("N"))
            {
               allatparam.setAllat_test_yn("N"); 
            }
            else
            {
               allatparam.setAllat_test_yn("Y"); 
            }
               
            logger.debug("allatparam.getAllat_test_yn : " + allatparam.getAllat_test_yn());  
               
            ht_appresult = appService.appMgrAllatAction(appFormBean,tranInfo);

            resultobj.setResult_cd((String)ht_appresult.get("RESULT_CD"));
            resultobj.setResult_msg((String)ht_appresult.get("RESULT_MSG"));            
        }        
        else
        {
            //throw new Exception("지원하지 않는 결제사 입니다.");
            /*
            model.addAttribute("RESULT_CD","9003");
            model.addAttribute("RESULT_MSG","지원하지 않는 결제사 입니다.");
            model.addAttribute("TRAN_SEQ", "");
            */
            resultobj.setResult_cd("OF03");
            resultobj.setResult_msg("지원하지 않는 결제사 입니다.");
        }
            
         return resultobj;
    }
    
 
    //취소의뢰 
    @RequestMapping(method= RequestMethod.POST,value = "/cnclReqAction")
    @ResponseBody
    public AppMgrResultVo appCnclReqAction(@Valid AppFormBean appFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        AppMgrResultVo resultobj = new AppMgrResultVo();
        String strTpResult = null;
        String strTpResultMsg = null;
        
        HttpSession session = request.getSession();
        
        SiteSession sessionobj = (SiteSession)session.getAttribute("siteSessionObj");

        String strUserCate = sessionobj.getSes_user_cate();

        appFormBean.setOnfftid(sessionobj.getSes_onfftid());
        appFormBean.setOnffmerch_no(sessionobj.getSes_onffmerch_no());
        appFormBean.setUser_cate(sessionobj.getSes_user_cate());  
        appFormBean.setUser_seq(sessionobj.getUser_seq());        
        
        appFormBean.setUser_id(sessionobj.getUser_id());
        
       
        //기본검증 및 결제필요정보를 가져온다.
        Hashtable ht = appService.appCnclReqAction(appFormBean);
        
        resultobj.setResult_cd((String)ht.get("RESULT_CD"));
        resultobj.setResult_msg((String)ht.get("RESULT_MSG"));
            
         return resultobj;
    }    
    

    
    //메일발송 
    @RequestMapping(method= RequestMethod.POST,value = "/cardSendMail")
    @ResponseBody
    public AppMgrResultVo cardSendMail(@Valid AppFormBean appFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_appresult;
        HttpSession session = request.getSession();
        SiteSession sessionobj = (SiteSession)session.getAttribute("siteSessionObj");
        String strUserCate = sessionobj.getSes_user_cate();

        appFormBean.setOnfftid(sessionobj.getSes_onfftid());
        appFormBean.setOnffmerch_no(sessionobj.getSes_onffmerch_no());
        appFormBean.setUser_cate(sessionobj.getSes_user_cate()); 
        appFormBean.setUser_seq(sessionobj.getUser_seq());

        AppMgrResultVo resultobj = new AppMgrResultVo();
        String strTpResult = null;
        String strTpResultMsg = null;
        
        EmailVO email = null;
        
        if(appFormBean.getTran_seq() != null || appFormBean.getTran_seq().equals("") )
        {   
            ht_appresult = appService.appResult(appFormBean);
            
            Tb_Tran_CardPg tran_info_obj =(Tb_Tran_CardPg)ht_appresult.get("tran_info_obj");
            
            Tb_Company tbOnffCompInfo =(Tb_Company)ht_appresult.get("onffCompInfo");
            
            String strTpCardNo = tran_info_obj.getCard_num();
            String strInputCardNo = "";
            if(!strTpCardNo.equals(""))
            {
		strInputCardNo = strTpCardNo.substring(0, 8) + "****" + strTpCardNo.substring(12 , strTpCardNo.length()); 
            }            
             
            String strTpOnffBizNo = tbOnffCompInfo.getBiz_no();
            strTpOnffBizNo = strTpOnffBizNo.substring(0, 6) +  "****" ;
            
             NumberFormat nf = NumberFormat.getInstance();
             
             String strViewAmt = "";
             String strViewSvcAmt = "";

             String strTaxAmt = com.onoffkorea.system.common.util.Util.nullToInt((String)tran_info_obj.getTax_amt());
             String strTotAmt = com.onoffkorea.system.common.util.Util.nullToInt((String)tran_info_obj.getTot_amt());
             String strSvcAmt = com.onoffkorea.system.common.util.Util.nullToInt((String)tran_info_obj.getSvc_amt());


             strViewAmt = nf.format( Integer.parseInt(strTotAmt) - Integer.parseInt(strSvcAmt) - Integer.parseInt(strTaxAmt));//과세승인금액
             strTaxAmt = nf.format( Integer.parseInt(strTaxAmt) );//부가세
             strTotAmt = nf.format( Integer.parseInt(strTotAmt) );//총금액
             strViewSvcAmt = nf.format( Integer.parseInt(strSvcAmt) );//비과세금액

                StringBuffer sbCardMail = new StringBuffer();
                sbCardMail.append("<html>");
                sbCardMail.append("<head>");
                sbCardMail.append("<title>ONOFFKOREA 카드결제 결과</title>");
                sbCardMail.append("<meta http-equiv='content-type' content='text/html; charset=utf-8'>");
                sbCardMail.append("<style>");
                sbCardMail.append("body, table, div, p ,input,span {");
                sbCardMail.append("font-family: Tahoma;");
                sbCardMail.append(" font-size: 11px;");
                sbCardMail.append("}");
                sbCardMail.append("</style>");
                sbCardMail.append("</head>");
                sbCardMail.append("<body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'>");
                sbCardMail.append("<form name='frm_card_reuslt_pop' id='frm_card_reuslt_pop' method='post' >");
                sbCardMail.append("<div id='divMsg' ondragstart='return false' onselectstart='return false' oncontextmenu='return false'>");
                sbCardMail.append("<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center'>");
                sbCardMail.append("	<tr>");
                sbCardMail.append("		<td height='5'></td>");
                sbCardMail.append("	</tr>");
                sbCardMail.append("	<tr>");
                sbCardMail.append("		<td width='100%' valign='middle' align='center' height='30'>");
                sbCardMail.append("		<div id='divAD' style='width:330' align='left' style='font-size:8pt'>");
                sbCardMail.append("			배경까지 인쇄하셔야 아래의 영수증을 보실 수 있습니다.</br>");
                sbCardMail.append("			■ 설정 : 도구&gt인터넷옵션&gt고급&gt인쇄(배경 및 이미지인쇄 선택)");
                sbCardMail.append("		</div>");
                sbCardMail.append("		</td>");
                sbCardMail.append("	</tr>");
                sbCardMail.append("	<tr>");
                sbCardMail.append("		<td width='100%' valign='middle' align='center'>");
                sbCardMail.append("		<table width='330' border='0' cellspacing='0' cellpadding='0' align='center'>");
                sbCardMail.append("			<tr>");
                sbCardMail.append("				<td height='5' colspan='3'></td>");
                sbCardMail.append("			</tr>");
                sbCardMail.append("			<tr>");
                sbCardMail.append("                            <td width='25'></td>");
                sbCardMail.append("				<td width='280' align='center' valign='middle'>");
                sbCardMail.append("				<table border='0' width='280' align='center' cellpadding='0' cellspacing='1' bgcolor='#7893C8'>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='100%' align='center' valign='middle' colspan='11' bgcolor='#FFFFFF'>");
                sbCardMail.append("						<table border='0' cellspacing='0' cellpadding='0' width='100%' bgcolor='#FFFFFF'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='35' colspan=2><u><font size='2'>카 드 매 출 전 표</font></u></td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' colspan='2'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>일련번호</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+tran_info_obj.getTran_seq()+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' >");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>카드종류</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+com.onoffkorea.system.common.util.Util.nullToString(tran_info_obj.getIss_nm()) +"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='50%' align='center' valign='middle'></td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' colspan='2'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>결제방법</td>");
                sbCardMail.append("							</tr>");
                
                String strTpInstallment = getNullToSpace(tran_info_obj.getInstallment());
                String strViewInstallment = "";
                if("00".equals(strTpInstallment))
                {
                    strViewInstallment = "일시불";
                }
                else
                {
                    strViewInstallment = strTpInstallment;
                }       

                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+strTpInstallment+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' >");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>카드번호</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+com.onoffkorea.system.common.util.Util.nullToString(strInputCardNo)+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='50%' align='center' valign='middle'></td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' colspan='2'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>유효기간</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>**/**</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' >");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                String strViewTranCate = "";
                if ("10".equals(tran_info_obj.getMassagetype()))
                {
                    strViewTranCate = "승인";
                }
                else
                {
                    strViewTranCate = "취소";
                }
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>거래유형</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+com.onoffkorea.system.common.util.Util.nullToString(strViewTranCate)+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='50%' align='center' valign='middle'></td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' colspan='2'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>거래일시</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("                                                            <td align='center' height='17'>"+ getNullToSpace(tran_info_obj.getApp_dt()) + " " + getNullToSpace(tran_info_obj.getApp_tm()) +"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' >");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>취소일시</td>");
                sbCardMail.append("                                <td align='center' height='17'>" + getNullToSpace(tran_info_obj.getCncl_dt())+"</td>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("                                                <td width='50%' align='center' valign='middle'></td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='30%' bgcolor='#DBE4F7'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left'>품명</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='20%' bgcolor='#DBE4F7'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td>&nbsp;</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='20%' bgcolor='#DBE4F7'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td>&nbsp;</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>                                                ");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<!--상품명-->");
                sbCardMail.append("						<td width='30%' bgcolor='#FFFFFF' rowspan='4'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left'>"+com.onoffkorea.system.common.util.Util.nullToString(tran_info_obj.getProduct_nm())+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='20%' bgcolor='#FFFFFF'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'");
                sbCardMail.append("							bgcolor='#DBE4F7'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left'>과세금액 <br>");
                sbCardMail.append("								<font style='font-size:7pt;font-family:돋움;'>(AMOUNT)</font></td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("");
                sbCardMail.append("						<td width='6%' bgcolor='#FFFFFF'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='right' width='6%'>"+strViewAmt+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='20%' bgcolor='#FFFFFF'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'");
                sbCardMail.append("							bgcolor='#DBE4F7'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left'>비과세금액 <br>");
                sbCardMail.append("								<font style='font-size:7pt;font-family:돋움;'>(SERVICE)</font></td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("");
                sbCardMail.append("						<td width='6%' bgcolor='#FFFFFF'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='right' width='6%'>"+strViewSvcAmt+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("					</tr>");                
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='20%' bgcolor='#FFFFFF'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'");
                sbCardMail.append("							bgcolor='#DBE4F7'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left'>세금 <br>");
                sbCardMail.append("								<font style='font-size:7pt;font-family:돋움;'>(TAXES)</font></td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("");
                sbCardMail.append("						<td width='6%' bgcolor='#FFFFFF'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='right' width='6%'>"+strTaxAmt+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='20%' bgcolor='#FFFFFF'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'");
                sbCardMail.append("							bgcolor='#DBE4F7'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left'>합계 <br>");
                sbCardMail.append("								<font style='font-size:7pt;font-family:돋움;'>(TOTALS)</font></td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("");
                sbCardMail.append("						<td width='6%' bgcolor='#FFFFFF'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='right' width='6%'>"+strTotAmt+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("                                            <td width='50%' bgcolor='#FFFFFF' colspan='2' >");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>고객명</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+com.onoffkorea.system.common.util.Util.nullToString(tran_info_obj.getUser_nm())+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>						");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' >");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>승인번호/고유번호</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+com.onoffkorea.system.common.util.Util.nullToString(tran_info_obj.getApp_no())+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' colspan='2'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>공급자 상호<br>");
                sbCardMail.append("								</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+tran_info_obj.getComp_nm()+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' >");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>사업자등록번호</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+tran_info_obj.getBiz_no()+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' colspan='2'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>대표자명<br>");
                sbCardMail.append("								</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+tran_info_obj.getComp_ceo_nm()+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' >");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>이용문의</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+tran_info_obj.getComp_tel1()+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='100%' bgcolor='#FFFFFF' colspan='9'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>사업장주소</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("                                                            <td align='center' height='17'>"+tran_info_obj.getAddr_1() + " " + tran_info_obj.getAddr_2() + "</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("					</tr>	                                        ");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' colspan='2'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>가맹점명</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("	");
                sbCardMail.append("								<td align='center' height='17'>"+tbOnffCompInfo.getComp_nm()+"</td>");
                sbCardMail.append("					");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' >");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>사업자등록번호</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("	");
                sbCardMail.append("								<td align='center' height='17'>"+strTpOnffBizNo+"</td>");
                sbCardMail.append("					");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("                                            <td width='50%' bgcolor='#FFFFFF' colspan='2'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>대표자</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("	");
                sbCardMail.append("								<td align='center' height='17'>"+tbOnffCompInfo.getComp_ceo_nm()+"</td>");
                sbCardMail.append("					");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' colspan='2'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>승인관련문의</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+tbOnffCompInfo.getComp_tel1()+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						");
                sbCardMail.append("					</tr>                                        ");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='100%' bgcolor='#FFFFFF' colspan='3'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>가맹점주소</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+ tbOnffCompInfo.getAddr_1() +"<br>"+ tbOnffCompInfo.getAddr_2()+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("		</table>");
                sbCardMail.append("		<!--영수증 end-->");
                sbCardMail.append("		</td>");
                sbCardMail.append("	</tr>");
                sbCardMail.append("</table>");
                sbCardMail.append("</div>");
                sbCardMail.append("</form>");
                sbCardMail.append("</body>");
                sbCardMail.append("</html>");

                
                email = new EmailVO();
                
                String receiver = appFormBean.getRecvmailaddr(); //Receiver.
                String subject = "온오프코리아 카드결제 내역입니다.";
                email.setSender(new String(getMessage("MAILSERV.sender")));
                email.setReceiver(receiver);
                email.setSubject(subject);
                email.setContent(sbCardMail.toString());
                boolean result = emailService.sendMail(email);

                if(result)
                {
                    resultobj.setResult_cd("0000");
                    resultobj.setResult_msg("메일이 발송되었습니다.");
                }
                else
                {
                    resultobj.setResult_cd("XM02");
                    resultobj.setResult_msg("메일발송에 실패하였습니다.");
                }
        }
        else
        {
            resultobj.setResult_cd("XM01");
            resultobj.setResult_msg("메일발송 결제내역이 없습니다.");
        }
        
        return resultobj;        
    }    
    
    
    //메일발송 
    @RequestMapping(method= RequestMethod.POST,value = "/cashSendMail")
    @ResponseBody
    public AppMgrResultVo cashSendMail(@Valid AppFormBean appFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_appresult;
        HttpSession session = request.getSession();
        SiteSession sessionobj = (SiteSession)session.getAttribute("siteSessionObj");
        String strUserCate = sessionobj.getSes_user_cate();

        appFormBean.setOnfftid(sessionobj.getSes_onfftid());
        appFormBean.setOnffmerch_no(sessionobj.getSes_onffmerch_no());
        appFormBean.setUser_cate(sessionobj.getSes_user_cate()); 
        appFormBean.setUser_seq(sessionobj.getUser_seq());

        AppMgrResultVo resultobj = new AppMgrResultVo();
        String strTpResult = null;
        String strTpResultMsg = null;
        
        EmailVO email = null;
        
        if(appFormBean.getTran_seq() != null || appFormBean.getTran_seq().equals("") )
        {   
            ht_appresult = appService.appCashResult(appFormBean);
            
            Tb_Tran_CardPg tran_info_obj =(Tb_Tran_CardPg)ht_appresult.get("tran_info_obj");
            
            Tb_Company tbOnffCompInfo =(Tb_Company)ht_appresult.get("onffCompInfo");
            
             NumberFormat nf = NumberFormat.getInstance();
             
             String strViewTotAmt = "";
             String strViewSvcAmt = "";
             String strViewTaxAmt = "";
             
             String strTaxAmt = com.onoffkorea.system.common.util.Util.nullToInt((String)tran_info_obj.getTax_amt());
             String strTotAmt = com.onoffkorea.system.common.util.Util.nullToInt((String)tran_info_obj.getTot_amt());
             String strSvcAmt = com.onoffkorea.system.common.util.Util.nullToInt((String)tran_info_obj.getSvc_amt());
             
             String strTaxTargetAmt = String.valueOf(Integer.parseInt(strTotAmt) -  Integer.parseInt(strSvcAmt) -  Integer.parseInt(strTaxAmt));

             strViewTotAmt = nf.format( Integer.parseInt(strTotAmt) );
             strViewSvcAmt = nf.format( Integer.parseInt(strSvcAmt) );
             strViewTaxAmt = nf.format( Integer.parseInt(strTaxAmt) );
             strTaxTargetAmt = nf.format( Integer.parseInt(strTaxTargetAmt) );
             
            String strTpOnffBizNo = tbOnffCompInfo.getBiz_no();
            strTpOnffBizNo = strTpOnffBizNo.substring(0, 6) +  "****" ;

            StringBuffer sbCardMail = new StringBuffer();
                sbCardMail.append("<html>");
                sbCardMail.append("<head>");
                sbCardMail.append("<title>ONOFFKOREA 현금영수증</title>");
                sbCardMail.append("<meta name='robots' content='noindex, nofollow'>");
                sbCardMail.append("<meta http-equiv='content-type' content='text/html; charset=utf-8'>");
                sbCardMail.append("<style>");
                sbCardMail.append("body, table, div, p ,input,span {");
                sbCardMail.append("font-family: Tahoma;");
                sbCardMail.append(" font-size: 11px;");
                sbCardMail.append("}");
                sbCardMail.append("</style>");
                sbCardMail.append("</head>");
                sbCardMail.append("<body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'>");
                sbCardMail.append("<form name='frm_cash_reuslt_pop' id='frm_cash_reuslt_pop' method='post' >");
                sbCardMail.append("<div id='divMsg' ondragstart='return false' onselectstart='return false' oncontextmenu='return false'>");
                sbCardMail.append("<table width='100%' border='0' cellspacing='0' cellpadding='0' align='center'>");
                sbCardMail.append("	<tr>");
                sbCardMail.append("		<td height='5'></td>");
                sbCardMail.append("	</tr>");
                sbCardMail.append("	<tr>");
                sbCardMail.append("		<td width='100%' valign='middle' align='center' height='30'>");
                sbCardMail.append("		<div id='divAD' style='width:330' align='left' style='font-size:8pt'>");
                sbCardMail.append("			배경까지 인쇄하셔야 아래의 영수증을 보실 수 있습니다.</br>");
                sbCardMail.append("			■ 설정 : 도구&gt인터넷옵션&gt고급&gt인쇄(배경 및 이미지인쇄 선택)");
                sbCardMail.append("		</div>");
                sbCardMail.append("		</td>");
                sbCardMail.append("	</tr>");
                sbCardMail.append("	<tr>");
                sbCardMail.append("		<td width='100%' valign='middle' align='center'>");
                sbCardMail.append("		<table width='330' border='0' cellspacing='0' cellpadding='0' align='center'>");
                sbCardMail.append("			<tr>");
                sbCardMail.append("				<td height='5' colspan='3'></td>");
                sbCardMail.append("			</tr>");
                sbCardMail.append("			<tr>");
                sbCardMail.append("                            <td width='25'></td>");
                sbCardMail.append("				<td width='280' align='center' valign='middle'>");
                sbCardMail.append("				<table border='0' width='280' align='center' cellpadding='0' cellspacing='1' bgcolor='#7893C8'>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='100%' align='center' valign='middle' colspan='11' bgcolor='#FFFFFF'>");
                sbCardMail.append("						<table border='0' cellspacing='0' cellpadding='0' width='100%' bgcolor='#FFFFFF'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='35' colspan=2><u><font size='2'>현 금 영 수 증</font></u></td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' colspan='2'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>일련번호</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+tran_info_obj.getTran_seq()+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' >");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>발급종류</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("                           <tr>");
                sbCardMail.append("								<td align='center' height='17'>"+com.onoffkorea.system.common.util.Util.nullToString(tran_info_obj.getWcc_nm())+"</td>");
                sbCardMail.append("							</tr>                                                        ");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='50%' align='center' valign='middle'></td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' colspan='2'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>인증번호종류</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("                                                        <tr>");
                sbCardMail.append("								<td align='center' height='17'>"+com.onoffkorea.system.common.util.Util.nullToString(tran_info_obj.getCard_cate_nm())+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' >");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>인증번호</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+com.onoffkorea.system.common.util.Util.nullToString(tran_info_obj.getCard_num())+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='50%' align='center' valign='middle'></td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' colspan='2'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>유효기간</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'></td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' >");
                String strViewTranCate = "";
                if ("10".equals(tran_info_obj.getMassagetype()))
                {
                    strViewTranCate = "발급";
                }
                else
                {
                    strViewTranCate = "취소";
                }                
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>거래유형</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+strViewTranCate+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='50%' align='center' valign='middle'></td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' colspan='2'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>거래일시</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("                                 <td align='center' height='17'>" + getNullToSpace(tran_info_obj.getApp_dt()) + " " + getNullToSpace(tran_info_obj.getApp_tm()) + "</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' >");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>취소일시</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("                                   <td align='center' height='17'>" + getNullToSpace(tran_info_obj.getCncl_dt())+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("                                                <td width='50%' align='center' valign='middle'></td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='30%' bgcolor='#DBE4F7'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left'>품명</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='20%' bgcolor='#DBE4F7'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td>&nbsp;</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='20%' bgcolor='#DBE4F7'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td>&nbsp;</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>                                                ");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<!--상품명-->");
                sbCardMail.append("						<td width='30%' bgcolor='#FFFFFF' rowspan='4'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left'>"+com.onoffkorea.system.common.util.Util.nullToString(tran_info_obj.getProduct_nm())+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='20%' bgcolor='#FFFFFF'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'");
                sbCardMail.append("							bgcolor='#DBE4F7'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left'>봉사료<br>");
                sbCardMail.append("								<font style='font-size:7pt;font-family:돋움;'>(Service)</font></td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='6%' bgcolor='#FFFFFF'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='right' width='6%'>"+strViewSvcAmt+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='20%' bgcolor='#FFFFFF'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'");
                sbCardMail.append("							bgcolor='#DBE4F7'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left'>과세금액<br><font style='font-size:7pt;font-family:돋움;'>(Amount)</font></td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='6%' bgcolor='#FFFFFF'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='right' width='6%'>"+strTaxTargetAmt+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("					</tr>                                        ");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='20%' bgcolor='#FFFFFF'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'");
                sbCardMail.append("							bgcolor='#DBE4F7'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left'>세금 <br>");
                sbCardMail.append("								<font style='font-size:7pt;font-family:돋움;'>(TAXES)</font></td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='6%' bgcolor='#FFFFFF'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='right' width='6%'>"+strViewTaxAmt+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='20%' bgcolor='#FFFFFF'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'");
                sbCardMail.append("							bgcolor='#DBE4F7'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left'>합계 <br>");
                sbCardMail.append("								<font style='font-size:7pt;font-family:돋움;'>(TOTALS)</font></td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='6%' bgcolor='#FFFFFF'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='right' width='6%'>"+strViewTotAmt+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' colspan='2'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>승인번호/고유번호</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+tran_info_obj.getApp_no()+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' >");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>고객명</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+com.onoffkorea.system.common.util.Util.nullToString(tran_info_obj.getUser_nm())+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' colspan='2'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>공급자 상호<br>");
                sbCardMail.append("								</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+tran_info_obj.getComp_nm()+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' >");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>사업자등록번호</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+tran_info_obj.getBiz_no()+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' colspan='2'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>대표자명<br>");
                sbCardMail.append("								</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+tran_info_obj.getComp_ceo_nm()+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' >");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>이용문의</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+tran_info_obj.getComp_tel1()+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='100%' bgcolor='#FFFFFF' colspan='9'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>사업장주소</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("                                                            <td align='center' height='17'>"+tran_info_obj.getAddr_1() + " " + tran_info_obj.getAddr_2() + "</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("					</tr>	");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' colspan='2'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>가맹점명</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+tbOnffCompInfo.getComp_nm()+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' >");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>사업자등록번호</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+strTpOnffBizNo+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' colspan='2'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>대표자</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+tbOnffCompInfo.getComp_ceo_nm()+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>                                            ");
                sbCardMail.append("						<td width='50%' bgcolor='#FFFFFF' colspan='2'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>승인관련문의</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+tbOnffCompInfo.getComp_tel1()+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("					</tr>");
                sbCardMail.append("					<tr>");
                sbCardMail.append("						<td width='100%' bgcolor='#FFFFFF' colspan='3'>");
                sbCardMail.append("						<table width='100%' border='0' cellspacing='0' cellpadding='0'>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='left' bgcolor='#DBE4F7'>가맹점주소</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("							<tr>");
                sbCardMail.append("								<td align='center' height='17'>"+ tbOnffCompInfo.getAddr_1() +"<br>"+ tbOnffCompInfo.getAddr_2()+"</td>");
                sbCardMail.append("							</tr>");
                sbCardMail.append("						</table>");
                sbCardMail.append("						</td>");
                sbCardMail.append("					</tr>                                        ");
                sbCardMail.append("				</table>");
                sbCardMail.append("				<!--border end--></td>");
                sbCardMail.append("				<td width='25'></td>");
                sbCardMail.append("			</tr>");
                sbCardMail.append("		</table>");
                sbCardMail.append("		<!--영수증 end-->");
                sbCardMail.append("		</td>");
                sbCardMail.append("	</tr>");
                sbCardMail.append("</table>");
                sbCardMail.append("</div>");
                sbCardMail.append("</form>");
                sbCardMail.append("</body>");
                sbCardMail.append("</html>");

                
                email = new EmailVO();
                
                String receiver = appFormBean.getRecvmailaddr(); //Receiver.
                String subject = "온오프코리아 현금영수증 발급내역입니다.";
                email.setSender(new String(getMessage("MAILSERV.sender")));
                email.setReceiver(receiver);
                email.setSubject(subject);
                email.setContent(sbCardMail.toString());
                boolean result = emailService.sendMail(email);

                if(result)
                {
                    resultobj.setResult_cd("0000");
                    resultobj.setResult_msg("메일이 발송되었습니다.");
                }
                else
                {
                    resultobj.setResult_cd("XM02");
                    resultobj.setResult_msg("메일발송에 실패하였습니다.");
                }
        }
        else
        {
            resultobj.setResult_cd("XM01");
            resultobj.setResult_msg("메일발송 승인내역이 없습니다.");
        }
        
        return resultobj;        
    }        
}
