/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.app.Dao;

import com.onoffkorea.system.app.Bean.AppFormBean;
import com.onoffkorea.system.app.Bean.InsertCardPgTranBean;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("appDAO")
public class AppDAOImpl extends SqlSessionDaoSupport  implements AppDAO {
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Override
    public List testlist() {
       return (List) getSqlSession().selectList("App.test");
    }
    
    
    @Override
    public List tranMasterList(AppFormBean appFormBean){
        //return (List) getSqlSession().selectList("App.paymtdMasterList", appFormBean);
        return null;
    }

    @Override
    public List GetOnffMerchInfo(AppFormBean appFormBean){
        logger.trace("----------------------------------------------------------------");
        logger.trace("AppDAOImpl GetOnffMerchInfo appFormBean val : " + appFormBean.getOnffmerch_no());
        logger.trace("----------------------------------------------------------------");
        return (List) getSqlSession().selectList("App.appGetOnffMerchInfo", appFormBean);

    }
    
    
    @Override
    public List GetOnffTidInfo(AppFormBean appFormBean) {
        logger.trace("----------------------------------------------------------------");
        logger.trace("AppDAOImpl GetOnffTidInfo appFormBean val : " + appFormBean.getOnfftid());
        logger.trace("----------------------------------------------------------------");
        return (List) getSqlSession().selectList("App.GetOnffTidInfo", appFormBean);
    }

    
    @Override
    public List GetAppTid(AppFormBean appFormBean){
        return (List) getSqlSession().selectList("App.appGetTid", appFormBean);
        //return null;
    }

    @Override
    public Integer CardTranInsert(AppFormBean appFormBean) {
        //return (Integer)getSqlSession().insert("App.CardTranInsert", appFormBean);
        return null;
        
    }

    @Override
    public String GetOrderSeq() {
        return (String) getSqlSession().selectOne("App.appGetOrderSeq");
    }

    @Override
    public String GetTranSeq() {
        return (String) getSqlSession().selectOne("App.appGetTranSeq");
    }

    @Override
    public String GetappChkAmt(AppFormBean appFormBean) {
        return (String) getSqlSession().selectOne("App.appChkAmt", appFormBean);        
    }

    @Override
    public String GetCardBeanInfo(AppFormBean appFormBean) {
        return (String) getSqlSession().selectOne("App.GetCardBeanInfo", appFormBean);
    }

    @Override
    public List GetTerminalInfo(AppFormBean appFormBean) {
        return (List) getSqlSession().selectList("App.GetTerminalInfo", appFormBean);
    }

    @Override
    public List GetOnffTidCommission(AppFormBean appFormBean) {
        return (List) getSqlSession().selectList("App.GetOnffTidCommission", appFormBean);
    }
    
        @Override
    public List GetCardCode(AppFormBean appFormBean) {
        return (List) getSqlSession().selectList("App.GetCardCode", appFormBean);
    }

    @Override
    public Integer InsertCardPgTran(InsertCardPgTranBean insertCardPgTranBean) {
                Integer result = getSqlSession().insert("App.CardPgTranInsert", insertCardPgTranBean);
        return result;
    }

    @Override
    public Integer OrgCardPgTranUpdate(InsertCardPgTranBean insertCardPgTranBean) {
       Integer result = getSqlSession().update("App.OrgCardPgTranUpdate", insertCardPgTranBean);
        return result;
    }

    @Override
    public List GetCardTranInfo(AppFormBean appFormBean) {
         return (List) getSqlSession().selectList("App.GetCardTrans", appFormBean);
    }

    @Override
    public Integer InsertCashPgTran(InsertCardPgTranBean insertCardPgTranBean) {
                Integer result = getSqlSession().insert("App.CashPgTranInsert", insertCardPgTranBean);
        return result;
    }

    @Override
    public Integer OrgCashPgTranUpdate(InsertCardPgTranBean insertCardPgTranBean) {
             Integer result = getSqlSession().update("App.OrgCashPgTranUpdate", insertCardPgTranBean);
        return result;
    }

    @Override
    public List GetCashTranInfo(AppFormBean appFormBean) {
        return (List) getSqlSession().selectList("App.GetCashTrans", appFormBean);
    }

    @Override
    public String GetTerminalMtdInfo(String strTerminalNo) {
       return (String) getSqlSession().selectOne("App.GetTerminalMtdInfo", strTerminalNo);
    }
    
    
    @Override
    public String GetTerminalPwdInfo(String strTerminalNo) {
       return (String) getSqlSession().selectOne("App.GetTerminalPwdInfo", strTerminalNo);
    }
    
    @Override
    public String GetMerchCommisionInfo(String strMerchNo) {
       return (String) getSqlSession().selectOne("App.GetMerchCommisionInfo", strMerchNo);
    }
    
    
    @Override
    public List GetTerminalInfo(String strTerminalNo) {
       return (List) getSqlSession().selectList("App.GetTerminalInfo_allat", strTerminalNo);
    }    

    @Override
    public String GetCurAppTotAmt(String onffmerchno) {
        return (String) getSqlSession().selectOne("App.GetCurAppTotAmt", onffmerchno);
    }

    @Override
    public Integer CnclReaCardTranUpdate(InsertCardPgTranBean insertCardPgTranBean) {
       Integer result = getSqlSession().update("App.CnclReaCardTranUpdate", insertCardPgTranBean);
        return result;
    }
    
    
    @Override
    public String getOnffmerchCnclAuth(String stronffmerchno) {
        return (String) getSqlSession().selectOne("App.getOnffmerchCnclAuth",stronffmerchno);
    }
    
    @Override
    public String getOnfftidCnclAuth(String stronfftid) {
        return (String) getSqlSession().selectOne("App.getOnfftidCnclAuth",stronfftid);
    }
    
    @Override
    public String getOnffCnclChkCnt(AppFormBean appFormBean) {
        return (String) getSqlSession().selectOne("App.getOnffCnclChkCnt",appFormBean);
    }
    
    @Override
    public String getOnffAppChkCnt(AppFormBean appFormBean) {
        return (String) getSqlSession().selectOne("App.getOnffAppChkCnt",appFormBean);
    }
}
