/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.app.Dao;

import com.onoffkorea.system.app.Bean.AppFormBean;
import com.onoffkorea.system.app.Bean.InsertCardPgTranBean;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public interface AppDAO {
    
    public List testlist();
    
    public List tranMasterList(AppFormBean appFormBean);

    public List GetOnffMerchInfo(AppFormBean appFormBean);
    
    public List GetOnffTidInfo(AppFormBean appFormBean);
    
    public List GetAppTid(AppFormBean appFormBean);
    
    public Integer CardTranInsert(AppFormBean appFormBean);

    public String GetOrderSeq();
    
    public String GetTranSeq();
    
    public String GetappChkAmt(AppFormBean appFormBean);
    
    public String GetCardBeanInfo(AppFormBean appFormBean);
    
    public List GetTerminalInfo(AppFormBean appFormBean);
    
    public List GetOnffTidCommission(AppFormBean appFormBean);
    
    public List GetCardCode(AppFormBean appFormBean);
    
    public Integer InsertCardPgTran(InsertCardPgTranBean insertCardPgTranBean);
    
    public Integer OrgCardPgTranUpdate(InsertCardPgTranBean insertCardPgTranBean);
    
    public List GetCardTranInfo(AppFormBean appFormBean);
    
    public Integer InsertCashPgTran(InsertCardPgTranBean insertCardPgTranBean);
    
    public Integer OrgCashPgTranUpdate(InsertCardPgTranBean insertCardPgTranBean);
    
    public List GetCashTranInfo(AppFormBean appFormBean);    
    
    public String GetTerminalMtdInfo(String strTerminalNo);
    
    public String GetTerminalPwdInfo(String strTerminalNo);
    
    public String GetMerchCommisionInfo(String strMerchNo);
    
    public List GetTerminalInfo(String strTerminalNo);
    
    public String GetCurAppTotAmt(String strTerminalNo);
    
    public Integer CnclReaCardTranUpdate(InsertCardPgTranBean insertCardPgTranBean);

    public String getOnffmerchCnclAuth(String stronffmerchno);
    
    public String getOnfftidCnclAuth(String stronfftid);
    
    public String getOnffCnclChkCnt(AppFormBean appFormBean);    
    
    public String getOnffAppChkCnt(AppFormBean appFormBean);
}
