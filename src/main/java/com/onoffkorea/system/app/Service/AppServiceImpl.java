/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.app.Service;

import com.allat.util.AllatUtil;
import com.kcp.C_PP_CLI;
import com.kicc.EasyPayClient;
import com.onoffkorea.system.app.Bean.AppFormBean;
import com.onoffkorea.system.app.Bean.InsertCardPgTranBean;
import com.onoffkorea.system.app.Dao.AppDAO;
import com.onoffkorea.system.app.Vo.AllatPgVo;
import com.onoffkorea.system.app.Vo.AppOnffTidInfoVo;
import com.onoffkorea.system.app.Vo.AppTerminalInfoVo;
import com.onoffkorea.system.app.Vo.KcpPgVo;
import com.onoffkorea.system.app.Vo.KiccPgVo;
import com.onoffkorea.system.app.Vo.Tb_Pay_Chn;
import com.onoffkorea.system.app.Vo.Tb_Tran_CardPg;
import com.onoffkorea.system.code.Vo.Tb_Main_Code;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.company.Bean.CompanyFormBean;
import com.onoffkorea.system.company.Dao.CompanyDAO;
import com.onoffkorea.system.company.Vo.Tb_Company;
import com.onoffkorea.system.merchant.Vo.Tb_Onffmerch_Mst;
import com.onoffkorea.system.tid.Vo.Tb_Tid_Info;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.*;

/**
 *
 * @author Administrator
 */
@Service("appService")
public class AppServiceImpl implements AppService
{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private AppDAO appDAO;
    
    @Autowired
    private CompanyDAO compDAO;
    
    
    @Override
    public Hashtable appselReady(AppFormBean appFormBean)  throws Exception {
        Hashtable ht = new Hashtable();
        
        logger.trace("----------------------------------------------------------------");
        logger.trace("AppServiceImpl appFormBean.getOnfftid val : " + appFormBean.getOnfftid());
        logger.trace("AppServiceImpl appFormBean.getOnffmerch_no val : " + appFormBean.getOnffmerch_no());
         logger.trace("AppServiceImpl appFormBean.getUser_cate val : " + appFormBean.getUser_cate());
        logger.trace("----------------------------------------------------------------");
        
        //온오프가맹점정보를 셋팅한다.
        String strUserCateVal = appFormBean.getUser_cate();
        
        StringBuffer sbtid = new StringBuffer();
        StringBuffer sbmerch = new StringBuffer();
        
        //가맹점 관리자
        if(strUserCateVal.equals("01"))
        {
          //TID 정보
           List list_onfftid_info = appDAO.GetOnffTidInfo(appFormBean);  
           
           for(int i=0 ;i < list_onfftid_info.size() ; i++ )
           {
               AppOnffTidInfoVo onfftidInfo = (AppOnffTidInfoVo)list_onfftid_info.get(i);
               
               sbtid.append("<option value="+onfftidInfo.getOnfftid()+">"+onfftidInfo.getOnfftid_nm()+"</option>");
           }
           
           ht.put("onfftid_info", sbtid.toString());
            
           //가맹점 정보
           List list_merch_info = appDAO.GetOnffMerchInfo(appFormBean);
        
           Tb_Onffmerch_Mst merch_info =  (Tb_Onffmerch_Mst)list_merch_info.get(0);
           
           sbmerch.append("<option value="+merch_info.getOnffmerch_no()+">"+merch_info.getMerch_nm()+"</option>");
        
           ht.put("onffmerch_info", sbmerch.toString());
           
        }
        //결제관리자
        else if(strUserCateVal.equals("02"))
        {
            ////TID 정보
           List list_onfftid_info = appDAO.GetOnffTidInfo(appFormBean);
           for(int i=0 ;i < list_onfftid_info.size() ; i++ )
           {
               AppOnffTidInfoVo onfftidInfo = (AppOnffTidInfoVo)list_onfftid_info.get(i);
               
               sbtid.append("<option value="+onfftidInfo.getOnfftid()+">"+onfftidInfo.getOnfftid_nm()+"</option>");
               sbmerch.append("<option value="+onfftidInfo.getOnffmerch_no()+">"+onfftidInfo.getMerch_nm()+"</option>");
           }
           
          ht.put("onfftid_info", sbtid.toString());
          ht.put("onffmerch_info", sbmerch.toString());          
        }
        
        return ht;
    }

    @Override
    @Transactional
    public Hashtable appReqForm(AppFormBean appFormBean) throws Exception {
        Hashtable ht = new Hashtable();
        
        logger.trace("----------------------------------------------------------------");
        logger.trace("AppServiceImpl appReqForm appFormBean.getOnfftid val : " + appFormBean.getOnfftid());
        logger.trace("AppServiceImpl appReqForm appFormBean.getOnffmerch_no val : " + appFormBean.getOnffmerch_no());
        logger.trace("AppServiceImpl appReqForm appFormBean.getUser_cate val : " + appFormBean.getUser_cate());
        
        logger.trace("AppServiceImpl appReqForm appFormBean.getParam_onffmerch_no val : " + appFormBean.getParam_onffmerch_no());
        logger.trace("AppServiceImpl appReqForm appFormBean.getParam_onfftid val : " + appFormBean.getParam_onfftid());
        logger.trace("AppServiceImpl appReqForm appFormBean.getUser_cate val : " + appFormBean.getUser_cate());
        
        logger.trace("----------------------------------------------------------------");
        
        //온오프가맹점정보를 셋팅한다.
        String strUserCateVal = appFormBean.getUser_cate();
        
        //가맹점 관리자
        if(strUserCateVal.equals("01"))
        {
           //가맹점 번호의 검사
           if(!appFormBean.getParam_onffmerch_no().equals(appFormBean.getOnffmerch_no()) || appFormBean.getOnffmerch_no() == null)
           {
               logger.trace("-----------------------strUserCateVal : 0 => 가맹점번호 검사 ----------------------------------");
               ht.put("result_code", "1");
               ht.put("result_msg", "사용할수 없는 가맹점번호 입니다.");
               return ht;
           }           
           //TID 정보
           List list_onfftid_info = appDAO.GetOnffTidInfo(appFormBean);  
           if(list_onfftid_info.size() == 0)
           {
               logger.trace("-----------------------strUserCateVal : 0 => TID 검사 ----------------------------------");
               ht.put("result_code", "2");
               ht.put("result_msg", "사용할수 없는 결제TID 입니다.");
               return ht;
           }           
           
           AppOnffTidInfoVo onfftidInfo = (AppOnffTidInfoVo)list_onfftid_info.get(0);
           ht.put("onfftid_info", onfftidInfo);
           
        }
        //결제관리자
        else if(strUserCateVal.equals("02"))
        {
           //TID번호의 검사
           if(!appFormBean.getParam_onfftid().equals(appFormBean.getOnfftid()) || appFormBean.getOnfftid() == null)
           {
               logger.trace("-----------------------strUserCateVal : 02 => TID 검사 ----------------------------------");
               ht.put("result_code", "2");
               ht.put("result_msg", "사용할수 없는 결제TID 입니다.");
               return ht;
           }                 
            
            ////TID 정보
           List list_onfftid_info = appDAO.GetOnffTidInfo(appFormBean);
           AppOnffTidInfoVo onfftidInfo = (AppOnffTidInfoVo)list_onfftid_info.get(0);
           
           ht.put("onfftid_info", onfftidInfo);
        }
        
        ht.put("result_code", "0");
        //가맹점주문번호를 생성한다.
        //String strOrder_seq = appDAO.GetOrderSeq();
        //ht.put("order_seq", strOrder_seq);
        
        return ht;
    }
    
    
    @Override
    @Transactional
    public Hashtable appReqAction(AppFormBean appFormBean) throws Exception {
        Hashtable ht = new Hashtable();

        //가맹점 번호 검사
        
        logger.trace("----------------------------------------------------------------");
        logger.trace("AppServiceImpl appReqAction appFormBean.getOnfftid val : " + appFormBean.getOnfftid());
        logger.trace("AppServiceImpl appReqAction appFormBean.getOnffmerch_no val : " + appFormBean.getOnffmerch_no());
        logger.trace("AppServiceImpl appReqAction appFormBean.getUser_cate val : " + appFormBean.getUser_cate());
        
        logger.trace("AppServiceImpl appReqAction appFormBean.getParam_onffmerch_no val : " + appFormBean.getParam_onffmerch_no());
        logger.trace("AppServiceImpl appReqAction appFormBean.getParam_onfftid val : " + appFormBean.getParam_onfftid());
        logger.trace("----------------------------------------------------------------");
        
        String strUserCateVal = appFormBean.getUser_cate();
        AppOnffTidInfoVo onfftidInfo = null;
        //가맹점 관리자
        if(strUserCateVal.equals("01"))
        {
           //가맹점 번호의 검사
           if(!appFormBean.getParam_onffmerch_no().equals(appFormBean.getOnffmerch_no()) || appFormBean.getOnffmerch_no() == null)
           {
               logger.trace("----------------------- appReqAction strUserCateVal : 0 => 가맹점번호 검사 ----------------------------------");
               //throw new Exception("사용할수 없는 가맹점번호 입니다.");
               ht.put("RESULT_CD", "OF01");
               ht.put("RESULT_MSG", "사용할수 없는 가맹점번호 입니다.");
               
               return ht;
           }           
           //TID 정보
           List list_onfftid_info = appDAO.GetOnffTidInfo(appFormBean);  
           if(list_onfftid_info.size() == 0)
           {
               logger.trace("----------------------- appReqAction strUserCateVal : 0 => TID 검사 ----------------------------------");
               //throw new Exception("사용할수 없는 결제TID 입니다.");
               ht.put("RESULT_CD", "OF02");
               ht.put("RESULT_MSG", "사용할수 없는 결제TID 입니다.(01)");
               
               return ht;
           }           
           
           onfftidInfo = (AppOnffTidInfoVo)list_onfftid_info.get(0);
        }
        //결제관리자
        else if(strUserCateVal.equals("02"))
        {
           //TID번호의 검사
           if(!appFormBean.getParam_onfftid().equals(appFormBean.getOnfftid()) || appFormBean.getOnfftid() == null)
           {
               logger.trace("----------------------- appReqAction strUserCateVal : 02 => TID 검사 ----------------------------------");
               //throw new Exception("사용할수 없는 결제TID 입니다.");
               ht.put("RESULT_CD", "OF02");
               ht.put("RESULT_MSG", "사용할수 없는 결제TID 입니다.(02)");
               
               return ht;               
           }                 
            
            ////TID 정보
           List list_onfftid_info = appDAO.GetOnffTidInfo(appFormBean);
           onfftidInfo = (AppOnffTidInfoVo)list_onfftid_info.get(0);
        }        
        logger.trace(" appReqAction onfftidInfo.toString : "+ onfftidInfo.toString() +" ----------------------------------");
        
        //결제관련 ONFFTID 정보셋팅
        ht.put("onfftidInfo", onfftidInfo);
        
        
        KiccPgVo reqcardinfoobj = appFormBean.getReqKiccPgVo();
        //결제급액 검사
            String strTpTotAmt = Util.getNullToZero(reqcardinfoobj.getTot_amt()).trim();
            String strTpComTaxAmt = Util.getNullToZero(reqcardinfoobj.getCom_tax_amt()).trim();
            String strTpComFreeAmt = Util.getNullToZero(reqcardinfoobj.getCom_free_amt()).trim();
            String strTpComVatAmt = Util.getNullToZero(reqcardinfoobj.getCom_vat_amt()).trim();

            int intTpTotAmt = Integer.parseInt(strTpTotAmt);
            int intTpComTaxAmt = Integer.parseInt(strTpComTaxAmt);
            int intTpComFreeAmt = Integer.parseInt(strTpComFreeAmt);
            int intTpComVatAmt = Integer.parseInt(strTpComVatAmt);
            
            if(intTpTotAmt <= 0 )
            {
                ht.put("RESULT_CD", "OF80");
                ht.put("RESULT_MSG", "결제금액이 잘못되었습니다.");
                return ht;                   
            }
            
            if( intTpTotAmt!=(intTpComTaxAmt+intTpComFreeAmt+intTpComVatAmt) )
            {
                ht.put("RESULT_CD", "OF80");
                ht.put("RESULT_MSG", "결제금액이 잘못되었습니다.");
                return ht;                 
            }
        
        
        
        logger.trace("----------------------- appReqAction reqcardinfoobj.getTot_amt() : "+ reqcardinfoobj.getTot_amt() +" ----------------------------------");
        appFormBean.setParam_appAmt(reqcardinfoobj.getTot_amt());
        
        logger.trace("----------------------- appReqAction strAmtChkFlag appFormBean.getParam_onffmerch_no() : "+ appFormBean.getParam_onffmerch_no() +" ----------------------------------");
        logger.trace("----------------------- appReqAction strAmtChkFlag appFormBean.getOnffmerch_no() : "+ appFormBean.getOnffmerch_no() +" ----------------------------------");
        logger.trace("----------------------- appReqAction strAmtChkFlag appFormBean.getParam_appAmt() : "+ appFormBean.getParam_appAmt() +" ----------------------------------");
        //한도검사
        String strAmtChkFlag = appDAO.GetappChkAmt(appFormBean);
        logger.trace("----------------------- appReqAction strAmtChkFlag : "+ strAmtChkFlag +" ----------------------------------");
        
        // 결제금액이 한도금액 초과인 경우 
        if(!strAmtChkFlag.equals("0"))
        {
            //throw new Exception("월결제 총액이 결제한도를 초과하였습니다.");
            ht.put("RESULT_CD", "OF04");
            ht.put("RESULT_MSG", "월결제 총액이 결제한도를 초과하였습니다.");
            return ht;             
        }
        
        
        //건별 결제한도 금액 확인
        String strTpAppReqChkAmt = Util.nullToInt(onfftidInfo.getAppreq_chk_amt());
        int intTpAppReqChkAmt = Integer.parseInt(strTpAppReqChkAmt);
                
        //건별 결제한도 금액이 0보다 크다면 금액검사
        if(intTpAppReqChkAmt > 0)
        {
            int intTpappAmt = Integer.parseInt(Util.nullToInt(appFormBean.getParam_appAmt()));
            
            if(intTpappAmt > intTpAppReqChkAmt)
            {
                ht.put("RESULT_CD", "OF05");
                ht.put("RESULT_MSG", "건별 결제한도를 초과하였습니다.");
                return ht;            
            }
            
        }
        
        
        //카드번호로 발급사 정보가져오기
       logger.trace("----------------------- reqcardinfoobj.getCard_no() : "+ reqcardinfoobj.getCard_no() +" ----------------------------------");
       String strTpCardNo = reqcardinfoobj.getCard_no();
       String strChkCardBean = strTpCardNo.substring(0, 6);
       appFormBean.setChkCardBean(strChkCardBean);
       
       String CardIssCd = appDAO.GetCardBeanInfo(appFormBean);
       logger.trace("-----------------------CardIssCd : "+ CardIssCd +" ----------------------------------");
       
       //카드빈에 발급사가 있는 경우
       List ls_TermInfo = null;
       if(CardIssCd != null)
       {
           logger.trace("----------------------- CardIssCd not null ----------------------------------");
            //발급사카드빈 정보로 결제용 TID정보를 가져온다.(결제용 TID)
            appFormBean.setApp_iss_cd(CardIssCd);
            appFormBean.setPay_mtd_seq(onfftidInfo.getPay_mtd_seq());

            ls_TermInfo =  appDAO.GetTerminalInfo(appFormBean);

            logger.trace("----------------------- CardIssCd exist!! ls_TermInfo.size() : "+ ls_TermInfo.size() +" ----------------------------------");
            //카드에 따른 카드 코드가 없는 경우 기본 TID 정보를 가져온다.
            if(ls_TermInfo == null || ls_TermInfo.size()==0)
            {
                appFormBean.setApp_iss_cd("000");
                ls_TermInfo =  appDAO.GetTerminalInfo(appFormBean);
            }

            logger.trace("-----------------------CardIssCd exist!!  ls_TermInfo.size() 2 : "+ ls_TermInfo.size() +" ----------------------------------");
            AppTerminalInfoVo appTidObj = (AppTerminalInfoVo) ls_TermInfo.get(0);
            logger.trace("-- appTidObj.toString() : "+ appTidObj.toString() +" --");
            //결제용 Terminal 정보 셋팅
            ht.put("appTidObj", appTidObj);            
       }
       //카드빈에 발급사가 없는 경우
       else
       {
           logger.trace("----------------------- CardIssCd null ----------------------------------");
            appFormBean.setPay_mtd_seq(onfftidInfo.getPay_mtd_seq());
            appFormBean.setApp_iss_cd("000");
            ls_TermInfo =  appDAO.GetTerminalInfo(appFormBean);
        
            logger.trace("----------------------- CardIssCd not exist!! ls_TermInfo.size() : "+ ls_TermInfo.size() +" ----------------------------------");
            
            AppTerminalInfoVo appTidObj = (AppTerminalInfoVo) ls_TermInfo.get(0);
            logger.trace("-- appTidObj.toString() : "+ appTidObj.toString() +" --");
            //결제용 Terminal 정보 셋팅
            ht.put("appTidObj", appTidObj);               
       }
       
        ht.put("RESULT_CD", "0000");
        
        return ht;
    }
        
    @Override
    @Transactional
    public Hashtable appKiccReqAction(AppFormBean appFormBean, AppOnffTidInfoVo appOnffTidInfoVo, AppTerminalInfoVo appTerminalInfoVo ) throws Exception {
        Hashtable ht = new Hashtable();
        
        KiccPgVo reqpgobj = appFormBean.getReqKiccPgVo();
        KiccPgVo respgobj = new KiccPgVo();
        
        Date today=new Date();
        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmss");       
        String strCurYear = formater.format(today);

        String g_cert_file  = reqpgobj.getKiccpg_cert_file();
	String g_log_dir    = reqpgobj.getKiccpg_g_log_dir();
	int g_log_level  = 1;
        
	// ============================================================================== /
	// =   02. 쇼핑몰 지불 정보 설정                                                = /
	// = -------------------------------------------------------------------------- = /		
        String g_gw_url    = reqpgobj.getKiccpg_g_gw_url();
        String g_gw_port   = reqpgobj.getKiccpg_g_gw_port();
        
	String g_mall_id   = appTerminalInfoVo.getTerminal_no();             // 리얼 반영시 KICC에 발급된 mall_id 사용
        String g_mall_name = appOnffTidInfoVo.getComp_nm();    
        
        if(g_mall_id == null || g_mall_id.equals(""))
        {
            throw new Exception("결제용TID가 없습니다.");
        }
        
                //가맹점주문번호를 생성한다.
        String strOrder_seq = appDAO.GetOrderSeq();
        
        reqpgobj.setOrder_no(strOrder_seq);

        logger.trace("------------------------------appKiccReqAction--------------------------------------");
        logger.trace("onffmerchno : " + appOnffTidInfoVo.getOnffmerch_no());
        logger.trace("onfftid : " + appOnffTidInfoVo.getOnfftid());
        logger.trace("Pay_mtd_seq : " + appOnffTidInfoVo.getPay_mtd_seq());        
        logger.trace("g_cert_file : " + g_cert_file);
        logger.trace("g_log_dir : " + g_log_dir);
        logger.trace("g_gw_url : " + g_gw_url);
        logger.trace("g_gw_port : " + g_gw_port);
        logger.trace("g_mall_id : " + g_mall_id);
        logger.trace("g_mall_name : " + g_mall_name);
        logger.trace("reqpgobj.getUser_nm() : " + reqpgobj.getUser_nm());
        logger.trace("reqpgobj.getProduct_nm() : " + reqpgobj.getProduct_nm());
        logger.trace("reqpgobj.getCard_no() : " + reqpgobj.getCard_no());
        logger.trace("reqpgobj.getCom_free_amt() : " + reqpgobj.getCom_free_amt());
        logger.trace("reqpgobj.getCom_tax_amt() : " + reqpgobj.getCom_tax_amt());
        logger.trace("reqpgobj.getCom_vat_amt() : " + reqpgobj.getCom_vat_amt());
        logger.trace("reqpgobj.getProduct_amt() : " + reqpgobj.getProduct_amt());
        logger.trace("reqpgobj.getCard_amt() : " + reqpgobj.getCard_amt());
        logger.trace("reqpgobj.getTax_flg() : " + reqpgobj.getTax_flg());
        logger.trace("--------------------------------------------------------------------");        
        
        //수수료정보를 가져온다.
        appFormBean.setPay_mtd_seq(appOnffTidInfoVo.getPay_mtd_seq());
        List ls_onfftidcmsinfo = appDAO.GetOnffTidCommission(appFormBean);
        AppOnffTidInfoVo onfftidcmsinfo = (AppOnffTidInfoVo)ls_onfftidcmsinfo.get(0);
        
	logger.trace("------------------------------appKiccReqAction--------------------------------------");   
        logger.trace("getOnfftid_cms_seq : " + onfftidcmsinfo.getOnfftid_cms_seq());        
        logger.trace("getOnfftid : " + onfftidcmsinfo.getOnfftid());      
        logger.trace("getCommission : " + onfftidcmsinfo.getCommission());        
        logger.trace("getStart_dt : " + onfftidcmsinfo.getStart_dt());        
        logger.trace("getEnd_dt : " + onfftidcmsinfo.getEnd_dt());        
        logger.trace("--------------------------------------------------------------------");  
        
        /* -------------------------------------------------------------------------- */
        /* ::: EasyPayClient 인스턴스 생성 [변경불가 !!].                             */
        /* -------------------------------------------------------------------------- */
        EasyPayClient easyPayClient = new EasyPayClient();
        easyPayClient.easypay_setenv_init( g_gw_url, g_gw_port, g_cert_file, g_log_dir );
        easyPayClient.easypay_reqdata_init();
        
        String TRAN_CD_NOR_PAYMENT = reqpgobj.getTRAN_CD_NOR_PAYMENT();
        String tr_cd = reqpgobj.getTr_cd();
        String req_cno = reqpgobj.getReq_cno();
        String pay_type = reqpgobj.getPay_type();
        String req_type = reqpgobj.getReq_type();
        String cert_type = reqpgobj.getCert_type();
        
        String TRAN_CD_NOR_MGR = reqpgobj.getTRAN_CD_NOR_MGR();

            /* -------------------------------------------------------------------------- */
            /* ::: 결제 결과                                                              */
            /* -------------------------------------------------------------------------- */
            String bDBProc          = "";
            String res_cd           = "";
            String res_msg          = "";
            String r_order_no       = "";
            String r_complex_yn     = "";
            String r_msg_type       = "";     //거래구분
            String r_noti_type	    = "";     //노티구분
            String r_cno            = "";     //PG거래번호
            String r_amount         = "";     //총 결제금액
            String r_auth_no        = "";     //승인번호
            String r_tran_date      = "";     //거래일시
            String r_pnt_auth_no    = "";     //포인트 승인 번호
            String r_pnt_tran_date  = "";     //포인트 승인 일시
            String r_cpon_auth_no   = "";     //쿠폰 승인 번호
            String r_cpon_tran_date = "";     //쿠폰 승인 일시
            String r_card_no        = "";     //카드번호
            String r_issuer_cd      = "";     //발급사코드
            String r_issuer_nm      = "";     //발급사명
            String r_acquirer_cd    = "";     //매입사코드
            String r_acquirer_nm    = "";     //매입사명
            String r_install_period = "";     //할부개월
            String r_noint          = "";     //무이자여부
            String r_bank_cd        = "";     //은행코드
            String r_bank_nm        = "";     //은행명
            String r_account_no     = "";     //계좌번호
            String r_deposit_nm     = "";     //입금자명
            String r_expire_date    = "";     //계좌사용 만료일
            String r_cash_res_cd    = "";     //현금영수증 결과코드
            String r_cash_res_msg   = "";     //현금영수증 결과메세지
            String r_cash_auth_no   = "";     //현금영수증 승인번호
            String r_cash_tran_date = "";     //현금영수증 승인일시
            String r_auth_id        = "";     //PhoneID
            String r_billid         = "";     //인증번호
            String r_mobile_no      = "";     //휴대폰번호
            String r_ars_no         = "";     //ARS 전화번호
            String r_cp_cd          = "";     //포인트사
            String r_used_pnt       = "";     //사용포인트
            String r_remain_pnt     = "";     //잔여한도
            String r_pay_pnt        = "";     //할인/발생포인트
            String r_accrue_pnt     = "";     //누적포인트
            String r_remain_cpon    = "";     //쿠폰잔액
            String r_used_cpon      = "";     //쿠폰 사용금액
            String r_mall_nm        = "";     //제휴사명칭
            String r_escrow_yn	    = "";     //에스크로 사용유무
            String r_canc_acq_data  = "";     //매입취소일시
            String r_canc_date      = "";     //취소일시
            String r_refund_date    = "";     //환불예정일시
            String r_join_no = ""; //카드사가맹점번호
        
        /* -------------------------------------------------------------------------- */
        /* ::: 승인요청 전문 설정                                                     */
        /* -------------------------------------------------------------------------- */
        if( TRAN_CD_NOR_PAYMENT.equals(tr_cd) ) {

            // 승인요청 전문 설정
            // 결제 주문 정보 DATA
            int easypay_order_data_item;
            easypay_order_data_item = easyPayClient.easypay_item( "order_data" );

            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_type"		,reqpgobj.getUser_type() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "order_no"		,reqpgobj.getOrder_no()		);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "memb_user_no"	,reqpgobj.getMemb_user_no() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_id"		,reqpgobj.getUser_id()		);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_nm"		,reqpgobj.getUser_nm()		);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_mail"		,reqpgobj.getUser_mail() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_phone1"	,reqpgobj.getUser_phone1() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_phone2"	,reqpgobj.getUser_phone2() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_addr"		,reqpgobj.getUser_addr() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "product_type"	,reqpgobj.getProduct_type() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "product_nm"	,reqpgobj.getProduct_nm() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "product_amt"	,reqpgobj.getProduct_amt() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_define1"	,reqpgobj.getUser_define1() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_define2"	,reqpgobj.getUser_define2() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_define3"	,reqpgobj.getUser_define3() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_define4"	,reqpgobj.getUser_define4() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_define5"	,reqpgobj.getUser_define5() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_define6"	,reqpgobj.getUser_define6() 	);

            // 결제정보 DATA부
            int easypay_pay_data_item;
            easypay_pay_data_item = easyPayClient.easypay_item( "pay_data" );

            // 결제 공통 정보 DATA
            int easypay_common_item;
            easypay_common_item = easyPayClient.easypay_item( "common" );

            easyPayClient.easypay_deli_us( easypay_common_item, "tot_amt"       , reqpgobj.getTot_amt()       );
            easyPayClient.easypay_deli_us( easypay_common_item, "currency"      , reqpgobj.getCurrency()      );
            easyPayClient.easypay_deli_us( easypay_common_item, "client_ip"     , reqpgobj.getClient_ip()     );
            easyPayClient.easypay_deli_us( easypay_common_item, "escrow_yn"     , reqpgobj.getEscrow_yn()     );
            easyPayClient.easypay_deli_us( easypay_common_item, "complex_yn"    , reqpgobj.getComplex_yn()    );
            easyPayClient.easypay_deli_us( easypay_common_item, "tax_flg"       , reqpgobj.getTax_flg()      );
            easyPayClient.easypay_deli_us( easypay_common_item, "com_tax_amt"   , reqpgobj.getCom_tax_amt()  );
            easyPayClient.easypay_deli_us( easypay_common_item, "com_free_amt"  , reqpgobj.getCom_free_amt() );
            easyPayClient.easypay_deli_us( easypay_common_item, "com_vat_amt"   , reqpgobj.getCom_vat_amt()  );

            if ( !req_cno.equals( "" ) )
            easyPayClient.easypay_deli_us( easypay_common_item, "req_cno", reqpgobj.getReq_cno()      );
            easyPayClient.easypay_deli_rs( easypay_pay_data_item , easypay_common_item );

            // 신용카드 결제 DATA SET
            if( pay_type.equals("card") ) {
                int easypay_card_item;

                easypay_card_item = easyPayClient.easypay_item( "card" );

                easyPayClient.easypay_deli_us( easypay_card_item, "card_txtype"    , reqpgobj.getCard_txtype()       );
                easyPayClient.easypay_deli_us( easypay_card_item, "req_type"       , reqpgobj.getReq_type()          );
                easyPayClient.easypay_deli_us( easypay_card_item, "card_amt"       , reqpgobj.getCard_amt()          );
                easyPayClient.easypay_deli_us( easypay_card_item, "noint"          , reqpgobj.getNoint()             );
                easyPayClient.easypay_deli_us( easypay_card_item, "cert_type"      , reqpgobj.getCert_type()         );
                easyPayClient.easypay_deli_us( easypay_card_item, "wcc"            , reqpgobj.getWcc()               );
                easyPayClient.easypay_deli_us( easypay_card_item, "install_period" , reqpgobj.getInstall_period()    );

                if( req_type.equals("0") ) {
                    // 일반(SSL)
                    easyPayClient.easypay_deli_us( easypay_card_item, "card_no"        , reqpgobj.getCard_no()           );
                    easyPayClient.easypay_deli_us( easypay_card_item, "expire_date"    , reqpgobj.getExpire_date()       );
                    easyPayClient.easypay_deli_us( easypay_card_item, "user_type"      , reqpgobj.getCard_user_type()    );

                    if( cert_type.equals("0") ) {

                        easyPayClient.easypay_deli_us( easypay_card_item, "password"       , reqpgobj.getPassword()          );
                        easyPayClient.easypay_deli_us( easypay_card_item, "auth_value"     , reqpgobj.getAuth_value()        );
                    }
                }

                easyPayClient.easypay_deli_rs( easypay_pay_data_item , easypay_card_item );
            }

        /* -------------------------------------------------------------------------- */
        /* ::: 변경관리 요청                                                          */
        /* -------------------------------------------------------------------------- */
        }else if( TRAN_CD_NOR_MGR.equals( tr_cd ) ) {

            int easypay_mgr_data_item;
            easypay_mgr_data_item = easyPayClient.easypay_item( "mgr_data" );

            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_txtype"		,  reqpgobj.getMgr_txtype() 	);
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_subtype"		,  reqpgobj.getMgr_subtype() 	);
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "org_cno"			,  reqpgobj.getOrg_cno() 		);
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "order_no"		,  reqpgobj.getOrder_no()		);
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "pay_type"		,  reqpgobj.getPay_type()		);
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_amt"			,  reqpgobj.getMgr_amt()		);
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_bank_cd"		,  reqpgobj.getMgr_bank_cd() 	);
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_account"		,  reqpgobj.getMgr_account() 	);
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_depositor"	,  reqpgobj.getMgr_depositor() );
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_socno"		,  reqpgobj.getMgr_socno() 	);
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_telno"		,  reqpgobj.getMgr_telno() 	);
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "deli_cd"			,  reqpgobj.getDeli_cd() 		);
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "deli_corp_cd"	,  reqpgobj.getDeli_corp_cd() 	);
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "deli_invoice"	,  reqpgobj.getDeli_invoice() 	);
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "deli_rcv_nm"		,  reqpgobj.getDeli_rcv_nm() 	);
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "deli_rcv_tel"	,  reqpgobj.getDeli_rcv_tel() 	);
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "req_ip"			,  reqpgobj.getClient_ip()		);
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "req_id"			,  reqpgobj.getReq_id()        );
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_msg"			,  reqpgobj.getMgr_msg()		);
        }

        /* -------------------------------------------------------------------------- */
        /* ::: 실행                                                                   */
        /* -------------------------------------------------------------------------- */
        if ( tr_cd.length() > 0 ) {
            easyPayClient.easypay_run( g_mall_id, tr_cd, reqpgobj.getOrder_no() );

            res_cd = easyPayClient.res_cd;
            res_msg = easyPayClient.res_msg;

        }
        else {
            res_cd  = "M114";
            res_msg = "연동 오류|tr_cd값이 설정되지 않았습니다.";
        }
        
	logger.trace("--------------------------------------------------------------------");
        logger.trace("res_cd : " + res_cd);
        logger.trace("res_msg : " + res_msg);
        logger.trace("--------------------------------------------------------------------");     
        
        /* -------------------------------------------------------------------------- */
        /* ::: 결과 처리                                                              */
        /* -------------------------------------------------------------------------- */
        r_cno             = Util.nullToSpaceString(easyPayClient.easypay_get_res( "cno"             ));    // PG거래번호
        r_amount          = Util.nullToSpaceString(easyPayClient.easypay_get_res( "amount"          ));    //총 결제금액
        r_order_no        = Util.nullToSpaceString(easyPayClient.easypay_get_res( "order_no"        ));    //주문번호
        r_noti_type       = Util.nullToSpaceString(easyPayClient.easypay_get_res( "noti_type"       ));    //노티구분
        r_auth_no         = Util.nullToSpaceString(easyPayClient.easypay_get_res( "auth_no"         ));    //승인번호
        r_tran_date       = Util.nullToSpaceString(easyPayClient.easypay_get_res( "tran_date"       ));    //승인일시
        r_pnt_auth_no     = Util.nullToSpaceString(easyPayClient.easypay_get_res( "pnt_auth_no"     ));    //포인트승인번호
        r_pnt_tran_date   = Util.nullToSpaceString(easyPayClient.easypay_get_res( "pnt_tran_date"   ));    //포인트승인일시
        r_cpon_auth_no    = Util.nullToSpaceString(easyPayClient.easypay_get_res( "cpon_auth_no"    ));    //쿠폰승인번호
        r_cpon_tran_date  = Util.nullToSpaceString(easyPayClient.easypay_get_res( "cpon_tran_date"  ));    //쿠폰승인일시
        r_card_no         = Util.nullToSpaceString(easyPayClient.easypay_get_res( "card_no"         ));    //카드번호
        r_issuer_cd       = Util.nullToSpaceString(easyPayClient.easypay_get_res( "issuer_cd"       ));    //발급사코드
        r_issuer_nm       = Util.nullToSpaceString(easyPayClient.easypay_get_res( "issuer_nm"       ));    //발급사명
        r_acquirer_cd     = Util.nullToSpaceString(easyPayClient.easypay_get_res( "acquirer_cd"     ));    //매입사코드
        r_acquirer_nm     = Util.nullToSpaceString(easyPayClient.easypay_get_res( "acquirer_nm"     ));    //매입사명
        r_install_period  = Util.nullToSpaceString(easyPayClient.easypay_get_res( "install_period"  ));    //할부개월
        r_noint           = Util.nullToSpaceString(easyPayClient.easypay_get_res( "noint"           ));    //무이자여부
        r_bank_cd         = Util.nullToSpaceString(easyPayClient.easypay_get_res( "bank_cd"         ));    //은행코드
        r_bank_nm         = Util.nullToSpaceString(easyPayClient.easypay_get_res( "bank_nm"         ));    //은행명
        r_account_no      = Util.nullToSpaceString(easyPayClient.easypay_get_res( "account_no"      ));    //계좌번호
        r_deposit_nm      = Util.nullToSpaceString(easyPayClient.easypay_get_res( "deposit_nm"      ));    //입금자명
        r_expire_date     = Util.nullToSpaceString(easyPayClient.easypay_get_res( "expire_date"     ));    //계좌사용만료일
        r_cash_res_cd     = Util.nullToSpaceString(easyPayClient.easypay_get_res( "cash_res_cd"     ));    //현금영수증 결과코드
        r_cash_res_msg    = Util.nullToSpaceString(easyPayClient.easypay_get_res( "cash_res_msg"    ));    //현금영수증 결과메세지
        r_cash_auth_no    = Util.nullToSpaceString(easyPayClient.easypay_get_res( "cash_auth_no"    ));    //현금영수증 승인번호
        r_cash_tran_date  = Util.nullToSpaceString(easyPayClient.easypay_get_res( "cash_tran_date"  ));    //현금영수증 승인일시
        r_auth_id         = Util.nullToSpaceString(easyPayClient.easypay_get_res( "auth_id"         ));    //PhoneID
        r_billid          = Util.nullToSpaceString(easyPayClient.easypay_get_res( "billid"          ));    //인증번호
        r_mobile_no       = Util.nullToSpaceString(easyPayClient.easypay_get_res( "mobile_no"       ));    //휴대폰번호
        r_ars_no          = Util.nullToSpaceString(easyPayClient.easypay_get_res( "ars_no"          ));    //전화번호
        r_cp_cd           = Util.nullToSpaceString(easyPayClient.easypay_get_res( "cp_cd"           ));    //포인트사/쿠폰사
        r_used_pnt        = Util.nullToSpaceString(easyPayClient.easypay_get_res( "used_pnt"        ));    //사용포인트
        r_remain_pnt      = Util.nullToSpaceString(easyPayClient.easypay_get_res( "remain_pnt"      ));    //잔여한도
        r_pay_pnt         = Util.nullToSpaceString(easyPayClient.easypay_get_res( "pay_pnt"         ));    //할인/발생포인트
        r_accrue_pnt      = Util.nullToSpaceString(easyPayClient.easypay_get_res( "accrue_pnt"      ));    //누적포인트
        r_remain_cpon     = Util.nullToSpaceString(easyPayClient.easypay_get_res( "remain_cpon"     ));    //쿠폰잔액
        r_used_cpon       = Util.nullToSpaceString(easyPayClient.easypay_get_res( "used_cpon"       ));    //쿠폰 사용금액
        r_mall_nm         = Util.nullToSpaceString(easyPayClient.easypay_get_res( "mall_nm"         ));    //제휴사명칭
        r_escrow_yn       = Util.nullToSpaceString(easyPayClient.easypay_get_res( "escrow_yn"       ));    //에스크로 사용유무
        r_complex_yn      = Util.nullToSpaceString(easyPayClient.easypay_get_res( "complex_yn"      ));    //복합결제 유무
        r_canc_acq_data   = Util.nullToSpaceString(easyPayClient.easypay_get_res( "canc_acq_data"   ));    //매입취소일시
        r_canc_date       = Util.nullToSpaceString(easyPayClient.easypay_get_res( "canc_date"       ));    //취소일시
        r_refund_date     = Util.nullToSpaceString(easyPayClient.easypay_get_res( "refund_date"     ));    //환불예정일시
        r_join_no    = Util.nullToSpaceString(easyPayClient.easypay_get_res( "join_no"     ));    //카드사가맹점번호
        
        logger.trace("--------------------------------------------------------------------");        
        logger.trace("r_cno            "+       r_cno             );// PG거래번호
        logger.trace("r_amount         "+       r_amount          );//총 결제금액
        logger.trace("r_order_no       "+       r_order_no        );//주문번호
        logger.trace("r_noti_type      "+       r_noti_type       );//노티구분
        logger.trace("r_auth_no        "+       r_auth_no         );//승인번호
        logger.trace("r_tran_date      "+       r_tran_date       );//승인일시
        logger.trace("r_pnt_auth_no    "+       r_pnt_auth_no     );//포인트승인번호
        logger.trace("r_pnt_tran_date  "+       r_pnt_tran_date   );//포인트승인일시
        logger.trace("r_cpon_auth_no   "+       r_cpon_auth_no    );//쿠폰승인번호
        logger.trace("r_cpon_tran_date "+       r_cpon_tran_date  );//쿠폰승인일시
        logger.trace("r_card_no        "+       r_card_no         );//카드번호
        logger.trace("r_issuer_cd      "+       r_issuer_cd       );//발급사코드
        logger.trace("r_issuer_nm      "+       r_issuer_nm       );//발급사명
        logger.trace("r_acquirer_cd    "+       r_acquirer_cd     );//매입사코드
        logger.trace("r_acquirer_nm    "+       r_acquirer_nm     );//매입사명
        logger.trace("r_install_period "+       r_install_period  );//할부개월
        logger.trace("r_noint          "+       r_noint           );//무이자여부
        logger.trace("r_bank_cd        "+       r_bank_cd         );//은행코드
        logger.trace("r_bank_nm        "+       r_bank_nm         );//은행명
        logger.trace("r_account_no     "+       r_account_no      );//계좌번호
        logger.trace("r_deposit_nm     "+       r_deposit_nm      );//입금자명
        logger.trace("r_expire_date    "+       r_expire_date     );//계좌사용만료일
        logger.trace("r_cash_res_cd    "+       r_cash_res_cd     );//현금영수증 결과코드
        logger.trace("r_cash_res_msg   "+       r_cash_res_msg    );//현금영수증 결과메세지
        logger.trace("r_cash_auth_no   "+       r_cash_auth_no    );//현금영수증 승인번호
        logger.trace("r_cash_tran_date "+       r_cash_tran_date  );//현금영수증 승인일시
        logger.trace("r_auth_id        "+       r_auth_id         );//PhoneID
        logger.trace("r_billid         "+       r_billid          );//인증번호
        logger.trace("r_mobile_no      "+       r_mobile_no       );//휴대폰번호
        logger.trace("r_ars_no         "+       r_ars_no          );//전화번호
        logger.trace("r_cp_cd          "+       r_cp_cd           );//포인트사/쿠폰사
        logger.trace("r_used_pnt       "+       r_used_pnt        );//사용포인트
        logger.trace("r_remain_pnt     "+       r_remain_pnt      );//잔여한도
        logger.trace("r_pay_pnt        "+       r_pay_pnt         );//할인/발생포인트
        logger.trace("r_accrue_pnt     "+       r_accrue_pnt      );//누적포인트
        logger.trace("r_remain_cpon    "+       r_remain_cpon     );//쿠폰잔액
        logger.trace("r_used_cpon      "+       r_used_cpon       );//쿠폰 사용금액
        logger.trace("r_mall_nm        "+       r_mall_nm         );//제휴사명칭
        logger.trace("r_escrow_yn      "+       r_escrow_yn       );//에스크로 사용유무
        logger.trace("r_complex_yn     "+       r_complex_yn      );//복합결제 유무
        logger.trace("r_canc_acq_data  "+       r_canc_acq_data   );//매입취소일시
        logger.trace("r_canc_date      "+       r_canc_date       );//취소일시
        logger.trace("r_refund_date    "+       r_refund_date     );//환불예정일시
        logger.trace("r_join_no        "+       r_join_no     );//카드사가맹점번호
        logger.trace("--------------------------------------------------------------------");
    
        respgobj.setR_cno             (r_cno            );
        respgobj.setR_amount          (r_amount         );
        respgobj.setR_order_no        (r_order_no       );
        respgobj.setR_noti_type       (r_noti_type      );
        respgobj.setR_auth_no         (r_auth_no        );
        respgobj.setR_tran_date       (r_tran_date      );
        respgobj.setR_pnt_auth_no     (r_pnt_auth_no    );
        respgobj.setR_pnt_tran_date   (r_pnt_tran_date  );
        respgobj.setR_cpon_auth_no    (r_cpon_auth_no   );
        respgobj.setR_cpon_tran_date  (r_cpon_tran_date );
        respgobj.setR_card_no         (r_card_no        );
        respgobj.setR_issuer_cd       (r_issuer_cd      );
        respgobj.setR_issuer_nm       (r_issuer_nm      );
        respgobj.setR_acquirer_cd     (r_acquirer_cd    );
        respgobj.setR_acquirer_nm     (r_acquirer_nm    );
        respgobj.setR_install_period  (r_install_period );
        respgobj.setR_noint           (r_noint          );
        respgobj.setR_bank_cd         (r_bank_cd        );
        respgobj.setR_bank_nm         (r_bank_nm        );
        respgobj.setR_account_no      (r_account_no     );
        respgobj.setR_deposit_nm      (r_deposit_nm     );
        respgobj.setR_expire_date     (r_expire_date    );
        respgobj.setR_cash_res_cd     (r_cash_res_cd    );
        respgobj.setR_cash_res_msg    (r_cash_res_msg   );
        respgobj.setR_cash_auth_no    (r_cash_auth_no   );
        respgobj.setR_cash_tran_date  (r_cash_tran_date );
        respgobj.setR_auth_id         (r_auth_id        );
        respgobj.setR_billid          (r_billid         );
        respgobj.setR_mobile_no       (r_mobile_no      );
        respgobj.setR_ars_no          (r_ars_no         );
        respgobj.setR_cp_cd           (r_cp_cd          );
        respgobj.setR_used_pnt        (r_used_pnt       );
        respgobj.setR_remain_pnt      (r_remain_pnt     );
        respgobj.setR_pay_pnt         (r_pay_pnt        );
        respgobj.setR_accrue_pnt      (r_accrue_pnt     );
        respgobj.setR_remain_cpon     (r_remain_cpon    );
        respgobj.setR_used_cpon       (r_used_cpon      );
        respgobj.setR_mall_nm         (r_mall_nm        );
        respgobj.setR_escrow_yn       (r_escrow_yn      );
        respgobj.setR_complex_yn      (r_complex_yn     );
        respgobj.setR_canc_acq_data   (r_canc_acq_data  );
        respgobj.setR_canc_date       (r_canc_date      );
        respgobj.setR_refund_date     (r_refund_date    );
        respgobj.setR_join_no     (r_join_no    );
        
        //가맹점 DB처리
        //저장정보를 만든다.
    
        InsertCardPgTranBean insobj = new InsertCardPgTranBean();
        //승인 결가에 따라 저장을 위한 테이블명을 만든다.
        //성공관련 data 셋팅
        if (res_cd.equals("0000"))
        {
            insobj.setInstablenm("TB_TRAN_CARDPG_"+r_tran_date.substring(2, 6));
            insobj.setApp_dt(r_tran_date.substring(0, 8));
            insobj.setApp_tm(r_tran_date.substring(8, 14));
            insobj.setResult_status("00");
            insobj.setAcc_chk_flag("0");
            insobj.setTot_amt(r_amount);//결제총금액
                        
        }
        else
        {
            insobj.setInstablenm("TB_TRAN_CARDPG_"+strCurYear.substring(2, 6));
            insobj.setResult_status("99");                        
            insobj.setAcc_chk_flag("3");
            insobj.setTot_amt(reqpgobj.getTot_amt());//결제총금액
        }
        
        String strTpTranseq = appDAO.GetTranSeq();
        insobj.setTran_seq(strTpTranseq);// 거래번호 셋팅
        //카드결제 시
             
                        insobj.setMassagetype("10");//메세지타입
                        //insobj.setCard_num(r_card_no);//카드번호
                        insobj.setCard_num(reqpgobj.getCard_no());//카드번호
                        insobj.setApp_card_num(r_card_no);//카드번호
                        insobj.setApp_no(r_auth_no);//승인번호

                        insobj.setPg_seq(r_cno);//pg거래고유번호
                        insobj.setPay_chn_cate(appOnffTidInfoVo.getPay_chn_cate());
                        insobj.setTid(g_mall_id);
                        insobj.setOnffmerch_no(appOnffTidInfoVo.getOnffmerch_no());        
                        insobj.setOnfftid(appOnffTidInfoVo.getOnfftid());
                        insobj.setWcc(reqpgobj.getWcc());
                        insobj.setExpire_dt(reqpgobj.getExpire_date());
                        insobj.setCard_cate(reqpgobj.getCard_user_type());
                        insobj.setOrder_seq(reqpgobj.getOrder_no());
                        insobj.setTran_dt(strCurYear.substring(0, 8));
                        insobj.setTran_tm(strCurYear.substring(8, 14));
                        insobj.setTran_cate(reqpgobj.getCard_txtype());
                        insobj.setFree_inst_flag(r_noint);
                        insobj.setTax_amt(reqpgobj.getCom_vat_amt());//부가세금액
                        insobj.setSvc_amt(reqpgobj.getCom_free_amt ());//비과세승인금액
                        //insobj.setCurrency("");
                        insobj.setInstallment(r_install_period);//할부
                        
                        if(res_cd.equals("0000"))
                        {    
                                appFormBean.setParam_app_cmp_cd("KICCPG");
                                appFormBean.setParam_card_cd(r_issuer_cd);
                                
                                List ls_iss_cd = null;
                                ls_iss_cd = appDAO.GetCardCode(appFormBean);
                                
                                if(ls_iss_cd == null || ls_iss_cd.size() <= 0)
                                {
                                    insobj.setIss_cd("091");
                                    insobj.setIss_nm("기타카드");                    
                                }
                                else
                                {
                                    Tb_Main_Code  isscodeobj = (Tb_Main_Code)ls_iss_cd.get(0);

                                    insobj.setIss_cd(isscodeobj.getDetail_code());
                                    insobj.setIss_nm(isscodeobj.getDetailcode_nm());            
                                }           

                                appFormBean.setParam_app_cmp_cd("KICCPG");
                                appFormBean.setParam_card_cd(r_acquirer_cd);
                                
                                List ls_acq_cd = null;
                                
                                ls_acq_cd = appDAO.GetCardCode(appFormBean);
                                
                                if(ls_acq_cd == null || ls_acq_cd.size() <= 0)
                                {
                                    insobj.setAcq_cd("091");
                                    insobj.setAcq_nm("기타카드"); 
                                }
                                else
                                 {
                                    Tb_Main_Code  acqcodeobj = (Tb_Main_Code)ls_acq_cd.get(0);

                                    insobj.setAcq_cd(acqcodeobj.getDetail_code());
                                    insobj.setAcq_nm(acqcodeobj.getDetailcode_nm());    
                                 }
                                
                                //수수료 계산
                                String strTpCms = onfftidcmsinfo.getCommission();
                                
                                /*
                                double dbl_TpCms = Double.parseDouble(strTpCms)/Double.parseDouble("100");
                                double dbl_TpApp_mat = Double.parseDouble(r_amount);
                                double dbl_onfftid_cms = Math.floor(dbl_TpCms * dbl_TpApp_mat);
                                double dbl_onfftid_payamt = dbl_TpApp_mat - dbl_onfftid_cms;
                                
                                insobj.setOnofftid_pay_amt(String.valueOf((int)dbl_onfftid_payamt));
                                insobj.setOnofftid_commision(String.valueOf((int)dbl_onfftid_cms));                                                                
                                */
                                
                                BigDecimal bigdec_TpCms = new BigDecimal(strTpCms).divide(new BigDecimal("100"));
                                
                                BigDecimal bigdec_TpApp_mat = new BigDecimal(r_amount);
           
                                BigDecimal bigdec_onfftid_cmd = bigdec_TpApp_mat.multiply(bigdec_TpCms);
                                
                                double dbl_onfftid_cms = Math.floor( bigdec_onfftid_cmd.doubleValue() );
                                
                                BigDecimal bigdec_onfftid_payamt = bigdec_TpApp_mat.subtract(new BigDecimal(String.valueOf(dbl_onfftid_cms))); 
                                
                                insobj.setOnofftid_pay_amt( String.valueOf((long)bigdec_onfftid_payamt.doubleValue()) );
                                insobj.setOnofftid_commision(String.valueOf((long)dbl_onfftid_cms));  
                                
                        }
                        
                        insobj.setApp_iss_cd(r_issuer_cd);
                        insobj.setApp_iss_nm(r_issuer_nm);
                        
                        insobj.setApp_acq_cd(r_acquirer_cd);
                        insobj.setApp_acq_nm(r_acquirer_nm);
                        
                        //pg의 경우 결제MID가 가맹점 번호로 성공시 나오는 카드사 가맹점 번호는 원 가맹점 번호로 입력
                        insobj.setMerch_no(g_mall_id);
                        insobj.setOrg_merch_no(r_join_no);
                        
                        insobj.setResult_cd(res_cd);
                        insobj.setResult_msg(res_msg);
                        /*
                        insobj.setOrg_app_dd("");
                        insobj.setOrg_app_no("");
                        insobj.setOrg_pg_seq("");
                        insobj.setCncl_reason("");
                        */
                        insobj.setTran_status("00");//승인 :00 ,10:당일취소 , 20:매입취소
                        insobj.setTran_step("00");
                        //insobj.setCncl_dt("");
                        //insobj.setAcq_dt("");
                        //insobj.setAcq_result_cd("");
                        //insobj.setHoldoff_dt("");
                        //insobj.setHoldon_dt("");
                        //insobj.setPay_dt("");
                        //insobj.setPay_amt("");
                        //insobj.setCommision("");
                        

                        
                        insobj.setClient_ip(reqpgobj.getClient_ip());
                        insobj.setUser_type(reqpgobj.getUser_type());
                        insobj.setMemb_user_no(reqpgobj.getMemb_user_no());
                        insobj.setUser_id(reqpgobj.getUser_id());
                        insobj.setUser_nm(reqpgobj.getUser_nm());
                        insobj.setUser_mail(reqpgobj.getUser_mail());
                        insobj.setUser_phone1(reqpgobj.getUser_phone1());
                        insobj.setUser_phone2(reqpgobj.getUser_phone2());
                        insobj.setUser_addr(reqpgobj.getUser_addr());
                        insobj.setProduct_type(reqpgobj.getProduct_type());
                        insobj.setProduct_nm(reqpgobj.getProduct_nm());
                        /*
                        insobj.setFiller("");
                        insobj.setFiller2("");
                        insobj.setTerm_filler1("");
                        insobj.setTerm_filler2("");
                        insobj.setTerm_filler3("");
                        insobj.setTerm_filler4("");
                        insobj.setTerm_filler5("");
                        insobj.setTerm_filler6("");
                        insobj.setTerm_filler7("");
                        insobj.setTerm_filler8("");
                        insobj.setTerm_filler9("");
                        insobj.setTerm_filler10("");
                        */
                        insobj.setTrad_chk_flag("0");
 
                        insobj.setIns_user(appFormBean.getUser_seq());
                        insobj.setMod_user(appFormBean.getUser_seq());
                        
                        //insobj.setAuth_pw(reqpgobj.getPassword());
                        //insobj.setAuth_value(reqpgobj.getAuth_value());

        //가맹점 DB 처리
        Integer int_dbresult = -1;
        
        try
        {
            int_dbresult = appDAO.InsertCardPgTran(insobj);
        }
        catch(Exception ex_ins)
        {
            logger.error(ex_ins);
            int_dbresult = -1;
        }
        
        String strTpResult = int_dbresult.toString();
        
        logger.trace("------------------Card App insert start------------------");
        logger.trace("card App insert result : " + strTpResult);
        logger.trace("------------------Card App insert End ------------------");
        
        //strTpResult ="0";
        
        /* -------------------------------------------------------------------------- */
        /* ::: 가맹점 DB 처리                                                         */
        /* -------------------------------------------------------------------------- */
        /* r_amount가 주문DB의 금액과 다를 시 반드시 취소 요청을 하시기 바랍니다.     */
        /* DB 처리 실패 시 취소 처리를 해주시기 바랍니다.                             */
        /* -------------------------------------------------------------------------- */        
        //DB처리 실패시 자동 망취소
        if ( !strTpResult.equals("1") && res_cd.equals("0000") )
        {
                int easypay_mgr_data_item;

                easyPayClient.easypay_reqdata_init();

                tr_cd = TRAN_CD_NOR_MGR;
                easypay_mgr_data_item = easyPayClient.easypay_item( "mgr_data" );
                if ( !r_escrow_yn.equals( "Y" ) )
                {
                    easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_txtype", "40" 	);
                }
                else
                {
                    easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_txtype",  "61" 	);
                    easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_subtype", "ES02" );

                }
                easyPayClient.easypay_deli_us( easypay_mgr_data_item, "org_cno",  r_cno 		);
                easyPayClient.easypay_deli_us( easypay_mgr_data_item, "order_no", reqpgobj.getOrder_no() 		);
                easyPayClient.easypay_deli_us( easypay_mgr_data_item, "req_ip",   reqpgobj.getClient_ip()  	);
                easyPayClient.easypay_deli_us( easypay_mgr_data_item, "req_id",   "MALL_R_TRANS" );
                easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_msg",  "DB 처리 실패로 망취소"  );

                easyPayClient.easypay_run( g_mall_id, tr_cd, reqpgobj.getOrder_no() );

                res_cd = Util.nullToSpaceString(easyPayClient.res_cd);
                res_msg = Util.nullToSpaceString(easyPayClient.res_msg);
                r_cno       = Util.nullToSpaceString(easyPayClient.easypay_get_res( "cno"             ));    // PG거래번호
                r_canc_date = Util.nullToSpaceString(easyPayClient.easypay_get_res( "canc_date"       ));    //취소일시
                
                logger.trace("------------------reverse tran result start------------------" );
                logger.trace("revers res_cd : " + res_cd);
                logger.trace("revers res_msg  : " + res_msg);
                logger.trace("revers r_cno : " + r_cno);
                logger.trace("revers r_canc_date : " + r_canc_date);
                logger.trace("------------------reverse tran result End ------------------" );
                
                Date today_cncl=new Date();
                String strCurYear_cncl = formater.format(today_cncl);//'YYYYMMDDhhmmss'
                
                //r거래값을 입력한다.
                InsertCardPgTranBean cnclobj = new InsertCardPgTranBean();
                //가맹점 DB 처리에러시 
                String strTpCnclTranseq = appDAO.GetTranSeq();
                cnclobj.setTran_seq(strTpCnclTranseq);// 거래번호 셋팅
                
                if (res_cd.equals("0000"))
                {
                    //cnclobj.setInstablenm("TB_TRAN_CARDPG_"+r_canc_date.substring(2, 6));
                    cnclobj.setInstablenm("TB_TRAN_CARDPG_RTRAN");
                    cnclobj.setApp_dt(r_canc_date.substring(0, 8));
                    cnclobj.setApp_tm(r_canc_date.substring(8, 14));
                    cnclobj.setResult_status("00");
                    cnclobj.setAcc_chk_flag("3");//r거래는 
                    cnclobj.setTran_step("00");
                    cnclobj.setCncl_dt(r_canc_date.substring(0, 8));
                }
                else
                {
                    //cnclobj.setInstablenm("TB_TRAN_CARDPG_"+strCurYear_cncl.substring(2, 6));
                    cnclobj.setInstablenm("TB_TRAN_CARDPG_RTRAN");
                    cnclobj.setResult_status("99");                        
                    cnclobj.setAcc_chk_flag("3");//r거래는3
                }
                
                cnclobj.setMassagetype("50");//메세지타입
                cnclobj.setCard_num(insobj.getCard_num());//카드번호
                cnclobj.setApp_card_num(insobj.getApp_card_num());//카드번호
                cnclobj.setApp_no(null);//승인번호
                cnclobj.setTot_amt(insobj.getTot_amt());//결제총금액
                cnclobj.setPg_seq(r_cno);//pg거래고유번호
                cnclobj.setPay_chn_cate(insobj.getPay_chn_cate());
                cnclobj.setTid(g_mall_id);
                cnclobj.setOnffmerch_no(insobj.getOnffmerch_no());        
                cnclobj.setOnfftid(insobj.getOnfftid());
                cnclobj.setWcc(insobj.getWcc());
                cnclobj.setExpire_dt(insobj.getExpire_dt());
                cnclobj.setCard_cate(insobj.getCard_cate());
                cnclobj.setOrder_seq(insobj.getOrder_seq());
                cnclobj.setTran_dt(insobj.getTran_dt());
                cnclobj.setTran_tm(insobj.getTran_tm());
                cnclobj.setTran_cate("40");//취소
                cnclobj.setFree_inst_flag(insobj.getFree_inst_flag());
                cnclobj.setTax_amt(insobj.getTax_amt());//부가세금액
                cnclobj.setSvc_amt(insobj.getSvc_amt());//비과세승인금액
                cnclobj.setInstallment(insobj.getInstallment());//할부
                
                cnclobj.setClient_ip(insobj.getClient_ip());
                cnclobj.setUser_nm(insobj.getUser_nm());
                cnclobj.setUser_phone1(insobj.getUser_phone1());
                cnclobj.setUser_phone2(insobj.getUser_phone2());
                cnclobj.setProduct_nm(insobj.getProduct_nm());                
                        
                cnclobj.setMerch_no(insobj.getMerch_no());
                cnclobj.setResult_cd(res_cd);
                cnclobj.setResult_msg(res_msg);

                cnclobj.setOrg_app_dd(insobj.getApp_dt());
                cnclobj.setOrg_app_no(insobj.getApp_no());
                cnclobj.setOrg_pg_seq(insobj.getPg_seq());
                cnclobj.setCncl_reason( "DB 처리 실패로 망취소");
                
                cnclobj.setTran_status("10");//승인 :00 ,10:당일취소 , 20:매입취소
                        
                cnclobj.setOnofftid_pay_amt(insobj.getOnofftid_pay_amt());
                cnclobj.setOnofftid_commision(insobj.getOnofftid_commision());
                cnclobj.setClient_ip(insobj.getClient_ip());
                
                cnclobj.setTrad_chk_flag("0");//거래대사
                
                cnclobj.setIns_user(appFormBean.getUser_seq());
                cnclobj.setMod_user(appFormBean.getUser_seq());
                
                Integer int_reversdbresult = appDAO.InsertCardPgTran(cnclobj);
                
                logger.trace("------------------reverse tran Dbinsert start------------------" );
                logger.trace("int_reversdbresult : " + int_reversdbresult);
                logger.trace("------------------reverse tran Dbinsert End ------------------" );                
                /*
                if (res_cd.equals("0000"))
                {
                    //r거래에대한 원거래를 Update 한다.
                    InsertCardPgTranBean orgcnclobj = new InsertCardPgTranBean();
                    orgcnclobj.setTran_seq(insobj.getTran_seq());
                    orgcnclobj.setInstablenm(insobj.getInstablenm());
                    orgcnclobj.setCncl_reason("DB 처리 실패로 망취소");
                    orgcnclobj.setAcc_chk_flag("3");
                    orgcnclobj.setTran_status("90");
                    orgcnclobj.setCncl_dt(cnclobj.getApp_dt());
                    orgcnclobj.setMod_user(appFormBean.getUser_seq());
                    
                    Integer int_reversdbupdate = appDAO.OrgCardPgTranUpdate(orgcnclobj);
                    
                    logger.trace("------------------reverse orgtran update start------------------" );
                    logger.trace("int_reversdbupdate : " + int_reversdbupdate);
                    logger.trace("------------------reverse orgtran update End ------------------" );                

                }
                */
            ht.put("RESULT_CD", "OF98");
            ht.put("RESULT_MSG", "시스템문제로 거래가 자동취소되었습니다."); 
            ht.put("TRAN_SEQ", strTpTranseq); 

        }
        else
        {
            ht.put("RESULT_CD", res_cd);
            ht.put("RESULT_MSG", res_msg); 
            ht.put("TRAN_SEQ", strTpTranseq); 
        }
        
        return ht;
    }
    
    
    @Override
    @Transactional
    public Hashtable appKcpReqAction(AppFormBean appFormBean, AppOnffTidInfoVo appOnffTidInfoVo, AppTerminalInfoVo appTerminalInfoVo ) throws Exception {
        Hashtable ht = new Hashtable();
        
        KiccPgVo reqRefobj =  appFormBean.getReqKiccPgVo();
        KcpPgVo reqpgobj = appFormBean.getReqKcpPgVo();
        KcpPgVo respgobj = new KcpPgVo();
        
        Date today=new Date();
        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmss");       
        String strCurYear = formater.format(today);

        int int_g_log_level  = Integer.parseInt(reqpgobj.getG_conf_log_level());
        
	String g_conf_site_cd   = appTerminalInfoVo.getTerminal_no();             // 리얼 반영시 KICC에 발급된 mall_id 사용
        String g_conf_site_key = appTerminalInfoVo.getTerminal_pwd();             // 리얼 반영시 KICC에 발급된 mall_id 사용
        
        
        if(g_conf_site_cd == null || g_conf_site_cd.equals(""))
        {
            throw new Exception("결제용TID가 없습니다.");
        }
        
                //가맹점주문번호를 생성한다.
        String strOrder_seq = appDAO.GetOrderSeq();
        
        reqpgobj.setOrdr_idxx(strOrder_seq);

        logger.trace("------------------------------appKicpReqAction--------------------------------------");
        logger.trace("onffmerchno : " + appOnffTidInfoVo.getOnffmerch_no());
        logger.trace("onfftid : " + appOnffTidInfoVo.getOnfftid());
        logger.trace("Pay_mtd_seq : " + appOnffTidInfoVo.getPay_mtd_seq());        
        logger.trace("g_log_dir : " + reqpgobj.getG_conf_log_dir());
        logger.trace("g_gw_url : " + reqpgobj.getG_conf_gw_url());
        logger.trace("g_gw_port : " + reqpgobj.getG_conf_gw_port());
        logger.trace("g_mall_id : " + g_conf_site_cd);
        logger.trace("g_mall_pwd : " + g_conf_site_key);
        logger.trace("reqpgobj.getUser_nm() : " + reqpgobj.getBuyr_name());
        logger.trace("reqpgobj.getProduct_nm() : " + reqpgobj.getGood_name());
        logger.trace("reqpgobj.getCard_no() : " + reqpgobj.getCard_no());
        logger.trace("reqpgobj.getCom_free_amt() : " + reqpgobj.getComm_free_mny());
        logger.trace("reqpgobj.getCom_tax_amt() : " + reqpgobj.getComm_tax_mny());
        logger.trace("reqpgobj.getCom_vat_amt() : " + reqpgobj.getComm_vat_mny());
        logger.trace("reqpgobj.getProduct_amt() : " + reqpgobj.getGood_name());
        logger.trace("reqpgobj.getCard_amt() : " + reqpgobj.getGood_mny());
        logger.trace("reqpgobj.getCard_amt() : " + reqpgobj.getCard_expiry());
        //logger.trace("reqpgobj.getTax_flg() : " + reqpgobj.getTax_flg());
        logger.trace("--------------------------------------------------------------------");        
        
        //수수료정보를 가져온다.
        appFormBean.setPay_mtd_seq(appOnffTidInfoVo.getPay_mtd_seq());
        List ls_onfftidcmsinfo = appDAO.GetOnffTidCommission(appFormBean);
        AppOnffTidInfoVo onfftidcmsinfo = (AppOnffTidInfoVo)ls_onfftidcmsinfo.get(0);
        
	logger.trace("------------------------------appKiccReqAction--------------------------------------");   
        logger.trace("getOnfftid_cms_seq : " + onfftidcmsinfo.getOnfftid_cms_seq());        
        logger.trace("getOnfftid : " + onfftidcmsinfo.getOnfftid());      
        logger.trace("getCommission : " + onfftidcmsinfo.getCommission());        
        logger.trace("getStart_dt : " + onfftidcmsinfo.getStart_dt());        
        logger.trace("getEnd_dt : " + onfftidcmsinfo.getEnd_dt());        
        logger.trace("--------------------------------------------------------------------");  
               
        
        /* -------------------------------------------------------------------------- */
        /* ::: 결제 결과                                                              */
        /* -------------------------------------------------------------------------- */
            String bDBProc          = "";
            String res_cd           = "";
            String res_msg          = "";
            String r_order_no       = "";
            String r_complex_yn     = "";
            String r_msg_type       = "";     //거래구분
            String r_noti_type	    = "";     //노티구분
            String r_cno            = "";     //PG거래번호
            String r_amount         = "";     //총 결제금액
            String r_auth_no        = "";     //승인번호
            String r_tran_date      = "";     //거래일시
            String r_pnt_auth_no    = "";     //포인트 승인 번호
            String r_pnt_tran_date  = "";     //포인트 승인 일시
            String r_cpon_auth_no   = "";     //쿠폰 승인 번호
            String r_cpon_tran_date = "";     //쿠폰 승인 일시
            String r_card_no        = "";     //카드번호
            String r_issuer_cd      = "";     //발급사코드
            String r_issuer_nm      = "";     //발급사명
            String r_acquirer_cd    = "";     //매입사코드
            String r_acquirer_nm    = "";     //매입사명
            String r_install_period = "";     //할부개월
            String r_noint          = "";     //무이자여부
            String r_bank_cd        = "";     //은행코드
            String r_bank_nm        = "";     //은행명
            String r_cash_res_cd    = "";     //현금영수증 결과코드
            String r_cash_res_msg   = "";     //현금영수증 결과메세지
            String r_cash_auth_no   = "";     //현금영수증 승인번호
            String r_cash_tran_date = "";     //현금영수증 승인일시
            String r_join_no = ""; //카드사가맹점번호
            
            //인스터스생성 초기화
            C_PP_CLI c_PayPlus = new C_PP_CLI();

            //c_PayPlus.mf_init( g_conf_home_dir, g_conf_gw_url, g_conf_gw_port, g_conf_key_dir, g_conf_log_dir, g_conf_tx_mode );
            int inttptxmod = Integer.parseInt(reqpgobj.getG_conf_tx_mode());
            //window
            //c_PayPlus.mf_init( reqpgobj.getG_conf_home_dir(), reqpgobj.getG_conf_gw_url(), reqpgobj.getG_conf_gw_port(), reqpgobj.getG_conf_key_dir(), reqpgobj.getG_conf_log_dir(), inttptxmod );
            //linux
            c_PayPlus.mf_init( reqpgobj.getG_conf_home_dir(), reqpgobj.getG_conf_gw_url(), reqpgobj.getG_conf_gw_port(), inttptxmod );
            
            c_PayPlus.mf_init_set();
        
        /* -------------------------------------------------------------------------- */
        /* ::: 승인요청 전문 설정                                                     */
        /* -------------------------------------------------------------------------- */
        if("pay".equals(reqpgobj.getReq_tx())) 
        {

            // 승인요청 전문 설정
            // 결제 주문 정보 DATA
            int payx_data_set;
            int common_data_set;

            payx_data_set   = c_PayPlus.mf_add_set( "payx_data" );
            common_data_set = c_PayPlus.mf_add_set( "common"    );


            c_PayPlus.mf_set_us( common_data_set, "amount",   reqpgobj.getGood_mny()    );
            c_PayPlus.mf_set_us( common_data_set, "tax_flag",   "TG03"    );
            c_PayPlus.mf_set_us( common_data_set, "comm_tax_mny",   reqpgobj.getComm_tax_mny()    );
            c_PayPlus.mf_set_us( common_data_set, "comm_vat_mny",   reqpgobj.getComm_vat_mny()    );
            c_PayPlus.mf_set_us( common_data_set, "comm_free_mny",   reqpgobj.getComm_free_mny()    );
            
            c_PayPlus.mf_set_us( common_data_set, "currency", reqpgobj.getCurrency()    );

            c_PayPlus.mf_set_us( common_data_set, "cust_ip",  reqpgobj.getCust_ip()     );
            c_PayPlus.mf_set_us( common_data_set, "escw_mod", "N"         );

            c_PayPlus.mf_add_rs( payx_data_set, common_data_set );

            // 주문 정보
            int ordr_data_set;

            ordr_data_set = c_PayPlus.mf_add_set( "ordr_data" );

            c_PayPlus.mf_set_us( ordr_data_set, "ordr_idxx", reqpgobj.getOrdr_idxx() );
            
            //String strtpgoodnm = Util.fromUtftoEuc(reqpgobj.getGood_name());
            String strtpgoodnm = reqpgobj.getGood_name();
            
            c_PayPlus.mf_set_us( ordr_data_set, "good_name", strtpgoodnm );
            c_PayPlus.mf_set_us( ordr_data_set, "good_mny",  reqpgobj.getGood_mny()   );
            
            //String strtpbuyr_name =  Util.fromUtftoEuc(reqpgobj.getBuyr_name());
            String strtpbuyr_name =  reqpgobj.getBuyr_name();
                    
            c_PayPlus.mf_set_us( ordr_data_set, "buyr_name", strtpbuyr_name );
            c_PayPlus.mf_set_us( ordr_data_set, "buyr_tel1", reqpgobj.getBuyr_tel1() );
            c_PayPlus.mf_set_us( ordr_data_set, "buyr_tel2", reqpgobj.getBuyr_tel2() );
            c_PayPlus.mf_set_us( ordr_data_set, "buyr_mail", reqpgobj.getBuyr_mail() );

            if ("CARD".equals(reqpgobj.getPay_method()) )
            {
                int card_data_set;

                card_data_set = c_PayPlus.mf_add_set( "card" );

                c_PayPlus.mf_set_us( card_data_set, "card_mny", reqpgobj.getGood_mny() );        // 결제 금액
                c_PayPlus.mf_set_us( card_data_set, "card_tx_type", reqpgobj.getCard_tx_type());
                c_PayPlus.mf_set_us( card_data_set, "quota",        reqpgobj.getQuota() );
                c_PayPlus.mf_set_us( card_data_set, "card_no",      reqpgobj.getCard_no() );
                c_PayPlus.mf_set_us( card_data_set, "card_expiry",  reqpgobj.getCard_expiry() );
                c_PayPlus.mf_set_us( card_data_set, "card_taxno", reqpgobj.getCardauth() );
                c_PayPlus.mf_set_us( card_data_set, "card_pwd",   reqpgobj.getCardpwd()  );

                c_PayPlus.mf_add_rs( payx_data_set, card_data_set );
            }        

        }

        /* -------------------------------------------------------------------------- */
        /* ::: 실행                                                                   */
        /* -------------------------------------------------------------------------- */
        //if ( Util.nullToString(reqpgobj.getCard_tx_type()).length() > 0 )
        if ( Util.nullToString(reqpgobj.getTx_cd()).length() > 0 )
        {
            c_PayPlus.mf_do_tx( g_conf_site_cd, g_conf_site_key, reqpgobj.getTx_cd(), reqpgobj.getCust_ip(), reqpgobj.getOrdr_idxx(), Util.getNullToZero(reqpgobj.getG_conf_log_level()), "1" );
        }
        else
        {
            c_PayPlus.m_res_cd  = "9562";
            c_PayPlus.m_res_msg = "연동 오류";
        }
        res_cd  = c_PayPlus.m_res_cd;                      // 결과 코드
        //res_msg = Util.fromEuctoUtf(c_PayPlus.m_res_msg);                     // 결과 메시지1004
        res_msg = c_PayPlus.m_res_msg;                     // 결과 메시지
        
	logger.trace("--------------------------------------------------------------------");
        logger.trace("res_cd : " + res_cd);
        logger.trace("res_msg : " + res_msg);
        logger.trace("--------------------------------------------------------------------");     
        
        /* -------------------------------------------------------------------------- */
        /* ::: 결과 처리                                                              */
        /* -------------------------------------------------------------------------- */
        r_cno             = Util.nullToSpaceString( c_PayPlus.mf_get_res( "tno" ));    // PG거래번호
        r_amount          = Util.nullToSpaceString( c_PayPlus.mf_get_res( "amount"    ));    //총 결제금액
        r_order_no        = Util.nullToSpaceString( reqpgobj.getOrdr_idxx());    //주문번호
        r_auth_no         = Util.nullToSpaceString(c_PayPlus.mf_get_res( "app_no"    ));    //승인번호
        r_tran_date       = Util.nullToSpaceString(c_PayPlus.mf_get_res( "app_time"  ));    //승인일시
        r_card_no         = Util.nullToSpaceString(c_PayPlus.mf_get_res( "card_no"  ));    //카드번호
        r_issuer_cd       = Util.nullToSpaceString(c_PayPlus.mf_get_res( "card_cd"   ));    //발급사코드
        //r_issuer_nm       = Util.fromEuctoUtf(c_PayPlus.mf_get_res( "card_name"   ));    //발급사명
        r_issuer_nm       = c_PayPlus.mf_get_res( "card_name"   );    //발급사명
        r_acquirer_cd     = Util.nullToSpaceString(c_PayPlus.mf_get_res( "acqu_cd"   ));    //매입사코드
        //r_acquirer_nm     = Util.fromUtftoEuc(c_PayPlus.mf_get_res( "acqu_name"   ));    //매입사명
        r_acquirer_nm     = Util.nullToSpaceString(c_PayPlus.mf_get_res( "acqu_name"   ));    //매입사명
        r_install_period  = Util.nullToSpaceString(c_PayPlus.mf_get_res( "quota"     ));    //할부개월
        r_noint           = Util.nullToSpaceString(c_PayPlus.mf_get_res( "noinf"     ));    //무이자여부
        
        //무이자일경우
        if(r_noint.equals("Y"))
        {
            r_noint = "02";
        }
        //일반일경우
        else
        {
            r_noint = "00";
        }
        /*
        r_cash_res_cd     = Util.nullToSpaceString(easyPayClient.easypay_get_res( "cash_res_cd"     ));    //현금영수증 결과코드
        r_cash_res_msg    = Util.nullToSpaceString(easyPayClient.easypay_get_res( "cash_res_msg"    ));    //현금영수증 결과메세지
        r_cash_auth_no    = Util.nullToSpaceString(easyPayClient.easypay_get_res( "cash_auth_no"    ));    //현금영수증 승인번호
        r_cash_tran_date  = Util.nullToSpaceString(easyPayClient.easypay_get_res( "cash_tran_date"  ));    //현금영수증 승인일시
        */
        r_join_no    = Util.nullToSpaceString(g_conf_site_cd);    //카드사가맹점번호
        
        logger.trace("--------------------------------------------------------------------");        
        logger.trace("r_cno            "+       r_cno             );// PG거래번호
        logger.trace("r_amount         "+       r_amount          );//총 결제금액
        logger.trace("r_order_no       "+       r_order_no        );//주문번호
        logger.trace("r_noti_type      "+       r_noti_type       );//노티구분
        logger.trace("r_auth_no        "+       r_auth_no         );//승인번호
        logger.trace("r_tran_date      "+       r_tran_date       );//승인일시
        /*
        logger.trace("r_pnt_auth_no    "+       r_pnt_auth_no     );//포인트승인번호
        logger.trace("r_pnt_tran_date  "+       r_pnt_tran_date   );//포인트승인일시
        logger.trace("r_cpon_auth_no   "+       r_cpon_auth_no    );//쿠폰승인번호
        logger.trace("r_cpon_tran_date "+       r_cpon_tran_date  );//쿠폰승인일시
        */
        logger.trace("r_card_no        "+       r_card_no         );//카드번호
        logger.trace("r_issuer_cd      "+       r_issuer_cd       );//발급사코드
        logger.trace("r_issuer_nm      "+       r_issuer_nm       );//발급사명
        logger.trace("r_acquirer_cd    "+       r_acquirer_cd     );//매입사코드
        logger.trace("r_acquirer_nm    "+       r_acquirer_nm     );//매입사명
        logger.trace("r_install_period "+       r_install_period  );//할부개월
        logger.trace("r_noint          "+       r_noint           );//무이자여부
/*
        logger.trace("r_bank_cd        "+       r_bank_cd         );//은행코드
        logger.trace("r_bank_nm        "+       r_bank_nm         );//은행명
        logger.trace("r_cash_res_cd    "+       r_cash_res_cd     );//현금영수증 결과코드
        logger.trace("r_cash_res_msg   "+       r_cash_res_msg    );//현금영수증 결과메세지
        logger.trace("r_cash_auth_no   "+       r_cash_auth_no    );//현금영수증 승인번호
        logger.trace("r_cash_tran_date "+       r_cash_tran_date  );//현금영수증 승인일시
*/
        /*
        logger.trace("r_auth_id        "+       r_auth_id         );//PhoneID
        logger.trace("r_billid         "+       r_billid          );//인증번호
        logger.trace("r_mobile_no      "+       r_mobile_no       );//휴대폰번호
        logger.trace("r_ars_no         "+       r_ars_no          );//전화번호
        logger.trace("r_cp_cd          "+       r_cp_cd           );//포인트사/쿠폰사
        logger.trace("r_used_pnt       "+       r_used_pnt        );//사용포인트
        logger.trace("r_remain_pnt     "+       r_remain_pnt      );//잔여한도
        logger.trace("r_pay_pnt        "+       r_pay_pnt         );//할인/발생포인트
        logger.trace("r_accrue_pnt     "+       r_accrue_pnt      );//누적포인트
        logger.trace("r_remain_cpon    "+       r_remain_cpon     );//쿠폰잔액
        logger.trace("r_used_cpon      "+       r_used_cpon       );//쿠폰 사용금액
        logger.trace("r_mall_nm        "+       r_mall_nm         );//제휴사명칭
        logger.trace("r_escrow_yn      "+       r_escrow_yn       );//에스크로 사용유무
        logger.trace("r_complex_yn     "+       r_complex_yn      );//복합결제 유무
        logger.trace("r_canc_acq_data  "+       r_canc_acq_data   );//매입취소일시
        logger.trace("r_canc_date      "+       r_canc_date       );//취소일시
        logger.trace("r_refund_date    "+       r_refund_date     );//환불예정일시
        */
        logger.trace("r_join_no        "+       r_join_no     );//카드사가맹점번호
        logger.trace("--------------------------------------------------------------------");
    
        respgobj.setTno(r_cno);
        respgobj.setAmount(r_amount);
        respgobj.setOrdr_idxx(r_order_no       );
        respgobj.setApp_no(r_auth_no        );
        respgobj.setApp_time(r_tran_date      );
        respgobj.setCard_no(r_card_no        );
        respgobj.setCard_cd(r_issuer_cd      );
        respgobj.setCard_name(r_issuer_nm      );
        respgobj.setAcqu_cd(r_acquirer_cd    );
        respgobj.setAcqu_name(r_acquirer_nm    );
        respgobj.setQuota(r_install_period );
        //respgobj.setR_escrow_yn       (r_escrow_yn      );
        //respgobj.setR_complex_yn      (r_complex_yn     );
        //respgobj.setR_canc_acq_data   (r_canc_acq_data  );
        //respgobj.setR_canc_date       (r_canc_date      );
        //respgobj.setR_refund_date     (r_refund_date    );
        //respgobj.setR_join_no     (r_join_no    );
        
        //가맹점 DB처리
        //저장정보를 만든다.
    
        InsertCardPgTranBean insobj = new InsertCardPgTranBean();
        //승인 결가에 따라 저장을 위한 테이블명을 만든다.
        //성공관련 data 셋팅
        if (res_cd.equals("0000"))
        {
            insobj.setInstablenm("TB_TRAN_CARDPG_"+r_tran_date.substring(2, 6));
            insobj.setApp_dt(r_tran_date.substring(0, 8));
            insobj.setApp_tm(r_tran_date.substring(8, 14));
            insobj.setResult_status("00");
            insobj.setAcc_chk_flag("0");
            insobj.setTot_amt(r_amount);//결제총금액
                        
        }
        else
        {
            insobj.setInstablenm("TB_TRAN_CARDPG_"+strCurYear.substring(2, 6));
            insobj.setResult_status("99");                        
            insobj.setAcc_chk_flag("3");
            insobj.setTot_amt(reqpgobj.getGood_mny());//결제총금액
        }
        
        String strTpTranseq = appDAO.GetTranSeq();
        insobj.setTran_seq(strTpTranseq);// 거래번호 셋팅
        //카드결제 시
             
                        insobj.setMassagetype("10");//메세지타입
                        //insobj.setCard_num(r_card_no);//카드번호
                        insobj.setCard_num(reqpgobj.getCard_no());//카드번호
                        insobj.setApp_card_num(r_card_no);//카드번호
                        insobj.setApp_no(r_auth_no);//승인번호

                        insobj.setPg_seq(r_cno);//pg거래고유번호
                        insobj.setPay_chn_cate(appOnffTidInfoVo.getPay_chn_cate());
                        insobj.setTid(g_conf_site_cd);
                        insobj.setOnffmerch_no(appOnffTidInfoVo.getOnffmerch_no());        
                        insobj.setOnfftid(appOnffTidInfoVo.getOnfftid());
                        insobj.setWcc(reqRefobj.getWcc());
                        insobj.setExpire_dt(reqpgobj.getCard_expiry());
                        insobj.setCard_cate(reqRefobj.getCard_user_type());
                        insobj.setOrder_seq(reqpgobj.getOrdr_idxx());
                        insobj.setTran_dt(strCurYear.substring(0, 8));
                        insobj.setTran_tm(strCurYear.substring(8, 14));
                        insobj.setTran_cate(reqRefobj.getCard_txtype());
                        insobj.setFree_inst_flag(r_noint);
                        insobj.setTax_amt(reqpgobj.getComm_vat_mny());//부가세금액
                        insobj.setSvc_amt(reqpgobj.getComm_free_mny());//비과세승인금액
                        //insobj.setCurrency("");
                        insobj.setInstallment(r_install_period);//할부
                        
                        if(res_cd.equals("0000"))
                        {    
                                appFormBean.setParam_app_cmp_cd("KCPPG");
                                appFormBean.setParam_card_cd(r_issuer_cd);
                                
                                List ls_iss_cd = null;
                                ls_iss_cd = appDAO.GetCardCode(appFormBean);
                                
                                if(ls_iss_cd == null || ls_iss_cd.size() <= 0)
                                {
                                    insobj.setIss_cd("091");
                                    insobj.setIss_nm("기타카드");                    
                                }
                                else
                                {
                                    Tb_Main_Code  isscodeobj = (Tb_Main_Code)ls_iss_cd.get(0);

                                    insobj.setIss_cd(isscodeobj.getDetail_code());
                                    insobj.setIss_nm(isscodeobj.getDetailcode_nm());            
                                }           

                                appFormBean.setParam_app_cmp_cd("KCPPG");
                                appFormBean.setParam_card_cd(r_acquirer_cd);
                                
                                List ls_acq_cd = null;
                                
                                ls_acq_cd = appDAO.GetCardCode(appFormBean);
                                
                                if(ls_acq_cd == null || ls_acq_cd.size() <= 0)
                                {
                                    insobj.setAcq_cd("091");
                                    insobj.setAcq_nm("기타카드"); 
                                }
                                else
                                 {
                                    Tb_Main_Code  acqcodeobj = (Tb_Main_Code)ls_acq_cd.get(0);

                                    insobj.setAcq_cd(acqcodeobj.getDetail_code());
                                    insobj.setAcq_nm(acqcodeobj.getDetailcode_nm());    
                                 }
                                
                                //수수료 계산
                                
                                String strTpCms = onfftidcmsinfo.getCommission();
                                /*
                                double dbl_TpCms = Double.parseDouble(strTpCms)/Double.parseDouble("100");
                                double dbl_TpApp_mat = Double.parseDouble(r_amount);
                                double dbl_onfftid_cms = Math.floor(dbl_TpCms * dbl_TpApp_mat);
                                double dbl_onfftid_payamt = dbl_TpApp_mat - dbl_onfftid_cms;

                                insobj.setOnofftid_pay_amt(String.valueOf((int)dbl_onfftid_payamt));
                                insobj.setOnofftid_commision(String.valueOf((int)dbl_onfftid_cms));                                
                                */
                                BigDecimal bigdec_TpCms = new BigDecimal(strTpCms).divide(new BigDecimal("100"));
                                
                                BigDecimal bigdec_TpApp_mat = new BigDecimal(r_amount);
           
                                BigDecimal bigdec_onfftid_cmd = bigdec_TpApp_mat.multiply(bigdec_TpCms);
                                
                                double dbl_onfftid_cms = Math.floor( bigdec_onfftid_cmd.doubleValue() );
                                
                                BigDecimal bigdec_onfftid_payamt = bigdec_TpApp_mat.subtract(new BigDecimal(String.valueOf(dbl_onfftid_cms))); 
                                
                                insobj.setOnofftid_pay_amt( String.valueOf((long)bigdec_onfftid_payamt.doubleValue()) );
                                insobj.setOnofftid_commision(String.valueOf((long)dbl_onfftid_cms));  
                        }
                        
                        insobj.setApp_iss_cd(r_issuer_cd);
                        insobj.setApp_iss_nm(r_issuer_nm);
                        
                        insobj.setApp_acq_cd(r_acquirer_cd);
                        insobj.setApp_acq_nm(r_acquirer_nm);
                        
                        //pg의 경우 결제MID가 가맹점 번호로 성공시 나오는 카드사 가맹점 번호는 원 가맹점 번호로 입력
                        insobj.setMerch_no(g_conf_site_cd);
                        insobj.setOrg_merch_no(r_join_no);
                        
                        insobj.setResult_cd(res_cd);
                        insobj.setResult_msg(res_msg);
                        /*
                        insobj.setOrg_app_dd("");
                        insobj.setOrg_app_no("");
                        insobj.setOrg_pg_seq("");
                        insobj.setCncl_reason("");
                        */
                        insobj.setTran_status("00");//승인 :00 ,10:당일취소 , 20:매입취소
                        insobj.setTran_step("00");
                        
                        insobj.setClient_ip(reqpgobj.getCust_ip());
                        insobj.setUser_type(reqRefobj.getUser_type());
                        insobj.setMemb_user_no(reqRefobj.getMemb_user_no());
                        insobj.setUser_id(reqRefobj.getUser_id());
                        insobj.setUser_nm(reqRefobj.getUser_nm());
                        insobj.setUser_mail(reqRefobj.getUser_mail());
                        insobj.setUser_phone1(reqRefobj.getUser_phone1());
                        insobj.setUser_phone2(reqRefobj.getUser_phone2());
                        insobj.setUser_addr(reqRefobj.getUser_addr());
                        insobj.setProduct_type(reqRefobj.getProduct_type());
                        insobj.setProduct_nm(reqRefobj.getProduct_nm());

                        insobj.setTrad_chk_flag("0");
 
                        insobj.setIns_user(appFormBean.getUser_seq());
                        insobj.setMod_user(appFormBean.getUser_seq());
                        
                        //insobj.setAuth_pw(reqpgobj.getPassword());
                        //insobj.setAuth_value(reqpgobj.getAuth_value());

        //20150902 거래 있을시 입력 패치   
        //가맹점 DB 처리
        /*
        Integer int_dbresult = -1;
        
        try
        {
            int_dbresult = appDAO.InsertCardPgTran(insobj);
        }
        catch(Exception ex_ins)
        {
            logger.error(ex_ins);
            int_dbresult = -1;
        }
        
        String strTpResult = int_dbresult.toString();
        */
        Integer int_dbresult = -1;
        
        try
        {
            AppFormBean appchkFormBean = new AppFormBean();
            //logger.debug("-------------appchk start------------------");
            appchkFormBean.setChkpgseq(r_cno);
            appchkFormBean.setChkpgtid(g_conf_site_cd);
            //logger.debug("r_cno : " + r_cno);
            //logger.debug("g_conf_site_cd : "+ g_conf_site_cd);
            String strTpAppChk = Util.nullToString(appDAO.getOnffAppChkCnt(appchkFormBean));
            
            if(strTpAppChk.equals("")) 
            {
                strTpAppChk="0";
            }
            
            logger.trace("strTpAppChk : " + strTpAppChk);

            Integer int_reversdbresult = -1;

            if(strTpAppChk.equals("0"))
            {
                int_dbresult = appDAO.InsertCardPgTran(insobj);
            }
            else
            {
                
                int_dbresult = 1;
                logger.info("승인거래 기존재");
            }            
            //logger.debug("-------------appchk end------------------");
        }
        catch(Exception ex_ins)
        {
            logger.error(ex_ins);
            int_dbresult = -1;
        }
        
        String strTpResult = int_dbresult.toString();
        
        logger.trace("------------------Card App insert start------------------");
        logger.trace("card App insert result : " + strTpResult);
        logger.trace("------------------Card App insert End ------------------");
        
        //strTpResult ="0";
        
        /* -------------------------------------------------------------------------- */
        /* ::: 가맹점 DB 처리                                                         */
        /* -------------------------------------------------------------------------- */
        /* r_amount가 주문DB의 금액과 다를 시 반드시 취소 요청을 하시기 바랍니다.     */
        /* DB 처리 실패 시 취소 처리를 해주시기 바랍니다.                             */
        /* -------------------------------------------------------------------------- */        
        //DB처리 실패시 자동 망취소
        if ( !strTpResult.equals("1") && res_cd.equals("0000") )
        {
            /*
                int easypay_mgr_data_item;

                easyPayClient.easypay_reqdata_init();

                tr_cd = TRAN_CD_NOR_MGR;
                easypay_mgr_data_item = easyPayClient.easypay_item( "mgr_data" );
                if ( !r_escrow_yn.equals( "Y" ) )
                {
                    easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_txtype", "40" 	);
                }
                else
                {
                    easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_txtype",  "61" 	);
                    easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_subtype", "ES02" );

                }
                easyPayClient.easypay_deli_us( easypay_mgr_data_item, "org_cno",  r_cno 		);
                easyPayClient.easypay_deli_us( easypay_mgr_data_item, "order_no", reqpgobj.getOrder_no() 		);
                easyPayClient.easypay_deli_us( easypay_mgr_data_item, "req_ip",   reqpgobj.getClient_ip()  	);
                easyPayClient.easypay_deli_us( easypay_mgr_data_item, "req_id",   "MALL_R_TRANS" );
                easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_msg",  "DB 처리 실패로 망취소"  );

                easyPayClient.easypay_run( g_mall_id, tr_cd, reqpgobj.getOrder_no() );

                res_cd = Util.nullToSpaceString(easyPayClient.res_cd);
                res_msg = Util.nullToSpaceString(easyPayClient.res_msg);
                r_cno       = Util.nullToSpaceString(easyPayClient.easypay_get_res( "cno"             ));    // PG거래번호
                r_canc_date = Util.nullToSpaceString(easyPayClient.easypay_get_res( "canc_date"       ));    //취소일시
            */ 
                int mod_data_set_no;

                c_PayPlus.mf_init_set();

                String tx_cd = "00200000";
                
                Date Rcncltoday=new Date();
                SimpleDateFormat Rcnclformater = new SimpleDateFormat("yyyyMMddHHmmss");       
                String RcnclCurDate = Rcnclformater.format(Rcncltoday);                

                mod_data_set_no = c_PayPlus.mf_add_set( "mod_data" );

                c_PayPlus.mf_set_us( mod_data_set_no, "tno",      respgobj.getTno()    );                         // KCP 원거래 거래번호
                c_PayPlus.mf_set_us( mod_data_set_no, "mod_type", "STSC" );                         // 원거래 변경 요청 종류
                c_PayPlus.mf_set_us( mod_data_set_no, "mod_ip",   reqpgobj.getCust_ip() );                        // 변경 요청자 IP
                c_PayPlus.mf_set_us( mod_data_set_no, "mod_desc", "결과 처리 오류 - 자동 취소" );   // 변경 사유
                c_PayPlus.mf_do_tx( g_conf_site_cd, g_conf_site_key, tx_cd, reqpgobj.getCust_ip(),  reqpgobj.getOrdr_idxx(), Util.getNullToZero(reqpgobj.getG_conf_log_level()), "1" );

                res_cd  = c_PayPlus.m_res_cd;
                res_msg = c_PayPlus.m_res_msg;
            
            
                logger.trace("------------------reverse tran result start------------------" );
                logger.trace("revers res_cd : " + res_cd);
                logger.trace("revers res_msg  : " + res_msg);
                logger.trace("revers r_cno : " + r_cno);
                logger.trace("revers r_canc_date : " + RcnclCurDate);
                logger.trace("------------------reverse tran result End ------------------" );
                
                Date today_cncl=new Date();
                String strCurYear_cncl = formater.format(today_cncl);//'YYYYMMDDhhmmss'
                
                //r거래값을 입력한다.
                InsertCardPgTranBean cnclobj = new InsertCardPgTranBean();
                //가맹점 DB 처리에러시 
                String strTpCnclTranseq = appDAO.GetTranSeq();
                cnclobj.setTran_seq(strTpCnclTranseq);// 거래번호 셋팅
                
                if (res_cd.equals("0000"))
                {
                    //cnclobj.setInstablenm("TB_TRAN_CARDPG_"+r_canc_date.substring(2, 6));
                    cnclobj.setInstablenm("TB_TRAN_CARDPG_RTRAN");
                    cnclobj.setApp_dt(RcnclCurDate.substring(0, 8));
                    cnclobj.setApp_tm(RcnclCurDate.substring(8, 14));
                    cnclobj.setResult_status("00");
                    cnclobj.setAcc_chk_flag("3");//r거래는 
                    cnclobj.setTran_step("00");
                    cnclobj.setCncl_dt(RcnclCurDate.substring(0, 8));
                }
                else
                {
                    //cnclobj.setInstablenm("TB_TRAN_CARDPG_"+strCurYear_cncl.substring(2, 6));
                    cnclobj.setInstablenm("TB_TRAN_CARDPG_RTRAN");
                    cnclobj.setResult_status("99");                        
                    cnclobj.setAcc_chk_flag("3");//r거래는3
                }
                
                cnclobj.setMassagetype("50");//메세지타입
                cnclobj.setCard_num(insobj.getCard_num());//카드번호
                cnclobj.setApp_card_num(insobj.getApp_card_num());//카드번호
                cnclobj.setApp_no(null);//승인번호
                cnclobj.setTot_amt(insobj.getTot_amt());//결제총금액
                cnclobj.setPg_seq(r_cno);//pg거래고유번호
                cnclobj.setPay_chn_cate(insobj.getPay_chn_cate());
                cnclobj.setTid(g_conf_site_cd);
                cnclobj.setOnffmerch_no(insobj.getOnffmerch_no());        
                cnclobj.setOnfftid(insobj.getOnfftid());
                cnclobj.setWcc(insobj.getWcc());
                cnclobj.setExpire_dt(insobj.getExpire_dt());
                cnclobj.setCard_cate(insobj.getCard_cate());
                cnclobj.setOrder_seq(insobj.getOrder_seq());
                cnclobj.setTran_dt(insobj.getTran_dt());
                cnclobj.setTran_tm(insobj.getTran_tm());
                cnclobj.setTran_cate("40");//취소
                cnclobj.setFree_inst_flag(insobj.getFree_inst_flag());
                cnclobj.setTax_amt(insobj.getTax_amt());//부가세금액
                cnclobj.setSvc_amt(insobj.getSvc_amt());//비과세승인금액
                cnclobj.setInstallment(insobj.getInstallment());//할부
                
                cnclobj.setClient_ip(insobj.getClient_ip());
                cnclobj.setUser_nm(insobj.getUser_nm());
                cnclobj.setUser_phone1(insobj.getUser_phone1());
                cnclobj.setUser_phone2(insobj.getUser_phone2());
                cnclobj.setProduct_nm(insobj.getProduct_nm());                
                        
                cnclobj.setMerch_no(insobj.getMerch_no());
                cnclobj.setResult_cd(res_cd);
                cnclobj.setResult_msg(res_msg);

                cnclobj.setOrg_app_dd(insobj.getApp_dt());
                cnclobj.setOrg_app_no(insobj.getApp_no());
                cnclobj.setOrg_pg_seq(insobj.getPg_seq());
                cnclobj.setCncl_reason( "DB 처리 실패로 망취소");
                
                cnclobj.setTran_status("10");//승인 :00 ,10:당일취소 , 20:매입취소
                        
                cnclobj.setOnofftid_pay_amt(insobj.getOnofftid_pay_amt());
                cnclobj.setOnofftid_commision(insobj.getOnofftid_commision());
                cnclobj.setClient_ip(insobj.getClient_ip());
                
                cnclobj.setTrad_chk_flag("0");//거래대사
                
                cnclobj.setIns_user(appFormBean.getUser_seq());
                cnclobj.setMod_user(appFormBean.getUser_seq());
                
                Integer int_reversdbresult = appDAO.InsertCardPgTran(cnclobj);
                
                logger.trace("------------------reverse tran Dbinsert start------------------" );
                logger.trace("int_reversdbresult : " + int_reversdbresult);
                logger.trace("------------------reverse tran Dbinsert End ------------------" );                

            ht.put("RESULT_CD", "OF98");
            ht.put("RESULT_MSG", "시스템문제로 거래가 자동취소되었습니다."); 
            ht.put("TRAN_SEQ", strTpTranseq); 

        }
        else
        {
            ht.put("RESULT_CD", res_cd);
            ht.put("RESULT_MSG", res_msg); 
            ht.put("TRAN_SEQ", strTpTranseq); 
        }
        
        return ht;
    }    
    
    
    @Override
    @Transactional
    public Hashtable appAllatReqAction(AppFormBean appFormBean, AppOnffTidInfoVo appOnffTidInfoVo, AppTerminalInfoVo appTerminalInfoVo ) throws Exception {
        Hashtable ht = new Hashtable();
        
        KiccPgVo reqRefobj =  appFormBean.getReqKiccPgVo();
        AllatPgVo reqpgobj = appFormBean.getReqAllatPgVo();
        AllatPgVo respgobj = new AllatPgVo();
        
        Date today=new Date();
        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmss");       
        String strCurYear = formater.format(today);

	String strShopId   = appTerminalInfoVo.getTerminal_no();             // 리얼 반영시 Allat에 발급된 mall_id 사용
        String strCrossKey = appTerminalInfoVo.getTerminal_pwd();             // 리얼 반영시 Allat에 발급된 mall_id 사용
                
        if(strShopId == null || strShopId.equals(""))
        {
            throw new Exception("결제용TID가 없습니다.");
        }
        
        logger.debug("appTerminalInfoVo.getMerch_no() : " + appTerminalInfoVo.getMerch_no());
        
        String strTpMerchCms = appDAO.GetMerchCommisionInfo(appTerminalInfoVo.getMerch_no());
        
        logger.debug("strTpMerchCms : " + strTpMerchCms);        
        
        
        //가맹점주문번호를 생성한다.
        String strOrder_seq = appDAO.GetOrderSeq();
        
        reqpgobj.setAllat_order_no(strOrder_seq);

        logger.trace("------------------------------appAllatReqAction--------------------------------------");
        logger.trace("onffmerchno : " + appOnffTidInfoVo.getOnffmerch_no());
        logger.trace("onfftid : " + appOnffTidInfoVo.getOnfftid());
        logger.trace("Pay_mtd_seq : " + appOnffTidInfoVo.getPay_mtd_seq());        
        logger.trace("Shopid : " + strShopId);
        logger.trace("crossKey : " + strCrossKey);
        logger.trace("reqpgobj.getAllat_shop_id		"+reqpgobj.getAllat_shop_id		());//상점 ID                        
        logger.trace("reqpgobj.getAllat_order_no	"+reqpgobj.getAllat_order_no		());//주문번호                       
        logger.trace("reqpgobj.getAllat_amt		"+reqpgobj.getAllat_amt			());//승인금액                       
        logger.trace("reqpgobj.getAllat_card_no		"+reqpgobj.getAllat_card_no		());//카드번호                       
        logger.trace("reqpgobj.getAllat_cardvalid_ym	"+reqpgobj.getAllat_cardvalid_ym	());//카드 유효기간                  
        logger.trace("reqpgobj.getAllat_passwd_no	"+reqpgobj.getAllat_passwd_no		());//카드비밀번호                   
        logger.trace("reqpgobj.getAllat_sell_mm		"+reqpgobj.getAllat_sell_mm		());//할부개월값                     
        logger.trace("reqpgobj.getAllat_business_type	"+reqpgobj.getAllat_business_type	());//결제자카드종류                 
        logger.trace("reqpgobj.getAllat_registry_no	"+reqpgobj.getAllat_registry_no		());//주민번호                       
        logger.trace("reqpgobj.getAllat_biz_no		"+reqpgobj.getAllat_biz_no		());//사업자번호                     
        logger.trace("reqpgobj.getAllat_shop_member_id	"+reqpgobj.getAllat_shop_member_id	());//회원 ID                        
        logger.trace("reqpgobj.getAllat_product_cd	"+reqpgobj.getAllat_product_cd		());//상품코드                       
        logger.trace("reqpgobj.getAllat_product_nm	"+reqpgobj.getAllat_product_nm		());//상품명                         
        logger.trace("reqpgobj.getAllat_cardcert_yn	"+reqpgobj.getAllat_cardcert_yn		());//카드인증여부                   
        logger.trace("reqpgobj.getAllat_zerofee_yn	"+reqpgobj.getAllat_zerofee_yn		());//일반/무이자할부 사용여부       
        logger.trace("reqpgobj.getAllat_buyer_nm	"+reqpgobj.getAllat_buyer_nm		());//결제자성명                     
        logger.trace("reqpgobj.getAllat_recp_name	"+reqpgobj.getAllat_recp_name		());//수취인성명                     
        logger.trace("reqpgobj.getAllat_recp_addr	"+reqpgobj.getAllat_recp_addr		());//수취인주소                     
        logger.trace("reqpgobj.getAllat_user_ip		"+reqpgobj.getAllat_user_ip		());//결제자 IP                      
        logger.trace("reqpgobj.getAllat_email_addr	"+reqpgobj.getAllat_email_addr		());//결제자 이메일 주소             
        logger.trace("reqpgobj.getAllat_bonus_yn	"+reqpgobj.getAllat_bonus_yn		());//보너스포인트 사용여부          
        logger.trace("reqpgobj.getAllat_gender		"+reqpgobj.getAllat_gender		());//구매자 성별                    
        logger.trace("reqpgobj.getAllat_birth_ymd	"+reqpgobj.getAllat_birth_ymd		());//구매자 생년월일                
        logger.trace("reqpgobj.getAllat_cost_amt	"+reqpgobj.getAllat_cost_amt		());//결제금액에 대한 원 공급가 금액 
        logger.trace("reqpgobj.getAllat_pay_type	"+reqpgobj.getAllat_pay_type		());//결제방식                       
        logger.trace("reqpgobj.getAllat_test_yn		"+reqpgobj.getAllat_test_yn		());//테스트여부                     
        logger.trace("reqpgobj.getAllat_opt_pin		"+reqpgobj.getAllat_opt_pin		());//올앳참조필드                   
        logger.trace("reqpgobj.getAllat_opt_mod		"+reqpgobj.getAllat_opt_mod		());//올앳참조필드    
        logger.trace("--------------------------------------------------------------------");        
        
        //복합과세용 정보를 만든다.
        //과세금액|부가세|면세금액형태로 만든다.
        String strTpTaxamt = Util.nullToInt(reqRefobj.getCom_tax_amt());//과세금액
        String strTpVamt = Util.nullToInt(reqRefobj.getCom_vat_amt());//부가세금액
        String strTpFreeAmt = Util.nullToInt(reqRefobj.getCom_free_amt());//비과세금액
        
        String strTpCPX = strTpTaxamt + "|" + strTpVamt + "|" + strTpFreeAmt;
        
        //수수료정보를 가져온다.
        appFormBean.setPay_mtd_seq(appOnffTidInfoVo.getPay_mtd_seq());
        List ls_onfftidcmsinfo = appDAO.GetOnffTidCommission(appFormBean);
        AppOnffTidInfoVo onfftidcmsinfo = (AppOnffTidInfoVo)ls_onfftidcmsinfo.get(0);
        
	logger.trace("------------------------------appAllatReqAction--------------------------------------");   
        logger.trace("getOnfftid_cms_seq : " + onfftidcmsinfo.getOnfftid_cms_seq());        
        logger.trace("getOnfftid : " + onfftidcmsinfo.getOnfftid());      
        logger.trace("getCommission : " + onfftidcmsinfo.getCommission());        
        logger.trace("getStart_dt : " + onfftidcmsinfo.getStart_dt());        
        logger.trace("getEnd_dt : " + onfftidcmsinfo.getEnd_dt());        
        logger.trace("--------------------------------------------------------------------");  
               
        
        /* -------------------------------------------------------------------------- */
        /* ::: 결제 결과                                                              */
        /* -------------------------------------------------------------------------- */
        String r_reply_cd	  = "";//결과코드     
        String r_reply_msg	  = "";//결과메세지   
        String r_order_no	  = "";//주문번호     
        String r_amt		  = "";//승인금액     
        String r_pay_type	  = "";//지불수단     
        String r_approval_ymdhms  = "";//승인일시     
        String r_seq_no		  = "";//거래일련번호 
        String r_approval_no	  = "";//승인번호     
        String r_card_id	  = "";//카드ID       
        String r_card_nm	  = "";//카드명       
        String r_sell_mm	  = "";//할부개월     
        String r_zerofee_yn	  = "";//무이자여부   
        String r_cert_yn	  = "";//인증여부     
        String r_contract_yn	  = "";//직가맹여부   
            
        HashMap reqHm=new HashMap();
        HashMap resHm=null;
        String szReqMsg="";
        String szAllatEncData="";
        String szCrossKey=""; 
        
        logger.debug("service reqpgobj.getAllat_test_yn() : ["+Util.nullToString(reqpgobj.getAllat_test_yn())+"]");
        
        reqHm.put("allat_card_no"           , Util.nullToString(reqpgobj.getAllat_card_no()));
        reqHm.put("allat_cardvalid_ym"      , Util.nullToString(reqpgobj.getAllat_cardvalid_ym()) );
        reqHm.put("allat_passwd_no"         , Util.nullToString(reqpgobj.getAllat_passwd_no())   );
        reqHm.put("allat_sell_mm"           , Util.nullToString(reqpgobj.getAllat_sell_mm())       );
        reqHm.put("allat_amt"               , Util.nullToString(reqpgobj.getAllat_amt())          );//승인금액
        //복합과세용 필드
        reqHm.put("allat_multi_amt"               , strTpCPX          );//복합결제금액
        reqHm.put("allat_business_type"     , Util.nullToString(reqpgobj.getAllat_business_type()) );
        if( Util.nullToSpaceString(reqpgobj.getAllat_business_type()).equals("0") ){
            reqHm.put( "allat_registry_no"  , Util.nullToString(reqpgobj.getAllat_registry_no())   );
        }else{
            reqHm.put( "allat_biz_no"       , Util.nullToString(reqpgobj.getAllat_biz_no())        );
        }
        reqHm.put("allat_shop_id"           , strShopId       );
        reqHm.put("allat_shop_member_id"    , Util.nullToString(reqpgobj.getAllat_shop_member_id()) );
        reqHm.put("allat_order_no"          , Util.nullToString(reqpgobj.getAllat_order_no())      );
        reqHm.put("allat_product_cd"        , Util.nullToString(reqpgobj.getAllat_product_cd())    );
        reqHm.put("allat_product_nm"        , Util.nullToString(reqpgobj.getAllat_product_nm())    );
        reqHm.put("allat_cardcert_yn"       , Util.nullToString(reqpgobj.getAllat_cardcert_yn())   );
        reqHm.put("allat_zerofee_yn"        , Util.nullToString(reqpgobj.getAllat_zerofee_yn())    );
        reqHm.put("allat_buyer_nm"          , Util.nullToString(reqpgobj.getAllat_buyer_nm())      );
        reqHm.put("allat_recp_name"         , Util.nullToString(reqpgobj.getAllat_recp_name())     );
        reqHm.put("allat_recp_addr"         , Util.nullToString(reqpgobj.getAllat_recp_addr())     );
        reqHm.put("allat_user_ip"           , Util.nullToString(reqpgobj.getAllat_user_ip())       );
        reqHm.put("allat_email_addr"        , Util.nullToString(reqpgobj.getAllat_email_addr())    );
        reqHm.put("allat_bonus_yn"          , Util.nullToString(reqpgobj.getAllat_bonus_yn())      );
        reqHm.put("allat_gender"            , Util.nullToString(reqpgobj.getAllat_gender())        );
        reqHm.put("allat_birth_ymd"         , Util.nullToString(reqpgobj.getAllat_birth_ymd())     );
        reqHm.put("allat_pay_type"          , "NOR"          );  //수정금지(결제방식 정의)
        //reqHm.put("allat_test_yn"         , "N"            );  //테스트 :Y, 서비스 :N
        reqHm.put("allat_test_yn"           , Util.nullToString(reqpgobj.getAllat_test_yn())            );  //테스트 :Y, 서비스 :N
        reqHm.put("allat_opt_pin"           , "NOUSE"        );  //수정금지(올앳 참조 필드)
        reqHm.put("allat_opt_mod"           , "APP"          );  //수정금지(올앳 참조 필드)        
        
        AllatUtil util = new AllatUtil();
        szAllatEncData=util.setValue(reqHm);
        szReqMsg  = "allat_shop_id="   + strShopId
              + "&allat_amt="      + Util.nullToString(reqpgobj.getAllat_amt())
              + "&allat_enc_data=" + szAllatEncData
              + "&allat_cross_key="+ strCrossKey;
        
        resHm = util.approvalReq(szReqMsg, "SSL");
        
        
        r_reply_cd   = (String)resHm.get("reply_cd");
        r_reply_msg  = (String)resHm.get("reply_msg");
        
	logger.trace("--------------------------------------------------------------------");
        logger.trace("r_reply_cd : " + r_reply_cd);
        logger.trace("r_reply_msg : " + r_reply_msg);
        logger.trace("--------------------------------------------------------------------");     
        
        /* -------------------------------------------------------------------------- */
        /* ::: 결과 처리                                                              */
        /* -------------------------------------------------------------------------- */
        r_order_no	  = Util.nullToSpaceString((String)resHm.get("order_no"));//주문번호     
        r_amt		  = Util.nullToSpaceString((String)resHm.get("amt"));//승인금액     
        r_pay_type	  = Util.nullToSpaceString((String)resHm.get("pay_type"));//지불수단     
        r_approval_ymdhms = Util.nullToSpaceString((String)resHm.get("approval_ymdhms"));//승인일시     
        r_seq_no	  = Util.nullToSpaceString((String)resHm.get("seq_no"));//거래일련번호 
        r_approval_no	  = Util.nullToSpaceString((String)resHm.get("approval_no"));//승인번호     
        r_card_id	  = Util.nullToSpaceString((String)resHm.get("card_id"));//카드ID       
        r_card_nm	  = Util.nullToSpaceString((String)resHm.get("card_nm"));//카드명       
        r_sell_mm	  = Util.nullToSpaceString((String)resHm.get("sell_mm"));//할부개월     
        r_zerofee_yn	  = Util.nullToSpaceString((String)resHm.get("zerofee_yn"));//무이자여부   
        r_cert_yn	  = Util.nullToSpaceString((String)resHm.get("cert_yn"));//인증여부     
        r_contract_yn	  = Util.nullToSpaceString((String)resHm.get("contract_yn"));//직가맹여부
        
        //무이자일경우
        if(r_zerofee_yn.equals("Y"))
        {
            r_zerofee_yn = "02";
        }
        //일반일경우
        else
        {
            r_zerofee_yn = "00";
        }
        
        logger.trace("--------------------------------------------------------------------");        
        logger.trace("r_order_no	:"+ r_order_no		  );//주문번호     
        logger.trace("r_amt		:"+ r_amt		  );//승인금액     
        logger.trace("r_pay_type	:"+ r_pay_type		  );//지불수단     
        logger.trace("r_approval_ymdhms	:"+ r_approval_ymdhms	  );//승인일시     
        logger.trace("r_seq_no		:"+ r_seq_no		  );//거래일련번호 
        logger.trace("r_approval_no	:"+ r_approval_no	  );//승인번호     
        logger.trace("r_card_id		:"+ r_card_id		  );//카드ID       
        logger.trace("r_card_nm		:"+ r_card_nm		  );//카드명       
        logger.trace("r_sell_mm		:"+ r_sell_mm		  );//할부개월     
        logger.trace("r_zerofee_yn	:"+ r_zerofee_yn	  );//무이자여부   
        logger.trace("r_cert_yn		:"+ r_cert_yn		  );//인증여부     
        logger.trace("r_contract_yn	:"+ r_contract_yn	  );//직가맹여부
        logger.trace("--------------------------------------------------------------------");
    
        respgobj.setReply_cd		(r_reply_cd);//결과코드                       
        respgobj.setReply_msg		(r_reply_msg);//결과메세지                     
        respgobj.setOrder_no		(r_order_no);//주문번호                       
        respgobj.setAmt			(r_amt);//승인금액                       
        respgobj.setPay_type		(r_pay_type);//지불수단                       
        respgobj.setApproval_ymdhms	(r_approval_ymdhms);//승인일시                       
        respgobj.setSeq_no		(r_seq_no);//거래일련번호                   
        respgobj.setApproval_no		(r_approval_no);//승인번호                       
        respgobj.setCard_id		(r_card_id);//카드ID                         
        respgobj.setCard_nm		(r_card_nm);//카드명                         
        respgobj.setSell_mm		(r_sell_mm);//할부개월                       
        respgobj.setZerofee_yn		(r_zerofee_yn);//무이자여부                     
        respgobj.setCert_yn		(r_cert_yn);//인증여부                       
        respgobj.setContract_yn		(r_contract_yn);//직가맹여부     
        
        //가맹점 DB처리
        //저장정보를 만든다.
    
        InsertCardPgTranBean insobj = new InsertCardPgTranBean();
        //승인 결가에 따라 저장을 위한 테이블명을 만든다.
        //성공관련 data 셋팅
        if (r_reply_cd.equals("0000"))
        {
            insobj.setInstablenm("TB_TRAN_CARDPG_"+r_approval_ymdhms.substring(2, 6));
            insobj.setApp_dt(r_approval_ymdhms.substring(0, 8));
            insobj.setApp_tm(r_approval_ymdhms.substring(8, 14));
            insobj.setResult_status("00");
            insobj.setAcc_chk_flag("0");
            insobj.setTot_amt(r_amt);//결제총금액
                        
        }
        else
        {
            insobj.setInstablenm("TB_TRAN_CARDPG_"+strCurYear.substring(2, 6));
            insobj.setResult_status("99");                        
            insobj.setAcc_chk_flag("3");
            insobj.setTot_amt(Util.nullToSpaceString(reqpgobj.getAllat_amt()) );//결제총금액
        }
        
        String strTpTranseq = appDAO.GetTranSeq();
        insobj.setTran_seq(strTpTranseq);// 거래번호 셋팅
        //카드결제 시
             
        insobj.setMassagetype("10");//메세지타입
        //insobj.setCard_num(r_card_no);//카드번호
        insobj.setCard_num(reqpgobj.getAllat_card_no());//카드번호
        insobj.setApp_card_num(reqpgobj.getAllat_card_no());//카드번호 승인시 카드번호 없어 기존카드번호 입력
        insobj.setApp_no(r_approval_no);//승인번호

        insobj.setPg_seq(r_seq_no);//pg거래고유번호
        insobj.setPay_chn_cate(appOnffTidInfoVo.getPay_chn_cate());
        insobj.setTid(strShopId);
        insobj.setOnffmerch_no(appOnffTidInfoVo.getOnffmerch_no());        
        insobj.setOnfftid(appOnffTidInfoVo.getOnfftid());
        insobj.setWcc(reqRefobj.getWcc());
        insobj.setExpire_dt(reqpgobj.getAllat_cardvalid_ym());
        insobj.setCard_cate(reqRefobj.getCard_user_type());
        insobj.setOrder_seq(reqpgobj.getAllat_order_no());
        insobj.setTran_dt(strCurYear.substring(0, 8));
        insobj.setTran_tm(strCurYear.substring(8, 14));
        insobj.setTran_cate(reqRefobj.getCard_txtype());
        insobj.setFree_inst_flag(r_zerofee_yn);
        insobj.setTax_amt(reqRefobj.getCom_vat_amt());//부가세금액
        insobj.setSvc_amt(reqRefobj.getCom_free_amt());//비과세승인금액
        //insobj.setCurrency("");
        insobj.setInstallment(r_sell_mm);//할부
                        
        if(r_reply_cd.equals("0000"))
        {    
            appFormBean.setParam_app_cmp_cd("ALLATPG");
            appFormBean.setParam_card_cd(r_card_id);
                                
            List ls_iss_cd = null;
            ls_iss_cd = appDAO.GetCardCode(appFormBean);
                
            if(ls_iss_cd == null || ls_iss_cd.size() <= 0)
            {
                insobj.setIss_cd("091");
                insobj.setIss_nm("기타카드");                    
            }
            else
            {
                Tb_Main_Code  isscodeobj = (Tb_Main_Code)ls_iss_cd.get(0);
                insobj.setIss_cd(isscodeobj.getDetail_code());
                insobj.setIss_nm(isscodeobj.getDetailcode_nm());            
            }           

            /* allat 매입사코드 없어서 기타카드처리
            appFormBean.setParam_app_cmp_cd("ALLATPG");
            appFormBean.setParam_card_cd(null);
                                
            List ls_acq_cd = null;
                                
            ls_acq_cd = appDAO.GetCardCode(appFormBean);
            */
            
            List ls_acq_cd = null;
            
            if(ls_acq_cd == null || ls_acq_cd.size() <= 0)
            {
                insobj.setAcq_cd("091");
                insobj.setAcq_nm("기타카드"); 
            }
            else
            {
                Tb_Main_Code  acqcodeobj = (Tb_Main_Code)ls_acq_cd.get(0);

                insobj.setAcq_cd(acqcodeobj.getDetail_code());
                insobj.setAcq_nm(acqcodeobj.getDetailcode_nm());    
            }
            
            //van수수료 계산
            BigDecimal bigdec_TpVanCms = new BigDecimal(strTpMerchCms).divide(new BigDecimal("100"));
            BigDecimal bigdec_TpApp_vanAmt = new BigDecimal(r_amt);
            BigDecimal bigdec_VanApp_cms = bigdec_TpApp_vanAmt.multiply(bigdec_TpVanCms);
            BigDecimal bigdec_VanApp_cms_vat = bigdec_VanApp_cms.multiply(new BigDecimal("0.1"));
            double dbl_VanApp_cms = Math.floor( bigdec_VanApp_cms.doubleValue() );
            double dbl_VanApp_cms_vat = Math.floor( bigdec_VanApp_cms_vat.doubleValue() );
            BigDecimal bigdec_VanApptot_cms = new BigDecimal(String.valueOf(dbl_VanApp_cms+dbl_VanApp_cms_vat));
            BigDecimal bigdec_VanApp_payamt = bigdec_TpApp_vanAmt.subtract(bigdec_VanApptot_cms); 
            insobj.setPay_amt(String.valueOf(bigdec_VanApp_payamt.doubleValue()));
            insobj.setCommision(String.valueOf(bigdec_VanApptot_cms.doubleValue()));
            
            //수수료 계산
            String strTpCms = onfftidcmsinfo.getCommission();
            
            BigDecimal bigdec_TpCms = new BigDecimal(strTpCms).divide(new BigDecimal("100"));
                                
            BigDecimal bigdec_TpApp_mat = new BigDecimal(r_amt);
           
            BigDecimal bigdec_onfftid_cmd = bigdec_TpApp_mat.multiply(bigdec_TpCms);
                                
            double dbl_onfftid_cms = Math.floor( bigdec_onfftid_cmd.doubleValue() );
                                
            BigDecimal bigdec_onfftid_payamt = bigdec_TpApp_mat.subtract(new BigDecimal(String.valueOf(dbl_onfftid_cms))); 
                                
            insobj.setOnofftid_pay_amt( String.valueOf((long)bigdec_onfftid_payamt.doubleValue()) );
            insobj.setOnofftid_commision(String.valueOf((long)dbl_onfftid_cms));  
        }
                        
        insobj.setApp_iss_cd(r_card_id);
        insobj.setApp_iss_nm(r_card_nm);
                        
        insobj.setApp_acq_cd(null);
        insobj.setApp_acq_nm(null);
                        
        //pg의 경우 결제MID가 가맹점 번호로 성공시 나오는 카드사 가맹점 번호는 원 가맹점 번호로 입력
        //allat의 경우 응답에 shopid가 없으므로 전송값 입력
        insobj.setMerch_no(strShopId);
        insobj.setOrg_merch_no(strShopId);
                        
        insobj.setResult_cd(r_reply_cd);
        insobj.setResult_msg(r_reply_msg);
        
        insobj.setTran_status("00");//승인 :00 ,10:당일취소 , 20:매입취소
        insobj.setTran_step("00");
                        
        insobj.setClient_ip(reqpgobj.getAllat_user_ip());
        insobj.setUser_type(reqRefobj.getUser_type());
        insobj.setMemb_user_no(reqRefobj.getMemb_user_no());
        insobj.setUser_id(reqRefobj.getUser_id());
        insobj.setUser_nm(reqRefobj.getUser_nm());
        insobj.setUser_mail(reqRefobj.getUser_mail());
        insobj.setUser_phone1(reqRefobj.getUser_phone1());
        insobj.setUser_phone2(reqRefobj.getUser_phone2());
        insobj.setUser_addr(reqRefobj.getUser_addr());
        insobj.setProduct_type(reqRefobj.getProduct_type());
        insobj.setProduct_nm(reqRefobj.getProduct_nm());

        insobj.setTrad_chk_flag("0");
 
        insobj.setIns_user(appFormBean.getUser_seq());
        insobj.setMod_user(appFormBean.getUser_seq());
                        
        
        //20150902 거래 있을시 입력 패치   
        //가맹점 DB 처리
        /*
        Integer int_dbresult = -1;
        
        try
        {
            int_dbresult = appDAO.InsertCardPgTran(insobj);
        }
        catch(Exception ex_ins)
        {
            logger.error(ex_ins);
            int_dbresult = -1;
        }
        
        String strTpResult = int_dbresult.toString();
        */
        Integer int_dbresult = -1;
        
        try
        {
            AppFormBean appchkFormBean = new AppFormBean();
            //logger.debug("-------------appchk start------------------");
            appchkFormBean.setChkpgseq(r_seq_no);
            appchkFormBean.setChkpgtid(strShopId);
            //logger.debug("r_cno : " + r_cno);
            //logger.debug("g_conf_site_cd : "+ g_conf_site_cd);
            String strTpAppChk = Util.nullToString(appDAO.getOnffAppChkCnt(appchkFormBean));
            
            if(strTpAppChk.equals("")) 
            {
                strTpAppChk="0";
            }
            
            logger.trace("strTpAppChk : " + strTpAppChk);

            Integer int_reversdbresult = -1;

            if(strTpAppChk.equals("0"))
            {
                int_dbresult = appDAO.InsertCardPgTran(insobj);
            }
            else
            {
                
                int_dbresult = 1;
                logger.info("승인거래 기존재");
            }            
            //logger.debug("-------------appchk end------------------");
        }
        catch(Exception ex_ins)
        {
            logger.error(ex_ins);
            int_dbresult = -1;
        }
        
        String strTpResult = int_dbresult.toString();
        
        logger.trace("------------------Card App insert start------------------");
        logger.trace("card App insert result : " + strTpResult);
        logger.trace("------------------Card App insert End ------------------");
        
        //strTpResult ="0";
        
        /* -------------------------------------------------------------------------- */
        /* ::: 가맹점 DB 처리                                                         */
        /* -------------------------------------------------------------------------- */
        /* r_amount가 주문DB의 금액과 다를 시 반드시 취소 요청을 하시기 바랍니다.     */
        /* DB 처리 실패 시 취소 처리를 해주시기 바랍니다.                             */
        /* -------------------------------------------------------------------------- */        
        //DB처리 실패시 자동 망취소
        if ( !strTpResult.equals("1") && r_reply_cd.equals("0000") )
        {
            HashMap rtran_reqHm=new HashMap();
            HashMap rtran_resHm=null;
            String rtran_szReqMsg="";
            String rtran_szAllatEncData="";
            String rtran_szCrossKey="";
            
            // 취소 요청 정보
            //------------------------------------------------------------------------
            String rtran_szShopId ="";
            String rtran_szAmt    ="";
            String rtran_szOrderNo="";
            String rtran_szPayType="";
            String rtran_szSeqNo  ="";
            
            //int mod_data_set_no;

        // 정보 입력
        //------------------------------------------------------------------------
            rtran_szCrossKey      = strCrossKey;    //해당 CrossKey값
            rtran_szShopId        = strShopId;      //ShopId 값               (최대  20 자리)
            rtran_szAmt           = r_amt;        //취소 금액               (최대  10 자리)
            rtran_szOrderNo       = r_order_no;   //주문번호                (최대  80 자리)
            rtran_szPayType       = "CARD";        //원거래건의 결제방식[카드:CARD,계좌이체:ABANK]
            rtran_szSeqNo         = r_seq_no;    //거래일련번호:옵션필드   (최대  10자리)

            rtran_reqHm.put("allat_shop_id" ,  rtran_szShopId );
            rtran_reqHm.put("allat_order_no",  rtran_szOrderNo);
            rtran_reqHm.put("allat_amt"     ,  rtran_szAmt    );
            rtran_reqHm.put("allat_pay_type",  rtran_szPayType);
            rtran_reqHm.put("allat_test_yn" ,  Util.nullToString(reqpgobj.getAllat_test_yn())      );    //테스트 :Y, 서비스 :N
            rtran_reqHm.put("allat_opt_pin" ,  "NOUSE"  );    //수정금지(올앳 참조 필드)
            rtran_reqHm.put("allat_opt_mod" ,  "APP"    );    //수정금지(올앳 참조 필드)
            rtran_reqHm.put("allat_seq_no"  ,  rtran_szSeqNo  );    //옵션 필드( 삭제 가능함 )            
                        

            AllatUtil rtran_util = new AllatUtil();

            rtran_szAllatEncData=rtran_util.setValue(reqHm);
            rtran_szReqMsg  = "allat_shop_id="   + rtran_szShopId
                      + "&allat_amt="      + rtran_szAmt
                      + "&allat_enc_data=" + rtran_szAllatEncData
                      + "&allat_cross_key="+ rtran_szCrossKey;

            rtran_resHm = rtran_util.cancelReq(rtran_szReqMsg, "SSL");
            
            String rtran_res_cd;
            String rtran_res_msg; 
            
            /*
            Date Rcncltoday=new Date();
            SimpleDateFormat Rcnclformater = new SimpleDateFormat("yyyyMMddHHmmss");       
            String RcnclCurDate = Rcnclformater.format(Rcncltoday);                
            */
            rtran_res_cd  = (String)resHm.get("reply_cd");
            rtran_res_msg = (String)resHm.get("reply_msg");
            
            String rtran_sCancelYMDHMS    = (String)rtran_resHm.get("cancel_ymdhms");
            String rtran_sPartCancelFlag  = (String)rtran_resHm.get("part_cancel_flag");
            String rtran_sRemainAmt       = (String)rtran_resHm.get("remain_amt");
            String rtran_sPayType         = (String)rtran_resHm.get("pay_type");
            
            
            logger.trace("------------------reverse tran result start------------------" );
            logger.trace("revers rtran_res_cd : " + rtran_res_cd);
            logger.trace("revers rtran_res_msg  : " + rtran_res_msg);
            logger.trace("revers rtran_szSeqNo : " + rtran_szSeqNo);
            logger.trace("revers rtran_sCancelYMDHMS : " + rtran_sCancelYMDHMS);
            logger.trace("------------------reverse tran result End ------------------" );
                
            Date today_cncl=new Date();
            String strCurYear_cncl = formater.format(today_cncl);//'YYYYMMDDhhmmss'
                
            //r거래값을 입력한다.
            InsertCardPgTranBean cnclobj = new InsertCardPgTranBean();
            //가맹점 DB 처리에러시 
            String strTpCnclTranseq = appDAO.GetTranSeq();
            cnclobj.setTran_seq(strTpCnclTranseq);// 거래번호 셋팅
                
            if (rtran_res_cd.equals("0000"))
            {
                //cnclobj.setInstablenm("TB_TRAN_CARDPG_"+r_canc_date.substring(2, 6));
                cnclobj.setInstablenm("TB_TRAN_CARDPG_RTRAN");
                cnclobj.setApp_dt(rtran_sCancelYMDHMS.substring(0, 8));
                cnclobj.setApp_tm(rtran_sCancelYMDHMS.substring(8, 14));
                cnclobj.setResult_status("00");
                cnclobj.setAcc_chk_flag("3");//r거래는 
                cnclobj.setTran_step("00");
                cnclobj.setCncl_dt(rtran_sCancelYMDHMS.substring(0, 8));
            }
            else
            {
                //cnclobj.setInstablenm("TB_TRAN_CARDPG_"+strCurYear_cncl.substring(2, 6));
                cnclobj.setInstablenm("TB_TRAN_CARDPG_RTRAN");
                cnclobj.setResult_status("99");                        
                cnclobj.setAcc_chk_flag("3");//r거래는3
            }
                
            cnclobj.setMassagetype("50");//메세지타입
            cnclobj.setCard_num(insobj.getCard_num());//카드번호
            cnclobj.setApp_card_num(insobj.getApp_card_num());//카드번호
            cnclobj.setApp_no(null);//승인번호
            cnclobj.setTot_amt(insobj.getTot_amt());//결제총금액
            cnclobj.setPg_seq(rtran_szSeqNo);//pg거래고유번호
            cnclobj.setPay_chn_cate(insobj.getPay_chn_cate());
            cnclobj.setTid(strShopId);
            cnclobj.setOnffmerch_no(insobj.getOnffmerch_no());        
            cnclobj.setOnfftid(insobj.getOnfftid());
            cnclobj.setWcc(insobj.getWcc());
            cnclobj.setExpire_dt(insobj.getExpire_dt());
            cnclobj.setCard_cate(insobj.getCard_cate());
            cnclobj.setOrder_seq(insobj.getOrder_seq());
            cnclobj.setTran_dt(insobj.getTran_dt());
            cnclobj.setTran_tm(insobj.getTran_tm());
            cnclobj.setTran_cate("40");//취소
            cnclobj.setFree_inst_flag(insobj.getFree_inst_flag());
            cnclobj.setTax_amt(insobj.getTax_amt());//부가세금액
            cnclobj.setSvc_amt(insobj.getSvc_amt());//비과세승인금액
            cnclobj.setInstallment(insobj.getInstallment());//할부
                
            cnclobj.setClient_ip(insobj.getClient_ip());
            cnclobj.setUser_nm(insobj.getUser_nm());
            cnclobj.setUser_phone1(insobj.getUser_phone1());
            cnclobj.setUser_phone2(insobj.getUser_phone2());
            cnclobj.setProduct_nm(insobj.getProduct_nm());                
                        
            cnclobj.setMerch_no(insobj.getMerch_no());
            cnclobj.setResult_cd(rtran_res_cd);
            cnclobj.setResult_msg(rtran_res_msg);

            cnclobj.setOrg_app_dd(insobj.getApp_dt());
            cnclobj.setOrg_app_no(insobj.getApp_no());
            cnclobj.setOrg_pg_seq(insobj.getPg_seq());
            cnclobj.setCncl_reason( "DB 처리 실패로 망취소");
                
            cnclobj.setTran_status("10");//승인 :00 ,10:당일취소 , 20:매입취소
                        
            cnclobj.setOnofftid_pay_amt(insobj.getOnofftid_pay_amt());
            cnclobj.setOnofftid_commision(insobj.getOnofftid_commision());
            cnclobj.setClient_ip(insobj.getClient_ip());
                
            cnclobj.setTrad_chk_flag("0");//거래대사
            
            cnclobj.setIns_user(appFormBean.getUser_seq());
            cnclobj.setMod_user(appFormBean.getUser_seq());
                
            Integer int_reversdbresult = appDAO.InsertCardPgTran(cnclobj);
                
            logger.trace("------------------reverse tran Dbinsert start------------------" );
            logger.trace("int_reversdbresult : " + int_reversdbresult);
            logger.trace("------------------reverse tran Dbinsert End ------------------" );                

            ht.put("RESULT_CD", "OF98");
            ht.put("RESULT_MSG", "시스템문제로 거래가 자동취소되었습니다."); 
            ht.put("TRAN_SEQ", strTpTranseq); 

        }
        else
        {
            ht.put("RESULT_CD", r_reply_cd);
            ht.put("RESULT_MSG", r_reply_msg); 
            ht.put("TRAN_SEQ", strTpTranseq); 
        }
        
        return ht;
    }    
    

    @Override
    @Transactional
    public Hashtable appResult(AppFormBean appFormBean) throws Exception {
         Hashtable ht = new Hashtable();
         List ls_tran_info = appDAO.GetCardTranInfo(appFormBean);
         
        Tb_Tran_CardPg cdResult =(Tb_Tran_CardPg)ls_tran_info.get(0);
        ht.put("tran_info_obj", cdResult);
         
        CompanyFormBean companyFormBean = new  CompanyFormBean();
        companyFormBean.setComp_seq("2015010100000001");        
        List ls_onffCompInfo = compDAO.companyMasterInfo(companyFormBean);
        
        Tb_Company onffCompInfo= (Tb_Company)ls_onffCompInfo.get(0);
        
        logger.trace(onffCompInfo.getComp_nm());
        
        ht.put("onffCompInfo",onffCompInfo);
        
        return ht;
    }

    @Override
    public Hashtable appCashReady(AppFormBean appFormBean) throws Exception {
        Hashtable ht = new Hashtable();
        
        logger.trace("----------------------------------------------------------------");
        logger.trace("appcashReady appFormBean.getOnfftid val : " + appFormBean.getOnfftid());
        logger.trace("appcashReady appFormBean.getOnffmerch_no val : " + appFormBean.getOnffmerch_no());
         logger.trace("appcashReady appFormBean.getUser_cate val : " + appFormBean.getUser_cate());
        logger.trace("----------------------------------------------------------------");
        
        //온오프가맹점정보를 셋팅한다.
        String strUserCateVal = appFormBean.getUser_cate();
        
        StringBuffer sbtid = new StringBuffer();
        StringBuffer sbmerch = new StringBuffer();
        
        //가맹점 관리자
        if(strUserCateVal.equals("01"))
        {
          //TID 정보
           List list_onfftid_info = appDAO.GetOnffTidInfo(appFormBean);  
           
           for(int i=0 ;i < list_onfftid_info.size() ; i++ )
           {
               AppOnffTidInfoVo onfftidInfo = (AppOnffTidInfoVo)list_onfftid_info.get(i);
               
               sbtid.append("<option value="+onfftidInfo.getOnfftid()+">"+onfftidInfo.getOnfftid_nm()+"</option>");
           }
           
           ht.put("onfftid_info", sbtid.toString());
            
           //가맹점 정보
           List list_merch_info = appDAO.GetOnffMerchInfo(appFormBean);
        
           Tb_Onffmerch_Mst merch_info =  (Tb_Onffmerch_Mst)list_merch_info.get(0);
           
           sbmerch.append("<option value="+merch_info.getOnffmerch_no()+">"+merch_info.getMerch_nm()+"</option>");
        
           ht.put("onffmerch_info", sbmerch.toString());
           
        }
        //결제관리자
        else if(strUserCateVal.equals("02"))
        {
            ////TID 정보
           List list_onfftid_info = appDAO.GetOnffTidInfo(appFormBean);
           for(int i=0 ;i < list_onfftid_info.size() ; i++ )
           {
               AppOnffTidInfoVo onfftidInfo = (AppOnffTidInfoVo)list_onfftid_info.get(i);
               
               sbtid.append("<option value="+onfftidInfo.getOnfftid()+">"+onfftidInfo.getOnfftid_nm()+"</option>");
               sbmerch.append("<option value="+onfftidInfo.getOnffmerch_no()+">"+onfftidInfo.getMerch_nm()+"</option>");
           }
           
          ht.put("onfftid_info", sbtid.toString());
          ht.put("onffmerch_info", sbmerch.toString());          
        }
        
        return ht;
    }

    @Override
    public Hashtable appReqCashAction(AppFormBean appFormBean) throws Exception {
     Hashtable ht = new Hashtable();

        //가맹점 번호 검사
        
        logger.trace("----------------------------------------------------------------");
        logger.trace("AppServiceImpl appReqCashAction appFormBean.getOnfftid val : " + appFormBean.getOnfftid());
        logger.trace("AppServiceImpl appReqCashAction appFormBean.getOnffmerch_no val : " + appFormBean.getOnffmerch_no());
        logger.trace("AppServiceImpl appReqCashAction appFormBean.getUser_cate val : " + appFormBean.getUser_cate());
        
        logger.trace("AppServiceImpl appReqCashAction appFormBean.getParam_onffmerch_no val : " + appFormBean.getParam_onffmerch_no());
        logger.trace("AppServiceImpl appReqCashAction appFormBean.getParam_onfftid val : " + appFormBean.getParam_onfftid());
        logger.trace("----------------------------------------------------------------");
        
        String strUserCateVal = appFormBean.getUser_cate();
        AppOnffTidInfoVo onfftidInfo = null;
        //가맹점 관리자
        if(strUserCateVal.equals("01"))
        {
           //가맹점 번호의 검사
           if(!appFormBean.getParam_onffmerch_no().equals(appFormBean.getOnffmerch_no()) || appFormBean.getOnffmerch_no() == null)
           {
               logger.trace("----------------------- appReqAction strUserCateVal : 0 => 가맹점번호 검사 ----------------------------------");
               //throw new Exception("사용할수 없는 가맹점번호 입니다.");
               ht.put("RESULT_CD", "OF01");
               ht.put("RESULT_MSG", "사용할수 없는 가맹점번호 입니다.");
               
               return ht;
           }           
           //TID 정보
           List list_onfftid_info = appDAO.GetOnffTidInfo(appFormBean);  
           if(list_onfftid_info.size() == 0)
           {
               logger.trace("----------------------- appReqAction strUserCateVal : 0 => TID 검사 ----------------------------------");
               //throw new Exception("사용할수 없는 결제TID 입니다.");
               ht.put("RESULT_CD", "OF02");
               ht.put("RESULT_MSG", "사용할수 없는 결제TID 입니다.(01)");
               
               return ht;
           }           
           
           onfftidInfo = (AppOnffTidInfoVo)list_onfftid_info.get(0);
        }
        //결제관리자
        else if(strUserCateVal.equals("02"))
        {
           //TID번호의 검사
           if(!appFormBean.getParam_onfftid().equals(appFormBean.getOnfftid()) || appFormBean.getOnfftid() == null)
           {
               logger.trace("----------------------- appReqAction strUserCateVal : 02 => TID 검사 ----------------------------------");
               //throw new Exception("사용할수 없는 결제TID 입니다.");
               ht.put("RESULT_CD", "OF02");
               ht.put("RESULT_MSG", "사용할수 없는 결제TID 입니다.(02)");
               
               return ht;               
           }                 
            
            ////TID 정보
           List list_onfftid_info = appDAO.GetOnffTidInfo(appFormBean);
           onfftidInfo = (AppOnffTidInfoVo)list_onfftid_info.get(0);
        }        
        logger.trace(" appReqAction onfftidInfo.toString : "+ onfftidInfo.toString() +" ----------------------------------");
        
        //결제관련 ONFFTID 정보셋팅
        ht.put("onfftidInfo", onfftidInfo);
        
        KiccPgVo reqcardinfoobj = appFormBean.getReqKiccPgVo();
        /*
                    kiccpgparam.setTot_amt          (getNullToZero(request.getParameter("EP_tot_amt")));          // [필수]결제 총 금액
            kiccpgparam.setCom_free_amt     (getNullToZero(request.getParameter("EP_service_amt")));     // [필수]비과세 승인 금액(봉사료)
            kiccpgparam.setCom_vat_amt      (getNullToZero(request.getParameter("EP_vat")));      // [필수]부가세 금액
        */
        
        //금액 검사
        String strTpTotAmt = Util.getNullToZero(reqcardinfoobj.getTot_amt()).trim();
        String strTpComFreeAmt = Util.getNullToZero(reqcardinfoobj.getCom_free_amt()).trim();
        String strTpComVatAmt = Util.getNullToZero(reqcardinfoobj.getCom_vat_amt()).trim();

        int intTpTotAmt = Integer.parseInt(strTpTotAmt);
        int intTpComFreeAmt = Integer.parseInt(strTpComFreeAmt);
        int intTpComVatAmt = Integer.parseInt(strTpComVatAmt);
            
        if(intTpTotAmt <= 0 )
        {
            ht.put("RESULT_CD", "OF80");
            ht.put("RESULT_MSG", "결제금액이 잘못되었습니다.");
            return ht;                   
        }
            
        if( intTpTotAmt < (intTpComFreeAmt+intTpComVatAmt) )
        {
            ht.put("RESULT_CD", "OF80");
            ht.put("RESULT_MSG", "결제금액이 잘못되었습니다.");
            return ht;                 
        }        
        
        
       //카드빈에 발급사가 있는 경우
       List ls_TermInfo = null;
       appFormBean.setPay_mtd_seq(onfftidInfo.getPay_mtd_seq());
       appFormBean.setApp_iss_cd("000");
       ls_TermInfo =  appDAO.GetTerminalInfo(appFormBean);
        
       logger.trace("----------------------- CardIssCd not exist!! ls_TermInfo.size() : "+ ls_TermInfo.size() +" ----------------------------------");
            
       AppTerminalInfoVo appTidObj = (AppTerminalInfoVo) ls_TermInfo.get(0);
       logger.trace("-- appTidObj.toString() : "+ appTidObj.toString() +" --");
       //결제용 Terminal 정보 셋팅
       ht.put("appTidObj", appTidObj);               
       
       ht.put("RESULT_CD", "0000");
        
        return ht;
    }

    @Override
    public Hashtable appKiccCashReqAction(AppFormBean appFormBean, AppOnffTidInfoVo appOnffTidInfoVo, AppTerminalInfoVo appTerminalInfoVo) throws Exception {
        Hashtable ht = new Hashtable();
        
        KiccPgVo reqpgobj = appFormBean.getReqKiccPgVo();
        KiccPgVo respgobj = new KiccPgVo();
        
        Date today=new Date();
        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmss");       
        String strCurYear = formater.format(today);

        String g_cert_file  = reqpgobj.getKiccpg_cert_file();
	String g_log_dir    = reqpgobj.getKiccpg_g_log_dir();
	int g_log_level  = 1;
        
	// ============================================================================== /
	// =   02. 쇼핑몰 지불 정보 설정                                                = /
	// = -------------------------------------------------------------------------- = /		
        String g_gw_url    = reqpgobj.getKiccpg_g_gw_url();
        String g_gw_port   = reqpgobj.getKiccpg_g_gw_port();
        
	String g_mall_id   = appTerminalInfoVo.getTerminal_no();             // 리얼 반영시 KICC에 발급된 mall_id 사용
        String g_mall_name = appOnffTidInfoVo.getComp_nm();    
        
        if(g_mall_id == null || g_mall_id.equals(""))
        {
            throw new Exception("결제용TID가 없습니다.");
        }
        
       //가맹점주문번호를 생성한다.
       String strOrder_seq = appDAO.GetOrderSeq();
       reqpgobj.setOrder_no(strOrder_seq);
        
	logger.trace("------------------------------appKiccCashReqAction--------------------------------------");
        logger.trace("onffmerchno : " + appOnffTidInfoVo.getOnffmerch_no());
        logger.trace("onfftid : " + appOnffTidInfoVo.getOnfftid());
        logger.trace("Pay_mtd_seq : " + appOnffTidInfoVo.getPay_mtd_seq());        
        logger.trace("g_cert_file : " + g_cert_file);
        logger.trace("g_log_dir : " + g_log_dir);
        logger.trace("g_gw_url : " + g_gw_url);
        logger.trace("g_gw_port : " + g_gw_port);
        logger.trace("g_mall_id : " + g_mall_id);
        logger.trace("g_mall_name : " + g_mall_name);
        logger.trace("reqpgobj.getUser_nm() : " + reqpgobj.getUser_nm());
        logger.trace("reqpgobj.getProduct_nm() : " + reqpgobj.getProduct_nm());
        
        logger.trace("reqpgobj.getCom_free_amt() : " + reqpgobj.getCom_free_amt());
        logger.trace("reqpgobj.getCom_tax_amt() : " + reqpgobj.getCom_tax_amt());
        logger.trace("reqpgobj.getCom_vat_amt() : " + reqpgobj.getCom_vat_amt());
        
        logger.trace("getClient_ip val : " + reqpgobj.getClient_ip());
        logger.trace("getTot_amt val : " + reqpgobj.getTot_amt());
        logger.trace("getCom_free_amt val : " + reqpgobj.getCom_free_amt() );
        logger.trace("getCom_vat_amt val : " + reqpgobj.getCom_vat_amt() );
            
            
        logger.trace("getSub_mall_yn val : " + reqpgobj.getSub_mall_yn());
        logger.trace("getSub_mall_buss val : " + reqpgobj.getSub_mall_buss());        
        logger.trace("--------------------------------------------------------------------");        
        
        //수수료정보를 가져온다.
        appFormBean.setPay_mtd_seq(appOnffTidInfoVo.getPay_mtd_seq());
        List ls_onfftidcmsinfo = appDAO.GetOnffTidCommission(appFormBean);
        AppOnffTidInfoVo onfftidcmsinfo = (AppOnffTidInfoVo)ls_onfftidcmsinfo.get(0);
        
	logger.trace("------------------------------appKiccReqAction--------------------------------------");   
        logger.trace("getOnfftid_cms_seq : " + onfftidcmsinfo.getOnfftid_cms_seq());        
        logger.trace("getOnfftid : " + onfftidcmsinfo.getOnfftid());      
        logger.trace("getCommission : " + onfftidcmsinfo.getCommission());        
        logger.trace("getStart_dt : " + onfftidcmsinfo.getStart_dt());        
        logger.trace("getEnd_dt : " + onfftidcmsinfo.getEnd_dt());        
        logger.trace("--------------------------------------------------------------------");  
        
        /* -------------------------------------------------------------------------- */
        /* ::: EasyPayClient 인스턴스 생성 [변경불가 !!].                             */
        /* -------------------------------------------------------------------------- */
        EasyPayClient easyPayClient = new EasyPayClient();
        easyPayClient.easypay_setenv_init( g_gw_url, g_gw_port, g_cert_file, g_log_dir );
        easyPayClient.easypay_reqdata_init();
        
        String ISSUE = reqpgobj.getISSUE();
        String CANCL = reqpgobj.getCANCL();
        String tr_cd = reqpgobj.getTr_cd();
        String req_cno = reqpgobj.getReq_cno();
        String pay_type = reqpgobj.getPay_type();
        String req_type = reqpgobj.getReq_type();
        String cert_type = reqpgobj.getCert_type();

            /* -------------------------------------------------------------------------- */
            /* ::: 결제 결과                                                              */
            /* -------------------------------------------------------------------------- */
        String res_cd         = "";
        String res_msg        = "";
        
        String r_cno             = "";	//PG거래번호 
        String r_amount          = "";	//총 결제금액
        String r_auth_no         = "";	//승인번호
        String r_tran_date       = "";	//승인일시
        String r_pnt_auth_no     = "";	//포인트승인번호
        String r_pnt_tran_date   = "";	//포인트승인일시
        String r_cpon_auth_no    = "";	//쿠폰승인번호
        String r_cpon_tran_date  = "";	//쿠폰승인일시
        String r_card_no         = "";	//카드번호
        String r_issuer_cd       = "";	//발급사코드
        String r_issuer_nm       = "";	//발급사명
        String r_acquirer_cd     = "";	//매입사코드
        String r_acquirer_nm     = "";	//매입사명
        String r_install_period  = "";	//할부개월
        String r_noint           = "";	//무이자여부
        String r_bank_cd         = "";	//은행코드
        String r_bank_nm         = "";	//은행명
        String r_account_no      = "";	//계좌번호
        String r_deposit_nm      = "";	//입금자명
        String r_expire_date     = "";	//계좌사용만료일
        String r_cash_res_cd     = "";	//현금영수증 결과코드
        String r_cash_res_msg    = "";	//현금영수증 결과메세지
        String r_cash_auth_no    = "";	//현금영수증 승인번호
        String r_cash_tran_date  = "";	//현금영수증 승인일시
        String r_auth_id         = "";	//PhoneID
        String r_billid          = "";	//인증번호
        String r_mobile_no       = "";	//휴대폰번호
        String r_ars_no          = "";	//전화번호
        String r_cp_cd           = "";	//포인트사/쿠폰사
        String r_used_pnt        = "";	//사용포인트
        String r_remain_pnt      = "";	//잔여한도
        String r_pay_pnt         = "";	//할인/발생포인트
        String r_accrue_pnt      = "";	//누적포인트
        String r_remain_cpon     = "";	//쿠폰잔액
        String r_used_cpon       = "";	//쿠폰 사용금액
        String r_mall_nm         = "";	//제휴사명칭
        String r_escrow_yn       = "";	//에스크로 사용유무
        String r_complex_yn      = "";	//복합결제 유무
        String r_canc_acq_date   = "";	//매입취소일시
        String r_canc_date       = "";	//취소일시
        String r_refund_date     = "";	//환불예정일시
        
        /* -------------------------------------------------------------------------- */
        /* ::: 승인요청 전문 설정                                                     */
        /* -------------------------------------------------------------------------- */
        if( ISSUE.equals(req_type) )
        {
    // 승인요청 전문 설정
            // 결제 주문 정보 DATA
            int easypay_order_data_item;
            easypay_order_data_item = easyPayClient.easypay_item( "cash_data" );

            easyPayClient.easypay_deli_us( easypay_order_data_item, "order_no"		,reqpgobj.getOrder_no()		);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_id"		,reqpgobj.getUser_id()		);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_nm"		,reqpgobj.getUser_nm()		);
            
            //자진발급인지 구분
            String strTpCash_issue_type = reqpgobj.getCash_issue_type();
            if(strTpCash_issue_type.equals("03"))
            {
                easyPayClient.easypay_deli_us( easypay_order_data_item, "issue_type"    , "01"   );
                easyPayClient.easypay_deli_us( easypay_order_data_item, "auth_type"     , reqpgobj.getCash_auth_type()    );
                easyPayClient.easypay_deli_us( easypay_order_data_item, "auth_value"    , reqpgobj.getCash_auth_value()  );	
            }
            //지출증빙, 소득공제
            else
            {
                easyPayClient.easypay_deli_us( easypay_order_data_item, "issue_type"    , reqpgobj.getCash_issue_type()   );
                easyPayClient.easypay_deli_us( easypay_order_data_item, "auth_type"     , reqpgobj.getCash_auth_type()    );
                easyPayClient.easypay_deli_us( easypay_order_data_item, "auth_value"    , reqpgobj.getCash_auth_value()  );	                
            }
            
            
            
            easyPayClient.easypay_deli_us( easypay_order_data_item, "sub_mall_yn"   , reqpgobj.getSub_mall_yn()  );	
            easyPayClient.easypay_deli_us( easypay_order_data_item, "sub_mall_buss", reqpgobj.getSub_mall_buss()   );	
            easyPayClient.easypay_deli_us( easypay_order_data_item, "tot_amt"      , reqpgobj.getTot_amt()      );	
            easyPayClient.easypay_deli_us( easypay_order_data_item, "service_amt"  , reqpgobj.getCom_free_amt()  );	
            easyPayClient.easypay_deli_us( easypay_order_data_item, "vat"          , reqpgobj.getCom_vat_amt()        );            
            
            easyPayClient.easypay_deli_us( easypay_order_data_item, "memb_user_no"	,reqpgobj.getMemb_user_no() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_mail"		,reqpgobj.getUser_mail() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_phone1"	,reqpgobj.getUser_phone1() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_phone2"	,reqpgobj.getUser_phone2() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_addr"		,reqpgobj.getUser_addr() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "product_type"	,reqpgobj.getProduct_type() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "product_nm"	,reqpgobj.getProduct_nm() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "product_amt"	,reqpgobj.getProduct_amt() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_define1"	,reqpgobj.getUser_define1() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_define2"	,reqpgobj.getUser_define2() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_define3"	,reqpgobj.getUser_define3() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_define4"	,reqpgobj.getUser_define4() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_define5"	,reqpgobj.getUser_define5() 	);
            easyPayClient.easypay_deli_us( easypay_order_data_item, "user_define6"	,reqpgobj.getUser_define6() 	);

        /* -------------------------------------------------------------------------- */
        /* ::: 변경관리 요청                                                          */
        /* -------------------------------------------------------------------------- */
        }else if( CANCL.equals( req_type ) ) {

            int easypay_mgr_data_item;
            easypay_mgr_data_item = easyPayClient.easypay_item( "mgr_data" );
            
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_txtype"		,  reqpgobj.getMgr_txtype() 	);
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "org_cno"			,  reqpgobj.getOrg_cno() 		);
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "req_ip"			,  reqpgobj.getClient_ip()		);
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "req_id"			,  reqpgobj.getReq_id()        );
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_msg"			,  reqpgobj.getMgr_msg()		);
        }

        /* -------------------------------------------------------------------------- */
        /* ::: 실행                                                                   */
        /* -------------------------------------------------------------------------- */
        if ( tr_cd.length() > 0 ) {
            easyPayClient.easypay_run( g_mall_id, tr_cd, reqpgobj.getOrder_no() );

            res_cd = easyPayClient.res_cd;
            res_msg = easyPayClient.res_msg;

        }
        else {
            res_cd  = "M114";
            res_msg = "연동 오류|tr_cd값이 설정되지 않았습니다.";
        }
        
	logger.trace("--------------------------------------------------------------------");
        logger.trace("res_cd : " + res_cd);
        logger.trace("res_msg : " + res_msg);
        logger.trace("--------------------------------------------------------------------");     
        
        /* -------------------------------------------------------------------------- */
        /* ::: 결과 처리                                                              */
        /* -------------------------------------------------------------------------- */
        r_cno             =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "cno"             ));    //PG거래번호 
        r_amount          =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "amount"          ));    //총 결제금액
        r_auth_no         =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "auth_no"         ));    //승인번호
        r_tran_date       =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "tran_date"       ));    //승인일시
        r_pnt_auth_no     =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "pnt_auth_no"     ));    //포인트승인번호
        r_pnt_tran_date   =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "pnt_tran_date"   ));    //포인트승인일시
        r_cpon_auth_no    =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "cpon_auth_no"    ));    //쿠폰승인번호
        r_cpon_tran_date  =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "cpon_tran_date"  ));    //쿠폰승인일시
        r_card_no         =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "card_no"         ));    //카드번호
        r_issuer_cd       =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "issuer_cd"       ));    //발급사코드
        r_issuer_nm       =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "issuer_nm"       ));    //발급사명
        r_acquirer_cd     =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "acquirer_cd"     ));    //매입사코드
        r_acquirer_nm     =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "acquirer_nm"     ));    //매입사명
        r_install_period  =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "install_period"  ));    //할부개월
        r_noint           =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "noint"           ));    //무이자여부
        r_bank_cd         =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "bank_cd"         ));    //은행코드
        r_bank_nm         =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "bank_nm"         ));    //은행명
        r_account_no      =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "account_no"      ));    //계좌번호
        r_deposit_nm      =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "deposit_nm"      ));    //입금자명
        r_expire_date     =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "expire_date"     ));    //계좌사용만료일
        r_cash_res_cd     =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "cash_res_cd"     ));    //현금영수증 결과코드
        r_cash_res_msg    =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "cash_res_msg"    ));    //현금영수증 결과메세지
        r_cash_auth_no    =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "cash_auth_no"    ));    //현금영수증 승인번호
        r_cash_tran_date  =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "cash_tran_date"  ));    //현금영수증 승인일시
        r_auth_id         =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "auth_id"         ));    //PhoneID
        r_billid          =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "billid"          ));    //인증번호
        r_mobile_no       =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "mobile_no"       ));    //휴대폰번호
        r_ars_no          =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "ars_no"          ));    //전화번호
        r_cp_cd           =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "cp_cd"           ));    //포인트사/쿠폰사
        r_used_pnt        =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "used_pnt"        ));    //사용포인트
        r_remain_pnt      =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "remain_pnt"      ));    //잔여한도
        r_pay_pnt         =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "pay_pnt"         ));    //할인/발생포인트
        r_accrue_pnt      =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "accrue_pnt"      ));    //누적포인트
        r_remain_cpon     =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "remain_cpon"     ));    //쿠폰잔액
        r_used_cpon       =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "used_cpon"       ));    //쿠폰 사용금액
        r_mall_nm         =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "mall_nm"         ));    //제휴사명칭
        r_escrow_yn       =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "escrow_yn"       ));    //에스크로 사용유무
        r_complex_yn      =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "complex_yn"      ));    //복합결제 유무
        r_canc_acq_date   =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "canc_acq_date"   ));    //매입취소일시
        r_canc_date       =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "canc_date"       ));    //취소일시
        r_refund_date     =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "refund_date"     ));    //환불예정일시
        
        logger.trace("--------------------------------------------------------------------");        
        logger.trace("r_cno            "+       r_cno             );// PG거래번호
        logger.trace("r_amount         "+       r_amount          );//총 결제금액
        logger.trace("r_auth_no        "+       r_auth_no         );//승인번호
        logger.trace("r_tran_date      "+       r_tran_date       );//승인일시
        logger.trace("r_pnt_auth_no    "+       r_pnt_auth_no     );//포인트승인번호
        logger.trace("r_pnt_tran_date  "+       r_pnt_tran_date   );//포인트승인일시
        logger.trace("r_cpon_auth_no   "+       r_cpon_auth_no    );//쿠폰승인번호
        logger.trace("r_cpon_tran_date "+       r_cpon_tran_date  );//쿠폰승인일시
        logger.trace("r_card_no        "+       r_card_no         );//카드번호
        logger.trace("r_issuer_cd      "+       r_issuer_cd       );//발급사코드
        logger.trace("r_issuer_nm      "+       r_issuer_nm       );//발급사명
        logger.trace("r_acquirer_cd    "+       r_acquirer_cd     );//매입사코드
        logger.trace("r_acquirer_nm    "+       r_acquirer_nm     );//매입사명
        logger.trace("r_install_period "+       r_install_period  );//할부개월
        logger.trace("r_noint          "+       r_noint           );//무이자여부
        logger.trace("r_bank_cd        "+       r_bank_cd         );//은행코드
        logger.trace("r_bank_nm        "+       r_bank_nm         );//은행명
        logger.trace("r_account_no     "+       r_account_no      );//계좌번호
        logger.trace("r_deposit_nm     "+       r_deposit_nm      );//입금자명
        logger.trace("r_expire_date    "+       r_expire_date     );//계좌사용만료일
        logger.trace("r_cash_res_cd    "+       r_cash_res_cd     );//현금영수증 결과코드
        logger.trace("r_cash_res_msg   "+       r_cash_res_msg    );//현금영수증 결과메세지
        logger.trace("r_cash_auth_no   "+       r_cash_auth_no    );//현금영수증 승인번호
        logger.trace("r_cash_tran_date "+       r_cash_tran_date  );//현금영수증 승인일시
        logger.trace("r_auth_id        "+       r_auth_id         );//PhoneID
        logger.trace("r_billid         "+       r_billid          );//인증번호
        logger.trace("r_mobile_no      "+       r_mobile_no       );//휴대폰번호
        logger.trace("r_ars_no         "+       r_ars_no          );//전화번호
        logger.trace("r_cp_cd          "+       r_cp_cd           );//포인트사/쿠폰사
        logger.trace("r_used_pnt       "+       r_used_pnt        );//사용포인트
        logger.trace("r_remain_pnt     "+       r_remain_pnt      );//잔여한도
        logger.trace("r_pay_pnt        "+       r_pay_pnt         );//할인/발생포인트
        logger.trace("r_accrue_pnt     "+       r_accrue_pnt      );//누적포인트
        logger.trace("r_remain_cpon    "+       r_remain_cpon     );//쿠폰잔액
        logger.trace("r_used_cpon      "+       r_used_cpon       );//쿠폰 사용금액
        logger.trace("r_mall_nm        "+       r_mall_nm         );//제휴사명칭
        logger.trace("r_escrow_yn      "+       r_escrow_yn       );//에스크로 사용유무
        logger.trace("r_complex_yn     "+       r_complex_yn      );//복합결제 유무
        logger.trace("r_canc_date      "+       r_canc_date       );//취소일시
        logger.trace("r_refund_date    "+       r_refund_date     );//환불예정일시
        logger.trace("--------------------------------------------------------------------");
    
        respgobj.setR_cno             (r_cno            );
        respgobj.setR_amount          (r_amount         );
        respgobj.setR_order_no        (reqpgobj.getOrder_no()	       );
        respgobj.setR_auth_no         (r_auth_no        );
        respgobj.setR_tran_date       (r_tran_date      );
        respgobj.setR_pnt_auth_no     (r_pnt_auth_no    );
        respgobj.setR_pnt_tran_date   (r_pnt_tran_date  );
        respgobj.setR_cpon_auth_no    (r_cpon_auth_no   );
        respgobj.setR_cpon_tran_date  (r_cpon_tran_date );
        respgobj.setR_card_no         (r_card_no        );
        respgobj.setR_issuer_cd       (r_issuer_cd      );
        respgobj.setR_issuer_nm       (r_issuer_nm      );
        respgobj.setR_acquirer_cd     (r_acquirer_cd    );
        respgobj.setR_acquirer_nm     (r_acquirer_nm    );
        respgobj.setR_install_period  (r_install_period );
        respgobj.setR_noint           (r_noint          );
        respgobj.setR_bank_cd         (r_bank_cd        );
        respgobj.setR_bank_nm         (r_bank_nm        );
        respgobj.setR_account_no      (r_account_no     );
        respgobj.setR_deposit_nm      (r_deposit_nm     );
        respgobj.setR_expire_date     (r_expire_date    );
        respgobj.setR_cash_res_cd     (r_cash_res_cd    );
        respgobj.setR_cash_res_msg    (r_cash_res_msg   );
        respgobj.setR_cash_auth_no    (r_cash_auth_no   );
        respgobj.setR_cash_tran_date  (r_cash_tran_date );
        respgobj.setR_auth_id         (r_auth_id        );
        respgobj.setR_billid          (r_billid         );
        respgobj.setR_mobile_no       (r_mobile_no      );
        respgobj.setR_ars_no          (r_ars_no         );
        respgobj.setR_cp_cd           (r_cp_cd          );
        respgobj.setR_used_pnt        (r_used_pnt       );
        respgobj.setR_remain_pnt      (r_remain_pnt     );
        respgobj.setR_pay_pnt         (r_pay_pnt        );
        respgobj.setR_accrue_pnt      (r_accrue_pnt     );
        respgobj.setR_remain_cpon     (r_remain_cpon    );
        respgobj.setR_used_cpon       (r_used_cpon      );
        respgobj.setR_mall_nm         (r_mall_nm        );
        respgobj.setR_escrow_yn       (r_escrow_yn      );
        respgobj.setR_complex_yn      (r_complex_yn     );
        
        respgobj.setR_canc_date       (r_canc_date      );
        respgobj.setR_refund_date     (r_refund_date    );
        
        //가맹점 DB처리
        //저장정보를 만든다.
    
        InsertCardPgTranBean insobj = new InsertCardPgTranBean();
        //승인 결가에 따라 저장을 위한 테이블명을 만든다.
        //성공관련 data 셋팅
        if (res_cd.equals("0000"))
        {
            insobj.setInstablenm("TB_TRAN_CASHPG_"+r_tran_date.substring(2, 6));
            insobj.setApp_dt(r_tran_date.substring(0, 8));
            insobj.setApp_tm(r_tran_date.substring(8, 14));
            insobj.setResult_status("00");
            insobj.setAcc_chk_flag("0");
            insobj.setTot_amt(r_amount);//결제총금액
                        
        }
        else
        {
            insobj.setInstablenm("TB_TRAN_CASHPG_"+strCurYear.substring(2, 6));
            insobj.setResult_status("99");                        
            insobj.setAcc_chk_flag("3");
            insobj.setTot_amt(reqpgobj.getTot_amt());//결제총금액
        }
        
        String strTpTranseq = appDAO.GetTranSeq();
        insobj.setTran_seq(strTpTranseq);// 거래번호 셋팅
        //카드결제 시
             
        insobj.setMassagetype("10");//메세지타입
        insobj.setCard_num(reqpgobj.getCash_auth_value());//인증값
        insobj.setApp_no(r_auth_no);//승인번호

        insobj.setPg_seq(r_cno);//pg거래고유번호
        insobj.setPay_chn_cate(appOnffTidInfoVo.getPay_chn_cate());
        insobj.setTid(g_mall_id);
        
        //kicc pg의 경우 결제ID를 가맹점 번호에 넣는다.
        insobj.setMerch_no(g_mall_id);
        
        insobj.setOnffmerch_no(appOnffTidInfoVo.getOnffmerch_no());        
        insobj.setOnfftid(appOnffTidInfoVo.getOnfftid());
        insobj.setWcc(reqpgobj.getCash_issue_type());
        insobj.setCard_cate(reqpgobj.getCash_auth_type());
        insobj.setOrder_seq(reqpgobj.getOrder_no());
        insobj.setTran_dt(strCurYear.substring(0, 8));
        insobj.setTran_tm(strCurYear.substring(8, 14));
        insobj.setTran_cate("10");
        
        insobj.setTax_amt(reqpgobj.getCom_vat_amt());//부가세금액
        insobj.setSvc_amt(reqpgobj.getCom_free_amt ());//비과세승인금액(서비스금액)
        
        
        insobj.setResult_cd(res_cd);
        insobj.setResult_msg(res_msg);
        
        insobj.setTran_status("00");//승인 :00 ,10:당일취소 , 20:매입취소
        insobj.setTran_step("00");
        insobj.setTrad_chk_flag("0");
        
        insobj.setClient_ip(reqpgobj.getClient_ip());
        insobj.setUser_type(reqpgobj.getUser_type());
        insobj.setMemb_user_no(reqpgobj.getMemb_user_no());
        insobj.setUser_id(reqpgobj.getUser_id());
        insobj.setUser_nm(reqpgobj.getUser_nm());
        insobj.setUser_mail(reqpgobj.getUser_mail());
        insobj.setUser_phone1(reqpgobj.getUser_phone1());
        insobj.setUser_phone2(reqpgobj.getUser_phone2());
        insobj.setUser_addr(reqpgobj.getUser_addr());
        insobj.setProduct_type(reqpgobj.getProduct_type());
        insobj.setProduct_nm(reqpgobj.getProduct_nm()); 
        insobj.setIns_user(appFormBean.getUser_seq());
        insobj.setMod_user(appFormBean.getUser_seq());
        
        //가맹점 DB 처리
        Integer int_dbresult = appDAO.InsertCashPgTran(insobj);
        String strTpResult = int_dbresult.toString();
        
        logger.trace("------------------Cash App insert start------------------");
        logger.trace("cash App insert result : " + strTpResult);
        logger.trace("------------------Cash App insert End ------------------");
        
        ht.put("RESULT_CD", res_cd);
        ht.put("RESULT_MSG", res_msg); 
        ht.put("TRAN_SEQ", strTpTranseq); 
        
        return ht;
    }
    

    @Override
    public Hashtable appKcpCashReqAction(AppFormBean appFormBean, AppOnffTidInfoVo appOnffTidInfoVo, AppTerminalInfoVo appTerminalInfoVo) throws Exception {
        Hashtable ht = new Hashtable();
        
        KiccPgVo reqRefobj =  appFormBean.getReqKiccPgVo();
        KcpPgVo reqpgobj = appFormBean.getReqKcpPgVo();
        KcpPgVo respgobj = new KcpPgVo();
        
        Date today=new Date();
        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmss");       
        String strCurYear = formater.format(today);
        
        reqpgobj.setTrad_time(strCurYear);

        int int_g_log_level  = Integer.parseInt(reqpgobj.getG_conf_log_level());
        
	String g_conf_site_cd   = appTerminalInfoVo.getTerminal_no();             // 리얼 반영시 KICC에 발급된 mall_id 사용
        String g_conf_site_key = appTerminalInfoVo.getTerminal_pwd();             // 리얼 반영시 KICC에 발급된 mall_id 사용
        
        if(g_conf_site_cd == null || g_conf_site_cd.equals(""))
        {
            throw new Exception("결제용TID가 없습니다.");
        }
        
        String tx_cd = reqpgobj.getTx_cd();
        
        //가맹점주문번호를 생성한다.
        String strOrder_seq = appDAO.GetOrderSeq();
        reqpgobj.setOrdr_idxx(strOrder_seq);
        
        //사업자정보를 가져온다.
        CompanyFormBean companyFormBean = new  CompanyFormBean();
        companyFormBean.setBiz_no(reqpgobj.getCorp_tax_no());
        List ls_onffCompInfo = compDAO.companyMasterInfo(companyFormBean);
        
        Tb_Company onffCompInfo= (Tb_Company)ls_onffCompInfo.get(0);
        
        reqpgobj.setCorp_nm(Util.nullToSpaceString(onffCompInfo.getComp_nm()));//사업자명
        reqpgobj.setCorp_owner_nm(Util.nullToSpaceString(onffCompInfo.getComp_ceo_nm()));//대표자명
        reqpgobj.setCorp_addr(Util.nullToSpaceString(onffCompInfo.getAddr_1())+" " + Util.nullToSpaceString(onffCompInfo.getAddr_2()));//주소
        reqpgobj.setCorp_telno(Util.nullToSpaceString(onffCompInfo.getComp_tel1()));//전호번호
        
        //사업자의 과세 기준에 따라 과세면세구분을 셋팅한다.
        String strtpChkTaxflag = Util.nullToString(onffCompInfo.getTax_flag());
        
        //비과세("01")
        if(strtpChkTaxflag.equals("01"))
        {
            reqpgobj.setCorp_tax_type("TG02");//비과세
        }
        //과세("00")
        else
        {
            reqpgobj.setCorp_tax_type("TG01");//비과세
        }
        
	logger.trace("------------------------------appKcpCashReqAction--------------------------------------");
        logger.trace("getOnffmerch_no : " + appOnffTidInfoVo.getOnffmerch_no());
        logger.trace("getOnfftid : " + appOnffTidInfoVo.getOnfftid());
        logger.trace("getPay_mtd_seq : " + appOnffTidInfoVo.getPay_mtd_seq());        
        logger.trace("getG_conf_log_dir : " + reqpgobj.getG_conf_log_dir());
        logger.trace("getG_conf_gw_url : " + reqpgobj.getG_conf_gw_url());
        logger.trace("getG_conf_gw_port : " + reqpgobj.getG_conf_gw_port());
        logger.trace("g_conf_site_cd : " + g_conf_site_cd);
        logger.trace("g_conf_site_key : " + g_conf_site_key);
        
        logger.trace("getTr_code val : " + reqpgobj.getTr_code());
        logger.trace("getId_info val : " + reqpgobj.getId_info());
        
        logger.trace("reqpgobj.getTrad_time() : " + reqpgobj.getTrad_time());
        logger.trace("reqpgobj.getBuyr_name() : " + reqpgobj.getBuyr_name());
        logger.trace("reqpgobj.getBuyr_mail() : " + reqpgobj.getBuyr_mail());
        logger.trace("reqpgobj.getBuyr_tel1() : " + reqpgobj.getBuyr_tel1());
        logger.trace("reqpgobj.getBuyr_tel2() : " + reqpgobj.getBuyr_tel2());
        logger.trace("reqpgobj.getGood_name() : " + reqpgobj.getGood_name());
        
        logger.trace("reqpgobj.getAmt_svc() : " + reqpgobj.getAmt_svc());
        logger.trace("reqpgobj.getAmt_sup() : " + reqpgobj.getAmt_sup());
        logger.trace("reqpgobj.getAmt_tax() : " + reqpgobj.getAmt_tax());
        
        logger.trace("getClient_ip val : " + reqpgobj.getCust_ip());
        logger.trace("getTot_amt val : " + reqpgobj.getAmt_tot());           
            
        logger.trace("getCorp_type val : " + reqpgobj.getCorp_type());
        logger.trace("getCorp_tax_type val : " + reqpgobj.getCorp_tax_type()); 
        logger.trace("getCorp_tax_no val : " + reqpgobj.getCorp_tax_no()); 
        logger.trace("getCorp_nm val : " + reqpgobj.getCorp_nm()); 
        logger.trace("getCorp_owner_nm val : " + reqpgobj.getCorp_owner_nm()); 
        logger.trace("getCorp_addr val : " + reqpgobj.getCorp_addr()); 
        logger.trace("getCorp_telno val : " + reqpgobj.getCorp_telno()); 
        
        
        logger.trace("--------------------------------------------------------------------");        
        
        //수수료정보를 가져온다.
        appFormBean.setPay_mtd_seq(appOnffTidInfoVo.getPay_mtd_seq());
        List ls_onfftidcmsinfo = appDAO.GetOnffTidCommission(appFormBean);
        AppOnffTidInfoVo onfftidcmsinfo = (AppOnffTidInfoVo)ls_onfftidcmsinfo.get(0);
        
	logger.trace("------------------------------appKcpReqAction--------------------------------------");   
        logger.trace("getOnfftid_cms_seq : " + onfftidcmsinfo.getOnfftid_cms_seq());        
        logger.trace("getOnfftid : " + onfftidcmsinfo.getOnfftid());      
        logger.trace("getCommission : " + onfftidcmsinfo.getCommission());        
        logger.trace("getStart_dt : " + onfftidcmsinfo.getStart_dt());        
        logger.trace("getEnd_dt : " + onfftidcmsinfo.getEnd_dt());        
        logger.trace("--------------------------------------------------------------------");  
        
        //인스터스생성 초기화
        C_PP_CLI c_PayPlus = new C_PP_CLI();

        //c_PayPlus.mf_init( g_conf_home_dir, g_conf_gw_url, g_conf_gw_port, g_conf_key_dir, g_conf_log_dir, g_conf_tx_mode );
        int inttptxmod = Integer.parseInt(reqpgobj.getG_conf_tx_mode());
        //window
        //c_PayPlus.mf_init( reqpgobj.getG_conf_home_dir(), reqpgobj.getG_conf_gw_url(), reqpgobj.getG_conf_gw_port(), reqpgobj.getG_conf_key_dir(), reqpgobj.getG_conf_log_dir(), inttptxmod );
        //linux
        c_PayPlus.mf_init( reqpgobj.getG_conf_home_dir(), reqpgobj.getG_conf_gw_url(), reqpgobj.getG_conf_gw_port(), inttptxmod );
            
        c_PayPlus.mf_init_set();

        /* -------------------------------------------------------------------------- */
        /* ::: 결제 결과                                                              */
        /* -------------------------------------------------------------------------- */
        
        String res_cd         = "";
        String res_msg        = "";
        
        String r_cno             = "";	//PG거래번호 
        String r_amount          = "";	//총 결제금액
        String r_auth_no         = "";	//승인번호
        String r_tran_date       = "";	//승인일시
        String r_card_no         = "";	//카드번호
        String r_issuer_cd       = "";	//발급사코드
        String r_issuer_nm       = "";	//발급사명
        String r_acquirer_cd     = "";	//매입사코드
        String r_acquirer_nm     = "";	//매입사명
        String r_install_period  = "";	//할부개월
        String r_noint           = "";	//무이자여부
        String r_expire_date     = "";	//계좌사용만료일
        String r_cash_res_cd     = "";	//현금영수증 결과코드
        String r_cash_res_msg    = "";	//현금영수증 결과메세지
        String r_cash_auth_no    = "";	//현금영수증 승인번호
        String r_cash_tran_date  = "";	//현금영수증 승인일시
        String r_canc_acq_date   = "";	//매입취소일시
        String r_canc_date       = "";	//취소일시
        String r_refund_date     = "";	//환불예정일시
        
        String r_reg_stat = ""; //등록상태코드
        
        /* -------------------------------------------------------------------------- */
        /* ::: 승인요청 전문 설정                                                     */
        /* -------------------------------------------------------------------------- */
        if("pay".equals(reqpgobj.getReq_tx())) 
        {
            
            int rcpt_data_set ;
            int ordr_data_set ;
            int corp_data_set ;

            rcpt_data_set   = c_PayPlus.mf_add_set( "rcpt_data" ) ;
            ordr_data_set   = c_PayPlus.mf_add_set( "ordr_data" ) ;
            corp_data_set   = c_PayPlus.mf_add_set( "corp_data" ) ;

            // 현금영수증 정보
            c_PayPlus.mf_set_us( rcpt_data_set, "user_type", Util.nullToString(reqpgobj.getG_conf_user_type())) ;
            c_PayPlus.mf_set_us( rcpt_data_set, "trad_time", Util.nullToString(reqpgobj.getTrad_time())) ;
            c_PayPlus.mf_set_us( rcpt_data_set, "tr_code"  , Util.nullToString(reqpgobj.getTr_code())   ) ;
            c_PayPlus.mf_set_us( rcpt_data_set, "id_info"  , Util.nullToString(reqpgobj.getId_info())   ) ;
            c_PayPlus.mf_set_us( rcpt_data_set, "amt_tot"  , Util.nullToString(reqpgobj.getAmt_tot())   ) ;
            c_PayPlus.mf_set_us( rcpt_data_set, "amt_sup"  , Util.nullToString(reqpgobj.getAmt_sup())   ) ;
            c_PayPlus.mf_set_us( rcpt_data_set, "amt_svc"  , Util.nullToString(reqpgobj.getAmt_svc())   ) ;
            c_PayPlus.mf_set_us( rcpt_data_set, "amt_tax"  , Util.nullToString(reqpgobj.getAmt_tax())   ) ;
            //c_PayPlus.mf_set_us( rcpt_data_set, "pay_type" , "PAXX"    ) ;
            c_PayPlus.mf_set_us( rcpt_data_set, "pay_type" , Util.nullToString(reqpgobj.getPay_type())       ) ;

            // 주문 정보
            c_PayPlus.mf_set_us( ordr_data_set, "ordr_idxx", Util.nullToString(reqpgobj.getOrdr_idxx()) ) ;
            c_PayPlus.mf_set_us( ordr_data_set, "good_name", Util.nullToString(reqpgobj.getGood_name()) ) ;
            c_PayPlus.mf_set_us( ordr_data_set, "buyr_name", Util.nullToString(reqpgobj.getBuyr_name()) ) ;
            c_PayPlus.mf_set_us( ordr_data_set, "buyr_tel1", Util.nullToString(reqpgobj.getBuyr_tel1()) ) ;
            c_PayPlus.mf_set_us( ordr_data_set, "buyr_mail", Util.nullToString(reqpgobj.getBuyr_mail()) ) ;
            c_PayPlus.mf_set_us( ordr_data_set, "comment"  , Util.nullToString(reqpgobj.getComment())   ) ;

            // 가맹점 정보
            c_PayPlus.mf_set_us( corp_data_set, "corp_type"      , Util.nullToString(reqpgobj.getCorp_type())       ) ;

            // 입점몰인 경우 판매상점 DATA 전문 생성
            if( "1".equals( Util.nullToString(reqpgobj.getCorp_type())  ) )
            {
                c_PayPlus.mf_set_us( corp_data_set, "corp_tax_type"   , Util.nullToString(reqpgobj.getCorp_tax_type())   ) ;
                c_PayPlus.mf_set_us( corp_data_set, "corp_tax_no"     , Util.nullToString(reqpgobj.getCorp_tax_no())     ) ;
                c_PayPlus.mf_set_us( corp_data_set, "corp_sell_tax_no", Util.nullToString(reqpgobj.getCorp_sell_tax_no())     ) ;
                c_PayPlus.mf_set_us( corp_data_set, "corp_nm"         , Util.nullToString(reqpgobj.getCorp_nm())         ) ;
                c_PayPlus.mf_set_us( corp_data_set, "corp_owner_nm"   , Util.nullToString(reqpgobj.getCorp_owner_nm())   ) ;
                c_PayPlus.mf_set_us( corp_data_set, "corp_addr"       , Util.nullToString(reqpgobj.getCorp_addr())       ) ;
                c_PayPlus.mf_set_us( corp_data_set, "corp_telno"      , Util.nullToString(reqpgobj.getCorp_telno())      ) ;
            }

            c_PayPlus.mf_add_rs( ordr_data_set , rcpt_data_set ) ;
            c_PayPlus.mf_add_rs( ordr_data_set , corp_data_set ) ;

        }
        /* -------------------------------------------------------------------------- */
        /* ::: 실행                                                                   */
        /* -------------------------------------------------------------------------- */
        if ( tx_cd.length() > 0 ) {
            c_PayPlus.mf_do_tx( g_conf_site_cd, "", tx_cd, reqpgobj.getCust_ip(), reqpgobj.getOrdr_idxx(), Util.getNullToZero(reqpgobj.getG_conf_log_level()), "1" ) ;
        }
        else {
            c_PayPlus.m_res_cd  = "9562" ;
            c_PayPlus.m_res_msg = "연동 오류" ;
        }
        
        res_cd  = c_PayPlus.m_res_cd ;
        res_msg = c_PayPlus.m_res_msg ;   
        
	logger.trace("--------------------------------------------------------------------");
        logger.trace("res_cd : " + res_cd);
        logger.trace("res_msg : " + res_msg);
        logger.trace("--------------------------------------------------------------------");     
        
        /* -------------------------------------------------------------------------- */
        /* ::: 결과 처리                                                              */
        /* -------------------------------------------------------------------------- */
        r_cno             =  Util.nullToSpaceString(c_PayPlus.mf_get_res( "cash_no"    ));    //PG거래번호 
        r_amount          =  Util.nullToSpaceString(reqpgobj.getAmt_tot());    //총 결제금액
        r_auth_no         =  Util.nullToSpaceString(c_PayPlus.mf_get_res( "receipt_no" ));    //승인번호
        r_tran_date       =  Util.nullToSpaceString(c_PayPlus.mf_get_res( "app_time"   ));    //승인일시
        r_reg_stat        =  Util.nullToSpaceString(c_PayPlus.mf_get_res( "reg_stat"   ));    //등록상태코드

        /*
        r_card_no         =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "card_no"         ));    //카드번호
        r_issuer_cd       =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "issuer_cd"       ));    //발급사코드
        r_issuer_nm       =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "issuer_nm"       ));    //발급사명
        r_acquirer_cd     =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "acquirer_cd"     ));    //매입사코드
        r_acquirer_nm     =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "acquirer_nm"     ));    //매입사명
        r_install_period  =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "install_period"  ));    //할부개월
        r_noint           =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "noint"           ));    //무이자여부
        r_expire_date     =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "expire_date"     ));    //계좌사용만료일
        */
        r_cash_res_cd     =  Util.nullToSpaceString(res_cd);    //현금영수증 결과코드
        r_cash_res_msg    =  Util.nullToSpaceString(res_msg);    //현금영수증 결과메세지
        r_cash_auth_no    =  Util.nullToSpaceString(c_PayPlus.mf_get_res( "receipt_no" ));    //현금영수증 승인번호
        r_cash_tran_date  =  Util.nullToSpaceString(c_PayPlus.mf_get_res( "app_time"   ));    //현금영수증 승인일시
        /*
        r_canc_acq_date   =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "canc_acq_date"   ));    //매입취소일시
        r_canc_date       =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "canc_date"       ));    //취소일시
        r_refund_date     =  Util.nullToSpaceString(easyPayClient.easypay_get_res( "refund_date"     ));    //환불예정일시
        */
        
        logger.trace("--------------------------------------------------------------------");        
        logger.trace("r_cno            "+       r_cno             );// PG거래번호
        logger.trace("r_amount         "+       r_amount          );//총 결제금액
        logger.trace("r_auth_no        "+       r_auth_no         );//승인번호
        logger.trace("r_tran_date      "+       r_tran_date       );//승인일시
        logger.trace("r_card_no        "+       r_card_no         );//카드번호
        logger.trace("r_issuer_cd      "+       r_issuer_cd       );//발급사코드
        logger.trace("r_issuer_nm      "+       r_issuer_nm       );//발급사명
        logger.trace("r_acquirer_cd    "+       r_acquirer_cd     );//매입사코드
        logger.trace("r_acquirer_nm    "+       r_acquirer_nm     );//매입사명
        logger.trace("r_install_period "+       r_install_period  );//할부개월
        logger.trace("r_noint          "+       r_noint           );//무이자여부
        logger.trace("r_expire_date    "+       r_expire_date     );//계좌사용만료일
        logger.trace("r_cash_res_cd    "+       r_cash_res_cd     );//현금영수증 결과코드
        logger.trace("r_cash_res_msg   "+       r_cash_res_msg    );//현금영수증 결과메세지
        logger.trace("r_cash_auth_no   "+       r_cash_auth_no    );//현금영수증 승인번호
        logger.trace("r_cash_tran_date "+       r_cash_tran_date  );//현금영수증 승인일시
        logger.trace("r_canc_date      "+       r_canc_date       );//취소일시
        logger.trace("r_refund_date    "+       r_refund_date     );//환불예정일시
        logger.trace("--------------------------------------------------------------------");
    
        /*
        respgobj.setR_cno             (r_cno            );
        respgobj.setR_amount          (r_amount         );
        respgobj.setR_order_no        (reqpgobj.getOrder_no()	       );
        respgobj.setR_auth_no         (r_auth_no        );
        respgobj.setR_tran_date       (r_tran_date      );
        respgobj.setR_cash_res_cd     (r_cash_res_cd    );
        respgobj.setR_cash_res_msg    (r_cash_res_msg   );
        respgobj.setR_cash_auth_no    (r_cash_auth_no   );
        respgobj.setR_cash_tran_date  (r_cash_tran_date );
        */
        
        
        //가맹점 DB처리
        //저장정보를 만든다.
    
        InsertCardPgTranBean insobj = new InsertCardPgTranBean();
        //승인 결가에 따라 저장을 위한 테이블명을 만든다.
        //성공관련 data 셋팅
        if (res_cd.equals("0000"))
        {
            insobj.setInstablenm("TB_TRAN_CASHPG_"+r_tran_date.substring(2, 6));
            insobj.setApp_dt(r_tran_date.substring(0, 8));
            insobj.setApp_tm(r_tran_date.substring(8, 14));
            insobj.setResult_status("00");
            insobj.setAcc_chk_flag("0");
            insobj.setTot_amt(reqpgobj.getAmt_tot());//결제총금액
                        
        }
        else
        {
            insobj.setInstablenm("TB_TRAN_CASHPG_"+strCurYear.substring(2, 6));
            insobj.setResult_status("99");                        
            insobj.setAcc_chk_flag("3");
            insobj.setTot_amt(reqpgobj.getAmt_tot());//결제총금액
        }
        
        String strTpTranseq = appDAO.GetTranSeq();
        insobj.setTran_seq(strTpTranseq);// 거래번호 셋팅
        //카드결제 시
             
        insobj.setMassagetype("10");//메세지타입
        insobj.setCard_num(reqpgobj.getId_info());//인증값
        insobj.setApp_no(r_auth_no);//승인번호

        insobj.setPg_seq(r_cno);//pg거래고유번호
        insobj.setPay_chn_cate(appOnffTidInfoVo.getPay_chn_cate());
        insobj.setTid(g_conf_site_cd);
        
        //kicc pg의 경우 결제ID를 가맹점 번호에 넣는다.
        insobj.setMerch_no(g_conf_site_cd);
        
        insobj.setOnffmerch_no(appOnffTidInfoVo.getOnffmerch_no());        
        insobj.setOnfftid(appOnffTidInfoVo.getOnfftid());
        insobj.setWcc(reqRefobj.getCash_issue_type());
        insobj.setCard_cate(reqRefobj.getCash_auth_type());
        insobj.setOrder_seq(reqpgobj.getOrdr_idxx());
        insobj.setTran_dt(strCurYear.substring(0, 8));
        insobj.setTran_tm(strCurYear.substring(8, 14));
        insobj.setTran_cate("10");
        
        insobj.setTax_amt(reqpgobj.getAmt_tax());//부가세금액
        insobj.setSvc_amt(reqpgobj.getAmt_svc());//비과세승인금액(서비스금액)
        
        insobj.setResult_cd(res_cd);
        insobj.setResult_msg(res_msg);
        
        insobj.setTran_status("00");//승인 :00 ,10:당일취소 , 20:매입취소
        insobj.setTran_step("00");
        insobj.setTrad_chk_flag("0");
        
        insobj.setClient_ip(reqpgobj.getCust_ip());
        insobj.setUser_type(reqpgobj.getUser_type());
        insobj.setMemb_user_no(reqRefobj.getMemb_user_no());
        insobj.setUser_id(reqRefobj.getUser_id());
        insobj.setUser_nm(reqpgobj.getBuyr_name());
        insobj.setUser_mail(reqpgobj.getBuyr_mail());
        insobj.setUser_phone1(reqpgobj.getBuyr_tel1());
        insobj.setUser_phone2(reqpgobj.getBuyr_tel2());
        insobj.setUser_addr(reqRefobj.getUser_addr());
        insobj.setProduct_type(reqRefobj.getProduct_type());
        insobj.setProduct_nm(reqpgobj.getGood_name()); 
        insobj.setIns_user(appFormBean.getUser_seq());
        insobj.setMod_user(appFormBean.getUser_seq());
        
        //가맹점 DB 처리
        Integer int_dbresult = appDAO.InsertCashPgTran(insobj);
        String strTpResult = int_dbresult.toString();
        
        logger.trace("------------------Cash App insert start------------------");
        logger.trace("cash App insert result : " + strTpResult);
        logger.trace("------------------Cash App insert End ------------------");
        
        ht.put("RESULT_CD", res_cd);
        ht.put("RESULT_MSG", res_msg); 
        ht.put("TRAN_SEQ", strTpTranseq); 
        
        return ht;
    }    
    
    

    @Override
    public Hashtable appAllatCashReqAction(AppFormBean appFormBean, AppOnffTidInfoVo appOnffTidInfoVo, AppTerminalInfoVo appTerminalInfoVo) throws Exception {
        Hashtable ht = new Hashtable();
        
        KiccPgVo reqRefobj =  appFormBean.getReqKiccPgVo();
        AllatPgVo reqpgobj = appFormBean.getReqAllatPgVo();
        AllatPgVo respgobj = new AllatPgVo();     
        
        Date today=new Date();
        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmss");       
        String strCurYear = formater.format(today);
        
        reqpgobj.setAllat_apply_ymdhms(strCurYear);
        
	String g_conf_site_cd   = appTerminalInfoVo.getTerminal_no();             // 리얼 반영시 KICC에 발급된 mall_id 사용
        String g_conf_site_key = appTerminalInfoVo.getTerminal_pwd();             // 리얼 반영시 KICC에 발급된 mall_id 사용
        
        if(g_conf_site_cd == null || g_conf_site_cd.equals(""))
        {
            throw new Exception("결제용TID가 없습니다.");
        }
        
        //가맹점주문번호를 생성한다.
        String strOrder_seq = appDAO.GetOrderSeq();
        reqpgobj.setAllat_order_no(strOrder_seq);
        
        //사업자정보를 가져온다.
        CompanyFormBean companyFormBean = new  CompanyFormBean();
        companyFormBean.setBiz_no(reqpgobj.getAllat_biz_no());
        List ls_onffCompInfo = compDAO.companyMasterInfo(companyFormBean);
        
        Tb_Company onffCompInfo= (Tb_Company)ls_onffCompInfo.get(0);
        
        //사업자의 과세 기준에 따라 과세면세구분을 셋팅한다.
        /*
        String strtpChkTaxflag = Util.nullToString(onffCompInfo.getTax_flag());
        
        //비과세("01")
        if(strtpChkTaxflag.equals("01"))
        {
            reqpgobj.setCorp_tax_type("TG02");//비과세
        }
        //과세("00")
        else
        {
            reqpgobj.setCorp_tax_type("TG01");//비과세
        }
        */
	logger.trace("------------------------------appAllatCashReqAction--------------------------------------");
        logger.trace("reqpgobj.getAllat_shop_id       () : " + reqpgobj.getAllat_shop_id       ());
        logger.trace("reqpgobj.getAllat_apply_ymdhms  () : " + reqpgobj.getAllat_apply_ymdhms  ());
        logger.trace("reqpgobj.getAllat_shop_member_id() : " + reqpgobj.getAllat_shop_member_id());
        logger.trace("reqpgobj.getAllat_cert_no       () : " + reqpgobj.getAllat_cert_no       ());
        logger.trace("reqpgobj.getAllat_supply_amt    () : " + reqpgobj.getAllat_supply_amt    ());
        logger.trace("reqpgobj.getAllat_vat_amt       () : " + reqpgobj.getAllat_vat_amt       ());
        logger.trace("reqpgobj.getAllat_receipt_type  () : " + reqpgobj.getAllat_receipt_type  ());
        logger.trace("reqpgobj.getAllat_product_nm    () : " + reqpgobj.getAllat_product_nm    ());
        logger.trace("reqpgobj.getAllat_seq_no        () : " + reqpgobj.getAllat_seq_no        ());
        logger.trace("reqpgobj.getAllat_reg_business_no() : " + reqpgobj.getAllat_reg_business_no());
        logger.trace("reqpgobj.getAllat_buyer_ip      () : " + reqpgobj.getAllat_buyer_ip      ());
        logger.trace("reqpgobj.getAllat_test_yn       () : " + reqpgobj.getAllat_test_yn       ());
        logger.trace("reqpgobj.getAllat_opt_pin       () : " + reqpgobj.getAllat_opt_pin       ());
        logger.trace("reqpgobj.getAllat_opt_mod       () : " + reqpgobj.getAllat_opt_mod       ());
        logger.trace("--------------------------------------------------------------------");        
        
        //수수료정보를 가져온다.
        appFormBean.setPay_mtd_seq(appOnffTidInfoVo.getPay_mtd_seq());
        List ls_onfftidcmsinfo = appDAO.GetOnffTidCommission(appFormBean);
        AppOnffTidInfoVo onfftidcmsinfo = (AppOnffTidInfoVo)ls_onfftidcmsinfo.get(0);
        
	logger.trace("------------------------------appAllatReqAction--------------------------------------");   
        logger.trace("getOnfftid_cms_seq : " + onfftidcmsinfo.getOnfftid_cms_seq());        
        logger.trace("getOnfftid : " + onfftidcmsinfo.getOnfftid());      
        logger.trace("getCommission : " + onfftidcmsinfo.getCommission());        
        logger.trace("getStart_dt : " + onfftidcmsinfo.getStart_dt());        
        logger.trace("getEnd_dt : " + onfftidcmsinfo.getEnd_dt());        
        logger.trace("--------------------------------------------------------------------");  
        
        //인스터스생성 초기화
        HashMap reqHm=new HashMap();
        HashMap resHm=null;
        String szReqMsg="";
        String szAllatEncData="";
        String szCrossKey="";
        
        AllatUtil util = new AllatUtil();
        
        /* -------------------------------------------------------------------------- */
        /* ::: 결제 결과                                                              */
        /* -------------------------------------------------------------------------- */
        String res_cd         = "";
        String res_msg        = "";
        String sApprovalNo    = "";
        String sCashBillNo    = "";
        
        /* -------------------------------------------------------------------------- */
        /* ::: 승인요청 전문 설정                                                     */
        /* -------------------------------------------------------------------------- */
        reqHm.put("allat_shop_id"           , g_conf_site_cd       );
        reqHm.put("allat_apply_ymdhms"      , g_conf_site_key  );
        reqHm.put("allat_shop_member_id"    , reqpgobj.getAllat_shop_member_id() );
        reqHm.put("allat_cert_no"           , reqpgobj.getAllat_cert_no()       );
        reqHm.put("allat_supply_amt"        , reqpgobj.getAllat_supply_amt()    );
        reqHm.put("allat_vat_amt"           , reqpgobj.getAllat_vat_amt()       );
        reqHm.put("allat_receipt_type"      , "NBANK"  );
        reqHm.put("allat_product_nm"        , reqpgobj.getAllat_product_nm()    );
        //reqHm.put("allat_seq_no"            , szSeqNo        );
        reqHm.put("allat_reg_business_no"   , reqpgobj.getAllat_reg_business_no());
        reqHm.put("allat_buyer_ip"          , reqpgobj.getAllat_buyer_ip()      );
        reqHm.put("allat_test_yn"           , Util.nullToString(reqpgobj.getAllat_test_yn())            );
        reqHm.put("allat_opt_pin"           , "NOUSE"        );
        reqHm.put("allat_opt_mod"           , "APP"          );

        szAllatEncData=util.setValue(reqHm);
        szReqMsg  = "allat_shop_id="   + g_conf_site_cd
                  + "&allat_enc_data=" + szAllatEncData
                  + "&allat_cross_key="+ g_conf_site_key;
        
        /* -------------------------------------------------------------------------- */
        /* ::: 실행                                                                   */
        /* -------------------------------------------------------------------------- */
        resHm = util.cashappReq(szReqMsg, "SSL");

        res_cd  = (String)resHm.get("reply_cd");
        res_msg = (String)resHm.get("reply_msg");
        
	logger.trace("--------------------------------------------------------------------");
        logger.trace("res_cd : " + res_cd);
        logger.trace("res_msg : " + res_msg);
        logger.trace("--------------------------------------------------------------------");     
        
        /* -------------------------------------------------------------------------- */
        /* ::: 결과 처리                                                              */
        /* -------------------------------------------------------------------------- */
        sApprovalNo    = (String)resHm.get("approval_no");
        sCashBillNo    = (String)resHm.get("cash_bill_no");
        
        logger.trace("--------------------------------------------------------------------");        
        logger.trace("r_cno            "+       sCashBillNo             );// 현금영수증 고유번호
        logger.trace("r_auth_no        "+       sApprovalNo         );//승인번호
        logger.trace("--------------------------------------------------------------------");
    
        
        //가맹점 DB처리
        //저장정보를 만든다.
    
        InsertCardPgTranBean insobj = new InsertCardPgTranBean();
        //승인 결가에 따라 저장을 위한 테이블명을 만든다.
        //성공관련 data 셋팅
        if (res_cd.equals("0000"))
        {
            insobj.setInstablenm("TB_TRAN_CASHPG_"+strCurYear.substring(2, 6));
            insobj.setApp_dt(strCurYear.substring(0, 8));
            insobj.setApp_tm(strCurYear.substring(8, 14));
            insobj.setResult_status("00");
            insobj.setAcc_chk_flag("0");
            insobj.setTot_amt(reqRefobj.getTot_amt());//결제총금액
                        
        }
        else
        {
            insobj.setInstablenm("TB_TRAN_CASHPG_"+strCurYear.substring(2, 6));
            insobj.setResult_status("99");                        
            insobj.setAcc_chk_flag("3");
            insobj.setTot_amt(reqRefobj.getTot_amt());//결제총금액
        }
        
        String strTpTranseq = appDAO.GetTranSeq();
        insobj.setTran_seq(strTpTranseq);// 거래번호 셋팅
        //카드결제 시
             
        insobj.setMassagetype("10");//메세지타입
        insobj.setCard_num(reqpgobj.getAllat_cert_no());//인증값
        insobj.setApp_no(sApprovalNo);//승인번호

        insobj.setPg_seq(sCashBillNo);//pg거래고유번호
        insobj.setPay_chn_cate(appOnffTidInfoVo.getPay_chn_cate());
        insobj.setTid(g_conf_site_cd);
        
        //kicc pg의 경우 결제ID를 가맹점 번호에 넣는다.
        insobj.setMerch_no(g_conf_site_cd);
        
        insobj.setOnffmerch_no(appOnffTidInfoVo.getOnffmerch_no());        
        insobj.setOnfftid(appOnffTidInfoVo.getOnfftid());
        insobj.setWcc(reqRefobj.getCash_issue_type());
        insobj.setCard_cate(reqRefobj.getCash_auth_type());
        insobj.setOrder_seq(reqpgobj.getAllat_order_no());
        insobj.setTran_dt(strCurYear.substring(0, 8));
        insobj.setTran_tm(strCurYear.substring(8, 14));
        insobj.setTran_cate("10");
        
        insobj.setTax_amt(reqRefobj.getCom_vat_amt());//부가세금액
        insobj.setSvc_amt(reqRefobj.getCom_free_amt());//비과세승인금액(서비스금액)
        
        insobj.setResult_cd(res_cd);
        insobj.setResult_msg(res_msg);
        
        insobj.setTran_status("00");//승인 :00 ,10:당일취소 , 20:매입취소
        insobj.setTran_step("00");
        insobj.setTrad_chk_flag("0");
        
        insobj.setClient_ip(reqRefobj.getClient_ip());
        insobj.setUser_type(reqRefobj.getUser_type());
        insobj.setMemb_user_no(reqRefobj.getMemb_user_no());
        insobj.setUser_id(reqRefobj.getUser_id());
        insobj.setUser_nm(reqRefobj.getUser_nm());
        insobj.setUser_mail(reqRefobj.getUser_mail());
        insobj.setUser_phone1(reqRefobj.getUser_phone1());
        insobj.setUser_phone2(reqRefobj.getUser_phone2());
        insobj.setUser_addr(reqRefobj.getUser_addr());
        insobj.setProduct_type(reqRefobj.getProduct_type());
        insobj.setProduct_nm(reqpgobj.getAllat_product_nm()); 
        insobj.setIns_user(appFormBean.getUser_seq());
        insobj.setMod_user(appFormBean.getUser_seq());
        
        //가맹점 DB 처리
        Integer int_dbresult = appDAO.InsertCashPgTran(insobj);
        String strTpResult = int_dbresult.toString();
        
        logger.trace("------------------Cash App insert start------------------");
        logger.trace("cash App insert result : " + strTpResult);
        logger.trace("------------------Cash App insert End ------------------");
        
        ht.put("RESULT_CD", res_cd);
        ht.put("RESULT_MSG", res_msg); 
        ht.put("TRAN_SEQ", strTpTranseq); 
        
        return ht;
    }    
        
    
    
    @Override
    @Transactional
    public Hashtable appCashResult(AppFormBean appFormBean) throws Exception {
         Hashtable ht = new Hashtable();
         List ls_tran_info = appDAO.GetCashTranInfo(appFormBean);
         
        Tb_Tran_CardPg cdResult =(Tb_Tran_CardPg)ls_tran_info.get(0);
        ht.put("tran_info_obj", cdResult);
         
        CompanyFormBean companyFormBean = new  CompanyFormBean();
        companyFormBean.setComp_seq("2015010100000001");        
        List ls_onffCompInfo = compDAO.companyMasterInfo(companyFormBean);
        
        Tb_Company onffCompInfo= (Tb_Company)ls_onffCompInfo.get(0);
        
        logger.trace(onffCompInfo.getComp_nm());
        
        ht.put("onffCompInfo",onffCompInfo); 
         
         
        return ht;
    }

    @Override
    @Transactional    
    public Hashtable appMgrAction(AppFormBean appFormBean) throws Exception {
        Hashtable ht = new Hashtable();

        //가맹점 번호 검사
        logger.trace("----------------------------------------------------------------");
        logger.trace("AppServiceImpl appMgrAction appFormBean.getOnfftid val : " + appFormBean.getOnfftid());
        logger.trace("AppServiceImpl appMgrAction appFormBean.getOnffmerch_no val : " + appFormBean.getOnffmerch_no());
        logger.trace("AppServiceImpl appMgrAction appFormBean.getUser_cate val : " + appFormBean.getUser_cate());
        
        logger.trace("AppServiceImpl appMgrAction appFormBean.getParam_onffmerch_no val : " + appFormBean.getParam_onffmerch_no());
        logger.trace("AppServiceImpl appMgrAction appFormBean.getParam_onfftid val : " + appFormBean.getParam_onfftid());
        logger.trace("----------------------------------------------------------------");
        
        String strUserCateVal = appFormBean.getUser_cate();
        
        KiccPgVo kiccpgparam = appFormBean.getReqKiccPgVo();
        
        logger.trace("----------------------------------------------------------------");
        logger.trace("AppControll appMgrAction kiccpgparam.setClient_ip val : " + kiccpgparam.getClient_ip() );
        logger.trace("AppControll appMgrAction kiccpgparam.getMgr_txtype val : " + kiccpgparam.getMgr_txtype() );
        logger.trace("AppControll appMgrAction kiccpgparam.getMgr_subtype val : " + kiccpgparam.getMgr_subtype());
        logger.trace("AppControll appMgrAction kiccpgparam.getOrg_cno val : " + kiccpgparam.getOrg_cno());
        logger.trace("AppControll appMgrAction kiccpgparam.getMgr_amt val : " + kiccpgparam.getMgr_amt());
        logger.trace("AppControll appMgrAction kiccpgparam.getReq_id val : " + kiccpgparam.getReq_id());
        logger.trace("AppControll appMgrAction kiccpgparam.getMgr_msg val : " + kiccpgparam.getMgr_msg());
        logger.trace("----------------------------------------------------------------");           
        
        String strTpMgr_txtype = kiccpgparam.getMgr_txtype() ;
        // 취소타입을 가져온다.
        //카드취소일 경우
        if(strTpMgr_txtype.equals("40"))
        {
            ////거래내역정보를 가져온다.
            List ls_tran_info = appDAO.GetCardTranInfo(appFormBean);
            
            Tb_Tran_CardPg cdResult =(Tb_Tran_CardPg)ls_tran_info.get(0);

            String strOnffTid = cdResult.getOnfftid();
            String strOnffMerch_no = cdResult.getOnffmerch_no();
            
            ht.put("TRAN_GUBUN","CARD");
            ht.put("TransCardInfo", cdResult);
            
            //취소가 가능한 유저타입인지 확인
            if(strUserCateVal.equals("00"))
            {
            }
            else if(strUserCateVal.equals("01"))
            {
                if(appFormBean.getOnffmerch_no() == null || !appFormBean.getOnffmerch_no().equals(strOnffMerch_no))
                {
                    ht.put("RESULT_CD", "OF31");
                    ht.put("RESULT_MSG", "취소권한이 없습니다.");
                    return ht;             
                }
                
            }
            else if(strUserCateVal.equals("02"))
            {
                if(appFormBean.getOnffmerch_no() == null || !appFormBean.getOnffmerch_no().equals(strOnffMerch_no))
                {
                    ht.put("RESULT_CD", "OF31");
                    ht.put("RESULT_MSG", "취소권한이 없습니다.");
                    return ht;             
                }                
                
                if(appFormBean.getOnfftid() == null || !appFormBean.getOnfftid().equals(strOnffTid))
                {
                    ht.put("RESULT_CD", "OF31");
                    ht.put("RESULT_MSG", "취소권한이 없습니다.");
                    return ht;             
                }
              
            }            
            else
            {
                ht.put("RESULT_CD", "OF31");
                ht.put("RESULT_MSG", "취소권한이 없습니다.");
                return ht;             
            }
            
            
            String strTpCurDt = Util.getTodayDate();
            String strTpAppdt = cdResult.getApp_dt();

            logger.debug("----AppServiceimpl strTpCurDt : " + strTpCurDt);
            logger.debug("----AppServiceimpl strTpAppdt : " + strTpAppdt);
            
            //당일취소가 아닐경우
            if(!strTpCurDt.equals(strTpAppdt))
            {
                //유저종류에 따라 취소권한검증
                //가맹점관리자
                if(strUserCateVal.equals("01"))
                {
                    //가맹점 취소권한 확인
                    String strTpOnffmerch_cnclauth =  Util.nullToString(appDAO.getOnffmerchCnclAuth(strOnffMerch_no));
                    logger.debug("----AppServiceimpl strUserCateVal : " + strUserCateVal);
                    logger.debug("----AppServiceimpl strOnffMerch_no : " + strOnffMerch_no);
                    logger.debug("----AppServiceimpl strTpOnffmerch_cnclauth : " + strTpOnffmerch_cnclauth);
                    
                    if(!strTpOnffmerch_cnclauth.equals("00"))
                    {
                        ht.put("RESULT_CD", "OF31");
                        ht.put("RESULT_MSG", "취소권한이 없습니다.");
                        return ht;        
                    }
                }
                //결제관리자
                else if(strUserCateVal.equals("02"))
                {
                    //가맹점 취소권한 확인
                    String strTpOnfftid_cnclauth =  Util.nullToString(appDAO.getOnfftidCnclAuth(strOnffTid));
                    logger.debug("----AppServiceimpl strUserCateVal : " + strUserCateVal);
                    logger.debug("----AppServiceimpl strOnffTid : " + strOnffTid);
                    logger.debug("----AppServiceimpl strTpOnfftid_cnclauth : " + strTpOnfftid_cnclauth);                    
                    
                    if(!strTpOnfftid_cnclauth.equals("00"))
                    {
                        ht.put("RESULT_CD", "OF31");
                        ht.put("RESULT_MSG", "취소권한이 없습니다.");
                        return ht;        
                    }                    
                }
            }
            
            //거래내역의 TID로 결제사를 가져온다.
            String strTidMth = appDAO.GetTerminalMtdInfo(cdResult.getTid());
            
            logger.debug("----------------------------------------------------------------");               
            logger.debug("AppControll appMgrAction strTidMth val : " + strTidMth);
            logger.debug("----------------------------------------------------------------");                       
            
            //allat의 경우 당일취소일 경우 검사한다.
            if(strTidMth != null && strTidMth.equals("ALLATPG"))
            {
                List<Tb_Tid_Info> ln_TidInfo = appDAO.GetTerminalInfo(cdResult.getTid());     
                
                Tb_Tid_Info cnktidinfo = ln_TidInfo.get(0);
                String tpStrCurDayCncFlag = Util.nullToString(cnktidinfo.getAppcnclflag());

                //올엣 pg offline 당일취소 막기위한 검사로직
                if(tpStrCurDayCncFlag.equals("N"))
                {
                    if(strTpAppdt.equals(strTpCurDt))
                    {
                        ht.put("RESULT_CD", "OF37");
                        ht.put("RESULT_MSG", "당일취소가 불가능한 TID입니다.");
                        return ht;                           
                    }
                }
            }
        

            //취소금액 검증
            if(!strUserCateVal.equals("00"))
            {
                String strCurAppAmt = appDAO.GetCurAppTotAmt(strOnffMerch_no);
                logger.debug("----------------------------------------------------------------");               
                logger.debug("AppControll appMgrAction cdResult.getTot_amt() val : " + cdResult.getTot_amt());
                logger.debug("AppControll appMgrAction strCurAppAmt val : " + strCurAppAmt);
                logger.debug("----------------------------------------------------------------");                       

                //double dblChk = Double.parseDouble(cdResult.getTot_amt())- Double.parseDouble(strCurAppAmt);

                if( Double.parseDouble(cdResult.getTot_amt()) > Double.parseDouble(strCurAppAmt))
                {
                    ht.put("RESULT_CD", "OF34");
                    ht.put("RESULT_MSG", "취소가능금액보다 취소요청금액이 큽니다.");
                    return ht;                 
                }
            }

            ht.put("TIDMTD", strTidMth);
            
        }
        //현금영수증 취소일 경우
        else if(strTpMgr_txtype.equals("51"))
        {
             ////거래내역정보를 가져온다.
            List ls_tran_info = appDAO.GetCashTranInfo(appFormBean);
            
            Tb_Tran_CardPg cdResult =(Tb_Tran_CardPg)ls_tran_info.get(0);

            String strOnffTid = cdResult.getOnfftid();
            String strOnffMerch_no = cdResult.getOnffmerch_no();
            
            ht.put("TRAN_GUBUN","CASH");
            ht.put("TransCashInfo", cdResult);
            
            //취소가 가능한 유저타입인지 확인
            if(strUserCateVal.equals("00"))
            {
            }
            else if(strUserCateVal.equals("01"))
            {
                if(appFormBean.getOnffmerch_no() == null || !appFormBean.getOnffmerch_no().equals(strOnffMerch_no))
                {
                    ht.put("RESULT_CD", "OF31");
                    ht.put("RESULT_MSG", "취소권한이 없습니다.");
                    return ht;             
                }
                
            }
            else if(strUserCateVal.equals("02"))
            {
                if(appFormBean.getOnffmerch_no() == null || !appFormBean.getOnffmerch_no().equals(strOnffMerch_no))
                {
                    ht.put("RESULT_CD", "OF31");
                    ht.put("RESULT_MSG", "취소권한이 없습니다.");
                    return ht;             
                }                
                
                if(appFormBean.getOnfftid() == null || !appFormBean.getOnfftid().equals(strOnffTid))
                {
                    ht.put("RESULT_CD", "OF31");
                    ht.put("RESULT_MSG", "취소권한이 없습니다.");
                    return ht;             
                }
              
            }            
            else
            {
                ht.put("RESULT_CD", "OF31");
                ht.put("RESULT_MSG", "취소권한이 없습니다.");
                return ht;             
            }
            //거래내역의 TID로 회사를 가져온다.
            String strTidMth = appDAO.GetTerminalMtdInfo(cdResult.getTid());
        
            logger.trace("----------------------------------------------------------------");               
            logger.trace("AppControll appMgrAction strTidMth val : " + strTidMth);
            logger.trace("----------------------------------------------------------------");     
            
            ht.put("TIDMTD", strTidMth);

        }

        ht.put("RESULT_CD", "0000");
        
        return ht;
    }
    

    @Override
    @Transactional    
    public Hashtable appCnclReqAction(AppFormBean appFormBean) throws Exception {
        Hashtable ht = new Hashtable();

        //가맹점 번호 검사
        logger.trace("----------------------------------------------------------------");
        logger.trace("AppServiceImpl appMgrAction appFormBean.getOnfftid val : " + appFormBean.getOnfftid());
        logger.trace("AppServiceImpl appMgrAction appFormBean.getOnffmerch_no val : " + appFormBean.getOnffmerch_no());
        logger.trace("AppServiceImpl appMgrAction appFormBean.getUser_cate val : " + appFormBean.getUser_cate());
        
        logger.trace("AppServiceImpl appMgrAction appFormBean.getParam_onffmerch_no val : " + appFormBean.getParam_onffmerch_no());
        logger.trace("AppServiceImpl appMgrAction appFormBean.getParam_onfftid val : " + appFormBean.getParam_onfftid());
        logger.trace("----------------------------------------------------------------");
        
        ////거래내역정보를 가져온다.
        List ls_tran_info = appDAO.GetCardTranInfo(appFormBean);
            
        Tb_Tran_CardPg cdResult =(Tb_Tran_CardPg)ls_tran_info.get(0);

        String strOnffTid = cdResult.getOnfftid();
        String strOnffMerch_no = cdResult.getOnffmerch_no();
        
        InsertCardPgTranBean paramdbobj = new InsertCardPgTranBean();
        paramdbobj.setInstablenm(cdResult.getTb_nm());
        paramdbobj.setTran_seq(appFormBean.getTran_seq());
        paramdbobj.setMod_user(appFormBean.getUser_seq());
        paramdbobj.setTran_status("05");
        
        Integer int_reversdbresult = appDAO.CnclReaCardTranUpdate(paramdbobj);
        
        ht.put("RESULT_CD", "0000");
        ht.put("RESULT_MSG", "취소요청 되었습니다.");
        
        return ht;
    }        

    /*
    @Override
    @Transactional    
    public Hashtable appForceCnclReqAction(AppFormBean appFormBean) throws Exception {
        Hashtable ht = new Hashtable();

        //가맹점 번호 검사
        logger.trace("----------------------------------------------------------------");
        logger.trace("AppServiceImpl appMgrAction appFormBean.getOnfftid val : " + appFormBean.getOnfftid());
        logger.trace("AppServiceImpl appMgrAction appFormBean.getOnffmerch_no val : " + appFormBean.getOnffmerch_no());
        logger.trace("AppServiceImpl appMgrAction appFormBean.getUser_cate val : " + appFormBean.getUser_cate());
        
        logger.trace("AppServiceImpl appMgrAction appFormBean.getParam_onffmerch_no val : " + appFormBean.getParam_onffmerch_no());
        logger.trace("AppServiceImpl appMgrAction appFormBean.getParam_onfftid val : " + appFormBean.getParam_onfftid());
        logger.trace("----------------------------------------------------------------");
        
        ////거래내역정보를 가져온다.
        List ls_tran_info = appDAO.GetCardTranInfo(appFormBean);
            
        Tb_Tran_CardPg cdResult =(Tb_Tran_CardPg)ls_tran_info.get(0);

        String strOnffTid = cdResult.getOnfftid();
        String strOnffMerch_no = cdResult.getOnffmerch_no();

        
        //취소정보를 생성한다.
        InsertCardPgTranBean paramdbobj = new InsertCardPgTranBean();
        
        
        //원거래를 UPDate한다.
        
        
        ht.put("RESULT_CD", "0000");
        ht.put("RESULT_MSG", "강제취소 되었습니다.");
        
        return ht;
    }    
*/
    @Override
    @Transactional
    public Hashtable appMgrKiccAction(AppFormBean appFormBean,Tb_Tran_CardPg tranInfo) throws Exception {
        Hashtable ht = new Hashtable();
        
        KiccPgVo reqpgobj = appFormBean.getReqKiccPgVo();
        KiccPgVo respgobj = new KiccPgVo();
        
        Date today=new Date();
        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmss");       
        String strCurYear = formater.format(today);

        String g_cert_file  = reqpgobj.getKiccpg_cert_file();
	String g_log_dir    = reqpgobj.getKiccpg_g_log_dir();
	int g_log_level  = 1;
        
	// ============================================================================== /
	// =   02. 쇼핑몰 지불 정보 설정                                                = /
	// = -------------------------------------------------------------------------- = /		
        String g_gw_url    = reqpgobj.getKiccpg_g_gw_url();
        String g_gw_port   = reqpgobj.getKiccpg_g_gw_port();
        
	String g_mall_id   = tranInfo.getTid();             // 리얼 반영시 KICC에 발급된 mall_id 사용
        String g_mall_name = tranInfo.getComp_nm();    
        
        if(g_mall_id == null || g_mall_id.equals(""))
        {
            throw new Exception("결제용TID가 없습니다.");
        }
        
	logger.trace("------------------------------appMgrKiccAction--------------------------------------");
        logger.trace("g_cert_file : " + g_cert_file);
        logger.trace("g_log_dir : " + g_log_dir);
        logger.trace("g_gw_url : " + g_gw_url);
        logger.trace("g_gw_port : " + g_gw_port);
        logger.trace("g_mall_id : " + g_mall_id);
        logger.trace("g_mall_name : " + g_mall_name);
        logger.trace("--------------------------------------------------------------------");        
        
        
        /* -------------------------------------------------------------------------- */
        /* ::: EasyPayClient 인스턴스 생성 [변경불가 !!].                             */
        /* -------------------------------------------------------------------------- */
        EasyPayClient easyPayClient = new EasyPayClient();
        easyPayClient.easypay_setenv_init( g_gw_url, g_gw_port, g_cert_file, g_log_dir );
        easyPayClient.easypay_reqdata_init();
        
        String tr_cd = reqpgobj.getTr_cd();
        String req_cno = reqpgobj.getReq_cno();
        String pay_type = reqpgobj.getPay_type();
        String req_type = reqpgobj.getReq_type();
        String cert_type = reqpgobj.getCert_type();
        
        String TRAN_CD_NOR_MGR = reqpgobj.getTRAN_CD_NOR_MGR();
        

            /* -------------------------------------------------------------------------- */
            /* ::: 결제 결과                                                              */
            /* -------------------------------------------------------------------------- */
            String bDBProc          = "";
            String res_cd           = "";
            String res_msg          = "";
            String r_order_no       = "";
            String r_complex_yn     = "";
            String r_msg_type       = "";     //거래구분
            String r_noti_type	    = "";     //노티구분
            String r_cno            = "";     //PG거래번호
            String r_amount         = "";     //총 결제금액
            String r_auth_no        = "";     //승인번호
            String r_tran_date      = "";     //거래일시
            String r_pnt_auth_no    = "";     //포인트 승인 번호
            String r_pnt_tran_date  = "";     //포인트 승인 일시
            String r_cpon_auth_no   = "";     //쿠폰 승인 번호
            String r_cpon_tran_date = "";     //쿠폰 승인 일시
            String r_card_no        = "";     //카드번호
            String r_issuer_cd      = "";     //발급사코드
            String r_issuer_nm      = "";     //발급사명
            String r_acquirer_cd    = "";     //매입사코드
            String r_acquirer_nm    = "";     //매입사명
            String r_install_period = "";     //할부개월
            String r_noint          = "";     //무이자여부
            String r_bank_cd        = "";     //은행코드
            String r_bank_nm        = "";     //은행명
            String r_account_no     = "";     //계좌번호
            String r_deposit_nm     = "";     //입금자명
            String r_expire_date    = "";     //계좌사용 만료일
            String r_cash_res_cd    = "";     //현금영수증 결과코드
            String r_cash_res_msg   = "";     //현금영수증 결과메세지
            String r_cash_auth_no   = "";     //현금영수증 승인번호
            String r_cash_tran_date = "";     //현금영수증 승인일시
            String r_auth_id        = "";     //PhoneID
            String r_billid         = "";     //인증번호
            String r_mobile_no      = "";     //휴대폰번호
            String r_ars_no         = "";     //ARS 전화번호
            String r_cp_cd          = "";     //포인트사
            String r_used_pnt       = "";     //사용포인트
            String r_remain_pnt     = "";     //잔여한도
            String r_pay_pnt        = "";     //할인/발생포인트
            String r_accrue_pnt     = "";     //누적포인트
            String r_remain_cpon    = "";     //쿠폰잔액
            String r_used_cpon      = "";     //쿠폰 사용금액
            String r_mall_nm        = "";     //제휴사명칭
            String r_escrow_yn	    = "";     //에스크로 사용유무
            String r_canc_acq_data  = "";     //매입취소일시
            String r_canc_date      = "";     //취소일시
            String r_refund_date    = "";     //환불예정일시
            String r_join_no = ""; //카드사가맹점번호
            
            //가맹점주문번호를 생성한다.
            String strOrder_seq = appDAO.GetOrderSeq();
            reqpgobj.setOrder_no(strOrder_seq);
        
            logger.trace("----------------------------------appserviceimple mgrkiccaction------------------------------------");
            logger.trace(" reqpgobj.getTr_cd() " +  reqpgobj.getTr_cd());
            logger.trace(" reqpgobj.getMgr_txtype() " +  reqpgobj.getMgr_txtype());
            logger.trace(" reqpgobj.getOrg_cno() " +  reqpgobj.getOrg_cno());
            logger.trace(" reqpgobj.getOrder_no() " +  reqpgobj.getOrder_no());
            logger.trace(" appFormBean.getUser_id() " +  appFormBean.getUser_id() );
            logger.trace(" reqpgobj.getMgr_msg() " +  reqpgobj.getMgr_msg());            
            logger.trace("----------------------------------------------------------------------");
            

        /* -------------------------------------------------------------------------- */
        /* ::: 변경관리 요청                                                          */
        /* -------------------------------------------------------------------------- */
        //if( TRAN_CD_NOR_MGR.equals( tr_cd ) ) {

            int easypay_mgr_data_item;
            easypay_mgr_data_item = easyPayClient.easypay_item( "mgr_data" );

            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_txtype"		,  reqpgobj.getMgr_txtype() 	);
            //easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_subtype"		,  reqpgobj.getMgr_subtype() 	);
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "org_cno"			,  reqpgobj.getOrg_cno() 		);
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "order_no"		,  reqpgobj.getOrder_no()		);
            //easyPayClient.easypay_deli_us( easypay_mgr_data_item, "pay_type"		,  reqpgobj.getPay_type()		);
            //easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_amt"			,  tranInfo.getTot_amt()		);
            //easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_bank_cd"		,  reqpgobj.getMgr_bank_cd() 	);
            //easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_account"		,  reqpgobj.getMgr_account() 	);
            //easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_depositor"	,  reqpgobj.getMgr_depositor() );
            //easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_socno"		,  reqpgobj.getMgr_socno() 	);
           // easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_telno"		,  reqpgobj.getMgr_telno() 	);
           // easyPayClient.easypay_deli_us( easypay_mgr_data_item, "deli_cd"			,  reqpgobj.getDeli_cd() 		);
           // easyPayClient.easypay_deli_us( easypay_mgr_data_item, "deli_corp_cd"	,  reqpgobj.getDeli_corp_cd() 	);
           // easyPayClient.easypay_deli_us( easypay_mgr_data_item, "deli_invoice"	,  reqpgobj.getDeli_invoice() 	);
           // easyPayClient.easypay_deli_us( easypay_mgr_data_item, "deli_rcv_nm"		,  reqpgobj.getDeli_rcv_nm() 	);
           // easyPayClient.easypay_deli_us( easypay_mgr_data_item, "deli_rcv_tel"	,  reqpgobj.getDeli_rcv_tel() 	);
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "req_ip"			,  reqpgobj.getClient_ip()		);
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "req_id"			,  appFormBean.getUser_id()        );
            easyPayClient.easypay_deli_us( easypay_mgr_data_item, "mgr_msg"			,  reqpgobj.getMgr_msg()		);
        //}

        /* -------------------------------------------------------------------------- */
        /* ::: 실행                                                                   */
        /* -------------------------------------------------------------------------- */
        if ( tr_cd.length() > 0 ) {
            easyPayClient.easypay_run( g_mall_id, tr_cd, reqpgobj.getOrder_no() );

            res_cd = easyPayClient.res_cd;
            res_msg = easyPayClient.res_msg;

        }
        else {
            res_cd  = "M114";
            res_msg = "연동 오류|tr_cd값이 설정되지 않았습니다.";
        }
        
	logger.trace("--------------------------------------------------------------------");
        logger.trace("res_cd : " + res_cd);
        logger.trace("res_msg : " + res_msg);
        logger.trace("--------------------------------------------------------------------");     
        
        /* -------------------------------------------------------------------------- */
        /* ::: 결과 처리                                                              */
        /* -------------------------------------------------------------------------- */
        r_cno             = Util.nullToSpaceString(easyPayClient.easypay_get_res( "cno"             ));    // PG거래번호
        r_amount          = Util.nullToSpaceString(easyPayClient.easypay_get_res( "amount"          ));    //총 결제금액
        r_order_no        = Util.nullToSpaceString(easyPayClient.easypay_get_res( "order_no"        ));    //주문번호
        r_noti_type       = Util.nullToSpaceString(easyPayClient.easypay_get_res( "noti_type"       ));    //노티구분
        r_auth_no         = Util.nullToSpaceString(easyPayClient.easypay_get_res( "auth_no"         ));    //승인번호
        r_tran_date       = Util.nullToSpaceString(easyPayClient.easypay_get_res( "tran_date"       ));    //승인일시
        r_pnt_auth_no     = Util.nullToSpaceString(easyPayClient.easypay_get_res( "pnt_auth_no"     ));    //포인트승인번호
        r_pnt_tran_date   = Util.nullToSpaceString(easyPayClient.easypay_get_res( "pnt_tran_date"   ));    //포인트승인일시
        r_cpon_auth_no    = Util.nullToSpaceString(easyPayClient.easypay_get_res( "cpon_auth_no"    ));    //쿠폰승인번호
        r_cpon_tran_date  = Util.nullToSpaceString(easyPayClient.easypay_get_res( "cpon_tran_date"  ));    //쿠폰승인일시
        r_card_no         = Util.nullToSpaceString(easyPayClient.easypay_get_res( "card_no"         ));    //카드번호
        r_issuer_cd       = Util.nullToSpaceString(easyPayClient.easypay_get_res( "issuer_cd"       ));    //발급사코드
        r_issuer_nm       = Util.nullToSpaceString(easyPayClient.easypay_get_res( "issuer_nm"       ));    //발급사명
        r_acquirer_cd     = Util.nullToSpaceString(easyPayClient.easypay_get_res( "acquirer_cd"     ));    //매입사코드
        r_acquirer_nm     = Util.nullToSpaceString(easyPayClient.easypay_get_res( "acquirer_nm"     ));    //매입사명
        r_install_period  = Util.nullToSpaceString(easyPayClient.easypay_get_res( "install_period"  ));    //할부개월
        r_noint           = Util.nullToSpaceString(easyPayClient.easypay_get_res( "noint"           ));    //무이자여부
        r_bank_cd         = Util.nullToSpaceString(easyPayClient.easypay_get_res( "bank_cd"         ));    //은행코드
        r_bank_nm         = Util.nullToSpaceString(easyPayClient.easypay_get_res( "bank_nm"         ));    //은행명
        r_account_no      = Util.nullToSpaceString(easyPayClient.easypay_get_res( "account_no"      ));    //계좌번호
        r_deposit_nm      = Util.nullToSpaceString(easyPayClient.easypay_get_res( "deposit_nm"      ));    //입금자명
        r_expire_date     = Util.nullToSpaceString(easyPayClient.easypay_get_res( "expire_date"     ));    //계좌사용만료일
        r_cash_res_cd     = Util.nullToSpaceString(easyPayClient.easypay_get_res( "cash_res_cd"     ));    //현금영수증 결과코드
        r_cash_res_msg    = Util.nullToSpaceString(easyPayClient.easypay_get_res( "cash_res_msg"    ));    //현금영수증 결과메세지
        r_cash_auth_no    = Util.nullToSpaceString(easyPayClient.easypay_get_res( "cash_auth_no"    ));    //현금영수증 승인번호
        r_cash_tran_date  = Util.nullToSpaceString(easyPayClient.easypay_get_res( "cash_tran_date"  ));    //현금영수증 승인일시
        r_auth_id         = Util.nullToSpaceString(easyPayClient.easypay_get_res( "auth_id"         ));    //PhoneID
        r_billid          = Util.nullToSpaceString(easyPayClient.easypay_get_res( "billid"          ));    //인증번호
        r_mobile_no       = Util.nullToSpaceString(easyPayClient.easypay_get_res( "mobile_no"       ));    //휴대폰번호
        r_ars_no          = Util.nullToSpaceString(easyPayClient.easypay_get_res( "ars_no"          ));    //전화번호
        r_cp_cd           = Util.nullToSpaceString(easyPayClient.easypay_get_res( "cp_cd"           ));    //포인트사/쿠폰사
        r_used_pnt        = Util.nullToSpaceString(easyPayClient.easypay_get_res( "used_pnt"        ));    //사용포인트
        r_remain_pnt      = Util.nullToSpaceString(easyPayClient.easypay_get_res( "remain_pnt"      ));    //잔여한도
        r_pay_pnt         = Util.nullToSpaceString(easyPayClient.easypay_get_res( "pay_pnt"         ));    //할인/발생포인트
        r_accrue_pnt      = Util.nullToSpaceString(easyPayClient.easypay_get_res( "accrue_pnt"      ));    //누적포인트
        r_remain_cpon     = Util.nullToSpaceString(easyPayClient.easypay_get_res( "remain_cpon"     ));    //쿠폰잔액
        r_used_cpon       = Util.nullToSpaceString(easyPayClient.easypay_get_res( "used_cpon"       ));    //쿠폰 사용금액
        r_mall_nm         = Util.nullToSpaceString(easyPayClient.easypay_get_res( "mall_nm"         ));    //제휴사명칭
        r_escrow_yn       = Util.nullToSpaceString(easyPayClient.easypay_get_res( "escrow_yn"       ));    //에스크로 사용유무
        r_complex_yn      = Util.nullToSpaceString(easyPayClient.easypay_get_res( "complex_yn"      ));    //복합결제 유무
        r_canc_acq_data   = Util.nullToSpaceString(easyPayClient.easypay_get_res( "canc_acq_data"   ));    //매입취소일시
        r_canc_date       = Util.nullToSpaceString(easyPayClient.easypay_get_res( "canc_date"       ));    //취소일시
        r_refund_date     = Util.nullToSpaceString(easyPayClient.easypay_get_res( "refund_date"     ));    //환불예정일시
        r_join_no    = Util.nullToSpaceString(easyPayClient.easypay_get_res( "join_no"     ));    //카드사가맹점번호
        
        logger.trace("--------------------------------------------------------------------");        
        logger.trace("r_cno            "+       r_cno             );// PG거래번호
        logger.trace("r_amount         "+       r_amount          );//총 결제금액
        logger.trace("r_order_no       "+       r_order_no        );//주문번호
        logger.trace("r_noti_type      "+       r_noti_type       );//노티구분
        logger.trace("r_auth_no        "+       r_auth_no         );//승인번호
        logger.trace("r_tran_date      "+       r_tran_date       );//승인일시
        logger.trace("r_pnt_auth_no    "+       r_pnt_auth_no     );//포인트승인번호
        logger.trace("r_pnt_tran_date  "+       r_pnt_tran_date   );//포인트승인일시
        logger.trace("r_cpon_auth_no   "+       r_cpon_auth_no    );//쿠폰승인번호
        logger.trace("r_cpon_tran_date "+       r_cpon_tran_date  );//쿠폰승인일시
        logger.trace("r_card_no        "+       r_card_no         );//카드번호
        logger.trace("r_issuer_cd      "+       r_issuer_cd       );//발급사코드
        logger.trace("r_issuer_nm      "+       r_issuer_nm       );//발급사명
        logger.trace("r_acquirer_cd    "+       r_acquirer_cd     );//매입사코드
        logger.trace("r_acquirer_nm    "+       r_acquirer_nm     );//매입사명
        logger.trace("r_install_period "+       r_install_period  );//할부개월
        logger.trace("r_noint          "+       r_noint           );//무이자여부
        logger.trace("r_bank_cd        "+       r_bank_cd         );//은행코드
        logger.trace("r_bank_nm        "+       r_bank_nm         );//은행명
        logger.trace("r_account_no     "+       r_account_no      );//계좌번호
        logger.trace("r_deposit_nm     "+       r_deposit_nm      );//입금자명
        logger.trace("r_expire_date    "+       r_expire_date     );//계좌사용만료일
        logger.trace("r_cash_res_cd    "+       r_cash_res_cd     );//현금영수증 결과코드
        logger.trace("r_cash_res_msg   "+       r_cash_res_msg    );//현금영수증 결과메세지
        logger.trace("r_cash_auth_no   "+       r_cash_auth_no    );//현금영수증 승인번호
        logger.trace("r_cash_tran_date "+       r_cash_tran_date  );//현금영수증 승인일시
        logger.trace("r_auth_id        "+       r_auth_id         );//PhoneID
        logger.trace("r_billid         "+       r_billid          );//인증번호
        logger.trace("r_mobile_no      "+       r_mobile_no       );//휴대폰번호
        logger.trace("r_ars_no         "+       r_ars_no          );//전화번호
        logger.trace("r_cp_cd          "+       r_cp_cd           );//포인트사/쿠폰사
        logger.trace("r_used_pnt       "+       r_used_pnt        );//사용포인트
        logger.trace("r_remain_pnt     "+       r_remain_pnt      );//잔여한도
        logger.trace("r_pay_pnt        "+       r_pay_pnt         );//할인/발생포인트
        logger.trace("r_accrue_pnt     "+       r_accrue_pnt      );//누적포인트
        logger.trace("r_remain_cpon    "+       r_remain_cpon     );//쿠폰잔액
        logger.trace("r_used_cpon      "+       r_used_cpon       );//쿠폰 사용금액
        logger.trace("r_mall_nm        "+       r_mall_nm         );//제휴사명칭
        logger.trace("r_escrow_yn      "+       r_escrow_yn       );//에스크로 사용유무
        logger.trace("r_complex_yn     "+       r_complex_yn      );//복합결제 유무
        logger.trace("r_canc_acq_data  "+       r_canc_acq_data   );//매입취소일시
        logger.trace("r_canc_date      "+       r_canc_date       );//취소일시
        logger.trace("r_refund_date    "+       r_refund_date     );//환불예정일시
        logger.trace("r_join_no        "+       r_join_no     );//카드사가맹점번호
        logger.trace("--------------------------------------------------------------------");
    
        respgobj.setR_cno             (r_cno            );
        respgobj.setR_amount          (r_amount         );
        respgobj.setR_order_no        (r_order_no       );
        respgobj.setR_noti_type       (r_noti_type      );
        respgobj.setR_auth_no         (r_auth_no        );
        respgobj.setR_tran_date       (r_tran_date      );
        respgobj.setR_pnt_auth_no     (r_pnt_auth_no    );
        respgobj.setR_pnt_tran_date   (r_pnt_tran_date  );
        respgobj.setR_cpon_auth_no    (r_cpon_auth_no   );
        respgobj.setR_cpon_tran_date  (r_cpon_tran_date );
        respgobj.setR_card_no         (r_card_no        );
        respgobj.setR_issuer_cd       (r_issuer_cd      );
        respgobj.setR_issuer_nm       (r_issuer_nm      );
        respgobj.setR_acquirer_cd     (r_acquirer_cd    );
        respgobj.setR_acquirer_nm     (r_acquirer_nm    );
        respgobj.setR_install_period  (r_install_period );
        respgobj.setR_noint           (r_noint          );
        respgobj.setR_bank_cd         (r_bank_cd        );
        respgobj.setR_bank_nm         (r_bank_nm        );
        respgobj.setR_account_no      (r_account_no     );
        respgobj.setR_deposit_nm      (r_deposit_nm     );
        respgobj.setR_expire_date     (r_expire_date    );
        respgobj.setR_cash_res_cd     (r_cash_res_cd    );
        respgobj.setR_cash_res_msg    (r_cash_res_msg   );
        respgobj.setR_cash_auth_no    (r_cash_auth_no   );
        respgobj.setR_cash_tran_date  (r_cash_tran_date );
        respgobj.setR_auth_id         (r_auth_id        );
        respgobj.setR_billid          (r_billid         );
        respgobj.setR_mobile_no       (r_mobile_no      );
        respgobj.setR_ars_no          (r_ars_no         );
        respgobj.setR_cp_cd           (r_cp_cd          );
        respgobj.setR_used_pnt        (r_used_pnt       );
        respgobj.setR_remain_pnt      (r_remain_pnt     );
        respgobj.setR_pay_pnt         (r_pay_pnt        );
        respgobj.setR_accrue_pnt      (r_accrue_pnt     );
        respgobj.setR_remain_cpon     (r_remain_cpon    );
        respgobj.setR_used_cpon       (r_used_cpon      );
        respgobj.setR_mall_nm         (r_mall_nm        );
        respgobj.setR_escrow_yn       (r_escrow_yn      );
        respgobj.setR_complex_yn      (r_complex_yn     );
        respgobj.setR_canc_acq_data   (r_canc_acq_data  );
        respgobj.setR_canc_date       (r_canc_date      );
        respgobj.setR_refund_date     (r_refund_date    );
        respgobj.setR_join_no     (r_join_no    );
        
        //가맹점 DB처리
        //결제수단 즉시취소
        if(reqpgobj.getMgr_txtype().equals("40"))
        {
            //r거래값을 입력한다.
            InsertCardPgTranBean cnclobj = new InsertCardPgTranBean();
            //승인 결가에 따라 저장을 위한 테이블명을 만든다.
    
            //가맹점 DB 처리에러시 
            String strTpCnclTranseq = appDAO.GetTranSeq();
            cnclobj.setTran_seq(strTpCnclTranseq);// 거래번호 셋팅

            //취소성공시
            if (res_cd.equals("0000"))
            {
                cnclobj.setInstablenm("TB_TRAN_CARDPG_"+r_canc_date.substring(2, 6));
                cnclobj.setApp_dt(r_canc_date.substring(0, 8));
                cnclobj.setApp_tm(r_canc_date.substring(8, 14));
                cnclobj.setResult_status("00");
                
                cnclobj.setTran_step("00");
                cnclobj.setCncl_dt(r_canc_date.substring(0, 8));
                        
                String strTpCnclDate = r_canc_date.substring(0, 8); 
                //당일취소,익일취소 구분
                String strTpOrgdt = tranInfo.getApp_dt().replaceAll("-", "");
                if(strTpCnclDate.equals(strTpOrgdt))
                {
                   cnclobj.setTran_status("10");//승인 :00 ,10:당일취소 , 20:매입취소
                   cnclobj.setAcc_chk_flag("3");//당일취소는 매입미대상
                }
                else
                {
                   cnclobj.setTran_status("20");//승인 :00 ,10:당일취소 , 20:매입취소
                   cnclobj.setAcc_chk_flag("0");//당일취소는 매입미대상                    
                }
                
            }
            else
            {
                cnclobj.setInstablenm("TB_TRAN_CARDPG_"+strCurYear.substring(2, 6));
                cnclobj.setResult_status("99");                        
                cnclobj.setAcc_chk_flag("3");//r거래는3
                
                //당일취소,익일취소 구분
                String strTpOrgdt = tranInfo.getApp_dt().replaceAll("-", "");
                if(strCurYear.substring(0, 8).equals(strTpOrgdt))
                {
                   cnclobj.setTran_status("10");//승인 :00 ,10:당일취소 , 20:매입취소
                }
                else
                {
                   cnclobj.setTran_status("20");//승인 :00 ,10:당일취소 , 20:매입취소
                }
            }

            cnclobj.setMassagetype("40");//메세지타입
            cnclobj.setCard_num(tranInfo.getCard_num());//카드번호
            cnclobj.setApp_card_num(tranInfo.getApp_card_num());//카드번호
            cnclobj.setApp_no(r_auth_no);//승인번호
            cnclobj.setTot_amt(tranInfo.getTot_amt());//결제총금액
            cnclobj.setPg_seq(r_cno);//pg거래고유번호
            cnclobj.setPay_chn_cate(tranInfo.getPay_chn_cate());
            cnclobj.setTid(g_mall_id);
            cnclobj.setOnffmerch_no(tranInfo.getOnffmerch_no());        
            cnclobj.setOnfftid(tranInfo.getOnfftid());
            cnclobj.setWcc(tranInfo.getWcc());
            cnclobj.setExpire_dt(tranInfo.getExpire_dt());
            cnclobj.setCard_cate(tranInfo.getCard_cate());
            cnclobj.setOrder_seq(reqpgobj.getOrder_no());
            //cnclobj.setTran_dt(tranInfo.getTran_dt());
            //cnclobj.setTran_tm(tranInfo.getTran_tm());
            cnclobj.setTran_dt(strCurYear.substring(0, 8));
            cnclobj.setTran_tm(strCurYear.substring(8, 14));                        
            cnclobj.setTran_cate("40");//취소
            cnclobj.setFree_inst_flag(tranInfo.getFree_inst_flag());
            cnclobj.setTax_amt(tranInfo.getTax_amt());//부가세금액
            cnclobj.setSvc_amt(tranInfo.getSvc_amt());//비과세승인금액
            cnclobj.setInstallment(tranInfo.getInstallment());//할부

            cnclobj.setMerch_no(tranInfo.getMerch_no());
            cnclobj.setOrg_merch_no(tranInfo.getOrg_merch_no());
            cnclobj.setResult_cd(res_cd);
            cnclobj.setResult_msg(res_msg);

            cnclobj.setOrg_app_dd(tranInfo.getApp_dt().replaceAll("-", ""));
            cnclobj.setOrg_app_no(tranInfo.getApp_no());
            cnclobj.setOrg_pg_seq(tranInfo.getPg_seq());
            cnclobj.setCncl_reason(reqpgobj.getMgr_msg());


            cnclobj.setOnofftid_pay_amt(tranInfo.getOnofftid_pay_amt());
            cnclobj.setOnofftid_commision(tranInfo.getOnofftid_commision());
            cnclobj.setClient_ip(reqpgobj.getClient_ip());

            cnclobj.setTrad_chk_flag("0");//거래대사

            cnclobj.setIns_user(appFormBean.getUser_seq());
            cnclobj.setMod_user(appFormBean.getUser_seq());

            Integer int_reversdbresult = appDAO.InsertCardPgTran(cnclobj);

            logger.trace("------------------reverse tran Dbinsert start------------------" );
            logger.trace("int_reversdbresult : " + int_reversdbresult);
            logger.trace("------------------reverse tran Dbinsert End ------------------" );                

            //성공시 거래내역을 UPDATE한다.
            if (res_cd.equals("0000"))
            {
                        //r거래에대한 원거래를 Update 한다.
                        InsertCardPgTranBean orgcnclobj = new InsertCardPgTranBean();
                        orgcnclobj.setTran_seq(tranInfo.getTran_seq());
                        orgcnclobj.setInstablenm(tranInfo.getTb_nm());
                        orgcnclobj.setCncl_reason(reqpgobj.getMgr_msg());
                        
                        String strTpCnclDate = r_canc_date.substring(0, 8);      
                        
                        String strTpOrgdt = tranInfo.getApp_dt().replaceAll("-", "");
                        //당일취소,익일취소 구분
                        if(strTpCnclDate.equals(strTpOrgdt))
                        {
                            orgcnclobj.setAcc_chk_flag("3");//매입비대상
                            orgcnclobj.setTran_status("10");//당일취소
                        }
                        else
                        {
                            //orgcnclobj.setAcc_chk_flag("0");//매입대상
                            orgcnclobj.setTran_status("20");//당일취소                            
                        }
                        
                        orgcnclobj.setCncl_dt(cnclobj.getApp_dt());
                        orgcnclobj.setMod_user(appFormBean.getUser_seq());

                        Integer int_reversdbupdate = appDAO.OrgCardPgTranUpdate(orgcnclobj);

                        logger.trace("------------------reverse orgtran update start------------------" );
                        logger.trace("int_reversdbupdate : " + int_reversdbupdate);
                        logger.trace("------------------reverse orgtran update End ------------------" );                

            }
        }
        //현금영수증일 경우 
        else
        {
             //r거래값을 입력한다.
            InsertCardPgTranBean cnclobj = new InsertCardPgTranBean();
            //승인 결가에 따라 저장을 위한 테이블명을 만든다.

            //가맹점 DB 처리에러시 
            String strTpCnclTranseq = appDAO.GetTranSeq();
            cnclobj.setTran_seq(strTpCnclTranseq);// 거래번호 셋팅

            //취소성공시
            if (res_cd.equals("0000"))
            {
                cnclobj.setInstablenm("TB_TRAN_CASHPG_"+strCurYear.substring(2, 6));
                cnclobj.setApp_dt(strCurYear.substring(0, 8));
                cnclobj.setApp_tm(strCurYear.substring(8, 14));
                cnclobj.setResult_status("00");
                
                cnclobj.setTran_step("00");
                cnclobj.setCncl_dt(strCurYear.substring(0, 8));
                        
                String strTpCnclDate = strCurYear.substring(0, 8); 
                
                 String strTpOrgdt = tranInfo.getApp_dt().replaceAll("-", "");
                //당일취소,익일취소 구분
                if(strTpCnclDate.equals(strTpOrgdt))
                {
                   cnclobj.setTran_status("10");//승인 :00 ,10:당일취소 , 20:매입취소
                   cnclobj.setAcc_chk_flag("3");//당일취소는 매입미대상
                }
                else
                {
                   cnclobj.setTran_status("20");//승인 :00 ,10:당일취소 , 20:매입취소
                   cnclobj.setAcc_chk_flag("0");//당일취소는 매입미대상                    
                }
                
            }
            else
            {
                cnclobj.setInstablenm("TB_TRAN_CASHPG_"+strCurYear.substring(2, 6));
                cnclobj.setResult_status("99");                        
                cnclobj.setAcc_chk_flag("3");//r거래는3
                String strTpOrgdt = tranInfo.getApp_dt().replaceAll("-", "");
                //당일취소,익일취소 구분
                if(strCurYear.substring(0, 8).equals(strTpOrgdt))
                {
                   cnclobj.setTran_status("10");//승인 :00 ,10:당일취소 , 20:매입취소
                }
                else
                {
                   cnclobj.setTran_status("20");//승인 :00 ,10:당일취소 , 20:매입취소
                }
            }

            cnclobj.setMassagetype("40");//메세지타입
            cnclobj.setCard_num(tranInfo.getCard_num());//카드번호
            cnclobj.setApp_no(r_auth_no);//승인번호
            cnclobj.setTot_amt(tranInfo.getTot_amt());//결제총금액
            cnclobj.setPg_seq(r_cno);//pg거래고유번호
            cnclobj.setPay_chn_cate(tranInfo.getPay_chn_cate());
            cnclobj.setTid(g_mall_id);
            cnclobj.setOnffmerch_no(tranInfo.getOnffmerch_no());        
            cnclobj.setOnfftid(tranInfo.getOnfftid());
            cnclobj.setWcc(tranInfo.getWcc());
            cnclobj.setExpire_dt(tranInfo.getExpire_dt());
            cnclobj.setCard_cate(tranInfo.getCard_cate());
            cnclobj.setOrder_seq(reqpgobj.getOrder_no());
            //cnclobj.setTran_dt(tranInfo.getTran_dt());
            //cnclobj.setTran_tm(tranInfo.getTran_tm());
            cnclobj.setTran_dt(strCurYear.substring(0, 8));
            cnclobj.setTran_tm(strCurYear.substring(8, 14));  
            cnclobj.setTran_cate("51");//취소
            cnclobj.setFree_inst_flag(tranInfo.getFree_inst_flag());
            cnclobj.setTax_amt(tranInfo.getTax_amt());//부가세금액
            cnclobj.setSvc_amt(tranInfo.getSvc_amt());//비과세승인금액
            cnclobj.setInstallment(tranInfo.getInstallment());//할부

            cnclobj.setMerch_no(tranInfo.getMerch_no());
            cnclobj.setOrg_merch_no(tranInfo.getOrg_merch_no());
            cnclobj.setResult_cd(res_cd);
            cnclobj.setResult_msg(res_msg);

            cnclobj.setOrg_app_dd(tranInfo.getApp_dt().replaceAll("-", ""));
            cnclobj.setOrg_app_no(tranInfo.getApp_no());
            cnclobj.setOrg_pg_seq(tranInfo.getPg_seq());
            cnclobj.setCncl_reason(reqpgobj.getMgr_msg());


            cnclobj.setOnofftid_pay_amt(tranInfo.getOnofftid_pay_amt());
            cnclobj.setOnofftid_commision(tranInfo.getOnofftid_commision());
            cnclobj.setClient_ip(reqpgobj.getClient_ip());

            cnclobj.setTrad_chk_flag("0");//거래대사

            cnclobj.setIns_user(appFormBean.getUser_seq());
            cnclobj.setMod_user(appFormBean.getUser_seq());

            Integer int_reversdbresult = appDAO.InsertCashPgTran(cnclobj);

            logger.trace("------------------reverse tran Dbinsert start------------------" );
            logger.trace("int_reversdbresult : " + int_reversdbresult);
            logger.trace("------------------reverse tran Dbinsert End ------------------" );                

            //성공시 거래내역을 UPDATE한다.
            if (res_cd.equals("0000"))
            {
                        //r거래에대한 원거래를 Update 한다.
                        InsertCardPgTranBean orgcnclobj = new InsertCardPgTranBean();
                        orgcnclobj.setTran_seq(tranInfo.getTran_seq());
                        orgcnclobj.setInstablenm(tranInfo.getTb_nm());
                        orgcnclobj.setCncl_reason(reqpgobj.getMgr_msg());
                        
                        String strTpCnclDate = strCurYear.substring(0, 8);      
                        //당일취소,익일취소 구분
                        
                         String strTpOrgdt = tranInfo.getApp_dt().replaceAll("-", "");
                        if(strTpCnclDate.equals(strTpOrgdt))
                        {
                            orgcnclobj.setAcc_chk_flag("3");//매입비대상
                            orgcnclobj.setTran_status("10");//당일취소
                        }
                        else
                        {
                            //orgcnclobj.setAcc_chk_flag("0");//매입대상
                            orgcnclobj.setTran_status("20");//매입취소                     
                        }
                        
                        orgcnclobj.setCncl_dt(strCurYear.substring(0, 8));
                        orgcnclobj.setMod_user(appFormBean.getUser_seq());
                        
                        logger.trace("------------------cncl cash orgtran update start------------------" );
                        logger.trace("orgcnclobj.getTran_seq : " + orgcnclobj.getTran_seq());
                        logger.trace("orgcnclobj.getInstablenm : " + orgcnclobj.getInstablenm());
                        logger.trace("orgcnclobj.getInstablenm : " + orgcnclobj.getInstablenm());
                        logger.trace("orgcnclobj.getCncl_reason : " + orgcnclobj.getCncl_reason());
                        
                         logger.trace("orgcnclobj.getAcc_chk_flag : " + orgcnclobj.getAcc_chk_flag());
                        logger.trace("orgcnclobj.setTran_status : " + orgcnclobj.getTran_status());
                        
                        logger.trace("orgcnclobj.getCncl_dt : " + orgcnclobj.getCncl_dt());
                        logger.trace("orgcnclobj.getMod_user : " + orgcnclobj.getMod_user());
                        logger.trace("tranInfo.getApp_dt() : " + tranInfo.getApp_dt());
                        logger.trace("strTpCnclDate : " + strTpCnclDate);
                        
                        logger.trace("------------------cncl cash orgtran update End ------------------" );     

                        Integer int_reversdbupdate = appDAO.OrgCashPgTranUpdate(orgcnclobj);

                        logger.trace("------------------reverse orgtran update start------------------" );
                        logger.trace("int_reversdbupdate : " + int_reversdbupdate);
                        logger.trace("------------------reverse orgtran update End ------------------" );                

            }
        }
        
        ht.put("RESULT_CD", res_cd);
        ht.put("RESULT_MSG", res_msg); 
        
        return ht;
    }


    @Override
    @Transactional
    public Hashtable appMgrKcpAction(AppFormBean appFormBean,Tb_Tran_CardPg tranInfo) throws Exception {
        Hashtable ht = new Hashtable();
        
        KiccPgVo reqRefobj =  appFormBean.getReqKiccPgVo();
        KcpPgVo reqpgobj = appFormBean.getReqKcpPgVo();
        KcpPgVo respgobj = new KcpPgVo();        
        
        Date today=new Date();
        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmss");       
        String strCurYear = formater.format(today);

	// ============================================================================== /
	// =   02. 쇼핑몰 지불 정보 설정                                                = /
	// = -------------------------------------------------------------------------- = /	
        int int_g_log_level  = Integer.parseInt(reqpgobj.getG_conf_log_level());
        
        
	String g_mall_id   = tranInfo.getTid();             // 리얼 반영시 KCP에 발급된 mall_id 사용
        String g_mall_pwd = appDAO.GetTerminalPwdInfo(g_mall_id);
        String g_mall_name = tranInfo.getComp_nm();    
        
        if(g_mall_id == null || g_mall_id.equals(""))
        {
            throw new Exception("결제용TID가 없습니다.");
        }
        
	logger.trace("------------------------------appMgrKcpAction--------------------------------------");
        logger.trace("g_mall_id : " + g_mall_id);
        logger.trace("g_mall_name : " + g_mall_name);
        logger.trace("--------------------------------------------------------------------");        
        
        //인스터스생성 초기화
        C_PP_CLI c_PayPlus = new C_PP_CLI();

        //c_PayPlus.mf_init( g_conf_home_dir, g_conf_gw_url, g_conf_gw_port, g_conf_key_dir, g_conf_log_dir, g_conf_tx_mode );
        int inttptxmod = Integer.parseInt(reqpgobj.getG_conf_tx_mode());
        //window
        //c_PayPlus.mf_init( reqpgobj.getG_conf_home_dir(), reqpgobj.getG_conf_gw_url(), reqpgobj.getG_conf_gw_port(), reqpgobj.getG_conf_key_dir(), reqpgobj.getG_conf_log_dir(), inttptxmod );
        //linux
        c_PayPlus.mf_init( reqpgobj.getG_conf_home_dir(), reqpgobj.getG_conf_gw_url(), reqpgobj.getG_conf_gw_port(), inttptxmod );
        c_PayPlus.mf_init_set();

            /* -------------------------------------------------------------------------- */
            /* ::: 결제 결과                                                              */
            /* -------------------------------------------------------------------------- */
            String bDBProc          = "";
            String res_cd           = "";
            String res_msg          = "";
            String r_order_no       = "";
            String r_complex_yn     = "";
            String r_msg_type       = "";     //거래구분
            String r_noti_type	    = "";     //노티구분
            String r_cno            = "";     //PG거래번호
            String r_amount         = "";     //총 결제금액
            String r_auth_no        = "";     //승인번호
            String r_tran_date      = "";     //거래일시
            String r_card_no        = "";     //카드번호
            String r_issuer_cd      = "";     //발급사코드
            String r_issuer_nm      = "";     //발급사명
            String r_acquirer_cd    = "";     //매입사코드
            String r_acquirer_nm    = "";     //매입사명
            String r_install_period = "";     //할부개월
            String r_noint          = "";     //무이자여부
            String r_expire_date    = "";     //계좌사용 만료일
            String r_cash_res_cd    = "";     //현금영수증 결과코드
            String r_cash_res_msg   = "";     //현금영수증 결과메세지
            String r_cash_auth_no   = "";     //현금영수증 승인번호
            String r_cash_tran_date = "";     //현금영수증 승인일시
            String r_auth_id        = "";     //PhoneID
            String r_billid         = "";     //인증번호
            String r_mobile_no      = "";     //휴대폰번호
            String r_ars_no         = "";     //ARS 전화번호
            String r_canc_acq_data  = "";     //매입취소일시
            String r_canc_date      = "";     //취소일시
            String r_refund_date    = "";     //환불예정일시
            String r_join_no = ""; //카드사가맹점번호
            
            String cash_no      = "" ;                                                     // 현금영수증 거래번호
            String receipt_no   = "" ;                                                     // 현금영수증 승인번호
            String app_time     = "" ;                                                     // 승인시간(YYYYMMDDhhmmss)
            String reg_stat     = "" ;                                                     // 등록 상태 코드
            String reg_desc     = "" ;                                                     // 등록 상태 설명            
            
            //가맹점주문번호를 생성한다.
            String strOrder_seq = appDAO.GetOrderSeq();
            reqpgobj.setOrdr_idxx(strOrder_seq);
        
            logger.trace("----------------------------------appserviceimple mgrkcpaction------------------------------------");
            logger.trace(" reqpgobj.getOrg_cno() " +  reqpgobj.getOrg_cno());
            logger.trace(" reqpgobj.getOrder_no() " +  reqpgobj.getOrdr_idxx());
            logger.trace(" appFormBean.getUser_id() " +  appFormBean.getUser_id() );
            logger.trace(" reqpgobj.getMgr_msg() " +  reqpgobj.getMod_desc());            
            logger.trace("----------------------------------------------------------------------");
            
        //카드취소일 경우
        if(reqRefobj.getMgr_txtype().equals("40"))
        {    
            /* -------------------------------------------------------------------------- */
            /* ::: 변경관리 요청                                                          */
            /* -------------------------------------------------------------------------- */
                int     mod_data_set_no;
                //tx_cd = "00200000";
                mod_data_set_no = c_PayPlus.mf_add_set( "mod_data" );

                c_PayPlus.mf_set_us( mod_data_set_no, "tno",        tranInfo.getPg_seq() );      // KCP 원거래 거래번호
                c_PayPlus.mf_set_us( mod_data_set_no, "mod_type",   reqpgobj.getMod_type()      );      // 원거래 변경 요청 종류
                c_PayPlus.mf_set_us( mod_data_set_no, "mod_ip",     reqpgobj.getCust_ip()       );      // 변경 요청자 IP
                c_PayPlus.mf_set_us( mod_data_set_no, "mod_desc",   reqpgobj.getMod_desc() );      // 변경 사유
        }
        //현금영수증일 경우
        else
        {
            int     mod_data_set_no ;

            mod_data_set_no = c_PayPlus.mf_add_set( "mod_data" ) ;

            /*
            if( mod_type.equals( "STSQ" ) )
            {
                tx_cd = "07030000" ;     // 조회 요청
            }
            else
            {
                tx_cd = "07020000" ;     // 취소 요청
            }

            if( mod_type.equals( "STPC" ) )     // 부분 취소
            {
                c_PayPlus.mf_set_us( mod_data_set_no, "mod_mny"  , mod_mny ) ;
                c_PayPlus.mf_set_us( mod_data_set_no, "rem_mny"  , rem_mny ) ;
            }
            */
            
            reqpgobj.setTx_cd("07020000");

            c_PayPlus.mf_set_us( mod_data_set_no, "mod_type"  , reqpgobj.getMod_type()  ) ;
            c_PayPlus.mf_set_us( mod_data_set_no, "mod_value" , tranInfo.getApp_no() ) ;
            c_PayPlus.mf_set_us( mod_data_set_no, "mod_gubn"  , "MG02"  ) ;//mod_value 종류 승인번호로 고정
            c_PayPlus.mf_set_us( mod_data_set_no, "trad_time" , tranInfo.getTran_dt()+tranInfo.getTran_tm() ) ;
        }

        /* -------------------------------------------------------------------------- */
        /* ::: 실행                                                                   */
        /* -------------------------------------------------------------------------- */
        if ( Util.nullToString(reqpgobj.getTx_cd()).length() > 0 )
        {
            c_PayPlus.mf_do_tx( g_mall_id, g_mall_pwd, reqpgobj.getTx_cd(), reqpgobj.getCust_ip(), reqpgobj.getOrdr_idxx(), Util.getNullToZero(reqpgobj.getG_conf_log_level()), "1" );
        }
        else
        {
            c_PayPlus.m_res_cd  = "9562";
            c_PayPlus.m_res_msg = "연동 오류";
        }
        res_cd  = c_PayPlus.m_res_cd;                      // 결과 코드
        //res_msg = Util.fromEuctoUtf(c_PayPlus.m_res_msg);                     // 결과 메시지1004
        res_msg = c_PayPlus.m_res_msg;        
        
	logger.trace("--------------------------------------------------------------------");
        logger.trace("res_cd : " + res_cd);
        logger.trace("res_msg : " + res_msg);
        logger.trace("--------------------------------------------------------------------");     
        
        /* -------------------------------------------------------------------------- */
        /* ::: 결과 처리                                                              */
        /* -------------------------------------------------------------------------- */
        r_cno             = Util.nullToString(tranInfo.getPg_seq() );    // PG거래번호
        r_amount          = Util.nullToString(c_PayPlus.mf_get_res( "amount"       ));    //총 결제금액
        r_canc_date       = Util.nullToString(c_PayPlus.mf_get_res( "canc_time"       ));    //취소시간
        
        cash_no    = Util.nullToString(c_PayPlus.mf_get_res( "cash_no"    )) ;  // 현금영수증 거래번호
        receipt_no = Util.nullToString(c_PayPlus.mf_get_res( "receipt_no" )) ;  // 현금영수증 승인번호
        app_time   = Util.nullToString(c_PayPlus.mf_get_res( "app_time"   )) ;  // 승인시간(YYYYMMDDhhmmss)
        reg_stat   = Util.nullToString(c_PayPlus.mf_get_res( "reg_stat"   )) ;  // 등록 상태 코드
        reg_desc   = Util.nullToString(c_PayPlus.mf_get_res( "reg_desc"   )) ;  // 등록 상태 설명
        
        
        logger.trace("--------------------------------------------------------------------");        
        logger.trace("r_cno            "+       r_cno             );// PG거래번호
        logger.trace("r_amount         "+       r_amount          );//총 결제금액
        logger.trace("r_order_no       "+       r_order_no        );//주문번호
        logger.trace("r_noti_type      "+       r_noti_type       );//노티구분
        logger.trace("r_auth_no        "+       r_auth_no         );//승인번호
        logger.trace("r_tran_date      "+       r_tran_date       );//승인일시
        
        logger.trace("r_card_no        "+       r_card_no         );//카드번호
        logger.trace("r_issuer_cd      "+       r_issuer_cd       );//발급사코드
        logger.trace("r_issuer_nm      "+       r_issuer_nm       );//발급사명
        logger.trace("r_acquirer_cd    "+       r_acquirer_cd     );//매입사코드
        logger.trace("r_acquirer_nm    "+       r_acquirer_nm     );//매입사명
        logger.trace("r_install_period "+       r_install_period  );//할부개월
        logger.trace("r_noint          "+       r_noint           );//무이자여부
        logger.trace("r_expire_date    "+       r_expire_date     );//계좌사용만료일
        logger.trace("r_cash_res_cd    "+       r_cash_res_cd     );//현금영수증 결과코드
        logger.trace("r_cash_res_msg   "+       r_cash_res_msg    );//현금영수증 결과메세지
        logger.trace("r_cash_auth_no   "+       r_cash_auth_no    );//현금영수증 승인번호
        logger.trace("r_cash_tran_date "+       r_cash_tran_date  );//현금영수증 승인일시
        logger.trace("r_auth_id        "+       r_auth_id         );//PhoneID
        logger.trace("r_billid         "+       r_billid          );//인증번호
        logger.trace("r_mobile_no      "+       r_mobile_no       );//휴대폰번호
        logger.trace("r_ars_no         "+       r_ars_no          );//전화번호
        logger.trace("r_complex_yn     "+       r_complex_yn      );//복합결제 유무
        logger.trace("r_canc_acq_data  "+       r_canc_acq_data   );//매입취소일시
        logger.trace("r_canc_date      "+       r_canc_date       );//취소일시
        logger.trace("r_refund_date    "+       r_refund_date     );//환불예정일시
        logger.trace("r_join_no        "+       r_join_no     );//카드사가맹점번호
        logger.trace("--------------------------------------------------------------------");
    
        respgobj.setTno(r_cno            );
        respgobj.setGood_mny(r_amount         );
        respgobj.setOrdr_idxx(r_order_no       );
        /*
        respgobj.setR_auth_no         (r_auth_no        );
        respgobj.setR_tran_date       (r_tran_date      );
        
        respgobj.setR_canc_acq_data   (r_canc_acq_data  );
        */
        respgobj.setApp_time(r_canc_date      );
        /*
        respgobj.setR_refund_date     (r_refund_date    );
        respgobj.setR_join_no     (r_join_no    );
        */
        //가맹점 DB처리
        //결제수단 즉시취소
        if(reqRefobj.getMgr_txtype().equals("40"))
        {
            //r거래값을 입력한다.
            InsertCardPgTranBean cnclobj = new InsertCardPgTranBean();
            //승인 결가에 따라 저장을 위한 테이블명을 만든다.
    
            //가맹점 DB 처리에러시 
            String strTpCnclTranseq = appDAO.GetTranSeq();
            cnclobj.setTran_seq(strTpCnclTranseq);// 거래번호 셋팅

            //취소성공시
            if (res_cd.equals("0000"))
            {
                cnclobj.setInstablenm("TB_TRAN_CARDPG_"+r_canc_date.substring(2, 6));
                cnclobj.setApp_dt(r_canc_date.substring(0, 8));
                cnclobj.setApp_tm(r_canc_date.substring(8, 14));
                cnclobj.setResult_status("00");
                
                cnclobj.setTran_step("00");
                cnclobj.setCncl_dt(r_canc_date.substring(0, 8));
                        
                String strTpCnclDate = r_canc_date.substring(0, 8); 
                //당일취소,익일취소 구분
                String strTpOrgdt = tranInfo.getApp_dt().replaceAll("-", "");
                if(strTpCnclDate.equals(strTpOrgdt))
                {
                   cnclobj.setTran_status("10");//승인 :00 ,10:당일취소 , 20:매입취소
                   cnclobj.setAcc_chk_flag("3");//당일취소는 매입미대상
                }
                else
                {
                   cnclobj.setTran_status("20");//승인 :00 ,10:당일취소 , 20:매입취소
                   cnclobj.setAcc_chk_flag("0");//당일취소는 매입미대상                    
                }
                
            }
            else
            {
                cnclobj.setInstablenm("TB_TRAN_CARDPG_"+strCurYear.substring(2, 6));
                cnclobj.setResult_status("99");                        
                cnclobj.setAcc_chk_flag("3");//r거래는3
                
                //당일취소,익일취소 구분
                String strTpOrgdt = tranInfo.getApp_dt().replaceAll("-", "");
                if(strCurYear.substring(0, 8).equals(strTpOrgdt))
                {
                   cnclobj.setTran_status("10");//승인 :00 ,10:당일취소 , 20:매입취소
                }
                else
                {
                   cnclobj.setTran_status("20");//승인 :00 ,10:당일취소 , 20:매입취소
                }
            }

            cnclobj.setMassagetype("40");//메세지타입
            cnclobj.setCard_num(tranInfo.getCard_num());//카드번호
            cnclobj.setApp_card_num(tranInfo.getApp_card_num());//카드번호
            cnclobj.setApp_no(r_auth_no);//승인번호
            cnclobj.setTot_amt(tranInfo.getTot_amt());//결제총금액
            cnclobj.setPg_seq(r_cno);//pg거래고유번호
            cnclobj.setPay_chn_cate(tranInfo.getPay_chn_cate());
            cnclobj.setTid(g_mall_id);
            cnclobj.setOnffmerch_no(tranInfo.getOnffmerch_no());        
            cnclobj.setOnfftid(tranInfo.getOnfftid());
            cnclobj.setWcc(tranInfo.getWcc());
            cnclobj.setExpire_dt(tranInfo.getExpire_dt());
            cnclobj.setCard_cate(tranInfo.getCard_cate());
            cnclobj.setOrder_seq(reqpgobj.getOrdr_idxx());
            //cnclobj.setTran_dt(tranInfo.getTran_dt());
            //cnclobj.setTran_tm(tranInfo.getTran_tm());
            cnclobj.setTran_dt(strCurYear.substring(0, 8));
            cnclobj.setTran_tm(strCurYear.substring(8, 14));            
            cnclobj.setTran_cate("40");//취소
            cnclobj.setFree_inst_flag(tranInfo.getFree_inst_flag());
            cnclobj.setTax_amt(tranInfo.getTax_amt());//부가세금액
            cnclobj.setSvc_amt(tranInfo.getSvc_amt());//비과세승인금액
            cnclobj.setInstallment(tranInfo.getInstallment());//할부

            cnclobj.setMerch_no(tranInfo.getMerch_no());
            cnclobj.setOrg_merch_no(tranInfo.getOrg_merch_no());
            cnclobj.setResult_cd(res_cd);
            cnclobj.setResult_msg(res_msg);

            cnclobj.setOrg_app_dd(tranInfo.getApp_dt().replaceAll("-", ""));
            cnclobj.setOrg_app_no(tranInfo.getApp_no());
            cnclobj.setOrg_pg_seq(tranInfo.getPg_seq());
            cnclobj.setCncl_reason(reqpgobj.getMod_desc());


            cnclobj.setOnofftid_pay_amt(tranInfo.getOnofftid_pay_amt());
            cnclobj.setOnofftid_commision(tranInfo.getOnofftid_commision());
            cnclobj.setClient_ip(reqpgobj.getCust_ip());

            cnclobj.setTrad_chk_flag("0");//거래대사

            cnclobj.setIns_user(appFormBean.getUser_seq());
            cnclobj.setMod_user(appFormBean.getUser_seq());

            //20150902 취소거래 있을시 입력 패치            
            //Integer int_reversdbresult = appDAO.InsertCardPgTran(cnclobj);
            //logger.debug("-------------cncl double chk start-----------------");
            AppFormBean cnclchkFormBean = new AppFormBean();
            
            cnclchkFormBean.setChkpgseq(r_cno);
            cnclchkFormBean.setChkpgtid(g_mall_id);
            
            String strTpCnclChk = Util.nullToString(appDAO.getOnffCnclChkCnt(cnclchkFormBean));
            
            if(strTpCnclChk.equals("")) 
            {
                strTpCnclChk="0";
            }
            
            logger.trace("strTpCnclChk : " + strTpCnclChk);
            
            Integer int_reversdbresult = -1;
            
            if(strTpCnclChk.equals("0"))
            {
                int_reversdbresult = appDAO.InsertCardPgTran(cnclobj);
            }
            else
            {
                logger.info("취소성공거래 기존재");
                int_reversdbresult = 1;
            }
            //logger.debug("-------------cncl double chk end-----------------");
            logger.trace("------------------ tran Dbinsert start------------------" );
            logger.trace("int_reversdbresult : " + int_reversdbresult);
            logger.trace("------------------ tran Dbinsert End ------------------" );                

            //성공시 거래내역을 UPDATE한다.
            if (res_cd.equals("0000"))
            {
                        //r거래에대한 원거래를 Update 한다.
                        InsertCardPgTranBean orgcnclobj = new InsertCardPgTranBean();
                        orgcnclobj.setTran_seq(tranInfo.getTran_seq());
                        orgcnclobj.setInstablenm(tranInfo.getTb_nm());
                        orgcnclobj.setCncl_reason(reqpgobj.getMod_desc());
                        
                        String strTpCnclDate = r_canc_date.substring(0, 8);      
                        
                        String strTpOrgdt = tranInfo.getApp_dt().replaceAll("-", "");
                        //당일취소,익일취소 구분
                        if(strTpCnclDate.equals(strTpOrgdt))
                        {
                            orgcnclobj.setAcc_chk_flag("3");//매입비대상
                            orgcnclobj.setTran_status("10");//당일취소
                        }
                        else
                        {
                            //orgcnclobj.setAcc_chk_flag("0");//매입대상
                            orgcnclobj.setTran_status("20");//당일취소                            
                        }
                        
                        orgcnclobj.setCncl_dt(cnclobj.getApp_dt());
                        orgcnclobj.setMod_user(appFormBean.getUser_seq());

                        Integer int_reversdbupdate = appDAO.OrgCardPgTranUpdate(orgcnclobj);

                        logger.trace("------------------reverse orgtran update start------------------" );
                        logger.trace("int_reversdbupdate : " + int_reversdbupdate);
                        logger.trace("------------------reverse orgtran update End ------------------" );                

            }
        }
        //현금영수증일 경우 
        else
        {
            /*
                cash_no    = Util.nullToString(c_PayPlus.mf_get_res( "cash_no"    )) ;  // 현금영수증 거래번호
                receipt_no = Util.nullToString(c_PayPlus.mf_get_res( "receipt_no" )) ;  // 현금영수증 승인번호
                app_time   = Util.nullToString(c_PayPlus.mf_get_res( "app_time"   )) ;  // 승인시간(YYYYMMDDhhmmss)
                reg_stat   = Util.nullToString(c_PayPlus.mf_get_res( "reg_stat"   )) ;  // 등록 상태 코드
                reg_desc   = Util.nullToString(c_PayPlus.mf_get_res( "reg_desc"   )) ;  // 등록 상태 설명
            */
            
            
             //r거래값을 입력한다.
            InsertCardPgTranBean cnclobj = new InsertCardPgTranBean();
            //승인 결가에 따라 저장을 위한 테이블명을 만든다.

            //가맹점 DB 처리에러시 
            String strTpCnclTranseq = appDAO.GetTranSeq();
            cnclobj.setTran_seq(strTpCnclTranseq);// 거래번호 셋팅

            //취소성공시
            if (res_cd.equals("0000"))
            {
                cnclobj.setInstablenm("TB_TRAN_CASHPG_"+strCurYear.substring(2, 6));
                //cnclobj.setApp_dt(strCurYear.substring(0, 8));
                //cnclobj.setApp_tm(strCurYear.substring(8, 14));
                cnclobj.setApp_dt(app_time.substring(0, 8));
                cnclobj.setApp_tm(app_time.substring(8, 14));                
                
                cnclobj.setResult_status("00");
                
                cnclobj.setTran_step("00");
                //cnclobj.setCncl_dt(strCurYear.substring(0, 8));
                cnclobj.setCncl_dt(app_time.substring(0, 8));
                        
                String strTpCnclDate = strCurYear.substring(0, 8); 
                
                 String strTpOrgdt = tranInfo.getApp_dt().replaceAll("-", "");
                //당일취소,익일취소 구분
                if(strTpCnclDate.equals(strTpOrgdt))
                {
                   cnclobj.setTran_status("10");//승인 :00 ,10:당일취소 , 20:매입취소
                   cnclobj.setAcc_chk_flag("3");//당일취소는 매입미대상
                }
                else
                {
                   cnclobj.setTran_status("20");//승인 :00 ,10:당일취소 , 20:매입취소
                   cnclobj.setAcc_chk_flag("0");//당일취소는 매입미대상                    
                }
                
            }
            else
            {
                cnclobj.setInstablenm("TB_TRAN_CASHPG_"+strCurYear.substring(2, 6));
                cnclobj.setResult_status("99");                        
                cnclobj.setAcc_chk_flag("3");//r거래는3
                String strTpOrgdt = tranInfo.getApp_dt().replaceAll("-", "");
                //당일취소,익일취소 구분
                if(strCurYear.substring(0, 8).equals(strTpOrgdt))
                {
                   cnclobj.setTran_status("10");//승인 :00 ,10:당일취소 , 20:매입취소
                }
                else
                {
                   cnclobj.setTran_status("20");//승인 :00 ,10:당일취소 , 20:매입취소
                }
            }

            cnclobj.setMassagetype("40");//메세지타입
            cnclobj.setCard_num(tranInfo.getCard_num());//카드번호
            cnclobj.setApp_no(receipt_no);//승인번호
            cnclobj.setTot_amt(tranInfo.getTot_amt());//결제총금액
            cnclobj.setPg_seq(r_cno);//pg거래고유번호
            cnclobj.setPay_chn_cate(tranInfo.getPay_chn_cate());
            cnclobj.setTid(g_mall_id);
            cnclobj.setOnffmerch_no(tranInfo.getOnffmerch_no());        
            cnclobj.setOnfftid(tranInfo.getOnfftid());
            cnclobj.setWcc(tranInfo.getWcc());
            cnclobj.setExpire_dt(tranInfo.getExpire_dt());
            cnclobj.setCard_cate(tranInfo.getCard_cate());
            cnclobj.setOrder_seq(reqpgobj.getOrdr_idxx());
            cnclobj.setTran_dt(strCurYear.substring(0, 8));
            cnclobj.setTran_tm(strCurYear.substring(8, 14));
            cnclobj.setTran_cate("51");//취소
            cnclobj.setFree_inst_flag(tranInfo.getFree_inst_flag());
            cnclobj.setTax_amt(tranInfo.getTax_amt());//부가세금액
            cnclobj.setSvc_amt(tranInfo.getSvc_amt());//비과세승인금액
            cnclobj.setInstallment(tranInfo.getInstallment());//할부

            cnclobj.setMerch_no(tranInfo.getMerch_no());
            cnclobj.setOrg_merch_no(tranInfo.getOrg_merch_no());
            cnclobj.setResult_cd(res_cd);
            cnclobj.setResult_msg(res_msg);

            cnclobj.setOrg_app_dd(tranInfo.getApp_dt().replaceAll("-", ""));
            cnclobj.setOrg_app_no(tranInfo.getApp_no());
            cnclobj.setOrg_pg_seq(tranInfo.getPg_seq());
            cnclobj.setCncl_reason(reqpgobj.getMod_desc());


            cnclobj.setOnofftid_pay_amt(tranInfo.getOnofftid_pay_amt());
            cnclobj.setOnofftid_commision(tranInfo.getOnofftid_commision());
            cnclobj.setClient_ip(reqpgobj.getCust_ip());

            cnclobj.setTrad_chk_flag("0");//거래대사

            cnclobj.setIns_user(appFormBean.getUser_seq());
            cnclobj.setMod_user(appFormBean.getUser_seq());

            Integer int_reversdbresult = appDAO.InsertCashPgTran(cnclobj);

            logger.trace("------------------reverse tran Dbinsert start------------------" );
            logger.trace("int_reversdbresult : " + int_reversdbresult);
            logger.trace("------------------reverse tran Dbinsert End ------------------" );                

            //성공시 거래내역을 UPDATE한다.
            if (res_cd.equals("0000"))
            {
                        //r거래에대한 원거래를 Update 한다.
                        InsertCardPgTranBean orgcnclobj = new InsertCardPgTranBean();
                        orgcnclobj.setTran_seq(tranInfo.getTran_seq());
                        orgcnclobj.setInstablenm(tranInfo.getTb_nm());
                        orgcnclobj.setCncl_reason(reqpgobj.getMod_desc());
                        
                        String strTpCnclDate = strCurYear.substring(0, 8);      
                        //당일취소,익일취소 구분
                        
                         String strTpOrgdt = tranInfo.getApp_dt().replaceAll("-", "");
                        if(strTpCnclDate.equals(strTpOrgdt))
                        {
                            orgcnclobj.setAcc_chk_flag("3");//매입비대상
                            orgcnclobj.setTran_status("10");//당일취소
                        }
                        else
                        {
                            //orgcnclobj.setAcc_chk_flag("0");//매입대상
                            orgcnclobj.setTran_status("20");//매입취소                     
                        }
                        
                        //orgcnclobj.setCncl_dt(strCurYear.substring(0, 8));
                        orgcnclobj.setCncl_dt(app_time.substring(0, 8));
                        orgcnclobj.setMod_user(appFormBean.getUser_seq());
                        
                        logger.trace("------------------cncl cash orgtran update start------------------" );
                        logger.trace("orgcnclobj.getTran_seq : " + orgcnclobj.getTran_seq());
                        logger.trace("orgcnclobj.getInstablenm : " + orgcnclobj.getInstablenm());
                        logger.trace("orgcnclobj.getInstablenm : " + orgcnclobj.getInstablenm());
                        logger.trace("orgcnclobj.getCncl_reason : " + orgcnclobj.getCncl_reason());
                        
                         logger.trace("orgcnclobj.getAcc_chk_flag : " + orgcnclobj.getAcc_chk_flag());
                        logger.trace("orgcnclobj.setTran_status : " + orgcnclobj.getTran_status());
                        
                        logger.trace("orgcnclobj.getCncl_dt : " + orgcnclobj.getCncl_dt());
                        logger.trace("orgcnclobj.getMod_user : " + orgcnclobj.getMod_user());
                        logger.trace("tranInfo.getApp_dt() : " + tranInfo.getApp_dt());
                        logger.trace("strTpCnclDate : " + strTpCnclDate);
                        
                        logger.trace("------------------cncl cash orgtran update End ------------------" );     

                        Integer int_reversdbupdate = appDAO.OrgCashPgTranUpdate(orgcnclobj);

                        logger.trace("------------------reverse orgtran update start------------------" );
                        logger.trace("int_reversdbupdate : " + int_reversdbupdate);
                        logger.trace("------------------reverse orgtran update End ------------------" );                

            }
        }
        
        ht.put("RESULT_CD", res_cd);
        ht.put("RESULT_MSG", res_msg); 
        
        return ht;
    }    
    
    

    @Override
    @Transactional
    public Hashtable appMgrAllatAction(AppFormBean appFormBean,Tb_Tran_CardPg tranInfo) throws Exception {
        Hashtable ht = new Hashtable();
        
        KiccPgVo reqRefobj =  appFormBean.getReqKiccPgVo();
        AllatPgVo reqpgobj = appFormBean.getReqAllatPgVo();
        AllatPgVo respgobj = new AllatPgVo();     
        
        Date today=new Date();
        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmss");       
        String strCurYear = formater.format(today);

        // ============================================================================== /
	// =   02. 쇼핑몰 지불 정보 설정                                                = /
	// = -------------------------------------------------------------------------- = /	
	String g_mall_id   = tranInfo.getTid();             // 리얼 반영시 KCP에 발급된 mall_id 사용
        String g_mall_pwd = appDAO.GetTerminalPwdInfo(g_mall_id);
        String g_mall_name = tranInfo.getComp_nm();    
               
        if(g_mall_id == null || g_mall_id.equals(""))
        {
            throw new Exception("결제용TID가 없습니다.");
        }
        
	logger.trace("------------------------------appMgrAllatAction--------------------------------------");
        //logger.trace("tpStrCurDayCncFlag : " + tpStrCurDayCncFlag);
        logger.trace("g_mall_id : " + g_mall_id);
        logger.trace("g_mall_name : " + g_mall_name);
        logger.trace("--------------------------------------------------------------------");    
                
        
        //인스터스생성 초기화
        AllatUtil util = new AllatUtil();
        HashMap reqHm=new HashMap();
        HashMap resHm=null;
        String szReqMsg="";
        String szAllatEncData="";
        String szCrossKey=g_mall_pwd;

        /* -------------------------------------------------------------------------- */
        /* ::: 결제 결과                                                              */
        /* -------------------------------------------------------------------------- */
        String bDBProc          = "";
        String res_cd           = "";
        String res_msg          = "";

        String sCancelYMDHMS    = "";
        String sPartCancelFlag  = "";
        String sRemainAmt       = "";
        String sPayType         = "";            
      
            
        //가맹점주문번호를 생성한다.
        String strOrder_seq = appDAO.GetOrderSeq();
        reqpgobj.setAllat_order_no(strOrder_seq);
        
        logger.trace("----------------------------------appserviceimple mgrallataction------------------------------------");
            
        //카드취소일 경우
        if(reqRefobj.getMgr_txtype().equals("40"))
        {    
            
            String szShopId        = g_mall_id;      //ShopId 값               (최대  20 자리)
            String szAmt           = tranInfo.getTot_amt();        //취소 금액               (최대  10 자리)
            String szOrderNo       = tranInfo.getOrder_seq();   //주문번호                (최대  80 자리)
            String szPayType       ="CARD";        //원거래건의 결제방식[카드:CARD,계좌이체:ABANK]
            String szSeqNo         = tranInfo.getPg_seq();    //거래일련번호:옵션필드   (최대  10자리)
            
            reqHm.put("allat_shop_id" ,  g_mall_id );
            reqHm.put("allat_order_no",  szOrderNo);
            reqHm.put("allat_amt"     ,  szAmt    );
            reqHm.put("allat_pay_type",  szPayType);
            reqHm.put("allat_test_yn" ,  Util.nullToString(reqpgobj.getAllat_test_yn())       );    //테스트 :Y, 서비스 :N
            reqHm.put("allat_opt_pin" ,  "NOUSE"  );    //수정금지(올앳 참조 필드)
            reqHm.put("allat_opt_mod" ,  "APP"    );    //수정금지(올앳 참조 필드)
            reqHm.put("allat_seq_no"  ,  szSeqNo  );    //옵션 필드( 삭제 가능함 )
            
            szAllatEncData=util.setValue(reqHm);
            szReqMsg  = "allat_shop_id="   + szShopId
              + "&allat_amt="      + szAmt
              + "&allat_enc_data=" + szAllatEncData
              + "&allat_cross_key="+ szCrossKey;
            
            
            resHm = util.cancelReq(szReqMsg, "SSL");
            
        }
        //현금영수증일 경우
        else
        {
            String szShopId = g_mall_id;              //상점ID(최대 20자)
            String szCashBillNo=tranInfo.getPg_seq();      //현금영수증일련번호(최대 10자)
            String szRegBusinessNo=tranInfo.getBiz_no();             //등록할 사업자 번호(10자리):상점 ID와 다른경우
            
            String strTpCshTotamt = Util.getNullToZero(tranInfo.getTot_amt());
            String strTpCshSvcamt = Util.getNullToZero(tranInfo.getSvc_amt());
            String strTpCshVatamt = Util.getNullToZero(tranInfo.getTax_amt());
            
            
            long lntpsupamt = Long.parseLong(strTpCshTotamt)-Long.parseLong(strTpCshSvcamt)-Long.parseLong(strTpCshVatamt);

            reqHm.put("allat_shop_id"           , szShopId       );
            reqHm.put("allat_cash_bill_no"      , szCashBillNo   );
            reqHm.put("allat_supply_amt"        , String.valueOf(lntpsupamt)    );
            reqHm.put("allat_vat_amt"           , strTpCshVatamt      );
            reqHm.put("allat_reg_business_no"   , szRegBusinessNo);
            reqHm.put("allat_test_yn"           , Util.nullToString(reqpgobj.getAllat_test_yn())             );  //테스트 :Y, 서비스 :N
            reqHm.put("allat_opt_pin"           , "NOUSE"        );  //수정금지(올앳 참조 필드)
            reqHm.put("allat_opt_mod"           , "APP"          );  //수정금지(올앳 참조 필드)
            
            szAllatEncData=util.setValue(reqHm);
            szReqMsg  = "allat_shop_id="   + szShopId
              + "&allat_enc_data=" + szAllatEncData
              + "&allat_cross_key="+ szCrossKey;            
            
            resHm = util.cashcanReq(szReqMsg, "SSL");
        }

        res_cd  = (String)resHm.get("reply_cd");                      // 결과 코드
        res_msg = (String)resHm.get("reply_msg");    
        
	logger.trace("--------------------------------------------------------------------");
        logger.trace("res_cd : " + res_cd);
        logger.trace("res_msg : " + res_msg);
        logger.trace("--------------------------------------------------------------------");     
        
        /* -------------------------------------------------------------------------- */
        /* ::: 결과 처리                                                              */
        /* -------------------------------------------------------------------------- */
        
        sCancelYMDHMS    = Util.nullToString((String)resHm.get("cancel_ymdhms"));
        sPartCancelFlag  = Util.nullToString((String)resHm.get("part_cancel_flag"));
        sRemainAmt       = Util.nullToString((String)resHm.get("remain_amt"));
        sPayType         = Util.nullToString((String)resHm.get("pay_type"));
        
        
        logger.trace("--------------------------------------------------------------------");        
        logger.trace("r_cno            "+       tranInfo.getPg_seq()             );// PG거래번호
        logger.trace("sRemainAmt         "+       sRemainAmt          );//총 결제금액
        logger.trace("r_order_no       "+       reqpgobj.getAllat_order_no()        );//주문번호
        logger.trace("r_canc_date      "+       sCancelYMDHMS       );//취소일시
        logger.trace("--------------------------------------------------------------------");
    
        //가맹점 DB처리
        //결제수단 즉시취소
        if(reqRefobj.getMgr_txtype().equals("40"))
        {
            //r거래값을 입력한다.
            InsertCardPgTranBean cnclobj = new InsertCardPgTranBean();
            //승인 결가에 따라 저장을 위한 테이블명을 만든다.
    
            //가맹점 DB 처리에러시 
            String strTpCnclTranseq = appDAO.GetTranSeq();
            cnclobj.setTran_seq(strTpCnclTranseq);// 거래번호 셋팅

            //취소성공시
            if (res_cd.equals("0000"))
            {
                cnclobj.setInstablenm("TB_TRAN_CARDPG_"+sCancelYMDHMS.substring(2, 6));
                cnclobj.setApp_dt(sCancelYMDHMS.substring(0, 8));
                cnclobj.setApp_tm(sCancelYMDHMS.substring(8, 14));
                cnclobj.setResult_status("00");
                
                cnclobj.setTran_step("00");
                cnclobj.setCncl_dt(sCancelYMDHMS.substring(0, 8));
                        
                String strTpCnclDate = sCancelYMDHMS.substring(0, 8); 
                //당일취소,익일취소 구분
                String strTpOrgdt = tranInfo.getApp_dt().replaceAll("-", "");
                if(strTpCnclDate.equals(strTpOrgdt))
                {
                   cnclobj.setTran_status("10");//승인 :00 ,10:당일취소 , 20:매입취소
                   cnclobj.setAcc_chk_flag("3");//당일취소는 매입미대상
                }
                else
                {
                   cnclobj.setTran_status("20");//승인 :00 ,10:당일취소 , 20:매입취소
                   cnclobj.setAcc_chk_flag("0");//당일취소는 매입미대상                    
                }
                
                String strTpPayAmt = Util.getNullToZero(tranInfo.getPay_amt());
                if(!strTpPayAmt.equals("0"))
                {
                    cnclobj.setPay_amt("-"+tranInfo.getPay_amt());
                    cnclobj.setCommision("-"+tranInfo.getCommision());
                }               
                
            }
            else
            {
                cnclobj.setInstablenm("TB_TRAN_CARDPG_"+strCurYear.substring(2, 6));
                cnclobj.setResult_status("99");                        
                cnclobj.setAcc_chk_flag("3");//r거래는3
                
                //당일취소,익일취소 구분
                String strTpOrgdt = tranInfo.getApp_dt().replaceAll("-", "");
                if(strCurYear.substring(0, 8).equals(strTpOrgdt))
                {
                   cnclobj.setTran_status("10");//승인 :00 ,10:당일취소 , 20:매입취소
                }
                else
                {
                   cnclobj.setTran_status("20");//승인 :00 ,10:당일취소 , 20:매입취소
                }
            }

            cnclobj.setMassagetype("40");//메세지타입
            cnclobj.setCard_num(tranInfo.getCard_num());//카드번호
            cnclobj.setApp_card_num(tranInfo.getApp_card_num());//카드번호
            cnclobj.setApp_no(null);//승인번호
            cnclobj.setTot_amt(tranInfo.getTot_amt());//결제총금액
            cnclobj.setPg_seq(tranInfo.getPg_seq());//pg거래고유번호
            cnclobj.setPay_chn_cate(tranInfo.getPay_chn_cate());
            cnclobj.setTid(g_mall_id);
            cnclobj.setOnffmerch_no(tranInfo.getOnffmerch_no());        
            cnclobj.setOnfftid(tranInfo.getOnfftid());
            cnclobj.setWcc(tranInfo.getWcc());
            cnclobj.setExpire_dt(tranInfo.getExpire_dt());
            cnclobj.setCard_cate(tranInfo.getCard_cate());
            cnclobj.setOrder_seq(reqpgobj.getAllat_order_no());
            //cnclobj.setTran_dt(tranInfo.getTran_dt());
            //cnclobj.setTran_tm(tranInfo.getTran_tm());
            cnclobj.setTran_dt(strCurYear.substring(0, 8));
            cnclobj.setTran_tm(strCurYear.substring(8, 14));            
            cnclobj.setTran_cate("40");//취소
            cnclobj.setFree_inst_flag(tranInfo.getFree_inst_flag());
            cnclobj.setTax_amt(tranInfo.getTax_amt());//부가세금액
            cnclobj.setSvc_amt(tranInfo.getSvc_amt());//비과세승인금액
            cnclobj.setInstallment(tranInfo.getInstallment());//할부

            cnclobj.setMerch_no(tranInfo.getMerch_no());
            cnclobj.setOrg_merch_no(tranInfo.getOrg_merch_no());
            cnclobj.setResult_cd(res_cd);
            cnclobj.setResult_msg(res_msg);

            cnclobj.setOrg_app_dd(tranInfo.getApp_dt().replaceAll("-", ""));
            cnclobj.setOrg_app_no(tranInfo.getApp_no());
            cnclobj.setOrg_pg_seq(tranInfo.getPg_seq());
            cnclobj.setCncl_reason("고객취소요청");

            cnclobj.setOnofftid_pay_amt(tranInfo.getOnofftid_pay_amt());
            cnclobj.setOnofftid_commision(tranInfo.getOnofftid_commision());
            cnclobj.setClient_ip(reqpgobj.getAllat_user_ip());

            cnclobj.setTrad_chk_flag("0");//거래대사

            cnclobj.setIns_user(appFormBean.getUser_seq());
            cnclobj.setMod_user(appFormBean.getUser_seq());

            //20150902 취소거래 있을시 입력 패치            
            //Integer int_reversdbresult = appDAO.InsertCardPgTran(cnclobj);
            //logger.debug("-------------cncl double chk start-----------------");
            AppFormBean cnclchkFormBean = new AppFormBean();
            
            cnclchkFormBean.setChkpgseq(tranInfo.getPg_seq());
            cnclchkFormBean.setChkpgtid(g_mall_id);
            
            String strTpCnclChk = Util.nullToString(appDAO.getOnffCnclChkCnt(cnclchkFormBean));
            
            if(strTpCnclChk.equals("")) 
            {
                strTpCnclChk="0";
            }
            
            logger.trace("strTpCnclChk : " + strTpCnclChk);
            
            Integer int_reversdbresult = -1;
            
            if(strTpCnclChk.equals("0"))
            {
                int_reversdbresult = appDAO.InsertCardPgTran(cnclobj);
            }
            else
            {
                logger.info("취소성공거래 기존재");
                int_reversdbresult = 1;
            }
            //logger.debug("-------------cncl double chk end-----------------");
            logger.trace("------------------ tran Dbinsert start------------------" );
            logger.trace("int_reversdbresult : " + int_reversdbresult);
            logger.trace("------------------ tran Dbinsert End ------------------" );                

            //성공시 거래내역을 UPDATE한다.
            if (res_cd.equals("0000"))
            {
                        //r거래에대한 원거래를 Update 한다.
                        InsertCardPgTranBean orgcnclobj = new InsertCardPgTranBean();
                        orgcnclobj.setTran_seq(tranInfo.getTran_seq());
                        orgcnclobj.setInstablenm(tranInfo.getTb_nm());
                        orgcnclobj.setCncl_reason(null);
                        
                        String strTpCnclDate = sCancelYMDHMS.substring(0, 8);      
                        
                        String strTpOrgdt = tranInfo.getApp_dt().replaceAll("-", "");
                        //당일취소,익일취소 구분
                        if(strTpCnclDate.equals(strTpOrgdt))
                        {
                            orgcnclobj.setAcc_chk_flag("3");//매입비대상
                            orgcnclobj.setTran_status("10");//당일취소
                        }
                        else
                        {
                            //orgcnclobj.setAcc_chk_flag("0");//매입대상
                            orgcnclobj.setTran_status("20");//당일취소                            
                        }
                        
                        orgcnclobj.setCncl_dt(cnclobj.getApp_dt());
                        orgcnclobj.setMod_user(appFormBean.getUser_seq());

                        Integer int_reversdbupdate = appDAO.OrgCardPgTranUpdate(orgcnclobj);

                        logger.trace("------------------reverse orgtran update start------------------" );
                        logger.trace("int_reversdbupdate : " + int_reversdbupdate);
                        logger.trace("------------------reverse orgtran update End ------------------" );                

            }
        }
        //현금영수증일 경우 
        else
        {
            
             //r거래값을 입력한다.
            InsertCardPgTranBean cnclobj = new InsertCardPgTranBean();
            //승인 결가에 따라 저장을 위한 테이블명을 만든다.

            //가맹점 DB 처리에러시 
            String strTpCnclTranseq = appDAO.GetTranSeq();
            cnclobj.setTran_seq(strTpCnclTranseq);// 거래번호 셋팅

            //취소성공시
            if (res_cd.equals("0000"))
            {
                cnclobj.setInstablenm("TB_TRAN_CASHPG_"+strCurYear.substring(2, 6));
                //cnclobj.setApp_dt(strCurYear.substring(0, 8));
                //cnclobj.setApp_tm(strCurYear.substring(8, 14));
                cnclobj.setApp_dt(sCancelYMDHMS.substring(0, 8));
                cnclobj.setApp_tm(sCancelYMDHMS.substring(8, 14));                
                
                cnclobj.setResult_status("00");
                
                cnclobj.setTran_step("00");
                //cnclobj.setCncl_dt(strCurYear.substring(0, 8));
                cnclobj.setCncl_dt(sCancelYMDHMS.substring(0, 8));
                        
                String strTpCnclDate = strCurYear.substring(0, 8); 
                
                 String strTpOrgdt = tranInfo.getApp_dt().replaceAll("-", "");
                //당일취소,익일취소 구분
                if(strTpCnclDate.equals(strTpOrgdt))
                {
                   cnclobj.setTran_status("10");//승인 :00 ,10:당일취소 , 20:매입취소
                   cnclobj.setAcc_chk_flag("3");//당일취소는 매입미대상
                }
                else
                {
                   cnclobj.setTran_status("20");//승인 :00 ,10:당일취소 , 20:매입취소
                   cnclobj.setAcc_chk_flag("0");//당일취소는 매입미대상                    
                }
                
            }
            else
            {
                cnclobj.setInstablenm("TB_TRAN_CASHPG_"+strCurYear.substring(2, 6));
                cnclobj.setResult_status("99");                        
                cnclobj.setAcc_chk_flag("3");//r거래는3
                String strTpOrgdt = tranInfo.getApp_dt().replaceAll("-", "");
                //당일취소,익일취소 구분
                if(strCurYear.substring(0, 8).equals(strTpOrgdt))
                {
                   cnclobj.setTran_status("10");//승인 :00 ,10:당일취소 , 20:매입취소
                }
                else
                {
                   cnclobj.setTran_status("20");//승인 :00 ,10:당일취소 , 20:매입취소
                }
            }

            cnclobj.setMassagetype("40");//메세지타입
            cnclobj.setCard_num(tranInfo.getCard_num());//카드번호
            cnclobj.setApp_no(null);//승인번호
            cnclobj.setTot_amt(tranInfo.getTot_amt());//결제총금액
            cnclobj.setPg_seq(tranInfo.getPg_seq());//pg거래고유번호
            cnclobj.setPay_chn_cate(tranInfo.getPay_chn_cate());
            cnclobj.setTid(g_mall_id);
            cnclobj.setOnffmerch_no(tranInfo.getOnffmerch_no());        
            cnclobj.setOnfftid(tranInfo.getOnfftid());
            cnclobj.setWcc(tranInfo.getWcc());
            cnclobj.setExpire_dt(tranInfo.getExpire_dt());
            cnclobj.setCard_cate(tranInfo.getCard_cate());
            cnclobj.setOrder_seq(reqpgobj.getAllat_order_no());
            cnclobj.setTran_dt(strCurYear.substring(0, 8));
            cnclobj.setTran_tm(strCurYear.substring(8, 14));
            cnclobj.setTran_cate("51");//취소
            cnclobj.setFree_inst_flag(tranInfo.getFree_inst_flag());
            cnclobj.setTax_amt(tranInfo.getTax_amt());//부가세금액
            cnclobj.setSvc_amt(tranInfo.getSvc_amt());//비과세승인금액
            cnclobj.setInstallment(tranInfo.getInstallment());//할부

            cnclobj.setMerch_no(tranInfo.getMerch_no());
            cnclobj.setOrg_merch_no(tranInfo.getOrg_merch_no());
            cnclobj.setResult_cd(res_cd);
            cnclobj.setResult_msg(res_msg);

            cnclobj.setOrg_app_dd(tranInfo.getApp_dt().replaceAll("-", ""));
            cnclobj.setOrg_app_no(tranInfo.getApp_no());
            cnclobj.setOrg_pg_seq(tranInfo.getPg_seq());
            cnclobj.setCncl_reason("고객취소요청");


            cnclobj.setOnofftid_pay_amt(tranInfo.getOnofftid_pay_amt());
            cnclobj.setOnofftid_commision(tranInfo.getOnofftid_commision());
            cnclobj.setClient_ip(reqpgobj.getAllat_user_ip());

            cnclobj.setTrad_chk_flag("0");//거래대사

            cnclobj.setIns_user(appFormBean.getUser_seq());
            cnclobj.setMod_user(appFormBean.getUser_seq());

            Integer int_reversdbresult = appDAO.InsertCashPgTran(cnclobj);

            logger.trace("------------------reverse tran Dbinsert start------------------" );
            logger.trace("int_reversdbresult : " + int_reversdbresult);
            logger.trace("------------------reverse tran Dbinsert End ------------------" );                

            //성공시 거래내역을 UPDATE한다.
            if (res_cd.equals("0000"))
            {
                        //r거래에대한 원거래를 Update 한다.
                        InsertCardPgTranBean orgcnclobj = new InsertCardPgTranBean();
                        orgcnclobj.setTran_seq(tranInfo.getTran_seq());
                        orgcnclobj.setInstablenm(tranInfo.getTb_nm());
                        orgcnclobj.setCncl_reason("고객취소요청");
                        
                        String strTpCnclDate = strCurYear.substring(0, 8);      
                        //당일취소,익일취소 구분
                        
                         String strTpOrgdt = tranInfo.getApp_dt().replaceAll("-", "");
                        if(strTpCnclDate.equals(strTpOrgdt))
                        {
                            orgcnclobj.setAcc_chk_flag("3");//매입비대상
                            orgcnclobj.setTran_status("10");//당일취소
                        }
                        else
                        {
                            //orgcnclobj.setAcc_chk_flag("0");//매입대상
                            orgcnclobj.setTran_status("20");//매입취소                     
                        }
                        
                        //orgcnclobj.setCncl_dt(strCurYear.substring(0, 8));
                        orgcnclobj.setCncl_dt(sCancelYMDHMS.substring(0, 8));
                        orgcnclobj.setMod_user(appFormBean.getUser_seq());
                        
                        logger.trace("------------------cncl cash orgtran update start------------------" );
                        logger.trace("orgcnclobj.getTran_seq : " + orgcnclobj.getTran_seq());
                        logger.trace("orgcnclobj.getInstablenm : " + orgcnclobj.getInstablenm());
                        logger.trace("orgcnclobj.getInstablenm : " + orgcnclobj.getInstablenm());
                        logger.trace("orgcnclobj.getCncl_reason : " + orgcnclobj.getCncl_reason());
                        
                         logger.trace("orgcnclobj.getAcc_chk_flag : " + orgcnclobj.getAcc_chk_flag());
                        logger.trace("orgcnclobj.setTran_status : " + orgcnclobj.getTran_status());
                        
                        logger.trace("orgcnclobj.getCncl_dt : " + orgcnclobj.getCncl_dt());
                        logger.trace("orgcnclobj.getMod_user : " + orgcnclobj.getMod_user());
                        logger.trace("tranInfo.getApp_dt() : " + tranInfo.getApp_dt());
                        logger.trace("strTpCnclDate : " + strTpCnclDate);
                        
                        logger.trace("------------------cncl cash orgtran update End ------------------" );     

                        Integer int_reversdbupdate = appDAO.OrgCashPgTranUpdate(orgcnclobj);

                        logger.trace("------------------reverse orgtran update start------------------" );
                        logger.trace("int_reversdbupdate : " + int_reversdbupdate);
                        logger.trace("------------------reverse orgtran update End ------------------" );                

            }
        }
        
        ht.put("RESULT_CD", res_cd);
        ht.put("RESULT_MSG", res_msg); 
        
        return ht;
    }     
    
    
    @Override
    @Transactional
    public String appErrCardTranSave(AppFormBean appFormBean, String strTid, String strResultCd, String StrResultMsg) throws Exception {
        
        KiccPgVo reqpgobj = appFormBean.getReqKiccPgVo();
        
        InsertCardPgTranBean insobj = new InsertCardPgTranBean();
        //승인 결가에 따라 저장을 위한 테이블명을 만든다.
        //성공관련 data 셋팅
        
        Date today=new Date();
        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmss");       
        String strCurYear = formater.format(today);

        insobj.setInstablenm("TB_TRAN_CARDPG_"+strCurYear.substring(2, 6));
        insobj.setResult_status("99");                        
        insobj.setAcc_chk_flag("3");
        insobj.setTot_amt(reqpgobj.getTot_amt());//결제총금액
        
        String strTpTranseq = appDAO.GetTranSeq();
        insobj.setTran_seq(strTpTranseq);// 거래번호 셋팅
             
        insobj.setMassagetype("10");//메세지타입
        insobj.setCard_num(reqpgobj.getCard_no());//카드번호
        
        insobj.setPay_chn_cate("001");
        insobj.setTid(strTid);
        insobj.setOnffmerch_no(appFormBean.getParam_onffmerch_no());        
        insobj.setOnfftid( appFormBean.getParam_onfftid());
        insobj.setWcc(reqpgobj.getWcc());
        insobj.setExpire_dt(reqpgobj.getExpire_date());
        insobj.setCard_cate(reqpgobj.getCard_user_type());
        insobj.setOrder_seq(reqpgobj.getOrder_no());
        insobj.setTran_dt(strCurYear.substring(0, 8));
        insobj.setTran_tm(strCurYear.substring(8, 14));
        insobj.setTran_cate(reqpgobj.getCard_txtype());
        //무이자구분
        insobj.setFree_inst_flag(reqpgobj.getNoint());
        insobj.setTax_amt(reqpgobj.getCom_vat_amt());//부가세금액
        insobj.setSvc_amt(reqpgobj.getCom_free_amt ());//비과세승인금액
        insobj.setInstallment(reqpgobj.getInstall_period());//할부
                        
        insobj.setResult_cd(strResultCd);
        insobj.setResult_msg(StrResultMsg);

        insobj.setTran_status("00");//승인 :00 ,10:당일취소 , 20:매입취소
        insobj.setTran_step("00");
                        
        insobj.setClient_ip(reqpgobj.getClient_ip());
        insobj.setUser_type(reqpgobj.getUser_type());
        insobj.setMemb_user_no(reqpgobj.getMemb_user_no());
        insobj.setUser_id(reqpgobj.getUser_id());
        insobj.setUser_nm(reqpgobj.getUser_nm());
        insobj.setUser_mail(reqpgobj.getUser_mail());
        insobj.setUser_phone1(reqpgobj.getUser_phone1());
        insobj.setUser_phone2(reqpgobj.getUser_phone2());
        insobj.setUser_addr(reqpgobj.getUser_addr());
        insobj.setProduct_type(reqpgobj.getProduct_type());
        insobj.setProduct_nm(reqpgobj.getProduct_nm());

        insobj.setTrad_chk_flag("0");        
        
        //가맹점 DB 처리
        logger.trace("-------error insert start!!-------");
        Integer int_dbresult = appDAO.InsertCardPgTran(insobj);
        String strTpResult = int_dbresult.toString();
        logger.trace("-------error insert end!!-------");
        return strTpResult;
    }

    @Override
    @Transactional
    public String appErrCashTranSave(AppFormBean appFormBean, String strTid, String strResultCd, String StrResultMsg) throws Exception {
       
        KiccPgVo reqpgobj = appFormBean.getReqKiccPgVo();
        
        InsertCardPgTranBean insobj = new InsertCardPgTranBean();
        //승인 결가에 따라 저장을 위한 테이블명을 만든다.
        //성공관련 data 셋팅
        
        Date today=new Date();
        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmss");       
        String strCurYear = formater.format(today);

        insobj.setInstablenm("TB_TRAN_CASHPG_"+strCurYear.substring(2, 6));
        insobj.setResult_status("99");                        
        insobj.setAcc_chk_flag("3");
        insobj.setTot_amt(reqpgobj.getTot_amt());//결제총금액
        
        String strTpTranseq = appDAO.GetTranSeq();
        insobj.setTran_seq(strTpTranseq);// 거래번호 셋팅
        //카드결제 시
             
        insobj.setMassagetype("10");//메세지타입
        insobj.setCard_num(reqpgobj.getCash_auth_value());//인증값
        //insobj.setApp_no(r_auth_no);//승인번호

        //insobj.setPg_seq(r_cno);//pg거래고유번호
        insobj.setPay_chn_cate("001");
        insobj.setTid(strTid);
        insobj.setOnffmerch_no(appFormBean.getParam_onffmerch_no());        
        insobj.setOnfftid( appFormBean.getParam_onfftid());
        insobj.setWcc(reqpgobj.getCash_issue_type());
        insobj.setCard_cate(reqpgobj.getCash_auth_type());
        insobj.setOrder_seq(reqpgobj.getOrder_no());
        insobj.setTran_dt(strCurYear.substring(0, 8));
        insobj.setTran_tm(strCurYear.substring(8, 14));
        insobj.setTran_cate("10");
        
        insobj.setTax_amt(reqpgobj.getCom_vat_amt());//부가세금액
        insobj.setSvc_amt(reqpgobj.getCom_free_amt ());//비과세승인금액(서비스금액)
        
        
        insobj.setResult_cd(strResultCd);
        insobj.setResult_msg(StrResultMsg);
        
        insobj.setTran_status("00");//승인 :00 ,10:당일취소 , 20:매입취소
        insobj.setTran_step("00");
        insobj.setTrad_chk_flag("0");
        
        insobj.setClient_ip(reqpgobj.getClient_ip());
        insobj.setUser_type(reqpgobj.getUser_type());
        insobj.setMemb_user_no(reqpgobj.getMemb_user_no());
        insobj.setUser_id(reqpgobj.getUser_id());
        insobj.setUser_nm(reqpgobj.getUser_nm());
        insobj.setUser_mail(reqpgobj.getUser_mail());
        insobj.setUser_phone1(reqpgobj.getUser_phone1());
        insobj.setUser_phone2(reqpgobj.getUser_phone2());
        insobj.setUser_addr(reqpgobj.getUser_addr());
        insobj.setProduct_type(reqpgobj.getProduct_type());
        insobj.setProduct_nm(reqpgobj.getProduct_nm()); 
        insobj.setIns_user(appFormBean.getUser_seq());
        insobj.setMod_user(appFormBean.getUser_seq());
        
        //가맹점 DB 처리
        logger.trace("-------error insert start!!-------");
        Integer int_dbresult = appDAO.InsertCashPgTran(insobj);
        String strTpResult = int_dbresult.toString();
        logger.trace("-------error insert end!!-------");
        
        return strTpResult;
        
        
    }

    @Override
    @Transactional
    public String mgrErrTranSave(AppFormBean appFormBean, String strTran_Gubun,Tb_Tran_CardPg tranInfo, String strResultCd, String StrResultMsg) throws Exception {
         String strTpResult = null;
         KiccPgVo reqpgobj = appFormBean.getReqKiccPgVo();
                 
        Date today=new Date();
        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddHHmmss");       
        String strCurYear = formater.format(today);

         
        if(strTran_Gubun.equals("CARD"))
        {
            //r거래값을 입력한다.
            InsertCardPgTranBean cnclobj = new InsertCardPgTranBean();
            //승인 결가에 따라 저장을 위한 테이블명을 만든다.
    
            //가맹점 DB 처리에러시 
            String strTpCnclTranseq = appDAO.GetTranSeq();
            cnclobj.setTran_seq(strTpCnclTranseq);// 거래번호 셋팅

            cnclobj.setInstablenm("TB_TRAN_CARDPG_"+strCurYear.substring(2, 6));
            cnclobj.setResult_status("99");                        
            cnclobj.setAcc_chk_flag("3");//r거래는3
            
             //당일취소,익일취소 구분
             String strTpOrgdt = tranInfo.getApp_dt().replaceAll("-", "");
             
	     if(strCurYear.substring(0, 8).equals(strTpOrgdt))
             {
                  cnclobj.setTran_status("10");//승인 :00 ,10:당일취소 , 20:매입취소
             }
             else
             {
                  cnclobj.setTran_status("20");//승인 :00 ,10:당일취소 , 20:매입취소
             }

            cnclobj.setMassagetype("40");//메세지타입
            cnclobj.setCard_num(tranInfo.getCard_num());//카드번호
            //cnclobj.setApp_no(r_auth_no);//승인번호
            cnclobj.setTot_amt(tranInfo.getTot_amt());//결제총금액
            //cnclobj.setPg_seq(r_cno);//pg거래고유번호
            cnclobj.setPay_chn_cate(tranInfo.getPay_chn_cate());
            cnclobj.setTid(tranInfo.getTid());
            cnclobj.setOnffmerch_no(tranInfo.getOnffmerch_no());        
            cnclobj.setOnfftid(tranInfo.getOnfftid());
            cnclobj.setWcc(tranInfo.getWcc());
            cnclobj.setExpire_dt(tranInfo.getExpire_dt());
            cnclobj.setCard_cate(tranInfo.getCard_cate());
            cnclobj.setOrder_seq(reqpgobj.getOrder_no());
            cnclobj.setTran_dt(tranInfo.getTran_dt());
            cnclobj.setTran_tm(tranInfo.getTran_tm());
            cnclobj.setTran_cate("40");//취소
            cnclobj.setFree_inst_flag(tranInfo.getFree_inst_flag());
            cnclobj.setTax_amt(tranInfo.getTax_amt());//부가세금액
            cnclobj.setSvc_amt(tranInfo.getSvc_amt());//비과세승인금액
            cnclobj.setInstallment(tranInfo.getInstallment());//할부

            cnclobj.setMerch_no(tranInfo.getMerch_no());
            cnclobj.setResult_cd(strResultCd);
            cnclobj.setResult_msg(StrResultMsg);

            cnclobj.setOrg_app_dd(tranInfo.getApp_dt().replaceAll("-", ""));
            cnclobj.setOrg_app_no(tranInfo.getApp_no());
            cnclobj.setOrg_pg_seq(tranInfo.getPg_seq());
            cnclobj.setCncl_reason(reqpgobj.getMgr_msg());


            cnclobj.setOnofftid_pay_amt(tranInfo.getOnofftid_pay_amt());
            cnclobj.setOnofftid_commision(tranInfo.getOnofftid_commision());
            cnclobj.setClient_ip(reqpgobj.getClient_ip());

            cnclobj.setTrad_chk_flag("0");//거래대사

            cnclobj.setIns_user(appFormBean.getUser_seq());
            cnclobj.setMod_user(appFormBean.getUser_seq());

            Integer int_reversdbresult = appDAO.InsertCardPgTran(cnclobj);

            logger.trace("------------------reverse tran Dbinsert start------------------" );
            logger.trace("int_reversdbresult : " + int_reversdbresult);
            logger.trace("------------------reverse tran Dbinsert End ------------------" );    
            
            strTpResult = int_reversdbresult.toString();

        }
        //현금영수증일 경우 
        else
        {
             //r거래값을 입력한다.
            InsertCardPgTranBean cnclobj = new InsertCardPgTranBean();
            //승인 결가에 따라 저장을 위한 테이블명을 만든다.

            //가맹점 DB 처리에러시 
            String strTpCnclTranseq = appDAO.GetTranSeq();
            cnclobj.setTran_seq(strTpCnclTranseq);// 거래번호 셋팅
         
            cnclobj.setInstablenm("TB_TRAN_CASHPG_"+strCurYear.substring(2, 6));
            cnclobj.setResult_status("99");                        
            cnclobj.setAcc_chk_flag("3");//r거래는3
            String strTpOrgdt = tranInfo.getApp_dt().replaceAll("-", "");
             //당일취소,익일취소 구분
            if(strCurYear.substring(0, 8).equals(strTpOrgdt))
            {
                 cnclobj.setTran_status("10");//승인 :00 ,10:당일취소 , 20:매입취소
            }
            else
            {
                 cnclobj.setTran_status("20");//승인 :00 ,10:당일취소 , 20:매입취소
             }
          
            cnclobj.setMassagetype("40");//메세지타입
            cnclobj.setCard_num(tranInfo.getCard_num());//카드번호
            //cnclobj.setApp_no(r_auth_no);//승인번호
            cnclobj.setTot_amt(tranInfo.getTot_amt());//결제총금액
            //cnclobj.setPg_seq(r_cno);//pg거래고유번호
            cnclobj.setPay_chn_cate(tranInfo.getPay_chn_cate());
            cnclobj.setTid(tranInfo.getTid());
            cnclobj.setOnffmerch_no(tranInfo.getOnffmerch_no());        
            cnclobj.setOnfftid(tranInfo.getOnfftid());
            cnclobj.setWcc(tranInfo.getWcc());
            cnclobj.setExpire_dt(tranInfo.getExpire_dt());
            cnclobj.setCard_cate(tranInfo.getCard_cate());
            cnclobj.setOrder_seq(reqpgobj.getOrder_no());
            cnclobj.setTran_dt(tranInfo.getTran_dt());
            cnclobj.setTran_tm(tranInfo.getTran_tm());
            cnclobj.setTran_cate("51");//취소
            cnclobj.setFree_inst_flag(tranInfo.getFree_inst_flag());
            cnclobj.setTax_amt(tranInfo.getTax_amt());//부가세금액
            cnclobj.setSvc_amt(tranInfo.getSvc_amt());//비과세승인금액
            cnclobj.setInstallment(tranInfo.getInstallment());//할부

            cnclobj.setMerch_no(tranInfo.getMerch_no());
            cnclobj.setResult_cd(strResultCd);
            cnclobj.setResult_msg(StrResultMsg);

            cnclobj.setOrg_app_dd(tranInfo.getApp_dt().replaceAll("-", ""));
            cnclobj.setOrg_app_no(tranInfo.getApp_no());
            cnclobj.setOrg_pg_seq(tranInfo.getPg_seq());
            cnclobj.setCncl_reason(reqpgobj.getMgr_msg());


            cnclobj.setOnofftid_pay_amt(tranInfo.getOnofftid_pay_amt());
            cnclobj.setOnofftid_commision(tranInfo.getOnofftid_commision());
            cnclobj.setClient_ip(reqpgobj.getClient_ip());

            cnclobj.setTrad_chk_flag("0");//거래대사

            cnclobj.setIns_user(appFormBean.getUser_seq());
            cnclobj.setMod_user(appFormBean.getUser_seq());

            Integer int_reversdbresult = appDAO.InsertCashPgTran(cnclobj);

            
            strTpResult = int_reversdbresult.toString();
            logger.trace("------------------reverse tran Dbinsert start------------------" );
            logger.trace("int_reversdbresult : " + int_reversdbresult);
            logger.trace("------------------reverse tran Dbinsert End ------------------" );                

        }         
         
        return strTpResult;
    }

    
}
