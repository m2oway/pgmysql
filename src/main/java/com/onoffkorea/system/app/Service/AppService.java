/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.app.Service;

import com.onoffkorea.system.app.Bean.AppFormBean;
import com.onoffkorea.system.app.Vo.AppOnffTidInfoVo;
import com.onoffkorea.system.app.Vo.AppTerminalInfoVo;
import com.onoffkorea.system.app.Vo.Tb_Tran_CardPg;
import java.util.Hashtable;

/**
 *
 * @author Administrator
 */
public interface AppService {
        
    public Hashtable appselReady(AppFormBean appFormBean) throws Exception;

    public Hashtable appReqForm(AppFormBean appFormBean) throws Exception;
    
    public Hashtable appReqAction(AppFormBean appFormBean) throws Exception;
    
    public Hashtable appKiccReqAction(AppFormBean appFormBean, AppOnffTidInfoVo appOnffTidInfoVo, AppTerminalInfoVo appTerminalInfoVo ) throws Exception;
    
    public Hashtable appKcpReqAction(AppFormBean appFormBean, AppOnffTidInfoVo appOnffTidInfoVo, AppTerminalInfoVo appTerminalInfoVo ) throws Exception;
    
    public Hashtable appAllatReqAction(AppFormBean appFormBean, AppOnffTidInfoVo appOnffTidInfoVo, AppTerminalInfoVo appTerminalInfoVo ) throws Exception;

    public Hashtable appResult(AppFormBean appFormBean) throws Exception;
    
    public Hashtable appCashReady(AppFormBean appFormBean) throws Exception;
    
    public Hashtable appReqCashAction(AppFormBean appFormBean) throws Exception;
    
    public Hashtable appKiccCashReqAction(AppFormBean appFormBean, AppOnffTidInfoVo appOnffTidInfoVo, AppTerminalInfoVo appTerminalInfoVo ) throws Exception;
    
    public Hashtable appKcpCashReqAction(AppFormBean appFormBean, AppOnffTidInfoVo appOnffTidInfoVo, AppTerminalInfoVo appTerminalInfoVo ) throws Exception;
    
    public Hashtable appAllatCashReqAction(AppFormBean appFormBean, AppOnffTidInfoVo appOnffTidInfoVo, AppTerminalInfoVo appTerminalInfoVo ) throws Exception;
    
    public Hashtable appCashResult(AppFormBean appFormBean) throws Exception;
    
    public Hashtable appMgrAction(AppFormBean appFormBean) throws Exception;
    
    public Hashtable appCnclReqAction(AppFormBean appFormBean) throws Exception;
    
    //public Hashtable appForceCnclReqAction(AppFormBean appFormBean) throws Exception;
    
    public Hashtable appMgrKiccAction(AppFormBean appFormBean,Tb_Tran_CardPg tranInfo) throws Exception;
    
    public Hashtable appMgrKcpAction(AppFormBean appFormBean,Tb_Tran_CardPg tranInfo) throws Exception;
    
    public Hashtable appMgrAllatAction(AppFormBean appFormBean,Tb_Tran_CardPg tranInfo) throws Exception;
    
    public String appErrCardTranSave(AppFormBean appFormBean, String strTid, String strResultCd, String StrResultMsg) throws Exception;
    
    public String appErrCashTranSave(AppFormBean appFormBean, String strTid, String strResultCd, String StrResultMsg) throws Exception;
    
    public String mgrErrTranSave(AppFormBean appFormBean, String strTran_Gubun,Tb_Tran_CardPg tranInfo, String strResultCd, String StrResultMsg) throws Exception;
}
