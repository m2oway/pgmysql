/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.popup.Controller;

import com.onoffkorea.system.agent.Bean.AgentFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.company.Bean.CompanyFormBean;
import com.onoffkorea.system.merchant.Bean.MerchantFormBean;
import com.onoffkorea.system.mid.Bean.MidFormBean;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import com.onoffkorea.system.paymtd.Bean.PaymtdFormBean;
import com.onoffkorea.system.popup.Service.PopupService;
import com.onoffkorea.system.terminal.Bean.TerminalFormBean;
import com.onoffkorea.system.tid.Bean.TidFormBean;
import java.util.Hashtable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/popup/**")
public class PopupContrloller {
    
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private PopupService popupService;
    
    @Autowired
    private CommonService commonService;
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }    
    
    //정보 조회
    @RequestMapping(method= RequestMethod.GET,value = "/MidSelectPopUp")
    public String midSelectPopup(@Valid MidFormBean midFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_midPopupList = popupService.midSelect(midFormBean);
        String ls_midPopupList = (String) ht_midPopupList.get("ResultSet");

        model.addAttribute("midPopupList",ls_midPopupList);
        
        return null;
        
    }
  
    
        //정보 조회
    @RequestMapping(method= RequestMethod.GET,value = "/TidSelectPopUp")
    public String tidSelectPopup(@Valid TidFormBean tidFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_tidPopupList = popupService.tidSelect(tidFormBean);
        String ls_tidPopupList = (String) ht_tidPopupList.get("ResultSet");

        model.addAttribute("tidPopupList",ls_tidPopupList);
        
        return null;
        
    }
    
        //정보 조회
    @RequestMapping(method= RequestMethod.GET,value = "/CompanySelectPopUp")
    public String companySelectPopupForm(@Valid CompanyFormBean companyFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_companyPopupList = popupService.companySelect(companyFormBean);
        String ls_companyPopupList = (String) ht_companyPopupList.get("ResultSet");

        model.addAttribute("companyPopupList",ls_companyPopupList);
        
        return null;
        
    }    
    
        //정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/CompanySelectPopUp")
    public @ResponseBody String companySelectPopup(@Valid CompanyFormBean companyFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_companyPopupList = popupService.companySelect(companyFormBean);
        String ls_companyPopupList = (String) ht_companyPopupList.get("ResultSet");
        
        return ls_companyPopupList;
        
    }        
    
            //정보 조회
    @RequestMapping(method= RequestMethod.GET,value = "/OnoffmerchSelectPopUp")
    public String onoffmerchSelectPopupForm(@Valid MerchantFormBean merchantFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        merchantFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            merchantFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            merchantFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            merchantFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                    
        
        Hashtable ht_onoffmerchPopupList = popupService.onoffmerchSelect(merchantFormBean);
        String ls_onoffmerchPopupList = (String) ht_onoffmerchPopupList.get("ResultSet");

        model.addAttribute("onoffmerchPopupList",ls_onoffmerchPopupList);
        
        return null;
        
    }    

    
            //정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/OnoffmerchSelectPopUp")
    public @ResponseBody String onoffmerchSelectPopup(@Valid MerchantFormBean merchantFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        merchantFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            merchantFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            merchantFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            merchantFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                 
        
        Hashtable ht_onoffmerchPopupList = popupService.onoffmerchSelect(merchantFormBean);
        String ls_onoffmerchPopupList = (String) ht_onoffmerchPopupList.get("ResultSet");
       
        return ls_onoffmerchPopupList;
        
    }        
    
    
    //정보 조회
    @RequestMapping(method= RequestMethod.GET,value = "/AgentSelectPopUp")
    public String agentSelectPopupForm(@Valid AgentFormBean agentFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        agentFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            agentFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            agentFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            agentFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                 
        
        Hashtable ht_agentPopupList = popupService.agentSelect(agentFormBean);
        String ls_agentPopupList = (String) ht_agentPopupList.get("ResultSet");

        model.addAttribute("ls_agentPopupList",ls_agentPopupList);
        
        return null;
        
    }    

    
    //정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/AgentSelectPopUp")
    public @ResponseBody String agentSelectPopup(@Valid AgentFormBean agentFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        agentFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            agentFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            agentFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            agentFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                
        
        Hashtable ht_agentPopupList = popupService.agentSelect(agentFormBean);
        String ls_agentPopupList = (String) ht_agentPopupList.get("ResultSet");
       
        return ls_agentPopupList;
        
    }        
    
    
    //정보 조회
    @RequestMapping(method= RequestMethod.GET,value = "/PayMtdSelectPopUp")
    public String paymtdSelectPopupForm(@Valid PaymtdFormBean paymtdFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_paymtdPopupList = popupService.paymtdSelect(paymtdFormBean);
        String ls_paymtdPopupList = (String) ht_paymtdPopupList.get("ResultSet");

        model.addAttribute("ls_paymtdPopupList",ls_paymtdPopupList);
        
        return null;
        
    }    
    
    
            //정보 조회
    @RequestMapping(method= RequestMethod.GET,value = "/OnofftidSelectPopUp")
    public String onofftidSelectPopupForm(@Valid TerminalFormBean terminalFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        terminalFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            terminalFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            terminalFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            terminalFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                
        
        Hashtable ht_onofftidPopupList = popupService.onofftidSelect(terminalFormBean);
        String ls_onofftidPopupList = (String) ht_onofftidPopupList.get("ResultSet");

        model.addAttribute("onofftidPopupList",ls_onofftidPopupList);
        
        return null;
        
    }    

    
            //정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/OnofftidSelectPopUp")
    public @ResponseBody String onofftidSelectPopup(@Valid TerminalFormBean terminalFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        terminalFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            terminalFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            terminalFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            terminalFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                
        
        Hashtable ht_onofftidPopupList = popupService.onofftidSelect(terminalFormBean);
        String ls_onofftidPopupList = (String) ht_onofftidPopupList.get("ResultSet");
       
        return ls_onofftidPopupList;
        
    }            
    
        
}
