/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.popup.Dao;

import com.onoffkorea.system.company.Bean.CompanyFormBean;
import com.onoffkorea.system.merchant.Bean.MerchantFormBean;
import com.onoffkorea.system.mid.Bean.MidFormBean;
import com.onoffkorea.system.tid.Bean.TidFormBean;
import java.util.Hashtable;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public interface PopupDAO {

    public List midSelect(MidFormBean midFormBean);
    
    public List tidSelect(TidFormBean tidFormBean);
    
    public List companySelect(CompanyFormBean companyFormBean) ;
    
    public List onoffmerchSelect(MerchantFormBean merchantFormBean) ;

}
