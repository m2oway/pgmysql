/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.popup.Dao;

import com.onoffkorea.system.company.Bean.CompanyFormBean;
import com.onoffkorea.system.merchant.Bean.MerchantFormBean;
import com.onoffkorea.system.mid.Bean.MidFormBean;
import com.onoffkorea.system.tid.Bean.TidFormBean;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("popupDAO")
public class PopupDAOImpl extends SqlSessionDaoSupport  implements PopupDAO {
    
    protected Logger logger = Logger.getLogger(getClass().getName());


    @Override
    public List midSelect(MidFormBean midFormBean) {
        return (List) getSqlSession().selectList("Mid.midPopupList", midFormBean);
    }
    
    @Override
    public List tidSelect(TidFormBean tidFormBean) {
        return (List) getSqlSession().selectList("Tid.tidPopupList", tidFormBean);
    }

    @Override
    public List companySelect(CompanyFormBean companyFormBean) {
        return (List) getSqlSession().selectList("Company.companyPopupList", companyFormBean);
    }

    @Override
    public List onoffmerchSelect(MerchantFormBean merchantFormBean) {
        return (List) getSqlSession().selectList("Merchant.merchantMasterList", merchantFormBean);
    }
    
}
