/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.popup.Service;

import com.onoffkorea.system.agent.Bean.AgentFormBean;
import com.onoffkorea.system.agent.Dao.AgentDAO;
import com.onoffkorea.system.agent.Vo.Tb_Agent;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.company.Bean.CompanyFormBean;
import com.onoffkorea.system.company.Vo.Tb_Company;
import com.onoffkorea.system.merchant.Bean.MerchantFormBean;
import com.onoffkorea.system.merchant.Dao.MerchantDAO;
import com.onoffkorea.system.merchant.Vo.Tb_Onffmerch_Mst;
import com.onoffkorea.system.mid.Bean.MidFormBean;
import com.onoffkorea.system.mid.Vo.Tb_Mid_Info;
import com.onoffkorea.system.paymtd.Bean.PaymtdFormBean;
import com.onoffkorea.system.paymtd.Dao.PaymtdDAO;
import com.onoffkorea.system.paymtd.Vo.Tb_Pay_Mtd;
import com.onoffkorea.system.popup.Dao.PopupDAO;
import com.onoffkorea.system.terminal.Bean.TerminalFormBean;
import com.onoffkorea.system.terminal.Dao.TerminalDAO;
import com.onoffkorea.system.terminal.Vo.Tb_Onfftid_Mst;
import com.onoffkorea.system.tid.Bean.TidFormBean;
import com.onoffkorea.system.tid.Vo.Tb_Tid_Info;
import java.util.Hashtable;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service("popupService")
public class PopupServiceImpl implements PopupService
{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private PopupDAO popupdDAO;
    
    @Autowired
    private MerchantDAO merchantDAO;    
    
    @Autowired
    private AgentDAO agentDAO;       
    
    @Autowired
    private PaymtdDAO paymtdDAO;        
    
    @Autowired
    private TerminalDAO terminalDAO;    
    
    @Override
    public Hashtable midSelect(MidFormBean midFormBean) throws Exception {
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
       
        List ls_midList = this.popupdDAO.midSelect(midFormBean);
        
        //sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(paymtdFormBean.getStart_no())-1)+"\",\"rows\" : [");
        sb.append("{"+"\"rows\" : [");
        for (int i = 0; i < ls_midList.size(); i++) {
                Tb_Mid_Info tb_midInfotd = (Tb_Mid_Info) ls_midList.get(i);
                //"<center>NO</center>,<center>MerchantSeq</center>,<center>MID구분명</center>,<center>MID구분</center>,<center>MID명</center>,<center>MID</center>,<center>시작일</center>,<center>종료일</center>"
                sb.append("{\"id\":" + tb_midInfotd.getMerchant_seq());
                sb.append(",\"data\":[");
                sb.append(" \"" + tb_midInfotd.getRnum() + "\"");
                sb.append(",\"" + Util.nullToString(tb_midInfotd.getMerchant_seq()) + "\"");         
                sb.append(",\"" + Util.nullToString(tb_midInfotd.getPay_mtd_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_midInfotd.getPay_mtd()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_midInfotd.getMid_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_midInfotd.getMerch_no()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_midInfotd.getStart_dt()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_midInfotd.getEnd_dt()) + "\"");
                
                if (i == (ls_midList.size() - 1)) {
                        sb.append("]}");
                } else {
                        sb.append("]},");
                }
        }

        sb.append("]}");    
        
        ht.put("ResultSet", sb.toString());
        
        return ht;
    }

    @Override
    public Hashtable tidSelect(TidFormBean tidFormBean) throws Exception {
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
       
        List ls_tidList = this.popupdDAO.tidSelect(tidFormBean);
        
        //sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(paymtdFormBean.getStart_no())-1)+"\",\"rows\" : [");
        sb.append("{"+"\"rows\" : [");
        for (int i = 0; i < ls_tidList.size(); i++) {
                Tb_Tid_Info tb_tidInfotd = (Tb_Tid_Info) ls_tidList.get(i);
                //"<center>NO</center>,<center>TIDSeq</center>,<center>구분명</center>,<center>구분</center>,<center>TID명</center>,<center>TID</center>,<center>시작일</center>,<center>종료일</center>"
                sb.append("{\"id\":" + tb_tidInfotd.getTid_seq());
                sb.append(",\"data\":[");
                sb.append(" \"" + tb_tidInfotd.getRnum() + "\"");
                sb.append(",\"" + Util.nullToString(tb_tidInfotd.getTid_seq()) + "\"");         
                sb.append(",\"" + Util.nullToString(tb_tidInfotd.getTid_mtd_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_tidInfotd.getTid_mtd()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_tidInfotd.getTid_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_tidInfotd.getTerminal_no()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_tidInfotd.getStart_dt()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_tidInfotd.getEnd_dt()) + "\"");
                
                if (i == (ls_tidList.size() - 1)) {
                        sb.append("]}");
                } else {
                        sb.append("]},");
                }
        }

        sb.append("]}");    
        
        ht.put("ResultSet", sb.toString());
        
        return ht;
    }
    
    @Override
    public Hashtable companySelect(CompanyFormBean companyFormBean) throws Exception {
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
       
        List ls_companyList = this.popupdDAO.companySelect(companyFormBean);

        sb.append("{"+"\"rows\" : [");
        for (int i = 0; i < ls_companyList.size(); i++) {
                Tb_Company tb_companyInfotd = (Tb_Company) ls_companyList.get(i);
                //NO,사업자명,사업자번호,법인번호,대표자,연락처1,연락처2,우편번호,주소1,주소2,사용여부,사용여부명,개인/법인
                //sb.append("{\"id\":" + tb_companyInfotd.getComp_seq());
                sb.append("{\"id\":" + (i+1));
                sb.append(",\"userdata\":{");
                sb.append("\"comp_nm\":\""+Util.nullToString(tb_companyInfotd.getComp_nm())+"\"");
                sb.append(",\"comp_seq\":\""+Util.nullToString(tb_companyInfotd.getComp_seq())+"\"");
                sb.append(",\"biz_no\":\""+Util.nullToString(tb_companyInfotd.getBiz_no())+"\"");
                sb.append(",\"comp_cate_nm\":\""+Util.nullToString(tb_companyInfotd.getCate_comp_nm())+"\"");
                sb.append(",\"corp_no\":\""+Util.nullToString(tb_companyInfotd.getCorp_no())+"\"");
                sb.append(",\"comp_ceo_nm\":\""+Util.nullToString(tb_companyInfotd.getComp_ceo_nm())+"\"");
                sb.append(",\"comp_tel1\":\""+Util.nullToString(tb_companyInfotd.getComp_tel1())+"\"");
                sb.append(",\"comp_tel2\":\""+Util.nullToString(tb_companyInfotd.getComp_tel2())+"\"");
                sb.append(",\"zip_cd\":\""+Util.nullToString(tb_companyInfotd.getZip_cd())+"\"");
                sb.append(",\"addr_1\":\""+Util.nullToString(tb_companyInfotd.getAddr_1())+"\"");
                sb.append(",\"addr_2\":\""+Util.nullToString(tb_companyInfotd.getAddr_2())+"\"");
                sb.append(",\"biz_cate\":\""+Util.nullToString(tb_companyInfotd.getBiz_cate())+"\"");
                sb.append(",\"biz_type\":\""+Util.nullToString(tb_companyInfotd.getBiz_type())+"\"");
                sb.append(",\"tax_flag\":\""+Util.nullToString(tb_companyInfotd.getTax_flag_nm())+"\"");
                sb.append("}");  
                sb.append(",\"data\":[");
                sb.append(" \"" + tb_companyInfotd.getRnum() + "\"");
                sb.append(",\"" + Util.nullToString(tb_companyInfotd.getComp_nm()) + "\"");         
                sb.append(",\"" + Util.nullToString(tb_companyInfotd.getCate_comp_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_companyInfotd.getBiz_no()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_companyInfotd.getCorp_no()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_companyInfotd.getComp_ceo_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_companyInfotd.getComp_tel1()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_companyInfotd.getComp_tel2()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_companyInfotd.getZip_cd()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_companyInfotd.getAddr_1()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_companyInfotd.getAddr_2()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_companyInfotd.getBiz_type()) + "\"");//업태
                sb.append(",\"" + Util.nullToString(tb_companyInfotd.getBiz_cate()) + "\"");//업종
                sb.append(",\"" + Util.nullToString(tb_companyInfotd.getTax_flag_nm()) + "\"");//과세구분                
                sb.append(",\"" + Util.nullToString(tb_companyInfotd.getUse_flag()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_companyInfotd.getUse_flag_nm()) + "\"");

                
                if (i == (ls_companyList.size() - 1)) {
                        sb.append("]}");
                } else {
                        sb.append("]},");
                }
        }

        sb.append("]}");    
        
        ht.put("ResultSet", sb.toString());
        
        return ht;
    }

    @Override
    public Hashtable onoffmerchSelect(MerchantFormBean merchantFormBean) throws Exception {
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
        
        String total_count = this.merchantDAO.merchantRecordCount(merchantFormBean);
       
        List ls_merchantMasterList = this.popupdDAO.onoffmerchSelect(merchantFormBean);
        
        
        ///sb.append("{"+"\"rows\" : [");
        sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(merchantFormBean.getStart_no())-1)+"\",\"rows\" : [");
        
        for (int i = 0; i < ls_merchantMasterList.size(); i++) {
                Tb_Onffmerch_Mst tb_Merchant = (Tb_Onffmerch_Mst) ls_merchantMasterList.get(i);
                
                //가맹점명,가맹점ID, 사업자명,개인법인구분,사업자번호, 법인번호, 대표자, 연락처1, 결제한도, 대리점, 상태
                sb.append("{\"id\":" + i);
                sb.append(",\"userdata\":{");
                sb.append("\"onffmerch_no\":\""+tb_Merchant.getOnffmerch_no()+"\"");
                sb.append("}");                
                sb.append(",\"data\":[");
                sb.append(" \"" + tb_Merchant.getRnum() + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getMerch_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getOnffmerch_no()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getComp_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getComp_cate_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getBiz_no()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getCorp_no()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getComp_ceo_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getTel_1()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getApp_chk_amt()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getAgent_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getSvc_stat_nm()) + "\"");
                
                if (i == (ls_merchantMasterList.size() - 1)) {
                        sb.append("]}");
                } else {
                        sb.append("]},");
                }
        }

        sb.append("]}");    
        
        ht.put("ResultSet", sb.toString());
        ht.put("ls_merchantMasterList",ls_merchantMasterList);
        
        return ht;
    }

    @Override
    public Hashtable agentSelect(AgentFormBean agentFormBean) throws Exception {

        Hashtable ht = new Hashtable();

        try {
                Integer total_count = agentDAO.agentMasterListCount(agentFormBean);
                List<Tb_Agent> ls_agentMasterList = agentDAO.agentMasterList(agentFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(agentFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_agentMasterList.size(); i++) {
                        Tb_Agent tb_Agent = ls_agentMasterList.get(i);
                        sb.append("{\"id\":" + tb_Agent.getAgent_seq());
                        sb.append(",\"userdata\":{");
                        sb.append(" \"comp_seq\":\""+tb_Agent.getComp_seq()+"\"");
                        sb.append("}");                                    
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Agent.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Agent.getAgent_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Agent.getBiz_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Agent.getCorp_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Agent.getComp_ceo_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Agent.getTel_1()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Agent.getTel_2()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Agent.getUse_flag_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Agent.getIns_dt()) + "\"");

                        if (i == (ls_agentMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                ht.put("ResultSet", sb.toString());
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;                    
        
    }
    
    @Override
    public Hashtable paymtdSelect(PaymtdFormBean paymtdFormBean) throws Exception {

        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
       
        String total_count = this.paymtdDAO.paymtdRecordCount(paymtdFormBean);
       
        List ls_paymtdMasterList = this.paymtdDAO.paymtdMasterList(paymtdFormBean);
        
        //sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(paymtdFormBean.getStart_no())-1)+"\",\"rows\" : [");
        sb.append("{"+"\"rows\" : [");
        
        for (int i = 0; i < ls_paymtdMasterList.size(); i++) {
                Tb_Pay_Mtd tb_Paymtd = (Tb_Pay_Mtd) ls_paymtdMasterList.get(i);
                //NO,선택,결제수단명,일반/무이자,자체/대행,시작일,종료일,사용여부
                sb.append("{\"id\":" + tb_Paymtd.getPay_mtd_seq());
                sb.append(",\"data\":[");
                sb.append(" \"" + tb_Paymtd.getRnum() + "\"");
                sb.append(",\"" + Util.nullToString(tb_Paymtd.getPay_mtd_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Paymtd.getStart_dt()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Paymtd.getEnd_dt()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Paymtd.getUse_flag_nm()) + "\"");
                
                if (i == (ls_paymtdMasterList.size() - 1)) {
                        sb.append("]}");
                } else {
                        sb.append("]},");
                }
        }

        sb.append("]}");    
        
        ht.put("ResultSet", sb.toString());
        
        return ht;
    }    

    @Override
    public Hashtable onofftidSelect(TerminalFormBean terminalFormBean) throws Exception {
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
       
        String total_count = this.terminalDAO.terminalRecordCount(terminalFormBean);
       
        List ls_terminalMasterList = this.terminalDAO.terminalMasterList(terminalFormBean);
        
        
        sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(terminalFormBean.getStart_no())-1)+"\",\"rows\" : [");
        
        for (int i = 0; i < ls_terminalMasterList.size(); i++) {
                Tb_Onfftid_Mst tb_Terminal = (Tb_Onfftid_Mst) ls_terminalMasterList.get(i);
                
                //no, 결제채널, 결제TID명,결제TID,가맹점명, 가맹점번호, 결제상품명 ,상태
                sb.append("{\"id\":" + i);
                sb.append(",\"userdata\":{");
                sb.append("\"onfftid\":\""+tb_Terminal.getOnfftid()+"\"");
                sb.append("}");                
                sb.append(",\"data\":[");
                sb.append(" \"" + tb_Terminal.getRnum() + "\"");
                sb.append(",\"" + Util.nullToString(tb_Terminal.getPay_chn_cate_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Terminal.getOnfftid_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Terminal.getOnfftid()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Terminal.getMerch_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Terminal.getOnffmerch_no()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Terminal.getPay_chn_cate_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Terminal.getSvc_stat_nm()) + "\"");

                if (i == (ls_terminalMasterList.size() - 1)) {
                        sb.append("]}");
                } else {
                        sb.append("]},");
                }
        }

        sb.append("]}");    
        
        ht.put("ResultSet", sb.toString());
        
        return ht;
    }
    
}
