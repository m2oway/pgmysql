/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.popup.Service;

import com.onoffkorea.system.agent.Bean.AgentFormBean;
import com.onoffkorea.system.company.Bean.CompanyFormBean;
import com.onoffkorea.system.merchant.Bean.MerchantFormBean;
import com.onoffkorea.system.mid.Bean.MidFormBean;
import com.onoffkorea.system.paymtd.Bean.PaymtdFormBean;
import com.onoffkorea.system.terminal.Bean.TerminalFormBean;
import com.onoffkorea.system.tid.Bean.TidFormBean;
import java.util.Hashtable;

/**
 *
 * @author Administrator
 */
public interface PopupService {
    
    public Hashtable midSelect(MidFormBean midFormBean) throws Exception;

    public Hashtable tidSelect(TidFormBean tidFormBean) throws Exception;
    
    public Hashtable companySelect(CompanyFormBean companyFormBean) throws Exception;
    
    public Hashtable onoffmerchSelect(MerchantFormBean merchantFormBean) throws Exception;
    
    public Hashtable onofftidSelect(TerminalFormBean terminalFormBean) throws Exception;
    
    public Hashtable agentSelect(AgentFormBean agentFormBean) throws Exception;
    
    public Hashtable paymtdSelect(PaymtdFormBean paymtdFormBean) throws Exception;    
}
