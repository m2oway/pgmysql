/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.login.Bean;

/**
 *
 * @author hoyeongheo
 */
public class LoginFormBean {
    private String user_id;
    private String user_pwd;
    private String basicsiteflag;

    public String getBasicsiteflag() {
        return basicsiteflag;
    }

    public void setBasicsiteflag(String basicsiteflag) {
        this.basicsiteflag = basicsiteflag;
    }
    
    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_pwd() {
        return user_pwd;
    }

    public void setUser_pwd(String user_pwd) {
        this.user_pwd = user_pwd;
    }
    
    
}
