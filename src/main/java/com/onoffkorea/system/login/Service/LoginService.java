package com.onoffkorea.system.login.Service;


import com.onoffkorea.system.login.Vo.Tb_User;

public interface LoginService {
	
	//로그인
	public Tb_User login(String login_id, String login_pw)throws Exception;


    //트랜잭션 테스트
    public void transTest()throws Exception;

	
}
