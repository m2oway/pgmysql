package com.onoffkorea.system.login.Service;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onoffkorea.system.login.Dao.LoginDAO;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.login.Vo.Tb_User;
import org.springframework.transaction.annotation.Transactional;


@Service("loginService")
public class LoginServiceImpl implements LoginService{

    protected Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private LoginDAO loginDAO;

    @Override
    public Tb_User login(String login_id, String login_pw) throws Exception {
        
        logger.debug(">>>>>>>>>> login");
        
        Tb_User tb_user = loginDAO.login(login_id, login_pw);

        return tb_user;
    }


    @Override
    @Transactional
    public void transTest() throws Exception {

        loginDAO.doInsertMerchnet();

        loginDAO.doInsertMember();

    }


}
