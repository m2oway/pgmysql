package com.onoffkorea.system.login.Vo;

/**
 * Created by gimseonhui on 14. 10. 25..
 */
public class Tb_Merch_Info {


    private String merchant_seq;
    private String acq_cd;
    private String use_flag;
    private String del_flag;
    private String func_cate;
    private String merch_no;
    private String commision;
    private String start_dt;
    private String end_dt;
    private String commision2;
    private String ins_dt;
    private String mod_dt;
    private String ins_user;
    private String mod_user;
    private String comp_seq;
    private String installment;
    private String point_cate;
    private String point_commsion;
    private String bank_info;
    private String acc_no;

    public String getMerchant_seq() {
        return merchant_seq;
    }

    public void setMerchant_seq(String merchant_seq) {
        this.merchant_seq = merchant_seq;
    }

    public String getAcq_cd() {
        return acq_cd;
    }

    public void setAcq_cd(String acq_cd) {
        this.acq_cd = acq_cd;
    }

    public String getUse_flag() {
        return use_flag;
    }

    public void setUse_flag(String use_flag) {
        this.use_flag = use_flag;
    }

    public String getDel_flag() {
        return del_flag;
    }

    public void setDel_flag(String del_flag) {
        this.del_flag = del_flag;
    }

    public String getFunc_cate() {
        return func_cate;
    }

    public void setFunc_cate(String func_cate) {
        this.func_cate = func_cate;
    }

    public String getMerch_no() {
        return merch_no;
    }

    public void setMerch_no(String merch_no) {
        this.merch_no = merch_no;
    }

    public String getCommision() {
        return commision;
    }

    public void setCommision(String commision) {
        this.commision = commision;
    }

    public String getStart_dt() {
        return start_dt;
    }

    public void setStart_dt(String start_dt) {
        this.start_dt = start_dt;
    }

    public String getEnd_dt() {
        return end_dt;
    }

    public void setEnd_dt(String end_dt) {
        this.end_dt = end_dt;
    }

    public String getCommision2() {
        return commision2;
    }

    public void setCommision2(String commision2) {
        this.commision2 = commision2;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    public String getComp_seq() {
        return comp_seq;
    }

    public void setComp_seq(String comp_seq) {
        this.comp_seq = comp_seq;
    }

    public String getInstallment() {
        return installment;
    }

    public void setInstallment(String installment) {
        this.installment = installment;
    }

    public String getPoint_cate() {
        return point_cate;
    }

    public void setPoint_cate(String point_cate) {
        this.point_cate = point_cate;
    }

    public String getPoint_commsion() {
        return point_commsion;
    }

    public void setPoint_commsion(String point_commsion) {
        this.point_commsion = point_commsion;
    }

    public String getBank_info() {
        return bank_info;
    }

    public void setBank_info(String bank_info) {
        this.bank_info = bank_info;
    }

    public String getAcc_no() {
        return acc_no;
    }

    public void setAcc_no(String acc_no) {
        this.acc_no = acc_no;
    }
}
