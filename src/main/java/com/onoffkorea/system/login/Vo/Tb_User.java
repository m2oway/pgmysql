/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.login.Vo;

/**
 *
 * @author hoyeongheo
 */
public class Tb_User {
    private String user_seq  ;
    private String onfftid   ;
    private String user_id   ;
    private String user_pwd  ;
    private String user_nm   ;
    private String tel_no_1  ;
    private String tel_no_2  ;
    private String user_email;
    private String user_memo ;
    private String agent_seq ;
    private String org_seq   ;
    private String auth_seq  ;
    private String ins_dt    ;
    private String mod_dt    ;
    private String ins_user  ;
    private String mod_user  ;
    private String onffmerch_no;
    private String user_cate;
    private String merch_cncl_auth;
    private String tid_cncl_auth;

    public String getMerch_cncl_auth() {
        return merch_cncl_auth;
    }

    public void setMerch_cncl_auth(String merch_cncl_auth) {
        this.merch_cncl_auth = merch_cncl_auth;
    }

    public String getTid_cncl_auth() {
        return tid_cncl_auth;
    }

    public void setTid_cncl_auth(String tid_cncl_auth) {
        this.tid_cncl_auth = tid_cncl_auth;
    }
    
    
                                
    public String getUser_cate() {
        return user_cate;
    }

    public void setUser_cate(String user_cate) {
        this.user_cate = user_cate;
    }

    public String getOnffmerch_no() {
        return onffmerch_no;
    }

    public void setOnffmerch_no(String onffmerch_no) {
        this.onffmerch_no = onffmerch_no;
    }

    public String getUser_seq() {
        return user_seq;
    }

    public void setUser_seq(String user_seq) {
        this.user_seq = user_seq;
    }

    public String getOnfftid() {
        return onfftid;
    }

    public void setOnfftid(String onfftid) {
        this.onfftid = onfftid;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_pwd() {
        return user_pwd;
    }

    public void setUser_pwd(String user_pwd) {
        this.user_pwd = user_pwd;
    }

    public String getUser_nm() {
        return user_nm;
    }

    public void setUser_nm(String user_nm) {
        this.user_nm = user_nm;
    }

    public String getTel_no_1() {
        return tel_no_1;
    }

    public void setTel_no_1(String tel_no_1) {
        this.tel_no_1 = tel_no_1;
    }

    public String getTel_no_2() {
        return tel_no_2;
    }

    public void setTel_no_2(String tel_no_2) {
        this.tel_no_2 = tel_no_2;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_memo() {
        return user_memo;
    }

    public void setUser_memo(String user_memo) {
        this.user_memo = user_memo;
    }

    public String getAgent_seq() {
        return agent_seq;
    }

    public void setAgent_seq(String agent_seq) {
        this.agent_seq = agent_seq;
    }

    public String getOrg_seq() {
        return org_seq;
    }

    public void setOrg_seq(String org_seq) {
        this.org_seq = org_seq;
    }

    public String getAuth_seq() {
        return auth_seq;
    }

    public void setAuth_seq(String auth_seq) {
        this.auth_seq = auth_seq;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }
    
    
    
}
