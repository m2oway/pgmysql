package com.onoffkorea.system.login.Dao;


import com.onoffkorea.system.login.Vo.Tb_User;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.stereotype.Repository;


@Repository("loginDAO")
public class LoginDAOImpl extends SqlSessionDaoSupport implements LoginDAO{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Override
    public Tb_User login(String login_id, String login_pw) throws Exception {
        
        return (Tb_User) getSqlSession().selectOne("Login.getUserInfo", login_id);
        
    }

    @Override
    public void doInsertMember() throws Exception {

        getSqlSession().insert("Login.doInsertMember");

    }

    @Override
    public void doInsertMerchnet() throws Exception {

        getSqlSession().insert("Login.doInsertMerchnet");


    }

}
