package com.onoffkorea.system.login.Dao;

import com.onoffkorea.system.login.Vo.Tb_Merch_Info;
import com.onoffkorea.system.login.Vo.Tb_User;



public interface LoginDAO {
    
    public Tb_User login(String login_id, String login_pw) throws Exception;

    public void doInsertMember() throws Exception;

    public void doInsertMerchnet() throws Exception;

}
