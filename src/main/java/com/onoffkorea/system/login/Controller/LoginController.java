package com.onoffkorea.system.login.Controller;

import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.login.Bean.LoginFormBean;
import com.onoffkorea.system.login.Service.LoginService;
import com.onoffkorea.system.login.Vo.Tb_User;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@SessionAttributes("dentalHopitalSession")
//@RequestMapping("/login/loginForm")
@RequestMapping("/login/**")
public class LoginController {

    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private LoginService loginService;

    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
    }

    @ModelAttribute("loginFormBean")
    public LoginFormBean createloginFormBean() {
        return new LoginFormBean();
    }

//    @RequestMapping(value = "/loginForm", method = RequestMethod.GET)
    @RequestMapping(method=RequestMethod.GET)
    public void form() {
        ModelAndView mv = new ModelAndView();

    }

    // 로그인
    @RequestMapping(value = "/loginForm", method = RequestMethod.POST)
    //@RequestMapping(method=RequestMethod.POST)
    public String processSubmit(@Valid LoginFormBean loginFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        
        SiteSession siteSessionObj = null;

        HttpSession session = null;
        
        String returnUrl = null;

        if(loginFormBean != null){

            Tb_User tu =  loginService.login(loginFormBean.getUser_id(), loginFormBean.getUser_pwd());

            if(tu != null){

                if(tu.getUser_pwd().equals(loginFormBean.getUser_pwd())){

                    model.addAttribute("message", tu.getUser_email());

                    session = request.getSession(true);

                    siteSessionObj = new SiteSession();

                    siteSessionObj.setUser_seq(tu.getUser_seq());
                    siteSessionObj.setUser_id(tu.getUser_id());
                    siteSessionObj.setOnfftid(tu.getOnfftid());
                    siteSessionObj.setOnffmerch_no(tu.getOnffmerch_no());
                    siteSessionObj.setOrg_seq(tu.getOrg_seq());
                    siteSessionObj.setAgent_seq(tu.getAgent_seq());
                    siteSessionObj.setUser_cate(tu.getUser_cate());
                    
                    siteSessionObj.setSes_user_cate(tu.getUser_cate());
                    siteSessionObj.setSes_onffmerch_no(tu.getOnffmerch_no());
                    siteSessionObj.setSes_onfftid(tu.getOnfftid());
                    siteSessionObj.setSes_agent_seq(tu.getAgent_seq());
                    
                    siteSessionObj.setSes_merch_cncl_auth(tu.getMerch_cncl_auth());
                    siteSessionObj.setSes_tid_cncl_auth(tu.getTid_cncl_auth());

                    session.setAttribute("siteSessionObj",siteSessionObj);

                    model.addAttribute("message", "success");

//                    model.addAttribute("context", request.getContextPath());

                    return returnUrl;

                }else{
                    model.addAttribute("message", "fail");

                    return returnUrl;
                }

            }else{

                model.addAttribute("message", "fail");

                return returnUrl;

            }

        }else {

            return returnUrl;
        }
    }

    
    // 로그인
    @RequestMapping(value = "/loginForm2", method = RequestMethod.POST)
    public @ResponseBody String processSubmit2(@Valid LoginFormBean loginFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        
        SiteSession siteSessionObj = null;

        HttpSession session = null;
        
        String returnUrl = null;

        logger.debug("########## loginForm2 processSubmit2 ##################");

        if(loginFormBean != null){

            Tb_User tu =  loginService.login(loginFormBean.getUser_id(), loginFormBean.getUser_pwd());

            if(tu != null){

                logger.debug("########## loginForm2 ##################");

                logger.debug("########## getUser_pwd : "+tu.getUser_pwd());
                logger.debug("########## loginFormBean.getUser_pwd : "+loginFormBean.getUser_pwd());

                if(tu.getUser_pwd().equals(loginFormBean.getUser_pwd())){

                    model.addAttribute("message", tu.getUser_email());

                    session = request.getSession(true);

                    siteSessionObj = new SiteSession();
                    siteSessionObj.setUser_seq(tu.getUser_seq());
                    siteSessionObj.setUser_id(tu.getUser_id());
                    siteSessionObj.setOnfftid(tu.getOnfftid());
                    siteSessionObj.setOnffmerch_no(tu.getOnffmerch_no());
                    siteSessionObj.setOrg_seq(tu.getOrg_seq());
                    siteSessionObj.setAgent_seq(tu.getAgent_seq());                    
                    siteSessionObj.setUser_cate(tu.getUser_cate());
                    
                    siteSessionObj.setSes_user_cate(tu.getUser_cate());
                    siteSessionObj.setSes_onffmerch_no(tu.getOnffmerch_no());
                    siteSessionObj.setSes_onfftid(tu.getOnfftid());
                    siteSessionObj.setSes_agent_seq(tu.getAgent_seq());
                    
                    siteSessionObj.setSes_merch_cncl_auth(tu.getMerch_cncl_auth());
                    siteSessionObj.setSes_tid_cncl_auth(tu.getTid_cncl_auth());

                    session.setAttribute("siteSessionObj",siteSessionObj);

                    return "success";

                }else{
                    return "fail";
                }

            }else{

                return "fail";

            }

        }else {

            return returnUrl;
        }
    }    
    
    // 트랜잭션 롤백 테스트
    @RequestMapping(value = "/transTest",method = RequestMethod.GET)
    public String transTest(HttpServletRequest request, HttpServletResponse response) throws Exception {

        loginService.transTest();

        return null;
    }

    // 로그아웃
    @RequestMapping(value = "/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        session.invalidate();
        return "redirect:/";
    }

}
