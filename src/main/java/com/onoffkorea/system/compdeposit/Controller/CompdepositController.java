/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.compdeposit.Controller;

import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.mid.Bean.MidFormBean;
import com.onoffkorea.system.compdeposit.Bean.CompdepositFormBean;
import com.onoffkorea.system.compdeposit.Service.CompdepositService;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import common.web.servlet.BaseSpringMultiActionController;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/compdeposit/**")
public class CompdepositController extends BaseSpringMultiActionController{
        
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private CommonService commonService;
    
    @Autowired
    private CompdepositService compdepositService;    
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }    

    //입금정산 레이아웃
    @RequestMapping(method= RequestMethod.GET,value = "/compdepositLayout")
    public String compdepositLayout(@Valid MidFormBean midFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        return null;
        
    }
    
    //입금정산 조회
    @RequestMapping(method= RequestMethod.GET,value = "/compdepositMasterList")
    public String compdepositMasterFormList(@Valid CompdepositFormBean compdepositFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        
           SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        if((compdepositFormBean.getDeposit_start_dt() == null) || (compdepositFormBean.getDeposit_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DAY_OF_WEEK, 7);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            compdepositFormBean.setDeposit_start_dt(end_dt);
            compdepositFormBean.setDeposit_end_dt(start_dt);
        }
        
         if((compdepositFormBean.getApp_start_dt() == null) || (compdepositFormBean.getApp_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DATE, -1);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            compdepositFormBean.setApp_start_dt(start_dt);
            compdepositFormBean.setApp_end_dt(start_dt);
        }

        Hashtable ht_compdepositMasterList = compdepositService.compdepositMasterList(compdepositFormBean);
        String ls_compdepositMasterList = (String) ht_compdepositMasterList.get("ResultSet");
        
        Tb_Code_Main midPayMtdList = (Tb_Code_Main) ht_compdepositMasterList.get("midPayMtdList");
        Tb_Code_Main procFlagList = (Tb_Code_Main) ht_compdepositMasterList.get("procFlagList");
        
        SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
        Date start_date = formatter.parse(compdepositFormBean.getDeposit_start_dt());
        Date end_date   = formatter.parse(compdepositFormBean.getDeposit_end_dt());
        
        String start_dt = formatter2.format(start_date);
        String end_dt = formatter2.format(end_date);

        compdepositFormBean.setDeposit_start_dt(start_dt);
        compdepositFormBean.setDeposit_end_dt(end_dt);
        
        Date app_start_date = formatter.parse(compdepositFormBean.getApp_start_dt());
        Date app_end_date   = formatter.parse(compdepositFormBean.getApp_end_dt());
        
        String app_start_dt = formatter2.format(app_start_date);
        String app_end_dt = formatter2.format(app_end_date);

        compdepositFormBean.setApp_start_dt(app_start_dt);
        compdepositFormBean.setApp_end_dt(app_end_dt);
        
        model.addAttribute("ls_compdepositMasterList",ls_compdepositMasterList);     
        model.addAttribute("midPayMtdList",midPayMtdList);     
        model.addAttribute("procFlagList",procFlagList);             
        
        
        return null;
        
    }
    
    //입금정산 조회
    @RequestMapping(method= RequestMethod.POST,value = "/compdepositMasterList")
    @ResponseBody
    public String compdepositMasterList(@Valid CompdepositFormBean compdepositFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_compdepositMasterList = compdepositService.compdepositMasterList(compdepositFormBean);
        String ls_compdepositMasterList = (String) ht_compdepositMasterList.get("ResultSet");
        
        return ls_compdepositMasterList;
        
    }    
    
    //입금정산 상세 조회
    @RequestMapping(method= RequestMethod.GET,value = "/compdepositDetailList")
    public String compdepositDetailFormList(@Valid CompdepositFormBean compdepositFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_compdepositDetailList = compdepositService.compdepositDetailList(compdepositFormBean);
        String ls_compdepositDetailList = (String) ht_compdepositDetailList.get("ResultSet");
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_compdepositDetailList.get("payChnCateList");
        
        Tb_Code_Main acqCdList = (Tb_Code_Main) ht_compdepositDetailList.get("acqCdList");
        
        model.addAttribute("ls_compdepositDetailList",ls_compdepositDetailList);     
        model.addAttribute("payChnCateList",payChnCateList);             
        model.addAttribute("acqCdList",acqCdList);       
        
        return null;
        
    }
    
    //입금정산 조회
    @RequestMapping(method= RequestMethod.POST,value = "/compdepositDetailList")
    @ResponseBody
    public String compdepositDetailList(@Valid CompdepositFormBean compdepositFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_compdepositDetailList = compdepositService.compdepositDetailList(compdepositFormBean);
        String ls_compdepositDetailList = (String) ht_compdepositDetailList.get("ResultSet");
        
        return ls_compdepositDetailList;
        
    }        
    
    /*
     *  입금대사 확정 처리
     */
    @RequestMapping(value="/depositMetabolismConfirmProcess", method= RequestMethod.POST)
    @ResponseBody
    public String depositMetabolismConfirmProcess(@Valid CompdepositFormBean compdepositFormBean, Model model , HttpServletRequest request,@ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        compdepositFormBean.setUser_seq(siteSession.getUser_seq());

        Hashtable ht_depositMetabolismConfirmProcess = compdepositService.depositMetabolismConfirmProcess(compdepositFormBean);

        return (String)ht_depositMetabolismConfirmProcess.get("Result");
    }    
    
    /*
     *  입금대사 확정 취소 처리
     */
    @RequestMapping(value="/depositMetabolismCancelProcess", method= RequestMethod.POST)
    @ResponseBody
    public String depositMetabolismCancelProcess(@Valid CompdepositFormBean compdepositFormBean, Model model , HttpServletRequest request,@ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        compdepositFormBean.setUser_seq(siteSession.getUser_seq());

        Hashtable ht_depositMetabolismCancelProcess = compdepositService.depositMetabolismCancelProcess(compdepositFormBean);

        return (String)ht_depositMetabolismCancelProcess.get("Result");
    }    
    
    //입금정산 엑셀
    @RequestMapping(method= RequestMethod.POST,value = "/compdepositMasterListExcel")
    public void compdepositMasterListExcel(@Valid CompdepositFormBean compdepositFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, HttpServletResponse response) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        
        
        if((compdepositFormBean.getDeposit_start_dt() == null) || (compdepositFormBean.getDeposit_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DAY_OF_WEEK, 7);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            compdepositFormBean.setDeposit_start_dt(end_dt);
            compdepositFormBean.setDeposit_end_dt(start_dt);
            
                    
        }
        
         if((compdepositFormBean.getApp_start_dt() == null) || (compdepositFormBean.getApp_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DATE, -1);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            compdepositFormBean.setApp_start_dt(start_dt);
            compdepositFormBean.setApp_end_dt(start_dt);
            
        }
        

        String excelDownloadFilename = new String(getMessage("compdeposit.compdepositMasterListExcel.fileName"));
        
        compdepositService.compdepositMasterListExcel(compdepositFormBean, excelDownloadFilename, response); 

    }        
    
    //입금정산 상세내역 엑셀
    @RequestMapping(method= RequestMethod.POST,value = "/compdepositDetailListExcel")
    public void compdepositDetailListExcel(@Valid CompdepositFormBean compdepositFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, HttpServletResponse response) throws Exception { 

        String excelDownloadFilename = new String(getMessage("compdeposit.compdepositDetailListExcel.fileName"));
        
        compdepositService.compdepositDetailListExcel(compdepositFormBean, excelDownloadFilename, response); 

    }            

}
