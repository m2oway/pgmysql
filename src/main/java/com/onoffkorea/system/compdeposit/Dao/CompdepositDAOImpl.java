/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.compdeposit.Dao;

import com.onoffkorea.system.acq_result.Vo.Tb_Acq_Result;
import com.onoffkorea.system.compdeposit.Bean.CompdepositFormBean;
import com.onoffkorea.system.compdeposit.Vo.Tb_Comp_Deposit;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("compdepositDAO")
public class CompdepositDAOImpl extends SqlSessionDaoSupport implements CompdepositDAO{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    //입금정산 조회
    @Override
    public List<Tb_Comp_Deposit> compdepositMasterList(CompdepositFormBean compdepositFormBean) {
        
        return (List)getSqlSession().selectList("Compdeposit.compdepositMasterList",compdepositFormBean);
        
    }

    //입금정산 조회
    @Override
    public List<Tb_Acq_Result> compdepositDetailList(CompdepositFormBean compdepositFormBean) {
        
        return (List)getSqlSession().selectList("Compdeposit.compdepositDetailList",compdepositFormBean);
        
    }

    //입금확정
    @Override
    public Integer depositMetabolismConfirmProcess(Tb_Comp_Deposit tb_Mid_Deposit) {
        
        return (Integer)getSqlSession().insert("Compdeposit.depositMetabolismConfirmProcess",tb_Mid_Deposit);
        
    }

    //입금확정취소
    @Override
    public Integer depositMetabolismCancelProcess(CompdepositFormBean compdepositFormBean) {
        
        return (Integer)getSqlSession().delete("Compdeposit.depositMetabolismCancelProcess",compdepositFormBean);
        
    }

    //입금정산 상세 내역 카운트
    @Override
    public Integer compdepositMasterListCount(CompdepositFormBean compdepositFormBean) {
        
        return (Integer)getSqlSession().selectOne("Compdeposit.compdepositDetailListCount",compdepositFormBean);
        
    }

    //입반내역 입금확정 처리
    @Override
    public void depositAcqDetailProcFlagYUpdate(CompdepositFormBean compdepositFormBean) {
        
        getSqlSession().update("Compdeposit.depositAcqDetailProcFlagYUpdate",compdepositFormBean);
        
    }
    
    //통장입금내역 입금확정 처리
    @Override
    public void depositBankDetailProcFlagYUpdate(CompdepositFormBean compdepositFormBean) {
        
        getSqlSession().update("Compdeposit.depositBankDetailProcFlagYUpdate",compdepositFormBean);
        
    }     
    
    //입반내역 입금확정취소 처리
    @Override
    public void depositAcqDetailProcFlagNUpdate(CompdepositFormBean compdepositFormBean) {
        
        getSqlSession().update("Compdeposit.depositAcqDetailProcFlagNUpdate",compdepositFormBean);
        
    }
    
    //통장입금내역 입금확정취소
    @Override
    public void depositBankDetailProcFlagNUpdate(CompdepositFormBean compdepositFormBean) {
        
        getSqlSession().update("Compdeposit.depositBankDetailProcFlagNUpdate",compdepositFormBean);
        
    }         

    @Override
    public List compdepositMasterListExcel(CompdepositFormBean compdepositFormBean) {

        return (List)getSqlSession().selectList("Compdeposit.compdepositMasterListExcel",compdepositFormBean);
        
    }
    
    @Override
    public List compdepositDetailListExcel(CompdepositFormBean compdepositFormBean) {
        
        return (List)getSqlSession().selectList("Compdeposit.compdepositDetailListExcel",compdepositFormBean);
        
    }    
    
}
