/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.compdeposit.Dao;

import com.onoffkorea.system.acq_result.Vo.Tb_Acq_Result;
import com.onoffkorea.system.compdeposit.Bean.CompdepositFormBean;
import com.onoffkorea.system.compdeposit.Vo.Tb_Comp_Deposit;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface CompdepositDAO {

    public List<Tb_Comp_Deposit> compdepositMasterList(CompdepositFormBean compdepositFormBean);

    public List<Tb_Acq_Result> compdepositDetailList(CompdepositFormBean compdepositFormBean);

    public Integer depositMetabolismConfirmProcess(Tb_Comp_Deposit tb_Mid_Deposit);

    public Integer depositMetabolismCancelProcess(CompdepositFormBean compdepositFormBean);

    public Integer compdepositMasterListCount(CompdepositFormBean compdepositFormBean);

    public void depositAcqDetailProcFlagYUpdate(CompdepositFormBean compdepositFormBean);

    public void depositBankDetailProcFlagYUpdate(CompdepositFormBean compdepositFormBean);

    public void depositAcqDetailProcFlagNUpdate(CompdepositFormBean compdepositFormBean);

    public void depositBankDetailProcFlagNUpdate(CompdepositFormBean compdepositFormBean);

    public List compdepositMasterListExcel(CompdepositFormBean compdepositFormBean);

    public List compdepositDetailListExcel(CompdepositFormBean compdepositFormBean);
    
}
