/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.compdeposit.Service;

import com.onoffkorea.system.acq_result.Vo.Tb_Acq_Result;
import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.compdeposit.Bean.CompdepositFormBean;
import com.onoffkorea.system.compdeposit.Dao.CompdepositDAO;
import com.onoffkorea.system.compdeposit.Vo.Tb_Comp_Deposit;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service("compdepositService")
public class CompdepositServiceImpl implements CompdepositService{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private CommonService commonService;    
    
    @Autowired
    private CompdepositDAO compdepositDAO;       

    @Override
    public Hashtable compdepositMasterList(CompdepositFormBean compdepositFormBean) {

        Hashtable ht = new Hashtable();

        try {
            
                List<Tb_Comp_Deposit> ls_compdepositMasterList = compdepositDAO.compdepositMasterList(compdepositFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"rows\" : [");
                
                for (int i = 0; i < ls_compdepositMasterList.size(); i++) {
                        Tb_Comp_Deposit tb_Mid_Deposit = ls_compdepositMasterList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"deposit_dt\":\""+Util.nullToString(tb_Mid_Deposit.getDeposit_dt())+"\"");
                        sb.append(",\"merch_no\":\""+Util.nullToString(tb_Mid_Deposit.getMerch_no())+"\"");
                        sb.append(",\"pay_mtd\":\""+Util.nullToString(tb_Mid_Deposit.getPay_mtd())+"\"");
                        sb.append(",\"seq\":\""+Util.nullToString(tb_Mid_Deposit.getDeposit_seq())+"\"");
                        sb.append(",\"proc_flag\":\""+Util.nullToString(tb_Mid_Deposit.getProc_flag())+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Mid_Deposit.getDeposit_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getApp_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getPay_mtd_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getTid()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getChkacqamt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getAcq_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getDeposit_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getCommission()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getT2acq_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getApp_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getCncl_amt()) + "\"");
                        

                        

                        if (i == (ls_compdepositMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
                commonCodeSearchFormBean.setMain_code("MID_PAY_MTD");
                Tb_Code_Main midPayMtdList = commonService.codeSearch(commonCodeSearchFormBean);
                
                commonCodeSearchFormBean.setMain_code("PROC_FLAG");
                Tb_Code_Main procFlagList = commonService.codeSearch(commonCodeSearchFormBean);
                
                ht.put("midPayMtdList", midPayMtdList);
                ht.put("procFlagList", procFlagList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;               
        
    }

    //입금정산 상세 조회
    @Override
    public Hashtable compdepositDetailList(CompdepositFormBean compdepositFormBean) {

        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = compdepositDAO.compdepositMasterListCount(compdepositFormBean);
                List<Tb_Acq_Result> ls_compdepositDetailList = compdepositDAO.compdepositDetailList(compdepositFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(compdepositFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_compdepositDetailList.size(); i++) {
                        Tb_Acq_Result tb_Acq_Result = ls_compdepositDetailList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Acq_Result.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getPay_type_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getMerch_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getRecord_cl_cd_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getAcq_cd_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getSal_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getAprv_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getTot_trx_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getVat_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getInstallment()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getAcq_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getPay_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getCmms_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getPay_amt()) + "\"");

                        if (i == (ls_compdepositDetailList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);
                
                commonCodeSearchFormBean.setMain_code("ACQ_CD");
                Tb_Code_Main acqCdList = commonService.codeSearch(commonCodeSearchFormBean);
                
                ht.put("payChnCateList", payChnCateList);
                ht.put("acqCdList", acqCdList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;                   
        
    }

    //입금확정
    @Override
    @Transactional
    public Hashtable depositMetabolismConfirmProcess(CompdepositFormBean compdepositFormBean) {
        
        Hashtable ht = new Hashtable();
        
        Integer updateResult = 0;

        //입금확정 등록        
        List<Tb_Comp_Deposit> ls_compdepositMasterList = compdepositDAO.compdepositMasterList(compdepositFormBean);
        Tb_Comp_Deposit tb_Mid_Deposit = ls_compdepositMasterList.get(0);
        tb_Mid_Deposit.setUser_seq(compdepositFormBean.getUser_seq());

        updateResult = compdepositDAO.depositMetabolismConfirmProcess(tb_Mid_Deposit);        
        
        //입반내역 입금확정
        compdepositDAO.depositAcqDetailProcFlagYUpdate(compdepositFormBean);
        
        //통장입금내역 입금확정  
        compdepositDAO.depositBankDetailProcFlagYUpdate(compdepositFormBean);

        if(updateResult == 0){
            ht.put("Result","fail");
        }else{
            ht.put("Result","success");
        }

        return ht;        
        
    }

    //입금확정취소
    @Override
    @Transactional
    public Hashtable depositMetabolismCancelProcess(CompdepositFormBean compdepositFormBean) {
        
        Hashtable ht = new Hashtable();

        Integer updateResult = 0;
        
        //입금확정 삭제
        updateResult = compdepositDAO.depositMetabolismCancelProcess(compdepositFormBean);
        
        //입반내역 입금확정취소
        compdepositDAO.depositAcqDetailProcFlagNUpdate(compdepositFormBean);
        
        //통장입금내역 입금확정취소  
        compdepositDAO.depositBankDetailProcFlagNUpdate(compdepositFormBean);        
         
        if(updateResult == 0){
            ht.put("Result","fail");
        }else{
            ht.put("Result","success");
        }

        return ht;        
        
    }

    @Override
    public void compdepositMasterListExcel(CompdepositFormBean compdepositFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
              
                List ls_compdepositMasterListExcel = compdepositDAO.compdepositMasterListExcel(compdepositFormBean);
                
                StringBuffer compdepositMasterListExcel = Util.makeData(ls_compdepositMasterListExcel); 
        
                Util.exceldownload(compdepositMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }                
        
    }

    @Override
    public void compdepositDetailListExcel(CompdepositFormBean compdepositFormBean, String excelDownloadFilename, HttpServletResponse response) {
        
        try {
              
                List ls_compdepositDetailListExcel = compdepositDAO.compdepositDetailListExcel(compdepositFormBean);
                
                StringBuffer compdepositDetailListExcel = Util.makeData(ls_compdepositDetailListExcel); 
        
                Util.exceldownload(compdepositDetailListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }                     
        
    }
    
}
