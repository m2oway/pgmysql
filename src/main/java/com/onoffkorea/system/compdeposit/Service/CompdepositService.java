/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.compdeposit.Service;

import com.onoffkorea.system.compdeposit.Bean.CompdepositFormBean;
import java.util.Hashtable;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public interface CompdepositService {

    public Hashtable compdepositMasterList(CompdepositFormBean compdepositFormBean);

    public Hashtable compdepositDetailList(CompdepositFormBean compdepositFormBean);

    public Hashtable depositMetabolismConfirmProcess(CompdepositFormBean compdepositFormBean);

    public Hashtable depositMetabolismCancelProcess(CompdepositFormBean compdepositFormBean);

    public void compdepositMasterListExcel(CompdepositFormBean compdepositFormBean, String excelDownloadFilename, HttpServletResponse response);

    public void compdepositDetailListExcel(CompdepositFormBean compdepositFormBean, String excelDownloadFilename, HttpServletResponse response);
    
}
