/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.tranfile.ksnet.tranfile;

import com.onoffkorea.system.common.util.Util;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author MoonBong
 */
public class tranfilecvs 
{
    public String strtargetFileNam = null;
    
    /**
     * 파라미터 체크 메소드
     */
    public String getNullToSpace(String param) {
        return (param == null) ? "" : param.trim();
    }

    public String getNullToZero(String param) {
        return (param == null) ? "0" : param.trim();
    }    

    public tranfilecvs() {
    }
    
    
        public void closeResultSet(ResultSet rs)
    {
        try
        {
            if ( rs != null ) rs.close();
        }
        catch(Exception ex)
        {
        }
    }
    
    public void closePstmt(PreparedStatement psmt)
    {
        try
        {
            if ( psmt != null ) psmt.close();
        }
        catch(Exception ex)
        {
        }
    }    
    
    public void closeConnection(Connection con)
    {
        try
        {
            if ( con != null ) con.close();
        }
        catch(Exception ex)
        {
        }
    }   
    
   /**
     * @param strFileName Filepath + filename info
     */    
    public tranfilecvs(String strFileName) {
        strtargetFileNam = strFileName;
    }
    
    public String getStrtargetFileNam() {
        return strtargetFileNam;
    }

    public void setStrtargetFileNam(String strtargetFileNam) {
        this.strtargetFileNam = strtargetFileNam;
    }
    
    public String run(Connection conn)
    {
        String strReturnval = null;
        
        FileInputStream fis = null;
        BufferedReader br = null;
        
        PreparedStatement pstmt = null;
        PreparedStatement pstmt2 = null;
        ResultSet rs = null;
        ResultSet rs2 = null;
        
        try
        {
            fis = new FileInputStream(strtargetFileNam);
            String sLine = "";
            
            int readCount =0 ;
            
            br = new BufferedReader(new InputStreamReader(fis));
            while( (sLine = br.readLine()) != null )
            {
                readCount++;
                System.out.println("SLINE : ["+sLine+"]");
                
                if(readCount > 1)
                {
                    String[] arrStrLineInfo = sLine.split(",", -1);                    
                    
                    String strCol1	=	(Util.nullToString(arrStrLineInfo[0]).trim()).replaceAll("=\"", "").replace("\"", "");	//온오프구분
                    String strCol2	=	(Util.nullToString(arrStrLineInfo[1]).trim()).replaceAll("=\"", "").replace("\"", "");	//상점ID
                    String strCol3	=	(Util.nullToString(arrStrLineInfo[2]).trim()).replaceAll("=\"", "").replace("\"", "");	//거래번호
                    String strCol4	=	(Util.nullToString(arrStrLineInfo[3]).trim()).replaceAll("=\"", "").replace("\"", "");	//주문번호
                    String strCol5	=	(Util.nullToString(arrStrLineInfo[4]).trim()).replaceAll("=\"", "").replace("\"", "");	//승인구분
                    String strCol6	=	(Util.nullToString(arrStrLineInfo[5]).trim()).replaceAll("=\"", "").replace("\"", "");	//매입사
                    String strCol7	=	(Util.nullToString(arrStrLineInfo[6]).trim()).replaceAll("=\"", "").replace("\"", "");	//카드번호
                    String strCol8	=	(Util.nullToString(arrStrLineInfo[7]).trim()).replaceAll("=\"", "").replace("\"", "");	//금액
                    String strCol9	=	(Util.nullToString(arrStrLineInfo[8]).trim()).replaceAll("=\"", "").replace("\"", "");	//할부
                    String strCol10	=	(Util.nullToString(arrStrLineInfo[9]).trim()).replaceAll("=\"", "").replace("\"", "");	//승인번호
                    String strCol11	=	(Util.nullToString(arrStrLineInfo[10]).trim()).replaceAll("=\"", "").replace("\"", "");	//승인일자
                    String strCol12	=	(Util.nullToString(arrStrLineInfo[11]).trim()).replaceAll("=\"", "").replace("\"", "");	//승인시간
                    String strCol13	=	(Util.nullToString(arrStrLineInfo[12]).trim()).replaceAll("=\"", "").replace("\"", "");	//취소일자
                    String strCol14	=	(Util.nullToString(arrStrLineInfo[13]).trim()).replaceAll("=\"", "").replace("\"", "");	//취소시간
                    String strCol15	=	(Util.nullToString(arrStrLineInfo[14]).trim()).replaceAll("=\"", "").replace("\"", "");	//매입요청일
                    String strCol16	=	(Util.nullToString(arrStrLineInfo[15]).trim()).replaceAll("=\"", "").replace("\"", "");	//매입완료일
                    String strCol17	=	(Util.nullToString(arrStrLineInfo[16]).trim()).replaceAll("=\"", "").replace("\"", "");	//매입상태
                    String strCol18	=	(Util.nullToString(arrStrLineInfo[17]).trim()).replaceAll("=\"", "").replace("\"", "");	//반송사유
                    String strCol19	=	(Util.nullToString(arrStrLineInfo[18]).trim()).replaceAll("=\"", "").replace("\"", "");	//주문자명
                    String strCol20	=	(Util.nullToString(arrStrLineInfo[19]).trim()).replaceAll("=\"", "").replace("\"", "");	//이자구분
                    String strCol21	=	(Util.nullToString(arrStrLineInfo[20]).trim()).replaceAll("=\"", "").replace("\"", "");	//발급사
                    String strCol22	=	(Util.nullToString(arrStrLineInfo[21]).trim()).replaceAll("=\"", "").replace("\"", "");	//제품명
                    String strCol23	=	(Util.nullToString(arrStrLineInfo[22]).trim()).replaceAll("=\"", "").replace("\"", "");	//카드구분
                    String strCol24	=	(Util.nullToString(arrStrLineInfo[23]).trim()).replaceAll("=\"", "").replace("\"", "");	//통화구분
                    String strCol25	=	(Util.nullToString(arrStrLineInfo[24]).trim()).replaceAll("=\"", "").replace("\"", "");	//간편결제
                    
                    //수신된 정보로 가맹점 정보를 검색
                    StringBuffer sb_onffTidInfo = new StringBuffer();

                    sb_onffTidInfo.append(" select m.onfftid, m.onffmerch_no, m.pay_chn_cate, n.terminal_no, n.merch_no ");
                    sb_onffTidInfo.append(" from TB_ONFFTID_MST m, ");
                    sb_onffTidInfo.append("       ( ");
                    sb_onffTidInfo.append("           SELECT b.pay_mtd_seq, a.start_dt, a.end_dt, b.APP_ISS_CD, b.TID_MTD, b.TERMINAL_NO,b.PAY_MTD,b.MERCH_NO  ");
                    sb_onffTidInfo.append("            FROM TB_PAY_MTD a ");
                    sb_onffTidInfo.append("                      ,TB_PAY_MTD_DETAIL b ");
                    sb_onffTidInfo.append("            WHERE  ");
                    sb_onffTidInfo.append("            a.PAY_MTD_SEQ = b.PAY_MTD_SEQ ");
                    sb_onffTidInfo.append("            and b.DEL_FLAG  = 'N' ");
                    sb_onffTidInfo.append("            and b.USE_FLAG = 'Y' ");
                    sb_onffTidInfo.append("            and b.DEL_FLAG  = 'N' ");
                    sb_onffTidInfo.append("            and b.USE_FLAG = 'Y' ");
                    sb_onffTidInfo.append("            and a.start_dt <= DATE_FORMAT(now(),'%Y%m%d') ");
                    sb_onffTidInfo.append("            and a.end_dt  >= DATE_FORMAT(now(),'%Y%m%d') ");
                    sb_onffTidInfo.append("       ) n ");
                    sb_onffTidInfo.append(" where  ");
                    sb_onffTidInfo.append(" m.PAY_MTD_SEQ = n.pay_mtd_seq        ");
                    sb_onffTidInfo.append(" and m.del_flag = 'N'  ");
                    sb_onffTidInfo.append(" and n.TERMINAL_NO = ? ");

                    pstmt = conn.prepareStatement(sb_onffTidInfo.toString());

                    pstmt.setString(1, strCol2);

                    rs = pstmt.executeQuery();

                    String strOnffTid = "";
                    String strOnffMerchNo = "";
                    String strPayChnCate = "";

                    if(rs != null && rs.next())
                    {
                        strOnffTid = getNullToSpace(rs.getString(1));
                        strOnffMerchNo = getNullToSpace(rs.getString(2));
                        strPayChnCate =  getNullToSpace(rs.getString(3));
                    }

                    closeResultSet(rs);
                    closePstmt(pstmt);
                    
                    String strTpCms="";
                    //승인이 가맹점정보가 있을시 수수료 정보를 가져온다.
                    //if(!strOnffTid.equals("") && !strOnffMerchNo.equals("") && r_noti_type.equals("10"))
                    if(!strOnffTid.equals("") && !strOnffMerchNo.equals(""))
                    {
                        //가맹점 정보에 대한 수수료 정보를 가져온다.
                        StringBuffer sb_onffcms = new StringBuffer();
                        sb_onffcms.append(" SELECT ONFFTID_CMS_SEQ, ONFFTID, COMMISSION, START_DT, END_DT ");
                        sb_onffcms.append("  FROM TB_ONFFTID_COMMISSION ");
                        sb_onffcms.append("  WHERE ONFFTID = ? ");
                        sb_onffcms.append("  and DEL_FLAG  = 'N' ");
                        sb_onffcms.append("  and USE_FLAG = 'Y' ");
                        sb_onffcms.append("  and START_DT <= ? ");
                        sb_onffcms.append("  and END_DT >= ? ");

                        String strTpAppDt = strCol11;

                        pstmt = conn.prepareStatement(sb_onffcms.toString());

                        pstmt.setString(1, strOnffTid);
                        pstmt.setString(2, strTpAppDt);
                        pstmt.setString(3, strTpAppDt);
                        rs = pstmt.executeQuery();  

                        if(rs != null && rs.next())
                        {
                            strTpCms = rs.getString(3);
                        }
                        closeResultSet(rs);
                        closePstmt(pstmt);
                    }                     
                    
                    
                    //승인 경우
                    if(strCol5.equals("승인"))
                    {
                        //신용카드일 경우
                        //if(r_pay_type.equals("11"))
                        {
                            //이미 입력거래가 있는경우
                            StringBuffer sbTranChkSql = new StringBuffer();
                            sbTranChkSql.append(" select count(1) cnt ");
                            sbTranChkSql.append(" from VW_TRAN_CARDPG ");
                            sbTranChkSql.append(" where pg_seq = ? ");
                            sbTranChkSql.append(" and TID = ? ");
                            sbTranChkSql.append(" and CARD_NUM = ? ");
                            sbTranChkSql.append(" and APP_DT = ? ");
                            sbTranChkSql.append(" and APP_NO = ? ");
                            sbTranChkSql.append(" and TOT_AMT = ? ");
                            sbTranChkSql.append(" and massagetype='10' ");

                            pstmt = conn.prepareStatement(sbTranChkSql.toString());

                            pstmt.setString(1, strCol3);
                            pstmt.setString(2, strCol2);
                            pstmt.setString(3, strCol7);
                            pstmt.setString(4, strCol11);
                            pstmt.setString(5, strCol10);
                            pstmt.setString(6, strCol8);                            

                            rs = pstmt.executeQuery();

                            int int_appChk_cnt = 0;

                            if(rs != null && rs.next())
                            {
                                int_appChk_cnt = rs.getInt("cnt");
                            }

                            closeResultSet(rs);
                            closePstmt(pstmt);                    

                            String strNotResult = null;
                            //접수된 승인건이 없는 경우
                            if(int_appChk_cnt == 0)
                            {

                                //발급사코드 변환
                                StringBuffer sb_isscd = new StringBuffer();
                                sb_isscd.append(" select main_code, detail_code, code_nm detailcode_nm ");
                                sb_isscd.append(" from TB_CODE_DETAIL ");
                                sb_isscd.append(" where main_code = 'ISS_CD' ");
                                sb_isscd.append(" and use_flag = 'Y' ");
                                sb_isscd.append(" and del_flag='N' ");
                                sb_isscd.append(" and detail_code = f_get_iss_cd('KSNETPG', ?) ");

                                pstmt = conn.prepareStatement(sb_isscd.toString());
                                pstmt.setString(1, null);

                                rs = pstmt.executeQuery();                            

                                String strOnffIssCd = null;
                                String strOnffIssNm = null;

                                if(rs != null && rs.next())
                                {
                                    strOnffIssCd = rs.getString(2);
                                    strOnffIssNm = rs.getString(3);
                                }    
                                else
                                {
                                    strOnffIssCd = "091";
                                    strOnffIssNm = "기타카드";
                                }
                                closeResultSet(rs);
                                closePstmt(pstmt);

                                //매입사코드변환
                                StringBuffer sb_acqcd = new StringBuffer();
                                sb_acqcd.append(" select main_code, detail_code, code_nm detailcode_nm ");
                                sb_acqcd.append(" from TB_CODE_DETAIL ");
                                sb_acqcd.append(" where main_code = 'ISS_CD' ");
                                sb_acqcd.append(" and use_flag = 'Y' ");
                                sb_acqcd.append(" and del_flag='N' ");
                                sb_acqcd.append(" and detail_code = f_get_iss_cd('KSNETPG', ?) ");

                                pstmt = conn.prepareStatement(sb_acqcd.toString());
                                pstmt.setString(1, null);

                                rs = pstmt.executeQuery(); 

                                String strOnffAcqCd = null;
                                String strOnffAcqNm = null;

                                if(rs != null && rs.next())
                                {
                                    strOnffAcqCd = rs.getString(2);
                                    strOnffAcqNm = rs.getString(3);
                                }
                                else
                                {
                                    strOnffAcqCd = "091";
                                    strOnffAcqNm = "기타카드";
                                }
                                closeResultSet(rs);
                                closePstmt(pstmt);

                                
                                //수수료를 계산한다.
                                double dbl_TpCms = Double.parseDouble(strTpCms)/Double.parseDouble("100");
                                double dbl_TpApp_mat = Double.parseDouble(strCol8);
                                double dbl_onfftid_cms = Math.floor(dbl_TpCms * dbl_TpApp_mat);
                                double dbl_onfftid_payamt = dbl_TpApp_mat - dbl_onfftid_cms;


                                //거래일련번호를 생성한다.
                                String strTpTranseq = null;
                                String sb_TpTranseqSql = "select concat(DATE_FORMAT(now(),'%Y%m%d%H%i%S'), LPAD(f_get_sequence('TRANSEQ'),8,'0'))";
                                pstmt = conn.prepareStatement(sb_TpTranseqSql);
                                rs = pstmt.executeQuery();
                                if(rs != null && rs.next())
                                {
                                    strTpTranseq = rs.getString(1);
                                }                

                                closeResultSet(rs);
                                closePstmt(pstmt);

                                //해당거래정보를 insert 한다.
                                StringBuffer sb_instraninfo = new StringBuffer();
                                sb_instraninfo.append(" insert ");
                                sb_instraninfo.append(" into TB_TRAN_CARDPG_"+strCol11.substring(2, 6) + " ( ");
                                sb_instraninfo.append(" TRAN_SEQ			 ");
                                sb_instraninfo.append(" ,MASSAGETYPE		 ");
                                sb_instraninfo.append(" ,CARD_NUM			 ");
                                sb_instraninfo.append(" ,APP_DT				 ");
                                sb_instraninfo.append(" ,APP_TM				 ");
                                sb_instraninfo.append(" ,APP_NO				 ");
                                sb_instraninfo.append(" ,TOT_AMT			 ");	
                                sb_instraninfo.append(" ,PG_SEQ				 ");
                                sb_instraninfo.append(" ,PAY_CHN_CATE		 ");	
                                sb_instraninfo.append(" ,TID				 ");	
                                sb_instraninfo.append(" ,ONFFTID			 ");	
                                sb_instraninfo.append(" ,WCC				 ");	
                                sb_instraninfo.append(" ,EXPIRE_DT			 ");
                                sb_instraninfo.append(" ,CARD_CATE			 ");
                                sb_instraninfo.append(" ,ORDER_SEQ			 ");
                                sb_instraninfo.append(" ,TRAN_DT			 ");
                                sb_instraninfo.append(" ,TRAN_TM			 ");	
                                sb_instraninfo.append(" ,TRAN_CATE			 ");
                                sb_instraninfo.append(" ,FREE_INST_FLAG		 ");
                                sb_instraninfo.append(" ,TAX_AMT			 ");	
                                sb_instraninfo.append(" ,SVC_AMT			 ");	
                                sb_instraninfo.append(" ,CURRENCY			 ");
                                sb_instraninfo.append(" ,INSTALLMENT		 ");	
                                sb_instraninfo.append(" ,ISS_CD				 ");
                                sb_instraninfo.append(" ,ISS_NM				 ");
                                sb_instraninfo.append(" ,APP_ISS_CD			 ");
                                sb_instraninfo.append(" ,APP_ISS_NM			 ");
                                sb_instraninfo.append(" ,ACQ_CD				 ");
                                sb_instraninfo.append(" ,ACQ_NM			 ");
                                sb_instraninfo.append(" ,APP_ACQ_CD			 ");
                                sb_instraninfo.append(" ,APP_ACQ_NM			 ");
                                sb_instraninfo.append(" ,MERCH_NO			 ");
                                sb_instraninfo.append(" ,ORG_MERCH_NO		 ");
                                sb_instraninfo.append(" ,RESULT_CD			 ");
                                sb_instraninfo.append(" ,RESULT_MSG			 ");
                                sb_instraninfo.append(" ,ORG_APP_DD			 ");
                                sb_instraninfo.append(" ,ORG_APP_NO			 ");
                                sb_instraninfo.append(" ,CNCL_REASON		 ");	
                                sb_instraninfo.append(" ,TRAN_STATUS		 ");	
                                sb_instraninfo.append(" ,RESULT_STATUS		 ");
                                sb_instraninfo.append(" ,TRAN_STEP			 ");
                                sb_instraninfo.append(" ,CNCL_DT			 ");	
                                sb_instraninfo.append(" ,ACQ_DT				 ");
                                sb_instraninfo.append(" ,ACQ_RESULT_CD		 ");
                                sb_instraninfo.append(" ,HOLDOFF_DT			 ");
                                sb_instraninfo.append(" ,HOLDON_DT			 ");
                                sb_instraninfo.append(" ,PAY_DT				 ");
                                sb_instraninfo.append(" ,PAY_AMT			 ");	
                                sb_instraninfo.append(" ,COMMISION			 ");
                                sb_instraninfo.append(" ,ONOFFTID_PAY_AMT	 ");	
                                sb_instraninfo.append(" ,ONOFFTID_COMMISION	 ");
                                sb_instraninfo.append(" ,CLIENT_IP			 ");
                                sb_instraninfo.append(" ,USER_TYPE			 ");
                                sb_instraninfo.append(" ,MEMB_USER_NO		 ");
                                sb_instraninfo.append(" ,USER_ID				 ");
                                sb_instraninfo.append(" ,USER_NM			 ");
                                sb_instraninfo.append(" ,USER_MAIL			 ");
                                sb_instraninfo.append(" ,USER_PHONE1		 ");
                                sb_instraninfo.append(" ,USER_PHONE2		 ");
                                sb_instraninfo.append(" ,USER_ADDR			 ");
                                sb_instraninfo.append(" ,PRODUCT_TYPE		 ");
                                sb_instraninfo.append(" ,PRODUCT_NM		 ");
                                sb_instraninfo.append(" ,FILLER				 ");
                                sb_instraninfo.append(" ,FILLER2				 ");
                                sb_instraninfo.append(" ,TERM_FILLER1		 ");
                                sb_instraninfo.append(" ,TERM_FILLER2		 ");	
                                sb_instraninfo.append(" ,TERM_FILLER3		 ");	
                                sb_instraninfo.append(" ,TERM_FILLER4		 ");	
                                sb_instraninfo.append(" ,TERM_FILLER5		 ");	
                                sb_instraninfo.append(" ,TERM_FILLER6		 ");	
                                sb_instraninfo.append(" ,TERM_FILLER7		 ");	
                                sb_instraninfo.append(" ,TERM_FILLER8		 ");	
                                sb_instraninfo.append(" ,TERM_FILLER9		 ");	
                                sb_instraninfo.append(" ,TERM_FILLER10		 ");
                                sb_instraninfo.append(" ,TRAD_CHK_FLAG		 ");
                                sb_instraninfo.append(" ,ACC_CHK_FLAG		 ");
                                sb_instraninfo.append(" ,INS_DT				 ");
                                sb_instraninfo.append(" ,MOD_DT				 ");
                                sb_instraninfo.append(" ,INS_USER			 ");	
                                sb_instraninfo.append(" ,MOD_USER			 ");
                                sb_instraninfo.append(" ,ORG_PG_SEQ			 ");
                                sb_instraninfo.append(" ,ONFFMERCH_NO	 ");
                                sb_instraninfo.append(" ,auth_pw ");
                                sb_instraninfo.append(" ,auth_value ");
                                sb_instraninfo.append(" ,app_card_num ");
                                sb_instraninfo.append(" )  ");
                                sb_instraninfo.append(" values ( ");
                                sb_instraninfo.append("  ? ");//tran_seq			
                                sb_instraninfo.append(" ,? ");//massagetype			
                                sb_instraninfo.append(" ,? ");//card_num			
                                sb_instraninfo.append(" ,? ");//app_dt			
                                sb_instraninfo.append(" ,? ");//app_tm			
                                sb_instraninfo.append(" ,? ");//app_no			
                                sb_instraninfo.append(" ,? ");//tot_amt			
                                sb_instraninfo.append(" ,? ");//pg_seq			
                                sb_instraninfo.append(" ,? ");//pay_chn_cate		
                                sb_instraninfo.append(" ,? ");//tid				
                                sb_instraninfo.append(" ,? ");//onfftid			
                                sb_instraninfo.append(" ,? ");//wcc				
                                sb_instraninfo.append(" ,? ");//expire_dt			
                                sb_instraninfo.append(" ,? ");//card_cate			
                                sb_instraninfo.append(" ,? ");//order_seq			
                                sb_instraninfo.append(" ,? ");//tran_dt			
                                sb_instraninfo.append(" ,? ");//tran_tm			
                                sb_instraninfo.append(" ,? ");//tran_cate			
                                sb_instraninfo.append(" ,? ");//free_inst_flag		
                                sb_instraninfo.append(" ,? ");//tax_amt			
                                sb_instraninfo.append(" ,? ");//svc_amt			
                                sb_instraninfo.append(" ,? ");//currency			
                                sb_instraninfo.append(" ,? ");//installment			
                                sb_instraninfo.append(" ,? ");//iss_cd			
                                sb_instraninfo.append(" ,? ");//iss_nm			
                                sb_instraninfo.append(" ,? ");//app_iss_cd			
                                sb_instraninfo.append(" ,? ");//app_iss_nm			
                                sb_instraninfo.append(" ,? ");//acq_cd			
                                sb_instraninfo.append(" ,? ");//acq_nm			
                                sb_instraninfo.append(" ,? ");//app_acq_cd			
                                sb_instraninfo.append(" ,? ");//app_acq_nm			
                                sb_instraninfo.append(" ,? ");//merch_no			
                                sb_instraninfo.append(" ,? ");//org_merch_no		
                                sb_instraninfo.append(" ,? ");//result_cd			
                                sb_instraninfo.append(" ,? ");//result_msg			
                                sb_instraninfo.append(" ,? ");//org_app_dd			
                                sb_instraninfo.append(" ,? ");//org_app_no			
                                sb_instraninfo.append(" ,? ");//cncl_reason			
                                sb_instraninfo.append(" ,? ");//tran_status			
                                sb_instraninfo.append(" ,? ");//result_status		
                                sb_instraninfo.append(" ,? ");//tran_step			
                                sb_instraninfo.append(" ,? ");//cncl_dt			
                                sb_instraninfo.append(" ,? ");//acq_dt			
                                sb_instraninfo.append(" ,? ");//acq_result_cd
                                sb_instraninfo.append(" ,? ");//holdoff_dt
                                sb_instraninfo.append(" ,? ");//holdon_dt
                                sb_instraninfo.append(" ,? ");//pay_dt
                                sb_instraninfo.append(" ,? ");//pay_amt
                                sb_instraninfo.append(" ,? ");//commision
                                sb_instraninfo.append(" ,? ");//onofftid_pay_amt
                                sb_instraninfo.append(" ,? ");//onofftid_commision
                                sb_instraninfo.append(" ,? ");//client_ip
                                sb_instraninfo.append(" ,? ");//user_type
                                sb_instraninfo.append(" ,? ");//memb_user_no
                                sb_instraninfo.append(" ,? ");//user_id
                                sb_instraninfo.append(" ,? ");//user_nm
                                sb_instraninfo.append(" ,? ");//user_mail
                                sb_instraninfo.append(" ,? ");//user_phone1
                                sb_instraninfo.append(" ,? ");//user_phone2
                                sb_instraninfo.append(" ,? ");//user_addr
                                sb_instraninfo.append(" ,? ");//product_type
                                sb_instraninfo.append(" ,? ");//product_nm
                                sb_instraninfo.append(" ,? ");//filler
                                sb_instraninfo.append(" ,? ");//filler2
                                sb_instraninfo.append(" ,? ");//term_filler1
                                sb_instraninfo.append(" ,? ");//term_filler2
                                sb_instraninfo.append(" ,? ");//term_filler3
                                sb_instraninfo.append(" ,? ");//term_filler4
                                sb_instraninfo.append(" ,? ");//term_filler5
                                sb_instraninfo.append(" ,? ");//term_filler6
                                sb_instraninfo.append(" ,? ");//term_filler7
                                sb_instraninfo.append(" ,? ");//term_filler8
                                sb_instraninfo.append(" ,? ");//term_filler9
                                sb_instraninfo.append(" ,? ");//term_filler10
                                sb_instraninfo.append(" ,? ");//trad_chk_flag
                                sb_instraninfo.append(" ,? ");//acc_chk_flag
                                sb_instraninfo.append("  , DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                                sb_instraninfo.append("  , DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                                sb_instraninfo.append(" ,? ");//ins_user			
                                sb_instraninfo.append(" ,? ");//mod_user			
                                sb_instraninfo.append(" ,? ");//org_pg_seq			
                                sb_instraninfo.append(" ,? ");//onffmerch_no		
                                sb_instraninfo.append(" ,? ");//auth_pw		  
                                sb_instraninfo.append(" ,? ");//auth_value		  
                                sb_instraninfo.append(" ,? ");//app_card_num	
                                sb_instraninfo.append(" ) ");          

                                pstmt = conn.prepareStatement(sb_instraninfo.toString());

                                pstmt.setString(1,strTpTranseq);//tran_seq				, jdbcType=VARCHAR} 
                                pstmt.setString(2,"10");//massagetype			, jdbcType=VARCHAR} 
                                pstmt.setString(3,  strCol7);//card_num			, jdbcType=VARCHAR} 
                                pstmt.setString(4,  strCol11);//app_dt				, jdbcType=VARCHAR}
                                pstmt.setString(5,  strCol12);//app_tm				 , jdbcType=VARCHAR}
                                pstmt.setString(6,  strCol10);//app_no				, jdbcType=VARCHAR}
                                pstmt.setString(7,  strCol8);//tot_amt				 , jdbcType=INTEGER }
                                pstmt.setString(8,  strCol4);//pg_seq				, jdbcType=VARCHAR}
                                pstmt.setString(9,  strPayChnCate);//pay_chn_cate			 , jdbcType=VARCHAR}
                                pstmt.setString(10, strCol2);//tid					, jdbcType=VARCHAR}
                                pstmt.setString(11, strOnffTid);//onfftid				, jdbcType=VARCHAR}
                                pstmt.setString(12, null);//wcc					, jdbcType=VARCHAR}
                                pstmt.setString(13,"0000");//expire_dt			, jdbcType=VARCHAR}
                                pstmt.setString(14, null);//card_cate			  , jdbcType=VARCHAR}
                                pstmt.setString(15, null);//order_seq			  , jdbcType=VARCHAR}
                                pstmt.setString(16, strCol11);//tran_dt				, jdbcType=VARCHAR}
                                pstmt.setString(17, strCol12);//tran_tm				, jdbcType=VARCHAR}
                                //pstmt.setString(18,r_noti_type);//tran_cate			  , jdbcType=VARCHAR}
                                pstmt.setString(18,"20");//tran_cate			  , jdbcType=VARCHAR}
                                pstmt.setString(19, null);//free_inst_flag		    , jdbcType=VARCHAR}
                                pstmt.setDouble(20,0);//tax_amt				, jdbcType=INTEGER }
                                pstmt.setDouble(21,0);//svc_amt				, jdbcType=INTEGER }
                                pstmt.setString(22,null);//currency			 , jdbcType=VARCHAR}
                                pstmt.setString(23,strCol9);//installment			   , jdbcType=VARCHAR}
                                pstmt.setString(24, strOnffIssCd);//iss_cd				, jdbcType=VARCHAR}
                                pstmt.setString(25, strOnffIssNm);//iss_nm				 , jdbcType=VARCHAR}
                                pstmt.setString(26, null);//app_iss_cd			 , jdbcType=VARCHAR}
                                pstmt.setString(27, strCol21);//app_iss_nm			 , jdbcType=VARCHAR}
                                pstmt.setString(28, null);//acq_cd				, jdbcType=VARCHAR}
                                pstmt.setString(29, strOnffAcqNm);//acq_nm				, jdbcType=VARCHAR}
                                pstmt.setString(30, null);//app_acq_cd			  , jdbcType=VARCHAR}
                                pstmt.setString(31, strCol6);//app_acq_nm			   , jdbcType=VARCHAR}
                                pstmt.setString(32, strCol2);//merch_no			  , jdbcType=VARCHAR}
                                pstmt.setString(33, strCol2);//org_merch_no			  , jdbcType=VARCHAR}
                                pstmt.setString(34, "0000");//result_cd			 , jdbcType=VARCHAR}
                                pstmt.setString(35, "성공");//result_msg			   , jdbcType=VARCHAR}
                                pstmt.setString(36,null);//org_app_dd			  , jdbcType=VARCHAR}
                                pstmt.setString(37,null);//org_app_no			   , jdbcType=VARCHAR}
                                pstmt.setString(38,null);//cncl_reason			, jdbcType=VARCHAR}
                                pstmt.setString(39,"00");//tran_status			   , jdbcType=VARCHAR}
                                pstmt.setString(40,"00");//result_status		     , jdbcType=VARCHAR}
                                pstmt.setString(41,"00");//tran_step			 , jdbcType=VARCHAR}
                                pstmt.setString(42,null);//cncl_dt				, jdbcType=VARCHAR}
                                pstmt.setString(43,null);//acq_dt				, jdbcType=VARCHAR}
                                pstmt.setString(44,null);//acq_result_cd		     , jdbcType=VARCHAR}
                                pstmt.setString(45,null);//holdoff_dt			  , jdbcType=VARCHAR}
                                pstmt.setString(46,null);//holdon_dt			  , jdbcType=VARCHAR}
                                pstmt.setString(47,null);//pay_dt				, jdbcType=VARCHAR}
                                pstmt.setString(48,null);//pay_amt				, jdbcType=INTEGER }
                                pstmt.setString(49,null);//commision			  , jdbcType=INTEGER }
                                pstmt.setDouble(50,dbl_onfftid_payamt);//onofftid_pay_amt		, jdbcType=INTEGER }
                                pstmt.setDouble(51,dbl_onfftid_cms);//onofftid_commision		, jdbcType=INTEGER }
                                pstmt.setString(52,null);//client_ip				, jdbcType=VARCHAR}
                                pstmt.setString(53,null);//user_type			, jdbcType=VARCHAR}
                                pstmt.setString(54,null);//memb_user_no		, jdbcType=VARCHAR}
                                pstmt.setString(55,null);//user_id				, jdbcType=VARCHAR}
                                pstmt.setString(56,null);//user_nm				, jdbcType=VARCHAR}
                                pstmt.setString(57,null);//user_mail			    , jdbcType=VARCHAR}
                                pstmt.setString(58,null);//user_phone1			 , jdbcType=VARCHAR}
                                pstmt.setString(59,null);//user_phone2			 , jdbcType=VARCHAR}
                                pstmt.setString(60,null);//user_addr			, jdbcType=VARCHAR}
                                pstmt.setString(61,null);//product_type		  , jdbcType=VARCHAR}
                                pstmt.setString(62,null);//product_nm			 , jdbcType=VARCHAR}
                                pstmt.setString(63,null);//filler				   , jdbcType=VARCHAR}
                                pstmt.setString(64,null);//filler2				   , jdbcType=VARCHAR}
                                pstmt.setString(65,null);//term_filler1			, jdbcType=VARCHAR}
                                pstmt.setString(66,null);//term_filler2			, jdbcType=VARCHAR}
                                pstmt.setString(67,null);//term_filler3			, jdbcType=VARCHAR}
                                pstmt.setString(68,null);//term_filler4			, jdbcType=VARCHAR}
                                pstmt.setString(69,null);//term_filler5			, jdbcType=VARCHAR}
                                pstmt.setString(70,null);//term_filler6				, jdbcType=VARCHAR}
                                pstmt.setString(71,null);//term_filler7			  , jdbcType=VARCHAR}
                                pstmt.setString(72,null);//term_filler8			, jdbcType=VARCHAR}
                                pstmt.setString(73,null);//term_filler9			, jdbcType=VARCHAR}
                                pstmt.setString(74,null);//term_filler10			, jdbcType=VARCHAR}
                                pstmt.setString(75,"0");//trad_chk_flag		  , jdbcType=VARCHAR}
                                pstmt.setString(76,"0");//acc_chk_flag			, jdbcType=VARCHAR}
                                pstmt.setString(77,"0");//ins_user				     , jdbcType=VARCHAR}
                                pstmt.setString(78,"0");//mod_user			, jdbcType=VARCHAR}
                                pstmt.setString(79,null);//org_pg_seq			, jdbcType=VARCHAR}
                                pstmt.setString(80,strOnffMerchNo);//onffmerch_no		  , jdbcType=VARCHAR}
                                pstmt.setString(81,null);//auth_pw		  , jdbcType=VARCHAR}
                                pstmt.setString(82,null);//auth_value		  , jdbcType=VARCHAR}   
                                pstmt.setString(83,strCol7);//app_card_num		  , jdbcType=VARCHAR}                

                                int int_insResult = pstmt.executeUpdate();                               
                                closeResultSet(rs);
                                closePstmt(pstmt);

                            }
                        }
                    }
                    //취소일경우
                    else if(strCol5.equals("취소"))
                    {
                         //카드취소일 경우
                        //if(r_pay_type.equals("11"))
                        {
                            //이미 입력거래가 있는경우
                            StringBuffer sbTranChkSql = new StringBuffer();
                            sbTranChkSql.append(" select count(1) cnt ");
                            sbTranChkSql.append(" from VW_TRAN_CARDPG ");
                            sbTranChkSql.append(" where pg_seq = ? ");
                            sbTranChkSql.append(" and TID = ? ");
                            sbTranChkSql.append(" and CARD_NUM = ? ");
                            sbTranChkSql.append(" and APP_DT = ? ");
                            sbTranChkSql.append(" and APP_NO = ? ");
                            sbTranChkSql.append(" and TOT_AMT = ? ");
                            sbTranChkSql.append(" and massagetype='40' ");
                            sbTranChkSql.append(" and RESULT_STATUS='00' ");

                            pstmt = conn.prepareStatement(sbTranChkSql.toString());

                            pstmt.setString(1, strCol3);//pg번호
                            pstmt.setString(2, strCol2);//tid
                            pstmt.setString(3, strCol7);//카드번호
                            pstmt.setString(4, strCol13);//승인일
                            pstmt.setString(5, strCol10);//승인번호
                            pstmt.setString(6, strCol8);//총금액           

                            rs = pstmt.executeQuery();

                            int int_CnclChk_cnt = 0;

                            if(rs != null && rs.next())
                            {
                                int_CnclChk_cnt = rs.getInt("cnt");
                            }

                            closeResultSet(rs);
                            closePstmt(pstmt);                    
                           
                            String strNotResult = null;
                            //접수된 승인건이 없는 경우
                            if(int_CnclChk_cnt == 0)
                            {
                                //원거래를 가져온다.
                                StringBuffer sb_orgtraninfoSql = new StringBuffer();

                                sb_orgtraninfoSql.append(" select  b.comp_nm, b.biz_no, b.corp_no, b.comp_ceo_nm, b.comp_tel1, b.tax_flag ,b.merch_nm,  b.app_chk_amt, b.addr_1, b.addr_2 ");
                                sb_orgtraninfoSql.append("         ,a.tran_seq,a.massagetype,a.card_num ");
                                sb_orgtraninfoSql.append("         ,a.app_dt ");
                                sb_orgtraninfoSql.append("         ,a.app_tm         ");
                                sb_orgtraninfoSql.append("         ,a.app_no,a.tot_amt,a.pg_seq,a.tid ");
                                sb_orgtraninfoSql.append("         ,a.pay_chn_cate,a.onfftid,a.onffmerch_no,a.wcc ");
                                sb_orgtraninfoSql.append("         ,a.expire_dt,a.card_cate,a.order_seq ");
                                sb_orgtraninfoSql.append("         ,a.tran_dt,a.tran_tm,a.tran_cate,a.free_inst_flag ");
                                sb_orgtraninfoSql.append("         ,a.tax_amt,a.svc_amt,a.currency ");
                                sb_orgtraninfoSql.append("         ,a.installment,a.iss_cd,a.iss_nm ");
                                sb_orgtraninfoSql.append("         ,a.app_iss_cd,a.app_iss_nm,a.acq_cd ");
                                sb_orgtraninfoSql.append("         ,a.acq_nm,a.app_acq_cd,a.app_acq_nm ");
                                sb_orgtraninfoSql.append("         ,a.merch_no,a.org_merch_no,a.result_cd,a.result_msg ");
                                sb_orgtraninfoSql.append("         ,a.tran_status,a.result_status ");
                                sb_orgtraninfoSql.append("         ,a.org_app_dd,a.org_app_no,a.cncl_reason ");
                                sb_orgtraninfoSql.append("         ,a.tran_step,a.cncl_dt,a.acq_dt,a.acq_result_cd ");
                                sb_orgtraninfoSql.append("         ,a.holdoff_dt,a.holdon_dt,a.pay_dt,a.pay_amt ");
                                sb_orgtraninfoSql.append("         ,a.commision,a.client_ip,a.user_type ");
                                sb_orgtraninfoSql.append("         ,a.memb_user_no,a.user_id,a.user_nm ");
                                sb_orgtraninfoSql.append("         ,a.user_mail,a.user_phone1,a.user_phone2 ");
                                sb_orgtraninfoSql.append("         ,a.user_addr,a.product_type,a.product_nm ");
                                sb_orgtraninfoSql.append("         ,a.filler,a.filler2,a.term_filler1,a.term_filler2 ");
                                sb_orgtraninfoSql.append("         ,a.term_filler3,a.term_filler4,a.term_filler5 ");
                                sb_orgtraninfoSql.append("         ,a.term_filler6,a.term_filler7,a.term_filler8 ");
                                sb_orgtraninfoSql.append("         ,a.term_filler9,a.term_filler10,a.trad_chk_flag ");
                                sb_orgtraninfoSql.append("         ,a.acc_chk_flag,a.ins_dt,a.mod_dt,a.ins_user,a.mod_user,a.onofftid_pay_amt,a.onofftid_commision,a.tb_nm, a.app_card_num	 ");
                                sb_orgtraninfoSql.append(" from VW_TRAN_CARDPG a ");
                                sb_orgtraninfoSql.append("          ,( ");
                                sb_orgtraninfoSql.append("               SELECT x1.comp_nm, x1.BIZ_NO, x1.CORP_NO, x1.COMP_CEO_NM, x1.COMP_TEL1, x1.addr_1, x1.addr_2, x1.TAX_FLAG , x.ONFFMERCH_NO, x.MERCH_NM,  x.APP_CHK_AMT ");
                                sb_orgtraninfoSql.append("                FROM  TB_ONFFMERCH_MST x, ");
                                sb_orgtraninfoSql.append("                          TB_COMPANY x1 ");
                                sb_orgtraninfoSql.append("                WHERE                  ");
                                sb_orgtraninfoSql.append("                          x.del_flag = 'N' ");
                                sb_orgtraninfoSql.append("                        AND x.SVC_STAT = '00' ");
                                sb_orgtraninfoSql.append("                        AND x1.del_flag = 'N' ");
                                sb_orgtraninfoSql.append("                        AND x1.use_flag = 'Y'                 ");
                                sb_orgtraninfoSql.append("                        AND  X.COMP_SEQ = x1.comp_seq ");
                                sb_orgtraninfoSql.append("          ) b ");
                                sb_orgtraninfoSql.append(" where ");
                                sb_orgtraninfoSql.append("      a.ONFFMERCH_NO = b.ONFFMERCH_NO ");
                                sb_orgtraninfoSql.append(" and       ");
                                sb_orgtraninfoSql.append("     a.pg_seq =? ");
                                sb_orgtraninfoSql.append(" and a.TID = ? ");
                                sb_orgtraninfoSql.append(" and a.CARD_NUM = ? ");
                                sb_orgtraninfoSql.append(" and a.APP_DT = ? ");
                                sb_orgtraninfoSql.append(" and a.APP_NO = ? ");
                                sb_orgtraninfoSql.append(" and a.TOT_AMT = ? ");                                
                                sb_orgtraninfoSql.append(" and    a.massagetype='10' ");
                                sb_orgtraninfoSql.append(" and    a.result_status='00' ");

                                pstmt2 = conn.prepareStatement(sb_orgtraninfoSql.toString());
                                   
                                pstmt.setString(1, strCol3);//pg번호
                                pstmt.setString(2, strCol2);//tid
                                pstmt.setString(3, strCol7);//카드번호
                                pstmt.setString(4, strCol11);//승인일
                                pstmt.setString(5, strCol10);//승인번호
                                pstmt.setString(6, strCol8);//총금액       
                                
                                rs2 = pstmt2.executeQuery();

                                //원거래정보
                                //원거래정보가 있는 경우
                                if(rs2 != null && rs2.next())
                                {
                                    //거래일련번호를 생성한다.
                                    String strTpTranseq = null;
                                    String sb_TpTranseqSql = "select concat(DATE_FORMAT(now(),'%Y%m%d%H%i%S'), LPAD(f_get_sequence('TRANSEQ'),8,'0'))";
                                    pstmt = conn.prepareStatement(sb_TpTranseqSql);
                                    rs = pstmt.executeQuery();
                                    if(rs != null && rs.next())
                                    {
                                        strTpTranseq = rs.getString(1);
                                    }                

                                    closeResultSet(rs);
                                    closePstmt(pstmt);

                                    //취소정보를 insert한다.
                                    StringBuffer sb_instraninfo = new StringBuffer();
                                    sb_instraninfo.append(" insert ");
                                    sb_instraninfo.append(" into TB_TRAN_CARDPG_"+strCol13.substring(2, 6) + " ( ");
                                    sb_instraninfo.append(" TRAN_SEQ			 ");
                                    sb_instraninfo.append(" ,MASSAGETYPE		 ");
                                    sb_instraninfo.append(" ,CARD_NUM			 ");
                                    sb_instraninfo.append(" ,APP_DT				 ");
                                    sb_instraninfo.append(" ,APP_TM				 ");
                                    sb_instraninfo.append(" ,APP_NO				 ");
                                    sb_instraninfo.append(" ,TOT_AMT			 ");	
                                    sb_instraninfo.append(" ,PG_SEQ				 ");
                                    sb_instraninfo.append(" ,PAY_CHN_CATE		 ");	
                                    sb_instraninfo.append(" ,TID				 ");	
                                    sb_instraninfo.append(" ,ONFFTID			 ");	
                                    sb_instraninfo.append(" ,WCC				 ");	
                                    sb_instraninfo.append(" ,EXPIRE_DT			 ");
                                    sb_instraninfo.append(" ,CARD_CATE			 ");
                                    sb_instraninfo.append(" ,ORDER_SEQ			 ");
                                    sb_instraninfo.append(" ,TRAN_DT			 ");
                                    sb_instraninfo.append(" ,TRAN_TM			 ");	
                                    sb_instraninfo.append(" ,TRAN_CATE			 ");
                                    sb_instraninfo.append(" ,FREE_INST_FLAG		 ");
                                    sb_instraninfo.append(" ,TAX_AMT			 ");	
                                    sb_instraninfo.append(" ,SVC_AMT			 ");	
                                    sb_instraninfo.append(" ,CURRENCY			 ");
                                    sb_instraninfo.append(" ,INSTALLMENT		 ");	
                                    sb_instraninfo.append(" ,ISS_CD				 ");
                                    sb_instraninfo.append(" ,ISS_NM				 ");
                                    sb_instraninfo.append(" ,APP_ISS_CD			 ");
                                    sb_instraninfo.append(" ,APP_ISS_NM			 ");
                                    sb_instraninfo.append(" ,ACQ_CD				 ");
                                    sb_instraninfo.append(" ,ACQ_NM			 ");
                                    sb_instraninfo.append(" ,APP_ACQ_CD			 ");
                                    sb_instraninfo.append(" ,APP_ACQ_NM			 ");
                                    sb_instraninfo.append(" ,MERCH_NO			 ");
                                    sb_instraninfo.append(" ,ORG_MERCH_NO		 ");
                                    sb_instraninfo.append(" ,RESULT_CD			 ");
                                    sb_instraninfo.append(" ,RESULT_MSG			 ");
                                    sb_instraninfo.append(" ,ORG_APP_DD			 ");
                                    sb_instraninfo.append(" ,ORG_APP_NO			 ");
                                    sb_instraninfo.append(" ,CNCL_REASON		 ");	
                                    sb_instraninfo.append(" ,TRAN_STATUS		 ");	
                                    sb_instraninfo.append(" ,RESULT_STATUS		 ");
                                    sb_instraninfo.append(" ,TRAN_STEP			 ");
                                    sb_instraninfo.append(" ,CNCL_DT			 ");	
                                    sb_instraninfo.append(" ,ACQ_DT				 ");
                                    sb_instraninfo.append(" ,ACQ_RESULT_CD		 ");
                                    sb_instraninfo.append(" ,HOLDOFF_DT			 ");
                                    sb_instraninfo.append(" ,HOLDON_DT			 ");
                                    sb_instraninfo.append(" ,PAY_DT				 ");
                                    sb_instraninfo.append(" ,PAY_AMT			 ");	
                                    sb_instraninfo.append(" ,COMMISION			 ");
                                    sb_instraninfo.append(" ,ONOFFTID_PAY_AMT	 ");	
                                    sb_instraninfo.append(" ,ONOFFTID_COMMISION	 ");
                                    sb_instraninfo.append(" ,CLIENT_IP			 ");
                                    sb_instraninfo.append(" ,USER_TYPE			 ");
                                    sb_instraninfo.append(" ,MEMB_USER_NO		 ");
                                    sb_instraninfo.append(" ,USER_ID				 ");
                                    sb_instraninfo.append(" ,USER_NM			 ");
                                    sb_instraninfo.append(" ,USER_MAIL			 ");
                                    sb_instraninfo.append(" ,USER_PHONE1		 ");
                                    sb_instraninfo.append(" ,USER_PHONE2		 ");
                                    sb_instraninfo.append(" ,USER_ADDR			 ");
                                    sb_instraninfo.append(" ,PRODUCT_TYPE		 ");
                                    sb_instraninfo.append(" ,PRODUCT_NM		 ");
                                    sb_instraninfo.append(" ,FILLER				 ");
                                    sb_instraninfo.append(" ,FILLER2				 ");
                                    sb_instraninfo.append(" ,TERM_FILLER1		 ");
                                    sb_instraninfo.append(" ,TERM_FILLER2		 ");	
                                    sb_instraninfo.append(" ,TERM_FILLER3		 ");	
                                    sb_instraninfo.append(" ,TERM_FILLER4		 ");	
                                    sb_instraninfo.append(" ,TERM_FILLER5		 ");	
                                    sb_instraninfo.append(" ,TERM_FILLER6		 ");	
                                    sb_instraninfo.append(" ,TERM_FILLER7		 ");	
                                    sb_instraninfo.append(" ,TERM_FILLER8		 ");	
                                    sb_instraninfo.append(" ,TERM_FILLER9		 ");	
                                    sb_instraninfo.append(" ,TERM_FILLER10		 ");
                                    sb_instraninfo.append(" ,TRAD_CHK_FLAG		 ");
                                    sb_instraninfo.append(" ,ACC_CHK_FLAG		 ");
                                    sb_instraninfo.append(" ,INS_DT				 ");
                                    sb_instraninfo.append(" ,MOD_DT				 ");
                                    sb_instraninfo.append(" ,INS_USER			 ");	
                                    sb_instraninfo.append(" ,MOD_USER			 ");
                                    sb_instraninfo.append(" ,ORG_PG_SEQ			 ");
                                    sb_instraninfo.append(" ,ONFFMERCH_NO	 ");
                                    sb_instraninfo.append(" ,auth_pw ");
                                    sb_instraninfo.append(" ,auth_value ");
                                    sb_instraninfo.append(" ,app_card_num ");
                                    sb_instraninfo.append(" )  ");
                                    sb_instraninfo.append(" values ( ");
                                    sb_instraninfo.append("  ? ");//tran_seq			
                                    sb_instraninfo.append(" ,? ");//massagetype			
                                    sb_instraninfo.append(" ,? ");//card_num			
                                    sb_instraninfo.append(" ,? ");//app_dt			
                                    sb_instraninfo.append(" ,? ");//app_tm			
                                    sb_instraninfo.append(" ,? ");//app_no			
                                    sb_instraninfo.append(" ,? ");//tot_amt			
                                    sb_instraninfo.append(" ,? ");//pg_seq			
                                    sb_instraninfo.append(" ,? ");//pay_chn_cate		
                                    sb_instraninfo.append(" ,? ");//tid				
                                    sb_instraninfo.append(" ,? ");//onfftid			
                                    sb_instraninfo.append(" ,? ");//wcc				
                                    sb_instraninfo.append(" ,? ");//expire_dt			
                                    sb_instraninfo.append(" ,? ");//card_cate			
                                    sb_instraninfo.append(" ,? ");//order_seq			
                                    sb_instraninfo.append(" ,? ");//tran_dt			
                                    sb_instraninfo.append(" ,? ");//tran_tm			
                                    sb_instraninfo.append(" ,? ");//tran_cate			
                                    sb_instraninfo.append(" ,? ");//free_inst_flag		
                                    sb_instraninfo.append(" ,? ");//tax_amt			
                                    sb_instraninfo.append(" ,? ");//svc_amt			
                                    sb_instraninfo.append(" ,? ");//currency			
                                    sb_instraninfo.append(" ,? ");//installment			
                                    sb_instraninfo.append(" ,? ");//iss_cd			
                                    sb_instraninfo.append(" ,? ");//iss_nm			
                                    sb_instraninfo.append(" ,? ");//app_iss_cd			
                                    sb_instraninfo.append(" ,? ");//app_iss_nm			
                                    sb_instraninfo.append(" ,? ");//acq_cd			
                                    sb_instraninfo.append(" ,? ");//acq_nm			
                                    sb_instraninfo.append(" ,? ");//app_acq_cd			
                                    sb_instraninfo.append(" ,? ");//app_acq_nm			
                                    sb_instraninfo.append(" ,? ");//merch_no			
                                    sb_instraninfo.append(" ,? ");//org_merch_no		
                                    sb_instraninfo.append(" ,? ");//result_cd			
                                    sb_instraninfo.append(" ,? ");//result_msg			
                                    sb_instraninfo.append(" ,? ");//org_app_dd			
                                    sb_instraninfo.append(" ,? ");//org_app_no			
                                    sb_instraninfo.append(" ,? ");//cncl_reason			
                                    sb_instraninfo.append(" ,? ");//tran_status			
                                    sb_instraninfo.append(" ,? ");//result_status		
                                    sb_instraninfo.append(" ,? ");//tran_step			
                                    sb_instraninfo.append(" ,? ");//cncl_dt			
                                    sb_instraninfo.append(" ,? ");//acq_dt			
                                    sb_instraninfo.append(" ,? ");//acq_result_cd
                                    sb_instraninfo.append(" ,? ");//holdoff_dt
                                    sb_instraninfo.append(" ,? ");//holdon_dt
                                    sb_instraninfo.append(" ,? ");//pay_dt
                                    sb_instraninfo.append(" ,? ");//pay_amt
                                    sb_instraninfo.append(" ,? ");//commision
                                    sb_instraninfo.append(" ,? ");//onofftid_pay_amt
                                    sb_instraninfo.append(" ,? ");//onofftid_commision
                                    sb_instraninfo.append(" ,? ");//client_ip
                                    sb_instraninfo.append(" ,? ");//user_type
                                    sb_instraninfo.append(" ,? ");//memb_user_no
                                    sb_instraninfo.append(" ,? ");//user_id
                                    sb_instraninfo.append(" ,? ");//user_nm
                                    sb_instraninfo.append(" ,? ");//user_mail
                                    sb_instraninfo.append(" ,? ");//user_phone1
                                    sb_instraninfo.append(" ,? ");//user_phone2
                                    sb_instraninfo.append(" ,? ");//user_addr
                                    sb_instraninfo.append(" ,? ");//product_type
                                    sb_instraninfo.append(" ,? ");//product_nm
                                    sb_instraninfo.append(" ,? ");//filler
                                    sb_instraninfo.append(" ,? ");//filler2
                                    sb_instraninfo.append(" ,? ");//term_filler1
                                    sb_instraninfo.append(" ,? ");//term_filler2
                                    sb_instraninfo.append(" ,? ");//term_filler3
                                    sb_instraninfo.append(" ,? ");//term_filler4
                                    sb_instraninfo.append(" ,? ");//term_filler5
                                    sb_instraninfo.append(" ,? ");//term_filler6
                                    sb_instraninfo.append(" ,? ");//term_filler7
                                    sb_instraninfo.append(" ,? ");//term_filler8
                                    sb_instraninfo.append(" ,? ");//term_filler9
                                    sb_instraninfo.append(" ,? ");//term_filler10
                                    sb_instraninfo.append(" ,? ");//trad_chk_flag
                                    sb_instraninfo.append(" ,? ");//acc_chk_flag
                                    sb_instraninfo.append("  , DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                                    sb_instraninfo.append("  , DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                                    sb_instraninfo.append(" ,? ");//ins_user			
                                    sb_instraninfo.append(" ,? ");//mod_user			
                                    sb_instraninfo.append(" ,? ");//org_pg_seq			
                                    sb_instraninfo.append(" ,? ");//onffmerch_no		
                                    sb_instraninfo.append(" ,? ");//auth_pw		  
                                    sb_instraninfo.append(" ,? ");//auth_value		  
                                    sb_instraninfo.append(" ,? ");//app_card_num	
                                    sb_instraninfo.append(" ) ");          

                                    //System.out.println("sb_instraninfo.toString() : " + sb_instraninfo.toString());

                                    pstmt = conn.prepareStatement(sb_instraninfo.toString());

                                    //당일취소,익일취소 구분
                                    String strTpOrgAppDt = rs2.getString("app_dt");
                                    String strTpCnclAppDt = strCol13;

                                    //당일취소
                                    if(strTpCnclAppDt.equals(strTpOrgAppDt))
                                    {                            
                                        pstmt.setString(4,strCol13);//app_dt				, jdbcType=VARCHAR}
                                        pstmt.setString(5,strCol14);//app_tm				 , jdbcType=VARCHAR}

                                        pstmt.setString(39,"10");//tran_status 당일취소 셋팅                            
                                        pstmt.setString(76,"3");//acc_chk_flag 매입비대상 셋팅
                                    }
                                    //익일취소
                                    else
                                    {
                                        pstmt.setString(4,strCol13);//app_dt				, jdbcType=VARCHAR}
                                        pstmt.setString(5,strCol14);//app_tm				 , jdbcType=VARCHAR}

                                        pstmt.setString(39,"20");//tran_status 익일취소(매입취소) 셋팅
                                        pstmt.setString(76,"0");//acc_chk_flag 매입비대상 셋팅
                                    }
                                    pstmt.setString(1,strTpTranseq);//tran_seq				, jdbcType=VARCHAR} 
                                    pstmt.setString(2,"40");//massagetype			, jdbcType=VARCHAR} 
                                    pstmt.setString(3,rs2.getString("card_num"));//card_num			, jdbcType=VARCHAR} 

                                    pstmt.setString(6,null);//app_no				, jdbcType=VARCHAR}
                                    pstmt.setString(7,rs2.getString("tot_amt"));//tot_amt				 , jdbcType=INTEGER }
                                    pstmt.setString(8,strCol3);//pg_seq				, jdbcType=VARCHAR}
                                    pstmt.setString(9,rs2.getString("pay_chn_cate"));//pay_chn_cate			 , jdbcType=VARCHAR}
                                    pstmt.setString(10,strCol2);//tid					, jdbcType=VARCHAR}
                                    pstmt.setString(11,rs2.getString("onfftid"));//onfftid				, jdbcType=VARCHAR}
                                    pstmt.setString(12,rs2.getString("wcc"));//wcc					, jdbcType=VARCHAR}
                                    pstmt.setString(13,rs2.getString("expire_dt"));//expire_dt			, jdbcType=VARCHAR}
                                    pstmt.setString(14,rs2.getString("card_cate"));//card_cate			  , jdbcType=VARCHAR}
                                    pstmt.setString(15, null);//order_seq			  , jdbcType=VARCHAR}
                                    pstmt.setString(16,strCol13);//tran_dt				, jdbcType=VARCHAR}
                                    pstmt.setString(17,strCol14);//tran_tm				, jdbcType=VARCHAR}
                                    pstmt.setString(18,"40");//tran_cate			  , jdbcType=VARCHAR}
                                    pstmt.setString(19,rs2.getString("free_inst_flag"));//free_inst_flag		    , jdbcType=VARCHAR}
                                    pstmt.setDouble(20,rs2.getDouble("tax_amt"));//tax_amt				, jdbcType=INTEGER }
                                    pstmt.setDouble(21,rs2.getDouble("svc_amt"));//svc_amt				, jdbcType=INTEGER }
                                    pstmt.setString(22,rs2.getString("currency"));//currency			 , jdbcType=VARCHAR}
                                    pstmt.setString(23,rs2.getString("installment"));//installment			   , jdbcType=VARCHAR}
                                    /*
                                    pstmt.setString(24,rs2.getString("iss_cd"));//iss_cd				, jdbcType=VARCHAR}
                                    pstmt.setString(25,rs2.getString("iss_nm"));//iss_nm				 , jdbcType=VARCHAR}
                                    pstmt.setString(26,rs2.getString("app_iss_cd"));//app_iss_cd			 , jdbcType=VARCHAR}
                                    pstmt.setString(27,rs2.getString("app_iss_nm"));//app_iss_nm			 , jdbcType=VARCHAR}
                                    pstmt.setString(28,rs2.getString("acq_cd"));//acq_cd				, jdbcType=VARCHAR}
                                    pstmt.setString(29,rs2.getString("acq_nm"));//acq_nm				, jdbcType=VARCHAR}
                                    pstmt.setString(30,rs2.getString("app_acq_cd"));//app_acq_cd			  , jdbcType=VARCHAR}
                                    pstmt.setString(31,rs2.getString("app_acq_nm"));//app_acq_nm			   , jdbcType=VARCHAR}
                                    */
                                    pstmt.setString(24,null);//iss_cd				, jdbcType=VARCHAR}
                                    pstmt.setString(25,null);//iss_nm				 , jdbcType=VARCHAR}
                                    pstmt.setString(26,null);//app_iss_cd			 , jdbcType=VARCHAR}
                                    pstmt.setString(27,null);//app_iss_nm			 , jdbcType=VARCHAR}
                                    pstmt.setString(28,null);//acq_cd				, jdbcType=VARCHAR}
                                    pstmt.setString(29,null);//acq_nm				, jdbcType=VARCHAR}
                                    pstmt.setString(30,null);//app_acq_cd			  , jdbcType=VARCHAR}
                                    pstmt.setString(31,null);//app_acq_nm			   , jdbcType=VARCHAR}

                                    pstmt.setString(32,rs2.getString("merch_no"));//merch_no			  , jdbcType=VARCHAR}
                                    pstmt.setString(33,rs2.getString("org_merch_no"));//org_merch_no			  , jdbcType=VARCHAR}
                                    pstmt.setString(34,"0000");//result_cd			 , jdbcType=VARCHAR}
                                    pstmt.setString(35,"취소성공");//result_msg			   , jdbcType=VARCHAR}
                                    pstmt.setString(36,rs2.getString("app_dt"));//org_app_dd			  , jdbcType=VARCHAR}
                                    pstmt.setString(37,rs2.getString("app_no"));//org_app_no			   , jdbcType=VARCHAR}
                                    pstmt.setString(38,"noti취소통지");//cncl_reason			, jdbcType=VARCHAR}
                                    pstmt.setString(40,"00");//result_status		     , jdbcType=VARCHAR}
                                    pstmt.setString(41,"00");//tran_step			 , jdbcType=VARCHAR}
                                    pstmt.setString(42, strCol13);//cncl_dt				, jdbcType=VARCHAR}
                                    pstmt.setString(43,null);//acq_dt				, jdbcType=VARCHAR}
                                    pstmt.setString(44,null);//acq_result_cd		     , jdbcType=VARCHAR}
                                    pstmt.setString(45,null);//holdoff_dt			  , jdbcType=VARCHAR}
                                    pstmt.setString(46,null);//holdon_dt			  , jdbcType=VARCHAR}
                                    pstmt.setString(47,null);//pay_dt				, jdbcType=VARCHAR}
                                    pstmt.setString(48,null);//pay_amt				, jdbcType=INTEGER }
                                    pstmt.setString(49,null);//commision			  , jdbcType=INTEGER }
                                    pstmt.setString(50,rs2.getString("onofftid_pay_amt"));//onofftid_pay_amt		, jdbcType=INTEGER }
                                    pstmt.setString(51,rs2.getString("onofftid_commision"));//onofftid_commision		, jdbcType=INTEGER }
                                    pstmt.setString(52,null);//client_ip				, jdbcType=VARCHAR}
                                    pstmt.setString(53,rs2.getString("user_type"));//user_type			, jdbcType=VARCHAR}
                                    pstmt.setString(54,rs2.getString("memb_user_no"));//memb_user_no		, jdbcType=VARCHAR}
                                    pstmt.setString(55,rs2.getString("user_id"));//user_id				, jdbcType=VARCHAR}
                                    pstmt.setString(56,rs2.getString("user_nm"));//user_nm				, jdbcType=VARCHAR}
                                    pstmt.setString(57,rs2.getString("user_mail"));//user_mail			    , jdbcType=VARCHAR}
                                    pstmt.setString(58,rs2.getString("user_phone1"));//user_phone1			 , jdbcType=VARCHAR}
                                    pstmt.setString(59,rs2.getString("user_phone2"));//user_phone2			 , jdbcType=VARCHAR}
                                    pstmt.setString(60,rs2.getString("user_addr"));//user_addr			, jdbcType=VARCHAR}
                                    pstmt.setString(61,rs2.getString("product_type"));//product_type		  , jdbcType=VARCHAR}
                                    pstmt.setString(62,rs2.getString("product_nm"));//product_nm			 , jdbcType=VARCHAR}
                                    pstmt.setString(63,null);//filler				   , jdbcType=VARCHAR}
                                    pstmt.setString(64,null);//filler2				   , jdbcType=VARCHAR}
                                    pstmt.setString(65, null);//term_filler1			, jdbcType=VARCHAR}
                                    pstmt.setString(66, null);//term_filler2			, jdbcType=VARCHAR}
                                    pstmt.setString(67, null);//term_filler3			, jdbcType=VARCHAR}
                                    pstmt.setString(68, null);//term_filler4			, jdbcType=VARCHAR}
                                    pstmt.setString(69, null);//term_filler5			, jdbcType=VARCHAR}
                                    pstmt.setString(70, null);//term_filler6				, jdbcType=VARCHAR}
                                    pstmt.setString(71,null);//term_filler7			  , jdbcType=VARCHAR}
                                    pstmt.setString(72,null);//term_filler8			, jdbcType=VARCHAR}
                                    pstmt.setString(73,null);//term_filler9			, jdbcType=VARCHAR}
                                    pstmt.setString(74,null);//term_filler10			, jdbcType=VARCHAR}
                                    pstmt.setString(75,"0");//trad_chk_flag		  , jdbcType=VARCHAR}
                                    pstmt.setString(77,"0");//ins_user				     , jdbcType=VARCHAR}
                                    pstmt.setString(78,"0");//mod_user			, jdbcType=VARCHAR}
                                    pstmt.setString(79,rs2.getString("pg_seq"));//org_pg_seq			, jdbcType=VARCHAR}
                                    pstmt.setString(80,rs2.getString("onffmerch_no"));//onffmerch_no		  , jdbcType=VARCHAR}
                                    pstmt.setString(81,null);//auth_pw		  , jdbcType=VARCHAR}
                                    pstmt.setString(82,null);//auth_value		  , jdbcType=VARCHAR}   
                                    pstmt.setString(83,rs2.getString("app_card_num"));//app_card_num		  , jdbcType=VARCHAR}                
                                    int int_insResult = pstmt.executeUpdate();                               

                                    closeResultSet(rs);
                                    closePstmt(pstmt);
                                    
                                    //원거래를 update한다.
                                    StringBuffer sb_orgtran_mod_sql = new StringBuffer();
                                    sb_orgtran_mod_sql.append(" update " + rs2.getString("tb_nm") + " ");
                                    sb_orgtran_mod_sql.append(" set      cncl_reason = ? ");
                                    sb_orgtran_mod_sql.append("              ,tran_status= ? ");
                                    sb_orgtran_mod_sql.append("              ,cncl_dt = ? ");
                                    sb_orgtran_mod_sql.append("              ,acc_chk_flag = ? ");
                                    sb_orgtran_mod_sql.append("               ,mod_dt = DATE_FORMAT(now(),'%Y%m%d%H%i%S') ");
                                    sb_orgtran_mod_sql.append("               ,mod_user= '0' ");
                                    sb_orgtran_mod_sql.append("          where 1=1 ");
                                    sb_orgtran_mod_sql.append("            and tran_seq = ? ");

                                    pstmt = conn.prepareStatement(sb_orgtran_mod_sql.toString());

                                    pstmt.setString(1, "noti취소통지");

                                    //당일취소
                                    if(strTpCnclAppDt.equals(strTpOrgAppDt))
                                    {
                                       pstmt.setString(2, "10");//당일취소
                                       pstmt.setString(4, "3");//매입비대상   
                                    }
                                    else
                                    {
                                        pstmt.setString(2, "20");//매입취소
                                        pstmt.setString(4, rs2.getString("acc_chk_flag"));
                                    }                        
                                                       
                                    pstmt.setString(5, rs2.getString("tran_seq"));

                                    int int_mod_orginfo = pstmt.executeUpdate();  

                                    closeResultSet(rs);
                                    closePstmt(pstmt);

                                    strNotResult = "10";

                                } 
                                //원거래정보가 없는 경우
                                else
                                {
                                    //원거래가 없어 insert안함.
                                    strNotResult = "30";
                                }
                            }
                        }
                    }                     
                }
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            try
            {
                if (br != null)
                {
                    br.close();
                }
            }
            catch(Exception ex){}
            
            try
            {
                if (fis != null)
                {
                    fis.close();
                }
            }
            catch(Exception ex){}
        }
        
        return strReturnval;
    }
    
    
    
   /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
}
