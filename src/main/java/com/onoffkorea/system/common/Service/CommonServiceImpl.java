/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.onoffkorea.system.common.Service;

import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Dao.CommonDao;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.mid.Vo.Tb_Mid_Info;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hoyeongheo on 14. 12. 3..
 */

@Service("commonService")
public class CommonServiceImpl implements  CommonService{

    @Autowired
    private CommonDao commonDAO;

    protected Log logger = LogFactory.getLog(getClass());

    @Override
    public ArrayList<Tb_Mid_Info> bankAccountNoSelect() throws Exception {

        return commonDAO.bankAccountNoSelect();

    }
    
    //가맹점 조회
    @Override
    public ArrayList<com.onoffkorea.system.mid.Vo.Tb_Mid_Info> briefMerchantList() throws Exception {

        return commonDAO.briefMerchantList();
    }    


    //공통코드 검색 service
    @Override
    public Tb_Code_Main codeSearch(CommonCodeSearchFormBean commonCodeSearchFormBean) throws Exception {

        logger.debug("?################# codeSearch : "+commonCodeSearchFormBean.getMain_code());

        return commonDAO.codeSearch(commonCodeSearchFormBean);

    }

    @Override
    public List totcodeSearch(CommonCodeSearchFormBean commonCodeSearchFormBean) throws Exception {
        return commonDAO.totcodeSearch(commonCodeSearchFormBean);
    }


}
