/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.onoffkorea.system.common.Service;

import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.mid.Vo.Tb_Mid_Info;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hoyeongheo on 14. 12. 3..
 */
public interface CommonService {

    //계좌번호 조회
    public ArrayList<Tb_Mid_Info> bankAccountNoSelect()throws Exception;

    //가맹점 정보조회
    public ArrayList<com.onoffkorea.system.mid.Vo.Tb_Mid_Info> briefMerchantList() throws Exception;
    
    public Tb_Code_Main codeSearch(CommonCodeSearchFormBean commonCodeSearchFormBean) throws Exception;

    public List totcodeSearch(CommonCodeSearchFormBean commonCodeSearchFormBean) throws Exception;
    
    
    
}
