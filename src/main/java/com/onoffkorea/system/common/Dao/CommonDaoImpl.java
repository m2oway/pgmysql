/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.onoffkorea.system.common.Dao;

import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.vo.Tb_Code_Detail;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.mid.Vo.Tb_Mid_Info;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.stereotype.Repository;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hoyeongheo on 14. 12. 3..
 */
@Repository("commonDAO")
public class CommonDaoImpl extends SqlSessionDaoSupport implements CommonDao  {

    protected Log logger = LogFactory.getLog(getClass());

    public ArrayList<Tb_Mid_Info> bankAccountNoSelect()throws Exception{

        return (ArrayList)getSqlSession().selectList("Common.bankAccountNoSelect");

    }
    
    //가맹점(매입사)정보 간편조회
    @Override
    public ArrayList<Tb_Mid_Info> briefMerchantList() throws Exception {

        return (ArrayList)getSqlSession().selectList("Common.briefMerchantList");
    }
    

    @Override
    public Tb_Code_Main codeSearch(CommonCodeSearchFormBean commonCodeSearchFormBean) throws Exception {

        Tb_Code_Main tb_code_main  = getSqlSession().selectOne("Common.codeSearch", commonCodeSearchFormBean);
        
        return tb_code_main;
    }

    @Override
    public List totcodeSearch(CommonCodeSearchFormBean commonCodeSearchFormBean) throws Exception {
        return (List) getSqlSession().selectList("Common.totcodeSearch", commonCodeSearchFormBean);
        
    }


}
