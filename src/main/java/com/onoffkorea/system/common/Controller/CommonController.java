package com.onoffkorea.system.common.Controller;

import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by hoyeongheo on 2014-10-28.
 */
@Controller
@RequestMapping("/common/**")
public class CommonController {

    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private CommonService commonService;

    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
    }

    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }       

    @RequestMapping(method= {RequestMethod.GET,RequestMethod.POST},value = "/main")
    public String mainPage(Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSessionObj) throws Exception {

        model.addAttribute("context",request.getContextPath());
        model.addAttribute("siteSessionObj",siteSessionObj);

        return null;
    }

    @RequestMapping(method= {RequestMethod.GET,RequestMethod.POST},value = "/menu")
    public @ResponseBody String menu(Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("dentalHopitalSession") SiteSession dentalHopitalSession ) throws Exception {

        logger.debug(">>>>>>>>>>>>>>>>>>>>>>> menu");

        StringBuilder sb = new StringBuilder();

        return sb.toString();

    }

    @RequestMapping(method= {RequestMethod.GET},value = "/codeSearch")
    @ResponseBody
    public String codeSearch(@ModelAttribute CommonCodeSearchFormBean commonCodeSearchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("dentalHopitalSession") SiteSession dentalHopitalSession ) throws Exception {

        logger.debug("#################################################### codeSearch");

        logger.debug(">>>>> main_code :  "+commonCodeSearchFormBean.getMain_code());

        commonService.codeSearch(commonCodeSearchFormBean);

        StringBuilder sb = new StringBuilder();

        return sb.toString();

    }

}
