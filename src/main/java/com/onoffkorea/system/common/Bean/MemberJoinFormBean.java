package com.onoffkorea.system.common.Bean;

import com.onoffkorea.system.common.util.CommonSortableListPagingForm;

public class MemberJoinFormBean extends CommonSortableListPagingForm {
	
    private String rb_user_dvsn;
    private String ip_user_id;
    private String hd_user_id;
    private String hd_user_id_cnt;
    private String ip_user_name;
    private String ip_nick_name;
    private String hd_user_nickName;
    private String hd_user_nickName_cnt;
    private String rb_sex;
    private String rb_calendar_dvsn;
    private String ip_password;
    private String ip_password_confirm;
    private String sl_tel_number1;
    private String ip_tel_number2;
    private String ip_tel_number3;
    private String sl_mobile_number1;
    private String ip_mobile_number2;
    private String ip_mobile_number3;
    private String hd_mobile_cert;
    private String ip_email1;
    private String sl_email2;
    private String ck_direct_email;
    private String ip_direct_email;
    private String zipcode_id;
    private String ip_post_number1;
    private String ip_post_number2;
    private String ip_address;
    private String ip_detail_address;
    private String ip_birthday;
    private String ip_company_name;
    private String ip_Business_number;
    private String certification;
    private String user_login_id;
    private String memb_dvsn;
    private String random_result;
    private String last_chng_pgm_id;
    private String src_firm_id;
	
	//(프리)회원정보 조회
    private String user_id;
    private String login_id;
    private String user_pw;
    private String ip_existing_pswd;
    private String user_rght_id;
    private String menu_dvsn;
    private String admin_yn;
    private String user_nick_name;
    private String dev_id;
    private String dev_name;
    private String dev_brth_date;
    private String dev_brth_dvsn;
    private String sex_dvsn;
    private String dev_tel_no;
    private String dev_tel_no1;
    private String dev_tel_no2;
    private String dev_tel_no3;
    private String dev_mobl_no;
    private String dev_mobl_no1;
    private String dev_mobl_no2;
    private String dev_mobl_no3;
    private String dev_email1;
    private String dev_email2;
    private String dev_email1_id;
    private String dev_email1_domain;
    private String dev_zipcode_id;
    private String zipcode;
    private String zipcode1;
    private String zipcode2;
    private String zip_addr;
    private String dev_addr_detl;
	
	public String getRb_user_dvsn() {
		return rb_user_dvsn;
	}
	public void setRb_user_dvsn(String rb_user_dvsn) {
		this.rb_user_dvsn = rb_user_dvsn;
	}
	public String getIp_user_id() {
		return ip_user_id;
	}
	public void setIp_user_id(String ip_user_id) {
		this.ip_user_id = ip_user_id;
	}
	public String getHd_user_id() {
		return hd_user_id;
	}
	public void setHd_user_id(String hd_user_id) {
		this.hd_user_id = hd_user_id;
	}
	public String getHd_user_id_cnt() {
		return hd_user_id_cnt;
	}
	public void setHd_user_id_cnt(String hd_user_id_cnt) {
		this.hd_user_id_cnt = hd_user_id_cnt;
	}
	public String getIp_user_name() {
		return ip_user_name;
	}
	public void setIp_user_name(String ip_user_name) {
		this.ip_user_name = ip_user_name;
	}
	public String getIp_nick_name() {
		return ip_nick_name;
	}
	public void setIp_nick_name(String ip_nick_name) {
		this.ip_nick_name = ip_nick_name;
	}
	public String getHd_user_nickName() {
		return hd_user_nickName;
	}
	public void setHd_user_nickName(String hd_user_nickName) {
		this.hd_user_nickName = hd_user_nickName;
	}
	public String getHd_user_nickName_cnt() {
		return hd_user_nickName_cnt;
	}
	public void setHd_user_nickName_cnt(String hd_user_nickName_cnt) {
		this.hd_user_nickName_cnt = hd_user_nickName_cnt;
	}
	public String getRb_sex() {
		return rb_sex;
	}
	public void setRb_sex(String rb_sex) {
		this.rb_sex = rb_sex;
		sex_dvsn = rb_sex;
	}
	public String getRb_calendar_dvsn() {
		return rb_calendar_dvsn;
	}
	public void setRb_calendar_dvsn(String rb_calendar_dvsn) {
		this.rb_calendar_dvsn = rb_calendar_dvsn;
		dev_brth_dvsn = rb_calendar_dvsn;
	}
	public String getIp_password() {
		return ip_password;
	}
	public void setIp_password(String ip_password) {
		this.ip_password = ip_password;
	}
	public String getIp_password_confirm() {
		return ip_password_confirm;
	}
	public void setIp_password_confirm(String ip_password_confirm) {
		this.ip_password_confirm = ip_password_confirm;
	}
	public String getSl_tel_number1() {
		return sl_tel_number1;
	}
	public void setSl_tel_number1(String sl_tel_number1) {
		this.sl_tel_number1 = sl_tel_number1;
	}
	public String getIp_tel_number2() {
		return ip_tel_number2;
	}
	public void setIp_tel_number2(String ip_tel_number2) {
		this.ip_tel_number2 = ip_tel_number2;
	}
	public String getIp_tel_number3() {
		return ip_tel_number3;
	}
	public void setIp_tel_number3(String ip_tel_number3) {
		this.ip_tel_number3 = ip_tel_number3;
	}
	public String getSl_mobile_number1() {
		return sl_mobile_number1;
	}
	public void setSl_mobile_number1(String sl_mobile_number1) {
		this.sl_mobile_number1 = sl_mobile_number1;
	}
	public String getIp_mobile_number2() {
		return ip_mobile_number2;
	}
	public void setIp_mobile_number2(String ip_mobile_number2) {
		this.ip_mobile_number2 = ip_mobile_number2;
	}
	public String getIp_mobile_number3() {
		return ip_mobile_number3;
	}
	public void setIp_mobile_number3(String ip_mobile_number3) {
		this.ip_mobile_number3 = ip_mobile_number3;
	}
	public String getHd_mobile_cert() {
		return hd_mobile_cert;
	}
	public void setHd_mobile_cert(String hd_mobile_cert) {
		this.hd_mobile_cert = hd_mobile_cert;
	}
	public String getIp_email1() {
		return ip_email1;
	}
	public void setIp_email1(String ip_email1) {
		this.ip_email1 = ip_email1;
	}
	public String getSl_email2() {
		return sl_email2;
	}
	public void setSl_email2(String sl_email2) {
		this.sl_email2 = sl_email2;
		dev_email1_domain = sl_email2;
	}
	public String getCk_direct_email() {
		return ck_direct_email;
	}
	public void setCk_direct_email(String ck_direct_email) {
		this.ck_direct_email = ck_direct_email;
	}
	public String getIp_direct_email() {
		return ip_direct_email;
	}
	public void setIp_direct_email(String ip_direct_email) {
		this.ip_direct_email = ip_direct_email;
		dev_email1_domain = ip_direct_email;
	}		
	public String getZipcode_id() {
		return zipcode_id;
	}
	public void setZipcode_id(String zipcode_id) {
		this.zipcode_id = zipcode_id;
	}
	public String getIp_post_number1() {
		return ip_post_number1;
	}
	public void setIp_post_number1(String ip_post_number1) {
		this.ip_post_number1 = ip_post_number1;
	}
	public String getIp_post_number2() {
		return ip_post_number2;
	}
	public void setIp_post_number2(String ip_post_number2) {
		this.ip_post_number2 = ip_post_number2;
	}
	public String getIp_address() {
		return ip_address;
	}
	public void setIp_address(String ip_address) {
		this.ip_address = ip_address;
	}
	public String getIp_detail_address() {
		return ip_detail_address;
	}
	public void setIp_detail_address(String ip_detail_address) {
		this.ip_detail_address = ip_detail_address;
	}
	public String getIp_birthday() {
		return ip_birthday;
	}
	public void setIp_birthday(String ip_birthday) {
		this.ip_birthday = ip_birthday;
	}
	public String getIp_company_name() {
		return ip_company_name;
	}
	public void setIp_company_name(String ip_company_name) {
		this.ip_company_name = ip_company_name;
	}
	public String getIp_Business_number() {
		return ip_Business_number;
	}
	public void setIp_Business_number(String ip_Business_number) {
		this.ip_Business_number = ip_Business_number;
	}
	public String getCertification() {
		return certification;
	}
	public void setCertification(String certification) {
		this.certification = certification;
	}
	public String getUser_login_id() {
		return user_login_id;
	}
	public void setUser_login_id(String user_login_id) {
		this.user_login_id = user_login_id;
	}
	public String getMemb_dvsn() {
		return memb_dvsn;
	}
	public String getRandom_result() {
		return random_result;
	}
	public void setRandom_result(String random_result) {
		this.random_result = random_result;
	}
	public void setMemb_dvsn(String memb_dvsn) {
		this.memb_dvsn = memb_dvsn;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public String getUser_pw() {
		return user_pw;
	}
	public void setUser_pw(String user_pw) {
		this.user_pw = user_pw;
	}
	public String getIp_existing_pswd() {
		return ip_existing_pswd;
	}
	public void setIp_existing_pswd(String ip_existing_pswd) {
		this.ip_existing_pswd = ip_existing_pswd;
	}
	public String getUser_rght_id() {
		return user_rght_id;
	}
	public void setUser_rght_id(String user_rght_id) {
		this.user_rght_id = user_rght_id;
	}
	public String getMenu_dvsn() {
		return menu_dvsn;
	}
	public void setMenu_dvsn(String menu_dvsn) {
		this.menu_dvsn = menu_dvsn;
	}
	public String getAdmin_yn() {
		return admin_yn;
	}
	public void setAdmin_yn(String admin_yn) {
		this.admin_yn = admin_yn;
	}
	public String getUser_nick_name() {
		return user_nick_name;
	}
	public void setUser_nick_name(String user_nick_name) {
		this.user_nick_name = user_nick_name;
	}
	public String getDev_id() {
		return dev_id;
	}
	public void setDev_id(String dev_id) {
		this.dev_id = dev_id;
	}
	public String getDev_name() {
		return dev_name;
	}
	public void setDev_name(String dev_name) {
		this.dev_name = dev_name;
	}
	public String getDev_brth_date() {
		return dev_brth_date;
	}
	public void setDev_brth_date(String dev_brth_date) {
		this.dev_brth_date = dev_brth_date;
	}
	public String getDev_brth_dvsn() {
		return dev_brth_dvsn;
	}
	public void setDev_brth_dvsn(String dev_brth_dvsn) {
		this.dev_brth_dvsn = dev_brth_dvsn;
	}
	public String getSex_dvsn() {
		return sex_dvsn;
	}
	public void setSex_dvsn(String sex_dvsn) {
		this.sex_dvsn = sex_dvsn;
	}
	public String getDev_tel_no() {
		return dev_tel_no;
	}
	public void setDev_tel_no(String dev_tel_no) {
		String[] split_str;
		this.dev_tel_no = dev_tel_no;
		split_str= dev_tel_no.split("-");
		dev_tel_no1 = split_str[0];
		dev_tel_no2 = split_str[1];
		dev_tel_no3 = split_str[2];
	}
	public String getDev_tel_no1() {
		return dev_tel_no1;
	}
	public String getDev_tel_no2() {
		return dev_tel_no2;
	}
	public String getDev_tel_no3() {
		return dev_tel_no3;
	}
	public String getDev_mobl_no() {
		return dev_mobl_no;
	}
	public void setDev_mobl_no(String dev_mobl_no) {
		String[] split_str;
		this.dev_mobl_no = dev_mobl_no;
		split_str = dev_mobl_no.split("-");
		dev_mobl_no1 = split_str[0];
		dev_mobl_no2 = split_str[1];
		dev_mobl_no3 = split_str[2];
	}
	public String getDev_mobl_no1() {
		return dev_mobl_no1;
	}
	public String getDev_mobl_no2() {
		return dev_mobl_no2;
	}
	public String getDev_mobl_no3() {
		return dev_mobl_no3;
	}
	public String getDev_email1() {
		return dev_email1;
	}
	public void setDev_email1(String dev_email1) {
		String[] split_str;
		this.dev_email1 = dev_email1;
		split_str = dev_email1.split("@");
		dev_email1_id = split_str[0];
		System.out.println("========================>>> "+dev_email1_id);
		dev_email1_domain = split_str[1];
		System.out.println("========================>>> "+dev_email1_domain);
	}
	public String getDev_email2() {
		return dev_email2;
	}
	public void setDev_email2(String dev_email2) {
		this.dev_email2 = dev_email2;
	}
	public String getDev_email1_id() {
		return dev_email1_id;
	}
	public void setDev_email1_domain(String dev_email1_domain) {
		this.dev_email1_domain = dev_email1_domain;
	}
	public String getDev_email1_domain() {
		return dev_email1_domain;
	}
	public String getDev_zipcode_id() {
		return dev_zipcode_id;
	}
	public void setDev_zipcode_id(String dev_zipcode_id) {
		this.dev_zipcode_id = dev_zipcode_id;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
		
		zipcode1 = zipcode.substring(0,3);
		zipcode2 = zipcode.substring(3);
	}
	public String getZipcode1() {
		return zipcode1;
	}
	public String getZipcode2() {
		return zipcode2;
	}
	public String getZip_addr() {
		return zip_addr;
	}
	public void setZip_addr(String zip_addr) {
		this.zip_addr = zip_addr;
	}
	public String getDev_addr_detl() {
		return dev_addr_detl;
	}
	public void setDev_addr_detl(String dev_addr_detl) {
		this.dev_addr_detl = dev_addr_detl;
	}
	@Override
	public String toString() {
		return "MemberJoinFormBean [rb_user_dvsn=" + rb_user_dvsn
				+ ", ip_user_id=" + ip_user_id + ", hd_user_id=" + hd_user_id
				+ ", hd_user_id_cnt=" + hd_user_id_cnt + ", ip_user_name="
				+ ip_user_name + ", ip_nick_name=" + ip_nick_name
				+ ", hd_user_nickName=" + hd_user_nickName
				+ ", hd_user_nickName_cnt=" + hd_user_nickName_cnt
				+ ", rb_sex=" + rb_sex + ", rb_calendar_dvsn="
				+ rb_calendar_dvsn + ", ip_password=" + ip_password
				+ ", ip_password_confirm=" + ip_password_confirm
				+ ", sl_tel_number1=" + sl_tel_number1 + ", ip_tel_number2="
				+ ip_tel_number2 + ", ip_tel_number3=" + ip_tel_number3
				+ ", sl_mobile_number1=" + sl_mobile_number1
				+ ", ip_mobile_number2=" + ip_mobile_number2
				+ ", ip_mobile_number3=" + ip_mobile_number3
				+ ", hd_mobile_cert=" + hd_mobile_cert + ", ip_email1="
				+ ip_email1 + ", sl_email2=" + sl_email2 + ", ck_direct_email="
				+ ck_direct_email + ", ip_direct_email=" + ip_direct_email
				+ ", zipcode_id=" + zipcode_id + ", ip_post_number1="
				+ ip_post_number1 + ", ip_post_number2=" + ip_post_number2
				+ ", ip_address=" + ip_address + ", ip_detail_address="
				+ ip_detail_address + ", ip_birthday=" + ip_birthday
				+ ", ip_company_name=" + ip_company_name
				+ ", ip_Business_number=" + ip_Business_number
				+ ", certification=" + certification + ", user_login_id="
				+ user_login_id + ", user_id=" + user_id + ", login_id="
				+ login_id + ", ip_existing_pswd=" + ip_existing_pswd
				+ ", user_rght_id=" + user_rght_id + ", menu_dvsn=" + menu_dvsn
				+ ", admin_yn=" + admin_yn + ", user_nick_name="
				+ user_nick_name + ", dev_id=" + dev_id + ", dev_name="
				+ dev_name + ", dev_brth_date=" + dev_brth_date
				+ ", dev_brth_dvsn=" + dev_brth_dvsn + ", sex_dvsn=" + sex_dvsn
				+ ", dev_tel_no=" + dev_tel_no + ", dev_tel_no1=" + dev_tel_no1
				+ ", dev_tel_no2=" + dev_tel_no2 + ", dev_tel_no3="
				+ dev_tel_no3 + ", dev_mobl_no=" + dev_mobl_no
				+ ", dev_mobl_no1=" + dev_mobl_no1 + ", dev_mobl_no2="
				+ dev_mobl_no2 + ", dev_mobl_no3=" + dev_mobl_no3
				+ ", dev_email1=" + dev_email1 + ", dev_email2=" + dev_email2
				+ ", dev_email1_id=" + dev_email1_id + ", dev_email1_domain="
				+ dev_email1_domain + ", dev_zipcode_id=" + dev_zipcode_id
				+ ", zipcode=" + zipcode + ", zipcode1=" + zipcode1
				+ ", zipcode2=" + zipcode2 + ", zip_addr=" + zip_addr
				+ ", dev_addr_detl=" + dev_addr_detl + "]";
	}
	public String getLast_chng_pgm_id() {
		return last_chng_pgm_id;
	}
	public void setLast_chng_pgm_id(String last_chng_pgm_id) {
		this.last_chng_pgm_id = last_chng_pgm_id;
	}
	public String getSrc_firm_id() {
		return src_firm_id;
	}
	public void setSrc_firm_id(String src_firm_id) {
		this.src_firm_id = src_firm_id;
	}
	
}
