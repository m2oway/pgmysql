package com.onoffkorea.system.common.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/common/exception/*")
public class BaseExceptionResolver implements HandlerExceptionResolver {

    protected static Logger logger = Logger.getLogger("BaseExceptionResolver");

    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse reponse, Object obj, Exception ex) {

        ModelAndView mv = new ModelAndView();

        mv.setViewName("common/handler/frameException");
        
        mv.addObject("url", request.getRequestURI());
        
        mv.addObject("exMsg", "시스템 에러입니다. length : "+ex.getStackTrace().length+", StackTrace: " + ex.getStackTrace()[0]+" message: " +ex.getMessage());

        reponse.setContentType("text/html; charset=utf-8");

        return mv;
    }
}
