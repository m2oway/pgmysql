package com.onoffkorea.system.common.exception;

import java.sql.SQLException;

public class BizSQLException extends SQLException{

	private static final long serialVersionUID = 1L;

	private int exCode;
	private String exMsg;
	private String prcId;
 
	public BizSQLException(Throwable e, String prcId) {
		SQLException ex = (SQLException)((Throwable) e).getCause();
		ex.printStackTrace();
		this.exCode = ex.getErrorCode();
		this.exMsg	= ex.getMessage();
		this.prcId	= prcId;
	}

	public String getMessage() {
		System.out.println(this.exMsg);
		String rtMsg = "<center><b>?�로?�스ID : " + prcId + "<br>????�???: RMS" + String.format("%05d", this.exCode) + "</b></center>";
		System.out.println(rtMsg);
		
		return rtMsg;
	}
}