package com.onoffkorea.system.common.exception;


public class BizException extends Exception{

	private static final long serialVersionUID = 1L;

	private String exCode;
	
	public BizException() {
		super();
	}
 
	public BizException(String exCode) {
		this.exCode = exCode;
	}

	public String getMessage() {
//		String rtMsg = "<center><b>?�러 : " + this.exCode + "</b></center>";
		String rtMsg = this.exCode;
		return rtMsg;
	}
}