package com.onoffkorea.system.common.util;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.springframework.web.multipart.MultipartFile;

public class FileUploadUtil {
	
	/**
	 * ��Ƽ ���Ͼ��ε� 
	 * @param uploadPath
	 * @param files
	 * @param constFileNamePattern
	 * @return
	 * @throws Exception
	 */
	//파일 전송 후 디비에 입력
	public static Map<String, String> attachFiles(String uploadPath, Map<String, MultipartFile> files )throws Exception {
		File saveFolder = new File(uploadPath);
		 
		if (!saveFolder.exists() || saveFolder.isFile()) {
			saveFolder.mkdirs();
		}

		Map<String, String> ret = new HashMap<String, String>();
		
		Iterator<Entry<String, MultipartFile>> itr = files.entrySet().iterator();
		
		MultipartFile file;
		String filePath;
		int cnt = -1;
		while (itr.hasNext()) {
			Entry<String, MultipartFile> entry = itr.next();
			
			file = entry.getValue();
			
			if ( !"".equals(file.getOriginalFilename()) ) {
				String saveFileName = file.getOriginalFilename();
				String uuid = UUID.randomUUID().toString() + saveFileName.substring(saveFileName.lastIndexOf("."));
				filePath = uploadPath + "/" + uuid;
				
				//파일 전송
				file.transferTo(new File(filePath));
				++cnt;
				ret.put(cnt+"", uuid);
			}else{
				++cnt;
			}
		}

		return ret;
	}
}
