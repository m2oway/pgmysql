/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.common.util;

import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonServiceImpl;
import java.util.List;

/**
 *
 * @author Administrator
 */
public class CodeMgr 
{
    private static CodeMgr codeMgrInstance;
    
    
    private java.util.HashMap hsCodeTable;
    
    
    
    public static CodeMgr getInstance()
    {
       if(CodeMgr.codeMgrInstance == null)
       {
           codeMgrInstance = new CodeMgr();
       }
       
       return codeMgrInstance;
    }
    
    
    private CodeMgr()
    {
        try
        {
            CommonServiceImpl code_dbtool = new CommonServiceImpl();
            
            CommonCodeSearchFormBean objparam = new CommonCodeSearchFormBean();
            
            List listobj= code_dbtool.totcodeSearch(objparam);
            
            if(listobj != null)
            {
                for(int i=0 ;i < listobj.size() ; i++)
                {
                    //hsCodeTable
                    com.onoffkorea.system.common.vo.Tb_Code_Detail codinfoRow = (com.onoffkorea.system.common.vo.Tb_Code_Detail)listobj.get(i);
                    
                    String strMainCode = codinfoRow.getMain_code();
                    String strDetailCode = codinfoRow.getDetail_code();
                    String strDetailCodeNm = codinfoRow.getCode_nm();
                    
                    if(hsCodeTable != null)
                    {
                        java.util.HashMap hsdetailCdTable= (java.util.HashMap)hsCodeTable.get(strMainCode);
                        
                        if(hsdetailCdTable != null)
                        {
                            String strCodNm = (String)hsdetailCdTable.get(strDetailCode);
                            
                            if(strCodNm == null)
                            {
                               hsdetailCdTable.put(strDetailCode, strDetailCodeNm);
                            }
                        }
                        else
                        {
                            java.util.HashMap newhsdetailCdTable = new java.util.HashMap();
                            
                            newhsdetailCdTable.put(strDetailCode, strDetailCodeNm);
                            
                            hsCodeTable.put(strMainCode, newhsdetailCdTable);
                        }
                    }
                    else
                    {
                        hsCodeTable = new java.util.HashMap();
                        
                        java.util.HashMap newhsdetailCdTable = new java.util.HashMap();
                            
                        newhsdetailCdTable.put(strDetailCode, strDetailCodeNm);
                        
                        hsCodeTable.put(strMainCode, newhsdetailCdTable);
                    }
                    
                    
                }
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
    
    public String getCodeNm(String strMainCode, String strDetailCode)
    {
        String strReturnval = null;
        java.util.HashMap hsdetailCdTable= (java.util.HashMap)hsCodeTable.get(strMainCode);
        
        if(hsdetailCdTable != null)
        {
           strReturnval = (String)hsdetailCdTable.get(strDetailCode);
        }
        
        return strReturnval;
        
    }
}
