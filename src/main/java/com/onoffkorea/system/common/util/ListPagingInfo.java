package com.onoffkorea.system.common.util;

import java.util.Arrays;

public class ListPagingInfo {
	
	private String total_cnt;
	private String first_page_no;
	private String prev_block_last_page_no;
	private String[] page_nos;
	private String next_block_first_page_no;
	private String last_page_no;
	private String page_no;
	private String is_first_loading;
	
	public ListPagingInfo(){}

	public ListPagingInfo(CommonListPagingForm commonListPagingForm, int record_cnt)
	{
		int page_size = Integer.parseInt(commonListPagingForm.getPage_size());
		int block_size = Integer.parseInt(commonListPagingForm.getBlock_size());
		int page_no = Integer.parseInt(commonListPagingForm.getPage_no());
		boolean is_first_loading = commonListPagingForm.getIs_first_loading().booleanValue();
		int row_cnt = record_cnt == 0 ? 1 : record_cnt;
		
		// 기본 �?구하�?		int page_cnt = row_cnt % page_size == 0 ? row_cnt / page_size : row_cnt / page_size + 1;
		
//		page_no = page_no <= page_cnt ?  page_no :1 ;
//		
//		int block_cnt = page_cnt % block_size == 0 ? page_cnt / block_size : page_cnt / block_size + 1;
//		int block_no = page_no % block_size == 0 ? page_no / block_size : page_no / block_size + 1;
//		int prev_block_no = block_no > 1 ? block_no - 1 : 1;
//		int next_block_no = block_no < block_cnt ? block_no + 1 : block_cnt;
//		
//		// �??�인?�의 �?구하�?		int first_page_no = 1;
//		int prev_block_last_page_no = prev_block_no < block_cnt ? block_size * prev_block_no : first_page_no;
//		int next_block_first_page_no = next_block_no > block_no ? block_size * block_no + 1 : page_cnt ;
//		int last_page_no = page_cnt;
//		
//		// Paging Number List 구하�?		int start_page_no = (block_no -1) * block_size + 1;
//		int end_page_no = next_block_no > block_no ? block_size * block_no : page_cnt ;
//		String page_nos = "";
//		for( int i=start_page_no; i <= end_page_no; i++ ) page_nos += String.valueOf(i) + ",";
//		page_nos = page_nos.substring(0,page_nos.length()-1);
		
		// �?멤버�?�� �??�정
//		this.total_cnt = String.valueOf(record_cnt);
//		this.first_page_no = String.valueOf(first_page_no);
//		this.prev_block_last_page_no = String.valueOf(prev_block_last_page_no);
//		this.page_nos = page_nos.split(",");
//		this.next_block_first_page_no = String.valueOf(next_block_first_page_no);
//		this.last_page_no = String.valueOf(last_page_no);
//		this.page_no =  String.valueOf(page_no);
//		this.is_first_loading = is_first_loading ? "yes" : "no";

	}
	
	public String getTotal_cnt() {
		return total_cnt;
	}
	public void setTotal_cnt(String total_cnt) {
		this.total_cnt = total_cnt;
	}
	public String getFirst_page_no() {
		return first_page_no;
	}
	public void setFirst_page_no(String first_page_no) {
		this.first_page_no = first_page_no;
	}
	public String getPrev_block_last_page_no() {
		return prev_block_last_page_no;
	}
	public void setPrev_block_last_page_no(String prev_block_last_page_no) {
		this.prev_block_last_page_no = prev_block_last_page_no;
	}
	public String[] getPage_nos() {
		return page_nos;
	}
	public void setPage_nos(String[] page_nos) {
		this.page_nos = page_nos;
	}
	public String getNext_block_first_page_no() {
		return next_block_first_page_no;
	}
	public void setNext_block_first_page_no(String next_block_first_page_no) {
		this.next_block_first_page_no = next_block_first_page_no;
	}
	public String getLast_page_no() {
		return last_page_no;
	}
	public void setLast_page_no(String last_page_no) {
		this.last_page_no = last_page_no;
	}
	public String getPage_no() {
		return page_no;
	}
	public void setPage_no(String page_no) {
		this.page_no = page_no;
	}
	public String getIs_first_loading() {
		return is_first_loading;
	}
	public void setIs_first_loading(String is_first_loading) {
		this.is_first_loading = is_first_loading;
	}

	@Override
	public String toString() {
		return "ListPagingInfo [total_cnt=" + total_cnt + ", first_page_no="
				+ first_page_no + ", prev_block_last_page_no="
				+ prev_block_last_page_no + ", page_nos="
				+ Arrays.toString(page_nos) + ", next_block_first_page_no="
				+ next_block_first_page_no + ", last_page_no=" + last_page_no
				+ ", page_no=" + page_no + ", is_first_loading="
				+ is_first_loading + "]";
	}
}
