package com.onoffkorea.system.common.util;

import com.onoffkorea.system.common.vo.SiteSession;

public class CommonListPagingForm extends SiteSession{

	private String page_size;
	private String block_size;
	private String page_no;
	private String page_cnt;

	private String total_cnt;
	private String start_no;
	private String end_no;

        private String nopageflag;

	//private final static String DEFAULT_PAGE_SIZE_10 = "10";
        private final static String DEFAULT_PAGE_SIZE_10 = "100";
	private final static String DEFAULT_BLOCK_SIZE_10 = "10";
	private final static String DEFAULT_PAGE_NO_1 = "1";

    public String getNopageflag() {
        return nopageflag;
    }

    public void setNopageflag(String nopageflag) {
        this.nopageflag = nopageflag;
    }
        
        
	
	public String getPage_size() {
		return page_size == null || page_size.equals("")
				? CommonListPagingForm.DEFAULT_PAGE_SIZE_10
				: page_size;
	}

	public void setPage_size(String pageSize) {
		page_size = pageSize;
	}


	public String getBlock_size() {
		return block_size == null || block_size.equals("")
		? CommonListPagingForm.DEFAULT_BLOCK_SIZE_10
		: block_size;
	}

	public void setBlock_size(String blockSize) {
		block_size = blockSize;
	}

	public String getPage_no() {
		if(page_cnt == null){
			return page_no == null || page_no.equals("")
					? CommonListPagingForm.DEFAULT_PAGE_NO_1
					: page_no;
		}else{
			return page_no == null || page_no.equals("") || Integer.parseInt(page_no) > Integer.parseInt(page_cnt)
					? CommonListPagingForm.DEFAULT_PAGE_NO_1
					: page_no;
		}
	}

	public void setPage_no(String pageNo) {
		page_no = pageNo;
	}


	private void generationPaging(){
		int page_size = Integer.parseInt(this.getPage_size());

		int page_no = Integer.parseInt(this.getPage_no());

		this.start_no = String.valueOf( (page_no - 1) * page_size + 1 );

		this.end_no  =String.valueOf(Integer.parseInt(this.start_no)+ Integer.parseInt(this.getPage_size()));

	}

	public String getStart_no() {
		int page_size = Integer.parseInt(this.getPage_size());
		int page_no = Integer.parseInt(this.getPage_no());
		return String.valueOf( (page_no - 1) * page_size + 1 );
	}

	public String getEnd_no(){
		int page_size = Integer.parseInt(this.getPage_size());
		int page_no = Integer.parseInt(this.getPage_no());
		int start_no = (page_no - 1) * page_size + 1;

		int end_no = start_no + Integer.parseInt(this.getPage_size()) - 1;

		return String.valueOf(end_no);
	}

	public Boolean getIs_first_loading() {
		return page_no == null || page_no.equals("") ? new Boolean(true) : new Boolean(false);
	}
	
	public String getPage_cnt() {
		return page_cnt;
	}

	public void setPage_cnt(String page_cnt) {
		this.page_cnt = page_cnt;
	}


	public String getTotal_cnt() {
		return total_cnt;
	}

	public void setTotal_cnt(String total_cnt) {
		this.total_cnt = total_cnt;
	}
}
