package com.onoffkorea.system.common.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.Map.Entry;
import java.util.Set;
import javax.servlet.ServletOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

public class Util {

    public static StringBuffer makeData(List tablObj) throws Exception {
        
        Set key = null;
        
        StringBuffer sb = new StringBuffer();
        sb.append("<TABLE border='1' bordercolor='black'>");
        for( int i=0 ; i < tablObj.size(); i++)
        {
            LinkedHashMap xx = (LinkedHashMap) tablObj.get(i);
            
            if(key==null)
            {
                key = xx.keySet();
                sb.append("<TR>");   
                 int jx= 0;
                  for (Iterator iterator = key.iterator(); iterator.hasNext();) {
                      String keyName = String.valueOf(iterator.next());
                      keyName = keyName.replaceAll("`", "");
                      
                      sb.append("<th>"+keyName+"</th>") ;
                      
                      jx++;
                  }
                  sb.append("</TR>");   
             }
            
             sb.append("<TR>"); 
            int tx=0;
            for (Iterator iterator = key.iterator(); iterator.hasNext();) 
            {
                 
                String strSeachKeyname = String.valueOf(iterator.next());
                
                int int_xx = strSeachKeyname.indexOf("`");
                
                      String valueName = String.valueOf(xx.get(strSeachKeyname));
                      if(int_xx > -1)
                      {
                          sb.append("<td style=\"mso-number-format:\\@;\">"+valueName+"</td>");
                      }
                      else
                      {
                          sb.append("<td >"+valueName+"</td>");
                      }
  
            }
            sb.append("</TR>");
        }
        sb.append("</TABLE>");
        
        return sb;
        
    }    
    
    public static StringBuffer makeData2(List tablObj) throws Exception {
        
        Set key = null;
        
        StringBuffer sb = new StringBuffer();

        for( int i=0 ; i < tablObj.size(); i++)
        {
            LinkedHashMap xx = (LinkedHashMap) tablObj.get(i);
            
            if(key==null)
            {
                key = xx.keySet();
                
                 int jx= 0;
                  for (Iterator iterator = key.iterator(); iterator.hasNext();) {
                      String keyName = String.valueOf(iterator.next());
                      keyName = keyName.replaceAll("'", "");
                      
                      keyName = "\""+keyName+"\"";
                      if(jx==0)
                      {
                          sb.append(keyName) ;
                      }
                      else
                      {
                          sb.append(","+keyName) ;
                      }
                      
                      jx++;
                  }
                  
                  sb.append("\n");
             }
            
            
            int tx=0;
            for (Iterator iterator = key.iterator(); iterator.hasNext();) 
            {
                String strSeachKeyname = String.valueOf(iterator.next());
                
                int int_xx = strSeachKeyname.indexOf("'");
                
                      String valueName = String.valueOf(xx.get(strSeachKeyname));
                      if(int_xx > -1)
                      {
                          valueName = "\"'"+valueName+"\"";
                      }
                      else
                      {
                          valueName = "\""+valueName+"\"";
                      }
                      
                      if(tx==0)
                      {
                          
                          sb.append(valueName) ;
                      }
                      else
                      {
                          sb.append(","+valueName) ;
                      }
                      tx++;
            }
            sb.append("\n");
        }
        
        return sb;
        
    }
	
	public static String nullToString(String str){
		String result = null;
        if (str == null)
        	result = "";
        else
        	result = str;
        return result;
	}
	
	public static String nullToSpaceString(String str){
		String result = null;
        if (str == null)
        	result = " ";
        else
        	result = str;
        return result;
	}
	/**
     * "" 값인경우 ?�트�?" " ??리턴?�다
     *
     */
	public static String nullToSpaceString2(String str){
		String result = "";
        if (str == null || str.equals(""))
        	result = " ";
        else
        	result = str;
        return result;
	}
	/**
     * null,"" 값인경우 ?�트�?"0" ??리턴?�다
     *
     */
	public static String nullToInt(String str){
		String result = null;
        if (str == null || str.equals(""))
        	result = "0";
        else
        	result = str;
        return result;
	}
	
	public static String convertStr(String str, String param1, String param2){
		if(str.equals(param1)){
			return param2;
		}else{
			return str;
		}
	}
	
	 /**
     * 8?�리 ???�도/????�?"/" 구분?��? ?�입?�여 리턴?�다.
     *
     */
	public static String getParseDateString(String date){
//		if (date != null  && !date.equals("") && date.length()==8){
//			date = date.substring(0,4)+"/"+date.substring(4,6)+"/"+date.substring(6,8);
//		}else if(date != null  && !date.equals("") && date.length()==10){
//			date = date.substring(0,4)+"/"+date.substring(4,6)+"/"+date.substring(6,8)+" " +date.substring(8,10)+"??";
//		}else if(date != null  && !date.equals("") && date.length()==12){
//			date = date.substring(0,4)+"/"+date.substring(4,6)+"/"+date.substring(6,8)+" " +date.substring(8,10)+"??"+date.substring(10,12)+"�?;
//		}else if(date != null  && !date.equals("") && date.length()==14){
//			date = date.substring(0,4)+"/"+date.substring(4,6)+"/"+date.substring(6,8)+" " +date.substring(8,10)+"??"+date.substring(10,12)+"�?"+date.substring(12,14)+"�?;
//		}

		return date;
	}
	
	/**
     * "/" 구분?��? ??��?�여 리턴?�다.
     *
     */
	public static String getOriginDateString(String date){
		
		String result = null;
        if (date == null){
        	result = "";
        }else{
        	if(!date.equals("")){
	        	result = date.replaceAll("/","");
        	}else{
        		result = date;
        	}
        }

		return result;
	}
	
	// \n??<br>�?바꿔�?��
	public static String convertDBToHTML(String content){
		return content.replaceAll(" ","&nbsp;").replaceAll("\n", "<br>");
	}
	
	// ?�자�?마다 컴마 ?�어주기
	public static String getNumFormat( String digit ){
		
		String result = "0";
		
		if(digit != null){
			try{
				
				double i = Double.parseDouble(digit);
				
				String double_num = NumberFormat.getInstance().format(i);
				if(!double_num.equals("0")){ 
					result = double_num;
				}
			}catch(Exception e){
				result = "0";
			}
		}else{
			result = "0";
		}
		
		return result;
	}
	
	// 콤마?�거
	public static String getOriginNumFormat( String digit ){
		
		String result = null;
        if (digit == null){
        	result = "";
        }else{
        	if(!digit.equals("")){
	        	result = digit.replaceAll(",","");
        	}else{
        		result = digit;
        	}
        }
        
        return result;
	}
	
    //?�목 줄이�?
    public static String  getShortString(String subject, String url, int byteSize) {
		int rSize = 0;

		int len = 0;

		if ( subject.getBytes().length > byteSize ) {
			for ( ; rSize < subject.length(); rSize++ ) {
				if ( subject.charAt( rSize ) > 0x007F )
					len += 2;
				else
					len++;

				if ( len > byteSize )
					break;
			}
			
			if(url.equals("")){
				subject = "<a style='cursor:hand' title='"+subject+"'>" + subject.substring( 0, rSize ) + "...</a>";
			}else{
				subject = "<a href="+url+" style='cursor:hand' title='"+subject+"'>" + subject.substring( 0, rSize ) + "...</a>";
			}
		}else{
			if(url.equals("")){
				subject = "<a style='cursor:hand' title='"+subject+"'>" + subject + "</a>";
			}else{
				subject = "<a href="+url+" style='cursor:hand' title='"+subject+"'>" + subject + "</a>";
			}
		}
		
		return subject;
	}	
    
    /**
	  * YEAR SELECT BOX 만들�?
	  * @param name  (?�이�?
	  * @param strtYear  (?�작?�도 YYYY)
	  * @param endYear  (종료?�도 YYYY)
	  * @param selected  (�?��?�도)
	  * @return Select Box ?�도 Html String
	  */ 
   public static String makeYearSelectBoxByPerd(String name, String strtYear, String endYear) {
		  String html = "";
		  int i_strtYear = new Integer(strtYear).intValue();
		  int i_endYear = new Integer(endYear).intValue();
		  int i_gap = i_endYear - i_strtYear + 1;
		  
		  html = "<select name='" + name + "' class='form' style='width:60px' notnull=yes dispname='?�도'>";
		  html += "<option value=''>?�도</option>";
		  for(int i = i_strtYear ; i < i_strtYear+i_gap; i++) {
			  html += "<option value ='" + i + "' >" + i + "</option>";
		  }
		  html += "</select>";
		  
		  return html;
   } 
    
    /**
	  * YEAR SELECT BOX 만들�?
	  * @param name  (?�이�?
	  * @param selected  (�?��?�도)
	  * @return Select Box ?�도 Html String
	  */ 
    public static String makeYearSelectBox(String name, int selected) {
		  String html = "";
		  String realSelected = "";
		  Calendar today     = Calendar.getInstance();
		  String asThisYear = today.get(today.YEAR)+"";

		  html = "<select name='" + name + "' class='text_select' style='width:80px'>";
		  int i_this_year = Integer.parseInt(asThisYear);
		  int i_gap = 10;
		  html += "<option value=''>?�도</option>";
		  for(int loopYear= i_this_year-i_gap ; loopYear<= i_this_year + i_gap; loopYear++) {
			  if(selected==loopYear) realSelected = "selected";
			  html += "<option value ='" + loopYear + "' " + realSelected + "  >" + loopYear + "</option>";
			  realSelected = "";
		  }
		  html += "</select>";

		  return html;
    }    
    
    /**
	  * MONTH SELECT BOX 만들�?
	  * @param selected  (�?�� ??
	  * @return Select Box ??Html String
	  */
	 public static String makeMonthSelectBox(String name, int selected) {
	  String html = "";
	  String s_month = "";
	  String realSelected = "";
	  html = "<select name='" + name + "' class='form' style='width:40px' notnull=yes dispname='??>";
	  html += "<option value=''>??/option>";
	  for(int i= 1 ; i<= 12; i++) {
		  if(selected == i) realSelected = "selected";
			  if(i<10){
				  s_month = "0"+i;
			  }else{
				  s_month = ""+i;
			  }
		 /* Calendar today     = Calendar.getInstance();
		  String asThisYear = today.get(today.MONTH)+1+"";*/
		  html += "<option value ='" + s_month + "' " + realSelected + " >" + s_month + "</option>";
		  realSelected = "";
	  }
	  html += "</select>";
	  return html;
	 }
	 
		/*몇번째주 ?�작???�일??구하�?
		int date ??그주???�작??
		SELECT
	        CASE WHEN DECODE(X,1,TO_DATE(200902,'yyyymm'),NEXT_DAY( TO_DATE(200902,'yyyymm') + (7*X-14) ,1)) > LAST_DAY (TO_DATE (200902, 'yyyymm'))
	        --2009?????�의 마�?막날??X번째주의 첫날 보다 ?�을경우
	        THEN SUBSTR(TO_DATE(200902,'yyyymm'),7)
	        --01??반환
	        ELSE SUBSTR(DECODE(X,1,TO_DATE(200902,'yyyymm'),NEXT_DAY( TO_DATE(200902,'yyyymm') + (7*X-14) ,1)),7)
	        --2009????X번째�?첫째?�을 반환
	    END start_day  
	    FROM dual;  
	        //반환�?
	        date:?�작??
	        beforDay:?�전?�자??
	        afterDay:?�후?�자??
	        lastDay:마�?막날(?�요??
		*/
	public static Hashtable getWeekDay(String year, String month, int date) {

		Hashtable ht = new Hashtable();

		// Calendar?�래?�의 ?�스?�스 cal ?�성 
		Calendar cal = java.util.Calendar.getInstance();
		//?�력?�팅
		cal.set(Integer.parseInt(year), Integer.parseInt(month)-1, 1); 	
		// ?�번?�의 마�?�?�?취득(10?�의 경우 31, 11?�의 경우 30) 
		int endDate = cal.getActualMaximum(Calendar.DAY_OF_MONTH); 
		// ?�번?�이 ?�작???�일??취득(?�요?? , ?�요??2... ?�요??7)
		int startDay = cal.get(Calendar.DAY_OF_WEEK); 
		// ?�일???�자값을 ??��(?�스???�보??1?�음.. ?�요??0, ?�요??1... ?�요??6)
		int nCol = 0;
		//1?�일경우 ?�에 빈칸??
		int beforDay = 0;
		//마�?막날??경우 ?�에 빈칸??
		int afterDay = 0;
		//그주??마�?막날
		int lastDay = 0;

		// ?�전?�자(첫주?�때)
		if(date==1){
			for(int i = 1; i < startDay; i++ ) {
				beforDay++;
				nCol++; 
			} 
		}
		//주의 마�?막날
		for(int i = date; i <= endDate; i++) { 
			nCol++; 	
			if ( nCol == 7 ){ 
				lastDay = i;
				break;
			} 	
			if(i==endDate)lastDay = endDate;
		} 	
		// ?�후?�자(마�?�?주일??
		for ( ; nCol<7 && nCol!=0; nCol++ ){ 
			afterDay++; 
		} 
		ht.put("date",date+"");
		ht.put("beforDay", beforDay+"");
		ht.put("afterDay", afterDay+"");
		ht.put("lastDay", lastDay+"");
		return ht;
		
	}
	
	/*?�력?�이 몇째주인�?반환
	 * 반환�?몇째�?String
	 */
	public static int getWeek(String year,String month, int day) {
		// Calendar?�래?�의 ?�스?�스 cal ?�성 
		Calendar cal = java.util.Calendar.getInstance();	
		cal.set(Integer.parseInt(year), Integer.parseInt(month)-1, 1); 
		
		// ?�번?�의 마�?�?�?취득(10?�의 경우 31, 11?�의 경우 30) 
		int endDate = cal.getActualMaximum(Calendar.DAY_OF_MONTH); 
	
		// ?�번?�이 ?�작???�일??취득(?�요?? , ?�요??2... ?�요??7)
		int startDay = cal.get(Calendar.DAY_OF_WEEK); 
	
		// ?�일???�자값을 ??��(?�스???�보??1?�음.. ?�요??0, ?�요??1... ?�요??6)
		int nCol = 0;
		int wek=1;
		// ?�작???�전 �??�팅
		for(int i = 1; i < startDay; i++ ) {
			nCol++; 
		} 		
		// ?�제 �?계산
		for(int i = 1; i <= endDate; i++) { 
			nCol++; 
			if(i==day) break;
			if ( nCol == 7 ){ 
				wek++;
				nCol=0; 
			} 
		} 
		return wek;
	}
	
	/**
    * ?�력???�wo?�자?�서 ?�력?�짜까�????�수�?계산?�줌.
    *
    */
	public static String getAfterDay(String sday){
		int count = 0;
		String countVal = "";
		if(sday != null ){

			if(!sday.equals("") ){
				int sdate_year  = Integer.parseInt(sday.substring(0,4));
				System.out.println(sdate_year);
				int sdate_month = Integer.parseInt(sday.substring(4,6));
				System.out.println(sdate_month);
				int sdate_day   = Integer.parseInt(sday.substring(6,8));
				System.out.println(sdate_day);
				
				Date today = new Date ( );
				Calendar cal = Calendar.getInstance ( );
				cal.setTime ( today );// ?�늘�??�정. 
			
				Calendar cal2 = Calendar.getInstance ( );
				cal2.set ( sdate_year, sdate_month-1, sdate_day ); // 기�??�로 ?�정. month??경우 ?�당?�수-1???�줍?�다.
										
					while ( !cal.after ( cal2 ) )
					{
					count++;
					cal.add ( Calendar.DATE, 1 ); // ?�음?�로 바�?
				
					}
				if(count < 60){
					countVal = "<font color='red'>"+count+"??/font>";
				}else{
//					countVal = count+"??;
				}
			}
		}
		
		return countVal;
	}

	//문장???�에 ?�당?�는 문장??기�??�로 leng길이만큼 ?�라??
	public static String getShortContet(String str, String key, int leng){
		int idx = 0;
		String temp = "";
		if(key == null || key.length() < 1){
			if(str.length() > leng){
				temp = str.substring(0, leng)+"...";
			}else{
				temp = str;
			}
		}else{
			int dleng = (leng-(key.length()))/2;
			
			if(str.length() <= leng){
				temp =str;
			}else{
				idx=str.indexOf(key);
				if(idx <= dleng){
					temp = str.substring(0, leng)+"...";
				}else{
					temp = "..."+str.substring(idx-dleng, idx+key.length()+dleng)+"...";
				}
			}
		}
		return temp;
	}
	
	//문장???�에?�당?�는 문자�?<strong>?�성??�?
	public static String getHighLightStr(String str, String key){
		if(key == null || key.length()<1){
			return str;
		}else{
			String temp = "";
			int i = 0;
			while(i != -1){
				i = str.indexOf(key); //key???�당?�는 문장??index추출
				if(i != -1){
					temp += str.substring(0, i)+"<strong>"+key+"</strong>";
					str = str.substring(i+(key.length()), str.length());
				}
			}
			temp = temp+str; //?�머�??�는 �?��???�시 붙여�?
			return temp;
		}
	}
	
	/**
	 * request Prameter�?HashMap?�로 반환?�다.
	 * 
	 * @author �?��??ktinybird@nate.com)
	 * @param request
	 * @return HashMap
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static HashMap<String, String> ReqToMap(HttpServletRequest request) throws Exception {
		boolean isNull = true;
		Enumeration eNum = request.getParameterNames();
		
		System.out.println("?�▶??request ==> HashMap");
		HashMap<String, String> rskMap = new HashMap<String, String>();
		while(eNum.hasMoreElements()) {
			String key = (String)eNum.nextElement();
			String value = Util.nullToString(request.getParameter(key));
			
			if(!"".equals(value)){
				rskMap.put(key, value);
				System.out.println("?�▶" + key + " [" + value + "]");
				isNull = false;
			}
		}
		if(isNull){
			System.out.println("?�▷ Request parameter  is Null");
			rskMap.put("result", "");
		}
		
		return rskMap;
	}
	
	/**
	 * request Prameter�?HashMap?�로 반환?�다.
	 * 
	 * @author �?��??ktinybird@nate.com)
	 * @param request, rskMap
	 * @return HashMap
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static void ReqToMap(HttpServletRequest request, HashMap<String, String> rskMap) throws Exception {
		Enumeration eNum = request.getParameterNames();
		
		System.out.println("?�▶??request ==> HashMap");
		while(eNum.hasMoreElements()) {
			String key = (String)eNum.nextElement();
			String value = Util.nullToString(request.getParameter(key));
			
			if(!"".equals(value)){
				rskMap.put(key, value);
				System.out.println("?�▶" + key + " [" + value + "]");
			}
		}
	}
	
	/**
	 * request Prameter�?HashMap?�로 반환?�다.
	 * 
	 * @author �?��??ktinybird@nate.com)
	 * @param request, rskMap
	 * @return HashMap
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static Properties ReqToProp(HttpServletRequest request) throws Exception {
		Enumeration eNum = request.getParameterNames();
		Properties prop = new Properties();
		System.out.println("?�▶??request ==> Properties");
		while(eNum.hasMoreElements()) {
			String key = (String)eNum.nextElement();
			String value = Util.nullToString(request.getParameter(key));
			prop.put(key, value);
			System.out.println("?�▶" + key + " [" + value + "]");
		}
		return prop;
	}
	
	/**
	 * request Prameter�?ModelAndView�?반환?�다.
	 * 
	 * @author �?��??ktinybird@nate.com)
	 * @param request
	 * @return ModelAndView
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static void ReqToMdAndView(HttpServletRequest request, ModelAndView mv) throws Exception {
		boolean isNull = true;
		Enumeration eNum = request.getParameterNames();
		System.out.println("?�▶??request ==> ModelAndView");
		String key;
		String value;
		String objValue[];
		while(eNum.hasMoreElements()) {
			key = (String)eNum.nextElement();
			value = Util.nullToString(request.getParameter(key));
			
			if(!"".equals(value)){
				mv.addObject(key, value);
				System.out.println("?�▶?�▶" + key + " [" + value + "]");
				isNull = false;
			}
			objValue = request.getParameterValues(key);
			if(objValue.length > 1){
				mv.addObject(key, objValue);
				System.out.println("?�▶?�▶" + key + " [" + objValue + "]");
				isNull = false;
			}
			
		}
	}

	/**
	 * HashMap객체�?Properties객체�?반환?�다.
	 * 
	 * @author �?��??ktinybird@nate.com)
	 * @param rskMap
	 * @return Properties
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static Properties hMapToProp(HashMap rskMap) throws Exception {
		
		Properties prop = new Properties();
		Iterator itr = rskMap.entrySet().iterator();
		System.out.println("?�▶??HashMap ==> Properties");
		while (itr.hasNext()) {
			String key = ((Entry)itr.next()).getKey().toString();
			prop.put(key, rskMap.get(key));
			System.out.println("?�▶" + key + " [" + rskMap.get(key) + "]");
		}
		return prop;
	}
	
	/**
	 * HashMap객체�?Properties객체�?반환?�다.
	 * 
	 * @author �?��??ktinybird@nate.com)
	 * @param rskMap, prop
	 * @return Properties
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static void hMapToProp(HashMap rskMap, Properties prop) throws Exception {
		
		Iterator itr = rskMap.entrySet().iterator();
		System.out.println("?�▶??HashMap ==> Properties");
		while (itr.hasNext()) {
			String key = ((Entry)itr.next()).getKey().toString();
			System.out.println("?�▶" + key + " [" + rskMap.get(key) + "]");
			prop.put(key, rskMap.get(key));
		}
	}
	
	/**
	 * yyyymmdd ?�식???�재?�짜�?반환?�다.
	 * @return String
	 */
	public static String getTodayDate(){
		Date today=new Date();
	    SimpleDateFormat formater = new SimpleDateFormat("yyyyMMdd");       
	    return formater.format(today);
	}
	
	/**
	 * yyyy-MM-dd kk:mm:ss ?�식???�재?�짜�?반환?�다.
	 * @return String
	 */
	public static String getTodayDate2(){
		Date today=new Date();
	    SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd kk:mm:ss");       
	    return formater.format(today);
	}	
        
	public static String getTodayDate3(){
		Date today=new Date();
	    SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_kk_mm_ss");       
	    return formater.format(today);
	}	  
        
        
	public static String getTodayDate4(){
		Date today=new Date();
	    SimpleDateFormat formater = new SimpleDateFormat("yyyyMMddkkmmss");       
	    return formater.format(today);
	}	        
	
	/**
	 * check date string validation with the default format "yyyyMMdd"
	 * @param s
	 * @return
	 * @throws java.text.ParseException
	 */
	public static java.util.Date check(String s) throws java.text.ParseException {
		return check(s, "yyyyMMdd");
	}
	
	/**
	 * check date string validation with an user defined format.
	 * @param s
	 * @param format
	 * @return
	 * @throws java.text.ParseException
	 */
	public static java.util.Date check(String s, String format) throws java.text.ParseException {
		if ( s == null )
			throw new java.text.ParseException("date string to check is null", 0);
		if ( format == null )
		    throw new java.text.ParseException("format string to check date is null", 0);
		
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat (format, java.util.Locale.KOREA);
		java.util.Date date = null;
		try {
			date = formatter.parse(s);
		}catch(java.text.ParseException e) {
			e.printStackTrace();
		}
		if ( ! formatter.format(date).equals(s) )
			throw new java.text.ParseException("Out of bound date:\"" + s + "\" with format \"" + format + "\"", 0);
		       
		return date;
	}
	
	/**
	 * ?�작?�자??종료?�자�?받아?????�짜?�이???�수�?구한??
	 * ex)int i = monthsBetween("19911022", "19920220", "yyyyMMdd");
	 * @param from ?�작?�자
	 * @param to 종료?�자
	 * @param format
	 * @return
	 * @throws java.text.ParseException
	 */
	public static int monthsBetween(String from, String to, String format) throws java.text.ParseException {
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat (format, java.util.Locale.KOREA);
		java.util.Date fromDate = check(from, format);
		java.util.Date toDate = check(to, format);

		//if two date are same, return 0.
		if (fromDate.compareTo(toDate) == 0) return 0;
		java.text.SimpleDateFormat yearFormat = new java.text.SimpleDateFormat("yyyy", java.util.Locale.KOREA);
		java.text.SimpleDateFormat monthFormat = new java.text.SimpleDateFormat("MM", java.util.Locale.KOREA);
		java.text.SimpleDateFormat dayFormat = new java.text.SimpleDateFormat("dd", java.util.Locale.KOREA);
		int fromYear = Integer.parseInt(yearFormat.format(fromDate));
		int toYear = Integer.parseInt(yearFormat.format(toDate));
		int fromMonth = Integer.parseInt(monthFormat.format(fromDate));
		int toMonth = Integer.parseInt(monthFormat.format(toDate));
		int fromDay = Integer.parseInt(dayFormat.format(fromDate));
		int toDay = Integer.parseInt(dayFormat.format(toDate));
		int result = 0;
		result += ((toYear - fromYear) * 12);
		result += (toMonth - fromMonth);
		
		//if (((toDay - fromDay) < 0) ) result += fromDate.compareTo(toDate);
		
		// ceil�?floor???�과
		if (((toDay - fromDay) > 0) ) result += toDate.compareTo(fromDate);

		return result;
	}
	
	/**
	 * yyyyMMdd??기본?�맷?�로 ?�력받�? ?�자??마�?�??�짜�?구한??
	 * @param src
	 * @return
	 * @throws java.text.ParseException
	 */
	public static String lastDayOfMonth(String src) throws java.text.ParseException {
        return lastDayOfMonth(src, "yyyyMMdd");
    }
	
	/**
	 * ?�짜?�맷�??�짜�??�력받아???�당?�의 마�?�??�자�?구한??
	 * @param src
	 * @param format
	 * @return
	 * @throws java.text.ParseException
	 */
    public static String lastDayOfMonth(String src, String format) throws java.text.ParseException {
    	java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat (format, java.util.Locale.KOREA);
    	java.util.Date date = check(src, format);
    	java.text.SimpleDateFormat yearFormat = new java.text.SimpleDateFormat("yyyy", java.util.Locale.KOREA);
    	java.text.SimpleDateFormat monthFormat = new java.text.SimpleDateFormat("MM", java.util.Locale.KOREA);
        int year = Integer.parseInt(yearFormat.format(date));
        int month = Integer.parseInt(monthFormat.format(date));
        int day = lastDay(year, month);
        java.text.DecimalFormat fourDf = new java.text.DecimalFormat("0000");
        java.text.DecimalFormat twoDf = new java.text.DecimalFormat("00");
        String tempDate = String.valueOf(fourDf.format(year))
                         + String.valueOf(twoDf.format(month))
                         + String.valueOf(twoDf.format(day));
        date = check(tempDate, format);
      
        return formatter.format(date);
    }
    
    private static int lastDay(int year, int month) throws java.text.ParseException {
        int day = 0;
        switch(month)
        {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12: day = 31;
                     break;
            case 2: if ((year % 4) == 0) {
                        if ((year % 100) == 0 && (year % 400) != 0) { day = 28; }
                        else { day = 29; }
                    } else { day = 28; }
                    break;
            default: day = 30;
        }
        return day;
    }
    
    /**
     * yyyyMMdd??기폰?�맷?�식???�짜???�할 ?�수�??�력받아??
     * yyyymmdd?�식???�짜�?반환?�다. 
     * @param s
     * @param month
     * @return
     * @throws Exception
     */
    public static String addMonths(String s, int month) throws Exception {
        return addMonths(s, month, "yyyyMMdd");
    }

    /**
     * 기폰?�맷?�식�??�짜???�할 ?�수�??�력받아??
     * ?�맷?�식???�짜�?반환?�다
     * @param s
     * @param addMonth
     * @param format
     * @return
     * @throws Exception
     */
    public static String addMonths(String s, int addMonth, String format) throws Exception {
    	java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat (format, java.util.Locale.KOREA);
    	java.util.Date date = check(s, format);
    	java.text.SimpleDateFormat yearFormat = new java.text.SimpleDateFormat("yyyy", java.util.Locale.KOREA);
    	java.text.SimpleDateFormat monthFormat = new java.text.SimpleDateFormat("MM", java.util.Locale.KOREA);
    	java.text.SimpleDateFormat dayFormat = new java.text.SimpleDateFormat("dd", java.util.Locale.KOREA);
        int year = Integer.parseInt(yearFormat.format(date));
        int month = Integer.parseInt(monthFormat.format(date));
        int day = Integer.parseInt(dayFormat.format(date));
        month += addMonth;
        if (addMonth > 0) {
            while (month > 12) {
                month -= 12;
                year += 1;
            }
        } else {
            while (month <= 0) {
                month += 12;
                year -= 1;
            }
        }
 
        java.text.DecimalFormat fourDf = new java.text.DecimalFormat("0000");
        java.text.DecimalFormat twoDf = new java.text.DecimalFormat("00");
        String tempDate = String.valueOf(fourDf.format(year))
                         + String.valueOf(twoDf.format(month))
                         + String.valueOf(twoDf.format(day));
        java.util.Date targetDate = null;
        try {
            targetDate = check(tempDate, "yyyyMMdd");
        } catch(java.text.ParseException pe) {
            day = lastDay(year, month);
            tempDate = String.valueOf(fourDf.format(year))
                         + String.valueOf(twoDf.format(month))
                         + String.valueOf(twoDf.format(day));
            targetDate = check(tempDate, "yyyyMMdd");
        }
        return formatter.format(targetDate);
    }
	
	public static String xmlParser(Object obj, String nodeName) throws ClassNotFoundException,
													IllegalAccessException,
													InvocationTargetException {
		StringBuffer stBp = new StringBuffer();
	
//		String className = "prm.plm.vo.TbLsscse";
		String className = obj.getClass().getName();
		String delStr = "public java.lang.String ";
		Class klass = Class.forName(className);
	
		//System.out.println( "Class name: " + klass.getName());
		
		Field[] fields = klass.getDeclaredFields();
		String fieldName = "";
		
		Method[] methods = klass.getMethods();
		String methodName = "";
		String retValue = "";
		
		stBp.append("<" + nodeName + ">");
		
		for(Method m : methods )
		{
			// getMethod�?추출
			if((m.toString()).matches(".*public java.lang.String.*")){
				if(!"public java.lang.String java.lang.Object.toString()".equals(m.toString())){
					methodName = (m.toString()).replaceAll(delStr + className + ".", "");
//					System.out.println( "Found a method: " + methodName );
		
					retValue = (String) m.invoke(obj, null);
					for(Field f : fields ){
						fieldName = (f.toString()).replaceAll("private java.lang.String " + className + ".", "");
						if(methodName.equalsIgnoreCase("get" + fieldName + "()")){
//							System.out.println( "fieldName : " + fieldName + " = " + retValue);
							stBp.append("<" + fieldName + "><![CDATA[" + nullToString(retValue) + "]]></" + fieldName + ">");
						}
					}
				}
			}
		}
		
		stBp.append("</" + nodeName + ">");
		
		return stBp.toString(); 
	}
	
	public static String xmlAppender(String xmlStr){
		StringBuffer stBp = new StringBuffer();
//		stBp.append("<?xml version='1.0' encoding='utf-8'?>");
		stBp.append("<rexdataset>"); 
		stBp.append(xmlStr); 
		stBp.append("</rexdataset>");
		return stBp.toString();
	}
	
	public static String intToHan(String amt){

		String tmpamt ="";
		amt = "000000000000" + amt;
		int j=0;
  
//		for(int i=amt.length();i>0;i--) {
//			j++;
//			if (!amt.substring(i-1,i).equals("0")){
//				if (j%4==2) tmpamt ="??+tmpamt;
//					if (j%4==3) tmpamt ="�?+tmpamt;
//						if (j>1 && (j%4)==0) tmpamt ="�?+tmpamt;
//			}
//   
//		   if (j==5 && Integer.parseInt(amt.substring(amt.length()-8,amt.length()-4))>0) tmpamt ="�?+tmpamt;
//		   if (j==9 && Integer.parseInt(amt.substring(amt.length()-12,amt.length()-8))>0) tmpamt ="??+tmpamt;
//		   if (amt.substring(i-1,i).equals("1")) tmpamt ="??+tmpamt;
//		   if (amt.substring(i-1,i).equals("2")) tmpamt ="??+tmpamt;
//		   if (amt.substring(i-1,i).equals("3")) tmpamt ="??+tmpamt;
//		   if (amt.substring(i-1,i).equals("4")) tmpamt ="??+tmpamt;
//		   if (amt.substring(i-1,i).equals("5")) tmpamt ="??+tmpamt;
//		   if (amt.substring(i-1,i).equals("6")) tmpamt ="??+tmpamt;
//		   if (amt.substring(i-1,i).equals("7")) tmpamt ="�?+tmpamt;
//		   if (amt.substring(i-1,i).equals("8")) tmpamt ="??+tmpamt;
//		   if (amt.substring(i-1,i).equals("9")) tmpamt ="�?+tmpamt;
//		} 
		
		return tmpamt;
	}
	
	public static String fileUuid(String originalFilename){
		String uuid = null;
		if ( !"".equals(originalFilename)) {
			uuid = UUID.randomUUID().toString() + originalFilename.substring(originalFilename.lastIndexOf("."));
		}
		return uuid;
	}
        
        public static String addComma(String data){
            
            String strTpdate = null;
            
            if(data == null || data.equals("")) 
            {
                strTpdate = "0";
            }
            else
            {
                strTpdate = data;
            }
                    
	    //int result = Integer.parseInt(data);
            //int result = Integer.parseInt(strTpdate);
	    //return new DecimalFormat("#,###").format(result);
            System.out.println("strTpdate : " + strTpdate);
            long result = Long.parseLong(strTpdate);
            System.out.println("result : "+ result);
            System.out.println("DecimalFormat(\"#,###\").format(result) : " + new DecimalFormat("#,###").format(result));
	    return new DecimalFormat("#,###").format(result);
	}        
        
        public static void exceldownload(String transMasterListExcel, String excelDownloadFilename, HttpServletResponse response){
            
            InputStream in = null;
            ServletOutputStream out = null;
            
            try{
                 /*
                response.setHeader("Content-Type", "applicaiton/octer-stream; charset=euc-kr" ); 
                response.setHeader("Content-Disposition", "attachment; filename="+excelDownloadFilename+".xls;");   
                response.setHeader("Content-Transfer-Encoding", "binary"); 
                */
                response.setContentType("application/vnd.ms-excel");
                response.setCharacterEncoding("utf-8");
                response.setHeader("Content-Disposition", "attachment; filename=\""+ excelDownloadFilename + "_" + Util.getTodayDate4()+ ".xls\"");
                response.setHeader("Pragma", "no-cache");
                response.setHeader("Cache-Control", "no-cache");
                
                //in = new ByteArrayInputStream(transMasterListExcel.getBytes(Charset.forName("euc-kr")));
                in = new ByteArrayInputStream(transMasterListExcel.getBytes(Charset.forName("utf-8")));
                out = response.getOutputStream();
                
                out.print("<meta http-equiv=\"Content-Type\" content=\"application/vnd.ms-excel; charset=utf-8\">");
    
                byte[] outputByte = new byte[1024];

                int ck = 0;
                while((ck = in.read(outputByte)) != -1){

                    out.write(outputByte,0,ck);
                    
                }

                out.flush();

            }catch(Exception e){
                e.printStackTrace();
            }finally{
                try{
                    
                    
                    out.close();
                    in.close();
                    
                    out = null;
                    in = null;
                    
                }catch(Exception e){
                    e.printStackTrace();
                }
                
            }

        }
        
        public static String maskCardNumber(String cardNumber) {

            int cardNumberLength = cardNumber.length();
            
            String mask = "";
            
            if(cardNumberLength == 14){
                mask = "####-####-xxxx-##";
            }else if(cardNumberLength == 15){
                mask = "####-####-xxxx-###";
            }else if(cardNumberLength == 16){
                mask = "####-####-xxxx-####";
            }else if(cardNumberLength == 17){
                mask = "####-####-xxxx-#####";
            }else if(cardNumberLength == 18){
                mask = "####-####-xxxx-######";
            }else if(cardNumberLength == 19){
                mask = "####-####-xxxx-#######";
            }else if(cardNumberLength == 20){
                mask = "####-####-xxxx-########";
            }else if(cardNumberLength == 21){
                mask = "####-####-xxxx-#########";
            }

            // format the number
            int index = 0;
            StringBuilder maskedNumber = new StringBuilder();
            for (int i = 0; i < mask.length(); i++) {
                char c = mask.charAt(i);
                if (c == '#') {
                    maskedNumber.append(cardNumber.charAt(index));
                    index++;
                } else if (c == 'x') {
                    maskedNumber.append(c);
                    index++;
                } else {
                    maskedNumber.append(c);
                }
            }

            // return the masked number
            return maskedNumber.toString();
        }        
	
    public static String getNullToZero(String param) {
        return (param == null) ? "0" : param.trim();
    }      
    
    public static String fromUtftoEuc(String param)
    {
        try
        {
            return new String(nullToSpaceString(param).getBytes("UTF-8"),"EUC-KR");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return param;
        }    
            
    }
    
    public static String fromEuctoUtf(String param)
    {
        try
        {
            return new String(nullToSpaceString(param).getBytes("EUC-KR"),"UTF-8");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return param;
        }    
            
    }
            
}
