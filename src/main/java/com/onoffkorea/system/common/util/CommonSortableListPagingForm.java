package com.onoffkorea.system.common.util;

public class CommonSortableListPagingForm extends CommonListPagingForm {
		
	private String cell;
	private String direction;

	public String getCell() {
		return cell;
	}

	public void setCell(String cell) {
		this.cell = cell;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}
}
