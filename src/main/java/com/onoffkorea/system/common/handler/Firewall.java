package com.onoffkorea.system.common.handler;

import java.io.IOException;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

//import com.onoffkorea.common.Service.LoginService;

//@SessionAttributes("itplayers_session")
public class Firewall implements HandlerInterceptor {
	
	protected final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
//	private LoginService loginService;
	
//	public void setLoginService(LoginService loginService){
//		this.loginService = loginService;
//	}
	
	public Firewall() throws IOException {
	}
	
	public void afterCompletion(HttpServletRequest arg0,
			HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
	}

	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1,
			Object arg2, ModelAndView arg3) throws Exception {
		
	}


	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.HandlerInterceptor#preHandle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, java.lang.Object)
	 */
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object arg2) throws Exception {
            
		  HttpSession session = request.getSession(false);
		  boolean login_check = false;
		  boolean url_check = false;
		  String user_id;
		  String urlpath;
		  logger.debug(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>> uri : "+request.getRequestURI());
		  
		  //if(request.getSession().getAttribute("detalHosptial_session") != null){
                  if(request.getSession().getAttribute("siteSessionObj") != null){
			  if(session != null){
				  login_check = true;
			  }else{
				  response.sendRedirect("/");
			  }			  
		  }else{
			  if(request.getRequestURI().equals("/") 
                                  
				
			    ){
						  logger.debug(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>> true");
						  login_check = true;
				}else{
					response.sendRedirect("/");
				}
		  }
		  return true;
	}
}
