package com.onoffkorea.system.common.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.onoffkorea.system.common.vo.SiteSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class LoginCheckInterceptor extends HandlerInterceptorAdapter {
	
	protected final Log logger = LogFactory.getLog(getClass());

    @Override
	 public boolean preHandle(HttpServletRequest request,HttpServletResponse response, Object handler) throws Exception 
         {

	    HttpSession session = request.getSession(false);

            String context = request.getContextPath();

            logger.trace("---------------------------------------------------------------------------");
            logger.trace("login intecptoer request.getRequestURL() : " + request.getRequestURL());
            logger.trace("login intecptoer request.getRequestURI() : " + request.getRequestURI());
            logger.trace("context+\"/login/loginForm\": " + context+"/login/loginForm");
            logger.trace("---------------------------------------------------------------------------");
        
            if(session !=null)
            {

                if (session.getAttribute("siteSessionObj") != null) 
                {

                     if(request.getRequestURI().equals(context+"/login/loginForm"))
                     {
                        //if(request.getRequestURL().equals(context+"/login/loginForm")){
                        session.invalidate();
                        response.sendRedirect(context+"/login/loginForm");
                        return false;

                     }
                     else
                     {
                        SiteSession dentalHopitalSession = (SiteSession)session.getAttribute("siteSessionObj");

                        if(dentalHopitalSession != null)
                        {
                            logger.debug(">>>>>>>>>>>>>>> getUser_id : "+dentalHopitalSession.getUser_id());
                            logger.debug(">>>>>>>>>>>>>>> getUser_cate : "+dentalHopitalSession.getUser_cate());
                            logger.debug(">>>>>>>>>>>>>>> getOnfftid : "+dentalHopitalSession.getOnfftid());
                            logger.debug(">>>>>>>>>>>>>>> getOnffmerch_no : "+dentalHopitalSession.getOnffmerch_no());
                            logger.debug(">>>>>>>>>>>>>>> getAgent_seq : "+dentalHopitalSession.getAgent_seq());
                            return true;
                        }
                        else
                        {
                         session.invalidate();
                         response.sendRedirect(context+"/login/loginForm");
                         return false;
                        }
                     }

                }
                else
                {
                   if(request.getRequestURI().equals(context+"/login/loginForm")
                         || request.getRequestURI().equals(context+"/login/loginForm2")
                         || request.getRequestURI().equals(context+"/")
                         || request.getRequestURI().indexOf(context+"/bbs") != -1
                         || request.getRequestURI().indexOf(context+"/resource") != -1
                         )
                   {
                     return true;

                    }
                   else
                   {
                        response.sendRedirect(context+"/login/loginForm");
                        return false;
                   }
                }

            }
            else
            {
                 if(request.getRequestURI().equals(context+"/login/loginForm")
                    || request.getRequestURI().equals(context+"/login/loginForm2")
                    || request.getRequestURI().equals(context+"/")
                    || request.getRequestURI().indexOf(context+"/bbs") != -1
                    || request.getRequestURI().indexOf(context+"/resources") != -1
                  )
                 {
                    return true;                    
                 }
                 else
                 {

                    response.sendRedirect(context+"/login/loginForm");

                    return false;
                 }
            }

	 }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }

}
