package com.onoffkorea.system.common.vo;

public class SiteSession {
    
    private String login_status;
    private String dev_id;
    private String user_name;
    private String user_id;
    private String login_id;
    private String menu_dvsn;
    private String user_rght_id;
    private String admin_yn;
    private String user_pw;
    private String src_firm_id;
    private String join_able_date;
    private String user_seq;
    private String company_seq;
    private String onfftid;
    private String org_seq;
    private String auth_seq;
    private String agent_seq;
    private String onffmerch_no;
    private String user_cate;
    
    private String ses_user_cate;
    private String ses_onffmerch_no;
    private String ses_onfftid;
    private String ses_agent_seq;
    
    private String ses_merch_cncl_auth;
    private String ses_tid_cncl_auth;

    public String getSes_merch_cncl_auth() {
        return ses_merch_cncl_auth;
    }

    public void setSes_merch_cncl_auth(String ses_merch_cncl_auth) {
        this.ses_merch_cncl_auth = ses_merch_cncl_auth;
    }

    public String getSes_tid_cncl_auth() {
        return ses_tid_cncl_auth;
    }

    public void setSes_tid_cncl_auth(String ses_tid_cncl_auth) {
        this.ses_tid_cncl_auth = ses_tid_cncl_auth;
    }

    public String getSes_user_cate() {
        return ses_user_cate;
    }

    public void setSes_user_cate(String ses_user_cate) {
        this.ses_user_cate = ses_user_cate;
    }

    public String getSes_onffmerch_no() {
        return ses_onffmerch_no;
    }

    public void setSes_onffmerch_no(String ses_onffmerch_no) {
        this.ses_onffmerch_no = ses_onffmerch_no;
    }

    public String getSes_onfftid() {
        return ses_onfftid;
    }

    public void setSes_onfftid(String ses_onfftid) {
        this.ses_onfftid = ses_onfftid;
    }

    public String getSes_agent_seq() {
        return ses_agent_seq;
    }

    public void setSes_agent_seq(String ses_agent_seq) {
        this.ses_agent_seq = ses_agent_seq;
    }

    public String getUser_cate() {
        return user_cate;
    }

    public void setUser_cate(String user_cate) {
        this.user_cate = user_cate;
    }

    public String getOnffmerch_no() {
        return onffmerch_no;
    }

    public void setOnffmerch_no(String onffmerch_no) {
        this.onffmerch_no = onffmerch_no;
    }


    public String getOnfftid() {
        return onfftid;
    }

    public void setOnfftid(String onfftid) {
        this.onfftid = onfftid;
    }

    public String getOrg_seq() {
        return org_seq;
    }

    public void setOrg_seq(String org_seq) {
        this.org_seq = org_seq;
    }

    public String getAuth_seq() {
        return auth_seq;
    }

    public void setAuth_seq(String auth_seq) {
        this.auth_seq = auth_seq;
    }

    public String getAgent_seq() {
        return agent_seq;
    }

    public void setAgent_seq(String agent_seq) {
        this.agent_seq = agent_seq;
    }

    public String getCompany_seq() {
        return company_seq;
    }

    public void setCompany_seq(String company_seq) {
        this.company_seq = company_seq;
    }

    public String getUser_seq() {
        return user_seq;
    }

    public void setUser_seq(String user_seq) {
        this.user_seq = user_seq;
    }

    public String getDev_id() {
            return dev_id;
    }
    public void setDev_id(String dev_id) {
            this.dev_id = dev_id;
    }
    public String getUser_name() {
            return user_name;
    }
    public void setUser_name(String user_name) {
            this.user_name = user_name;
    }
    public String getUser_id() {
            return user_id;
    }
    public void setUser_id(String user_id) {
            this.user_id = user_id;
    }
    public String getLogin_id() {
            return login_id;
    }
    public void setLogin_id(String login_id) {
            this.login_id = login_id;
    }
    public String getLogin_status() {
            return login_status;
    }
    public void setLogin_status(String login_status) {
            this.login_status = login_status;
    }
    public String getMenu_dvsn() {
            return menu_dvsn;
    }
    public void setMenu_dvsn(String menu_dvsn) {
            this.menu_dvsn = menu_dvsn;
    }
    public String getUser_rght_id() {
            return user_rght_id;
    }
    public void setUser_rght_id(String user_rght_id) {
            this.user_rght_id = user_rght_id;
    }
    public String getAdmin_yn() {
            return admin_yn;
    }
    public void setAdmin_yn(String admin_yn) {
            this.admin_yn = admin_yn;
    }
    public String getUser_pw() {
            return user_pw;
    }
    public void setUser_pw(String user_pw) {
            this.user_pw = user_pw;
    }
    public String getSrc_firm_id() {
            return src_firm_id;
    }
    public void setSrc_firm_id(String src_firm_id) {
            this.src_firm_id = src_firm_id;
    }
    public String getJoin_able_date() {
            return join_able_date;
    }
    public void setJoin_able_date(String join_able_date) {
            this.join_able_date = join_able_date;
    }

    @Override
    public String toString() {
        return "SiteSession{" + "login_status=" + login_status + ", dev_id=" + dev_id + ", user_name=" + user_name + ", user_id=" + user_id + ", login_id=" + login_id + ", menu_dvsn=" + menu_dvsn + ", user_rght_id=" + user_rght_id + ", admin_yn=" + admin_yn + ", user_pw=" + user_pw + ", src_firm_id=" + src_firm_id + ", join_able_date=" + join_able_date + ", user_seq=" + user_seq + ", company_seq=" + company_seq + ", onfftid=" + onfftid + ", org_seq=" + org_seq + ", auth_seq=" + auth_seq + ", agent_seq=" + agent_seq + ", onffmerch_no=" + onffmerch_no + ", user_cate=" + user_cate + '}';
    }

}
