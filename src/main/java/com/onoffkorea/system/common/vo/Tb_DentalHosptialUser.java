package com.onoffkorea.system.common.vo;

public class Tb_DentalHosptialUser {
    private String user_id;
    private String login_id;
    private String user_pw;
    private String ip_existing_pswd;
    private String user_rght_id;
    private String menu_dvsn;
    private String admin_yn;
    private String user_nick_name;
    private String dev_id;
    private String dev_name;
    private String dev_brth_date;
    private String dev_brth_dvsn;
    private String sex_dvsn;
    private String dev_tel_no;
    private String dev_tel_no1;
    private String dev_tel_no2;
    private String dev_tel_no3;
    private String dev_mobl_no;
    private String dev_mobl_no1;
    private String dev_mobl_no2;
    private String dev_mobl_no3;
    private String dev_email1;
    private String dev_email2;
    private String dev_email1_id;
    private String dev_email1_domain;
    private String dev_zipcode_id;
    private String zipcode;
    private String zipcode1;
    private String zipcode2;
    private String zip_addr;
    private String dev_addr_detl;
    
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public String getUser_pw() {
		return user_pw;
	}
	public void setUser_pw(String user_pw) {
		this.user_pw = user_pw;
	}
	public String getIp_existing_pswd() {
		return ip_existing_pswd;
	}
	public void setIp_existing_pswd(String ip_existing_pswd) {
		this.ip_existing_pswd = ip_existing_pswd;
	}
	public String getUser_rght_id() {
		return user_rght_id;
	}
	public void setUser_rght_id(String user_rght_id) {
		this.user_rght_id = user_rght_id;
	}
	public String getMenu_dvsn() {
		return menu_dvsn;
	}
	public void setMenu_dvsn(String menu_dvsn) {
		this.menu_dvsn = menu_dvsn;
	}
	public String getAdmin_yn() {
		return admin_yn;
	}
	public void setAdmin_yn(String admin_yn) {
		this.admin_yn = admin_yn;
	}
	public String getUser_nick_name() {
		return user_nick_name;
	}
	public void setUser_nick_name(String user_nick_name) {
		this.user_nick_name = user_nick_name;
	}
	public String getDev_id() {
		return dev_id;
	}
	public void setDev_id(String dev_id) {
		this.dev_id = dev_id;
	}
	public String getDev_name() {
		return dev_name;
	}
	public void setDev_name(String dev_name) {
		this.dev_name = dev_name;
	}
	public String getDev_brth_date() {
		return dev_brth_date;
	}
	public void setDev_brth_date(String dev_brth_date) {
		this.dev_brth_date = dev_brth_date;
	}
	public String getDev_brth_dvsn() {
		return dev_brth_dvsn;
	}
	public void setDev_brth_dvsn(String dev_brth_dvsn) {
		this.dev_brth_dvsn = dev_brth_dvsn;
	}
	public String getSex_dvsn() {
		return sex_dvsn;
	}
	public void setSex_dvsn(String sex_dvsn) {
		this.sex_dvsn = sex_dvsn;
	}
	public String getDev_tel_no() {
		return dev_tel_no;
	}
	public void setDev_tel_no(String dev_tel_no) {
		this.dev_tel_no = dev_tel_no;
	}
	public String getDev_tel_no1() {
		return dev_tel_no1;
	}
	public void setDev_tel_no1(String dev_tel_no1) {
		this.dev_tel_no1 = dev_tel_no1;
	}
	public String getDev_tel_no2() {
		return dev_tel_no2;
	}
	public void setDev_tel_no2(String dev_tel_no2) {
		this.dev_tel_no2 = dev_tel_no2;
	}
	public String getDev_tel_no3() {
		return dev_tel_no3;
	}
	public void setDev_tel_no3(String dev_tel_no3) {
		this.dev_tel_no3 = dev_tel_no3;
	}
	public String getDev_mobl_no() {
		return dev_mobl_no;
	}
	public void setDev_mobl_no(String dev_mobl_no) {
		this.dev_mobl_no = dev_mobl_no;
	}
	public String getDev_mobl_no1() {
		return dev_mobl_no1;
	}
	public void setDev_mobl_no1(String dev_mobl_no1) {
		this.dev_mobl_no1 = dev_mobl_no1;
	}
	public String getDev_mobl_no2() {
		return dev_mobl_no2;
	}
	public void setDev_mobl_no2(String dev_mobl_no2) {
		this.dev_mobl_no2 = dev_mobl_no2;
	}
	public String getDev_mobl_no3() {
		return dev_mobl_no3;
	}
	public void setDev_mobl_no3(String dev_mobl_no3) {
		this.dev_mobl_no3 = dev_mobl_no3;
	}
	public String getDev_email1() {
		return dev_email1;
	}
	public void setDev_email1(String dev_email1) {
		this.dev_email1 = dev_email1;
	}
	public String getDev_email2() {
		return dev_email2;
	}
	public void setDev_email2(String dev_email2) {
		this.dev_email2 = dev_email2;
	}
	public String getDev_email1_id() {
		return dev_email1_id;
	}
	public void setDev_email1_id(String dev_email1_id) {
		this.dev_email1_id = dev_email1_id;
	}
	public String getDev_email1_domain() {
		return dev_email1_domain;
	}
	public void setDev_email1_domain(String dev_email1_domain) {
		this.dev_email1_domain = dev_email1_domain;
	}
	public String getDev_zipcode_id() {
		return dev_zipcode_id;
	}
	public void setDev_zipcode_id(String dev_zipcode_id) {
		this.dev_zipcode_id = dev_zipcode_id;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getZipcode1() {
		return zipcode1;
	}
	public void setZipcode1(String zipcode1) {
		this.zipcode1 = zipcode1;
	}
	public String getZipcode2() {
		return zipcode2;
	}
	public void setZipcode2(String zipcode2) {
		this.zipcode2 = zipcode2;
	}
	public String getZip_addr() {
		return zip_addr;
	}
	public void setZip_addr(String zip_addr) {
		this.zip_addr = zip_addr;
	}
	public String getDev_addr_detl() {
		return dev_addr_detl;
	}
	public void setDev_addr_detl(String dev_addr_detl) {
		this.dev_addr_detl = dev_addr_detl;
	}
	
	@Override
	public String toString() {
		return "ItPlayers_user [user_id=" + user_id + ", login_id=" + login_id
				+ ", user_pw=" + user_pw + ", ip_existing_pswd="
				+ ip_existing_pswd + ", user_rght_id=" + user_rght_id
				+ ", menu_dvsn=" + menu_dvsn + ", admin_yn=" + admin_yn
				+ ", user_nick_name=" + user_nick_name + ", dev_id=" + dev_id
				+ ", dev_name=" + dev_name + ", dev_brth_date=" + dev_brth_date
				+ ", dev_brth_dvsn=" + dev_brth_dvsn + ", sex_dvsn=" + sex_dvsn
				+ ", dev_tel_no=" + dev_tel_no + ", dev_tel_no1=" + dev_tel_no1
				+ ", dev_tel_no2=" + dev_tel_no2 + ", dev_tel_no3="
				+ dev_tel_no3 + ", dev_mobl_no=" + dev_mobl_no
				+ ", dev_mobl_no1=" + dev_mobl_no1 + ", dev_mobl_no2="
				+ dev_mobl_no2 + ", dev_mobl_no3=" + dev_mobl_no3
				+ ", dev_email1=" + dev_email1 + ", dev_email2=" + dev_email2
				+ ", dev_email1_id=" + dev_email1_id + ", dev_email1_domain="
				+ dev_email1_domain + ", dev_zipcode_id=" + dev_zipcode_id
				+ ", zipcode=" + zipcode + ", zipcode1=" + zipcode1
				+ ", zipcode2=" + zipcode2 + ", zip_addr=" + zip_addr
				+ ", dev_addr_detl=" + dev_addr_detl + "]";
	}

}
