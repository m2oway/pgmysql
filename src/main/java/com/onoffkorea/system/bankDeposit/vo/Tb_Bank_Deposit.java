/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.onoffkorea.system.bankDeposit.vo;

/**
 * Created by hoyeongheo on 14. 12. 16..
 */
public class Tb_Bank_Deposit {

    private String bank_dep_seq;	
    private String merch_no;	
    private String pay_mtd;
    private String pay_mtd_nm;
    private String deposit_dt;	
    private String bank_cd;		
    private String bank_cd_nm;	
    private String account_no;	
    private String deposit_amt;	
    private String collect_method;	
    private String collect_method_nm;	
    private String breakdown;	
    private String memo	;	
    private String ins_dt;		
    private String mod_dt;		
    private String ins_user;	
    private String mod_user;	

    public String getPay_mtd() {
        return pay_mtd;
    }

    public void setPay_mtd(String pay_mtd) {
        this.pay_mtd = pay_mtd;
    }

    public String getPay_mtd_nm() {
        return pay_mtd_nm;
    }

    public void setPay_mtd_nm(String pay_mtd_nm) {
        this.pay_mtd_nm = pay_mtd_nm;
    }

    public String getBank_cd_nm() {
        return bank_cd_nm;
    }

    public void setBank_cd_nm(String bank_cd_nm) {
        this.bank_cd_nm = bank_cd_nm;
    }

    public String getCollect_method_nm() {
        return collect_method_nm;
    }

    public void setCollect_method_nm(String collect_method_nm) {
        this.collect_method_nm = collect_method_nm;
    }
    

    public String getAccount_no() {
        return account_no;
    }

    public void setAccount_no(String account_no) {
        this.account_no = account_no;
    }

    public String getBank_cd() {
        return bank_cd;
    }

    public void setBank_cd(String bank_cd) {
        this.bank_cd = bank_cd;
    }

    public String getBank_dep_seq() {
        return bank_dep_seq;
    }

    public void setBank_dep_seq(String bank_dep_seq) {
        this.bank_dep_seq = bank_dep_seq;
    }

    public String getBreakdown() {
        return breakdown;
    }

    public void setBreakdown(String breakdown) {
        this.breakdown = breakdown;
    }

    public String getCollect_method() {
        return collect_method;
    }

    public void setCollect_method(String collect_method) {
        this.collect_method = collect_method;
    }

    public String getDeposit_amt() {
        return deposit_amt;
    }

    public void setDeposit_amt(String deposit_amt) {
        this.deposit_amt = deposit_amt;
    }

    public String getDeposit_dt() {
        return deposit_dt;
    }

    public void setDeposit_dt(String deposit_dt) {
        this.deposit_dt = deposit_dt;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getMerch_no() {
        return merch_no;
    }

    public void setMerch_no(String merch_no) {
        this.merch_no = merch_no;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }
}
