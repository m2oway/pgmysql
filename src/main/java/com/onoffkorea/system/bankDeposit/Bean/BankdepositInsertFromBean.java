/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.onoffkorea.system.bankDeposit.Bean;

/**
 * Created by hoyeongheo on 14. 12. 22..
 */
public class BankdepositInsertFromBean { 

    private String pay_mtd; //가맹점 구분
    private String merch_no;        //가맹점 번호
    private String collect_method; //입력 방법
    private String deposit_dt;      //입금일자
    private String deposit_amt;     //입금 금액
    private String memo;            //메모
    
    private String breakdown;      //적용

    private String account_no;      //계좌번호
    private String bank_cd;         //은행코드
    private String bank_dep_seq;    //통장입금내역 순번

    private String ins_user;        //등록자 아이디
    private String mod_user;        //수정자 아이디
    private String comp_seq;        //회사 등록 seq

    public String getPay_mtd() {
        return pay_mtd;
    }

    public void setPay_mtd(String pay_mtd) {
        this.pay_mtd = pay_mtd;
    }

    public String getMerch_no() {
        return merch_no;
    }

    public void setMerch_no(String merch_no) {
        this.merch_no = merch_no;
    }

    public String getCollect_method() {
        return collect_method;
    }

    public void setCollect_method(String collect_method) {
        this.collect_method = collect_method;
    }

    public String getDeposit_dt() {
        return deposit_dt;
    }

    public void setDeposit_dt(String deposit_dt) {
        this.deposit_dt = deposit_dt;
    }

    public String getDeposit_amt() {
        return deposit_amt;
    }

    public void setDeposit_amt(String deposit_amt) {
        this.deposit_amt = deposit_amt;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getBreakdown() {
        return breakdown;
    }

    public void setBreakdown(String breakdown) {
        this.breakdown = breakdown;
    }

    public String getAccount_no() {
        return account_no;
    }

    public void setAccount_no(String account_no) {
        this.account_no = account_no;
    }

    public String getBank_cd() {
        return bank_cd;
    }

    public void setBank_cd(String bank_cd) {
        this.bank_cd = bank_cd;
    }

    public String getBank_dep_seq() {
        return bank_dep_seq;
    }

    public void setBank_dep_seq(String bank_dep_seq) {
        this.bank_dep_seq = bank_dep_seq;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    public String getComp_seq() {
        return comp_seq;
    }

    public void setComp_seq(String comp_seq) {
        this.comp_seq = comp_seq;
    }

    @Override
    public String toString() {
        return "BankdepositInsertFromBean{" +
                "merch_no='" + merch_no + '\'' +
                ", collect_method='" + collect_method + '\'' +
                ", deposit_dt='" + deposit_dt + '\'' +
                ", deposit_amt='" + deposit_amt + '\'' +
                ", memo='" + memo + '\'' +
                ", breakdown='" + breakdown + '\'' +
                ", account_no='" + account_no + '\'' +
                ", bank_cd='" + bank_cd + '\'' +
                ", bank_dep_seq='" + bank_dep_seq + '\'' +
                ", ins_user='" + ins_user + '\'' +
                ", mod_user='" + mod_user + '\'' +
                ", comp_seq='" + comp_seq + '\'' +
                '}';
    }
}
