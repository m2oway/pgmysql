/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.onoffkorea.system.bankDeposit.Bean;

import com.onoffkorea.system.common.util.CommonListPagingForm;

import java.io.Serializable;

/**
 * Created by hoyeongheo on 14. 12. 2..
 */
public class BankdepositSearchFromBean extends CommonListPagingForm implements Serializable {

    private String from_date;     //조회 시작일자
    private String to_date;       //조회 종료일자
    private String account_no;        //계좌 번호
    private String merch_no;      //가맹점 번호
    private String bank_dep_seq;  //통장입금내역 일련번호

    public String getAccount_no() {
        return account_no;
    }

    public void setAccount_no(String account_no) {
        this.account_no = account_no;
    }


    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    
    public String getMerch_no() {
        return merch_no;
    }

    public void setMerch_no(String merch_no) {
        this.merch_no = merch_no;
    }

   
    public String getBank_dep_seq() {
        return bank_dep_seq;
    }

    public void setBank_dep_seq(String bank_dep_seq) {
        this.bank_dep_seq = bank_dep_seq;
    }
}
