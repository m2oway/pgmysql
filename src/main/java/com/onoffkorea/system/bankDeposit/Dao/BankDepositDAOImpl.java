package com.onoffkorea.system.bankDeposit.Dao;

import com.onoffkorea.system.bankDeposit.Bean.BankdepositInsertFromBean;
import com.onoffkorea.system.bankDeposit.Bean.BankdepositSearchFromBean;
import com.onoffkorea.system.bankDeposit.vo.Bank_Deposit_Search;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hoyoungher on 2014. 12. 15..
 */
@Repository("bankDepositDAO")
public class BankDepositDAOImpl extends SqlSessionDaoSupport implements BankDepositDAO {

    @Override
    public String bankDepositListSearchCount(BankdepositSearchFromBean bankdepositSearchFromBean) throws Exception {

        return (String)getSqlSession().selectOne("BankDeposit.bankDepositListSearchCount",bankdepositSearchFromBean);
    }

    @Override
    public ArrayList<Bank_Deposit_Search> bankDepositListSearch(BankdepositSearchFromBean bankdepositSearchFromBean) throws Exception {

        ArrayList<Bank_Deposit_Search> list = (ArrayList)getSqlSession().selectList("BankDeposit.bankDepositListSearch",bankdepositSearchFromBean);

        return list;
    }

//    상세 검색
    @Override
    public Bank_Deposit_Search bankDepositDetailSearch(BankdepositSearchFromBean bankdepositSearchFromBean) throws Exception {

        return (Bank_Deposit_Search)getSqlSession().selectOne("BankDeposit.bankDepositDetailSearch",bankdepositSearchFromBean);

    }

    //통장 입금내역 등록
    @Override
    public Integer bankDepositInsert(BankdepositInsertFromBean bankdepositInsertFromBean) throws Exception {

        return (Integer)getSqlSession().insert("BankDeposit.bankDeposiInsert",bankdepositInsertFromBean);

    }

    //통장 입금내역 수정
    @Override
    public Integer bankDepositUpdate(BankdepositInsertFromBean bankdepositInsertFromBean) throws Exception {

        return (Integer)getSqlSession().update("BankDeposit.bankDepositUpdate",bankdepositInsertFromBean);

    }

    //통장 입금내역 삭제
    @Override
    public Integer bankDepositDelete(BankdepositInsertFromBean bankdepositInsertFromBean) throws Exception {

        if(bankdepositInsertFromBean.getBank_dep_seq() == null || bankdepositInsertFromBean.getBank_dep_seq().equals(""))
        {
            return 0;
        }
        else
        {
            return (Integer)getSqlSession().delete("BankDeposit.bankDepositDelete",bankdepositInsertFromBean);
        }
        

    }

    @Override
    public List bankDepositListExcel(BankdepositSearchFromBean bankdepositSearchFromBean) {
        
        return (List)getSqlSession().selectList("BankDeposit.bankDepositListExcel",bankdepositSearchFromBean);
        
    }


}
