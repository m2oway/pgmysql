package com.onoffkorea.system.bankDeposit.Dao;

import com.onoffkorea.system.bankDeposit.Bean.BankdepositInsertFromBean;
import com.onoffkorea.system.bankDeposit.Bean.BankdepositSearchFromBean;
import com.onoffkorea.system.bankDeposit.vo.Bank_Deposit_Search;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by hoyoungher on 2014. 12. 15..
 */
public interface BankDepositDAO {

    public String bankDepositListSearchCount(BankdepositSearchFromBean bankdepositSearchFromBean) throws Exception;

    public ArrayList<Bank_Deposit_Search> bankDepositListSearch(BankdepositSearchFromBean bankdepositSearchFromBean) throws Exception;

    public Bank_Deposit_Search bankDepositDetailSearch(BankdepositSearchFromBean bankdepositSearchFromBean) throws Exception;

    public Integer bankDepositInsert(BankdepositInsertFromBean bankdepositInsertFromBean) throws Exception;

    public Integer bankDepositUpdate(BankdepositInsertFromBean bankdepositInsertFromBean) throws Exception;

    public Integer bankDepositDelete(BankdepositInsertFromBean bankdepositInsertFromBean) throws Exception;

    public List bankDepositListExcel(BankdepositSearchFromBean bankdepositSearchFromBean);


}
