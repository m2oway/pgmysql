package com.onoffkorea.system.bankDeposit.Service;

import com.onoffkorea.system.bankDeposit.Bean.BankdepositInsertFromBean;
import com.onoffkorea.system.bankDeposit.Bean.BankdepositSearchFromBean;
import com.onoffkorea.system.bankDeposit.Dao.BankDepositDAO;
import com.onoffkorea.system.bankDeposit.vo.Bank_Deposit_Search;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.mid.Dao.MidDAO;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by hoyoungher on 2014. 12. 15..
 */
@Service("bankDepositService")
public class BankDepositServiceImpl  implements  BankDepositService{

    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private BankDepositDAO bankDepositDAO;

    @Autowired
    private MidDAO merchDAO;

    @Override
    public Hashtable bankDepositListSearch(BankdepositSearchFromBean bankdepositSearchFromBean) throws Exception{

        Hashtable ht = new Hashtable();

        String total_count = this.bankDepositDAO.bankDepositListSearchCount(bankdepositSearchFromBean);

        ArrayList<Bank_Deposit_Search> bankDepositListSearch = bankDepositDAO.bankDepositListSearch(bankdepositSearchFromBean);

        StringBuilder jsonSb = new StringBuilder();

        jsonSb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(bankdepositSearchFromBean.getStart_no())-1)+"\",\"rows\" : [");

        String cardNum=null;
        //NO,입금일,은행,통장번호,MID구분,가맹점번호,입금금액,내역,수집방법,Memo
        if(bankDepositListSearch.size() > 0){
            for (int i = 0; i < bankDepositListSearch.size(); i++) {
                 if(!"".equals(Util.nullToString(bankDepositListSearch.get(i).getAccount_no()))){
                        cardNum = bankDepositListSearch.get(i).getAccount_no().substring(0, 6)+"******"+bankDepositListSearch.get(i).getAccount_no().substring(12);
                    }else {
                        cardNum="";
}
                
                jsonSb.append("{\"id\":" + i);
                jsonSb.append(",\"userdata\":{");
                jsonSb.append("\"bank_dep_seq\":\""+ Util.nullToString(bankDepositListSearch.get(i).getBank_dep_seq())+"\"");
                jsonSb.append("}");
                jsonSb.append(",\"data\":[");
                jsonSb.append(" \"" + Util.nullToString(bankDepositListSearch.get(i).getRnum()) + "\"");
                jsonSb.append(" ,\"" + Util.nullToString(bankDepositListSearch.get(i).getDeposit_dt()) + "\"");
                jsonSb.append(" ,\"" + Util.nullToString(bankDepositListSearch.get(i).getBank_cd_nm()) + "\"");
                jsonSb.append(" ,\"" + cardNum + "\"");                
                jsonSb.append(" ,\"" + Util.nullToString(bankDepositListSearch.get(i).getPay_mtd_nm()) + "\"");
                jsonSb.append(" ,\"" + Util.nullToString(bankDepositListSearch.get(i).getMerch_no()) + "\"");
                jsonSb.append(" ,\"" + Util.nullToString(bankDepositListSearch.get(i).getDeposit_amt()) + "\"");
                jsonSb.append(" ,\"" + Util.nullToString(bankDepositListSearch.get(i).getCollect_method_nm()) + "\"");
                jsonSb.append(" ,\"" + Util.nullToString(bankDepositListSearch.get(i).getMemo()) + "\"");

                if (i == (bankDepositListSearch.size() - 1)) {
                    jsonSb.append("]}");
                } else {
                    jsonSb.append("]},");
                }

            }
        }else{
//            jsonSb.append("{\"id\": -1");
//            jsonSb.append(",\"data\":[");
//            jsonSb.append(" \"조회된 결과가 없습니다.\"");
//            jsonSb.append("]}");
        }

        jsonSb.append("]}");

        ht.put("bankDepositListSearchJsonString", jsonSb.toString());

        return ht;


    }

    //통장입금내역 상세 조회
    @Override
    public Bank_Deposit_Search bankDepositDetailSearch(BankdepositSearchFromBean bankdepositSearchFromBean) throws Exception {

        return bankDepositDAO.bankDepositDetailSearch(bankdepositSearchFromBean);

    }

    @Override
    public Hashtable bankDepositInsertManualForm(BankdepositInsertFromBean bankdepositInsertFromBean) throws Exception {

        Hashtable ht = new Hashtable();
        //통장 입금내역을 등록하기전에 가맹점 번호로 가맹점 정보를 재조회 한다.

        Integer insertResult = bankDepositDAO.bankDepositInsert(bankdepositInsertFromBean);

        logger.debug("insertResult : "+insertResult);

        if(insertResult == 0){
            ht.put("Result","fail");
        }else{
            ht.put("Result","success");
        }

        return ht;

    }

    @Override
    public Hashtable bankDepositUpdate(BankdepositInsertFromBean bankdepositInsertFromBean) throws Exception {

        Hashtable ht = new Hashtable();
        //통장 입금내역을 수정하기전에 가맹점 번호로 가맹점 정보를 재조회 한다.

        Integer updateResult = bankDepositDAO.bankDepositUpdate(bankdepositInsertFromBean);

        logger.debug(">>>>>> UpdateResult : "+updateResult);

        if(updateResult == 0){
            ht.put("Result","fail");
        }else{
            ht.put("Result","success");
        }

        return ht;
    }

    @Override
    public Hashtable bankDepositDelete(BankdepositInsertFromBean bankdepositInsertFromBean) throws Exception {

        Hashtable ht = new Hashtable();

        //Integer deleteResult = bankDepositDAO.bankDepositUpdate(bankdepositInsertFromBean);
        
        Integer deleteResult = bankDepositDAO.bankDepositDelete(bankdepositInsertFromBean);
        
        logger.debug(">>>>>> bankDepositDelete : "+deleteResult);

        if(deleteResult == 0){
            ht.put("Result","fail");
        }else{
            ht.put("Result","success");
        }

        return ht;
    }

    @Override
    public void bankDepositListExcel(BankdepositSearchFromBean bankdepositSearchFromBean, String excelDownloadFilename, HttpServletResponse response) {
        
        try {
              
                List ls_bankDepositListExcel = bankDepositDAO.bankDepositListExcel(bankdepositSearchFromBean);
                
                StringBuffer bankDepositListExcel = Util.makeData(ls_bankDepositListExcel); 
        
                Util.exceldownload(bankDepositListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    }



}
