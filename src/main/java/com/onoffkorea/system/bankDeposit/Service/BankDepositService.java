package com.onoffkorea.system.bankDeposit.Service;

import com.onoffkorea.system.bankDeposit.Bean.BankdepositInsertFromBean;
import com.onoffkorea.system.bankDeposit.Bean.BankdepositSearchFromBean;
import com.onoffkorea.system.bankDeposit.vo.Bank_Deposit_Search;

import java.util.Hashtable;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by hoyoungher on 2014. 12. 15..
 */
public interface BankDepositService {
    //통장 입금내역 조회
    public Hashtable bankDepositListSearch(BankdepositSearchFromBean bankdepositSearchFromBean) throws Exception;

    public Bank_Deposit_Search bankDepositDetailSearch(BankdepositSearchFromBean bankdepositSearchFromBean) throws Exception;

    //통장 입금내역 등록(Form : 개별)
    public Hashtable bankDepositInsertManualForm(BankdepositInsertFromBean bankdepositInsertFromBean) throws Exception;


    //통장 입금내역 수정
    public Hashtable bankDepositUpdate(BankdepositInsertFromBean bankdepositInsertFromBean) throws Exception;

    //통장 입금내역 삭제
    public Hashtable bankDepositDelete(BankdepositInsertFromBean bankdepositInsertFromBean) throws Exception;

    public void bankDepositListExcel(BankdepositSearchFromBean bankdepositSearchFromBean, String excelDownloadFilename, HttpServletResponse response);

}
