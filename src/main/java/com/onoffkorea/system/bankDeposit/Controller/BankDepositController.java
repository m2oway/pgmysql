/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.onoffkorea.system.bankDeposit.Controller;

import com.onoffkorea.system.bankDeposit.Bean.BankdepositInsertFromBean;
import com.onoffkorea.system.bankDeposit.Bean.BankdepositSearchFromBean;
import com.onoffkorea.system.bankDeposit.Service.BankDepositService;
import com.onoffkorea.system.bankDeposit.vo.*;
import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.mid.Vo.Tb_Mid_Info;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import common.web.servlet.BaseSpringMultiActionController;
import java.text.SimpleDateFormat;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by hoyeongheo on 14. 12. 2..
 * 통장 입금내역 관리
 */
@Controller
@RequestMapping("/bankDeposit/**")
public class BankDepositController extends BaseSpringMultiActionController{

    @Autowired
    private BankDepositService bankDepositService;

    @Autowired
    private CommonService commonService;

    protected Log logger = LogFactory.getLog(getClass());


    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();

        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));

    }


    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {

        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));

    }


    /*
        통장 입금내역 조회 Form
     */
    @RequestMapping(value="/bankDepositList", method= RequestMethod.GET)
    public String bankDepositListForm(BankdepositSearchFromBean bankdepositSearchFromBean,Model model , HttpServletRequest request,@ModelAttribute("siteSessionObj") SiteSession  dentalHopitalSession) throws Exception {
        
           SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        if((bankdepositSearchFromBean.getFrom_date() == null) || (bankdepositSearchFromBean.getFrom_date()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            bankdepositSearchFromBean.setFrom_date(start_dt);
            bankdepositSearchFromBean.setTo_date(end_dt);
        }

        logger.debug("bankDepositListForm : >>>>>>>>>>>");

        Tb_Mid_Info tb_Merch = new Tb_Mid_Info();

        ArrayList<Tb_Mid_Info> tb_Merch_list = null;

        StringBuilder sb = new StringBuilder();
        sb.append("{rows : []}");
        
        SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
        Date start_date = formatter.parse(bankdepositSearchFromBean.getFrom_date());
        Date end_date   = formatter.parse(bankdepositSearchFromBean.getTo_date());
        
        String start_dt = formatter2.format(start_date);
        String end_dt = formatter2.format(end_date);

        bankdepositSearchFromBean.setFrom_date(start_dt);
        bankdepositSearchFromBean.setTo_date(end_dt);
        
        
        
        model.addAttribute("bankdepositSearchFromBean",bankdepositSearchFromBean);
        model.addAttribute("bankDepositList",sb.toString());
        model.addAttribute("accountNoList", commonService.bankAccountNoSelect());

        return null;
    }


    /*
        통장 입금내역 조회
     */
    @RequestMapping(value="/bankDepositList", method= RequestMethod.POST)
    @ResponseBody
    public String bankDepositListSearch(@Valid BankdepositSearchFromBean bankdepositSearchFromBean, Model model , HttpServletRequest request) throws Exception {

        Hashtable ht_bankDepositListSearch = bankDepositService.bankDepositListSearch(bankdepositSearchFromBean);

        String ls_ht_bankDepositListSearch = (String) ht_bankDepositListSearch.get("bankDepositListSearchJsonString");

        return ls_ht_bankDepositListSearch;
    }

    /*
    * 통장 입금내역 등록 폼
    * */
    @RequestMapping(value="/bankDepositInsert", method= RequestMethod.GET)
     public String bankDepositInsertForm(Model model , HttpServletRequest request,@ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        Tb_Mid_Info tb_Merch = new Tb_Mid_Info();

        CommonCodeSearchFormBean commonCodeSearchFormBean= new CommonCodeSearchFormBean();

        ArrayList<Tb_Mid_Info>  tb_merchs = commonService.briefMerchantList();

        commonCodeSearchFormBean.setMain_code("COLLECT_METHOD");

        Tb_Code_Main collect_method = commonService.codeSearch(commonCodeSearchFormBean);

        model.addAttribute("tb_merchs", tb_merchs);

        model.addAttribute("collect_methods", collect_method);

        return null;
    }

    /*
    * 통장 입금내역 등록
    * */
    @RequestMapping(value="/bankDepositInsertManualForm", method= RequestMethod.POST)
    @ResponseBody
     public String bankDepositInsertManualForm(BankdepositInsertFromBean bankdepositInsertFromBean, Model model , HttpServletRequest request,@ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        Hashtable<String,String> ht = null;

        bankdepositInsertFromBean.setIns_user(dentalHopitalSession.getUser_seq());

        ht = bankDepositService.bankDepositInsertManualForm(bankdepositInsertFromBean);

        return ht.get("Result");
    }


    /*
    * 통장 입금내역 수정 폼
    * */
    @RequestMapping(value="/bankDepositUpdate", method= RequestMethod.GET)
    public String bankDepositUpdateForm(@Valid BankdepositSearchFromBean bankdepositSearchFromBean, Model model , HttpServletRequest request,@ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {


        logger.debug(">>>>>>>>>>>>>>>>>>>>>>>> getBank_dep_seq :"+bankdepositSearchFromBean.getBank_dep_seq() );


        CommonCodeSearchFormBean commonCodeSearchFormBean= new CommonCodeSearchFormBean();

        ArrayList<Tb_Mid_Info>  tb_merchs= commonService.briefMerchantList();

        commonCodeSearchFormBean.setMain_code("COLLECT_METHOD");

        Tb_Code_Main collect_method = commonService.codeSearch(commonCodeSearchFormBean);

        Bank_Deposit_Search bank_Deposit_Search = bankDepositService.bankDepositDetailSearch(bankdepositSearchFromBean);

        model.addAttribute("tb_merchs", tb_merchs);

        model.addAttribute("collect_methods", collect_method);

        model.addAttribute("bank_Deposit_Search", bank_Deposit_Search);

        return null;
    }


    /*
   * 통장 입금내역 수정
   * */
    @RequestMapping(value="/bankDepositUpdate", method= RequestMethod.POST)
    @ResponseBody
    public String bankDepositUpdate(@Valid BankdepositInsertFromBean bankdepositInsertFromBean, Model model , HttpServletRequest request,@ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        Hashtable<String,String> ht = null;
        
        String deposit_amt = bankdepositInsertFromBean.getDeposit_amt().replaceAll(",","");
        bankdepositInsertFromBean.setDeposit_amt(deposit_amt);
        

        bankdepositInsertFromBean.setIns_user(dentalHopitalSession.getUser_seq());

        ht = bankDepositService.bankDepositUpdate(bankdepositInsertFromBean);

        return ht.get("Result");

    }


    /*
  * 통장 입금내역 삭제
  * */
    @RequestMapping(value="/bankDepositDelete", method= RequestMethod.POST)
    @ResponseBody
    public String bankDepositDelete(@Valid BankdepositInsertFromBean bankdepositInsertFromBean, Model model , HttpServletRequest request,@ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        Hashtable<String,String> ht = null;

        bankdepositInsertFromBean.setIns_user(dentalHopitalSession.getUser_seq());

        ht = bankDepositService.bankDepositDelete(bankdepositInsertFromBean);

        return ht.get("Result");

    }

    /*
        통장 입금내역 조회
     */
    @RequestMapping(value="/bankDepositListExcel", method= RequestMethod.POST)
    public void bankDepositListExcel(@Valid BankdepositSearchFromBean bankdepositSearchFromBean, Model model , HttpServletRequest request, HttpServletResponse response) throws Exception {

        String excelDownloadFilename = new String(getMessage("bankDeposit.bankDepositListExcel.fileName"));
        
        bankDepositService.bankDepositListExcel(bankdepositSearchFromBean, excelDownloadFilename, response); 
        
    }

}
