/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.middeposit.Bean;

import com.onoffkorea.system.common.util.CommonListPagingForm;
import java.io.Serializable;

/**
 *
 * @author Administrator
 */
public class MiddepositFormBean extends CommonListPagingForm implements Serializable {

    private String deposit_dt     ;
    private String merch_no       ;
    private String pay_mtd        ;
    private String deposit_seq    ;
    private String bank_cd        ;
    private String acc_no         ;
    private String tot_cnt        ;
    private String tot_amt        ;
    private String acq_cnt        ;
    private String acq_amt        ;
    private String ret_cnt        ;
    private String ret_amt        ;
    private String dfr_cnt        ;
    private String dfr_amt        ;
    private String dfr_rese_cnt   ;
    private String dfr_rese_amt   ;
    private String deposit_amt    ;
    private String in_amt         ;
    private String mis_amt        ;
    private String out_amt        ;
    private String decision_dt    ;
    private String decision_user  ;
    private String proc_flag      ;
    private String ins_dt         ;
    private String mod_dt         ;
    private String ins_user       ;
    private String mod_user       ;   
    private String deposit_start_dt;
    private String deposit_end_dt;
    private String pay_dt;
    private String sal_start_dt;
    private String sal_end_dt;
    private String pay_type;
    private String aprv_no;
    private String tot_trx_amt;
    private String acq_cd;
    private String seq;
    private String user_seq;
    private String acq_dt;

    public String getAcq_dt() {
        return acq_dt;
    }

    public void setAcq_dt(String acq_dt) {
        this.acq_dt = acq_dt;
    }

    public String getUser_seq() {
        return user_seq;
    }

    public void setUser_seq(String user_seq) {
        this.user_seq = user_seq;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getAprv_no() {
        return aprv_no;
    }

    public void setAprv_no(String aprv_no) {
        this.aprv_no = aprv_no;
    }

    public String getTot_trx_amt() {
        return tot_trx_amt;
    }

    public void setTot_trx_amt(String tot_trx_amt) {
        this.tot_trx_amt = tot_trx_amt;
    }

    public String getAcq_cd() {
        return acq_cd;
    }

    public void setAcq_cd(String acq_cd) {
        this.acq_cd = acq_cd;
    }

    public String getPay_type() {
        return pay_type;
    }

    public void setPay_type(String pay_type) {
        this.pay_type = pay_type;
    }

    public String getSal_start_dt() {
        return sal_start_dt;
    }

    public void setSal_start_dt(String sal_start_dt) {
        this.sal_start_dt = sal_start_dt;
    }

    public String getSal_end_dt() {
        return sal_end_dt;
    }

    public void setSal_end_dt(String sal_end_dt) {
        this.sal_end_dt = sal_end_dt;
    }

    public String getPay_dt() {
        return pay_dt;
    }

    public void setPay_dt(String pay_dt) {
        this.pay_dt = pay_dt;
    }

    public String getDeposit_start_dt() {
        return deposit_start_dt;
    }

    public void setDeposit_start_dt(String deposit_start_dt) {
        this.deposit_start_dt = deposit_start_dt;
    }

    public String getDeposit_end_dt() {
        return deposit_end_dt;
    }

    public void setDeposit_end_dt(String deposit_end_dt) {
        this.deposit_end_dt = deposit_end_dt;
    }

    public String getDeposit_dt() {
        return deposit_dt;
    }

    public void setDeposit_dt(String deposit_dt) {
        this.deposit_dt = deposit_dt;
    }

    public String getMerch_no() {
        return merch_no;
    }

    public void setMerch_no(String merch_no) {
        this.merch_no = merch_no;
    }

    public String getPay_mtd() {
        return pay_mtd;
    }

    public void setPay_mtd(String pay_mtd) {
        this.pay_mtd = pay_mtd;
    }

    public String getDeposit_seq() {
        return deposit_seq;
    }

    public void setDeposit_seq(String deposit_seq) {
        this.deposit_seq = deposit_seq;
    }

    public String getBank_cd() {
        return bank_cd;
    }

    public void setBank_cd(String bank_cd) {
        this.bank_cd = bank_cd;
    }

    public String getAcc_no() {
        return acc_no;
    }

    public void setAcc_no(String acc_no) {
        this.acc_no = acc_no;
    }

    public String getTot_cnt() {
        return tot_cnt;
    }

    public void setTot_cnt(String tot_cnt) {
        this.tot_cnt = tot_cnt;
    }

    public String getTot_amt() {
        return tot_amt;
    }

    public void setTot_amt(String tot_amt) {
        this.tot_amt = tot_amt;
    }

    public String getAcq_cnt() {
        return acq_cnt;
    }

    public void setAcq_cnt(String acq_cnt) {
        this.acq_cnt = acq_cnt;
    }

    public String getAcq_amt() {
        return acq_amt;
    }

    public void setAcq_amt(String acq_amt) {
        this.acq_amt = acq_amt;
    }

    public String getRet_cnt() {
        return ret_cnt;
    }

    public void setRet_cnt(String ret_cnt) {
        this.ret_cnt = ret_cnt;
    }

    public String getRet_amt() {
        return ret_amt;
    }

    public void setRet_amt(String ret_amt) {
        this.ret_amt = ret_amt;
    }

    public String getDfr_cnt() {
        return dfr_cnt;
    }

    public void setDfr_cnt(String dfr_cnt) {
        this.dfr_cnt = dfr_cnt;
    }

    public String getDfr_amt() {
        return dfr_amt;
    }

    public void setDfr_amt(String dfr_amt) {
        this.dfr_amt = dfr_amt;
    }

    public String getDfr_rese_cnt() {
        return dfr_rese_cnt;
    }

    public void setDfr_rese_cnt(String dfr_rese_cnt) {
        this.dfr_rese_cnt = dfr_rese_cnt;
    }

    public String getDfr_rese_amt() {
        return dfr_rese_amt;
    }

    public void setDfr_rese_amt(String dfr_rese_amt) {
        this.dfr_rese_amt = dfr_rese_amt;
    }

    public String getDeposit_amt() {
        return deposit_amt;
    }

    public void setDeposit_amt(String deposit_amt) {
        this.deposit_amt = deposit_amt;
    }

    public String getIn_amt() {
        return in_amt;
    }

    public void setIn_amt(String in_amt) {
        this.in_amt = in_amt;
    }

    public String getMis_amt() {
        return mis_amt;
    }

    public void setMis_amt(String mis_amt) {
        this.mis_amt = mis_amt;
    }

    public String getOut_amt() {
        return out_amt;
    }

    public void setOut_amt(String out_amt) {
        this.out_amt = out_amt;
    }

    public String getDecision_dt() {
        return decision_dt;
    }

    public void setDecision_dt(String decision_dt) {
        this.decision_dt = decision_dt;
    }

    public String getDecision_user() {
        return decision_user;
    }

    public void setDecision_user(String decision_user) {
        this.decision_user = decision_user;
    }

    public String getProc_flag() {
        return proc_flag;
    }

    public void setProc_flag(String proc_flag) {
        this.proc_flag = proc_flag;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }
    
}
