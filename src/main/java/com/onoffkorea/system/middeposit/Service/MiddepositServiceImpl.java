/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.middeposit.Service;

import com.onoffkorea.system.acq_result.Vo.Tb_Acq_Result;
import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.middeposit.Bean.MiddepositFormBean;
import com.onoffkorea.system.middeposit.Dao.MiddepositDAO;
import com.onoffkorea.system.middeposit.Vo.Tb_Mid_Deposit;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service("middepositService")
public class MiddepositServiceImpl implements MiddepositService{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private CommonService commonService;    
    
    @Autowired
    private MiddepositDAO middepositDAO;       

    @Override
    public Hashtable middepositMasterList(MiddepositFormBean middepositFormBean) {

        Hashtable ht = new Hashtable();

        try {
            
                List<Tb_Mid_Deposit> ls_middepositMasterList = middepositDAO.middepositMasterList(middepositFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"rows\" : [");
                
                for (int i = 0; i < ls_middepositMasterList.size(); i++) {
                        Tb_Mid_Deposit tb_Mid_Deposit = ls_middepositMasterList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"deposit_dt\":\""+Util.nullToString(tb_Mid_Deposit.getDeposit_dt())+"\"");
                        sb.append(",\"merch_no\":\""+Util.nullToString(tb_Mid_Deposit.getMerch_no())+"\"");
                        sb.append(",\"pay_mtd\":\""+Util.nullToString(tb_Mid_Deposit.getPay_mtd())+"\"");
                        sb.append(",\"seq\":\""+Util.nullToString(tb_Mid_Deposit.getDeposit_seq())+"\"");
                        sb.append(",\"proc_flag\":\""+Util.nullToString(tb_Mid_Deposit.getProc_flag())+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Mid_Deposit.getDeposit_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getPay_mtd_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getMerch_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getMid_nm()) + "\"");
                        sb.append(",\"" + Util.nullToInt(tb_Mid_Deposit.getDeposit_amt()) + "\"");
                        sb.append(",\"" + Util.nullToInt(tb_Mid_Deposit.getIn_amt()) + "\"");
                        
                        switch(Util.nullToString(tb_Mid_Deposit.getProc_flag())){
                            case "N":
                                //sb.append(" ,\"<span class=misAmt row_id="+i+">" + Util.nullToString(Util.addComma(tb_Mid_Deposit.getMis_amt())) + "</span>\"");
                                //sb.append(" ,\"" + Util.nullToInt(tb_Mid_Deposit.getMis_amt()) + "\"");
                                sb.append(" ,\"<span class=misAmt row_id="+i+">" + Util.addComma(Util.nullToInt(tb_Mid_Deposit.getMis_amt())) + "</span>\"");
                                break;
                            case "Y":
                                //sb.append(" ,\"" + Util.nullToString(Util.addComma(tb_Mid_Deposit.getMis_amt())) + "\"");
                                //sb.append(" ,\"" + Util.nullToInt(tb_Mid_Deposit.getMis_amt()) + "\"");
                                sb.append(" ,\"" + Util.addComma(Util.nullToInt(tb_Mid_Deposit.getMis_amt())) + "\"");
                                break;
                        }                        

                        switch(Util.nullToString(tb_Mid_Deposit.getProc_flag())){
                            case "N":
                                //sb.append(" ,\"<span class=outAmt row_id="+i+">" + Util.nullToString(Util.addComma(tb_Mid_Deposit.getOut_amt())) + "</span>\"");
                                sb.append(" ,\"<span class=outAmt row_id="+i+">" + Util.addComma(Util.nullToInt(tb_Mid_Deposit.getOut_amt())) + "</span>\"");
                                break;
                            case "Y":
                                //sb.append(" ,\"<span class=outAmtProc row_id="+i+">" + Util.nullToString(Util.addComma(tb_Mid_Deposit.getOut_amt())) + "</span>\"");
                                sb.append(" ,\"<span class=outAmtProc row_id="+i+">" + Util.addComma(Util.nullToInt(tb_Mid_Deposit.getOut_amt())) + "</span>\"");
                                break;
                        }                        
                        
                        sb.append(",\"" + Util.nullToInt(tb_Mid_Deposit.getAcq_amt()) + "\"");
                        sb.append(",\"" + Util.nullToInt(tb_Mid_Deposit.getCommission()) + "\"");
                        
                        sb.append(",\"" + Util.nullToInt(tb_Mid_Deposit.getOnofftid_pay_amt()) + "\"");
                        sb.append(",\"" + Util.nullToInt(tb_Mid_Deposit.getAgent_fee()) + "\"");                        
                        sb.append(",\"" + Util.nullToInt(tb_Mid_Deposit.getProfit()) + "\"");
                                             

                        switch(Util.nullToString(tb_Mid_Deposit.getProc_flag())){
                            case "N" :
                                sb.append(" ,\"<button class=procButton row_id="+i+">입금확정</button>\"");
                                break;
                            case "Y" :
                                sb.append(" ,\"<button class=procCancelButton row_id="+i+">확정취소</button>\"");
                                break;
                        }

                        if (i == (ls_middepositMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
                commonCodeSearchFormBean.setMain_code("MID_PAY_MTD");
                Tb_Code_Main midPayMtdList = commonService.codeSearch(commonCodeSearchFormBean);
                
                commonCodeSearchFormBean.setMain_code("PROC_FLAG");
                Tb_Code_Main procFlagList = commonService.codeSearch(commonCodeSearchFormBean);
                
                ht.put("midPayMtdList", midPayMtdList);
                ht.put("procFlagList", procFlagList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;               
        
    }

    //입금정산 상세 조회
    @Override
    public Hashtable middepositDetailList(MiddepositFormBean middepositFormBean) {

        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = middepositDAO.middepositMasterListCount(middepositFormBean);
                List<Tb_Acq_Result> ls_middepositDetailList = middepositDAO.middepositDetailList(middepositFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(middepositFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_middepositDetailList.size(); i++) {
                        Tb_Acq_Result tb_Acq_Result = ls_middepositDetailList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Acq_Result.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getPay_type_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getMerch_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getRecord_cl_cd_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getAcq_cd_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getSal_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getAprv_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getTot_trx_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getVat_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getInstallment()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getAcq_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getPay_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getCmms_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getPay_amt()) + "\"");

                        if (i == (ls_middepositDetailList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);
                
                commonCodeSearchFormBean.setMain_code("ACQ_CD");
                Tb_Code_Main acqCdList = commonService.codeSearch(commonCodeSearchFormBean);
                
                ht.put("payChnCateList", payChnCateList);
                ht.put("acqCdList", acqCdList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;                   
        
    }

    //입금확정
    @Override
    @Transactional
    public Hashtable depositMetabolismConfirmProcess(MiddepositFormBean middepositFormBean) {
        
        Hashtable ht = new Hashtable();
        
        Integer updateResult = 0;

        //입금확정 등록        
        List<Tb_Mid_Deposit> ls_middepositMasterList = middepositDAO.middepositMasterList(middepositFormBean);
        Tb_Mid_Deposit tb_Mid_Deposit = ls_middepositMasterList.get(0);
        tb_Mid_Deposit.setUser_seq(middepositFormBean.getUser_seq());

        updateResult = middepositDAO.depositMetabolismConfirmProcess(tb_Mid_Deposit);        
        
        //입반내역 입금확정
        middepositDAO.depositAcqDetailProcFlagYUpdate(middepositFormBean);
        
        //통장입금내역 입금확정  
        middepositDAO.depositBankDetailProcFlagYUpdate(middepositFormBean);

        if(updateResult == 0){
            ht.put("Result","fail");
        }else{
            ht.put("Result","success");
        }

        return ht;        
        
    }

    //입금확정취소
    @Override
    @Transactional
    public Hashtable depositMetabolismCancelProcess(MiddepositFormBean middepositFormBean) {
        
        Hashtable ht = new Hashtable();

        Integer updateResult = 0;
        
        //입금확정 삭제
        updateResult = middepositDAO.depositMetabolismCancelProcess(middepositFormBean);
        
        //입반내역 입금확정취소
        middepositDAO.depositAcqDetailProcFlagNUpdate(middepositFormBean);
        
        //통장입금내역 입금확정취소  
        middepositDAO.depositBankDetailProcFlagNUpdate(middepositFormBean);        
         
        if(updateResult == 0){
            ht.put("Result","fail");
        }else{
            ht.put("Result","success");
        }

        return ht;        
        
    }

    @Override
    public void middepositMasterListExcel(MiddepositFormBean middepositFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
              
                List ls_middepositMasterListExcel = middepositDAO.middepositMasterListExcel(middepositFormBean);
                
                StringBuffer middepositMasterListExcel = Util.makeData(ls_middepositMasterListExcel); 
        
                Util.exceldownload(middepositMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }                
        
    }

    @Override
    public void middepositDetailListExcel(MiddepositFormBean middepositFormBean, String excelDownloadFilename, HttpServletResponse response) {
        
        try {
              
                List ls_middepositDetailListExcel = middepositDAO.middepositDetailListExcel(middepositFormBean);
                
                StringBuffer middepositDetailListExcel = Util.makeData(ls_middepositDetailListExcel); 
        
                Util.exceldownload(middepositDetailListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }                     
        
    }
    
}
