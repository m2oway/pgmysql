/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.middeposit.Service;

import com.onoffkorea.system.middeposit.Bean.MiddepositFormBean;
import java.util.Hashtable;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public interface MiddepositService {

    public Hashtable middepositMasterList(MiddepositFormBean middepositFormBean);

    public Hashtable middepositDetailList(MiddepositFormBean middepositFormBean);

    public Hashtable depositMetabolismConfirmProcess(MiddepositFormBean middepositFormBean);

    public Hashtable depositMetabolismCancelProcess(MiddepositFormBean middepositFormBean);

    public void middepositMasterListExcel(MiddepositFormBean middepositFormBean, String excelDownloadFilename, HttpServletResponse response);

    public void middepositDetailListExcel(MiddepositFormBean middepositFormBean, String excelDownloadFilename, HttpServletResponse response);
    
}
