/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.middeposit.Controller;

import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.mid.Bean.MidFormBean;
import com.onoffkorea.system.middeposit.Bean.MiddepositFormBean;
import com.onoffkorea.system.middeposit.Service.MiddepositService;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import common.web.servlet.BaseSpringMultiActionController;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/middeposit/**")
public class MiddepositController extends BaseSpringMultiActionController{
        
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private CommonService commonService;
    
    @Autowired
    private MiddepositService middepositService;    
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }    

    //입금정산 레이아웃
    @RequestMapping(method= RequestMethod.GET,value = "/middepositLayout")
    public String middepositLayout(@Valid MidFormBean midFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        return null;
        
    }
    
    //입금정산 조회
    @RequestMapping(method= RequestMethod.GET,value = "/middepositMasterList")
    public String middepositMasterFormList(@Valid MiddepositFormBean middepositFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        
        
           SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        if((middepositFormBean.getDeposit_start_dt() == null) || (middepositFormBean.getDeposit_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            middepositFormBean.setDeposit_start_dt(start_dt);
            middepositFormBean.setDeposit_end_dt(end_dt);
        }

        Hashtable ht_middepositMasterList = middepositService.middepositMasterList(middepositFormBean);
        String ls_middepositMasterList = (String) ht_middepositMasterList.get("ResultSet");
        
        Tb_Code_Main midPayMtdList = (Tb_Code_Main) ht_middepositMasterList.get("midPayMtdList");
        Tb_Code_Main procFlagList = (Tb_Code_Main) ht_middepositMasterList.get("procFlagList");
        
        SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
        Date start_date = formatter.parse(middepositFormBean.getDeposit_start_dt());
        Date end_date   = formatter.parse(middepositFormBean.getDeposit_end_dt());
        
        String start_dt = formatter2.format(start_date);
        String end_dt = formatter2.format(end_date);

        middepositFormBean.setDeposit_start_dt(start_dt);
        middepositFormBean.setDeposit_end_dt(end_dt);
        
        model.addAttribute("ls_middepositMasterList",ls_middepositMasterList);     
        model.addAttribute("midPayMtdList",midPayMtdList);     
        model.addAttribute("procFlagList",procFlagList);             
        
        return null;
        
    }
    
    //입금정산 조회
    @RequestMapping(method= RequestMethod.POST,value = "/middepositMasterList")
    @ResponseBody
    public String middepositMasterList(@Valid MiddepositFormBean middepositFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_middepositMasterList = middepositService.middepositMasterList(middepositFormBean);
        String ls_middepositMasterList = (String) ht_middepositMasterList.get("ResultSet");
        
        return ls_middepositMasterList;
        
    }    
    
    //입금정산 상세 조회
    @RequestMapping(method= RequestMethod.GET,value = "/middepositDetailList")
    public String middepositDetailFormList(@Valid MiddepositFormBean middepositFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_middepositDetailList = middepositService.middepositDetailList(middepositFormBean);
        String ls_middepositDetailList = (String) ht_middepositDetailList.get("ResultSet");
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_middepositDetailList.get("payChnCateList");
        
        Tb_Code_Main acqCdList = (Tb_Code_Main) ht_middepositDetailList.get("acqCdList");
        
        model.addAttribute("ls_middepositDetailList",ls_middepositDetailList);     
        model.addAttribute("payChnCateList",payChnCateList);             
        model.addAttribute("acqCdList",acqCdList);       
        
        return null;
        
    }
    
    //입금정산 조회
    @RequestMapping(method= RequestMethod.POST,value = "/middepositDetailList")
    @ResponseBody
    public String middepositDetailList(@Valid MiddepositFormBean middepositFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_middepositDetailList = middepositService.middepositDetailList(middepositFormBean);
        String ls_middepositDetailList = (String) ht_middepositDetailList.get("ResultSet");
        
        return ls_middepositDetailList;
        
    }        
    
    /*
     *  입금대사 확정 처리
     */
    @RequestMapping(value="/depositMetabolismConfirmProcess", method= RequestMethod.POST)
    @ResponseBody
    public String depositMetabolismConfirmProcess(@Valid MiddepositFormBean middepositFormBean, Model model , HttpServletRequest request,@ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        middepositFormBean.setUser_seq(siteSession.getUser_seq());

        Hashtable ht_depositMetabolismConfirmProcess = middepositService.depositMetabolismConfirmProcess(middepositFormBean);

        return (String)ht_depositMetabolismConfirmProcess.get("Result");
    }    
    
    /*
     *  입금대사 확정 취소 처리
     */
    @RequestMapping(value="/depositMetabolismCancelProcess", method= RequestMethod.POST)
    @ResponseBody
    public String depositMetabolismCancelProcess(@Valid MiddepositFormBean middepositFormBean, Model model , HttpServletRequest request,@ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        middepositFormBean.setUser_seq(siteSession.getUser_seq());

        Hashtable ht_depositMetabolismCancelProcess = middepositService.depositMetabolismCancelProcess(middepositFormBean);

        return (String)ht_depositMetabolismCancelProcess.get("Result");
    }    
    
    //입금정산 엑셀
    @RequestMapping(method= RequestMethod.POST,value = "/middepositMasterListExcel")
    public void middepositMasterListExcel(@Valid MiddepositFormBean middepositFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, HttpServletResponse response) throws Exception {

        String excelDownloadFilename = new String(getMessage("middeposit.middepositMasterListExcel.fileName"));
        
        middepositService.middepositMasterListExcel(middepositFormBean, excelDownloadFilename, response); 

    }        
    
    //입금정산 상세내역 엑셀
    @RequestMapping(method= RequestMethod.POST,value = "/middepositDetailListExcel")
    public void middepositDetailListExcel(@Valid MiddepositFormBean middepositFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, HttpServletResponse response) throws Exception { 

        String excelDownloadFilename = new String(getMessage("middeposit.middepositDetailListExcel.fileName"));
        
        middepositService.middepositDetailListExcel(middepositFormBean, excelDownloadFilename, response); 

    }            

}
