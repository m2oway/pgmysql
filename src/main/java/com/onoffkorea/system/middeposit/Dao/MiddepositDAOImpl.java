/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.middeposit.Dao;

import com.onoffkorea.system.acq_result.Vo.Tb_Acq_Result;
import com.onoffkorea.system.middeposit.Bean.MiddepositFormBean;
import com.onoffkorea.system.middeposit.Vo.Tb_Mid_Deposit;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("middepositDAO")
public class MiddepositDAOImpl extends SqlSessionDaoSupport implements MiddepositDAO{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    //입금정산 조회
    @Override
    public List<Tb_Mid_Deposit> middepositMasterList(MiddepositFormBean middepositFormBean) {
        
        return (List)getSqlSession().selectList("Middeposit.middepositMasterList",middepositFormBean);
        
    }

    //입금정산 조회
    @Override
    public List<Tb_Acq_Result> middepositDetailList(MiddepositFormBean middepositFormBean) {
        
        return (List)getSqlSession().selectList("Middeposit.middepositDetailList",middepositFormBean);
        
    }

    //입금확정
    @Override
    public Integer depositMetabolismConfirmProcess(Tb_Mid_Deposit tb_Mid_Deposit) {
        
        return (Integer)getSqlSession().insert("Middeposit.depositMetabolismConfirmProcess",tb_Mid_Deposit);
        
    }

    //입금확정취소
    @Override
    public Integer depositMetabolismCancelProcess(MiddepositFormBean middepositFormBean) {
        
        return (Integer)getSqlSession().delete("Middeposit.depositMetabolismCancelProcess",middepositFormBean);
        
    }

    //입금정산 상세 내역 카운트
    @Override
    public Integer middepositMasterListCount(MiddepositFormBean middepositFormBean) {
        
        return (Integer)getSqlSession().selectOne("Middeposit.middepositDetailListCount",middepositFormBean);
        
    }

    //입반내역 입금확정 처리
    @Override
    public void depositAcqDetailProcFlagYUpdate(MiddepositFormBean middepositFormBean) {
        
        getSqlSession().update("Middeposit.depositAcqDetailProcFlagYUpdate",middepositFormBean);
        
    }
    
    //통장입금내역 입금확정 처리
    @Override
    public void depositBankDetailProcFlagYUpdate(MiddepositFormBean middepositFormBean) {
        
        getSqlSession().update("Middeposit.depositBankDetailProcFlagYUpdate",middepositFormBean);
        
    }     
    
    //입반내역 입금확정취소 처리
    @Override
    public void depositAcqDetailProcFlagNUpdate(MiddepositFormBean middepositFormBean) {
        
        getSqlSession().update("Middeposit.depositAcqDetailProcFlagNUpdate",middepositFormBean);
        
    }
    
    //통장입금내역 입금확정취소
    @Override
    public void depositBankDetailProcFlagNUpdate(MiddepositFormBean middepositFormBean) {
        
        getSqlSession().update("Middeposit.depositBankDetailProcFlagNUpdate",middepositFormBean);
        
    }         

    @Override
    public List middepositMasterListExcel(MiddepositFormBean middepositFormBean) {
        
        return (List)getSqlSession().selectList("Middeposit.middepositMasterListExcel",middepositFormBean);
        
    }
    
    @Override
    public List middepositDetailListExcel(MiddepositFormBean middepositFormBean) {
        
        return (List)getSqlSession().selectList("Middeposit.middepositDetailListExcel",middepositFormBean);
        
    }    
    
}
