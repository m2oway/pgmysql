/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.middeposit.Dao;

import com.onoffkorea.system.acq_result.Vo.Tb_Acq_Result;
import com.onoffkorea.system.middeposit.Bean.MiddepositFormBean;
import com.onoffkorea.system.middeposit.Vo.Tb_Mid_Deposit;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface MiddepositDAO {

    public List<Tb_Mid_Deposit> middepositMasterList(MiddepositFormBean middepositFormBean);

    public List<Tb_Acq_Result> middepositDetailList(MiddepositFormBean middepositFormBean);

    public Integer depositMetabolismConfirmProcess(Tb_Mid_Deposit tb_Mid_Deposit);

    public Integer depositMetabolismCancelProcess(MiddepositFormBean middepositFormBean);

    public Integer middepositMasterListCount(MiddepositFormBean middepositFormBean);

    public void depositAcqDetailProcFlagYUpdate(MiddepositFormBean middepositFormBean);

    public void depositBankDetailProcFlagYUpdate(MiddepositFormBean middepositFormBean);

    public void depositAcqDetailProcFlagNUpdate(MiddepositFormBean middepositFormBean);

    public void depositBankDetailProcFlagNUpdate(MiddepositFormBean middepositFormBean);

    public List middepositMasterListExcel(MiddepositFormBean middepositFormBean);

    public List middepositDetailListExcel(MiddepositFormBean middepositFormBean);
    
}
