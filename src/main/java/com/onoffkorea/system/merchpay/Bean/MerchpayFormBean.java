/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.merchpay.Bean;

import com.onoffkorea.system.common.util.CommonListPagingForm;

/**
 *
 * @author Administrator
 */
public class MerchpayFormBean extends CommonListPagingForm{
    
    private String pay_chn_cate    ;
    private String onfftid         ;
    private String gen_dt          ;
    private String pay_sche_seq    ;
    private String exp_pay_dt      ;
    private String pay_amt         ;
    private String pay_targetamt   ;
    private String commission      ;
    private String app_amt         ;
    private String cncl_amt        ;
    private String out_amt         ;
    private String decision_pay_amt;
    private String decision_dt     ;
    private String decision_flag   ;
    private String ins_dt          ;
    private String mod_dt          ;
    private String ins_user        ;
    private String mod_user        ;
    private String merch_nm        ;
    private String pay_chn_cate_nm ;
    private String exp_pay_start_dt;
    private String exp_pay_end_dt;
    private String seq;
    private String user_seq;
    private String onffmerch_no;
    private String onfftid_nm;
    private String dec_dfr_amt;
    private String rel_dfr_amt;
    private String setoff_amt;
    private String outamt_seq;

    public String getOutamt_seq() {
        return outamt_seq;
    }

    public void setOutamt_seq(String outamt_seq) {
        this.outamt_seq = outamt_seq;
    }

    public String getDec_dfr_amt() {
        return dec_dfr_amt;
    }

    public void setDec_dfr_amt(String dec_dfr_amt) {
        this.dec_dfr_amt = dec_dfr_amt;
    }

    public String getRel_dfr_amt() {
        return rel_dfr_amt;
    }

    public void setRel_dfr_amt(String rel_dfr_amt) {
        this.rel_dfr_amt = rel_dfr_amt;
    }

    public String getSetoff_amt() {
        return setoff_amt;
    }

    public void setSetoff_amt(String setoff_amt) {
        this.setoff_amt = setoff_amt;
    }

    public String getOnfftid_nm() {
        return onfftid_nm;
    }

    public void setOnfftid_nm(String onfftid_nm) {
        this.onfftid_nm = onfftid_nm;
    }

    public String getOnffmerch_no() {
        return onffmerch_no;
    }

    public void setOnffmerch_no(String onffmerch_no) {
        this.onffmerch_no = onffmerch_no;
    }

    public String getUser_seq() {
        return user_seq;
    }

    public void setUser_seq(String user_seq) {
        this.user_seq = user_seq;
    }

    public String getExp_pay_start_dt() {
        return exp_pay_start_dt;
    }

    public void setExp_pay_start_dt(String exp_pay_start_dt) {
        this.exp_pay_start_dt = exp_pay_start_dt;
    }

    public String getExp_pay_end_dt() {
        return exp_pay_end_dt;
    }

    public void setExp_pay_end_dt(String exp_pay_end_dt) {
        this.exp_pay_end_dt = exp_pay_end_dt;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getPay_chn_cate() {
        return pay_chn_cate;
    }

    public void setPay_chn_cate(String pay_chn_cate) {
        this.pay_chn_cate = pay_chn_cate;
    }

    public String getOnfftid() {
        return onfftid;
    }

    public void setOnfftid(String onfftid) {
        this.onfftid = onfftid;
    }

    public String getGen_dt() {
        return gen_dt;
    }

    public void setGen_dt(String gen_dt) {
        this.gen_dt = gen_dt;
    }

    public String getPay_sche_seq() {
        return pay_sche_seq;
    }

    public void setPay_sche_seq(String pay_sche_seq) {
        this.pay_sche_seq = pay_sche_seq;
    }

    public String getExp_pay_dt() {
        return exp_pay_dt;
    }

    public void setExp_pay_dt(String exp_pay_dt) {
        this.exp_pay_dt = exp_pay_dt;
    }

    public String getPay_amt() {
        return pay_amt;
    }

    public void setPay_amt(String pay_amt) {
        this.pay_amt = pay_amt;
    }

    public String getPay_targetamt() {
        return pay_targetamt;
    }

    public void setPay_targetamt(String pay_targetamt) {
        this.pay_targetamt = pay_targetamt;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getApp_amt() {
        return app_amt;
    }

    public void setApp_amt(String app_amt) {
        this.app_amt = app_amt;
    }

    public String getCncl_amt() {
        return cncl_amt;
    }

    public void setCncl_amt(String cncl_amt) {
        this.cncl_amt = cncl_amt;
    }

    public String getOut_amt() {
        return out_amt;
    }

    public void setOut_amt(String out_amt) {
        this.out_amt = out_amt;
    }

    public String getDecision_pay_amt() {
        return decision_pay_amt;
    }

    public void setDecision_pay_amt(String decision_pay_amt) {
        this.decision_pay_amt = decision_pay_amt;
    }

    public String getDecision_dt() {
        return decision_dt;
    }

    public void setDecision_dt(String decision_dt) {
        this.decision_dt = decision_dt;
    }

    public String getDecision_flag() {
        return decision_flag;
    }

    public void setDecision_flag(String decision_flag) {
        this.decision_flag = decision_flag;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    public String getMerch_nm() {
        return merch_nm;
    }

    public void setMerch_nm(String merch_nm) {
        this.merch_nm = merch_nm;
    }

    public String getPay_chn_cate_nm() {
        return pay_chn_cate_nm;
    }

    public void setPay_chn_cate_nm(String pay_chn_cate_nm) {
        this.pay_chn_cate_nm = pay_chn_cate_nm;
    }
    
}
