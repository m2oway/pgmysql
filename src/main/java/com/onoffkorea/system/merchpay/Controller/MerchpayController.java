/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.merchpay.Controller;

import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.merchpay.Bean.MerchpayFormBean;
import com.onoffkorea.system.merchpay.Service.MerchpayService;
import com.onoffkorea.system.merchpay.Vo.Tb_Merch_Pay;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import com.onoffkorea.system.trans.Bean.TransFormBean;
import common.web.servlet.BaseSpringMultiActionController;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/merchpay/**")
public class MerchpayController extends BaseSpringMultiActionController{
    
    protected Log logger = LogFactory.getLog(getClass());
    
    @Autowired
    private CommonService commonService;
    
    @Autowired
    private MerchpayService merchpayService;      
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }
    
    //지급 조회
    @RequestMapping(method= RequestMethod.GET,value = "/merchpayMasterList")
    public String merchpayMasterFormList(@Valid MerchpayFormBean merchpayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
           SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        if((merchpayFormBean.getExp_pay_start_dt()== null) || (merchpayFormBean.getExp_pay_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            //String start_dt = formatter.format ( cal.getTime());
            String start_dt = formatter.format(currentTime.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            merchpayFormBean.setExp_pay_start_dt(start_dt);
            merchpayFormBean.setExp_pay_end_dt(end_dt);
        }

        Hashtable ht_merchpayMasterFormList = merchpayService.merchpayMasterFormList(merchpayFormBean);
        String ls_merchpayMasterList = (String) ht_merchpayMasterFormList.get("ResultSet");
        
        Tb_Code_Main decisionFlagList = (Tb_Code_Main) ht_merchpayMasterFormList.get("decisionFlagList");
        
        SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
        Date start_date = formatter.parse(merchpayFormBean.getExp_pay_start_dt());
        Date end_date   = formatter.parse(merchpayFormBean.getExp_pay_end_dt());
        
        String start_dt = formatter2.format(start_date);
        String end_dt = formatter2.format(end_date);

        merchpayFormBean.setExp_pay_start_dt(start_dt);
        merchpayFormBean.setExp_pay_end_dt(end_dt);
        
        model.addAttribute("ls_merchpayMasterList",ls_merchpayMasterList);     
        model.addAttribute("decisionFlagList",decisionFlagList);     
        
        return null;
        
    }
    
    //지급 조회
    @RequestMapping(method= RequestMethod.POST,value = "/merchpayMasterList")
    @ResponseBody
    public String merchpayMasterList(@Valid MerchpayFormBean merchpayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        Hashtable ht_merchpayMasterFormList = merchpayService.merchpayMasterFormList(merchpayFormBean);
        String ls_merchpayMasterList = (String) ht_merchpayMasterFormList.get("ResultSet");
        
        return ls_merchpayMasterList;
        
    }       
    
    /*
     *  지급 확정 처리
     */
    @RequestMapping(value="/merchpayMetabolismConfirmProcess", method= RequestMethod.POST)
    @ResponseBody
    public String merchpayMetabolismConfirmProcess(@Valid MerchpayFormBean merchpayFormBean, Model model , HttpServletRequest request,@ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        merchpayFormBean.setUser_seq(siteSession.getUser_seq());

        Hashtable ht_depositMetabolismConfirmProcess = merchpayService.merchpayMetabolismConfirmProcess(merchpayFormBean);

        return (String)ht_depositMetabolismConfirmProcess.get("Result");
    }    
    
    /*
     *  지급 확정 취소 처리
     */
    @RequestMapping(value="/merchpayMetabolismCancelProcess", method= RequestMethod.POST)
    @ResponseBody
    public String merchpayMetabolismCancelProcess(@Valid MerchpayFormBean merchpayFormBean, Model model , HttpServletRequest request,@ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {  
        
        merchpayFormBean.setUser_seq(siteSession.getUser_seq());

        Hashtable ht_depositMetabolismCancelProcess = merchpayService.merchpayMetabolismCancelProcess(merchpayFormBean);

        return (String)ht_depositMetabolismCancelProcess.get("Result");
    }        
    
    //지급 상세 조회
    @RequestMapping(method= RequestMethod.GET,value = "/merchpayDetailList")
    public String merchpayDetailFormList(@Valid MerchpayFormBean merchpayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {    
        
        Hashtable ht_merchpayDetailList = merchpayService.merchpayDetailList(merchpayFormBean);
        String ls_merchpayDetailList = (String) ht_merchpayDetailList.get("ResultSet"); 
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_merchpayDetailList.get("payChnCateList");
        
        model.addAttribute("ls_merchpayDetailList",ls_merchpayDetailList);     
        model.addAttribute("payChnCateList",payChnCateList); 
        
        return null;
        
    }
    
    //지급 상세 조회
    @RequestMapping(method= RequestMethod.POST,value = "/merchpayDetailList")
    @ResponseBody
    public String merchpayDetailList(@Valid MerchpayFormBean merchpayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {       
        
        Hashtable ht_merchpayDetailList = merchpayService.merchpayDetailList(merchpayFormBean);
        String ls_merchpayDetailList = (String) ht_merchpayDetailList.get("ResultSet");
        
        return ls_merchpayDetailList;
        
    }           
    
    //지급 내역 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/merchpayMasterListExcel")
    public void merchpayMasterListExcel(@Valid MerchpayFormBean merchpayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("merchpay.merchpayMasterListExcel.fileName"));
        
        merchpayService.merchpayMasterListExcel(merchpayFormBean, excelDownloadFilename, response); 
        
    }             
    
    //지급 내역 상세 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/merchpayDetailListExcel")
    public void merchpayDetailListExcel(@Valid MerchpayFormBean merchpayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("merchpay.merchpayDetailListExcel.fileName"));
        
        merchpayService.merchpayDetailListExcel(merchpayFormBean, excelDownloadFilename, response); 
        
    }      
    
    //가맹점 일별 정산
    @RequestMapping(method= RequestMethod.GET,value = "/basic_merchMerchpayMasterList")
    public String basic_merchMerchpayMasterFormList(@Valid MerchpayFormBean merchpayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
           SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        if((merchpayFormBean.getExp_pay_start_dt()== null) || (merchpayFormBean.getExp_pay_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            merchpayFormBean.setExp_pay_start_dt(start_dt);
            merchpayFormBean.setExp_pay_end_dt(end_dt);
        }

        merchpayFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }             
        
        Hashtable ht_merchpayMasterFormList = merchpayService.basic_merchMerchpayMasterList(merchpayFormBean);
        List ls_merchpayMasterList = (List) ht_merchpayMasterFormList.get("ls_merchpayMasterFormList");
        
        model.addAttribute("total_rows_cnt",(String)ht_merchpayMasterFormList.get("total_rows_cnt"));
        model.addAttribute("total_app_amt",(String)ht_merchpayMasterFormList.get("total_app_amt")); 
        model.addAttribute("total_cncl_amt",(String)ht_merchpayMasterFormList.get("total_cncl_amt"));
        model.addAttribute("total_pay_targetamt",(String)ht_merchpayMasterFormList.get("total_pay_targetamt"));
        model.addAttribute("total_commission",(String)ht_merchpayMasterFormList.get("total_commission")); 
        model.addAttribute("total_vat",(String)ht_merchpayMasterFormList.get("total_vat")); 
        model.addAttribute("total_decision_pay_amt",(String)ht_merchpayMasterFormList.get("total_decision_pay_amt"));
        
        SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
        Date start_date = formatter.parse(merchpayFormBean.getExp_pay_start_dt());
        Date end_date   = formatter.parse(merchpayFormBean.getExp_pay_end_dt());
        
        String start_dt = formatter2.format(start_date);
        String end_dt = formatter2.format(end_date);

        merchpayFormBean.setExp_pay_start_dt(start_dt);
        merchpayFormBean.setExp_pay_end_dt(end_dt);
        
        Tb_Code_Main decisionFlagList = (Tb_Code_Main) ht_merchpayMasterFormList.get("decisionFlagList");
        
        model.addAttribute("ls_merchpayMasterList",ls_merchpayMasterList);     
        
        model.addAttribute("decisionFlagList",decisionFlagList);    
        
        return null;
        
    }    
    
    //가맹점 일별 정산
    @RequestMapping(method= RequestMethod.POST,value = "/basic_merchMerchpayMasterList")
    public String basic_merchMerchpayMasterList(@Valid MerchpayFormBean merchpayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

           SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        if((merchpayFormBean.getExp_pay_start_dt()== null) || (merchpayFormBean.getExp_pay_start_dt().equals(""))){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            merchpayFormBean.setExp_pay_start_dt(start_dt);
            merchpayFormBean.setExp_pay_end_dt(end_dt);
        }

        merchpayFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }             
        
        Hashtable ht_merchpayMasterFormList = merchpayService.basic_merchMerchpayMasterList(merchpayFormBean);
        List ls_merchpayMasterList = (List) ht_merchpayMasterFormList.get("ls_merchpayMasterFormList");
        
        model.addAttribute("total_rows_cnt",(String)ht_merchpayMasterFormList.get("total_rows_cnt"));
        model.addAttribute("total_app_amt",(String)ht_merchpayMasterFormList.get("total_app_amt")); 
        model.addAttribute("total_cncl_amt",(String)ht_merchpayMasterFormList.get("total_cncl_amt"));
        model.addAttribute("total_pay_targetamt",(String)ht_merchpayMasterFormList.get("total_pay_targetamt"));
        model.addAttribute("total_commission",(String)ht_merchpayMasterFormList.get("total_commission")); 
        model.addAttribute("total_vat",(String)ht_merchpayMasterFormList.get("total_vat")); 
        model.addAttribute("total_decision_pay_amt",(String)ht_merchpayMasterFormList.get("total_decision_pay_amt"));       
        
        SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
        Date start_date = formatter.parse(merchpayFormBean.getExp_pay_start_dt().replaceAll("-", ""));
        Date end_date   = formatter.parse(merchpayFormBean.getExp_pay_end_dt().replaceAll("-", ""));
        
        String start_dt = formatter2.format(start_date);
        String end_dt = formatter2.format(end_date);

        merchpayFormBean.setExp_pay_start_dt(start_dt);
        merchpayFormBean.setExp_pay_end_dt(end_dt);
        
        Tb_Code_Main decisionFlagList = (Tb_Code_Main) ht_merchpayMasterFormList.get("decisionFlagList");
        
        model.addAttribute("ls_merchpayMasterList",ls_merchpayMasterList);     
        
        model.addAttribute("decisionFlagList",decisionFlagList);    
        
        return null;
        
    }       
    
    //가맹점 일별 정산
    @RequestMapping(method= RequestMethod.GET,value = "/merchMerchpayMasterList")
    public String merchMerchpayMasterFormList(@Valid MerchpayFormBean merchpayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
           SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        if((merchpayFormBean.getExp_pay_start_dt()== null) || (merchpayFormBean.getExp_pay_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            merchpayFormBean.setExp_pay_start_dt(start_dt);
            merchpayFormBean.setExp_pay_end_dt(end_dt);
        }

        merchpayFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }             
        
        Hashtable ht_merchpayMasterFormList = merchpayService.merchMerchpayMasterList(merchpayFormBean);
        String ls_merchpayMasterList = (String) ht_merchpayMasterFormList.get("ResultSet");
        
        SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
        Date start_date = formatter.parse(merchpayFormBean.getExp_pay_start_dt());
        Date end_date   = formatter.parse(merchpayFormBean.getExp_pay_end_dt());
        
        String start_dt = formatter2.format(start_date);
        String end_dt = formatter2.format(end_date);

        merchpayFormBean.setExp_pay_start_dt(start_dt);
        merchpayFormBean.setExp_pay_end_dt(end_dt);
        
        Tb_Code_Main decisionFlagList = (Tb_Code_Main) ht_merchpayMasterFormList.get("decisionFlagList");
        
        model.addAttribute("ls_merchpayMasterList",ls_merchpayMasterList);     
        
        model.addAttribute("decisionFlagList",decisionFlagList);    
        
        return null;
        
    }
    
    //가맹점 일별 정산
    @RequestMapping(method= RequestMethod.POST,value = "/merchMerchpayMasterList")
    @ResponseBody
    public String merchMerchpayMasterList(@Valid MerchpayFormBean merchpayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        merchpayFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }               
        
        Hashtable ht_merchpayMasterFormList = merchpayService.merchMerchpayMasterList(merchpayFormBean);
        String ls_merchpayMasterList = (String) ht_merchpayMasterFormList.get("ResultSet");
        
        return ls_merchpayMasterList;
        
    }   
    
    //가맹점 일별 정산 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/merchMerchpayMasterListExcel")
    public void merchMerchpayMasterListExcel(@Valid MerchpayFormBean merchpayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("merchpay.merchpayMasterListExcel.fileName"));

        merchpayFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                  
        
        merchpayService.merchMerchpayMasterListExcel(merchpayFormBean, excelDownloadFilename, response); 
        
    }         
    
    //가맹점 일별 정산 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/basic_merchMerchpayMasterListExcel")
    public void basic_merchMerchpayMasterListExcel(@Valid MerchpayFormBean merchpayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("merchpay.merchpayMasterListExcel.fileName"));

        merchpayFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                  
        
        merchpayService.basic_merchMerchpayMasterListExcel(merchpayFormBean, excelDownloadFilename, response); 
        
    }        
    
    //가맹점 일별 정산 상세 조회
    @RequestMapping(method= RequestMethod.GET,value = "/merchMerchpayDetailList")
    public String merchMerchpayDetailFormList(@Valid MerchpayFormBean merchpayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {    

        merchpayFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                 
        
        Hashtable ht_merchpayDetailList = merchpayService.merchMerchpayDetailList(merchpayFormBean);
        String ls_merchpayDetailList = (String) ht_merchpayDetailList.get("ResultSet"); 
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_merchpayDetailList.get("payChnCateList");
        
        model.addAttribute("ls_merchpayDetailList",ls_merchpayDetailList);     
        model.addAttribute("payChnCateList",payChnCateList); 
        
        return null;
        
    }
    
    //가맹점 일별 정산 상세 조회
    @RequestMapping(method= RequestMethod.POST,value = "/merchMerchpayDetailList")
    @ResponseBody
    public String merchMerchpayDetailList(@Valid MerchpayFormBean merchpayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {       
        
        merchpayFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                 
        
        Hashtable ht_merchpayDetailList = merchpayService.merchMerchpayDetailList(merchpayFormBean);
        String ls_merchpayDetailList = (String) ht_merchpayDetailList.get("ResultSet");
        
        return ls_merchpayDetailList;
        
    }         
    
    //가맹점 일별 정산 상세 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/merchMerchpayDetailListExcel")
    public void merchMerchpayDetailListExcel(@Valid MerchpayFormBean merchpayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("merchpay.merchpayDetailListExcel.fileName"));

        merchpayFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            merchpayFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                 
        
        merchpayService.merchMerchpayDetailListExcel(merchpayFormBean, excelDownloadFilename, response); 
        
    }            
    
    //가맹점 개별 정산 조회
    @RequestMapping(method= RequestMethod.GET,value = "/merchMerchpayTransList")
    public String merchMerchpayTransFormList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {    

        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                 
        
        Hashtable ht_merchMerchpayTransList = merchpayService.merchMerchpayTransList(transFormBean);
        String ls_merchMerchpayTransList = (String) ht_merchMerchpayTransList.get("ResultSet"); 
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_merchMerchpayTransList.get("payChnCateList");
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_merchMerchpayTransList.get("resultStatusList");
        Tb_Code_Main tranStatusList = (Tb_Code_Main) ht_merchMerchpayTransList.get("tranStatusList");
        Tb_Code_Main issCdList = (Tb_Code_Main) ht_merchMerchpayTransList.get("issCdList");
        
        model.addAttribute("ls_merchMerchpayTransList",ls_merchMerchpayTransList);     
        model.addAttribute("payChnCateList",payChnCateList); 
        model.addAttribute("resultStatusList",resultStatusList);                   
        model.addAttribute("tranStatusList",tranStatusList);             
        model.addAttribute("issCdList",issCdList);              
        
        return null;
        
    }
    
    //가맹점 개별 정산 조회
    @RequestMapping(method= RequestMethod.POST,value = "/merchMerchpayTransList")
    @ResponseBody
    public String merchMerchpayTransList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {       
        
        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                 
        
        Hashtable ht_merchMerchpayTransList = merchpayService.merchMerchpayTransList(transFormBean);
        String ls_merchMerchpayTransList = (String) ht_merchMerchpayTransList.get("ResultSet");
        
        return ls_merchMerchpayTransList;
        
    }         
    
    //가맹점 개별 정산 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/merchMerchpayTransListExcel")
    public void merchMerchpayTransListExcel(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("merchpay.merchpayTransListExcel.fileName"));

        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                 
        
        merchpayService.merchMerchpayTransListExcel(transFormBean, excelDownloadFilename, response); 
        
    }    
    
    
    //가맹점 개별 정산 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/basic_merchMerchpayTransListExcel")
    public void basic_merchMerchpayTransListExcel(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("merchpay.merchpayTransListExcel.fileName"));

        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                 
        
        merchpayService.basic_merchMerchpayTransListExcel(transFormBean, excelDownloadFilename, response); 
        
    }        
    
    //가맹점 개별 정산 조회
    @RequestMapping(method= RequestMethod.GET,value = "/basic_merchMerchpayTransList")
    public String basic_merchMerchpayTransFormList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {    

        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                 
        
        Hashtable ht_merchMerchpayTransList = merchpayService.basic_merchMerchpayTransList(transFormBean);
        

	model.addAttribute("TotAppCnt", (String)ht_merchMerchpayTransList.get("TotAppCnt"));
        model.addAttribute("TotAppAmt", (String)ht_merchMerchpayTransList.get("TotAppAmt"));
        model.addAttribute("TotCnclCnt", (String)ht_merchMerchpayTransList.get("TotCnclCnt"));
        model.addAttribute("TotCnclAmt", (String)ht_merchMerchpayTransList.get("TotCnclAmt"));
        model.addAttribute("TotAmt", (String)ht_merchMerchpayTransList.get("TotAmt"));
        model.addAttribute("TotChktrancnt", (String)ht_merchMerchpayTransList.get("TotChktrancnt"));                
        model.addAttribute("Onofftid_commision", (String)ht_merchMerchpayTransList.get("Onofftid_commision"));
        model.addAttribute("Onofftid_tax_amt", (String)ht_merchMerchpayTransList.get("Onofftid_tax_amt"));
        model.addAttribute("TotOnofftid_pay_amt", (String)ht_merchMerchpayTransList.get("TotOnofftid_pay_amt"));
        
        List ls_merchMerchpayTransList = (List) ht_merchMerchpayTransList.get("ls_transDetailList"); 
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_merchMerchpayTransList.get("payChnCateList");
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_merchMerchpayTransList.get("resultStatusList");
        Tb_Code_Main tranStatusList = (Tb_Code_Main) ht_merchMerchpayTransList.get("tranStatusList");
        Tb_Code_Main issCdList = (Tb_Code_Main) ht_merchMerchpayTransList.get("issCdList");
        
        model.addAttribute("ls_merchMerchpayTransList",ls_merchMerchpayTransList);     
        model.addAttribute("payChnCateList",payChnCateList); 
        model.addAttribute("resultStatusList",resultStatusList);                   
        model.addAttribute("tranStatusList",tranStatusList);             
        model.addAttribute("issCdList",issCdList);              
        
        String strTpTotal_count = (String)ht_merchMerchpayTransList.get("total_count");
        model.addAttribute("total_count",strTpTotal_count);
        return null;
        
    }
    
    //가맹점 개별 정산 조회
    @RequestMapping(method= RequestMethod.POST,value = "/basic_merchMerchpayTransList")
    public String basic_merchMerchpayTransList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {       
        
        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                 
        
        Hashtable ht_merchMerchpayTransList = merchpayService.basic_merchMerchpayTransList(transFormBean);
        List ls_merchMerchpayTransList = (List) ht_merchMerchpayTransList.get("ls_transDetailList"); 
        
        
	model.addAttribute("TotAppCnt", (String)ht_merchMerchpayTransList.get("TotAppCnt"));
        model.addAttribute("TotAppAmt", (String)ht_merchMerchpayTransList.get("TotAppAmt"));
        model.addAttribute("TotCnclCnt", (String)ht_merchMerchpayTransList.get("TotCnclCnt"));
        model.addAttribute("TotCnclAmt", (String)ht_merchMerchpayTransList.get("TotCnclAmt"));
        model.addAttribute("TotAmt", (String)ht_merchMerchpayTransList.get("TotAmt"));
        model.addAttribute("TotChktrancnt", (String)ht_merchMerchpayTransList.get("TotChktrancnt"));                
        model.addAttribute("Onofftid_commision", (String)ht_merchMerchpayTransList.get("Onofftid_commision"));
        model.addAttribute("Onofftid_tax_amt", (String)ht_merchMerchpayTransList.get("Onofftid_tax_amt"));
        model.addAttribute("TotOnofftid_pay_amt", (String)ht_merchMerchpayTransList.get("TotOnofftid_pay_amt"));
        
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_merchMerchpayTransList.get("payChnCateList");
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_merchMerchpayTransList.get("resultStatusList");
        Tb_Code_Main tranStatusList = (Tb_Code_Main) ht_merchMerchpayTransList.get("tranStatusList");
        Tb_Code_Main issCdList = (Tb_Code_Main) ht_merchMerchpayTransList.get("issCdList");
        
        model.addAttribute("ls_merchMerchpayTransList",ls_merchMerchpayTransList);     
        model.addAttribute("payChnCateList",payChnCateList); 
        model.addAttribute("resultStatusList",resultStatusList);                   
        model.addAttribute("tranStatusList",tranStatusList);             
        model.addAttribute("issCdList",issCdList);   
        
        String strTpTotal_count = (String)ht_merchMerchpayTransList.get("total_count");
        model.addAttribute("total_count",strTpTotal_count);
        
        return null;
        
    }             
    
    
    //임시 등록 조회
    @RequestMapping(method= RequestMethod.GET,value = "/merchpayScheTmpList")
    public String merchpayScheTmpList(@Valid MerchpayFormBean merchpayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        Hashtable ht_merchpayMasterFormList = merchpayService.merchpayMasterFormList(merchpayFormBean);
        List<Tb_Merch_Pay> ls_merchpayMasterList = (List<Tb_Merch_Pay>) ht_merchpayMasterFormList.get("ls_merchpayMasterFormList");
        
        Hashtable ht_merchpayScheTmpList = merchpayService.merchpayScheTmpList(merchpayFormBean);
        List<Tb_Merch_Pay> ls_merchpayScheTmpList = (List<Tb_Merch_Pay>) ht_merchpayScheTmpList.get("ResultSet");
        
        model.addAttribute("ls_merchpayMasterList",ls_merchpayMasterList);     
        model.addAttribute("ls_merchpayScheTmpList",ls_merchpayScheTmpList);     
        
        return null;
        
    }    
    
    /*
     *  임시 정보 저장
     */
    @RequestMapping(value="/merchpayScheTmpInsert", method= RequestMethod.POST)
    @ResponseBody
    public String merchpayScheTmpInsert(@Valid MerchpayFormBean merchpayFormBean, Model model , HttpServletRequest request,@ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        merchpayFormBean.setUser_seq(siteSession.getUser_seq());

        merchpayService.merchpayScheTmpInsert(merchpayFormBean); 

        return null;
    }        
    
    //선지급
    @RequestMapping(method= RequestMethod.GET,value = "/merchpayCashInAdvanceList")
    public String merchpayCashInAdvanceList(@Valid MerchpayFormBean merchpayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        return null;
        
    }        
    
    /*
     *  선지급 저장
     */
    @RequestMapping(value="/merchpayCashInAdvanceInsert", method= RequestMethod.POST)
    @ResponseBody
    public String merchpayCashInAdvanceInsert(@Valid MerchpayFormBean merchpayFormBean, Model model , HttpServletRequest request,@ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        merchpayFormBean.setUser_seq(siteSession.getUser_seq());

        merchpayService.merchpayCashInAdvanceInsert(merchpayFormBean); 

        return null;
    }           
    
}
