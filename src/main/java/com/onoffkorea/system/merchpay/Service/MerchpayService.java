/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.merchpay.Service;

import com.onoffkorea.system.merchpay.Bean.MerchpayFormBean;
import com.onoffkorea.system.trans.Bean.TransFormBean;
import java.util.Hashtable;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public interface MerchpayService {

    public Hashtable merchpayMasterFormList(MerchpayFormBean merchpayFormBean);

    public Hashtable merchpayMetabolismConfirmProcess(MerchpayFormBean merchpayFormBean);

    public Hashtable merchpayMetabolismCancelProcess(MerchpayFormBean merchpayFormBean);

    public Hashtable merchpayDetailList(MerchpayFormBean merchpayFormBean);

    public void merchpayMasterListExcel(MerchpayFormBean merchpayFormBean, String excelDownloadFilename, HttpServletResponse response);

    public void merchpayDetailListExcel(MerchpayFormBean merchpayFormBean, String excelDownloadFilename, HttpServletResponse response);

    public Hashtable merchMerchpayMasterList(MerchpayFormBean merchpayFormBean);
    
    public void merchMerchpayMasterListExcel(MerchpayFormBean merchpayFormBean, String excelDownloadFilename, HttpServletResponse response);

    public Hashtable basic_merchMerchpayMasterList(MerchpayFormBean merchpayFormBean);
    
    public void basic_merchMerchpayMasterListExcel(MerchpayFormBean merchpayFormBean, String excelDownloadFilename, HttpServletResponse response);    
        
    public Hashtable merchMerchpayDetailList(MerchpayFormBean merchpayFormBean);

    public void merchMerchpayDetailListExcel(MerchpayFormBean merchpayFormBean, String excelDownloadFilename, HttpServletResponse response);
    
    public Hashtable basic_merchMerchpayTransList(TransFormBean transFormBean);
    
    public Hashtable merchMerchpayTransList(TransFormBean transFormBean);

    public void merchMerchpayTransListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response);
    
    public void basic_merchMerchpayTransListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response);

    public Hashtable merchpayScheTmpList(MerchpayFormBean merchpayFormBean);

    public void merchpayScheTmpInsert(MerchpayFormBean merchpayFormBean);

    public void merchpayCashInAdvanceInsert(MerchpayFormBean merchpayFormBean);

}
