/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.merchpay.Service;

import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.dframt.Bean.DfrAmtFormBean;
import com.onoffkorea.system.dframt.Dao.DfrAmtDAO;
import com.onoffkorea.system.dframt.Vo.Tb_Onffmerch_Dfr_Gen;
import com.onoffkorea.system.merchoutamt.Bean.MerchoutamtFormBean;
import com.onoffkorea.system.merchoutamt.Dao.MerchoutamtDAO;
import com.onoffkorea.system.merchoutamt.Vo.Tb_Merch_Outamt_Main;
import com.onoffkorea.system.merchpay.Bean.MerchpayFormBean;
import com.onoffkorea.system.merchpay.Dao.MerchpayDAO;
import com.onoffkorea.system.merchpay.Vo.Tb_Merch_Pay;
import com.onoffkorea.system.trans.Bean.TransFormBean;
import com.onoffkorea.system.trans.Vo.Tb_Tran_Cardpg;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service("merchpayService")
public class MerchpayServiceImpl implements MerchpayService{
    
    protected Logger logger = Logger.getLogger(getClass().getName());
    
    @Autowired
    private CommonService commonService;    
    
    @Autowired
    private MerchpayDAO merchpayDAO;    
    
    @Autowired
    private DfrAmtDAO dframtDAO;    
    
    @Autowired
    private MerchoutamtDAO merchoutamtDAO;       

    //지급 조회
    @Override
    public Hashtable merchpayMasterFormList(MerchpayFormBean merchpayFormBean) {

        Hashtable ht = new Hashtable();
        String accNo = null;

        try {
            
                List<Tb_Merch_Pay> ls_merchpayMasterFormList = merchpayDAO.merchpayMasterFormList(merchpayFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"rows\" : [");
                for (int i = 0; i < ls_merchpayMasterFormList.size(); i++) {
                    
                        Tb_Merch_Pay tb_Merch_Pay = ls_merchpayMasterFormList.get(i);
                        
                         if(!"".equals(Util.nullToString(tb_Merch_Pay.getAcc_no()))){
                            accNo = tb_Merch_Pay.getAcc_no().substring(0, 4)+"******"+tb_Merch_Pay.getAcc_no().substring(tb_Merch_Pay.getAcc_no().length()-4, tb_Merch_Pay.getAcc_no().length());
                        }else {
                            accNo="";
                        }
                         
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"exp_pay_dt\":\""+Util.nullToString(tb_Merch_Pay.getExp_pay_dt())+"\"");
                        sb.append(",\"onffmerch_no\":\""+Util.nullToString(tb_Merch_Pay.getOnffmerch_no())+"\"");
                        sb.append(",\"decision_flag\":\""+Util.nullToString(tb_Merch_Pay.getDecision_flag())+"\"");
                        sb.append(",\"pay_sche_seq\":\""+Util.nullToString(tb_Merch_Pay.getPay_sche_seq())+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Merch_Pay.getExp_pay_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getOnffmerch_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getDecision_pay_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getPay_amt()) + "\"");
                        sb.append(" ,\"" + Util.nullToString(tb_Merch_Pay.getDfr_bal_amt()) + "\"");
                        
                        switch(Util.nullToString(tb_Merch_Pay.getDecision_flag())){
                            case "N":
                                //sb.append(" ,\"<span class=outAmt>" + Util.nullToString(Util.addComma(tb_Merch_Pay.getDec_dfr_amt())) + "</span>\"");
                                sb.append(" ,\"<span class=outAmt  row_id="+i+">" + Util.addComma(Util.nullToInt(tb_Merch_Pay.getDec_dfr_amt())) + "</span>\"");
                                break;
                            case "Y":
                                //sb.append(" ,\"" + Util.nullToString(Util.addComma(tb_Merch_Pay.getDec_dfr_amt())) + "\"");
                                sb.append(" ,\"" +  Util.addComma(Util.nullToInt(tb_Merch_Pay.getDec_dfr_amt())) + "\"");
                                break;
                        }
                        
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getDec_bal_amt()) + "\"");
                        
                        switch(Util.nullToString(tb_Merch_Pay.getDecision_flag())){
                            case "N":
                                //sb.append(" ,\"<span class=outAmt>" + Util.nullToString(Util.addComma(tb_Merch_Pay.getRel_dfr_amt())) + "</span>\"");
                                sb.append(" ,\"<span class=outAmt row_id="+i+">" + Util.addComma(Util.nullToInt(tb_Merch_Pay.getRel_dfr_amt())) + "</span>\"");
                                break;
                            case "Y":
                                //sb.append(" ,\"" + Util.nullToString(Util.addComma(tb_Merch_Pay.getRel_dfr_amt())) + "\"");
                                sb.append(" ,\"" + Util.addComma(Util.nullToInt(tb_Merch_Pay.getRel_dfr_amt())) + "\"");
                                break;
                        }                        
                        
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getCur_outamt()) + "\"");
                        
                        switch(Util.nullToString(tb_Merch_Pay.getDecision_flag())){
                            case "N":
                                //sb.append(" ,\"<span class=outAmt>" + Util.nullToString(Util.addComma(tb_Merch_Pay.getOut_amt())) + "</span>\"");
                                sb.append(" ,\"<span class=outAmt row_id="+i+">" + Util.addComma(Util.nullToInt(tb_Merch_Pay.getOut_amt())) + "</span>\"");
                                break;
                            case "Y":
                                //sb.append(" ,\"" + Util.nullToString(Util.addComma(tb_Merch_Pay.getOut_amt())) + "\"");
                                sb.append(" ,\"" + Util.addComma(Util.nullToInt(tb_Merch_Pay.getOut_amt())) + "\"");
                                break;
                        }                                  
                        
                        switch(Util.nullToString(tb_Merch_Pay.getDecision_flag())){
                            case "N":
                                //sb.append(" ,\"<span class=outAmt>" + Util.nullToString(Util.addComma(tb_Merch_Pay.getSetoff_amt())) + "</span>\"");
                                sb.append(" ,\"<span class=outAmt row_id="+i+">" + Util.addComma(Util.nullToInt(tb_Merch_Pay.getSetoff_amt())) + "</span>\"");
                                break;
                            case "Y":
                                //sb.append(" ,\"" + Util.nullToString(Util.addComma(tb_Merch_Pay.getSetoff_amt())) + "\"");
                                sb.append(" ,\"" + Util.addComma(Util.nullToInt(tb_Merch_Pay.getSetoff_amt())) + "\"");
                                break;
                        }                        
                        
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getPay_targetamt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getCommission()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getVat()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getWithhold_tax()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getApp_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getCncl_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getBank_cd_nm()) + "\"");
                        sb.append(",\"" + accNo + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getAcc_nm()) + "\"");

                        switch(Util.nullToString(tb_Merch_Pay.getDecision_flag())){
                            case "N" :
                                sb.append(" ,\"<button class=procButton row_id="+i+">지급확정</button>\"");
                                break;
                            case "Y" :
                                sb.append(" ,\"<button class=procCancelButton row_id="+i+">확정취소</button>\"");
                                break;
                        }

                        if (i == (ls_merchpayMasterFormList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
                commonCodeSearchFormBean.setMain_code("DECISION_FLAG");
                Tb_Code_Main decisionFlagList = commonService.codeSearch(commonCodeSearchFormBean);
                
                ht.put("ls_merchpayMasterFormList", ls_merchpayMasterFormList);
                ht.put("decisionFlagList", decisionFlagList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;             
        
    }

    //지급확정
    @Override
    @Transactional
    public Hashtable merchpayMetabolismConfirmProcess(MerchpayFormBean merchpayFormBean) {
        
        Hashtable ht = new Hashtable();

        List<Tb_Merch_Pay> ls_merchpayMasterFormList = merchpayDAO.merchpayMasterFormList(merchpayFormBean);
        Tb_Merch_Pay tb_Merch_Pay = ls_merchpayMasterFormList.get(0);
        tb_Merch_Pay.setUser_seq(merchpayFormBean.getUser_seq());

        //지급확정 처리
        String updateResult = merchpayDAO.merchpayMetabolismConfirmProcess(tb_Merch_Pay);
        
        //지급 상세건 확정 처리
        merchpayDAO.merchpayDecisionFlagYUpdate(merchpayFormBean);
        
        //보류확정액 생성
        if(Integer.parseInt(tb_Merch_Pay.getDec_dfr_amt()) > 0){
            int dec_dfr_amt_org = Integer.parseInt(tb_Merch_Pay.getDec_dfr_amt());
            int dec_dfr_amt = Integer.parseInt(tb_Merch_Pay.getDec_dfr_amt());
            //보류잔액 리스트
            List<Tb_Onffmerch_Dfr_Gen> ls_dfrBalAmtList = merchpayDAO.dfrBalAmtList(merchpayFormBean);
            for(int i=0;i<ls_dfrBalAmtList.size();i++){
                if(dec_dfr_amt > 0){
                    Tb_Onffmerch_Dfr_Gen tb_Onffmerch_Dfr_Gen = ls_dfrBalAmtList.get(i);
                    int dfr_bal_amt = Integer.parseInt(tb_Onffmerch_Dfr_Gen.getDfr_bal_amt());
                    //보류확정액이 보류잔액보다 클 경우
                    if(dec_dfr_amt > dfr_bal_amt){
                        DfrAmtFormBean dfrAmtFormBean = new DfrAmtFormBean();
                        dfrAmtFormBean.setOnffmerch_no(tb_Onffmerch_Dfr_Gen.getOnffmerch_no());
                        dfrAmtFormBean.setDfr_seq(tb_Onffmerch_Dfr_Gen.getDfr_seq());
                        dfrAmtFormBean.setDfr_dec_amt(tb_Onffmerch_Dfr_Gen.getDfr_bal_amt());
                        dfrAmtFormBean.setDfr_dec_dt(merchpayFormBean.getExp_pay_dt());
                        dfrAmtFormBean.setDfr_dec_reson("001");
                        dfrAmtFormBean.setPay_sche_seq(tb_Merch_Pay.getPay_sche_seq());
                        dfrAmtFormBean.setExp_pay_dt(merchpayFormBean.getExp_pay_dt());
                        dfrAmtFormBean.setMemo("");
                        dfrAmtFormBean.setUser_seq(merchpayFormBean.getUser_seq());
                        //보류확정 등록
                        dframtDAO.dframtDetailInsert(dfrAmtFormBean);
                        //보류 수정 (보류확정액 증감)
                        dframtDAO.dframtDetailUpdatePlus(dfrAmtFormBean);
                        dec_dfr_amt = dec_dfr_amt - Integer.parseInt(tb_Onffmerch_Dfr_Gen.getDfr_bal_amt());
                    }
                    //보류확정액이 보류잔액보다 작을 경우
                    else{
                        DfrAmtFormBean dfrAmtFormBean = new DfrAmtFormBean();
                        dfrAmtFormBean.setOnffmerch_no(tb_Onffmerch_Dfr_Gen.getOnffmerch_no());
                        dfrAmtFormBean.setDfr_seq(tb_Onffmerch_Dfr_Gen.getDfr_seq());
                        dfrAmtFormBean.setDfr_dec_amt("" + dec_dfr_amt);
                        dfrAmtFormBean.setDfr_dec_dt(merchpayFormBean.getExp_pay_dt());
                        dfrAmtFormBean.setDfr_dec_reson("001");
                        dfrAmtFormBean.setPay_sche_seq(tb_Merch_Pay.getPay_sche_seq());
                        dfrAmtFormBean.setExp_pay_dt(merchpayFormBean.getExp_pay_dt());
                        dfrAmtFormBean.setMemo("");
                        dfrAmtFormBean.setUser_seq(merchpayFormBean.getUser_seq());
                        //보류확정 등록
                        dframtDAO.dframtDetailInsert(dfrAmtFormBean);
                        //보류 수정 (보류확정액 증감)
                        dframtDAO.dframtDetailUpdatePlus(dfrAmtFormBean);                        
                        dec_dfr_amt = 0;
                    }                
                }
            }
            //누적 보류 수정(보류확정액 증감, 보류잔액 차감)
            DfrAmtFormBean dfrAmtFormBean = new DfrAmtFormBean();
            dfrAmtFormBean.setOnffmerch_no(merchpayFormBean.getOnffmerch_no());
            dfrAmtFormBean.setUser_seq(merchpayFormBean.getUser_seq());
            dfrAmtFormBean.setDfr_dec_amt("" + dec_dfr_amt_org);
            dframtDAO.dframtDetailMainUpdatePlus(dfrAmtFormBean);                       
        }
        
        //보류해제 등록
        if(Integer.parseInt(tb_Merch_Pay.getRel_dfr_amt()) > 0){
            int rel_dfr_amt_org = Integer.parseInt(tb_Merch_Pay.getRel_dfr_amt());
            int rel_dfr_amt = Integer.parseInt(tb_Merch_Pay.getRel_dfr_amt());
            //보류확정잔액 리스트
            List<Tb_Onffmerch_Dfr_Gen> ls_decBalAmtList = merchpayDAO.decBalAmtList(merchpayFormBean);
            for(int i=0;i<ls_decBalAmtList.size();i++){
                if(rel_dfr_amt > 0){
                    Tb_Onffmerch_Dfr_Gen tb_Onffmerch_Dfr_Gen = ls_decBalAmtList.get(i);
                    int dec_bal_amt = Integer.parseInt(tb_Onffmerch_Dfr_Gen.getDec_bal_amt());
                    //보류해제액이 보류확정잔액보다 클 경우
                    if(rel_dfr_amt > dec_bal_amt){
                        DfrAmtFormBean dfrAmtFormBean = new DfrAmtFormBean();
                        dfrAmtFormBean.setOnffmerch_no(tb_Onffmerch_Dfr_Gen.getOnffmerch_no());
                        dfrAmtFormBean.setDfr_seq(tb_Onffmerch_Dfr_Gen.getDfr_seq());
                        dfrAmtFormBean.setDfr_rel_amt(tb_Onffmerch_Dfr_Gen.getDec_bal_amt());
                        dfrAmtFormBean.setDfr_rel_dt(merchpayFormBean.getExp_pay_dt());
                        dfrAmtFormBean.setDfr_rel_reson("001");
                        dfrAmtFormBean.setPay_sche_seq(tb_Merch_Pay.getPay_sche_seq());
                        dfrAmtFormBean.setExp_pay_dt(merchpayFormBean.getExp_pay_dt());
                        dfrAmtFormBean.setMemo("");
                        dfrAmtFormBean.setUser_seq(merchpayFormBean.getUser_seq());
                        //보류해제 등록
                        dframtDAO.dframtRelInsert(dfrAmtFormBean);
                        //보류 수정 (보류해제액 증감)
                        dframtDAO.dframtRelUpdatePlus(dfrAmtFormBean);
                        rel_dfr_amt = rel_dfr_amt - Integer.parseInt(tb_Onffmerch_Dfr_Gen.getDec_bal_amt());
                    }
                    //보류해제액이 보류확정잔액보다 작을 경우
                    else{
                        DfrAmtFormBean dfrAmtFormBean = new DfrAmtFormBean();
                        dfrAmtFormBean.setOnffmerch_no(tb_Onffmerch_Dfr_Gen.getOnffmerch_no());
                        dfrAmtFormBean.setDfr_seq(tb_Onffmerch_Dfr_Gen.getDfr_seq());
                        dfrAmtFormBean.setDfr_rel_amt("" + rel_dfr_amt);
                        dfrAmtFormBean.setDfr_rel_dt(merchpayFormBean.getExp_pay_dt());
                        dfrAmtFormBean.setDfr_rel_reson("001");
                        dfrAmtFormBean.setPay_sche_seq(tb_Merch_Pay.getPay_sche_seq());
                        dfrAmtFormBean.setExp_pay_dt(merchpayFormBean.getExp_pay_dt());
                        dfrAmtFormBean.setMemo("");
                        dfrAmtFormBean.setUser_seq(merchpayFormBean.getUser_seq());
                        //보류해제 등록
                        dframtDAO.dframtRelInsert(dfrAmtFormBean);
                        //보류 수정 (보류해제액 증감)
                        dframtDAO.dframtRelUpdatePlus(dfrAmtFormBean);                  
                        rel_dfr_amt = 0;
                    }                
                }
            }
            //누적 보류 수정(보류해제액 증감, 보류확정잔액 차감)
            DfrAmtFormBean dfrAmtFormBean = new DfrAmtFormBean();
            dfrAmtFormBean.setOnffmerch_no(merchpayFormBean.getOnffmerch_no());
            dfrAmtFormBean.setUser_seq(merchpayFormBean.getUser_seq());
            dfrAmtFormBean.setDfr_rel_amt("" + rel_dfr_amt_org);
            dframtDAO.dframtRelInsertMainPlus(dfrAmtFormBean);                       
        }       
        
        //미수상계액 등록
        if(Integer.parseInt(tb_Merch_Pay.getSetoff_amt()) > 0){
            int setoff_amt = Integer.parseInt(tb_Merch_Pay.getSetoff_amt());
            //미수잔액 조회
            List<Tb_Merch_Outamt_Main> ls_merchCuroutamtList = merchpayDAO.merchCuroutamtList(merchpayFormBean);
            for(int i=0;i<ls_merchCuroutamtList.size();i++){
                if(setoff_amt > 0){
                    Tb_Merch_Outamt_Main tb_Merch_Outamt_Main = ls_merchCuroutamtList.get(i);
                    int cur_outamt = Integer.parseInt(tb_Merch_Outamt_Main.getCur_outamt());
                    //미수상계액이 미수잔액보다 클 경우
                    if(setoff_amt > cur_outamt){
                        MerchoutamtFormBean merchoutamtFormBean = new MerchoutamtFormBean();
                        merchoutamtFormBean.setOutamt_seq(tb_Merch_Outamt_Main.getOutamt_seq());
                        merchoutamtFormBean.setSetoff_cate("002");
                        merchoutamtFormBean.setSetoff_amt(tb_Merch_Outamt_Main.getCur_outamt());
                        merchoutamtFormBean.setSetoff_date(merchpayFormBean.getExp_pay_dt());
                        merchoutamtFormBean.setMemo("");
                        merchoutamtFormBean.setUser_seq(merchpayFormBean.getUser_seq());
                        merchoutamtFormBean.setPay_sche_seq(tb_Merch_Pay.getPay_sche_seq());
                        //미수상계 등록
                        merchoutamtDAO.merchoutamtDetailInsert(merchoutamtFormBean);
                        //미수잔액 수정
                        merchoutamtDAO.merchoutamtMasterUpdate_minusoutamt(merchoutamtFormBean);
                        setoff_amt = setoff_amt - Integer.parseInt(tb_Merch_Outamt_Main.getCur_outamt());
                    }
                    //미수상계액이 미수잔액보다 작을 경우
                    else{
                        MerchoutamtFormBean merchoutamtFormBean = new MerchoutamtFormBean();
                        merchoutamtFormBean.setOutamt_seq(tb_Merch_Outamt_Main.getOutamt_seq());
                        merchoutamtFormBean.setSetoff_cate("002");
                        merchoutamtFormBean.setSetoff_amt("" + setoff_amt);
                        merchoutamtFormBean.setSetoff_date(merchpayFormBean.getExp_pay_dt());
                        merchoutamtFormBean.setMemo("");
                        merchoutamtFormBean.setUser_seq(merchpayFormBean.getUser_seq());
                        merchoutamtFormBean.setPay_sche_seq(tb_Merch_Pay.getPay_sche_seq());
                        //미수상계 등록
                        merchoutamtDAO.merchoutamtDetailInsert(merchoutamtFormBean);
                        //미수잔액 수정
                        merchoutamtDAO.merchoutamtMasterUpdate_minusoutamt(merchoutamtFormBean);
                        setoff_amt = 0;
                    }                
                }
            }             
        }
        
        //미수금생성 등록
        if(Integer.parseInt(tb_Merch_Pay.getOut_amt()) > 0){
            MerchoutamtFormBean merchoutamtFormBean = new MerchoutamtFormBean();
            merchoutamtFormBean.setGen_reson("006");
            merchoutamtFormBean.setGen_dt(merchpayFormBean.getExp_pay_dt());
            merchoutamtFormBean.setOut_amt(tb_Merch_Pay.getOut_amt());
            merchoutamtFormBean.setCur_outamt(tb_Merch_Pay.getOut_amt());
            merchoutamtFormBean.setMemo("");
            merchoutamtFormBean.setUser_seq(merchpayFormBean.getUser_seq());
            merchoutamtFormBean.setOnffmerch_no(merchpayFormBean.getOnffmerch_no());
            merchoutamtFormBean.setExp_pay_dt(merchpayFormBean.getExp_pay_dt());
            merchoutamtFormBean.setPay_sche_seq(tb_Merch_Pay.getPay_sche_seq());
            merchoutamtDAO.merchoutamtMasterInsert(merchoutamtFormBean);
            
            //지급예정액이 0 보다 클 경우
            if(Integer.parseInt(tb_Merch_Pay.getPay_amt()) >= 0){
                merchoutamtFormBean.setOutamt_seq(merchoutamtFormBean.getOutamt_seq());
                merchoutamtFormBean.setSetoff_cate("002");
                merchoutamtFormBean.setSetoff_amt(tb_Merch_Pay.getOut_amt());
                merchoutamtFormBean.setSetoff_date(merchpayFormBean.getExp_pay_dt());
                merchoutamtFormBean.setMemo("");
                merchoutamtFormBean.setUser_seq(merchpayFormBean.getUser_seq());
                merchoutamtFormBean.setPay_sche_seq(tb_Merch_Pay.getPay_sche_seq());
                //미수상계 등록
                merchoutamtDAO.merchoutamtDetailInsert(merchoutamtFormBean);
                //미수잔액 수정
                merchoutamtDAO.merchoutamtMasterUpdate_minusoutamt(merchoutamtFormBean);                    
            }
        }        

        if("".equals(Util.nullToString(updateResult))){
            ht.put("Result","fail");
        }else{
            ht.put("Result","success");
        }

        return ht;          
        
    }

    //지급확정취소
    @Override
    @Transactional
    public Hashtable merchpayMetabolismCancelProcess(MerchpayFormBean merchpayFormBean) {

        Hashtable ht = new Hashtable();
        
        //지급확정 정보 조회
        List<Tb_Merch_Pay> ls_merchpayMasterFormList = merchpayDAO.merchpayMasterFormList(merchpayFormBean);
        Tb_Merch_Pay tb_Merch_Pay = ls_merchpayMasterFormList.get(0);
        tb_Merch_Pay.setUser_seq(merchpayFormBean.getUser_seq());        
        
        //미수금 삭제
        if(Integer.parseInt(tb_Merch_Pay.getOut_amt()) > 0){
            //지급예정액이 0 보다 크거나 같을 경우
            if(Integer.parseInt(tb_Merch_Pay.getPay_amt()) >= 0){
                    
                //미수상계 조회
                List<Tb_Merch_Outamt_Main> ls_setoffamtList = merchpayDAO.setoffamtList(merchpayFormBean);
                for(int i=0;i<ls_setoffamtList.size();i++){
                    Tb_Merch_Outamt_Main tb_Merch_Outamt_Main = ls_setoffamtList.get(i);
                    MerchoutamtFormBean merchoutamtFormBean = new MerchoutamtFormBean();
                    merchoutamtFormBean.setOutamt_seq(tb_Merch_Outamt_Main.getOutamt_seq());
                    merchoutamtFormBean.setSetoff_seq(tb_Merch_Outamt_Main.getSetoff_seq());
                    merchoutamtFormBean.setSetoff_amt(tb_Merch_Outamt_Main.getSetoff_amt());
                    merchoutamtFormBean.setUser_seq(merchpayFormBean.getUser_seq());
                    //미수상계 삭제
                    merchoutamtDAO.merchoutamtSetoffDelete(merchoutamtFormBean);
                    //미수잔액 수정
                    merchoutamtDAO.merchoutamtMasterUpdate_plusoutamt(merchoutamtFormBean);
                }                
                
                //미수금 조회
                List<Tb_Merch_Outamt_Main> ls_merchoutamtList = merchpayDAO.merchoutamtList(merchpayFormBean);
                for(int i=0;i<ls_merchoutamtList.size();i++){
                    Tb_Merch_Outamt_Main tb_Merch_Outamt_Main = ls_merchoutamtList.get(i);
                    int out_amt = Integer.parseInt(Util.nullToInt(tb_Merch_Outamt_Main.getOut_amt()));
                    int cur_outamt = Integer.parseInt(Util.nullToInt(tb_Merch_Outamt_Main.getCur_outamt()));
                    if(out_amt != cur_outamt){
                        new Exception("미수상계내역 존재");
                    }else{
                        MerchoutamtFormBean merchoutamtFormBean = new MerchoutamtFormBean();
                        merchoutamtFormBean.setOutamt_seq(tb_Merch_Outamt_Main.getOutamt_seq());
                        //미수금 삭제
                        merchoutamtDAO.merchoutamtMasterDelete(merchoutamtFormBean);                
                    }
                }                 
            }
            //지급예정액이 0 보다 작을 경우
            else{        
                
                //미수금 조회
                List<Tb_Merch_Outamt_Main> ls_merchoutamtList = merchpayDAO.merchoutamtList(merchpayFormBean);
                for(int i=0;i<ls_merchoutamtList.size();i++){
                    Tb_Merch_Outamt_Main tb_Merch_Outamt_Main = ls_merchoutamtList.get(i);
                    int out_amt = Integer.parseInt(Util.nullToInt(tb_Merch_Outamt_Main.getOut_amt()));
                    int cur_outamt = Integer.parseInt(Util.nullToInt(tb_Merch_Outamt_Main.getCur_outamt()));
                    if(out_amt != cur_outamt){
                        new Exception("미수상계내역 존재");
                    }else{
                        MerchoutamtFormBean merchoutamtFormBean = new MerchoutamtFormBean();
                        merchoutamtFormBean.setOutamt_seq(tb_Merch_Outamt_Main.getOutamt_seq());
                        //미수금 삭제
                        merchoutamtDAO.merchoutamtMasterDelete(merchoutamtFormBean);                
                    }
                }                 
            }
        }

        //미수상계 삭제
        if(Integer.parseInt(tb_Merch_Pay.getSetoff_amt()) > 0){
            //미수상계 조회
            List<Tb_Merch_Outamt_Main> ls_setoffamtList = merchpayDAO.setoffamtList(merchpayFormBean);
            for(int i=0;i<ls_setoffamtList.size();i++){
                Tb_Merch_Outamt_Main tb_Merch_Outamt_Main = ls_setoffamtList.get(i);
                MerchoutamtFormBean merchoutamtFormBean = new MerchoutamtFormBean();
                merchoutamtFormBean.setOutamt_seq(tb_Merch_Outamt_Main.getOutamt_seq());
                merchoutamtFormBean.setSetoff_seq(tb_Merch_Outamt_Main.getSetoff_seq());
                merchoutamtFormBean.setSetoff_amt(tb_Merch_Outamt_Main.getSetoff_amt());
                merchoutamtFormBean.setUser_seq(merchpayFormBean.getUser_seq());
                //미수상계 삭제
                merchoutamtDAO.merchoutamtSetoffDelete(merchoutamtFormBean);
                //미수잔액 수정
                merchoutamtDAO.merchoutamtMasterUpdate_plusoutamt(merchoutamtFormBean);
            }                    
        }
        
        //보류해제 삭제
        if(Integer.parseInt(tb_Merch_Pay.getRel_dfr_amt()) > 0){
            List<Tb_Onffmerch_Dfr_Gen> ls_relDfrAmtList = merchpayDAO.relDfrAmtList(merchpayFormBean);
            for(int i=0;i<ls_relDfrAmtList.size();i++){
                Tb_Onffmerch_Dfr_Gen tb_Onffmerch_Dfr_Gen = ls_relDfrAmtList.get(i);
                DfrAmtFormBean dfrAmtFormBean = new DfrAmtFormBean();
                dfrAmtFormBean.setDfr_rel_seq(tb_Onffmerch_Dfr_Gen.getDfr_rel_seq());
                dfrAmtFormBean.setDfr_seq(tb_Onffmerch_Dfr_Gen.getDfr_seq());
                dfrAmtFormBean.setDfr_rel_amt(tb_Onffmerch_Dfr_Gen.getDfr_rel_amt());
                dfrAmtFormBean.setOnffmerch_no(merchpayFormBean.getOnffmerch_no());
                dfrAmtFormBean.setUser_seq(merchpayFormBean.getUser_seq());
                //누적 보류 수정 보류해제금 차감 보류확정잔액 증감
                dframtDAO.dframtRelMainUpdateMinus(dfrAmtFormBean); 
                //보류 상세 수정
                dframtDAO.dframtRelUpdateMinus(dfrAmtFormBean);          
                //보류해제 삭제
                dframtDAO.dframtRelDelete(dfrAmtFormBean);                
            }
        }        
        
        //보류확정 삭제
        if(Integer.parseInt(tb_Merch_Pay.getDec_dfr_amt()) > 0){
            List<Tb_Onffmerch_Dfr_Gen> ls_decDfrAmtList = merchpayDAO.decDfrAmtList(merchpayFormBean);
            for(int i=0;i<ls_decDfrAmtList.size();i++){
                Tb_Onffmerch_Dfr_Gen tb_Onffmerch_Dfr_Gen = ls_decDfrAmtList.get(i);
                DfrAmtFormBean dfrAmtFormBean = new DfrAmtFormBean();
                dfrAmtFormBean.setDec_dfr_seq(tb_Onffmerch_Dfr_Gen.getDec_dfr_seq());
                dfrAmtFormBean.setDfr_seq(tb_Onffmerch_Dfr_Gen.getDfr_seq());
                dfrAmtFormBean.setDfr_dec_amt(tb_Onffmerch_Dfr_Gen.getDfr_dec_amt());
                dfrAmtFormBean.setOnffmerch_no(merchpayFormBean.getOnffmerch_no());
                dfrAmtFormBean.setUser_seq(merchpayFormBean.getUser_seq());
                //누적 보류 수정 확정보류금 차감 보류확정잔액 차감 보류요청잔액 증감
                dframtDAO.dframtDetailMainUpdateMinus(dfrAmtFormBean); 
                //보류 상세 수정
                dframtDAO.dframtDetailUpdateMinus(dfrAmtFormBean);          
                //보류확정 삭제
                dframtDAO.dframtDetailDelete(dfrAmtFormBean);            
            }
        }                
        
        //지급 상세건 확정취소 처리
        merchpayDAO.merchpayDecisionFlagNUpdate(merchpayFormBean);        
        
        //지급확정취소 처리
        Integer updateResult = merchpayDAO.merchpayMetabolismCancelProcess(merchpayFormBean);        
        
        if(updateResult == 0){
            ht.put("Result","fail");
        }else{
            ht.put("Result","success");
        }

        return ht;              
        
    }
    
    //지급 상세 조회
    @Override
    public Hashtable merchpayDetailList(MerchpayFormBean merchpayFormBean) {

        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = merchpayDAO.merchpayDetailListCount(merchpayFormBean);
                List<Tb_Merch_Pay> ls_merchpayDetailList = merchpayDAO.merchpayDetailList(merchpayFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(merchpayFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_merchpayDetailList.size(); i++) {
                        Tb_Merch_Pay tb_Merch_Pay = ls_merchpayDetailList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Merch_Pay.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getExp_pay_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getOnffmerch_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getOnfftid()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getOnfftid_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getPay_chn_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getPay_amt()) + "\"");                                        
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getCommission()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getVat()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getWithhold_tax()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getApp_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getCncl_amt()) + "\"");

                        if (i == (ls_merchpayDetailList.size() - 1)) { 
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);
                
                ht.put("payChnCateList", payChnCateList);        
                
                

        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;             
        
    }    

    //지급 엑셀다운로드
    @Override
    public void merchpayMasterListExcel(MerchpayFormBean merchpayFormBean, String excelDownloadFilename, HttpServletResponse response) {
        
        try {
             
                List ls_merchpayMasterListExcel = merchpayDAO.merchpayMasterListExcel(merchpayFormBean);
                
                StringBuffer merchpayMasterListExcel = Util.makeData(ls_merchpayMasterListExcel);
        
                Util.exceldownload(merchpayMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }
        
    }

    //지급 내역 상세 엑셀다운로드
    @Override
    public void merchpayDetailListExcel(MerchpayFormBean merchpayFormBean, String excelDownloadFilename, HttpServletResponse response) {
        
        try {
             
                List ls_merchpayDetailListExcel = merchpayDAO.merchpayDetailListExcel(merchpayFormBean);
                
                StringBuffer merchpayDetailListExcel = Util.makeData(ls_merchpayDetailListExcel);
        
                Util.exceldownload(merchpayDetailListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    }

    //가맹점 일별 정산
    @Override
    public Hashtable merchMerchpayMasterList(MerchpayFormBean merchpayFormBean) {

        Hashtable ht = new Hashtable();

        try {
            
                List<Tb_Merch_Pay> ls_merchpayMasterFormList = merchpayDAO.merchMerchpayMasterList(merchpayFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"rows\" : [");
                for (int i = 0; i < ls_merchpayMasterFormList.size(); i++) {
                        Tb_Merch_Pay tb_Merch_Pay = ls_merchpayMasterFormList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"exp_pay_dt\":\""+Util.nullToString(tb_Merch_Pay.getExp_pay_dt())+"\"");
                        sb.append(",\"onffmerch_no\":\""+Util.nullToString(tb_Merch_Pay.getOnffmerch_no())+"\"");
                        sb.append(",\"decision_flag\":\""+Util.nullToString(tb_Merch_Pay.getDecision_flag())+"\"");
                        sb.append(",\"pay_sche_seq\":\""+Util.nullToString(tb_Merch_Pay.getPay_sche_seq())+"\"");                        
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Merch_Pay.getExp_pay_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getOnffmerch_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getMerch_nm()) + "\"");                        
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getDecision_pay_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getPay_amt()) + "\"");
                        sb.append(" ,\"" + Util.nullToString((tb_Merch_Pay.getDec_dfr_amt())) + "\"");             
                        sb.append(" ,\"" + Util.nullToString((tb_Merch_Pay.getRel_dfr_amt())) + "\"");                        
                        sb.append(" ,\"" + Util.nullToString((tb_Merch_Pay.getOut_amt())) + "\"");
                        sb.append(" ,\"" + Util.nullToString((tb_Merch_Pay.getSetoff_amt())) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getPay_targetamt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getCommission()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getVat()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getApp_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getCncl_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getBank_cd_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getAcc_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getAcc_nm()) + "\"");

                        if (i == (ls_merchpayMasterFormList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
                commonCodeSearchFormBean.setMain_code("DECISION_FLAG");
                Tb_Code_Main decisionFlagList = commonService.codeSearch(commonCodeSearchFormBean);
                
                ht.put("ls_merchpayMasterFormList", ls_merchpayMasterFormList);
                ht.put("decisionFlagList", decisionFlagList);                
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;             
        
    }

    //가맹점 일별 정산 엑셀다운로드
    @Override
    public void merchMerchpayMasterListExcel(MerchpayFormBean merchpayFormBean, String excelDownloadFilename, HttpServletResponse response) {
        
        try {
             
                List ls_merchpayMasterListExcel = merchpayDAO.merchMerchpayMasterListExcel(merchpayFormBean);
                
                StringBuffer merchpayMasterListExcel = Util.makeData(ls_merchpayMasterListExcel);
        
                Util.exceldownload(merchpayMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    }
    
    //가맹점 일별 정산
    @Override
    public Hashtable basic_merchMerchpayMasterList(MerchpayFormBean merchpayFormBean) {

        Hashtable ht = new Hashtable();

        try {
            
                List<Tb_Merch_Pay> ls_merchpayMasterSum = merchpayDAO.basic_merchpayMasterSum(merchpayFormBean);
                
                Tb_Merch_Pay tb_Tot_SumInfo = ls_merchpayMasterSum.get(0);
                
                String strTprows_cnt = Util.nullToInt(tb_Tot_SumInfo.getRows_cnt());
                String strTpapp_amt  = Util.nullToInt(tb_Tot_SumInfo.getApp_amt());
                String strTpcncl_amt = Util.nullToInt(tb_Tot_SumInfo.getCncl_amt());
                String strTppay_targetamt = Util.nullToInt(tb_Tot_SumInfo.getPay_targetamt());
                String strTpcommission  = Util.nullToInt(tb_Tot_SumInfo.getCommission());
                String strTpvat  = Util.nullToInt(tb_Tot_SumInfo.getVat());
                String strTpdecision_pay_amt = Util.nullToInt(tb_Tot_SumInfo.getDecision_pay_amt());
                
                ht.put("total_rows_cnt",strTprows_cnt);
                ht.put("total_app_amt",strTpapp_amt); 
                ht.put("total_cncl_amt",strTpcncl_amt);
                ht.put("total_pay_targetamt",strTppay_targetamt);
                ht.put("total_commission",strTpcommission); 
                ht.put("total_vat",strTpvat); 
                ht.put("total_decision_pay_amt",strTpdecision_pay_amt);
            
                List<Tb_Merch_Pay> ls_merchpayMasterFormList = merchpayDAO.merchMerchpayMasterList(merchpayFormBean);
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
                commonCodeSearchFormBean.setMain_code("DECISION_FLAG");
                Tb_Code_Main decisionFlagList = commonService.codeSearch(commonCodeSearchFormBean);
                
                ht.put("ls_merchpayMasterFormList", ls_merchpayMasterFormList);
                ht.put("decisionFlagList", decisionFlagList);                
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;             
        
    }

    //가맹점 일별 정산 엑셀다운로드
    @Override
    public void basic_merchMerchpayMasterListExcel(MerchpayFormBean merchpayFormBean, String excelDownloadFilename, HttpServletResponse response) {
        
        try {
             
                List ls_merchpayMasterListExcel = merchpayDAO.basic_merchMerchpayMasterListExcel(merchpayFormBean);
                
                StringBuffer merchpayMasterListExcel = Util.makeData(ls_merchpayMasterListExcel);
        
                Util.exceldownload(merchpayMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    }    

    //가맹점 일별 정산 상세 조회
    @Override
    public Hashtable merchMerchpayDetailList(MerchpayFormBean merchpayFormBean) {
        
        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = merchpayDAO.merchMerchpayDetailListCount(merchpayFormBean);
                List<Tb_Merch_Pay> ls_merchpayDetailList = merchpayDAO.merchMerchpayDetailList(merchpayFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(merchpayFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_merchpayDetailList.size(); i++) {
                        Tb_Merch_Pay tb_Merch_Pay = ls_merchpayDetailList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Merch_Pay.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getExp_pay_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getOnffmerch_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getOnfftid()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getOnfftid_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getPay_chn_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getPay_amt()) + "\"");                                        
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getCommission()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getVat()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getApp_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Pay.getCncl_amt()) + "\"");

                        if (i == (ls_merchpayDetailList.size() - 1)) { 
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);
                
                ht.put("payChnCateList", payChnCateList);                

        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;                  
        
    }

    //가맹점 일별 정산 상세 엑셀다운로드
    @Override
    public void merchMerchpayDetailListExcel(MerchpayFormBean merchpayFormBean, String excelDownloadFilename, HttpServletResponse response) {
        
        try {
             
                List ls_merchpayDetailListExcel = merchpayDAO.merchMerchpayDetailListExcel(merchpayFormBean);
                
                StringBuffer merchpayDetailListExcel = Util.makeData(ls_merchpayDetailListExcel);
        
                Util.exceldownload(merchpayDetailListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }                
        
    }

    //가맹점 개별 정산 조회
    @Override
    public Hashtable merchMerchpayTransList(TransFormBean transFormBean) {

         SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
         /*
        if((transFormBean.getAdj_start_dt() == null) || (transFormBean.getAdj_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.WEEK_OF_MONTH, -1);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            transFormBean.setAdj_start_dt(start_dt);
            transFormBean.setAdj_end_dt(end_dt);
        }
        */
        if((transFormBean.getApp_dt_start() == null) || (transFormBean.getApp_dt_start()== "")){
        // if((transFormBean.getPay_start_dt() == null) || (transFormBean.getPay_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            //cal.add(Calendar.WEEK_OF_MONTH, -1);

            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());
            

            transFormBean.setApp_dt_start(start_dt);
            transFormBean.setApp_dt_end(end_dt);
        //    transFormBean.setPay_start_dt(start_dt);
        //    transFormBean.setPay_end_dt(end_dt);
            
            logger.trace("start_dt : " +start_dt);
            logger.trace("end_dt : " +end_dt);
        } 
        

         
        Hashtable ht = new Hashtable();

        try {

                Integer total_count = merchpayDAO.merchMerchpayTransListCount(transFormBean);
                List<Tb_Tran_Cardpg> ls_transDetailList = merchpayDAO.merchMerchpayTransList(transFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(transFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_transDetailList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_transDetailList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"data\":[");                        
                        sb.append("\"" + Util.nullToString(tb_Tran_Cardpg.getRnum()) + "\"");
                        //입금일
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnoffmerch_ext_pay_dt()) + "\"");          
                        //승인일
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_dt()) + "\"");                        
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnfftid_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_phone2()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getProduct_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMassagetype_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getIss_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCard_num()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getInstallment()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTot_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_no()) + "\"");
                        //승인시간
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_tm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTran_status_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnofftid_pay_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnofftid_commision()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnofftid_tax_amt()) + "\"");                        
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getWithhold_tax()) + "\""); 
                        //정산일
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnoffmerch_adj_dt()) + "\"");
                        
                        if (i == (ls_transDetailList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");              
                
            SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
            /*
            Date start_date = formatter.parse(transFormBean.getAdj_start_dt());
            Date end_date   = formatter.parse(transFormBean.getAdj_end_dt());
            
            String start_dt = formatter2.format(start_date);
            String end_dt = formatter2.format(end_date);

            transFormBean.setAdj_start_dt(start_dt);
            transFormBean.setAdj_end_dt(end_dt);
            */
            
            Date start_date = formatter.parse(transFormBean.getApp_dt_start());
            Date end_date   = formatter.parse(transFormBean.getApp_dt_end());
            //Date start_date = formatter.parse(transFormBean.getPay_start_dt());
            //Date end_date   = formatter.parse(transFormBean.getPay_end_dt());
            
            String start_dt = formatter2.format(start_date);
            String end_dt = formatter2.format(end_date);

            transFormBean.setApp_dt_start(start_dt);
            transFormBean.setApp_dt_end(end_dt);
            //transFormBean.setPay_start_dt(start_dt);
            //transFormBean.setPay_end_dt(end_dt);            
            
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("TRAN_STATUS");
                Tb_Code_Main tranStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("RESULT_STATUS");
                Tb_Code_Main resultStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("ISS_CD");
                Tb_Code_Main issCdList = commonService.codeSearch(commonCodeSearchFormBean);                                
                
                ht.put("payChnCateList", payChnCateList);
                ht.put("tranStatusList", tranStatusList);
                ht.put("resultStatusList", resultStatusList);
                ht.put("issCdList", issCdList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;           
        
    }
    
    

    //가맹점 개별 정산 조회
    @Override
    public Hashtable basic_merchMerchpayTransList(TransFormBean transFormBean) {

         SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );

        if((transFormBean.getApp_dt_start() == null) || (transFormBean.getApp_dt_start()== "")){
        // if((transFormBean.getPay_start_dt() == null) || (transFormBean.getPay_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            //cal.add(Calendar.WEEK_OF_MONTH, -1);

            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());

            transFormBean.setApp_dt_start(start_dt);
            transFormBean.setApp_dt_end(end_dt);

        } 
        

         
        Hashtable ht = new Hashtable();

        try {

                Integer total_count = merchpayDAO.merchMerchpayTransListCount(transFormBean);
                
                List<Tb_Tran_Cardpg> ls_transDetailList_Sum = merchpayDAO.basic_merchMerchpayTransListSum(transFormBean);
                
                
                Tb_Tran_Cardpg tb_Tot_Tran_Cardpg = ls_transDetailList_Sum.get(0);
                
                String strTpTotAppCnt = Util.nullToInt(tb_Tot_Tran_Cardpg.getApp_cnt());
                String strTpTotAppAmt = Util.nullToInt(tb_Tot_Tran_Cardpg.getApp_amt());
                String strTpTotCnclCnt = Util.nullToInt(tb_Tot_Tran_Cardpg.getCncl_cnt());
                String strTpTotCnclAmt = Util.nullToInt(tb_Tot_Tran_Cardpg.getCncl_amt());
                String strTpTotAmt = Util.nullToInt(tb_Tot_Tran_Cardpg.getTot_amt());
                String strtpTotChktrancnt = Util.nullToInt(tb_Tot_Tran_Cardpg.getChktrancnt());
                String strTpTotOnofftid_commision = Util.nullToInt(tb_Tot_Tran_Cardpg.getOnofftid_commision());
                String strTpTotOnofftid_tax_amt = Util.nullToInt(tb_Tot_Tran_Cardpg.getOnofftid_tax_amt());
                String strTpTotOnofftid_pay_amt = Util.nullToInt(tb_Tot_Tran_Cardpg.getOnofftid_pay_amt());
                
                ht.put("TotAppCnt", strTpTotAppCnt);
                ht.put("TotAppAmt", strTpTotAppAmt);
                ht.put("TotCnclCnt", strTpTotCnclCnt);
                ht.put("TotCnclAmt", strTpTotCnclAmt);
                ht.put("TotAmt", strTpTotAmt);
                ht.put("TotChktrancnt", strtpTotChktrancnt);                
                ht.put("Onofftid_commision", strTpTotOnofftid_commision);
                ht.put("Onofftid_tax_amt", strTpTotOnofftid_tax_amt);
                ht.put("TotOnofftid_pay_amt", strTpTotOnofftid_pay_amt);
                
                List<Tb_Tran_Cardpg> ls_transDetailList = merchpayDAO.merchMerchpayTransList(transFormBean);
                
            SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
            
            Date start_date = formatter.parse(transFormBean.getApp_dt_start().replaceAll("-",""));
            Date end_date   = formatter.parse(transFormBean.getApp_dt_end().replaceAll("-",""));
            
            String start_dt = formatter2.format(start_date);
            String end_dt = formatter2.format(end_date);

            transFormBean.setApp_dt_start(start_dt);
            transFormBean.setApp_dt_end(end_dt);
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("TRAN_STATUS");
                Tb_Code_Main tranStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("RESULT_STATUS");
                Tb_Code_Main resultStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("ISS_CD");
                Tb_Code_Main issCdList = commonService.codeSearch(commonCodeSearchFormBean);                                
                
                ht.put("total_count", String.valueOf(total_count.intValue()));
                
                ht.put("ls_transDetailList", ls_transDetailList);
                ht.put("payChnCateList", payChnCateList);
                ht.put("tranStatusList", tranStatusList);
                ht.put("resultStatusList", resultStatusList);
                ht.put("issCdList", issCdList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;           
        
    }    
    

    //가맹점 개별 정산 엑셀다운로드
    @Override
    public void merchMerchpayTransListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
             
                List ls_merchMerchpayTransListExcel = merchpayDAO.merchMerchpayTransListExcel(transFormBean);
                
                StringBuffer merchMerchpayTransListExcel = Util.makeData(ls_merchMerchpayTransListExcel);
        
                Util.exceldownload(merchMerchpayTransListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }                
        
    }

    //가맹점 개별 정산 엑셀다운로드
    @Override
    public void basic_merchMerchpayTransListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
             
                List ls_merchMerchpayTransListExcel = merchpayDAO.basic_merchMerchpayTransListExcel(transFormBean);
                
                StringBuffer merchMerchpayTransListExcel = Util.makeData(ls_merchMerchpayTransListExcel);
        
                Util.exceldownload(merchMerchpayTransListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }                
        
    }    
    
    //임시 정보 조회
    @Override
    public Hashtable merchpayScheTmpList(MerchpayFormBean merchpayFormBean) {
        Hashtable ht = new Hashtable();

        try {
            
                List<Tb_Merch_Pay> merchpayScheTmpList = merchpayDAO.merchpayScheTmpList(merchpayFormBean);
                
                ht.put("ResultSet", merchpayScheTmpList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;   
    }

    //임시 정보 저장
    @Override
    @Transactional
    public void merchpayScheTmpInsert(MerchpayFormBean merchpayFormBean) {
        
        Integer scheTmpCount = merchpayDAO.merchpayScheTmpCount(merchpayFormBean);
        
        if(scheTmpCount > 0){
            merchpayDAO.merchpayScheTmpUpdate(merchpayFormBean);
        }else{
            merchpayDAO.merchpayScheTmpInsert(merchpayFormBean);        
        } 
        
    }
    
    //선지급 저장
    @Override
    @Transactional
    public void merchpayCashInAdvanceInsert(MerchpayFormBean merchpayFormBean) {

        Tb_Merch_Pay tb_Merch_Pay = new Tb_Merch_Pay();
        tb_Merch_Pay.setUser_seq(merchpayFormBean.getUser_seq());
        tb_Merch_Pay.setOnffmerch_no(merchpayFormBean.getOnffmerch_no());
        tb_Merch_Pay.setExp_pay_dt(merchpayFormBean.getExp_pay_dt().replaceAll("-", ""));
        tb_Merch_Pay.setPay_amt("0");
        //tb_Merch_Pay.setPay_amt(merchpayFormBean.getOut_amt());
        tb_Merch_Pay.setPay_targetamt("0");
        tb_Merch_Pay.setCommission("0");
        tb_Merch_Pay.setApp_amt("0");
        tb_Merch_Pay.setCncl_amt("0");
        tb_Merch_Pay.setOut_amt(merchpayFormBean.getOut_amt());
        //tb_Merch_Pay.setDecision_pay_amt("0");
        tb_Merch_Pay.setDecision_pay_amt(merchpayFormBean.getOut_amt());
        tb_Merch_Pay.setDfr_bal_amt("0");
        tb_Merch_Pay.setGen_dfr_amt("0");
        tb_Merch_Pay.setDec_dfr_amt("0");
        tb_Merch_Pay.setDec_bal_amt("0");
        tb_Merch_Pay.setRel_dfr_amt("0");
        tb_Merch_Pay.setCur_outamt("0");
        tb_Merch_Pay.setSetoff_amt("0");
        tb_Merch_Pay.setVat("0");
        
        //지급확정처리
        merchpayDAO.merchpayMetabolismConfirmProcess(tb_Merch_Pay);
        
	//미수금 생성
        MerchoutamtFormBean merchoutamtFormBean = new MerchoutamtFormBean();
        merchoutamtFormBean.setGen_reson("007");
        merchoutamtFormBean.setGen_dt(merchpayFormBean.getExp_pay_dt());
        merchoutamtFormBean.setOut_amt(tb_Merch_Pay.getOut_amt());
        merchoutamtFormBean.setCur_outamt(tb_Merch_Pay.getOut_amt());
        merchoutamtFormBean.setMemo("");
        merchoutamtFormBean.setUser_seq(merchpayFormBean.getUser_seq());
        merchoutamtFormBean.setOnffmerch_no(merchpayFormBean.getOnffmerch_no());
        merchoutamtFormBean.setExp_pay_dt(merchpayFormBean.getExp_pay_dt().replaceAll("-", ""));
        merchoutamtFormBean.setPay_sche_seq(tb_Merch_Pay.getPay_sche_seq());
        merchoutamtDAO.merchoutamtMasterInsert(merchoutamtFormBean);        
        
    }    
    
}
