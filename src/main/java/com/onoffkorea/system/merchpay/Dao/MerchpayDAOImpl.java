/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.merchpay.Dao;

import com.onoffkorea.system.dframt.Vo.Tb_Onffmerch_Dfr_Gen;
import com.onoffkorea.system.merchoutamt.Vo.Tb_Merch_Outamt_Main;
import com.onoffkorea.system.merchpay.Bean.MerchpayFormBean;
import com.onoffkorea.system.merchpay.Vo.Tb_Merch_Pay;
import com.onoffkorea.system.trans.Bean.TransFormBean;
import com.onoffkorea.system.trans.Vo.Tb_Tran_Cardpg;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("merchpayDAO")
public class MerchpayDAOImpl extends SqlSessionDaoSupport implements MerchpayDAO{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    //지급 조회
    @Override
    public List<Tb_Merch_Pay> merchpayMasterFormList(MerchpayFormBean merchpayFormBean) {
        
        return (List)getSqlSession().selectList("Merchpay.merchpayMasterFormList",merchpayFormBean);
        
    }
    
    
    //지급 조회
    @Override
    public List<Tb_Merch_Pay> basic_merchpayMasterSum(MerchpayFormBean merchpayFormBean) {
        
        return (List)getSqlSession().selectList("Merchpay.basic_sum_merchMerchpayMasterList",merchpayFormBean);
        
    }    

    //지급확정
    @Override
    public String merchpayMetabolismConfirmProcess(Tb_Merch_Pay tb_Merch_Pay) {
        
        getSqlSession().insert("Merchpay.merchpayMetabolismConfirmProcess",tb_Merch_Pay);
        
        return tb_Merch_Pay.getPay_sche_seq();
        
    }

    //지급확정취소
    @Override
    public Integer merchpayMetabolismCancelProcess(MerchpayFormBean merchpayFormBean) {
        
        return (Integer)getSqlSession().delete("Merchpay.merchpayMetabolismCancelProcess",merchpayFormBean);
        
    }

    //지급 상세 조회
    @Override
    public List<Tb_Merch_Pay> merchpayDetailList(MerchpayFormBean merchpayFormBean) {
        
        return (List)getSqlSession().selectList("Merchpay.merchpayDetailList",merchpayFormBean);
        
    } 
    
    //지급 상세 조회
    @Override
    public Integer merchpayDetailListCount(MerchpayFormBean merchpayFormBean) {
        
        return (Integer)getSqlSession().selectOne("Merchpay.merchpayDetailListCount",merchpayFormBean);
        
    }     
    
    //지급 상세건 확정 처리
    @Override
    public void merchpayDecisionFlagYUpdate(MerchpayFormBean merchpayFormBean) {
        
        getSqlSession().update("Merchpay.merchpayDecisionFlagYUpdate",merchpayFormBean);
        
    }    
    
    //지급 상세건 확정취소 처리
    @Override
    public void merchpayDecisionFlagNUpdate(MerchpayFormBean merchpayFormBean) {
        
        getSqlSession().update("Merchpay.merchpayDecisionFlagNUpdate",merchpayFormBean);
        
    }        

    //지급 엑셀다운로드
    @Override
    public List merchpayMasterListExcel(MerchpayFormBean merchpayFormBean) {
        
        return (List)getSqlSession().selectList("Merchpay.merchpayMasterListExcel",merchpayFormBean);
        
    }

    //지급내역 상세 엑셀다운로드
    @Override
    public List merchpayDetailListExcel(MerchpayFormBean merchpayFormBean) {
        
        return (List)getSqlSession().selectList("Merchpay.merchpayDetailListExcel",merchpayFormBean);
        
    }

    //가맹점 일별 정산 조회
    @Override
    public List<Tb_Merch_Pay> merchMerchpayMasterList(MerchpayFormBean merchpayFormBean) {
        
        return (List)getSqlSession().selectList("Merchpay.merchMerchpayMasterList",merchpayFormBean);
        
    }

    //가맹점 일별 정산 엑셀다운로드
    @Override
    public List merchMerchpayMasterListExcel(MerchpayFormBean merchpayFormBean) {
        
        return (List)getSqlSession().selectList("Merchpay.merchMerchpayMasterListExcel",merchpayFormBean);
        
    }

        //가맹점 일별 정산 엑셀다운로드
    @Override
    public List basic_merchMerchpayMasterListExcel(MerchpayFormBean merchpayFormBean) {
        
        return (List)getSqlSession().selectList("Merchpay.basic_merchMerchpayMasterListExcel",merchpayFormBean);
        
    }
    
    //가맹점 일별 정산 상세 조회 카운트
    @Override
    public Integer merchMerchpayDetailListCount(MerchpayFormBean merchpayFormBean) {
        
        return (Integer)getSqlSession().selectOne("Merchpay.merchMerchpayDetailListCount",merchpayFormBean);
        
    }

    //가맹점 일별 정산 상세 조회
    @Override
    public List<Tb_Merch_Pay> merchMerchpayDetailList(MerchpayFormBean merchpayFormBean) {
        
        return (List)getSqlSession().selectList("Merchpay.merchMerchpayDetailList",merchpayFormBean);
        
    }

    //가맹점 일별 정산 상세 엑셀다운로드
    @Override
    public List merchMerchpayDetailListExcel(MerchpayFormBean merchpayFormBean) {
        
        return (List)getSqlSession().selectList("Merchpay.merchMerchpayDetailListExcel",merchpayFormBean);
        
    }

    //가맹점 개별 정산 조회 카운트
    @Override
    public Integer merchMerchpayTransListCount(TransFormBean transFormBean) {
        logger.trace("-------------------DAO app_dt_start : " + transFormBean.getApp_dt_start());
        logger.trace("-------------------DAO app_dt_end : " + transFormBean.getApp_dt_end());
        return (Integer)getSqlSession().selectOne("Merchpay.merchMerchpayTransListCount",transFormBean);
        
    }

    ////가맹점 개별 정산 조회
    @Override
    public List<Tb_Tran_Cardpg> merchMerchpayTransList(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Merchpay.merchMerchpayTransList",transFormBean);
        
    }
    
    ////가맹점 개별 정산 조회
    @Override
    public List<Tb_Tran_Cardpg> basic_merchMerchpayTransListSum(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Merchpay.basic_merchMerchpayTransListSum",transFormBean);
        
    }    

    //가맹점 개별 정산 조회 엑셀다운로드
    @Override
    public List merchMerchpayTransListExcel(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Merchpay.merchMerchpayTransListExcel",transFormBean);
        
    }
    
        //가맹점 개별 정산 조회 엑셀다운로드
    @Override
    public List basic_merchMerchpayTransListExcel(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Merchpay.basic_merchMerchpayTransListExcel",transFormBean);
        
    }


    //임시 정보 조회
    @Override
    public List<Tb_Merch_Pay> merchpayScheTmpList(MerchpayFormBean merchpayFormBean) {
        return (List)getSqlSession().selectList("Merchpay.merchpayScheTmpList",merchpayFormBean);
    }

    //임시 정보 생성
    @Override
    public void merchpayScheTmpInsert(MerchpayFormBean merchpayFormBean) {
        getSqlSession().insert("Merchpay.merchpayScheTmpInsert",merchpayFormBean);
    }

    //임시 정보 카운트
    @Override
    public Integer merchpayScheTmpCount(MerchpayFormBean merchpayFormBean) {
        return (Integer)getSqlSession().selectOne("Merchpay.merchpayScheTmpCount",merchpayFormBean); 
    }

    //임시 정보 수정
    @Override
    public void merchpayScheTmpUpdate(MerchpayFormBean merchpayFormBean) {
        getSqlSession().update("Merchpay.merchpayScheTmpUpdate",merchpayFormBean);
    }

    //보류잔액 조회
    @Override
    public List<Tb_Onffmerch_Dfr_Gen> dfrBalAmtList(MerchpayFormBean merchpayFormBean) {
        return (List)getSqlSession().selectList("Merchpay.dfrBalAmtList",merchpayFormBean);
    }

    //보류확정잔액 조회
    @Override
    public List<Tb_Onffmerch_Dfr_Gen> decBalAmtList(MerchpayFormBean merchpayFormBean) {
        return (List)getSqlSession().selectList("Merchpay.decBalAmtList",merchpayFormBean);
    }

    //미수잔액 조회
    @Override
    public List<Tb_Merch_Outamt_Main> merchCuroutamtList(MerchpayFormBean merchpayFormBean) {
        return (List)getSqlSession().selectList("Merchpay.merchCuroutamtList",merchpayFormBean);
    }

    //미수금 조회
    @Override
    public List<Tb_Merch_Outamt_Main> merchoutamtList(MerchpayFormBean merchpayFormBean) {
        return (List)getSqlSession().selectList("Merchpay.merchoutamtList",merchpayFormBean);
    }

    //미수상계 조회
    @Override
    public List<Tb_Merch_Outamt_Main> setoffamtList(MerchpayFormBean merchpayFormBean) {
        return (List)getSqlSession().selectList("Merchpay.setoffamtList",merchpayFormBean);
    }

    //보류해제 조회
    @Override
    public List<Tb_Onffmerch_Dfr_Gen> relDfrAmtList(MerchpayFormBean merchpayFormBean) {
        return (List)getSqlSession().selectList("Merchpay.relDfrAmtList",merchpayFormBean);
    }

    //보류확정 조회
    @Override
    public List<Tb_Onffmerch_Dfr_Gen> decDfrAmtList(MerchpayFormBean merchpayFormBean) {
        return (List)getSqlSession().selectList("Merchpay.decDfrAmtList",merchpayFormBean);
    }
    
    //선지급 생성
    @Override
    public void merchpayCashInAdvanceInsert(MerchpayFormBean merchpayFormBean) {
        getSqlSession().insert("Merchpay.merchpayCashInAdvanceInsert",merchpayFormBean);
    }    

}
