/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.merchpay.Dao;

import com.onoffkorea.system.dframt.Vo.Tb_Onffmerch_Dfr_Gen;
import com.onoffkorea.system.merchoutamt.Vo.Tb_Merch_Outamt_Main;
import com.onoffkorea.system.merchpay.Bean.MerchpayFormBean;
import com.onoffkorea.system.merchpay.Vo.Tb_Merch_Pay;
import com.onoffkorea.system.trans.Bean.TransFormBean;
import com.onoffkorea.system.trans.Vo.Tb_Tran_Cardpg;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface MerchpayDAO {

    public List<Tb_Merch_Pay> merchpayMasterFormList(MerchpayFormBean merchpayFormBean);
    
    public List<Tb_Merch_Pay> basic_merchpayMasterSum(MerchpayFormBean merchpayFormBean);

    public String merchpayMetabolismConfirmProcess(Tb_Merch_Pay tb_Merch_Pay);

    public Integer merchpayMetabolismCancelProcess(MerchpayFormBean merchpayFormBean);

    public List<Tb_Merch_Pay> merchpayDetailList(MerchpayFormBean merchpayFormBean);

    public Integer merchpayDetailListCount(MerchpayFormBean merchpayFormBean);

    public void merchpayDecisionFlagYUpdate(MerchpayFormBean merchpayFormBean);

    public void merchpayDecisionFlagNUpdate(MerchpayFormBean merchpayFormBean);

    public List merchpayMasterListExcel(MerchpayFormBean merchpayFormBean);

    public List merchpayDetailListExcel(MerchpayFormBean merchpayFormBean);

    public List<Tb_Merch_Pay> merchMerchpayMasterList(MerchpayFormBean merchpayFormBean);

    public List merchMerchpayMasterListExcel(MerchpayFormBean merchpayFormBean);
    
    public List basic_merchMerchpayMasterListExcel(MerchpayFormBean merchpayFormBean);

    public Integer merchMerchpayDetailListCount(MerchpayFormBean merchpayFormBean);

    public List<Tb_Merch_Pay> merchMerchpayDetailList(MerchpayFormBean merchpayFormBean);

    public List merchMerchpayDetailListExcel(MerchpayFormBean merchpayFormBean);

    public Integer merchMerchpayTransListCount(TransFormBean transFormBean);

    public List<Tb_Tran_Cardpg> merchMerchpayTransList(TransFormBean transFormBean);
    
    public List<Tb_Tran_Cardpg> basic_merchMerchpayTransListSum(TransFormBean transFormBean);

    public List merchMerchpayTransListExcel(TransFormBean transFormBean);
    
    public List basic_merchMerchpayTransListExcel(TransFormBean transFormBean);

    public List<Tb_Merch_Pay> merchpayScheTmpList(MerchpayFormBean merchpayFormBean);

    public void merchpayScheTmpInsert(MerchpayFormBean merchpayFormBean);

    public Integer merchpayScheTmpCount(MerchpayFormBean merchpayFormBean);

    public void merchpayScheTmpUpdate(MerchpayFormBean merchpayFormBean);

    public List<Tb_Onffmerch_Dfr_Gen> dfrBalAmtList(MerchpayFormBean merchpayFormBean);

    public List<Tb_Onffmerch_Dfr_Gen> decBalAmtList(MerchpayFormBean merchpayFormBean);

    public List<Tb_Merch_Outamt_Main> merchCuroutamtList(MerchpayFormBean merchpayFormBean);

    public List<Tb_Merch_Outamt_Main> merchoutamtList(MerchpayFormBean merchpayFormBean);

    public List<Tb_Merch_Outamt_Main> setoffamtList(MerchpayFormBean merchpayFormBean);

    public List<Tb_Onffmerch_Dfr_Gen> relDfrAmtList(MerchpayFormBean merchpayFormBean);

    public List<Tb_Onffmerch_Dfr_Gen> decDfrAmtList(MerchpayFormBean merchpayFormBean);

    public void merchpayCashInAdvanceInsert(MerchpayFormBean merchpayFormBean);
    
}
