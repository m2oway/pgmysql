/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.onfftotinput.Controller;

import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.common.vo.Tb_Code_Detail;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import com.onoffkorea.system.onfftotinput.Bean.OnoffTotInpuFormBean;
import com.onoffkorea.system.onfftotinput.Service.OnfftotinputService;
import common.web.servlet.BaseSpringMultiActionController;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/onfftotinput/**")
public class OnfftotinputContrloller extends BaseSpringMultiActionController{
    
    protected Log logger = LogFactory.getLog(getClass());

    /*
    @Autowired
    private MerchantService merchantService;
    */
    
    @Autowired
    private CommonService commonService;
    
    @Autowired
    private OnfftotinputService onfftotinputService;
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }    
    
    
    //정보 등록
    @RequestMapping(method= RequestMethod.GET,value = "/onfftotInput")
    public String onfftotInsertForm(@Valid OnoffTotInpuFormBean onfftotinputFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        //사업자정보용코드를 만든다.
        commonCodeSearchFormBean.setMain_code("COMP_CATE");
        Tb_Code_Main compCateList = commonService.codeSearch(commonCodeSearchFormBean);
        
        model.addAttribute("compCateList",compCateList);
        
        
        commonCodeSearchFormBean.setMain_code("TAX_FLAG");
        Tb_Code_Main TaxFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("TaxFlagList",TaxFlagList);
        
        //지급ID용 코드를 만든다.
        commonCodeSearchFormBean.setMain_code("SVC_STAT");
        Tb_Code_Main SvcStatList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("SvcStatList",SvcStatList);
        
        commonCodeSearchFormBean.setMain_code("MERCH_CATE");
        Tb_Code_Main MerchCateList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("MerchCateList",MerchCateList);

        commonCodeSearchFormBean.setMain_code("BAL_PERIOD");
        Tb_Code_Main BalPeriodList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("BalPeriodList",BalPeriodList);
        
        commonCodeSearchFormBean.setMain_code("PAY_DT_CD_1");
        Tb_Code_Main PayDtCdList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("PayDtCdList",PayDtCdList);
        
        commonCodeSearchFormBean.setMain_code("CNCL_AUTH");
        Tb_Code_Main Cncl_AuthList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("Cncl_AuthList",Cncl_AuthList);
        
        //코드표를 만든다.
        StringBuilder sb_day = new StringBuilder();
        StringBuilder sb_week = new StringBuilder();        
        StringBuilder sb_half = new StringBuilder();        
        StringBuilder sb_month = new StringBuilder();
        
        sb_day.append("[");
        sb_week.append("[");
        sb_half.append("[");
        sb_month.append("[");
        
        int int_day = 0;
        int int_week = 0;
        int int_half = 0;
        int int_month = 0;
        
        for (int i = 0; i < PayDtCdList.getTb_code_details().size(); i++) {
                Tb_Code_Detail detailcodeobj = (Tb_Code_Detail) PayDtCdList.getTb_code_details().get(i);
           
                logger.trace("detailcodeobj.getCode_memo() : " + detailcodeobj.getCode_memo());
                
                if("DAY".equals(detailcodeobj.getCode_memo()))
                {   
                    if(int_day == 0)
                    {
                        sb_day.append("{");
                    }
                    else
                    {
                        sb_day.append(",{");
                    }
                    sb_day.append("\"VIEW\":\""+ detailcodeobj.getCode_nm() +"\",\"ID\":\""+ detailcodeobj.getDetail_code() +"\"");
                    sb_day.append("}");
                    int_day++;
                }
                else if("WEEK".equals(detailcodeobj.getCode_memo()))
                {
                    if(int_week == 0)
                    {
                        sb_week.append("{");
                    }
                    else
                    {
                        sb_week.append(",{");
                    }
                    sb_week.append("\"VIEW\":\""+ detailcodeobj.getCode_nm() +"\",\"ID\":\""+ detailcodeobj.getDetail_code() +"\"");
                    sb_week.append("}");
                    int_week++;                    
                }
                else if("HALF".equals(detailcodeobj.getCode_memo()))
                {
                    if(int_half == 0)
                    {
                        sb_half.append("{");
                    }
                    else
                    {
                        sb_half.append(",{");
                    }
                    sb_half.append("\"VIEW\":\""+ detailcodeobj.getCode_nm() +"\",\"ID\":\""+ detailcodeobj.getDetail_code() +"\"");
                    sb_half.append("}");
                    int_half++;                    
                }
                else if("MONTH".equals(detailcodeobj.getCode_memo()))
                {
                    if(int_month == 0)
                    {
                        sb_month.append("{");
                    }
                    else
                    {
                        sb_month.append(",{");
                    }
                    sb_month.append("\"VIEW\":\""+ detailcodeobj.getCode_nm() +"\",\"ID\":\""+ detailcodeobj.getDetail_code() +"\"");
                    sb_month.append("}");
                    int_month++;                    
                }
        }
        
        sb_day.append("]");
        sb_week.append("]");
        sb_half.append("]");
        sb_month.append("]");
        
        model.addAttribute("PAY_DT_CD_DAY",sb_day.toString());       
        model.addAttribute("PAY_DT_CD_WEEK",sb_week.toString());       
        model.addAttribute("PAY_DT_CD_HALF",sb_half.toString());       
        model.addAttribute("PAY_DT_CD_MONTH",sb_month.toString());       
        
        commonCodeSearchFormBean.setMain_code("BANK_CD");
        Tb_Code_Main BankCdList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("BankCdList",BankCdList);       
        
        commonCodeSearchFormBean.setMain_code("BIZ_TYPE");
        Tb_Code_Main BizTypeList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("BizTypeList",BizTypeList);
        
        commonCodeSearchFormBean.setMain_code("BIZ_CATE");
        Tb_Code_Main BizCateList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("BizCateList",BizCateList);        
        
        commonCodeSearchFormBean.setMain_code("SECURITY_CATE");
        Tb_Code_Main SecurityCateList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("SecurityCateList",SecurityCateList);        

        //UID정보 코드를 만든다.
          
        commonCodeSearchFormBean.setMain_code("SVC_STAT");
        Tb_Code_Main UseFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("UseFlagList",UseFlagList);

        commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
        Tb_Code_Main PayChnCateList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("PayChnCateList",PayChnCateList);
        
        
        commonCodeSearchFormBean.setMain_code("CERT_TYPE");
        Tb_Code_Main CertTypeList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("CertTypeList",CertTypeList);

        //사용자 유형
        commonCodeSearchFormBean.setMain_code("USER_CATE");
        Tb_Code_Main userCateList = commonService.codeSearch(commonCodeSearchFormBean); 
        model.addAttribute("userCateList",userCateList);
        
        return null;
        
    }   
 
    //merchant 정보 등록
    @RequestMapping(method= RequestMethod.POST,value = "/onfftotInput")
    public @ResponseBody String onfftotInsert(@Valid OnoffTotInpuFormBean onfftotinputFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {
        String result = null;
        
        System.out.println("\n\n\n-------------onfftotInsert------------");
        
        logger.debug("dentalHopitalSession.getUser_seq() : " + dentalHopitalSession.getUser_seq());
        
        onfftotinputFormBean.setIns_user(dentalHopitalSession.getUser_seq());
        onfftotinputFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        onfftotinputFormBean.setUser_seq(dentalHopitalSession.getUser_seq());
        result = onfftotinputService.OnfftotInsert(onfftotinputFormBean);
        
        return result;
        
    }       

    //사업자번호 중복 첵크
    @RequestMapping(method= RequestMethod.GET,value = "/onfftotInputBizNoChk")
    public @ResponseBody String getInputBizNoChkForm(@Valid OnoffTotInpuFormBean onfftotinputFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception 
    {
        String result = null;
        result = onfftotinputService.getInputBizNoChk(onfftotinputFormBean);
        return result;        
    }
    
    //ID 중복 첵크
    @RequestMapping(method= RequestMethod.GET,value = "/onfftotInputLoginIdChk")
    public @ResponseBody String getInputLoginIdChkForm(@Valid OnoffTotInpuFormBean onfftotinputFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception 
    {
        String result = null;
        result = onfftotinputService.getInputLoginIdChk(onfftotinputFormBean);
        return result;        
    }           
    
}
