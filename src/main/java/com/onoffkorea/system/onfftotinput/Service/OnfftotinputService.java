/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.onfftotinput.Service;

import com.onoffkorea.system.onfftotinput.Bean.OnoffTotInpuFormBean;
import java.util.Hashtable;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public interface OnfftotinputService {
   
    public String OnfftotInsert( OnoffTotInpuFormBean onfftotinputFormBean) throws Exception;
    
    
    public String getInputBizNoChk( OnoffTotInpuFormBean onfftotinputFormBean) throws Exception;
    
    
    public String getInputLoginIdChk( OnoffTotInpuFormBean onfftotinputFormBean) throws Exception;
    
   
}
