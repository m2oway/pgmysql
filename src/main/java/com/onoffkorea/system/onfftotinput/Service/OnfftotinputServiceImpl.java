/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.onfftotinput.Service;

import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.company.Bean.CompanyFormBean;
import com.onoffkorea.system.company.Dao.CompanyDAO;
import com.onoffkorea.system.merchant.Bean.MerchantFormBean;
import com.onoffkorea.system.merchant.Bean.SecurityFormBean;
import com.onoffkorea.system.merchant.Dao.MerchantDAO;
import com.onoffkorea.system.merchant.Vo.Tb_Onffmerch_Mst;
import com.onoffkorea.system.merchant.Vo.Tb_Security;
import com.onoffkorea.system.onfftotinput.Bean.OnoffTotInpuFormBean;
import com.onoffkorea.system.onfftotinput.Dao.OnfftotinputDAO;
import com.onoffkorea.system.terminal.Bean.TerminalFormBean;
import com.onoffkorea.system.terminal.Bean.TmlCmsFormBean;
import com.onoffkorea.system.terminal.Dao.TerminalDAO;
import com.onoffkorea.system.user.Bean.UserFormBean;
import com.onoffkorea.system.user.Dao.UserDAO;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service("onfftotinputService")
public class OnfftotinputServiceImpl implements OnfftotinputService
{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    
    @Autowired
    private CommonService commonService;    
    
    @Autowired
    private OnfftotinputDAO onfftotinputDAO;    
    
    //회사
    @Autowired
    private CompanyDAO companyDAO;
    
    //지급ID
    @Autowired
    private MerchantDAO merchantDAO;
    
    //UID
    @Autowired
    private TerminalDAO terminalDAO;
    
    //user
    
    @Autowired
    private UserDAO userDAO;

    
    @Override
    @Transactional
    public String OnfftotInsert( OnoffTotInpuFormBean onfftotinputFormBean) throws Exception {
        Integer result = null;
        
        
        logger.debug("---------totinsert-------------------------");
        //회사정보를 입력한다.
                
        //입력할 회사정보의 SEQ를 가져온다.
        
        logger.debug("---------cmop insert-------------------------");
        String strComp_seq = onfftotinputDAO.getCompInfoSeq("");
        
        logger.debug("strComp_seq : " + strComp_seq);
        
        CompanyFormBean companyFormBean = new CompanyFormBean();
        companyFormBean.setUser_seq(onfftotinputFormBean.getUser_seq());
        companyFormBean.setComp_seq   (strComp_seq);     
        companyFormBean.setComp_nm    (onfftotinputFormBean.getComp_nm());     
        companyFormBean.setBiz_no     (onfftotinputFormBean.getComp_biz_no());     
        companyFormBean.setCorp_no    (onfftotinputFormBean.getComp_corp_no());     
        companyFormBean.setComp_ceo_nm(onfftotinputFormBean.getComp_ceo_nm());     
        companyFormBean.setComp_tel1  (onfftotinputFormBean.getComp_tel1());     
        companyFormBean.setComp_tel2  (onfftotinputFormBean.getComp_tel2());     
        companyFormBean.setUse_flag   (onfftotinputFormBean.getComp_use_flag());     
        //companyFormBean.setUse_flag_nm(onfftotinputFormBean);     
        logger.debug("xxx onfftotinputFormBean.getComp_cate() : " + onfftotinputFormBean.getComp_cate() );
        companyFormBean.setComp_cate  (onfftotinputFormBean.getComp_cate());     
        companyFormBean.setZip_cd     (onfftotinputFormBean.getComp_zip_cd());     
        companyFormBean.setAddr_1     (onfftotinputFormBean.getComp_addr_1());     
        companyFormBean.setAddr_2     (onfftotinputFormBean.getComp_addr_2());     
        //companyFormBean.setCate_comp  (onfftotinputFormBean.getComp_cate_comp());       
        companyFormBean.setDel_flag   (onfftotinputFormBean.getComp_del_flag());        
        companyFormBean.setMemo       (onfftotinputFormBean.getComp_memo());            
        companyFormBean.setBiz_cate   (onfftotinputFormBean.getComp_biz_cate());        
        companyFormBean.setBiz_type(onfftotinputFormBean.getComp_biz_type());        
        companyFormBean.setTax_flag(onfftotinputFormBean.getComp_tax_flag());        
        //companyFormBean.setTax_flag_nm(onfftotinputFormBean);     
        companyFormBean.setComp_email(onfftotinputFormBean.getComp_email());      
        companyFormBean.setIns_dt     (onfftotinputFormBean.getIns_dt());     
        companyFormBean.setMod_dt     (onfftotinputFormBean.getMod_dt());     
        companyFormBean.setIns_user   (onfftotinputFormBean.getIns_user());     
        companyFormBean.setMod_user   (onfftotinputFormBean.getMod_user());                   
        
        result = this.onfftotinputDAO.companyMasterInsert(companyFormBean);

        logger.debug("------comp insert result : " + result);
        
        //지급id(가맹점정보)를 등록한다.
        logger.debug("------mid insert resu------------");
        MerchantFormBean merchantFormBean = new MerchantFormBean();
        
        String strOnffmerch_no = this.merchantDAO.GetmerchantMasterKey();
        merchantFormBean.setOnffmerch_no(strOnffmerch_no);
        
        logger.debug("------strOnffmerch_no : " + strOnffmerch_no);
     
       merchantFormBean.setComp_seq(strComp_seq);
       merchantFormBean.setUser_seq(onfftotinputFormBean.getUser_seq());
       merchantFormBean.setMerch_nm(onfftotinputFormBean.getMidinfo_merch_nm());
       merchantFormBean.setTel_1(onfftotinputFormBean.getMidinfo_tel_1());
       merchantFormBean.setTel_2(onfftotinputFormBean.getMidinfo_tel_2());
       merchantFormBean.setEmail(onfftotinputFormBean.getMidinfo_email());
       merchantFormBean.setZip_cd(onfftotinputFormBean.getMidinfo_zip_cd());
       merchantFormBean.setAddr_1(onfftotinputFormBean.getMidinfo_addr_1());
       merchantFormBean.setAddr_2(onfftotinputFormBean.getMidinfo_addr_2());
       merchantFormBean.setSvc_stat(onfftotinputFormBean.getMidinfo_svc_stat());
       //merchantFormBean.setSvc_stat_nm(onfftotinputFormBean);
       merchantFormBean.setMerch_cate(onfftotinputFormBean.getMidinfo_merch_cate());
       //merchantFormBean.setMerch_cate_nm(onfftotinputFormBean);
       merchantFormBean.setBiz_type(onfftotinputFormBean.getMidinfo_biz_cate());
       //merchantFormBean.setBiz_type_nm(onfftotinputFormBean);
       merchantFormBean.setBiz_cate(onfftotinputFormBean.getMidinfo_biz_type());
       //merchantFormBean.setBiz_cate_nm(onfftotinputFormBean);
       merchantFormBean.setTax_flag(onfftotinputFormBean.getMidinfo_tax_flag());
       //merchantFormBean.setTax_flag_nm(onfftotinputFormBean);
       merchantFormBean.setClose_dt(onfftotinputFormBean.getMidinfo_close_dt());
       merchantFormBean.setClose_user(onfftotinputFormBean.getMidinfo_close_user());
       merchantFormBean.setClose_reson(onfftotinputFormBean.getMidinfo_close_reson());
       merchantFormBean.setCommision(onfftotinputFormBean.getMidinfo_commision());
       merchantFormBean.setBal_period(onfftotinputFormBean.getMidinfo_bal_period());
       
       merchantFormBean.setPay_dt_cd_1(onfftotinputFormBean.getMidinfo_pay_dt_cd_1());
       merchantFormBean.setPay_dt_cd_2(onfftotinputFormBean.getMidinfo_pay_dt_cd_2());
       merchantFormBean.setBank_cd(onfftotinputFormBean.getMidinfo_bank_cd());
       merchantFormBean.setAcc_no(onfftotinputFormBean.getMidinfo_acc_no());
       merchantFormBean.setAcc_nm(onfftotinputFormBean.getMidinfo_acc_nm());
       merchantFormBean.setMemo(onfftotinputFormBean.getMidinfo_memo());
       merchantFormBean.setAgent_commission(onfftotinputFormBean.getMidinfo_agent_commission());
       //merchantFormBean.setOrg_seq(onfftotinputFormBean);
       merchantFormBean.setAgent_seq(onfftotinputFormBean.getMidinfo_agent_seq());
       //merchantFormBean.setAgent_nm(onfftotinputFormBean);
       merchantFormBean.setDel_flag(onfftotinputFormBean.getMidinfo_del_flag());
       merchantFormBean.setApp_chk_amt(onfftotinputFormBean.getMidinfo_app_chk_amt());
       merchantFormBean.setPay_dt(onfftotinputFormBean.getMidinfo_pay_dt());
       merchantFormBean.setPay_chn_info(onfftotinputFormBean.getMidinfo_pay_chn_info());
       
       merchantFormBean.setCncl_auth(onfftotinputFormBean.getMidinfo_cncl_auth());
       //merchantFormBean.setCncl_auth_nm(onfftotinputFormBean);
       merchantFormBean.setAppreq_chk_amt(onfftotinputFormBean.getMidinfo_appreq_chk_amt());

       merchantFormBean.setIns_dt     (onfftotinputFormBean.getIns_dt());     
       merchantFormBean.setMod_dt     (onfftotinputFormBean.getMod_dt());     
       merchantFormBean.setIns_user   (onfftotinputFormBean.getIns_user());     
       merchantFormBean.setMod_user   (onfftotinputFormBean.getMod_user());                   
        
        result = this.merchantDAO.merchantMasterInsert(merchantFormBean);
        
        String strmerchantDtlInfo = onfftotinputFormBean.getSecurity_info();
        
        logger.trace("strmerchantDtlInfo :" + strmerchantDtlInfo);
        
        if(strmerchantDtlInfo != null && !strmerchantDtlInfo.equals(""))
        {
            String[] rows = strmerchantDtlInfo.split("]]]");
            
            for(int x=0 ; x < rows.length ; x++)
            {
                String[] cols = rows[x].split("```");
                
                //담보종류코드,담보액,적용시작일,적용종료일,MEMO
                //   0           1      2        3        4
                SecurityFormBean insdtlobj = new SecurityFormBean();
                
                insdtlobj.setOnffmerch_no(strOnffmerch_no);
                insdtlobj.setSecurity_cate(cols[0]);
                insdtlobj.setSecurity_amt(cols[1]);
                insdtlobj.setStart_dt(cols[2]);
                insdtlobj.setEnd_dt(cols[3]);
                insdtlobj.setMemo(cols[4]);
                insdtlobj.setIns_user(merchantFormBean.getIns_user());
                insdtlobj.setMod_user(merchantFormBean.getIns_user());
                
                this.merchantDAO.merchantDtlInsert(insdtlobj);
            }
        }
        
        logger.debug("------mid insert result : " + result);
       
        //cid 입력
        
        logger.debug("------cid insert ------------------" );
        TerminalFormBean terminalFormBean  = new TerminalFormBean();
        
        String strOnfftid = this.terminalDAO.GetterminalMasterKey();
        
        logger.debug("------strOnfftid : "+ strOnfftid);
        
        terminalFormBean.setOnfftid(strOnfftid);
        
        terminalFormBean.setPay_chn_cate(onfftotinputFormBean.getUid_pay_chn_cate());
        //terminalFormBean.setPay_chn_cate_nm(onfftotinputFormBean);
        //terminalFormBean.setOnfftid			(onfftotinputFormBean.getuid_on);
        terminalFormBean.setOnfftid_nm		(onfftotinputFormBean.getUid_onfftid_nm());
        terminalFormBean.setOnffmerch_no(strOnffmerch_no);
        //terminalFormBean.setMerch_nm(onfftotinputFormBean);
        terminalFormBean.setSvc_stat			(onfftotinputFormBean.getUid_svc_stat());
        //terminalFormBean.setSvc_stat_nm	(onfftotinputFormBean);
        terminalFormBean.setPay_mtd_seq(onfftotinputFormBean.getUid_pay_mtd_seq());
        //terminalFormBean.setPay_mtd_nm(onfftotinputFormBean);
        terminalFormBean.setMemo			(onfftotinputFormBean.getUid_memo());
        terminalFormBean.setDel_flag			(onfftotinputFormBean.getUid_del_flag());
        terminalFormBean.setCert_type(onfftotinputFormBean.getUid_cert_type());
        //terminalFormBean.setCert_type_nm(onfftotinputFormBean);
        terminalFormBean.setCncl_auth(onfftotinputFormBean.getUid_cncl_auth());
       terminalFormBean.setIns_dt     (onfftotinputFormBean.getIns_dt());     
       terminalFormBean.setMod_dt     (onfftotinputFormBean.getMod_dt());     
       terminalFormBean.setIns_user   (onfftotinputFormBean.getIns_user());     
       terminalFormBean.setMod_user   (onfftotinputFormBean.getMod_user());       


        //메인정보 입력
        result = this.terminalDAO.terminalMasterInsert(terminalFormBean);
        
        String strterminalDtlInfo = onfftotinputFormBean.getCommissioninfo();
        
        logger.trace("strterminalDtlInfo :" + strterminalDtlInfo);
        
        if(strterminalDtlInfo != null && !strterminalDtlInfo.equals(""))
        {
            String[] rows = strterminalDtlInfo.split("]]]");
            
            for(int x=0 ; x < rows.length ; x++)
            {
                String[] cols = rows[x].split("```");
                

                TmlCmsFormBean insdtlobj = new TmlCmsFormBean();
                
                insdtlobj.setOnfftid(strOnfftid);
                insdtlobj.setCommission(cols[0]);
                insdtlobj.setStart_dt(cols[1]);
                insdtlobj.setEnd_dt(cols[2]);
                insdtlobj.setMemo(cols[3]);
                insdtlobj.setIns_user(terminalFormBean.getIns_user());
                insdtlobj.setMod_user(terminalFormBean.getIns_user());
                
                this.terminalDAO.terminalDtlInsert(insdtlobj);
            }
        }
        logger.debug("------cid result : "+ result);
        //user정보 입력
        logger.debug("------user insert -----------");
        String strUserInfo = onfftotinputFormBean.getUser_info();
        
        logger.trace("strUserInfo :" + strUserInfo);
        
        if(strUserInfo != null && !strUserInfo.equals(""))
        {
            String[] rows = strUserInfo.split("]]]");
            
            for(int x=0 ; x < rows.length ; x++)
            {
                String[] cols = rows[x].split("```");
                
                UserFormBean insuserobj = new UserFormBean();
                ////"<center>ID</center>,<center>암호</center>,<center>이름</center>,<center>사용자유형명</center>,<center>사용자유형</center>";
                insuserobj.setUser_id(cols[0]);
                insuserobj.setUser_pwd(cols[1]);
                insuserobj.setUser_nm(cols[2]);
                insuserobj.setUser_cate(cols[3]);
                
                String strtpCate = cols[3];
                
                //정산관리자
                if(strtpCate.equals("01"))
                {
                    insuserobj.setOnffmerch_no(strOnffmerch_no);
                }
                //결제관리자
                else if(strtpCate.equals("02"))
                {
                    insuserobj.setOnffmerch_no(strOnffmerch_no);
                    insuserobj.setOnfftid(strOnfftid);
                }
                
                insuserobj.setIns_user(onfftotinputFormBean.getIns_user());
                insuserobj.setMod_user(onfftotinputFormBean.getIns_user());

                this.userDAO.userMasterInsert(insuserobj);
            }
        }
        
        logger.debug("------user insert end -----------");
        
        return result.toString();
    }
    
    @Override
    public String getInputBizNoChk( OnoffTotInpuFormBean onfftotinputFormBean) throws Exception {
        
        String strReturnval = this.onfftotinputDAO.getInputBizNoChk(onfftotinputFormBean);
        
        return strReturnval;
    }    
    
    @Override
    public String getInputLoginIdChk( OnoffTotInpuFormBean onfftotinputFormBean) throws Exception {

        String strReturnval = this.onfftotinputDAO.getInputLoginIdChk(onfftotinputFormBean);
        
        return strReturnval;
    }
    
}
