/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.onfftotinput.Bean;

import com.onoffkorea.system.common.util.CommonSortableListPagingForm;

/**
 *
 * @author Administrator
 */
public class OnoffTotInpuFormBean extends CommonSortableListPagingForm{
    //회사정보
    private String comp_seq   ;
    private String comp_nm    ;
    private String comp_biz_no     ;
    private String comp_corp_no    ;
    private String comp_ceo_nm;
    private String comp_tel1  ;
    private String comp_tel2  ;
    private String comp_use_flag   ;
    private String comp_use_flag_nm;

    private String comp_cate  ;
    private String comp_zip_cd     ;
    private String comp_addr_1     ;
    private String comp_addr_2     ;
    private String comp_user_seq   ;
    private String comp_del_flag;
    private String comp_memo;
    private String comp_biz_cate;  
    private String comp_biz_type;   
    private String comp_tax_flag;
    private String comp_tax_flag_nm;
    private String comp_email;    


    //지급ID 등록정보
    private String midinfo_onffmerch_no;
    private String midinfo_merch_nm;
    private String midinfo_tel_1;
    private String midinfo_tel_2;
    private String midinfo_email;
    private String midinfo_zip_cd;
    private String midinfo_addr_1;
    private String midinfo_addr_2;
    private String midinfo_svc_stat;
    private String midinfo_svc_stat_nm;
    private String midinfo_merch_cate;
    private String midinfo_merch_cate_nm;
    private String midinfo_biz_type;
    private String midinfo_biz_type_nm;
    private String midinfo_biz_cate;
    private String midinfo_biz_cate_nm;
    private String midinfo_tax_flag;
    private String midinfo_tax_flag_nm;
    private String midinfo_close_dt;
    private String midinfo_close_user;
    private String midinfo_close_reson;
    private String midinfo_commision;
    private String midinfo_bal_period;
    private String midinfo_bal_period_nm;
    private String midinfo_pay_dt_cd_1;
    private String midinfo_pay_dt_cd_1_nm;
    private String midinfo_pay_dt_cd_2;
    private String midinfo_pay_dt_cd_2_nm;
    private String midinfo_bank_cd;
    private String midinfo_bank_cd_nm;
    private String midinfo_acc_no;
    private String midinfo_acc_nm;
    private String midinfo_memo;
    private String midinfo_agent_commission;
    private String midinfo_org_seq;
    private String midinfo_agent_seq;
    private String midinfo_agent_nm;
    private String midinfo_del_flag;
    private String midinfo_app_chk_amt;
    private String midinfo_pay_dt;
    private String midinfo_pay_chn_info;
    private String midinfo_cncl_auth;
    private String midinfo_cncl_auth_nm;
    private String midinfo_agentsearchflag;
    private String midinfo_appreq_chk_amt;
    private String security_info;

    
    //UID정보
    private String uid_pay_chn_cate;
    private String uid_pay_chn_cate_nm;
    private String uid_onfftid			;
    private String uid_onfftid_nm		;
    private String uid_onffmerch_no;
    private String uid_merch_nm;
    private String uid_svc_stat			;
    private String uid_svc_stat_nm	;
    private String uid_pay_mtd_seq;
    private String uid_pay_mtd_nm;
    private String uid_memo			;
    private String uid_del_flag			;
    private String uid_cert_type;
    private String uid_cert_type_nm;
    private String uid_cncl_auth;
    private String commissioninfo;
    
    //user info
    private String user_info;
    private String userinfo_userid;

    
    private String ins_dt     ;
    private String mod_dt     ;
    private String ins_user   ;
    private String mod_user   ;
    
    
    public String getComp_seq() {
        return comp_seq;
    }

    public void setComp_seq(String comp_seq) {
        this.comp_seq = comp_seq;
    }

    public String getComp_nm() {
        return comp_nm;
    }

    public void setComp_nm(String comp_nm) {
        this.comp_nm = comp_nm;
    }

    public String getComp_biz_no() {
        return comp_biz_no;
    }

    public void setComp_biz_no(String comp_biz_no) {
        this.comp_biz_no = comp_biz_no;
    }

    public String getComp_corp_no() {
        return comp_corp_no;
    }

    public void setComp_corp_no(String comp_corp_no) {
        this.comp_corp_no = comp_corp_no;
    }

    public String getComp_ceo_nm() {
        return comp_ceo_nm;
    }

    public void setComp_ceo_nm(String comp_ceo_nm) {
        this.comp_ceo_nm = comp_ceo_nm;
    }

    public String getComp_tel1() {
        return comp_tel1;
    }

    public void setComp_tel1(String comp_tel1) {
        this.comp_tel1 = comp_tel1;
    }

    public String getComp_tel2() {
        return comp_tel2;
    }

    public void setComp_tel2(String comp_tel2) {
        this.comp_tel2 = comp_tel2;
    }

    public String getComp_use_flag() {
        return comp_use_flag;
    }

    public void setComp_use_flag(String comp_use_flag) {
        this.comp_use_flag = comp_use_flag;
    }

    public String getComp_use_flag_nm() {
        return comp_use_flag_nm;
    }

    public void setComp_use_flag_nm(String comp_use_flag_nm) {
        this.comp_use_flag_nm = comp_use_flag_nm;
    }

    public String getComp_zip_cd() {
        return comp_zip_cd;
    }

    public void setComp_zip_cd(String comp_zip_cd) {
        this.comp_zip_cd = comp_zip_cd;
    }

    public String getComp_addr_1() {
        return comp_addr_1;
    }

    public void setComp_addr_1(String comp_addr_1) {
        this.comp_addr_1 = comp_addr_1;
    }

    public String getComp_addr_2() {
        return comp_addr_2;
    }

    public void setComp_addr_2(String comp_addr_2) {
        this.comp_addr_2 = comp_addr_2;
    }

    public String getComp_user_seq() {
        return comp_user_seq;
    }

    public void setComp_user_seq(String comp_user_seq) {
        this.comp_user_seq = comp_user_seq;
    }


    public String getComp_del_flag() {
        return comp_del_flag;
    }

    public void setComp_del_flag(String comp_del_flag) {
        this.comp_del_flag = comp_del_flag;
    }

    public String getComp_memo() {
        return comp_memo;
    }

    public void setComp_memo(String comp_memo) {
        this.comp_memo = comp_memo;
    }

    public String getComp_biz_cate() {
        return comp_biz_cate;
    }

    public void setComp_biz_cate(String comp_biz_cate) {
        this.comp_biz_cate = comp_biz_cate;
    }

    public String getComp_biz_type() {
        return comp_biz_type;
    }

    public void setComp_biz_type(String comp_biz_type) {
        this.comp_biz_type = comp_biz_type;
    }

    public String getComp_tax_flag() {
        return comp_tax_flag;
    }

    public void setComp_tax_flag(String comp_tax_flag) {
        this.comp_tax_flag = comp_tax_flag;
    }

    public String getComp_tax_flag_nm() {
        return comp_tax_flag_nm;
    }

    public void setComp_tax_flag_nm(String comp_tax_flag_nm) {
        this.comp_tax_flag_nm = comp_tax_flag_nm;
    }

    public String getComp_email() {
        return comp_email;
    }

    public void setComp_email(String comp_email) {
        this.comp_email = comp_email;
    }

    public String getMidinfo_onffmerch_no() {
        return midinfo_onffmerch_no;
    }

    public void setMidinfo_onffmerch_no(String midinfo_onffmerch_no) {
        this.midinfo_onffmerch_no = midinfo_onffmerch_no;
    }

    public String getMidinfo_merch_nm() {
        return midinfo_merch_nm;
    }

    public void setMidinfo_merch_nm(String midinfo_merch_nm) {
        this.midinfo_merch_nm = midinfo_merch_nm;
    }

    public String getMidinfo_tel_1() {
        return midinfo_tel_1;
    }

    public void setMidinfo_tel_1(String midinfo_tel_1) {
        this.midinfo_tel_1 = midinfo_tel_1;
    }

    public String getMidinfo_tel_2() {
        return midinfo_tel_2;
    }

    public void setMidinfo_tel_2(String midinfo_tel_2) {
        this.midinfo_tel_2 = midinfo_tel_2;
    }

    public String getMidinfo_email() {
        return midinfo_email;
    }

    public void setMidinfo_email(String midinfo_email) {
        this.midinfo_email = midinfo_email;
    }

    public String getMidinfo_zip_cd() {
        return midinfo_zip_cd;
    }

    public void setMidinfo_zip_cd(String midinfo_zip_cd) {
        this.midinfo_zip_cd = midinfo_zip_cd;
    }

    public String getMidinfo_addr_1() {
        return midinfo_addr_1;
    }

    public void setMidinfo_addr_1(String midinfo_addr_1) {
        this.midinfo_addr_1 = midinfo_addr_1;
    }

    public String getMidinfo_addr_2() {
        return midinfo_addr_2;
    }

    public void setMidinfo_addr_2(String midinfo_addr_2) {
        this.midinfo_addr_2 = midinfo_addr_2;
    }

    public String getMidinfo_svc_stat() {
        return midinfo_svc_stat;
    }

    public void setMidinfo_svc_stat(String midinfo_svc_stat) {
        this.midinfo_svc_stat = midinfo_svc_stat;
    }

    public String getMidinfo_svc_stat_nm() {
        return midinfo_svc_stat_nm;
    }

    public void setMidinfo_svc_stat_nm(String midinfo_svc_stat_nm) {
        this.midinfo_svc_stat_nm = midinfo_svc_stat_nm;
    }

    public String getMidinfo_merch_cate() {
        return midinfo_merch_cate;
    }

    public void setMidinfo_merch_cate(String midinfo_merch_cate) {
        this.midinfo_merch_cate = midinfo_merch_cate;
    }

    public String getMidinfo_merch_cate_nm() {
        return midinfo_merch_cate_nm;
    }

    public void setMidinfo_merch_cate_nm(String midinfo_merch_cate_nm) {
        this.midinfo_merch_cate_nm = midinfo_merch_cate_nm;
    }

    public String getMidinfo_biz_type() {
        return midinfo_biz_type;
    }

    public void setMidinfo_biz_type(String midinfo_biz_type) {
        this.midinfo_biz_type = midinfo_biz_type;
    }

    public String getMidinfo_biz_type_nm() {
        return midinfo_biz_type_nm;
    }

    public void setMidinfo_biz_type_nm(String midinfo_biz_type_nm) {
        this.midinfo_biz_type_nm = midinfo_biz_type_nm;
    }

    public String getMidinfo_biz_cate() {
        return midinfo_biz_cate;
    }

    public void setMidinfo_biz_cate(String midinfo_biz_cate) {
        this.midinfo_biz_cate = midinfo_biz_cate;
    }

    public String getMidinfo_biz_cate_nm() {
        return midinfo_biz_cate_nm;
    }

    public void setMidinfo_biz_cate_nm(String midinfo_biz_cate_nm) {
        this.midinfo_biz_cate_nm = midinfo_biz_cate_nm;
    }

    public String getMidinfo_tax_flag() {
        return midinfo_tax_flag;
    }

    public void setMidinfo_tax_flag(String midinfo_tax_flag) {
        this.midinfo_tax_flag = midinfo_tax_flag;
    }

    public String getMidinfo_tax_flag_nm() {
        return midinfo_tax_flag_nm;
    }

    public void setMidinfo_tax_flag_nm(String midinfo_tax_flag_nm) {
        this.midinfo_tax_flag_nm = midinfo_tax_flag_nm;
    }

    public String getMidinfo_close_dt() {
        return midinfo_close_dt;
    }

    public void setMidinfo_close_dt(String midinfo_close_dt) {
        this.midinfo_close_dt = midinfo_close_dt;
    }

    public String getMidinfo_close_user() {
        return midinfo_close_user;
    }

    public void setMidinfo_close_user(String midinfo_close_user) {
        this.midinfo_close_user = midinfo_close_user;
    }

    public String getMidinfo_close_reson() {
        return midinfo_close_reson;
    }

    public void setMidinfo_close_reson(String midinfo_close_reson) {
        this.midinfo_close_reson = midinfo_close_reson;
    }

    public String getMidinfo_commision() {
        return midinfo_commision;
    }

    public void setMidinfo_commision(String midinfo_commision) {
        this.midinfo_commision = midinfo_commision;
    }

    public String getMidinfo_bal_period() {
        return midinfo_bal_period;
    }

    public void setMidinfo_bal_period(String midinfo_bal_period) {
        this.midinfo_bal_period = midinfo_bal_period;
    }

    public String getMidinfo_bal_period_nm() {
        return midinfo_bal_period_nm;
    }

    public void setMidinfo_bal_period_nm(String midinfo_bal_period_nm) {
        this.midinfo_bal_period_nm = midinfo_bal_period_nm;
    }

    public String getMidinfo_pay_dt_cd_1() {
        return midinfo_pay_dt_cd_1;
    }

    public void setMidinfo_pay_dt_cd_1(String midinfo_pay_dt_cd_1) {
        this.midinfo_pay_dt_cd_1 = midinfo_pay_dt_cd_1;
    }

    public String getMidinfo_pay_dt_cd_1_nm() {
        return midinfo_pay_dt_cd_1_nm;
    }

    public void setMidinfo_pay_dt_cd_1_nm(String midinfo_pay_dt_cd_1_nm) {
        this.midinfo_pay_dt_cd_1_nm = midinfo_pay_dt_cd_1_nm;
    }

    public String getMidinfo_pay_dt_cd_2() {
        return midinfo_pay_dt_cd_2;
    }

    public void setMidinfo_pay_dt_cd_2(String midinfo_pay_dt_cd_2) {
        this.midinfo_pay_dt_cd_2 = midinfo_pay_dt_cd_2;
    }

    public String getMidinfo_pay_dt_cd_2_nm() {
        return midinfo_pay_dt_cd_2_nm;
    }

    public void setMidinfo_pay_dt_cd_2_nm(String midinfo_pay_dt_cd_2_nm) {
        this.midinfo_pay_dt_cd_2_nm = midinfo_pay_dt_cd_2_nm;
    }

    public String getMidinfo_bank_cd() {
        return midinfo_bank_cd;
    }

    public void setMidinfo_bank_cd(String midinfo_bank_cd) {
        this.midinfo_bank_cd = midinfo_bank_cd;
    }

    public String getMidinfo_bank_cd_nm() {
        return midinfo_bank_cd_nm;
    }

    public void setMidinfo_bank_cd_nm(String midinfo_bank_cd_nm) {
        this.midinfo_bank_cd_nm = midinfo_bank_cd_nm;
    }

    public String getMidinfo_acc_no() {
        return midinfo_acc_no;
    }

    public void setMidinfo_acc_no(String midinfo_acc_no) {
        this.midinfo_acc_no = midinfo_acc_no;
    }

    public String getMidinfo_acc_nm() {
        return midinfo_acc_nm;
    }

    public void setMidinfo_acc_nm(String midinfo_acc_nm) {
        this.midinfo_acc_nm = midinfo_acc_nm;
    }

    public String getMidinfo_memo() {
        return midinfo_memo;
    }

    public void setMidinfo_memo(String midinfo_memo) {
        this.midinfo_memo = midinfo_memo;
    }

    public String getMidinfo_agent_commission() {
        return midinfo_agent_commission;
    }

    public void setMidinfo_agent_commission(String midinfo_agent_commission) {
        this.midinfo_agent_commission = midinfo_agent_commission;
    }

    public String getMidinfo_org_seq() {
        return midinfo_org_seq;
    }

    public void setMidinfo_org_seq(String midinfo_org_seq) {
        this.midinfo_org_seq = midinfo_org_seq;
    }

    public String getMidinfo_agent_seq() {
        return midinfo_agent_seq;
    }

    public void setMidinfo_agent_seq(String midinfo_agent_seq) {
        this.midinfo_agent_seq = midinfo_agent_seq;
    }

    public String getMidinfo_agent_nm() {
        return midinfo_agent_nm;
    }

    public void setMidinfo_agent_nm(String midinfo_agent_nm) {
        this.midinfo_agent_nm = midinfo_agent_nm;
    }

    public String getMidinfo_del_flag() {
        return midinfo_del_flag;
    }

    public void setMidinfo_del_flag(String midinfo_del_flag) {
        this.midinfo_del_flag = midinfo_del_flag;
    }

    public String getMidinfo_app_chk_amt() {
        return midinfo_app_chk_amt;
    }

    public void setMidinfo_app_chk_amt(String midinfo_app_chk_amt) {
        this.midinfo_app_chk_amt = midinfo_app_chk_amt;
    }

    public String getMidinfo_pay_dt() {
        return midinfo_pay_dt;
    }

    public void setMidinfo_pay_dt(String midinfo_pay_dt) {
        this.midinfo_pay_dt = midinfo_pay_dt;
    }

    public String getMidinfo_pay_chn_info() {
        return midinfo_pay_chn_info;
    }

    public void setMidinfo_pay_chn_info(String midinfo_pay_chn_info) {
        this.midinfo_pay_chn_info = midinfo_pay_chn_info;
    }

    public String getMidinfo_cncl_auth() {
        return midinfo_cncl_auth;
    }

    public void setMidinfo_cncl_auth(String midinfo_cncl_auth) {
        this.midinfo_cncl_auth = midinfo_cncl_auth;
    }

    public String getMidinfo_cncl_auth_nm() {
        return midinfo_cncl_auth_nm;
    }

    public void setMidinfo_cncl_auth_nm(String midinfo_cncl_auth_nm) {
        this.midinfo_cncl_auth_nm = midinfo_cncl_auth_nm;
    }

    public String getMidinfo_agentsearchflag() {
        return midinfo_agentsearchflag;
    }

    public void setMidinfo_agentsearchflag(String midinfo_agentsearchflag) {
        this.midinfo_agentsearchflag = midinfo_agentsearchflag;
    }

    public String getMidinfo_appreq_chk_amt() {
        return midinfo_appreq_chk_amt;
    }

    public void setMidinfo_appreq_chk_amt(String midinfo_appreq_chk_amt) {
        this.midinfo_appreq_chk_amt = midinfo_appreq_chk_amt;
    }

    public String getUid_pay_chn_cate() {
        return uid_pay_chn_cate;
    }

    public void setUid_pay_chn_cate(String uid_pay_chn_cate) {
        this.uid_pay_chn_cate = uid_pay_chn_cate;
    }

    public String getUid_pay_chn_cate_nm() {
        return uid_pay_chn_cate_nm;
    }

    public void setUid_pay_chn_cate_nm(String uid_pay_chn_cate_nm) {
        this.uid_pay_chn_cate_nm = uid_pay_chn_cate_nm;
    }

    public String getUid_onfftid() {
        return uid_onfftid;
    }

    public void setUid_onfftid(String uid_onfftid) {
        this.uid_onfftid = uid_onfftid;
    }

    public String getUid_onfftid_nm() {
        return uid_onfftid_nm;
    }

    public void setUid_onfftid_nm(String uid_onfftid_nm) {
        this.uid_onfftid_nm = uid_onfftid_nm;
    }

    public String getUid_onffmerch_no() {
        return uid_onffmerch_no;
    }

    public void setUid_onffmerch_no(String uid_onffmerch_no) {
        this.uid_onffmerch_no = uid_onffmerch_no;
    }

    public String getUid_merch_nm() {
        return uid_merch_nm;
    }

    public void setUid_merch_nm(String uid_merch_nm) {
        this.uid_merch_nm = uid_merch_nm;
    }

    public String getUid_svc_stat() {
        return uid_svc_stat;
    }

    public void setUid_svc_stat(String uid_svc_stat) {
        this.uid_svc_stat = uid_svc_stat;
    }

    public String getUid_svc_stat_nm() {
        return uid_svc_stat_nm;
    }

    public void setUid_svc_stat_nm(String uid_svc_stat_nm) {
        this.uid_svc_stat_nm = uid_svc_stat_nm;
    }

    public String getUid_pay_mtd_seq() {
        return uid_pay_mtd_seq;
    }

    public void setUid_pay_mtd_seq(String uid_pay_mtd_seq) {
        this.uid_pay_mtd_seq = uid_pay_mtd_seq;
    }

    public String getUid_pay_mtd_nm() {
        return uid_pay_mtd_nm;
    }

    public void setUid_pay_mtd_nm(String uid_pay_mtd_nm) {
        this.uid_pay_mtd_nm = uid_pay_mtd_nm;
    }

    public String getUid_memo() {
        return uid_memo;
    }

    public void setUid_memo(String uid_memo) {
        this.uid_memo = uid_memo;
    }

    public String getUid_del_flag() {
        return uid_del_flag;
    }

    public void setUid_del_flag(String uid_del_flag) {
        this.uid_del_flag = uid_del_flag;
    }

    public String getUid_cert_type() {
        return uid_cert_type;
    }

    public void setUid_cert_type(String uid_cert_type) {
        this.uid_cert_type = uid_cert_type;
    }

    public String getUid_cert_type_nm() {
        return uid_cert_type_nm;
    }

    public void setUid_cert_type_nm(String uid_cert_type_nm) {
        this.uid_cert_type_nm = uid_cert_type_nm;
    }

    public String getUid_cncl_auth() {
        return uid_cncl_auth;
    }

    public void setUid_cncl_auth(String uid_cncl_auth) {
        this.uid_cncl_auth = uid_cncl_auth;
    }

    public String getSecurity_info() {
        return security_info;
    }

    public void setSecurity_info(String security_info) {
        this.security_info = security_info;
    }

    public String getCommissioninfo() {
        return commissioninfo;
    }

    public void setCommissioninfo(String commissioninfo) {
        this.commissioninfo = commissioninfo;
    }

    public String getUser_info() {
        return user_info;
    }

    public void setUser_info(String user_info) {
        this.user_info = user_info;
    }

    public String getUserinfo_userid() {
        return userinfo_userid;
    }

    public void setUserinfo_userid(String userinfo_userid) {
        this.userinfo_userid = userinfo_userid;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    public String getComp_cate() {
        return comp_cate;
    }

    public void setComp_cate(String comp_cate) {
        this.comp_cate = comp_cate;
    }

    @Override
    public String toString() {
        return "OnoffTotInpuFormBean{" + "comp_seq=" + comp_seq + ", comp_nm=" + comp_nm + ", comp_biz_no=" + comp_biz_no + ", comp_corp_no=" + comp_corp_no + ", comp_ceo_nm=" + comp_ceo_nm + ", comp_tel1=" + comp_tel1 + ", comp_tel2=" + comp_tel2 + ", comp_use_flag=" + comp_use_flag + ", comp_use_flag_nm=" + comp_use_flag_nm + ", comp_cate=" + comp_cate + ", comp_zip_cd=" + comp_zip_cd + ", comp_addr_1=" + comp_addr_1 + ", comp_addr_2=" + comp_addr_2 + ", comp_user_seq=" + comp_user_seq + ", comp_del_flag=" + comp_del_flag + ", comp_memo=" + comp_memo + ", comp_biz_cate=" + comp_biz_cate + ", comp_biz_type=" + comp_biz_type + ", comp_tax_flag=" + comp_tax_flag + ", comp_tax_flag_nm=" + comp_tax_flag_nm + ", comp_email=" + comp_email + ", midinfo_onffmerch_no=" + midinfo_onffmerch_no + ", midinfo_merch_nm=" + midinfo_merch_nm + ", midinfo_tel_1=" + midinfo_tel_1 + ", midinfo_tel_2=" + midinfo_tel_2 + ", midinfo_email=" + midinfo_email + ", midinfo_zip_cd=" + midinfo_zip_cd + ", midinfo_addr_1=" + midinfo_addr_1 + ", midinfo_addr_2=" + midinfo_addr_2 + ", midinfo_svc_stat=" + midinfo_svc_stat + ", midinfo_svc_stat_nm=" + midinfo_svc_stat_nm + ", midinfo_merch_cate=" + midinfo_merch_cate + ", midinfo_merch_cate_nm=" + midinfo_merch_cate_nm + ", midinfo_biz_type=" + midinfo_biz_type + ", midinfo_biz_type_nm=" + midinfo_biz_type_nm + ", midinfo_biz_cate=" + midinfo_biz_cate + ", midinfo_biz_cate_nm=" + midinfo_biz_cate_nm + ", midinfo_tax_flag=" + midinfo_tax_flag + ", midinfo_tax_flag_nm=" + midinfo_tax_flag_nm + ", midinfo_close_dt=" + midinfo_close_dt + ", midinfo_close_user=" + midinfo_close_user + ", midinfo_close_reson=" + midinfo_close_reson + ", midinfo_commision=" + midinfo_commision + ", midinfo_bal_period=" + midinfo_bal_period + ", midinfo_bal_period_nm=" + midinfo_bal_period_nm + ", midinfo_pay_dt_cd_1=" + midinfo_pay_dt_cd_1 + ", midinfo_pay_dt_cd_1_nm=" + midinfo_pay_dt_cd_1_nm + ", midinfo_pay_dt_cd_2=" + midinfo_pay_dt_cd_2 + ", midinfo_pay_dt_cd_2_nm=" + midinfo_pay_dt_cd_2_nm + ", midinfo_bank_cd=" + midinfo_bank_cd + ", midinfo_bank_cd_nm=" + midinfo_bank_cd_nm + ", midinfo_acc_no=" + midinfo_acc_no + ", midinfo_acc_nm=" + midinfo_acc_nm + ", midinfo_memo=" + midinfo_memo + ", midinfo_agent_commission=" + midinfo_agent_commission + ", midinfo_org_seq=" + midinfo_org_seq + ", midinfo_agent_seq=" + midinfo_agent_seq + ", midinfo_agent_nm=" + midinfo_agent_nm + ", midinfo_del_flag=" + midinfo_del_flag + ", midinfo_app_chk_amt=" + midinfo_app_chk_amt + ", midinfo_pay_dt=" + midinfo_pay_dt + ", midinfo_pay_chn_info=" + midinfo_pay_chn_info + ", midinfo_cncl_auth=" + midinfo_cncl_auth + ", midinfo_cncl_auth_nm=" + midinfo_cncl_auth_nm + ", midinfo_agentsearchflag=" + midinfo_agentsearchflag + ", midinfo_appreq_chk_amt=" + midinfo_appreq_chk_amt + ", security_info=" + security_info + ", uid_pay_chn_cate=" + uid_pay_chn_cate + ", uid_pay_chn_cate_nm=" + uid_pay_chn_cate_nm + ", uid_onfftid=" + uid_onfftid + ", uid_onfftid_nm=" + uid_onfftid_nm + ", uid_onffmerch_no=" + uid_onffmerch_no + ", uid_merch_nm=" + uid_merch_nm + ", uid_svc_stat=" + uid_svc_stat + ", uid_svc_stat_nm=" + uid_svc_stat_nm + ", uid_pay_mtd_seq=" + uid_pay_mtd_seq + ", uid_pay_mtd_nm=" + uid_pay_mtd_nm + ", uid_memo=" + uid_memo + ", uid_del_flag=" + uid_del_flag + ", uid_cert_type=" + uid_cert_type + ", uid_cert_type_nm=" + uid_cert_type_nm + ", uid_cncl_auth=" + uid_cncl_auth + ", commissioninfo=" + commissioninfo + ", user_info=" + user_info + ", userinfo_userid=" + userinfo_userid + ", ins_dt=" + ins_dt + ", mod_dt=" + mod_dt + ", ins_user=" + ins_user + ", mod_user=" + mod_user + '}';
    }

    

}
