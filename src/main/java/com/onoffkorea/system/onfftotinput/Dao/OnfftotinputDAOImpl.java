/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.onfftotinput.Dao;

import com.onoffkorea.system.company.Bean.CompanyFormBean;
import com.onoffkorea.system.onfftotinput.Bean.OnoffTotInpuFormBean;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("onfftotinputDAO")
public class OnfftotinputDAOImpl extends SqlSessionDaoSupport  implements OnfftotinputDAO {
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Override
    public String getInputBizNoChk(OnoffTotInpuFormBean onfftotinputFormBean) {
        return (String) getSqlSession().selectOne("OnffTotInput.getInputBizNoChk", onfftotinputFormBean);
    }

    @Override
    public String getInputLoginIdChk(OnoffTotInpuFormBean onfftotinputFormBean) {
        return (String) getSqlSession().selectOne("OnffTotInput.getInputLoginIdChk", onfftotinputFormBean);
    }
    
    @Override
    public String getCompInfoSeq(String param)
    {
        return (String) getSqlSession().selectOne("OnffTotInput.getCompInfoSeq", param);
    }
   
    @Override
    public Integer companyMasterInsert(CompanyFormBean companyFormBean) {
        Integer result = getSqlSession().insert("OnffTotInput.companyMasterInsert", companyFormBean);
        return result;
        
    }    

}
