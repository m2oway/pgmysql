/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.onfftotinput.Dao;

import com.onoffkorea.system.company.Bean.CompanyFormBean;
import com.onoffkorea.system.onfftotinput.Bean.OnoffTotInpuFormBean;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface OnfftotinputDAO {
    
  public String getInputBizNoChk(OnoffTotInpuFormBean onfftotinputFormBean);
  
  public String getInputLoginIdChk(OnoffTotInpuFormBean onfftotinputFormBean);
  
  public String getCompInfoSeq(String param);
  
  public Integer companyMasterInsert(CompanyFormBean companyFormBean);
  
}
