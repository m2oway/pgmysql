/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.acq.Dao;

import com.onoffkorea.system.acq.Bean.AcqFormBean;
import com.onoffkorea.system.trans.Vo.Tb_Tran_Cardpg;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("acqDAO")
public class AcqDAOImpl extends SqlSessionDaoSupport implements AcqDAO{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    //미매입내역 집계 정보 조회
    @Override
    public List<Tb_Tran_Cardpg> acqMasterList(AcqFormBean acqFormBean) {
        
        return (List) getSqlSession().selectList("Acq.acqMasterList", acqFormBean);
        
    }

    //미매입내역 집계 엑셀다운로드
    @Override
    public List acqMasterListExcel(AcqFormBean acqFormBean) {
        
        return (List)getSqlSession().selectList("Acq.acqMasterListExcel",acqFormBean);
        
    }
    
}
