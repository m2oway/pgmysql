/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.acq.Dao;

import com.onoffkorea.system.acq.Bean.AcqFormBean;
import com.onoffkorea.system.trans.Vo.Tb_Tran_Cardpg;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface AcqDAO {

    public List<Tb_Tran_Cardpg> acqMasterList(AcqFormBean acqFormBean);

    public List acqMasterListExcel(AcqFormBean acqFormBean);
    
}
