/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.acq.Vo;

/**
 *
 * @author Administrator
 */
public class Tb_Acq {
    private String division;
    private String non_acc_sum;
    private String acc_sum;
    private String non_acc_cnt;
    private String acc_cnt;
    private String user_seq;
    private String file_dd;
    private String exp_dd;
    private String mid;
    private String req_cnt;
    private String req_amt;
    private String ban_cnt;
    private String ban_amt;
    private String def_cnt;
    private String def_amt;
    private String dec_cnt;
    private String dec_amt;
    private String tot_cnt;
    private String tot_amt;
    private String exp_amt;
    private String inp_amt;
    private String account_no;
    private String ins_dt;
    private String mod_dt;
    private String ins_user;
    private String mod_user;
    private String seq_van;       
    private String comp_seq;      
    private String org_seq;       
    private String tid;           
    private String tran_seq;      
    private String app_dt;       
    private String app_tm;        
    private String app_no;        
    private String tran_cate;     
    private String card_num;      
    private String currency;      
    private String tax_amt;       
    private String installment;   
    private String iss_cd;        
    private String iss_nm;        
    private String acc_cd;        
    private String acc_nm;        
    private String merch_no;      
    private String card_cate;     
    private String result_cd;     
    private String result_msg;    
    private String org_app_dd;    
    private String org_app_no;    
    private String cncl_reason;   
    private String tran_step;     
    private String filler;        
    private String svc_amt;       
    private String free_inst_flag;
    private String tran_dt;       
    private String tran_tm;       
    private String order_seq;  
    private String filler2;  
    private String term_filler1;  
    private String term_filler2;  
    private String term_filler3;  
    private String term_filler4;  
    private String term_filler5;  
    private String term_filler6;  
    private String term_filler7;  
    private String term_filler8;  
    private String term_filler9; 
    private String term_filler10; 
    private String trad_chk_flag; 
    private String acc_chk_flag;  
    private String dep_seq;        
    private String tran_no;        
    private String pay_dt;         
    private String acq_dt;         
    private String recode_cate;    
    private String sales_dt;       
    private String sale_amt;       
    private String card_reject_cd; 
    private String van_reject_cd;  
    private String commision;      
    private String file_nm;  
    private String comp_nm;
    private String org_nm;
    private String acq_nm;
    private String acq_no;
    private String acq_chk_flag;
    private String app_dt_start;
    private String app_dt_end;
    private String code_nm;
    private String acq_cd;
    private String select_nm;
    private String rnum;

    public String getRnum() {
        return rnum;
    }

    public void setRnum(String rnum) {
        this.rnum = rnum;
    }
    
    
    
    public String getSelect_nm() {
        return select_nm;
    }

    public void setSelect_nm(String select_nm) {
        this.select_nm = select_nm;
    }
    
    
    
    public String getAcq_cd() {
        return acq_cd;
    }

    public void setAcq_cd(String acq_cd) {
        this.acq_cd = acq_cd;
    }



    public String getCode_nm() {
        return code_nm;
    }

    public void setCode_nm(String code_nm) {
        this.code_nm = code_nm;
    }

    public String getApp_dt_start() {
        return app_dt_start;
    }

    public void setApp_dt_start(String app_dt_start) {
        this.app_dt_start = app_dt_start;
    }

    public String getApp_dt_end() {
        return app_dt_end;
    }

    public void setApp_dt_end(String app_dt_end) {
        this.app_dt_end = app_dt_end;
    }
    
    public String getAcq_nm() {
        return acq_nm;
    }

    public void setAcq_nm(String acq_nm) {
        this.acq_nm = acq_nm;
    }

    public String getAcq_no() {
        return acq_no;
    }

    public void setAcq_no(String acq_no) {
        this.acq_no = acq_no;
    }

    public String getAcq_chk_flag() {
        return acq_chk_flag;
    }

    public void setAcq_chk_flag(String acq_chk_flag) {
        this.acq_chk_flag = acq_chk_flag;
    }

    public String getOrg_nm() {
        return org_nm;
    }

    public void setOrg_nm(String org_nm) {
        this.org_nm = org_nm;
    }
    
    
    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    
    public String getComp_nm() {
        return comp_nm;
    }

    public void setComp_nm(String comp_nm) {
        this.comp_nm = comp_nm;
    }
    
     public String getNon_acc_sum() {
        return non_acc_sum;
    }

    public void setNon_acc_sum(String non_acc_sum) {
        this.non_acc_sum = non_acc_sum;
    }

    public String getAcc_sum() {
        return acc_sum;
    }

    public void setAcc_sum(String acc_sum) {
        this.acc_sum = acc_sum;
    }

    public String getNon_acc_cnt() {
        return non_acc_cnt;
    }

    public void setNon_acc_cnt(String non_acc_cnt) {
        this.non_acc_cnt = non_acc_cnt;
    }

    public String getAcc_cnt() {
        return acc_cnt;
    }

    public void setAcc_cnt(String acc_cnt) {
        this.acc_cnt = acc_cnt;
    }
    

    public String getSeq_van() {
        return seq_van;
    }

    public void setSeq_van(String seq_van) {
        this.seq_van = seq_van;
    }

    public String getComp_seq() {
        return comp_seq;
    }

    public void setComp_seq(String comp_seq) {
        this.comp_seq = comp_seq;
    }

    public String getOrg_seq() {
        return org_seq;
    }

    public void setOrg_seq(String org_seq) {
        this.org_seq = org_seq;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getTran_seq() {
        return tran_seq;
    }

    public void setTran_seq(String tran_seq) {
        this.tran_seq = tran_seq;
    }

    public String getApp_dt() {
        return app_dt;
    }

    public void setApp_dt(String app_dt) {
        this.app_dt = app_dt;
    }

    public String getApp_tm() {
        return app_tm;
    }

    public void setApp_tm(String app_tm) {
        this.app_tm = app_tm;
    }

    public String getApp_no() {
        return app_no;
    }

    public void setApp_no(String app_no) {
        this.app_no = app_no;
    }

    public String getTran_cate() {
        return tran_cate;
    }

    public void setTran_cate(String tran_cate) {
        this.tran_cate = tran_cate;
    }

    public String getCard_num() {
        return card_num;
    }

    public void setCard_num(String card_num) {
        this.card_num = card_num;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTax_amt() {
        return tax_amt;
    }

    public void setTax_amt(String tax_amt) {
        this.tax_amt = tax_amt;
    }

    public String getInstallment() {
        return installment;
    }

    public void setInstallment(String installment) {
        this.installment = installment;
    }

    public String getIss_cd() {
        return iss_cd;
    }

    public void setIss_cd(String iss_cd) {
        this.iss_cd = iss_cd;
    }

    public String getIss_nm() {
        return iss_nm;
    }

    public void setIss_nm(String iss_nm) {
        this.iss_nm = iss_nm;
    }

    public String getAcc_cd() {
        return acc_cd;
    }

    public void setAcc_cd(String acc_cd) {
        this.acc_cd = acc_cd;
    }

    public String getAcc_nm() {
        return acc_nm;
    }

    public void setAcc_nm(String acc_nm) {
        this.acc_nm = acc_nm;
    }

    public String getMerch_no() {
        return merch_no;
    }

    public void setMerch_no(String merch_no) {
        this.merch_no = merch_no;
    }

    public String getCard_cate() {
        return card_cate;
    }

    public void setCard_cate(String card_cate) {
        this.card_cate = card_cate;
    }

    public String getResult_cd() {
        return result_cd;
    }

    public void setResult_cd(String result_cd) {
        this.result_cd = result_cd;
    }

    public String getResult_msg() {
        return result_msg;
    }

    public void setResult_msg(String result_msg) {
        this.result_msg = result_msg;
    }

    public String getOrg_app_dd() {
        return org_app_dd;
    }

    public void setOrg_app_dd(String org_app_dd) {
        this.org_app_dd = org_app_dd;
    }

    public String getOrg_app_no() {
        return org_app_no;
    }

    public void setOrg_app_no(String org_app_no) {
        this.org_app_no = org_app_no;
    }

    public String getCncl_reason() {
        return cncl_reason;
    }

    public void setCncl_reason(String cncl_reason) {
        this.cncl_reason = cncl_reason;
    }

    public String getTran_step() {
        return tran_step;
    }

    public void setTran_step(String tran_step) {
        this.tran_step = tran_step;
    }

    public String getFiller() {
        return filler;
    }

    public void setFiller(String filler) {
        this.filler = filler;
    }

    public String getSvc_amt() {
        return svc_amt;
    }

    public void setSvc_amt(String svc_amt) {
        this.svc_amt = svc_amt;
    }

    public String getFree_inst_flag() {
        return free_inst_flag;
    }

    public void setFree_inst_flag(String free_inst_flag) {
        this.free_inst_flag = free_inst_flag;
    }

    public String getTran_dt() {
        return tran_dt;
    }

    public void setTran_dt(String tran_dt) {
        this.tran_dt = tran_dt;
    }

    public String getTran_tm() {
        return tran_tm;
    }

    public void setTran_tm(String tran_tm) {
        this.tran_tm = tran_tm;
    }

    public String getOrder_seq() {
        return order_seq;
    }

    public void setOrder_seq(String order_seq) {
        this.order_seq = order_seq;
    }

    public String getFiller2() {
        return filler2;
    }

    public void setFiller2(String filler2) {
        this.filler2 = filler2;
    }

    public String getTerm_filler1() {
        return term_filler1;
    }

    public void setTerm_filler1(String term_filler1) {
        this.term_filler1 = term_filler1;
    }

    public String getTerm_filler2() {
        return term_filler2;
    }

    public void setTerm_filler2(String term_filler2) {
        this.term_filler2 = term_filler2;
    }

    public String getTerm_filler3() {
        return term_filler3;
    }

    public void setTerm_filler3(String term_filler3) {
        this.term_filler3 = term_filler3;
    }

    public String getTerm_filler4() {
        return term_filler4;
    }

    public void setTerm_filler4(String term_filler4) {
        this.term_filler4 = term_filler4;
    }

    public String getTerm_filler5() {
        return term_filler5;
    }

    public void setTerm_filler5(String term_filler5) {
        this.term_filler5 = term_filler5;
    }

    public String getTerm_filler6() {
        return term_filler6;
    }

    public void setTerm_filler6(String term_filler6) {
        this.term_filler6 = term_filler6;
    }

    public String getTerm_filler7() {
        return term_filler7;
    }

    public void setTerm_filler7(String term_filler7) {
        this.term_filler7 = term_filler7;
    }

    public String getTerm_filler8() {
        return term_filler8;
    }

    public void setTerm_filler8(String term_filler8) {
        this.term_filler8 = term_filler8;
    }

    public String getTerm_filler9() {
        return term_filler9;
    }

    public void setTerm_filler9(String term_filler9) {
        this.term_filler9 = term_filler9;
    }

    public String getTerm_filler10() {
        return term_filler10;
    }

    public void setTerm_filler10(String term_filler10) {
        this.term_filler10 = term_filler10;
    }

    public String getTrad_chk_flag() {
        return trad_chk_flag;
    }

    public void setTrad_chk_flag(String trad_chk_flag) {
        this.trad_chk_flag = trad_chk_flag;
    }

    public String getAcc_chk_flag() {
        return acc_chk_flag;
    }

    public void setAcc_chk_flag(String acc_chk_flag) {
        this.acc_chk_flag = acc_chk_flag;
    }

    public String getDep_seq() {
        return dep_seq;
    }

    public void setDep_seq(String dep_seq) {
        this.dep_seq = dep_seq;
    }

    public String getTran_no() {
        return tran_no;
    }

    public void setTran_no(String tran_no) {
        this.tran_no = tran_no;
    }

    public String getPay_dt() {
        return pay_dt;
    }

    public void setPay_dt(String pay_dt) {
        this.pay_dt = pay_dt;
    }

    public String getAcq_dt() {
        return acq_dt;
    }

    public void setAcq_dt(String acq_dt) {
        this.acq_dt = acq_dt;
    }

    public String getRecode_cate() {
        return recode_cate;
    }

    public void setRecode_cate(String recode_cate) {
        this.recode_cate = recode_cate;
    }

    public String getSales_dt() {
        return sales_dt;
    }

    public void setSales_dt(String sales_dt) {
        this.sales_dt = sales_dt;
    }

    public String getSale_amt() {
        return sale_amt;
    }

    public void setSale_amt(String sale_amt) {
        this.sale_amt = sale_amt;
    }

    public String getCard_reject_cd() {
        return card_reject_cd;
    }

    public void setCard_reject_cd(String card_reject_cd) {
        this.card_reject_cd = card_reject_cd;
    }

    public String getVan_reject_cd() {
        return van_reject_cd;
    }

    public void setVan_reject_cd(String van_reject_cd) {
        this.van_reject_cd = van_reject_cd;
    }

    public String getCommision() {
        return commision;
    }

    public void setCommision(String commision) {
        this.commision = commision;
    }

    public String getFile_nm() {
        return file_nm;
    }

    public void setFile_nm(String file_nm) {
        this.file_nm = file_nm;
    }


    

    
    public String getUser_seq() {
        return user_seq;
    }

    public void setUser_seq(String user_seq) {
        this.user_seq = user_seq;
    }

    public String getFile_dd() {
        return file_dd;
    }

    public void setFile_dd(String file_dd) {
        this.file_dd = file_dd;
    }

    public String getExp_dd() {
        return exp_dd;
    }

    public void setExp_dd(String exp_dd) {
        this.exp_dd = exp_dd;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getReq_cnt() {
        return req_cnt;
    }

    public void setReq_cnt(String req_cnt) {
        this.req_cnt = req_cnt;
    }

    public String getReq_amt() {
        return req_amt;
    }

    public void setReq_amt(String req_amt) {
        this.req_amt = req_amt;
    }

    public String getBan_cnt() {
        return ban_cnt;
    }

    public void setBan_cnt(String ban_cnt) {
        this.ban_cnt = ban_cnt;
    }

    public String getBan_amt() {
        return ban_amt;
    }

    public void setBan_amt(String ban_amt) {
        this.ban_amt = ban_amt;
    }

    public String getDef_cnt() {
        return def_cnt;
    }

    public void setDef_cnt(String def_cnt) {
        this.def_cnt = def_cnt;
    }

    public String getDef_amt() {
        return def_amt;
    }

    public void setDef_amt(String def_amt) {
        this.def_amt = def_amt;
    }

    public String getDec_cnt() {
        return dec_cnt;
    }

    public void setDec_cnt(String dec_cnt) {
        this.dec_cnt = dec_cnt;
    }

    public String getDec_amt() {
        return dec_amt;
    }

    public void setDec_amt(String dec_amt) {
        this.dec_amt = dec_amt;
    }

    public String getTot_cnt() {
        return tot_cnt;
    }

    public void setTot_cnt(String tot_cnt) {
        this.tot_cnt = tot_cnt;
    }

    public String getTot_amt() {
        return tot_amt;
    }

    public void setTot_amt(String tot_amt) {
        this.tot_amt = tot_amt;
    }

    public String getExp_amt() {
        return exp_amt;
    }

    public void setExp_amt(String exp_amt) {
        this.exp_amt = exp_amt;
    }

    public String getInp_amt() {
        return inp_amt;
    }

    public void setInp_amt(String inp_amt) {
        this.inp_amt = inp_amt;
    }

    public String getAccount_no() {
        return account_no;
    }

    public void setAccount_no(String account_no) {
        this.account_no = account_no;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }
}
