/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.acq.Controller;

import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import com.onoffkorea.system.acq.Bean.AcqFormBean;
import com.onoffkorea.system.acq.Service.AcqService;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.trans.Bean.TransFormBean;
import com.onoffkorea.system.trans.Service.TransService;
import common.web.servlet.BaseSpringMultiActionController;
import java.util.Hashtable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/acq/**")
public class AcqController extends BaseSpringMultiActionController{
        
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private AcqService acqService;
    
    @Autowired
    private TransService transService;        
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }    
    
    //미매입 정보 조회
    @RequestMapping(method= RequestMethod.GET,value = "/acqMasterList")
    public String acqMasterFormList(@Valid AcqFormBean acqFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_acqMasterFormList = acqService.acqMasterList(acqFormBean);
        String ls_acqMasterFormList = (String) ht_acqMasterFormList.get("ResultSet");
        
        model.addAttribute("ls_acqMasterFormList",ls_acqMasterFormList);     
        
        return null;
        
    }

    //미매입  정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/acqMasterList")
    public @ResponseBody String acqMasterList(@Valid AcqFormBean acqFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
        
        Hashtable ht_acqMasterFormList = acqService.acqMasterList(acqFormBean);
        String ls_acqMasterFormList = (String) ht_acqMasterFormList.get("ResultSet");
        
        return ls_acqMasterFormList;
        
    }
    
    //미매입 상세 정보 조회
    @RequestMapping(method= RequestMethod.GET,value = "/acqMasterDetail")
    public String acqDetailFormList(@Valid AcqFormBean acqFormBean, TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                
        
        transFormBean.setApp_dt(acqFormBean.getApp_dt());
        transFormBean.setMerch_no(acqFormBean.getMerch_no());
        
        Hashtable ht_transDetailList = transService.transDetailList(transFormBean);
        String ls_transDetailList = (String) ht_transDetailList.get("ResultSet");
        
        model.addAttribute("ls_transDetailList",ls_transDetailList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_transDetailList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);           
        
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_transDetailList.get("resultStatusList");
        
        model.addAttribute("resultStatusList",resultStatusList);         
        
        
        Tb_Code_Main tranStatusList = (Tb_Code_Main) ht_transDetailList.get("tranStatusList");
        
        model.addAttribute("tranStatusList",tranStatusList);       
        
        Tb_Code_Main AccChkFlagList = (Tb_Code_Main) ht_transDetailList.get("AccChkFlagList");
        
        model.addAttribute("AccChkFlagList",AccChkFlagList);           
        
        return null;
        
    }

    //미매입 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/acqMasterDetail")
    public @ResponseBody String acqDetailList(@Valid AcqFormBean acqFormBean, TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
        
        transFormBean.setApp_start_dt(acqFormBean.getApp_start_dt());
        transFormBean.setApp_end_dt(acqFormBean.getApp_end_dt());
        transFormBean.setMerch_no(acqFormBean.getMerch_no());
        transFormBean.setPay_chn_cate(acqFormBean.getPay_chn_cate());
        transFormBean.setOnffmerch_no(acqFormBean.getOnffmerch_no());
        transFormBean.setMerch_nm(acqFormBean.getMerch_nm());
        transFormBean.setOnfftid(acqFormBean.getOnfftid());
        transFormBean.setOnfftid_nm(acqFormBean.getOnfftid_nm());
        transFormBean.setApp_no(acqFormBean.getApp_no());
        transFormBean.setCard_num(acqFormBean.getCard_num());
        transFormBean.setTot_amt(acqFormBean.getTot_amt());
        
        Hashtable ht_transDetailList = transService.transDetailList(transFormBean);
        String ls_transDetailList = (String) ht_transDetailList.get("ResultSet");

        return ls_transDetailList;
        
    }    
    
    //미매입 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/acqMasterListExcel")
    public void acqMasterListExcel(@Valid AcqFormBean acqFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, HttpServletResponse response) throws Exception {

        String excelDownloadFilename = new String(getMessage("acq.acqMasterListExcel.fileName")); 
          
        acqService.acqMasterListExcel(acqFormBean, excelDownloadFilename, response); 

    }    
    
}