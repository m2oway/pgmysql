/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.acq.Service;

import com.onoffkorea.system.acq.Bean.AcqFormBean;
import com.onoffkorea.system.acq.Dao.AcqDAO;
import com.onoffkorea.system.acq.Vo.Tb_Acq;
import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.trans.Vo.Tb_Tran_Cardpg;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 */
@Service("acqService")
public class AcqServiceImpl  implements AcqService{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private AcqDAO acqDAO;
    
    @Autowired
    private CommonService commonService;        

    //미매입 정보 관리
    @Override
    public Hashtable acqMasterList(AcqFormBean acqFormBean) {
        
         SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        if((acqFormBean.getApp_start_dt() == null) || (acqFormBean.getApp_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            acqFormBean.setApp_start_dt(start_dt);
            acqFormBean.setApp_end_dt(end_dt);
        }
        
        
        Hashtable ht = new Hashtable();

        try {
            
                List<Tb_Tran_Cardpg> ls_acqMasterList = acqDAO.acqMasterList(acqFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"rows\" : [");
                for (int i = 0; i < ls_acqMasterList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_acqMasterList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"app_dt\":\""+tb_Tran_Cardpg.getApp_dt()+"\"");
                        sb.append(",\"merch_no\":\""+tb_Tran_Cardpg.getMerch_no()+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Tran_Cardpg.getApp_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMis_cnt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMis_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTot_cnt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTot_amt()) + "\"");
                        
                        if (i == (ls_acqMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");       
                
            SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
            Date start_date = formatter.parse(acqFormBean.getApp_start_dt());
            Date end_date   = formatter.parse(acqFormBean.getApp_end_dt());
            
            String start_dt = formatter2.format(start_date);
            String end_dt = formatter2.format(end_date);

            acqFormBean.setApp_start_dt(start_dt);
            acqFormBean.setApp_end_dt(end_dt);
            
                ht.put("ResultSet", sb.toString());
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;         
        
    }

    //미매입내역 집계 엑셀다운로드
    @Override
    public void acqMasterListExcel(AcqFormBean acqFormBean, String excelDownloadFilename, HttpServletResponse response) {
         
        try {
             
                List ls_acqMasterListExcel = acqDAO.acqMasterListExcel(acqFormBean);
                
                StringBuffer acqMasterListExcel = Util.makeData(ls_acqMasterListExcel);
        
                Util.exceldownload(acqMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    }
    
}
