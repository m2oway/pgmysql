/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.acq.Service;

import com.onoffkorea.system.acq.Bean.AcqFormBean;
import java.util.Hashtable;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public interface AcqService {

    public Hashtable acqMasterList(AcqFormBean acqFormBean);

    public void acqMasterListExcel(AcqFormBean acqFormBean, String excelDownloadFilename, HttpServletResponse response);
    
}