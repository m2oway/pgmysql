/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.dframt.Dao;

import com.onoffkorea.system.dframt.Bean.DfrAmtFormBean;
import com.onoffkorea.system.dframt.Bean.DfrAmtMainFormBean;
import com.onoffkorea.system.dframt.Vo.Tb_Onffmerch_Dfr_Dec;
import com.onoffkorea.system.dframt.Vo.Tb_Onffmerch_Dfr_Gen;
import com.onoffkorea.system.dframt.Vo.Tb_Onffmerch_Dfr_Main;
import com.onoffkorea.system.dframt.Vo.Tb_Onffmerch_Dfr_Rel;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author MoonbongChoi
 */
@Repository("dframtDAO")
public class DfrAmtDAOImpl extends SqlSessionDaoSupport implements DfrAmtDAO
{
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Override
    public Integer dframtMasterListCount(DfrAmtMainFormBean dframtFormBean) {
        return (Integer)getSqlSession().selectOne("Dframt.dframtMasterListCount",dframtFormBean);
    }

    @Override
    public List<Tb_Onffmerch_Dfr_Main> dframtMasterList(DfrAmtMainFormBean dframtFormBean) {
          return (List)getSqlSession().selectList("Dframt.dframtMasterList",dframtFormBean);
    }
    
    
    @Override
    public List<Tb_Onffmerch_Dfr_Main> dframtMasterListExcel(DfrAmtMainFormBean dfrAmtMainFormBean) {
        return (List)getSqlSession().selectList("Dframt.dframtMasterListExcel",dfrAmtMainFormBean);
    }

    @Override
    public Integer dframtDetailListCount(DfrAmtFormBean dframtFormBean) {
        return (Integer)getSqlSession().selectOne("Dframt.dframtDetailListCount",dframtFormBean);
    }

    @Override
    public List<Tb_Onffmerch_Dfr_Gen> dframtDetailList(DfrAmtFormBean dframtFormBean) {
        return (List)getSqlSession().selectList("Dframt.dframtDetailList",dframtFormBean);
    }

    @Override
    public List<Tb_Onffmerch_Dfr_Gen> dframtDetailListExcel(DfrAmtFormBean dfrAmtFormBean) {
        return (List)getSqlSession().selectList("Dframt.dframtDetailListExcel",dfrAmtFormBean);
    }

    //보류금 등록
    @Override
    public String dframtMasterInsert(DfrAmtFormBean drfamtFormBean) {
        
        Integer result = getSqlSession().insert("Dframt.dframtMasterInsert", drfamtFormBean);
        
        return result.toString();                
        
    }

    //누적 보류금 카운트
    @Override
    public Integer dfrMainCount(DfrAmtFormBean dframtFormBean) {
        return (Integer)getSqlSession().selectOne("Dframt.dfrMainCount",dframtFormBean);
    }

    //누적 보류 수정
    @Override
    public void dframtMasterInsertMainUpdatePlus(DfrAmtFormBean drfamtFormBean) {
        getSqlSession().update("Dframt.dframtMasterInsertMainUpdatePlus", drfamtFormBean);
    }

    //누적 보류 생성
    @Override
    public void dframtMasterInsertMainInsert(DfrAmtFormBean drfamtFormBean) {
        getSqlSession().insert("Dframt.dframtMasterInsertMainInsert", drfamtFormBean);
    }

    //보류 수정
    @Override
    public void dframtMasterUpdate(DfrAmtFormBean dfrAmtFormBean) {
        getSqlSession().update("Dframt.dframtMasterUpdate", dfrAmtFormBean);
    }

    //보류 삭제
    @Override
    public void dframtMasterDelete(DfrAmtFormBean dfrAmtFormBean) {
        getSqlSession().delete("Dframt.dframtMasterDelete", dfrAmtFormBean);
    }

    //보류확정 조회
    @Override
    public List<Tb_Onffmerch_Dfr_Dec> dfrDecList(DfrAmtFormBean dfrAmtFormBean) {
        return (List)getSqlSession().selectList("Dframt.dfrDecList",dfrAmtFormBean);
    }

    //보류확정 등록
    @Override
    public void dframtDetailInsert(DfrAmtFormBean dfrAmtFormBean) {
        getSqlSession().insert("Dframt.dframtDetailInsert", dfrAmtFormBean);
    }

    //보류확정시 누적 보류 수정
    @Override
    public void dframtDetailMainUpdatePlus(DfrAmtFormBean dfrAmtFormBean) {
        getSqlSession().update("Dframt.dframtDetailMainUpdatePlus", dfrAmtFormBean); 
    }

    //보류 수정 증감
    @Override
    public void dframtDetailUpdatePlus(DfrAmtFormBean dfrAmtFormBean) {
        getSqlSession().update("Dframt.dframtDetailUpdatePlus", dfrAmtFormBean);  
    }

    //누적 보류 수정 차감
    @Override
    public void dframtMasterInsertMainUpdateMinus(DfrAmtFormBean dfrAmtFormBean) {
        getSqlSession().update("Dframt.dframtMasterInsertMainUpdateMinus", dfrAmtFormBean);  
    }

    //누적 보류 수정 차감
    @Override
    public void dframtDetailMainUpdateMinus(DfrAmtFormBean dfrAmtFormBean) {
        getSqlSession().update("Dframt.dframtDetailMainUpdateMinus", dfrAmtFormBean);  
    }

    //누적 보류생성액 확인
    @Override
    public Integer dframtMainAmt(DfrAmtFormBean dfrAmtFormBean) {
        return (Integer)getSqlSession().selectOne("Dframt.dframtMainAmt",dfrAmtFormBean);
    }

    //누적 보류 삭제
    @Override
    public void dframtMainDelete(DfrAmtFormBean dfrAmtFormBean) {
        getSqlSession().delete("Dframt.dframtMainDelete", dfrAmtFormBean);
    }
    
    //보류 수정 차감
    @Override
    public void dframtDetailUpdateMinus(DfrAmtFormBean dfrAmtFormBean) {
        getSqlSession().update("Dframt.dframtDetailUpdateMinus", dfrAmtFormBean);  
    }    

    //보류확정 삭제
    @Override
    public void dframtDetailDelete(DfrAmtFormBean dfrAmtFormBean) {
        getSqlSession().delete("Dframt.dframtDetailDelete", dfrAmtFormBean); 
    }

    //보류해제 조회
    @Override
    public List<Tb_Onffmerch_Dfr_Rel> dfrRelList(DfrAmtFormBean dfrAmtFormBean) {
        return (List)getSqlSession().selectList("Dframt.dfrRelList",dfrAmtFormBean); 
    }

    //누적 보류 수정 보류해제금 증감 보류확정잔액 차감
    @Override
    public void dframtRelInsertMainPlus(DfrAmtFormBean dfrAmtFormBean) {
        getSqlSession().update("Dframt.dframtRelInsertMainPlus", dfrAmtFormBean);  
    }

    //보류 상세 수정 보류해제금 증감
    @Override
    public void dframtRelUpdatePlus(DfrAmtFormBean dfrAmtFormBean) {
        getSqlSession().update("Dframt.dframtRelUpdatePlus", dfrAmtFormBean);  
    }

    //보류해제 등록
    @Override
    public void dframtRelInsert(DfrAmtFormBean dfrAmtFormBean) {
        getSqlSession().insert("Dframt.dframtRelInsert", dfrAmtFormBean);  
    }

    //누적 보류 수정 보류해제금 차감 보류확정잔액 증감
    @Override
    public void dframtRelMainUpdateMinus(DfrAmtFormBean dfrAmtFormBean) {
        getSqlSession().update("Dframt.dframtRelMainUpdateMinus", dfrAmtFormBean);  
    }

    //보류해지시 보류 상세 수정
    @Override
    public void dframtRelUpdateMinus(DfrAmtFormBean dfrAmtFormBean) {
        getSqlSession().update("Dframt.dframtRelUpdateMinus", dfrAmtFormBean);  
    }

    //보류해제 삭제
    @Override
    public void dframtRelDelete(DfrAmtFormBean dfrAmtFormBean) {
        getSqlSession().delete("Dframt.dframtRelDelete", dfrAmtFormBean); 
    }
    
}
