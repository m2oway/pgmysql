/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.dframt.Dao;

import com.onoffkorea.system.dframt.Bean.DfrAmtFormBean;
import com.onoffkorea.system.dframt.Bean.DfrAmtMainFormBean;
import com.onoffkorea.system.dframt.Vo.Tb_Onffmerch_Dfr_Dec;
import com.onoffkorea.system.dframt.Vo.Tb_Onffmerch_Dfr_Gen;
import com.onoffkorea.system.dframt.Vo.Tb_Onffmerch_Dfr_Main;
import com.onoffkorea.system.dframt.Vo.Tb_Onffmerch_Dfr_Rel;
import java.util.List;

/**
 *
 * @author MoonbongChoi
 */
public interface DfrAmtDAO {
    public Integer dframtMasterListCount(DfrAmtMainFormBean dfrAmtMainFormBean);
    
    public List<Tb_Onffmerch_Dfr_Main> dframtMasterList(DfrAmtMainFormBean dfrAmtMainFormBean);
    
    public List<Tb_Onffmerch_Dfr_Main> dframtMasterListExcel(DfrAmtMainFormBean dfrAmtMainFormBean);
    
    public Integer dframtDetailListCount(DfrAmtFormBean dframtFormBean);
    
    public List<Tb_Onffmerch_Dfr_Gen> dframtDetailList(DfrAmtFormBean dframtFormBean);    
    public List<Tb_Onffmerch_Dfr_Gen> dframtDetailListExcel(DfrAmtFormBean dfrAmtMainFormBean);

    public String dframtMasterInsert(DfrAmtFormBean drfamtFormBean);

    public Integer dfrMainCount(DfrAmtFormBean drfamtFormBean);

    public void dframtMasterInsertMainUpdatePlus(DfrAmtFormBean drfamtFormBean);

    public void dframtMasterInsertMainInsert(DfrAmtFormBean drfamtFormBean);

    public void dframtMasterUpdate(DfrAmtFormBean dfrAmtFormBean);

    public void dframtMasterDelete(DfrAmtFormBean dfrAmtFormBean);

    public List<Tb_Onffmerch_Dfr_Dec> dfrDecList(DfrAmtFormBean dfrAmtFormBean);

    public void dframtDetailInsert(DfrAmtFormBean dfrAmtFormBean);

    public void dframtDetailMainUpdatePlus(DfrAmtFormBean dfrAmtFormBean);

    public void dframtDetailUpdatePlus(DfrAmtFormBean dfrAmtFormBean);

    public void dframtMasterInsertMainUpdateMinus(DfrAmtFormBean dfrAmtFormBean);

    public void dframtDetailMainUpdateMinus(DfrAmtFormBean dfrAmtFormBean);

    public Integer dframtMainAmt(DfrAmtFormBean dfrAmtFormBean);

    public void dframtMainDelete(DfrAmtFormBean dfrAmtFormBean);

    public void dframtDetailUpdateMinus(DfrAmtFormBean dfrAmtFormBean);

    public void dframtDetailDelete(DfrAmtFormBean dfrAmtFormBean);

    public List<Tb_Onffmerch_Dfr_Rel> dfrRelList(DfrAmtFormBean dfrAmtFormBean);

    public void dframtRelInsertMainPlus(DfrAmtFormBean dfrAmtFormBean);

    public void dframtRelUpdatePlus(DfrAmtFormBean dfrAmtFormBean);

    public void dframtRelInsert(DfrAmtFormBean dfrAmtFormBean);

    public void dframtRelMainUpdateMinus(DfrAmtFormBean dfrAmtFormBean);

    public void dframtRelUpdateMinus(DfrAmtFormBean dfrAmtFormBean);

    public void dframtRelDelete(DfrAmtFormBean dfrAmtFormBean);
    
}
