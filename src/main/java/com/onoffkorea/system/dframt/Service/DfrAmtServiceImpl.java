/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.dframt.Service;

import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.dframt.Bean.DfrAmtFormBean;
import com.onoffkorea.system.dframt.Bean.DfrAmtMainFormBean;
import com.onoffkorea.system.dframt.Dao.DfrAmtDAO;
import com.onoffkorea.system.dframt.Vo.Tb_Onffmerch_Dfr_Dec;
import com.onoffkorea.system.dframt.Vo.Tb_Onffmerch_Dfr_Gen;
import com.onoffkorea.system.dframt.Vo.Tb_Onffmerch_Dfr_Main;
import com.onoffkorea.system.dframt.Vo.Tb_Onffmerch_Dfr_Rel;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author MoonbongChoi
 */
@Service("dframtService")
public class DfrAmtServiceImpl implements DfrAmtService  {
    protected Logger logger = Logger.getLogger(getClass().getName());
    
    @Autowired
    private CommonService commonService;    
    
    
    @Autowired
    private DfrAmtDAO dframtDAO;
    
    
    @Override
    @Transactional
    public Hashtable dframtMasterList(DfrAmtMainFormBean drfamtFormBean) {
     Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = dframtDAO.dframtMasterListCount(drfamtFormBean);
                List<Tb_Onffmerch_Dfr_Main> ls_dframtMasterFormList = dframtDAO.dframtMasterList(drfamtFormBean);
                
                 StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(drfamtFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_dframtMasterFormList.size(); i++) {
                        Tb_Onffmerch_Dfr_Main tb_dfrmain = ls_dframtMasterFormList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"onffmerch_no\":\""+tb_dfrmain.getOnffmerch_no()+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append(" \"" + Util.nullToString(tb_dfrmain.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_dfrmain.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_dfrmain.getOnffmerch_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_dfrmain.getGen_dfr_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_dfrmain.getDec_dfr_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_dfrmain.getRel_dfr_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_dfrmain.getDfr_bal_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_dfrmain.getDec_bal_amt()) + "\"");
                        
                        if (i == (ls_dframtMasterFormList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");   
                
            
                ht.put("ResultSet", sb.toString());

                /*
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                ht.put("payChnCateList", payChnCateList);
                */
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;             
    }

    @Override
    public void dframtMasterListExcel(DfrAmtMainFormBean drfamtFormBean, String excelDownloadFilename, HttpServletResponse response) {
        try {
            
                List ls_dframtMasterListExcel = dframtDAO.dframtMasterListExcel(drfamtFormBean);
                
                StringBuffer dframtMasterListExcel = Util.makeData(ls_dframtMasterListExcel);
        
                Util.exceldownload(dframtMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }
    }

    //보류 상세 조회
    @Override
    public Hashtable dframtDetailList(DfrAmtFormBean drfamtFormBean) {
     Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = dframtDAO.dframtDetailListCount(drfamtFormBean);
                List<Tb_Onffmerch_Dfr_Gen> ls_dframtDetailFormList = dframtDAO.dframtDetailList(drfamtFormBean);
                
                 StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(drfamtFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_dframtDetailFormList.size(); i++) {
                        Tb_Onffmerch_Dfr_Gen tb_dfrgen = ls_dframtDetailFormList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"dfr_seq\":\""+tb_dfrgen.getDfr_seq()+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append(" \"" + Util.nullToString(tb_dfrgen.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_dfrgen.getGen_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_dfrgen.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_dfrgen.getOnffmerch_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_dfrgen.getGen_dfr_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_dfrgen.getDec_dfr_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_dfrgen.getRel_dfr_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_dfrgen.getGen_reson_nm()) + "\"");
                        
                        if (i == (ls_dframtDetailFormList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");   
                
            
                ht.put("ResultSet", sb.toString());
                ht.put("ls_dframtDetailFormList", ls_dframtDetailFormList);

                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("DFR_GEN_RESON");
                Tb_Code_Main DfrGenResonList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                ht.put("DfrGenResonList", DfrGenResonList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;      
    }

    @Override
    public void dframtDetailListExcel(DfrAmtFormBean drfamtFormBean, String excelDownloadFilename, HttpServletResponse response) {
        try {
            
                List ls_dframtMasterListExcel = dframtDAO.dframtDetailListExcel(drfamtFormBean);
                
                StringBuffer dframtMasterListExcel = Util.makeData(ls_dframtMasterListExcel);
        
                Util.exceldownload(dframtMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }
    }

    //보류금 등록
    @Override
    @Transactional
    public String dframtMasterInsert(DfrAmtFormBean drfamtFormBean) {
        
        //보류금 생성
        String dfr_seq = dframtDAO.dframtMasterInsert(drfamtFormBean);
        
        //누적 보류금 생성 카운트
        Integer dfr_cnt = dframtDAO.dfrMainCount(drfamtFormBean);
        
        if(dfr_cnt > 0){
            //누적 보류금 수정 보류생성액 증감
            dframtDAO.dframtMasterInsertMainUpdatePlus(drfamtFormBean);
        }else{
            //누적 보류금 생성
            dframtDAO.dframtMasterInsertMainInsert(drfamtFormBean);
        }
        
        return dfr_seq;        
        
    }

    //보류 수정
    @Override
    @Transactional
    public void dframtMasterUpdate(DfrAmtFormBean dfrAmtFormBean) {
        
        dframtDAO.dframtMasterUpdate(dfrAmtFormBean);
        
    }

    //보류 삭제
    @Override
    @Transactional
    public void dframtMasterDelete(DfrAmtFormBean dfrAmtFormBean) {
        //보류 삭제
        dframtDAO.dframtMasterDelete(dfrAmtFormBean);
        //누적 보류 수정 보류생성액 보류요청잔액 차감
        dframtDAO.dframtMasterInsertMainUpdateMinus(dfrAmtFormBean);
        //보류 생성액 확인
        Integer gen_dfr_amt = dframtDAO.dframtMainAmt(dfrAmtFormBean);
        
        if(gen_dfr_amt == 0){
            //누적 보류 삭제
            dframtDAO.dframtMainDelete(dfrAmtFormBean);
        }
    }

    //보류 확정 조회
    @Override
    public Hashtable dfrDecList(DfrAmtFormBean dfrAmtFormBean) {

        Hashtable ht = new Hashtable();

        try {
            
                List<Tb_Onffmerch_Dfr_Dec> ls_dfrDecList = dframtDAO.dfrDecList(dfrAmtFormBean);
                
                 StringBuilder sb = new StringBuilder();
                sb.append("{\"rows\" : [");
                for (int i = 0; i < ls_dfrDecList.size(); i++) {
                        Tb_Onffmerch_Dfr_Dec tb_Onffmerch_Dfr_Dec = ls_dfrDecList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"dfr_seq\":\""+tb_Onffmerch_Dfr_Dec.getDfr_seq()+"\"");
                        sb.append(" ,\"dec_dfr_seq\":\""+tb_Onffmerch_Dfr_Dec.getDec_dfr_seq()+"\"");
                        sb.append(" ,\"dfr_dec_amt\":\""+tb_Onffmerch_Dfr_Dec.getDfr_dec_amt()+"\"");
                        sb.append(" ,\"onffmerch_no\":\""+tb_Onffmerch_Dfr_Dec.getOnffmerch_no()+"\"");
                        sb.append(",\"pay_sche_seq\":\""+tb_Onffmerch_Dfr_Dec.getPay_sche_seq()+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append(" \"" + Util.nullToString(tb_Onffmerch_Dfr_Dec.getRnum()) + "\"");
                        sb.append(",\"" + "" + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Onffmerch_Dfr_Dec.getDfr_dec_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Onffmerch_Dfr_Dec.getDfr_dec_reson_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Onffmerch_Dfr_Dec.getDfr_dec_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Onffmerch_Dfr_Dec.getMemo()) + "\"");
                        
                        if (i == (ls_dfrDecList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");   
                
            
                ht.put("ResultSet", sb.toString());

                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("DFR_GEN_RESON");
                Tb_Code_Main DfrGenResonList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                ht.put("DfrGenResonList", DfrGenResonList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;            
        
    }

    //보류확정 등록
    @Override
    @Transactional
    public void dframtDetailInsert(DfrAmtFormBean dfrAmtFormBean) {
        
        //누적 보류 수정 보류확정액 증감 보류요청잔액 차감
        dframtDAO.dframtDetailMainUpdatePlus(dfrAmtFormBean); 
        //보류 상세 수정
        dframtDAO.dframtDetailUpdatePlus(dfrAmtFormBean);          
        //보류확정 등록
        dframtDAO.dframtDetailInsert(dfrAmtFormBean);
        
    }

    //보류 확정 삭제
    @Override
    public void dframtDetailDelete(DfrAmtFormBean dfrAmtFormBean) {
        //누적 보류 수정 확정보류금 차감 보류확정잔액 차감 보류요청잔액 증감
        dframtDAO.dframtDetailMainUpdateMinus(dfrAmtFormBean); 
        //보류 상세 수정
        dframtDAO.dframtDetailUpdateMinus(dfrAmtFormBean);          
        //보류확정 삭제
        dframtDAO.dframtDetailDelete(dfrAmtFormBean);
    }
    
    //보류 해제 조회
    @Override
    public Hashtable dfrRelList(DfrAmtFormBean dfrAmtFormBean) {

        Hashtable ht = new Hashtable();

        try {
            
                List<Tb_Onffmerch_Dfr_Rel> ls_dfrRelList = dframtDAO.dfrRelList(dfrAmtFormBean); 
                
                 StringBuilder sb = new StringBuilder();
                sb.append("{\"rows\" : [");
                for (int i = 0; i < ls_dfrRelList.size(); i++) {
                        Tb_Onffmerch_Dfr_Rel tb_Onffmerch_Dfr_Rel = ls_dfrRelList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"dfr_seq\":\""+tb_Onffmerch_Dfr_Rel.getDfr_seq()+"\"");
                        sb.append(" ,\"dfr_rel_seq\":\""+tb_Onffmerch_Dfr_Rel.getDfr_rel_seq()+"\"");
                        sb.append(" ,\"dfr_rel_amt\":\""+tb_Onffmerch_Dfr_Rel.getDfr_rel_amt()+"\"");
                        sb.append(" ,\"onffmerch_no\":\""+tb_Onffmerch_Dfr_Rel.getOnffmerch_no()+"\"");
                        sb.append(" ,\"pay_sche_seq\":\""+tb_Onffmerch_Dfr_Rel.getPay_sche_seq()+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append(" \"" + Util.nullToString(tb_Onffmerch_Dfr_Rel.getRnum()) + "\"");
                        sb.append(",\"" + "" + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Onffmerch_Dfr_Rel.getDfr_rel_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Onffmerch_Dfr_Rel.getDfr_rel_reson_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Onffmerch_Dfr_Rel.getDfr_rel_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Onffmerch_Dfr_Rel.getMemo()) + "\"");
                        
                        if (i == (ls_dfrRelList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");   
                
            
                ht.put("ResultSet", sb.toString());

                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;            
        
    }    
    
    //보류해제 등록
    @Override
    @Transactional
    public void dframtRelInsert(DfrAmtFormBean dfrAmtFormBean) {
        
        //누적 보류 수정 보류해제금 증감 보류확정잔액 차감
        dframtDAO.dframtRelInsertMainPlus(dfrAmtFormBean); 
        //보류 상세 수정 보류해제금 증감
        dframtDAO.dframtRelUpdatePlus(dfrAmtFormBean);          
        //보류해제 등록
        dframtDAO.dframtRelInsert(dfrAmtFormBean);
        
    }    

    //보류해제 삭제
    @Override
    public void dframtRelDelete(DfrAmtFormBean dfrAmtFormBean) {
        //누적 보류 수정 보류해제금 차감 보류확정잔액 증감
        dframtDAO.dframtRelMainUpdateMinus(dfrAmtFormBean); 
        //보류 상세 수정
        dframtDAO.dframtRelUpdateMinus(dfrAmtFormBean);          
        //보류해제 삭제
        dframtDAO.dframtRelDelete(dfrAmtFormBean);
    }
    
}
