/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.dframt.Service;

import com.onoffkorea.system.dframt.Bean.DfrAmtFormBean;
import com.onoffkorea.system.dframt.Bean.DfrAmtMainFormBean;
import java.util.Hashtable;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author MoonbongChoi
 */
public interface DfrAmtService {

    public Hashtable dframtMasterList(DfrAmtMainFormBean drfamtFormBean);           
    public void dframtMasterListExcel(DfrAmtMainFormBean drfamtFormBean, String excelDownloadFilename, HttpServletResponse response);
    
    public Hashtable dframtDetailList(DfrAmtFormBean drfamtFormBean);           
    public void dframtDetailListExcel(DfrAmtFormBean drfamtFormBean, String excelDownloadFilename, HttpServletResponse response);    

    public String dframtMasterInsert(DfrAmtFormBean drfamtFormBean);

    public void dframtMasterUpdate(DfrAmtFormBean dfrAmtFormBean);

    public void dframtMasterDelete(DfrAmtFormBean dfrAmtFormBean);

    public Hashtable dfrDecList(DfrAmtFormBean dfrAmtFormBean);

    public void dframtDetailInsert(DfrAmtFormBean dfrAmtFormBean);

    public void dframtDetailDelete(DfrAmtFormBean dfrAmtFormBean);

    public Hashtable dfrRelList(DfrAmtFormBean dfrAmtFormBean);

    public void dframtRelInsert(DfrAmtFormBean dfrAmtFormBean);

    public void dframtRelDelete(DfrAmtFormBean dfrAmtFormBean);
    
}
