/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.dframt.Vo;

/**
 *
 * @author MoonbongChoi
 */
public class Tb_Onffmerch_Dfr_Dec {

    private String dec_dfr_seq		;
    private String onffmerch_no	;
    private String dfr_seq			;
    private String dfr_dec_amt		;
    private String dfr_dec_dt		;
    private String dfr_dec_reson	;
    private String dfr_dec_reson_nm	;
    private String pay_sche_seq		;
    private String exp_pay_dt		;
    private String memo			;
    private String ins_dt			;
    private String mod_dt			;
    private String ins_user			;
    private String mod_user		;
    
    private String rnum;

    public String getRnum() {
        return rnum;
    }

    public void setRnum(String rnum) {
        this.rnum = rnum;
    }

    
    
    public String getDec_dfr_seq() {
        return dec_dfr_seq;
    }

    public void setDec_dfr_seq(String dec_dfr_seq) {
        this.dec_dfr_seq = dec_dfr_seq;
    }

    public String getOnffmerch_no() {
        return onffmerch_no;
    }

    public void setOnffmerch_no(String onffmerch_no) {
        this.onffmerch_no = onffmerch_no;
    }

    public String getDfr_seq() {
        return dfr_seq;
    }

    public void setDfr_seq(String dfr_seq) {
        this.dfr_seq = dfr_seq;
    }

    public String getDfr_dec_amt() {
        return dfr_dec_amt;
    }

    public void setDfr_dec_amt(String dfr_dec_amt) {
        this.dfr_dec_amt = dfr_dec_amt;
    }

    public String getDfr_dec_dt() {
        return dfr_dec_dt;
    }

    public void setDfr_dec_dt(String dfr_dec_dt) {
        this.dfr_dec_dt = dfr_dec_dt;
    }

    public String getDfr_dec_reson() {
        return dfr_dec_reson;
    }

    public void setDfr_dec_reson(String dfr_dec_reson) {
        this.dfr_dec_reson = dfr_dec_reson;
    }

    public String getDfr_dec_reson_nm() {
        return dfr_dec_reson_nm;
    }

    public void setDfr_dec_reson_nm(String dfr_dec_reson_nm) {
        this.dfr_dec_reson_nm = dfr_dec_reson_nm;
    }

    public String getPay_sche_seq() {
        return pay_sche_seq;
    }

    public void setPay_sche_seq(String pay_sche_seq) {
        this.pay_sche_seq = pay_sche_seq;
    }

    public String getExp_pay_dt() {
        return exp_pay_dt;
    }

    public void setExp_pay_dt(String exp_pay_dt) {
        this.exp_pay_dt = exp_pay_dt;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }
    
    
    
}
