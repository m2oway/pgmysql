/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.dframt.Bean;

import com.onoffkorea.system.common.util.CommonSortableListPagingForm;

/**
 *
 * @author MoonbongChoi
 */
public class DfrAmtFormBean extends CommonSortableListPagingForm{

    private String merch_nm;
    private String pay_sche_seq;
    private String exp_pay_dt;
    private String memo;

    private String dfr_seq;
    private String gen_dfr_amt;
    private String dec_dfr_amt;
    private String rel_dfr_amt;
    private String gen_dt;
    
    private String gen_dt_start;
    private String gen_dt_end;
    
    
    
    private String gen_reson;
    private String ins_dt;
    private String mod_dt;
    private String ins_user;
    private String mod_user;

    private String dec_dfr_seq;
    private String dfr_dec_amt;
    private String dfr_dec_dt;
    
    private String dfr_dec_dt_start;
    private String dfr_dec_dt_end;
    
    private String dfr_dec_reson;

    private String dfr_rel_seq;
    private String dfr_rel_amt;
    private String dfr_rel_dt;
    
    
    private String dfr_rel_dt_start;
    private String dfr_rel_dt_end;
    
    private String dfr_rel_reson;
    
    private String merchpay_popup_yn;
    
    private String[] dec_dfr_seqs;
    private String[] dfr_rel_seqs;
    
    private String dec_rel_seq;

    public String getDec_rel_seq() {
        return dec_rel_seq;
    }

    public void setDec_rel_seq(String dec_rel_seq) {
        this.dec_rel_seq = dec_rel_seq;
    }

    public String[] getDfr_rel_seqs() {
        return dfr_rel_seqs;
    }

    public void setDfr_rel_seqs(String[] dfr_rel_seqs) {
        this.dfr_rel_seqs = dfr_rel_seqs;
    }

    public String[] getDec_dfr_seqs() {
        return dec_dfr_seqs;
    }

    public void setDec_dfr_seqs(String[] dec_dfr_seqs) {
        this.dec_dfr_seqs = dec_dfr_seqs;
    }

    public String getMerchpay_popup_yn() {
        return merchpay_popup_yn;
    }

    public void setMerchpay_popup_yn(String merchpay_popup_yn) {
        this.merchpay_popup_yn = merchpay_popup_yn;
    }

    public String getGen_dt_start() {
        return gen_dt_start;
    }

    public void setGen_dt_start(String gen_dt_start) {
        this.gen_dt_start = gen_dt_start;
    }

    public String getGen_dt_end() {
        return gen_dt_end;
    }

    public void setGen_dt_end(String gen_dt_end) {
        this.gen_dt_end = gen_dt_end;
    }

    public String getDfr_dec_dt_start() {
        return dfr_dec_dt_start;
    }

    public void setDfr_dec_dt_start(String dfr_dec_dt_start) {
        this.dfr_dec_dt_start = dfr_dec_dt_start;
    }

    public String getDfr_dec_dt_end() {
        return dfr_dec_dt_end;
    }

    public void setDfr_dec_dt_end(String dfr_dec_dt_end) {
        this.dfr_dec_dt_end = dfr_dec_dt_end;
    }

    public String getDfr_rel_dt_start() {
        return dfr_rel_dt_start;
    }

    public void setDfr_rel_dt_start(String dfr_rel_dt_start) {
        this.dfr_rel_dt_start = dfr_rel_dt_start;
    }

    public String getDfr_rel_dt_end() {
        return dfr_rel_dt_end;
    }

    public void setDfr_rel_dt_end(String dfr_rel_dt_end) {
        this.dfr_rel_dt_end = dfr_rel_dt_end;
    }
    
    
    
    

    public String getMerch_nm() {
        return merch_nm;
    }

    public void setMerch_nm(String merch_nm) {
        this.merch_nm = merch_nm;
    }

    public String getPay_sche_seq() {
        return pay_sche_seq;
    }

    public void setPay_sche_seq(String pay_sche_seq) {
        this.pay_sche_seq = pay_sche_seq;
    }

    public String getExp_pay_dt() {
        return exp_pay_dt;
    }

    public void setExp_pay_dt(String exp_pay_dt) {
        this.exp_pay_dt = exp_pay_dt;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getDfr_seq() {
        return dfr_seq;
    }

    public void setDfr_seq(String dfr_seq) {
        this.dfr_seq = dfr_seq;
    }

    public String getGen_dfr_amt() {
        return gen_dfr_amt;
    }

    public void setGen_dfr_amt(String gen_dfr_amt) {
        this.gen_dfr_amt = gen_dfr_amt;
    }

    public String getDec_dfr_amt() {
        return dec_dfr_amt;
    }

    public void setDec_dfr_amt(String dec_dfr_amt) {
        this.dec_dfr_amt = dec_dfr_amt;
    }

    public String getRel_dfr_amt() {
        return rel_dfr_amt;
    }

    public void setRel_dfr_amt(String rel_dfr_amt) {
        this.rel_dfr_amt = rel_dfr_amt;
    }

    public String getGen_dt() {
        return gen_dt;
    }

    public void setGen_dt(String gen_dt) {
        this.gen_dt = gen_dt;
    }

    public String getGen_reson() {
        return gen_reson;
    }

    public void setGen_reson(String gen_reson) {
        this.gen_reson = gen_reson;
    }


    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    public String getDec_dfr_seq() {
        return dec_dfr_seq;
    }

    public void setDec_dfr_seq(String dec_dfr_seq) {
        this.dec_dfr_seq = dec_dfr_seq;
    }

    public String getDfr_dec_amt() {
        return dfr_dec_amt;
    }

    public void setDfr_dec_amt(String dfr_dec_amt) {
        this.dfr_dec_amt = dfr_dec_amt;
    }

    public String getDfr_dec_dt() {
        return dfr_dec_dt;
    }

    public void setDfr_dec_dt(String dfr_dec_dt) {
        this.dfr_dec_dt = dfr_dec_dt;
    }

    public String getDfr_dec_reson() {
        return dfr_dec_reson;
    }

    public void setDfr_dec_reson(String dfr_dec_reson) {
        this.dfr_dec_reson = dfr_dec_reson;
    }

    public String getDfr_rel_seq() {
        return dfr_rel_seq;
    }

    public void setDfr_rel_seq(String dfr_rel_seq) {
        this.dfr_rel_seq = dfr_rel_seq;
    }

    public String getDfr_rel_amt() {
        return dfr_rel_amt;
    }

    public void setDfr_rel_amt(String dfr_rel_amt) {
        this.dfr_rel_amt = dfr_rel_amt;
    }

    public String getDfr_rel_dt() {
        return dfr_rel_dt;
    }

    public void setDfr_rel_dt(String dfr_rel_dt) {
        this.dfr_rel_dt = dfr_rel_dt;
    }

    public String getDfr_rel_reson() {
        return dfr_rel_reson;
    }

    public void setDfr_rel_reson(String dfr_rel_reson) {
        this.dfr_rel_reson = dfr_rel_reson;
    }

    @Override
    public String toString() {
        return "DfrAmtFormBean{" + "merch_nm=" + merch_nm + ", pay_sche_seq=" + pay_sche_seq + ", exp_pay_dt=" + exp_pay_dt + ", memo=" + memo + ", dfr_seq=" + dfr_seq + ", gen_dfr_amt=" + gen_dfr_amt + ", dec_dfr_amt=" + dec_dfr_amt + ", rel_dfr_amt=" + rel_dfr_amt + ", gen_dt=" + gen_dt + ", gen_dt_start=" + gen_dt_start + ", gen_dt_end=" + gen_dt_end + ", gen_reson=" + gen_reson + ", ins_dt=" + ins_dt + ", mod_dt=" + mod_dt + ", ins_user=" + ins_user + ", mod_user=" + mod_user + ", dec_dfr_seq=" + dec_dfr_seq + ", dfr_dec_amt=" + dfr_dec_amt + ", dfr_dec_dt=" + dfr_dec_dt + ", dfr_dec_dt_start=" + dfr_dec_dt_start + ", dfr_dec_dt_end=" + dfr_dec_dt_end + ", dfr_dec_reson=" + dfr_dec_reson + ", dfr_rel_seq=" + dfr_rel_seq + ", dfr_rel_amt=" + dfr_rel_amt + ", dfr_rel_dt=" + dfr_rel_dt + ", dfr_rel_dt_start=" + dfr_rel_dt_start + ", dfr_rel_dt_end=" + dfr_rel_dt_end + ", dfr_rel_reson=" + dfr_rel_reson + ", merchpay_popup_yn=" + merchpay_popup_yn + '}';
    }

    
    
}
