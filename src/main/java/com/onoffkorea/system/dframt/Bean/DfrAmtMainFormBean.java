/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.dframt.Bean;

import com.onoffkorea.system.common.util.CommonSortableListPagingForm;
 
/**
 *
 * @author MoonbongChoi
 */
public class DfrAmtMainFormBean  extends CommonSortableListPagingForm{
    private String onffmerch_no;
    private String merch_nm;
    private String gen_dfr_amt;
    private String dec_dfr_amt;
    private String rel_dfr_amt;
    private String dfr_bal_amt;
    private String dec_bal_amt;
    private String ins_dt	;
    private String mod_dt	;
    private String ins_user	;
    private String mod_user;

    public String getOnffmerch_no() {
        return onffmerch_no;
    }

    public void setOnffmerch_no(String onffmerch_no) {
        this.onffmerch_no = onffmerch_no;
    }
    

    public String getMerch_nm() {
        return merch_nm;
    }

    public void setMerch_nm(String merch_nm) {
        this.merch_nm = merch_nm;
    }

    public String getGen_dfr_amt() {
        return gen_dfr_amt;
    }

    public void setGen_dfr_amt(String gen_dfr_amt) {
        this.gen_dfr_amt = gen_dfr_amt;
    }

    public String getDec_dfr_amt() {
        return dec_dfr_amt;
    }

    public void setDec_dfr_amt(String dec_dfr_amt) {
        this.dec_dfr_amt = dec_dfr_amt;
    }

    public String getRel_dfr_amt() {
        return rel_dfr_amt;
    }

    public void setRel_dfr_amt(String rel_dfr_amt) {
        this.rel_dfr_amt = rel_dfr_amt;
    }

    public String getDfr_bal_amt() {
        return dfr_bal_amt;
    }

    public void setDfr_bal_amt(String dfr_bal_amt) {
        this.dfr_bal_amt = dfr_bal_amt;
    }

    public String getDec_bal_amt() {
        return dec_bal_amt;
    }

    public void setDec_bal_amt(String dec_bal_amt) {
        this.dec_bal_amt = dec_bal_amt;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

}
