/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.dframt.Controller;

import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.dframt.Bean.DfrAmtFormBean;
import com.onoffkorea.system.dframt.Bean.DfrAmtMainFormBean;
import com.onoffkorea.system.dframt.Service.DfrAmtService;
import com.onoffkorea.system.dframt.Vo.Tb_Onffmerch_Dfr_Gen;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import common.web.servlet.BaseSpringMultiActionController;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/dframt/**")
public class DfrAmtController extends BaseSpringMultiActionController {
    
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private CommonService commonService;
    
    @Autowired
    private DfrAmtService dframtService;
        
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }
            
    //보류 레이아웃
    @RequestMapping(method= RequestMethod.GET,value = "/dframtLayout")
    public String dframtLayout(@Valid DfrAmtMainFormBean dfrAmtMainFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        return null;
    }
    
    //보류 집계 조회
    @RequestMapping(method= RequestMethod.GET,value = "/dframtMasterList")
    public String dframtMasterFormList(@Valid DfrAmtMainFormBean dfrAmtMainFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        Hashtable ht_dframtMasterList = dframtService.dframtMasterList(dfrAmtMainFormBean);
        String ls_dframtMasterList = (String) ht_dframtMasterList.get("ResultSet");
        
        model.addAttribute("ls_dframtMasterList",ls_dframtMasterList);     
        
        return null;
    }
    
    //보류 집계 조회
    @RequestMapping(method= RequestMethod.POST,value = "/dframtMasterList")
    @ResponseBody    
    public String dframtMasterList(@Valid DfrAmtMainFormBean dfrAmtMainFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        Hashtable ht_dframtMasterList = dframtService.dframtMasterList(dfrAmtMainFormBean);
        String ls_dframtMasterList = (String) ht_dframtMasterList.get("ResultSet");
        
        return ls_dframtMasterList;
    }    
    
    //보류 집계 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/dframtMasterListExcel")
    
    public void dframtMasterListExcel(@Valid DfrAmtMainFormBean dfrAmtMainFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request, HttpServletResponse response,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
    
        
          String excelDownloadFilename = new String(getMessage("dframt.dframtMasterListExcel.fileName"));   
          dframtService.dframtMasterListExcel(dfrAmtMainFormBean, excelDownloadFilename, response);
        
    
    }    
    
    
    //보류상세조회
    @RequestMapping(method= RequestMethod.GET,value = "/dframtDetailList")
    public String dframtDetailFormList(@Valid DfrAmtFormBean dfrAmtFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
            
            /*
                SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        
                if((dfrAmtFormBean.getGen_dt_start() == null) || (dfrAmtFormBean.getGen_dt_start() == "")){
                    Date now = new Date();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(now);
                    cal.add(Calendar.DAY_OF_WEEK, -6);


                    Date currentTime = new Date ( );
                    String start_dt = formatter.format ( cal.getTime());
                    String end_dt = formatter.format(currentTime.getTime());


                    dfrAmtFormBean.setGen_dt_start(start_dt);
                    dfrAmtFormBean.setGen_dt_end(end_dt);
                }
            */       
        
        Hashtable ht_dframtDetailList = dframtService.dframtDetailList(dfrAmtFormBean);
        String ls_dframtDetailList = (String) ht_dframtDetailList.get("ResultSet");
        
        model.addAttribute("ls_dframtDetailList",ls_dframtDetailList);     
        
        model.addAttribute("dfrAmtFormBean",dfrAmtFormBean);
        
        Tb_Code_Main DfrGenResonList = (Tb_Code_Main) ht_dframtDetailList.get("DfrGenResonList");
        
        model.addAttribute("DfrGenResonList",DfrGenResonList);                   
        
        return null;
    }
    
    //보류상세조회
    @RequestMapping(method= RequestMethod.POST,value = "/dframtDetailList")
    @ResponseBody    
    public String dframtDetailList(@Valid DfrAmtFormBean dfrAmtFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        Hashtable ht_dframtDetailList = dframtService.dframtDetailList(dfrAmtFormBean);
        String ls_dframtDetailList = (String) ht_dframtDetailList.get("ResultSet");
        
        return ls_dframtDetailList;
    }    
    
    //보류상세 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/dframtDetailListExcel")    
    public void dframtDetailListExcel(@Valid DfrAmtFormBean dfrAmtFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request, HttpServletResponse response,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
    
          String excelDownloadFilename = new String(getMessage("dframt.dframtDetailListExcel.fileName"));   
          dframtService.dframtDetailListExcel(dfrAmtFormBean, excelDownloadFilename, response);
    }        
    
    //보류 등록
    @RequestMapping(method= RequestMethod.GET,value = "/dframtMasterInsert")
    public String dframtMasterInsertForm(@Valid DfrAmtFormBean dfrAmtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("DFR_GEN_RESON");
        Tb_Code_Main dfrGenResonList = commonService.codeSearch(commonCodeSearchFormBean);
        
        model.addAttribute("dfrGenResonList",dfrGenResonList);
        
        return null;
        
    }       
    
    //보류 등록
    @RequestMapping(method= RequestMethod.POST,value = "/dframtMasterInsert")
    public @ResponseBody String dframtMasterInsert(@Valid DfrAmtFormBean dfrAmtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        dfrAmtFormBean.setUser_seq(siteSession.getUser_seq());
        String dfr_seq = dframtService.dframtMasterInsert(dfrAmtFormBean);
        
        return dfr_seq;
        
    }         
    
    //보류 수정
    @RequestMapping(method= RequestMethod.GET,value = "/dframtMasterUpdate")
    public String dframtMasterUpdateForm(@Valid DfrAmtFormBean dfrAmtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("DFR_GEN_RESON");
        Tb_Code_Main dfrGenResonList = commonService.codeSearch(commonCodeSearchFormBean);
        
        commonCodeSearchFormBean.setMain_code("DFR_DEC_RESON");
        Tb_Code_Main dfrDecResonList = commonService.codeSearch(commonCodeSearchFormBean);
        
        commonCodeSearchFormBean.setMain_code("DFR_REL_RESON");
        Tb_Code_Main dfrRelResonList = commonService.codeSearch(commonCodeSearchFormBean);        
        
        Hashtable ht_dframtDetailList = dframtService.dframtDetailList(dfrAmtFormBean);
        List<Tb_Onffmerch_Dfr_Gen> ls_dframtDetailList = (List<Tb_Onffmerch_Dfr_Gen>) ht_dframtDetailList.get("ls_dframtDetailFormList");
        
        Hashtable ht_drfDecList = dframtService.dfrDecList(dfrAmtFormBean);
        String ls_drfDecList = (String) ht_drfDecList.get("ResultSet");
        
        Hashtable ht_drfRelList = dframtService.dfrRelList(dfrAmtFormBean);
        String ls_drfRelList = (String) ht_drfRelList.get("ResultSet");
        
        model.addAttribute("dfrGenResonList",dfrGenResonList);
        model.addAttribute("dfrDecResonList",dfrDecResonList);
        model.addAttribute("dfrRelResonList",dfrRelResonList);
        model.addAttribute("ls_dframtDetailList",ls_dframtDetailList);
        model.addAttribute("ls_drfDecList",ls_drfDecList);
        model.addAttribute("ls_drfRelList",ls_drfRelList);
        
        return null;
        
    }       
    
    //보류 수정
    @RequestMapping(method= RequestMethod.POST,value = "/dframtMasterUpdate")
    public @ResponseBody String dframtMasterUpdate(@Valid DfrAmtFormBean dfrAmtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        dfrAmtFormBean.setUser_seq(siteSession.getUser_seq());
        dframtService.dframtMasterUpdate(dfrAmtFormBean);
        
        return null;
        
    }            
    
    //보류 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/dframtMasterDelete")
    public @ResponseBody String dframtMasterDelete(@Valid DfrAmtFormBean dfrAmtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        dfrAmtFormBean.setUser_seq(siteSession.getUser_seq());
        dframtService.dframtMasterDelete(dfrAmtFormBean);
        
        return null;
        
    }              
    
    //보류확정 등록
    @RequestMapping(method= RequestMethod.POST,value = "/dframtDetailInsert")
    public @ResponseBody String dframtDetailInsert(@Valid DfrAmtFormBean dfrAmtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        dfrAmtFormBean.setUser_seq(siteSession.getUser_seq());
        dframtService.dframtDetailInsert(dfrAmtFormBean);
        
        return null;
        
    }           
    
    //보류확정 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/dframtDetailDelete")
    public @ResponseBody String dframtDetailDelete(@Valid DfrAmtFormBean dfrAmtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        dfrAmtFormBean.setUser_seq(siteSession.getUser_seq());
        dframtService.dframtDetailDelete(dfrAmtFormBean);
        
        return null;
        
    }             
    
    //보류해제 등록
    @RequestMapping(method= RequestMethod.POST,value = "/dframtRelInsert")
    public @ResponseBody String dframtRelInsert(@Valid DfrAmtFormBean dfrAmtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        dfrAmtFormBean.setUser_seq(siteSession.getUser_seq());
        dframtService.dframtRelInsert(dfrAmtFormBean);
        
        return null;
        
    }             
    
    //보류해제 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/dframtRelDelete")
    public @ResponseBody String dframtRelDelete(@Valid DfrAmtFormBean dfrAmtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        dfrAmtFormBean.setUser_seq(siteSession.getUser_seq());
        dframtService.dframtRelDelete(dfrAmtFormBean);
        
        return null;
        
    }           
    
}
