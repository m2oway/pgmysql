/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.outamt.Controller;

import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import com.onoffkorea.system.outamt.Bean.OutamtFormBean;
import com.onoffkorea.system.outamt.Service.OutamtService;
import com.onoffkorea.system.outamt.Vo.Tb_Outamt_Main;
import com.onoffkorea.system.outamt.Vo.Tb_Setoffinfo;
import common.web.servlet.BaseSpringMultiActionController;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/outamt/**")
public class OutamtController extends BaseSpringMultiActionController{
    
    protected Log logger = LogFactory.getLog(getClass());
    
    @Autowired
    private OutamtService outamtService;    
    
    @Autowired
    private CommonService commonService;
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }   
    
    //미수금 정보 조회
    @RequestMapping(method= RequestMethod.GET,value = "/outamtMasterList")
    public String outamtMasterFormList(@ModelAttribute OutamtFormBean outamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        
           SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        if((outamtFormBean.getGen_start_dt() == null) || (outamtFormBean.getGen_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            outamtFormBean.setGen_start_dt(start_dt);
            outamtFormBean.setGen_end_dt(end_dt);
        }
        
        Hashtable ht_outamtMasterList = outamtService.outamtMasterList(outamtFormBean);
        List<Tb_Outamt_Main> ls_outamtMasterList = (List<Tb_Outamt_Main>) ht_outamtMasterList.get("ResultSet");

        StringBuilder sb = new StringBuilder();
        sb.append("{\"rows\" : [");
        
        if(ls_outamtMasterList != null){
            for (int i = 0; i < ls_outamtMasterList.size(); i++) {
                Tb_Outamt_Main tb_Outamt_Main = ls_outamtMasterList.get(i);
                sb.append("{\"id\":" + i);
                sb.append(",\"userdata\":{");
                sb.append("\"pay_mtd\":\""+tb_Outamt_Main.getPay_mtd()+"\"");
                sb.append(",\"merch_no\":\""+tb_Outamt_Main.getMerch_no()+"\"");
                sb.append(",\"gen_dt\":\""+tb_Outamt_Main.getGen_dt()+"\"");
                sb.append("}");                  
                sb.append(",\"data\":[");
                sb.append(" \"" + tb_Outamt_Main.getRnum() + "\"");
                sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getGen_dt()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getMid_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getMerch_no()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getOut_amt()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getCur_outamt()) + "\"");

                if (i == (ls_outamtMasterList.size() - 1)) {
                        sb.append("]}");
                } else {
                        sb.append("]},");
                }
            }        
        }

        sb.append("]}");    
        
        SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
        Date start_date = formatter.parse(outamtFormBean.getGen_start_dt());
        Date end_date   = formatter.parse(outamtFormBean.getGen_end_dt());
        
        String start_dt = formatter2.format(start_date);
        String end_dt = formatter2.format(end_date);

        outamtFormBean.setGen_start_dt(start_dt);
        outamtFormBean.setGen_end_dt(end_dt);

        model.addAttribute("outamtMasterList",sb.toString());       
        
        return null;
        
    }        
    
    //미수금 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/outamtMasterList")
    public @ResponseBody String outamtMasterList(@ModelAttribute OutamtFormBean outamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        
        Hashtable ht_outamtMasterList = outamtService.outamtMasterList(outamtFormBean);
        List<Tb_Outamt_Main> ls_outamtMasterList = (List<Tb_Outamt_Main>) ht_outamtMasterList.get("ResultSet");

        StringBuilder sb = new StringBuilder();
        sb.append("{\"rows\" : [");
        
        if(ls_outamtMasterList != null){
            for (int i = 0; i < ls_outamtMasterList.size(); i++) {
                Tb_Outamt_Main tb_Outamt_Main = ls_outamtMasterList.get(i);
                sb.append("{\"id\":" + i);
                sb.append(",\"userdata\":{");
                sb.append("\"pay_mtd\":\""+tb_Outamt_Main.getPay_mtd()+"\"");
                sb.append(",\"merch_no\":\""+tb_Outamt_Main.getMerch_no()+"\"");
                sb.append(",\"gen_dt\":\""+tb_Outamt_Main.getGen_dt()+"\"");
                sb.append("}");                  
                sb.append(",\"data\":[");
                sb.append(" \"" + tb_Outamt_Main.getRnum() + "\"");
                sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getGen_dt()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getMid_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getMerch_no()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getOut_amt()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getCur_outamt()) + "\"");

                if (i == (ls_outamtMasterList.size() - 1)) {
                        sb.append("]}");
                } else {
                        sb.append("]},");
                }
            }        
        }

        sb.append("]}");       

        return sb.toString();
        
    }          
    
    //미수금 상세 목록 조회
    @RequestMapping(method= RequestMethod.GET,value = "/outamtDetailList")
    public String outamtMasterDetailList(@ModelAttribute OutamtFormBean outamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        
        Hashtable ht_outamtDetailList = outamtService.outamtDetailList(outamtFormBean);
        List<Tb_Outamt_Main> ls_outamtDetailList = (List<Tb_Outamt_Main>) ht_outamtDetailList.get("ResultSet");

        StringBuilder sb = new StringBuilder();
        sb.append("{\"rows\" : [");
        if(ls_outamtDetailList != null){
            for (int i = 0; i < ls_outamtDetailList.size(); i++) {
                    Tb_Outamt_Main tb_Outamt_Main = ls_outamtDetailList.get(i);
                    sb.append("{\"id\":" + i);              
                    sb.append(",\"userdata\":{");
                    sb.append(" \"outamt_seq\":\""+tb_Outamt_Main.getOutamt_seq()+"\"");
                    sb.append("}");                      
                    sb.append(",\"data\":[");
                    sb.append(" \"" + tb_Outamt_Main.getRnum() + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getGen_dt()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getMid_nm()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getMerch_no()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getOut_amt()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getCur_outamt()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getIns_reson_nm()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getOutamt_stat_nm()) + "\"");

                    if (i == (ls_outamtDetailList.size() - 1)) {
                            sb.append("]}");
                    } else {
                            sb.append("]},");
                    }
            }            
        }

        sb.append("]}");    
        
        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("OUTAMT_STAT");
        Tb_Code_Main outamtStatList = commonService.codeSearch(commonCodeSearchFormBean);
        
        model.addAttribute("outamtDetailList",sb.toString()); 
        model.addAttribute("outamtStatList",outamtStatList);    
        
        return null;
        
    }



    //미수금 상세 목록 조회
    @RequestMapping(method= RequestMethod.GET,value = "/outamtDetailListWindow")
    public String outamtDetailListWindow(@ModelAttribute OutamtFormBean outamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_outamtDetailList = outamtService.outamtDetailList(outamtFormBean);
        List<Tb_Outamt_Main> ls_outamtDetailList = (List<Tb_Outamt_Main>) ht_outamtDetailList.get("ResultSet");

        StringBuilder sb = new StringBuilder();
        sb.append("{\"rows\" : [");
        for (int i = 0; i < ls_outamtDetailList.size(); i++) {
            Tb_Outamt_Main tb_Outamt_Main = ls_outamtDetailList.get(i);
            sb.append("{\"id\":" + i);
            sb.append(",\"userdata\":{");
            sb.append(" \"outamt_seq\":\""+tb_Outamt_Main.getOutamt_seq()+"\"");
            sb.append("}");
            sb.append(",\"data\":[");
            sb.append(" \"" + tb_Outamt_Main.getRnum() + "\"");
            sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getGen_dt()) + "\"");
            sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getMid_nm()) + "\"");
            sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getMerch_no()) + "\"");
            sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getOut_amt()) + "\"");
            sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getCur_outamt()) + "\"");
            sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getIns_reson_nm()) + "\"");
            sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getOutamt_stat_nm()) + "\"");

            if (i == (ls_outamtDetailList.size() - 1)) {
                sb.append("]}");
            } else {
                sb.append("]},");
            }
        }

        sb.append("]}");

        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

        commonCodeSearchFormBean.setMain_code("ACQ_CD");
        Tb_Code_Main acqCdList = commonService.codeSearch(commonCodeSearchFormBean);

        commonCodeSearchFormBean.setMain_code("OUTAMT_STAT");
        Tb_Code_Main outamtStatList = commonService.codeSearch(commonCodeSearchFormBean);

        model.addAttribute("outamtDetailList",sb.toString());
        model.addAttribute("acqCdList",acqCdList);
        model.addAttribute("outamtStatList",outamtStatList);
        model.addAttribute("comp_seq",outamtFormBean.getComp_seq());
        model.addAttribute("acq_cd",outamtFormBean.getAcq_cd());
        model.addAttribute("merch_no",outamtFormBean.getMerch_no());
        model.addAttribute("bank_cd",outamtFormBean.getBank_cd());
        model.addAttribute("acc_no",outamtFormBean.getAcc_no());
        model.addAttribute("gen_dt",outamtFormBean.getGen_dt());
        model.addAttribute("deposit_popup_yn",outamtFormBean.getDeposit_popup_yn());
        
        return null;

    }

    //미수금 상세 목록 조회
    @RequestMapping(method= RequestMethod.POST,value = "/outamtDetailList")
    public @ResponseBody String outamtDetailList(@ModelAttribute OutamtFormBean outamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {    

        Hashtable ht_outamtDetailList = outamtService.outamtDetailList(outamtFormBean);
        List<Tb_Outamt_Main> ls_outamtDetailList = (List<Tb_Outamt_Main>) ht_outamtDetailList.get("ResultSet");

        StringBuilder sb = new StringBuilder();
        sb.append("{\"rows\" : [");
        if(ls_outamtDetailList != null){
            for (int i = 0; i < ls_outamtDetailList.size(); i++) {
                    Tb_Outamt_Main tb_Outamt_Main = ls_outamtDetailList.get(i);
                    sb.append("{\"id\":" + i);              
                    sb.append(",\"userdata\":{");
                    sb.append(" \"outamt_seq\":\""+tb_Outamt_Main.getOutamt_seq()+"\"");
                    sb.append("}");                      
                    sb.append(",\"data\":[");
                    sb.append(" \"" + tb_Outamt_Main.getRnum() + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getGen_dt()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getMid_nm()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getMerch_no()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getOut_amt()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getCur_outamt()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getIns_reson_nm()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Outamt_Main.getOutamt_stat_nm()) + "\"");

                    if (i == (ls_outamtDetailList.size() - 1)) {
                            sb.append("]}");
                    } else {
                            sb.append("]},");
                    }
            }            
        }

        sb.append("]}");            
        
        return sb.toString();
        
    }

    
    //미수금 등록
    @RequestMapping(method= RequestMethod.GET,value = "/outamtMasterInsert")
    public String outamtMasterInsertForm(@Valid OutamtFormBean outamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("MID_PAY_MTD");
        Tb_Code_Main midPayMtdList = commonService.codeSearch(commonCodeSearchFormBean);
        
        commonCodeSearchFormBean.setMain_code("INS_RESON");
        Tb_Code_Main insResonList = commonService.codeSearch(commonCodeSearchFormBean);
        
        model.addAttribute("midPayMtdList",midPayMtdList);   
        model.addAttribute("insResonList",insResonList);   
        
        return null;
        
    }   
    
    //미수금 등록
    @RequestMapping(method= RequestMethod.POST,value = "/outamtMasterInsert")
    public @ResponseBody String outamtMasterInsert(@Valid OutamtFormBean outamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        outamtFormBean.setUser_seq(siteSession.getUser_seq());
        String outamt_seq = outamtService.outamtMasterInsert(outamtFormBean);
        
        return outamt_seq;
        
    }           
    
    //미수금 상계
    @RequestMapping(method= RequestMethod.GET,value = "/outamtMasterUpdate")
    public String outamtMasterUpdateForm(@Valid OutamtFormBean outamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_outamtDetailList = outamtService.outamtDetailList(outamtFormBean);
        List<Tb_Outamt_Main> ls_outamtDetailList = (List<Tb_Outamt_Main>) ht_outamtDetailList.get("ResultSet");
        
        Hashtable ht_setoffinfoList = outamtService.setoffinfoList(outamtFormBean);
        List<Tb_Setoffinfo> ls_setoffinfoList = (List<Tb_Setoffinfo>) ht_setoffinfoList.get("ResultSet");
        
        StringBuilder sb = new StringBuilder();
        sb.append("{\"rows\" : [");
        for (int i = 0; i < ls_setoffinfoList.size(); i++) {
                Tb_Setoffinfo tb_Setoffinfo = ls_setoffinfoList.get(i);
                sb.append("{\"id\":" + tb_Setoffinfo.getSetoff_seq());          
                sb.append(",\"userdata\":{");
                sb.append(" \"outamt_seq\":\""+tb_Setoffinfo.getOutamt_seq()+"\"");
                sb.append(",\"setoff_seq\":\""+tb_Setoffinfo.getSetoff_seq()+"\"");
                sb.append(",\"setoff_amt\":\""+tb_Setoffinfo.getSetoff_amt()+"\"");
                sb.append(",\"setoff_date\":\""+tb_Setoffinfo.getSetoff_date()+"\"");
                sb.append("}");                                      
                sb.append(",\"data\":[");
                sb.append(" \"" + tb_Setoffinfo.getRnum() + "\"");
                sb.append(",\"" + "" + "\"");
                sb.append(",\"" + Util.nullToString(tb_Setoffinfo.getSetoff_date()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Setoffinfo.getSetoff_cate_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Setoffinfo.getSetoff_amt()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Setoffinfo.getMemo()) + "\"");

                if (i == (ls_setoffinfoList.size() - 1)) {
                        sb.append("]}");
                } else {
                        sb.append("]},");
                }
        }

        sb.append("]}");           
        
        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("SETOFF_CATE");
        Tb_Code_Main setoffCateList = commonService.codeSearch(commonCodeSearchFormBean);  
        
        commonCodeSearchFormBean.setMain_code("INS_RESON");
        Tb_Code_Main insResonList = commonService.codeSearch(commonCodeSearchFormBean);
        
        model.addAttribute("ls_outamtDetailList",ls_outamtDetailList);
        model.addAttribute("ls_setoffinfoList",sb.toString());
        model.addAttribute("setoffCateList",setoffCateList);
        model.addAttribute("insResonList",insResonList);
        
        return null;
        
    }       
    
    //미수금 수정
    @RequestMapping(method= RequestMethod.POST,value = "/outamtMasterUpdate")
    public @ResponseBody String outamtMasterUpdate(@Valid OutamtFormBean outamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        outamtFormBean.setUser_seq(siteSession.getUser_seq());
        outamtService.outamtMasterUpdate(outamtFormBean);
        
        return null;
        
    }              
    
    //미수금 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/outamtMasterDelete")
    public @ResponseBody String outamtMasterDelete(@Valid OutamtFormBean outamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        outamtFormBean.setUser_seq(siteSession.getUser_seq());
        outamtService.outamtMasterDelete(outamtFormBean);
        
        return null;
        
    }             
    
    //미수금 상계 등록
    @RequestMapping(method= RequestMethod.POST,value = "/outamtDetailInsert")
    public @ResponseBody String outamtDetailInsert(@Valid OutamtFormBean outamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        outamtFormBean.setUser_seq(siteSession.getUser_seq());
        outamtService.outamtDetailInsert(outamtFormBean);
        
        return null;
        
    }              
    
    //상계 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/outamtSetoffDelete")
    public @ResponseBody String outamtSetoffDelete(@Valid OutamtFormBean outamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        logger.debug("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa = " + outamtFormBean.getSetoff_seqs()[0]); 
        
        outamtFormBean.setUser_seq(siteSession.getUser_seq());        
        outamtService.outamtSetoffDelete(outamtFormBean);
        
        return null;
        
    }                 
    
    //매입사별 미수금 현황 조회
    @RequestMapping(method= RequestMethod.GET,value = "/acq_outamtMasterList")
    public String acq_outamtMasterFormList(@ModelAttribute OutamtFormBean outamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        
        Hashtable ht_acq_outamtMasterList = outamtService.acq_outamtMasterList(outamtFormBean);
        String ls_acq_outamtMasterList = (String) ht_acq_outamtMasterList.get("ResultSet");
        
        model.addAttribute("acq_outamtMasterList",ls_acq_outamtMasterList);       
        
        return null;
        
    }            
    
    //매입사별 미수금 현황 상세 조회
    @RequestMapping(method= RequestMethod.GET,value = "/acq_outamtDetailList")
    public String acq_outamtDetailList(@ModelAttribute OutamtFormBean outamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        
        logger.debug("111111111111111111111111111111111111111111111");
        
        Hashtable ht_acq_outamtDetailList = outamtService.acq_outamtDetailList(outamtFormBean);
        String ls_acq_outamtDetailList = (String) ht_acq_outamtDetailList.get("ResultSet");
        
        model.addAttribute("acq_outamtDetailList",ls_acq_outamtDetailList);       
        
        return null;
        
    }          
    
    //미수금 집계 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/outamtMasterListExcel")
    public void outamtMasterListExcel(@Valid OutamtFormBean outamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("outamt.outamtMasterListExcel.fileName"));

        outamtService.outamtMasterListExcel(outamtFormBean, excelDownloadFilename, response); 
        
    }                
    
    //미수금 상세 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/outamtDetailListExcel")
    public void outamtDetailListExcel(@Valid OutamtFormBean outamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("outamt.outamtDetailListExcel.fileName"));

        outamtService.outamtDetailListExcel(outamtFormBean, excelDownloadFilename, response); 
        
    }         
    
}
