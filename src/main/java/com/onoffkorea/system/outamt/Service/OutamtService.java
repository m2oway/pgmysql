/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.outamt.Service;

import com.onoffkorea.system.outamt.Bean.OutamtFormBean;
import java.util.Hashtable;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public interface OutamtService {

    public Hashtable outamtMasterList(OutamtFormBean outamtFormBean);

    public Hashtable outamtDetailList(OutamtFormBean outamtFormBean);

    public String outamtMasterInsert(OutamtFormBean outamtFormBean);

    public Hashtable setoffinfoList(OutamtFormBean outamtFormBean);

    public void outamtMasterUpdate(OutamtFormBean outamtFormBean);

    public void outamtMasterDelete(OutamtFormBean outamtFormBean);

    public void outamtDetailInsert(OutamtFormBean outamtFormBean);

    public void outamtSetoffDelete(OutamtFormBean outamtFormBean);

    public Hashtable acq_outamtMasterList(OutamtFormBean outamtFormBean);

    public Hashtable acq_outamtDetailList(OutamtFormBean outamtFormBean);

    public void outamtMasterListExcel(OutamtFormBean outamtFormBean, String excelDownloadFilename, HttpServletResponse response);

    public void outamtDetailListExcel(OutamtFormBean outamtFormBean, String excelDownloadFilename, HttpServletResponse response);
    
}
