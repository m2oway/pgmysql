/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.outamt.Service;

import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.outamt.Bean.OutamtFormBean;
import com.onoffkorea.system.outamt.Dao.OutamtDAO;
import com.onoffkorea.system.outamt.Vo.Tb_Stsoso;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service("outamtService")
public class OutamtServiceImpl implements OutamtService{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private OutamtDAO outamtDAO;    

    //미수금 조회
    @Override
    public Hashtable outamtMasterList(OutamtFormBean outamtFormBean) {
        
        Hashtable ht = new Hashtable();

        try {
                ht.put("ResultSet", this.outamtDAO.outamtMasterList(outamtFormBean));
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;
        
    }

    //미수금 상세 목록 조회
    @Override
    public Hashtable outamtDetailList(OutamtFormBean outamtFormBean) {

        Hashtable ht = new Hashtable();

        try {
                ht.put("ResultSet", this.outamtDAO.outamtDetailList(outamtFormBean));
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;        
        
    }

    //미수금 등록
    @Override
    @Transactional
    public String outamtMasterInsert(OutamtFormBean outamtFormBean) {
        
        String outamt_seq = outamtDAO.outamtMasterInsert(outamtFormBean);

//        outamtFormBean.setGen_dt(outamtFormBean.getGen_dt().replaceAll("-", ""));
//        outamtDAO.outamtMasterInsert_Make_Outamt_Balance(outamtFormBean);
        
        return outamt_seq;
        
    }

    //미수금 상계 조회
    @Override
    public Hashtable setoffinfoList(OutamtFormBean outamtFormBean) {
        
        Hashtable ht = new Hashtable();

        try {
                ht.put("ResultSet", this.outamtDAO.setoffinfoList(outamtFormBean));
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;         
        
    }

    //미수금 수정
    @Override
    @Transactional
    public void outamtMasterUpdate(OutamtFormBean outamtFormBean) {
        
        outamtDAO.outamtMasterUpdate(outamtFormBean);
        
    }

    //미수금 삭제
    @Override
    @Transactional
    public void outamtMasterDelete(OutamtFormBean outamtFormBean) {
        
        outamtDAO.outamtMasterDelete(outamtFormBean);
        
//        outamtFormBean.setGen_dt(outamtFormBean.getGen_dt().replaceAll("-", ""));
//        outamtDAO.outamtMasterInsert_Make_Outamt_Balance(outamtFormBean);
        
    }

    //미수금 상계 등록
    @Override
    @Transactional
    public void outamtDetailInsert(OutamtFormBean outamtFormBean) {
        
        outamtDAO.outamtDetailInsert(outamtFormBean);
        //미수잔액 수정
        outamtDAO.outamtMasterUpdate_minusoutamt(outamtFormBean);
        
//        outamtFormBean.setSetoff_date(outamtFormBean.getSetoff_date().replaceAll("-", ""));
//        outamtDAO.outamtDetailInsert_Make_Outamt_Balance(outamtFormBean);
        
    }

    //상계 삭제
    @Override
    @Transactional
    public void outamtSetoffDelete(OutamtFormBean outamtFormBean) {
        
        outamtDAO.outamtSetoffDelete(outamtFormBean);
        
//        String[] setoff_dates = outamtFormBean.getSetoff_date().split(",");
//        
//        for(String setoff_date : setoff_dates){
//            outamtFormBean.setSetoff_date(setoff_date);
//            outamtDAO.outamtDetailInsert_Make_Outamt_Balance(outamtFormBean);
//        }
        
        outamtDAO.outamtMasterUpdate_plusoutamt(outamtFormBean);
        
    }

    //매입사별 미수금 현황
    @Override
    public Hashtable acq_outamtMasterList(OutamtFormBean outamtFormBean) {
        
        Hashtable ht = new Hashtable();

        try {
            
                List<Tb_Stsoso> ls_acq_outamtMasterList = outamtDAO.acq_outamtMasterList(outamtFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"rows\" : [");
                for (int i = 0; i < ls_acq_outamtMasterList.size(); i++) {
                        Tb_Stsoso tb_stsoso = ls_acq_outamtMasterList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"acq_cd\":\""+tb_stsoso.getAcq_cd()+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_stsoso.getAcq_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_stsoso.getUnpay_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_stsoso.getOffset_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_stsoso.getCur_unpay_amt()) + "\"");

                        if (i == (ls_acq_outamtMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                ht.put("ResultSet", sb.toString());
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;                
        
    }

    //매입사별 미수잔액 현황 상세 조회
    @Override
    public Hashtable acq_outamtDetailList(OutamtFormBean outamtFormBean) {

        logger.debug("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
        
        Hashtable ht = new Hashtable();

        try {
            
                List<Tb_Stsoso> ls_acq_outamtDetailList = outamtDAO.acq_outamtDetailList(outamtFormBean);
                
                logger.debug("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"rows\" : [");
                for (int i = 0; i < ls_acq_outamtDetailList.size(); i++) {
                        Tb_Stsoso tb_stsoso = ls_acq_outamtDetailList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_stsoso.getAcq_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_stsoso.getNrcv_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_stsoso.getUnpay_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_stsoso.getOffset_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_stsoso.getBalance()) + "\"");

                        if (i == (ls_acq_outamtDetailList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                logger.debug("ccccccccccccccccccccccccccccccccccccccc");
                
                ht.put("ResultSet", sb.toString());
        } catch (Exception e) {
                e.printStackTrace();
        }

        logger.debug("ddddddddddddddddddddddddddddddddddddddddddd");
        
        return ht;                  
        
    }

    //미수금 집계 엑셀다운로드
    @Override
    public void outamtMasterListExcel(OutamtFormBean outamtFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
             
                List ls_outamtMasterListExcel = outamtDAO.outamtMasterListExcel(outamtFormBean);
                
                StringBuffer outamtMasterListExcel = Util.makeData(ls_outamtMasterListExcel);
        
                Util.exceldownload(outamtMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    }
    
    //미수금 상세 엑셀다운로드
    @Override
    public void outamtDetailListExcel(OutamtFormBean outamtFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
             
                List ls_outamtDetailListExcel = outamtDAO.outamtDetailListExcel(outamtFormBean);
                
                StringBuffer outamtDetailListExcel = Util.makeData(ls_outamtDetailListExcel);
        
                Util.exceldownload(outamtDetailListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    }    
    
}
