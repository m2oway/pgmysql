/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.outamt.Bean;

/**
 *
 * @author Administrator
 */
public class OutamtFormBean {
    
    private String outamt_seq   ;
    private String gen_dt       ;
    private String comp_seq     ;
    private String acq_cd       ;
    private String merch_no     ;
    private String bank_cd      ;
    private String acc_no       ;
    private String out_amt      ;
    private String cur_outamt   ;
    private String ins_reson    ;
    private String proc_method  ;
    private String memo         ;
    private String ins_dt       ;
    private String mod_dt       ;
    private String ins_user     ;
    private String mod_user     ;
    private String setoff_seq   ;
    private String setoff_cate  ;
    private String setoff_amt   ;
    private String setoff_date  ;
    private String gen_start_dt ;
    private String gen_end_dt   ;
    private String outamt_stat  ;
    private String user_seq     ;
    private String[] setoff_seqs  ;
    private String pay_mtd;
    private String mid_nm;
    private String pay_mtd_nm;
    private String gen_reson;
    private String deposit_popup_yn;

    public String getDeposit_popup_yn() {
        return deposit_popup_yn;
    }

    public void setDeposit_popup_yn(String deposit_popup_yn) {
        this.deposit_popup_yn = deposit_popup_yn;
    }

    public String getGen_reson() {
        return gen_reson;
    }

    public void setGen_reson(String gen_reson) {
        this.gen_reson = gen_reson;
    }

    public String getPay_mtd() {
        return pay_mtd;
    }

    public void setPay_mtd(String pay_mtd) {
        this.pay_mtd = pay_mtd;
    }

    public String getMid_nm() {
        return mid_nm;
    }

    public void setMid_nm(String mid_nm) {
        this.mid_nm = mid_nm;
    }

    public String getPay_mtd_nm() {
        return pay_mtd_nm;
    }

    public void setPay_mtd_nm(String pay_mtd_nm) {
        this.pay_mtd_nm = pay_mtd_nm;
    }

    public String[] getSetoff_seqs() {
        return setoff_seqs;
    }

    public void setSetoff_seqs(String[] setoff_seqs) {
        this.setoff_seqs = setoff_seqs;
    }

    public String getUser_seq() {
        return user_seq;
    }

    public void setUser_seq(String user_seq) {
        this.user_seq = user_seq;
    }

    public String getOutamt_stat() {
        return outamt_stat;
    }

    public void setOutamt_stat(String outamt_stat) {
        this.outamt_stat = outamt_stat;
    }

    public String getGen_start_dt() {
        return gen_start_dt;
    }

    public void setGen_start_dt(String gen_start_dt) {
        this.gen_start_dt = gen_start_dt;
    }

    public String getGen_end_dt() {
        return gen_end_dt;
    }

    public void setGen_end_dt(String gen_end_dt) {
        this.gen_end_dt = gen_end_dt;
    }

    public String getBank_cd() {
        return bank_cd;
    }

    public void setBank_cd(String bank_cd) {
        this.bank_cd = bank_cd;
    }

    public String getIns_reson() {
        return ins_reson;
    }

    public void setIns_reson(String ins_reson) {
        this.ins_reson = ins_reson;
    }

    public String getSetoff_seq() {
        return setoff_seq;
    }

    public void setSetoff_seq(String setoff_seq) {
        this.setoff_seq = setoff_seq;
    }

    public String getSetoff_cate() {
        return setoff_cate;
    }

    public void setSetoff_cate(String setoff_cate) {
        this.setoff_cate = setoff_cate;
    }

    public String getSetoff_amt() {
        return setoff_amt;
    }

    public void setSetoff_amt(String setoff_amt) {
        this.setoff_amt = setoff_amt;
    }

    public String getSetoff_date() {
        return setoff_date;
    }

    public void setSetoff_date(String setoff_date) {
        this.setoff_date = setoff_date;
    }
    
    public String getComp_seq() {
        return comp_seq;
    }

    public void setComp_seq(String comp_seq) {
        this.comp_seq = comp_seq;
    }

    public String getAcq_cd() {
        return acq_cd;
    }

    public void setAcq_cd(String acq_cd) {
        this.acq_cd = acq_cd;
    }
    
    public String getOutamt_seq() {
        return outamt_seq;
    }

    public void setOutamt_seq(String outamt_seq) {
        this.outamt_seq = outamt_seq;
    }

    public String getGen_dt() {
        return gen_dt;
    }

    public void setGen_dt(String gen_dt) {
        this.gen_dt = gen_dt;
    }

    public String getMerch_no() {
        return merch_no;
    }

    public void setMerch_no(String merch_no) {
        this.merch_no = merch_no;
    }

    public String getAcc_no() {
        return acc_no;
    }

    public void setAcc_no(String acc_no) {
        this.acc_no = acc_no;
    }

    public String getOut_amt() {
        return out_amt;
    }

    public void setOut_amt(String out_amt) {
        this.out_amt = out_amt;
    }

    public String getCur_outamt() {
        return cur_outamt;
    }

    public void setCur_outamt(String cur_outamt) {
        this.cur_outamt = cur_outamt;
    }

    public String getProc_method() {
        return proc_method;
    }

    public void setProc_method(String proc_method) {
        this.proc_method = proc_method;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    @Override
    public String toString() {
        return "OutamtFormBean{" + "outamt_seq=" + outamt_seq + ", gen_dt=" + gen_dt + ", comp_seq=" + comp_seq + ", acq_cd=" + acq_cd + ", merch_no=" + merch_no + ", bank_cd=" + bank_cd + ", acc_no=" + acc_no + ", out_amt=" + out_amt + ", cur_outamt=" + cur_outamt + ", ins_reson=" + ins_reson + ", proc_method=" + proc_method + ", memo=" + memo + ", ins_dt=" + ins_dt + ", mod_dt=" + mod_dt + ", ins_user=" + ins_user + ", mod_user=" + mod_user + ", setoff_seq=" + setoff_seq + ", setoff_cate=" + setoff_cate + ", setoff_amt=" + setoff_amt + ", setoff_date=" + setoff_date + ", gen_start_dt=" + gen_start_dt + ", gen_end_dt=" + gen_end_dt + ", outamt_stat=" + outamt_stat + ", user_seq=" + user_seq + ", setoff_seqs=" + setoff_seqs + '}';
    }
    
}
