/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.outamt.Vo;

/**
 *
 * @author Administrator
 */
public class Tb_Outamt_Detail {
    
    private String detail_cate  ;
    private String ins_dt       ;
    private String mod_dt       ;
    private String ins_user     ;
    private String mod_user     ;
    private String mst_out_seq  ;
    private String sub_out_seq  ;    

    public String getDetail_cate() {
        return detail_cate;
    }

    public void setDetail_cate(String detail_cate) {
        this.detail_cate = detail_cate;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    public String getMst_out_seq() {
        return mst_out_seq;
    }

    public void setMst_out_seq(String mst_out_seq) {
        this.mst_out_seq = mst_out_seq;
    }

    public String getSub_out_seq() {
        return sub_out_seq;
    }

    public void setSub_out_seq(String sub_out_seq) {
        this.sub_out_seq = sub_out_seq;
    }
    
}
