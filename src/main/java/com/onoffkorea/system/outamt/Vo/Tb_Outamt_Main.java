/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.outamt.Vo;

import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class Tb_Outamt_Main {

    private String outamt_seq   ;
    private String gen_dt       ;
    private String comp_seq     ;
    private String acq_cd       ;
    private String merch_no     ;
    private String bank_cd      ;
    private String acc_no       ;
    private String out_amt      ;
    private String cur_outamt   ;
    private String ins_reson    ;
    private String proc_method  ;
    private String memo         ;
    private String outamt_stat  ;
    private String ins_dt       ;
    private String mod_dt       ;
    private String ins_user     ;
    private String mod_user     ;
    private String tot_outamt   ;
    private String comp_nm      ;
    private String acq_nm       ;
    private String rnum         ;
    private String ins_reson_nm;
    private String outamt_stat_nm;
    private String bank_nm;
    private String proc_flag;
    private ArrayList<Tb_Setoffinfo> outamtDetailList;
    private String pay_mtd;
    private String mid_nm;
    private String pay_mtd_nm;
    private String gen_reson;

    public String getGen_reson() {
        return gen_reson;
    }

    public void setGen_reson(String gen_reson) {
        this.gen_reson = gen_reson;
    }

    public String getPay_mtd_nm() {
        return pay_mtd_nm;
    }

    public void setPay_mtd_nm(String pay_mtd_nm) {
        this.pay_mtd_nm = pay_mtd_nm;
    }

    public String getPay_mtd() {
        return pay_mtd;
    }

    public void setPay_mtd(String pay_mtd) {
        this.pay_mtd = pay_mtd;
    }

    public String getMid_nm() {
        return mid_nm;
    }

    public void setMid_nm(String mid_nm) {
        this.mid_nm = mid_nm;
    }

    public String getProc_flag() {
        return proc_flag;
    }

    public void setProc_flag(String proc_flag) {
        this.proc_flag = proc_flag;
    }

    public String getBank_nm() {
        return bank_nm;
    }

    public void setBank_nm(String bank_nm) {
        this.bank_nm = bank_nm;
    }

    public String getIns_reson_nm() {
        return ins_reson_nm;
    }

    public void setIns_reson_nm(String ins_reson_nm) {
        this.ins_reson_nm = ins_reson_nm;
    }

    public String getOutamt_stat_nm() {
        return outamt_stat_nm;
    }

    public void setOutamt_stat_nm(String outamt_stat_nm) {
        this.outamt_stat_nm = outamt_stat_nm;
    }

    public String getBank_cd() {
        return bank_cd;
    }

    public void setBank_cd(String bank_cd) {
        this.bank_cd = bank_cd;
    }

    public String getComp_seq() {
        return comp_seq;
    }

    public void setComp_seq(String comp_seq) {
        this.comp_seq = comp_seq;
    }

    public String getIns_reson() {
        return ins_reson;
    }

    public void setIns_reson(String ins_reson) {
        this.ins_reson = ins_reson;
    }

    public String getOutamt_stat() {
        return outamt_stat;
    }

    public void setOutamt_stat(String outamt_stat) {
        this.outamt_stat = outamt_stat;
    }

    public ArrayList<Tb_Setoffinfo> getOutamtDetailList() {
        return outamtDetailList;
    }

    public void setOutamtDetailList(ArrayList<Tb_Setoffinfo> outamtDetailList) {
        this.outamtDetailList = outamtDetailList;
    }

    public String getRnum() {
        return rnum;
    }

    public void setRnum(String rnum) {
        this.rnum = rnum;
    }

    public String getComp_nm() {
        return comp_nm;
    }

    public void setComp_nm(String comp_nm) {
        this.comp_nm = comp_nm;
    }

    public String getAcq_nm() {
        return acq_nm;
    }

    public void setAcq_nm(String acq_nm) {
        this.acq_nm = acq_nm;
    }

    public String getTot_outamt() {
        return tot_outamt;
    }

    public void setTot_outamt(String tot_outamt) {
        this.tot_outamt = tot_outamt;
    }

    public String getAcq_cd() {
        return acq_cd;
    }

    public void setAcq_cd(String acq_cd) {
        this.acq_cd = acq_cd;
    }

    public String getOutamt_seq() {
        return outamt_seq;
    }

    public void setOutamt_seq(String outamt_seq) {
        this.outamt_seq = outamt_seq;
    }

    public String getGen_dt() {
        return gen_dt;
    }

    public void setGen_dt(String gen_dt) {
        this.gen_dt = gen_dt;
    }

    public String getMerch_no() {
        return merch_no;
    }

    public void setMerch_no(String merch_no) {
        this.merch_no = merch_no;
    }

    public String getAcc_no() {
        return acc_no;
    }

    public void setAcc_no(String acc_no) {
        this.acc_no = acc_no;
    }

    public String getOut_amt() {
        return out_amt;
    }

    public void setOut_amt(String out_amt) {
        this.out_amt = out_amt;
    }

    public String getCur_outamt() {
        return cur_outamt;
    }

    public void setCur_outamt(String cur_outamt) {
        this.cur_outamt = cur_outamt;
    }

    public String getProc_method() {
        return proc_method;
    }

    public void setProc_method(String proc_method) {
        this.proc_method = proc_method;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }
    
}
