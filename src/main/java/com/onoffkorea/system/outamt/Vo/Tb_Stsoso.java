/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.outamt.Vo;

/**
 *
 * @author Administrator
 */
public class Tb_Stsoso {
    
    private String acq_cd      ;
    private String nrcv_dt     ;
    private String unpay_amt   ;
    private String offset_amt  ;
    private String balance     ;    
    private String cur_unpay_amt;
    private String acq_nm;

    public String getAcq_nm() {
        return acq_nm;
    }

    public void setAcq_nm(String acq_nm) {
        this.acq_nm = acq_nm;
    }

    public String getCur_unpay_amt() {
        return cur_unpay_amt;
    }

    public void setCur_unpay_amt(String cur_unpay_amt) {
        this.cur_unpay_amt = cur_unpay_amt;
    }

    public String getAcq_cd() {
        return acq_cd;
    }

    public void setAcq_cd(String acq_cd) {
        this.acq_cd = acq_cd;
    }

    public String getNrcv_dt() {
        return nrcv_dt;
    }

    public void setNrcv_dt(String nrcv_dt) {
        this.nrcv_dt = nrcv_dt;
    }

    public String getUnpay_amt() {
        return unpay_amt;
    }

    public void setUnpay_amt(String unpay_amt) {
        this.unpay_amt = unpay_amt;
    }

    public String getOffset_amt() {
        return offset_amt;
    }

    public void setOffset_amt(String offset_amt) {
        this.offset_amt = offset_amt;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }
    
}
