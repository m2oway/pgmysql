/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.outamt.Dao;

import com.onoffkorea.system.outamt.Bean.OutamtFormBean;
import com.onoffkorea.system.outamt.Vo.Tb_Outamt_Main;
import com.onoffkorea.system.outamt.Vo.Tb_Setoffinfo;
import com.onoffkorea.system.outamt.Vo.Tb_Stsoso;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("outamtDAO")
public class OutamtDAOImpl extends SqlSessionDaoSupport implements OutamtDAO{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    //미수금 조회
    @Override
    public List<Tb_Outamt_Main> outamtMasterList(OutamtFormBean outamtFormBean) {
        
        return (List)getSqlSession().selectList("Outamt.outamtMasterList",outamtFormBean);
        
    }

    //미수금 상세 목록 조회
    @Override
    public List<Tb_Outamt_Main> outamtDetailList(OutamtFormBean outamtFormBean) {
        
        return (List)getSqlSession().selectList("Outamt.outamtDetailList",outamtFormBean);
        
    }

    //미수금 등록
    @Override
    public String outamtMasterInsert(OutamtFormBean outamtFormBean) {
        
        Integer result = getSqlSession().insert("Outamt.outamtMasterInsert", outamtFormBean);
        
        return result.toString();
        
    }

    //미수금 상계 조회
    @Override
    public List<Tb_Setoffinfo> setoffinfoList(OutamtFormBean outamtFormBean) {
        
        return (List)getSqlSession().selectList("Outamt.setoffinfoList",outamtFormBean);
        
    }

    //미수금 수정
    @Override
    public void outamtMasterUpdate(OutamtFormBean outamtFormBean) {
        
        getSqlSession().update("Outamt.outamtMasterUpdate", outamtFormBean);
        
    }

    //미수금 삭제
    @Override
    public void outamtMasterDelete(OutamtFormBean outamtFormBean) {
        
        getSqlSession().update("Outamt.outamtMasterDelete", outamtFormBean);
        
    }

    //미수금 상계 등록
    @Override
    public void outamtDetailInsert(OutamtFormBean outamtFormBean) {
        
        getSqlSession().insert("Outamt.outamtDetailInsert", outamtFormBean);
        
    }

    //미수금 상계 후 미수잔액 수정
    @Override
    public void outamtMasterUpdate_minusoutamt(OutamtFormBean outamtFormBean) {
        
        getSqlSession().update("Outamt.outamtMasterUpdate_minusoutamt", outamtFormBean);
        
    }

    //상계 삭제
    @Override
    public void outamtSetoffDelete(OutamtFormBean outamtFormBean) {
        
        getSqlSession().delete("Outamt.outamtSetoffDelete", outamtFormBean);
        
    }

    //상계 삭제 후 미수잔액 수정
    @Override
    public void outamtMasterUpdate_plusoutamt(OutamtFormBean outamtFormBean) {
        
        getSqlSession().update("Outamt.outamtMasterUpdate_plusoutamt", outamtFormBean);
        
    }

    //매입사별 미수잔액 현황
    @Override
    public List<Tb_Stsoso> acq_outamtMasterList(OutamtFormBean outamtFormBean) {
        
        return (List)getSqlSession().selectList("Outamt.acq_outamtMasterList",outamtFormBean);
        
    }

    //매입사별 미수잔액 현황 상세 조회
    @Override
    public List<Tb_Stsoso> acq_outamtDetailList(OutamtFormBean outamtFormBean) {
        
        return (List)getSqlSession().selectList("Outamt.acq_outamtDetailList",outamtFormBean);
        
    }

    //미수금 등록, 삭제시 매입사별 미수잔액 생성 프로시져 호출
    @Override
    public void outamtMasterInsert_Make_Outamt_Balance(OutamtFormBean outamtFormBean) {
        
        logger.debug("asdfsadfsdafsdafsdafsdfsdfsdfsdfsdfsdfsdfsadf");
        
        getSqlSession().selectOne("Outamt.outamtMasterInsert_Make_Outamt_Balance", outamtFormBean);
        
    }

    //미수금 상계시 매입사별 미수잔액 생성 프로시져 호출
    @Override
    public void outamtDetailInsert_Make_Outamt_Balance(OutamtFormBean outamtFormBean) {
        
        getSqlSession().selectOne("Outamt.outamtDetailInsert_Make_Outamt_Balance", outamtFormBean);
        
    }

    //미수금 집계 엑셀다운로드
    @Override
    public List outamtMasterListExcel(OutamtFormBean outamtFormBean) {
        
        return (List)getSqlSession().selectList("Outamt.outamtMasterListExcel",outamtFormBean);
        
    }
    
    //미수금 상세 엑셀다운로드
    @Override
    public List outamtDetailListExcel(OutamtFormBean outamtFormBean) {
        
        return (List)getSqlSession().selectList("Outamt.outamtDetailListExcel",outamtFormBean);
        
    }    
    
}
