/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.outamt.Dao;

import com.onoffkorea.system.outamt.Bean.OutamtFormBean;
import com.onoffkorea.system.outamt.Vo.Tb_Outamt_Main;
import com.onoffkorea.system.outamt.Vo.Tb_Setoffinfo;
import com.onoffkorea.system.outamt.Vo.Tb_Stsoso;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface OutamtDAO {

    public List<Tb_Outamt_Main> outamtMasterList(OutamtFormBean outamtFormBean);

    public List<Tb_Outamt_Main> outamtDetailList(OutamtFormBean outamtFormBean);

    public String outamtMasterInsert(OutamtFormBean outamtFormBean);

    public List<Tb_Setoffinfo> setoffinfoList(OutamtFormBean outamtFormBean);

    public void outamtMasterUpdate(OutamtFormBean outamtFormBean);

    public void outamtMasterDelete(OutamtFormBean outamtFormBean);

    public void outamtDetailInsert(OutamtFormBean outamtFormBean);

    public void outamtMasterUpdate_minusoutamt(OutamtFormBean outamtFormBean);

    public void outamtSetoffDelete(OutamtFormBean outamtFormBean);

    public void outamtMasterUpdate_plusoutamt(OutamtFormBean outamtFormBean);

    public List<Tb_Stsoso> acq_outamtMasterList(OutamtFormBean outamtFormBean);

    public List<Tb_Stsoso> acq_outamtDetailList(OutamtFormBean outamtFormBean);

    public void outamtMasterInsert_Make_Outamt_Balance(OutamtFormBean outamtFormBean);

    public void outamtDetailInsert_Make_Outamt_Balance(OutamtFormBean outamtFormBean);

    public List outamtMasterListExcel(OutamtFormBean outamtFormBean);

    public List outamtDetailListExcel(OutamtFormBean outamtFormBean);
    
}
