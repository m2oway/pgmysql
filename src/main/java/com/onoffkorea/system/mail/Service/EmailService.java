package com.onoffkorea.system.mail.service;

import com.onoffkorea.system.mail.vo.EmailVO;

/**
 * Created by hoyeongheo on 15. 2. 9..
 */
public interface EmailService {

    public boolean sendMail(EmailVO email) throws Exception;

}
