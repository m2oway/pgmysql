package com.onoffkorea.system.mail.service;

import com.onoffkorea.system.mail.vo.EmailVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


/**
 * Created by hoyeongheo on 15. 2. 9..
 */

@Service("emailService")
public class EmailServiceImpl implements EmailService {

    @Autowired
    protected JavaMailSender mailSender;

    @Override
    public boolean sendMail(EmailVO email) throws Exception {

        MimeMessage msg = mailSender.createMimeMessage();
        msg.setFrom(email.getSender()); // 송신자를 설정해도 소용없지만 없으면 오류가 발생한다
        msg.setSubject(email.getSubject());
        //msg.setText(email.getContent());
        msg.setContent(email.getContent(),  "text/html; charset=utf-8");
        msg.setRecipient(MimeMessage.RecipientType.TO , new InternetAddress(email.getReceiver()));

        mailSender.send(msg);
        return true;
    }
}
