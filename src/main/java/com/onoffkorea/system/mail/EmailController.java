package com.onoffkorea.system.mail;

import com.onoffkorea.system.mail.service.EmailService;
import com.onoffkorea.system.mail.vo.EmailVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by hoyeongheo on 15. 2. 9..
 */

@Controller
@RequestMapping("/email/**")
public class EmailController {

    @Autowired
    private EmailService emailService;


    @RequestMapping(method= RequestMethod.POST,value = "/send")
    @ResponseBody
    public String sendMail() throws Exception{

        EmailVO email = new EmailVO();

        String receiver = "hoyeong.heo@vub.co.kr"; //Receiver.
        String subject = "메일보내기 테스트";
        String content = "dddddddd 테스트";


        email.setReceiver(receiver);
        email.setSubject(subject);
        email.setContent(content);
        boolean result = emailService.sendMail(email);

        return "Mail Send: "+result;

    }

}
