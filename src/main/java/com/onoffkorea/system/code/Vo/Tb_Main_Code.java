/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.code.Vo;

/**
 *
 * @author Administrator
 */
public class Tb_Main_Code {
      //메인코드 조회
    private String main_code   ;
    private String code_nm    ;
    private String code_memo   ;
    private String del_flag   ;
    private String ins_dt   ;
    private String mod_dt   ;
    private String ins_user   ;
    private String mod_user   ;
    private String rnum;
     private String use_flag ;

   
    
    //상세코드 조회
    private String detail_code   ;
    private String detailcode_nm   ;
    private String sort    ;
    private String decode_no    ;
    private String main_code_nm;

    
     public String getUse_flag() {
        return use_flag;
    }

    public void setUse_flag(String use_flag) {
        this.use_flag = use_flag;
    }
    
    public String getMain_code_nm() {
        return main_code_nm;
    }

    public void setMain_code_nm(String main_code_nm) {
        this.main_code_nm = main_code_nm;
    }

    
    
    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getRnum() {
        return rnum;
    }

    public void setRnum(String rnum) {
        this.rnum = rnum;
    }
    
    
      public String getDetailcode_nm() {
        return detailcode_nm;
    }

    public void setDetailcode_nm(String detailcode_nm) {
        this.detailcode_nm = detailcode_nm;
    }
    
        //메인코드 조회
    public String getMain_code() {
        return main_code;
    }
    public void setMain_code(String main_code) {
        this.main_code = main_code;
    }

    public String getCode_nm() {
        return code_nm;
    }
    public void setCode_nm(String code_nm) {
        this.code_nm = code_nm;
    }
    
     public String getCode_memo() {
        return code_memo;
    }
    public void setCode_memo(String code_memo) {
        this.code_memo = code_memo;
    }
    
      public String getDel_flag() {
        return del_flag;
    } 
    public void setDel_flag(String del_flag) {
        this.del_flag = del_flag;
    }

    public String getIns_dt() {
        return ins_dt;
    }
    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }
    
    public String getIns_user() {
        return ins_user;
    }
    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }
    
    
    public String getMod_user() {
        return mod_user;
    }
    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }
    
    public String getDetail_code() {
        return detail_code;
    }    
    public void setDetail_code(String detail_code) {
        this.detail_code = detail_code;
    }
    
    public String getSort() {
        return sort;
    }
    public void setSort(String sort) {
        this.sort = sort;
    }
    
    public String getDecode_no() {
        return decode_no;
    }
    public void setDecode_no(String decode_no) {
        this.decode_no = decode_no;
    }
}
