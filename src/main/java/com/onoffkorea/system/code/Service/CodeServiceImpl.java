/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.code.Service;

import com.onoffkorea.system.code.Service.CodeService;
import com.onoffkorea.system.code.Bean.CodeFormBean;
import com.onoffkorea.system.code.Dao.CodeDAO;
import java.util.Hashtable;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service("codeService")
public class CodeServiceImpl  implements CodeService{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private CodeDAO codeDAO;

    //코드 정보 관리
    @Override
    public Hashtable codeMaster(CodeFormBean codeFormBean) {
        
        Hashtable ht = new Hashtable();

        try {
                ht.put("ResultSet", this.codeDAO.codeMaster(codeFormBean));
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;
    }
   //코드 정보 관리
    @Override
    public Hashtable codeDetailMaster(CodeFormBean codeFormBean) {
        
        Hashtable ht = new Hashtable();

        try {
                ht.put("ResultSet", this.codeDAO.codeDetailMaster(codeFormBean));
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;
    }
    //코드 정보 삭제
    @Override
    @Transactional
    public void codeMasterDelete(CodeFormBean codeFormBean) {
        
        codeDAO.codeDetailMasterDelete(codeFormBean);
        codeDAO.codeMasterDelete(codeFormBean);
        
    }
    
    //상세코드 정보 삭제
    @Override
    @Transactional
    public void codeDetailMasterDelete(CodeFormBean codeFormBean) {
        
        codeDAO.codeDetailMasterDelete(codeFormBean);
        
    }

    //코드 정보 등록
    @Override
    public void mcodeMasterInsert(CodeFormBean codeFormBean) {
        
        codeDAO.mcodeMasterInsert(codeFormBean);
        
    }
      //코드 정보 등록
    @Override
    public String dcodeMasterInsert(CodeFormBean codeFormBean) {
        
        return codeDAO.dcodeMasterInsert(codeFormBean);
        
    }

    //코드 정보 수정
    @Override
    public void codeMasterUpdate(CodeFormBean codeFormBean) {
        
        codeDAO.codeMasterUpdate(codeFormBean);
        
    }

    //상세코드 정보 수정
    @Override
    public void codeDetailMasterUpdate(CodeFormBean codeFormBean) {
        
        codeDAO.codeDetailMasterUpdate(codeFormBean);
        
    }
}
