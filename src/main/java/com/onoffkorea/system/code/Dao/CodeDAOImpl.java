/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.code.Dao;

import com.onoffkorea.system.code.Dao.CodeDAO;
import com.onoffkorea.system.code.Bean.CodeFormBean;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("codeDAO")
public class CodeDAOImpl extends SqlSessionDaoSupport implements CodeDAO{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    //코드 정보 관리
    @Override
    public List codeMaster(CodeFormBean codeFormBean) {
        
        return (List) getSqlSession().selectList("Code.codeMaster", codeFormBean);
        
    }

    //상세코드 정보 관리
    @Override
    public List codeDetailMaster(CodeFormBean codeFormBean) {
        
        return (List) getSqlSession().selectList("Code.codeDetailMaster", codeFormBean);
        
    }
    //코드 정보 삭제
    @Override
    public void codeMasterDelete(CodeFormBean codeFormBean) {

        getSqlSession().update("Code.codeMasterDelete", codeFormBean);
        
    }
    
        //코드 정보 삭제
    @Override
    public void codeDetailMasterDelete(CodeFormBean codeFormBean) {

        getSqlSession().update("Code.codeDetailMasterDelete", codeFormBean);
        
    }

    //코드 정보 등록
    @Override
    public void mcodeMasterInsert(CodeFormBean codeFormBean) {

        getSqlSession().insert("Code.codeMasterInsert", codeFormBean);
        
    }
    
      //코드 정보 등록
    @Override
    public String dcodeMasterInsert(CodeFormBean codeFormBean) {

        Integer result = getSqlSession().insert("Code.codeDetailMasterInsert", codeFormBean);

        return result.toString();
        
    }

    //코드 정보 수정
    @Override
    public void codeMasterUpdate(CodeFormBean codeFormBean) {

        getSqlSession().update("Code.codeMasterUpdate", codeFormBean);
        
    }
    
    //코드 정보 수정
    @Override
    public void codeDetailMasterUpdate(CodeFormBean codeFormBean) {

        getSqlSession().update("Code.codeDetailMasterUpdate", codeFormBean);
        
    }
    
}
