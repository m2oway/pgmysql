/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.code.Dao;

import com.onoffkorea.system.code.Bean.CodeFormBean;

/**
 *
 * @author Administrator
 */
public interface CodeDAO {

    public Object codeMaster(CodeFormBean codeFormBean);
    
    public Object codeDetailMaster(CodeFormBean codeFormBean);

    public void codeMasterDelete(CodeFormBean codeFormBean);
    
    public void codeDetailMasterDelete(CodeFormBean codeFormBean);

    public String dcodeMasterInsert(CodeFormBean codeFormBean);
    
    public void mcodeMasterInsert(CodeFormBean codeFormBean);

    public void codeMasterUpdate(CodeFormBean codeFormBean);
    
    public void codeDetailMasterUpdate(CodeFormBean codeFormBean);
    
    
}
