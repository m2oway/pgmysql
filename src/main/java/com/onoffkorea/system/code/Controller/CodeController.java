/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.code.Controller;

import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.code.Bean.CodeFormBean;
import com.onoffkorea.system.code.Service.CodeService;
import com.onoffkorea.system.code.Vo.Tb_Main_Code;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/code/**")
public class CodeController {
        
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private CodeService codeService;
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }    
    
    //레이아웃
    @RequestMapping(method= RequestMethod.GET,value = "/codeLayout")
    public String codeLayout(@Valid CodeFormBean codeFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        return null;
        
    }
     //코드 정보 조회
     @RequestMapping(method= RequestMethod.GET,value = "/codeMaster")
    public String codeMaster(@Valid CodeFormBean codeFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
 
        String use_flag = Util.nullToString(codeFormBean.getUse_flag());
//        if("".equals(use_flag)){
//            codeFormBean.setUse_flag("Y");
//        }
        
        Hashtable ht_codeMaster = codeService.codeMaster(codeFormBean);
        List ls_codeMaster = (List) ht_codeMaster.get("ResultSet");
        StringBuilder cc = new StringBuilder();
        cc.append("{\"rows\" : [");
        for (int i = 0; i < ls_codeMaster.size(); i++) {
                Tb_Main_Code tb_Main_Code = (Tb_Main_Code) ls_codeMaster.get(i);
                cc.append("{\"id\":" + i);
                cc.append(",\"userdata\":{");
                cc.append("\"main_code\":\""+Util.nullToString(tb_Main_Code.getMain_code())+"\"");
                cc.append(" ,\"use_flag\":\""+Util.nullToString(tb_Main_Code.getUse_flag())+"\"");
                cc.append("}");
                cc.append(",\"data\":[");
                cc.append(" \"" + Util.nullToString(tb_Main_Code.getRnum()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_Main_Code.getCode_nm()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_Main_Code.getMain_code()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_Main_Code.getUse_flag()) + "\"");
//                cc.append(",\"" + tb_Main_Code.getDel_flag() + "\"");

                if (i == (ls_codeMaster.size() - 1)) {
                        cc.append("]}");
                } else {
                        cc.append("]},");
                }
        }

        cc.append("]}");    
        logger.debug("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaahaa ="+cc.toString());
        model.addAttribute("codeMaster",cc.toString());
        return null;
        


        
    }
     //상세코드 정보 조회
     @RequestMapping(method= RequestMethod.GET,value = "/codeDetailMaster")
    public String codeDetailMaster(@Valid CodeFormBean codeFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        StringBuilder dc = new StringBuilder();
        dc.append("{rows : []}");

        model.addAttribute("codeDetailMaster",dc.toString());
        
        return null;
        
    } 
    
    //코드 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/codeMaster")
    public @ResponseBody String codeMaster(@Valid CodeFormBean codeFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
        
        Hashtable ht_codeMaster = codeService.codeMaster(codeFormBean);
        List ls_codeMaster = (List) ht_codeMaster.get("ResultSet");
        
        logger.debug("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaahaa =" + ht_codeMaster.size());


        StringBuilder cc = new StringBuilder();
        cc.append("{\"rows\" : [");
        for (int i = 0; i < ls_codeMaster.size(); i++) {
                Tb_Main_Code tb_Main_Code = (Tb_Main_Code) ls_codeMaster.get(i);
                cc.append("{\"id\":" + i);
                cc.append(",\"userdata\":{");
                cc.append("\"main_code\":\""+Util.nullToString(tb_Main_Code.getMain_code())+"\"");
                cc.append(" ,\"use_flag\":\""+Util.nullToString(tb_Main_Code.getUse_flag())+"\"");
                cc.append("}");
                cc.append(",\"data\":[");
                cc.append(" \"" + Util.nullToString(tb_Main_Code.getRnum()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_Main_Code.getCode_nm()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_Main_Code.getMain_code()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_Main_Code.getUse_flag()) + "\"");
//                cc.append(",\"" + tb_Main_Code.getCode_memo() + "\"");

                if (i == (ls_codeMaster.size() - 1)) {
                        cc.append("]}");
                } else {
                        cc.append("]},");
                }
        }

        cc.append("]}");    
     System.out.println(cc);
        return cc.toString();
       
    }
    
    //상세코드 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/codeDetailMaster")
    public @ResponseBody String codeDetailMaster(@Valid CodeFormBean codeFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
       
        request.getParameter("main_code");
        
        Hashtable ht_codeDetailMaster = codeService.codeDetailMaster(codeFormBean);
        List ls_codeDetailMaster = (List) ht_codeDetailMaster.get("ResultSet");

        StringBuilder cc = new StringBuilder();
        cc.append("{\"total_count\":\"500\",\"pos\":\"0\",\"rows\":[");

        for (int i = 0; i < ls_codeDetailMaster.size(); i++) {
                Tb_Main_Code tb_Main_Code = (Tb_Main_Code) ls_codeDetailMaster.get(i);
                cc.append("{\"id\":" + tb_Main_Code.getRnum());
                cc.append(",\"userdata\":{");
                cc.append("\"main_code\":\""+Util.nullToString(tb_Main_Code.getMain_code())+"\"");
                cc.append(" ,\"detail_code\":\""+Util.nullToString(tb_Main_Code.getDetail_code())+"\"");
                cc.append("}");
                cc.append(",\"data\":[");
                cc.append(" \"" + Util.nullToString(tb_Main_Code.getRnum()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_Main_Code.getMain_code()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_Main_Code.getDetail_code()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_Main_Code.getCode_nm()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_Main_Code.getSort()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_Main_Code.getCode_memo()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_Main_Code.getUse_flag()) + "\"");

                if (i == (ls_codeDetailMaster.size() - 1)) {
                        cc.append("]}");
                } else {
                        cc.append("]},");
                }
        }

        cc.append("]}");

     System.out.println(cc);
     
        return cc.toString();
       
    }
    
     
    
    //코드 정보 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/codeMasterDelete")
    public @ResponseBody String codeMasterDelete(@Valid CodeFormBean codeFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {
             
        codeFormBean.setUser_seq(dentalHopitalSession.getUser_seq());
        codeService.codeMasterDelete(codeFormBean);
        
        return null;
        
    }
    
       //코드 정보 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/codeDetailMasterDelete")
    public @ResponseBody String codeDetailMasterDelete(@Valid CodeFormBean codeFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {
        
        codeFormBean.setUser_seq(dentalHopitalSession.getUser_seq());
        codeService.codeDetailMasterDelete(codeFormBean);
        
        return null;
        
    }
    
    
    //코드 정보 등록
    @RequestMapping(method= RequestMethod.GET,value = "/codeMasterInsert")
    public String codeMasterInsertForm(@Valid CodeFormBean codeFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        return null;
        
    }   
    
    //코드 정보 등록
    @RequestMapping(method= RequestMethod.POST,value = "/codeMasterInsert")
    public @ResponseBody String codeMasterInsert(@Valid CodeFormBean codeFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        codeFormBean.setUser_seq(dentalHopitalSession.getUser_seq());
         codeService.mcodeMasterInsert(codeFormBean);
         String main_code = codeFormBean.getMain_code();
        
        return main_code;
        
    }  
    
    //상세코드 정보 등록
    @RequestMapping(method= RequestMethod.GET,value = "/codeDetailMasterInsert")
    public String codeDetailMasterInsertForm(@Valid CodeFormBean codeFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
    
        logger.debug("codeFormBean ======?????***********************======???=" +codeFormBean.getMain_code());
        String maincode = Util.nullToString(codeFormBean.getMain_code());
//        model.addAttribute("main_code",codeFormBean.getMain_code());
        
        codeFormBean.setMain_code("");
        
        Hashtable ht_codeMaster = codeService.codeMaster(codeFormBean);
        List ls_codeMaster = (List) ht_codeMaster.get("ResultSet");
        
        model.addAttribute("ls_codeMaster",ls_codeMaster);
        model.addAttribute("main_code",maincode);
        return null;
        
    }   
    
    //상세코드 정보 등록
    @RequestMapping(method= RequestMethod.POST,value = "/codeDetailMasterInsert")
    public @ResponseBody String codeDetailMasterInsert(@Valid CodeFormBean codeFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {
         logger.debug("codeFormBean =============" +codeFormBean.getCode_nm());
        codeFormBean.setUser_seq(dentalHopitalSession.getUser_seq());
        
        String detail_code = codeService.dcodeMasterInsert(codeFormBean);
        
        return codeFormBean.getDetail_code();
         
        
    }     
    
    //코드 정보 수정 
    @RequestMapping(method= RequestMethod.GET,value = "/codeMasterUpdate")
    public String codeMasterUpdateForm(@Valid CodeFormBean codeFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        logger.debug("aaaaaaaaaaaaaaaa = " + codeFormBean.getMain_code());
        
        Hashtable ht_codeMaster = codeService.codeMaster(codeFormBean);
        List ls_codeMaster = (List) ht_codeMaster.get("ResultSet");
        Tb_Main_Code tb_Code = (Tb_Main_Code) ls_codeMaster.get(0);
        
        model.addAttribute("tb_Code",tb_Code);
        
        return null;
        
    }       
    
    //코드 정보 수정 
    @RequestMapping(method= RequestMethod.POST,value = "/codeMasterUpdate")
    public @ResponseBody String codeMasterUpdate(@Valid CodeFormBean codeFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        codeFormBean.setUser_seq(dentalHopitalSession.getUser_seq());
        codeService.codeMasterUpdate(codeFormBean);
        
        return null;
        
    } 
    
    //상세코드 정보 수정 
    @RequestMapping(method= RequestMethod.GET,value = "/codeDetailMasterUpdate")
    public String codeDetailMasterUpdateForm(@Valid CodeFormBean codeFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        
        Hashtable ht_codeDetailMaster = codeService.codeDetailMaster(codeFormBean);
        List ls_codeDetailMaster = (List) ht_codeDetailMaster.get("ResultSet");
        
        Tb_Main_Code tb_Code = (Tb_Main_Code) ls_codeDetailMaster.get(0);
        
        model.addAttribute("tb_Code",tb_Code);
        return null;
    }       
    
    //상세코드 정보 수정 
    @RequestMapping(method= RequestMethod.POST,value = "/codeDetailMasterUpdate")
    public @ResponseBody String codeDetailMasterUpdate(@Valid CodeFormBean codeFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        codeFormBean.setUser_seq(dentalHopitalSession.getUser_seq());
        codeService.codeDetailMasterUpdate(codeFormBean);
        
        return null;
        
    } 

}
