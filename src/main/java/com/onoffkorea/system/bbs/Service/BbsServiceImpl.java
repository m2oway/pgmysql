/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.bbs.Service;

import com.onoffkorea.system.bbs.Bean.BbsFormBean;
import com.onoffkorea.system.bbs.Dao.BbsDAO;
import com.onoffkorea.system.bbs.Vo.Tb_Bbs;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.util.Util;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service("bbsService")
public class BbsServiceImpl implements BbsService{
    
    protected Logger logger = Logger.getLogger(getClass().getName());
    
    @Autowired
    private CommonService commonService;        

    @Autowired
    private BbsDAO bbsDAO;    
    
    //공지사항 조회
    @Override
    public Hashtable basic_bbsMasterList(BbsFormBean bbsFormBean) {
        
        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = bbsDAO.bbsMasterListCount(bbsFormBean);
                List<Tb_Bbs> ls_bbsMasterList = bbsDAO.bbsMasterList(bbsFormBean);
                
                ht.put("total_count", String.valueOf(total_count.intValue()));
                ht.put("ls_bbsMasterList", ls_bbsMasterList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;          
        
    }    

    //공지사항 조회
    @Override
    public Hashtable bbsMasterList(BbsFormBean bbsFormBean) {
        
        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = bbsDAO.bbsMasterListCount(bbsFormBean);
                List<Tb_Bbs> ls_bbsMasterList = bbsDAO.bbsMasterList(bbsFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(bbsFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_bbsMasterList.size(); i++) {
                        Tb_Bbs tb_Bbs = ls_bbsMasterList.get(i);
                        sb.append("{\"id\":" + tb_Bbs.getBbs_seq());
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Bbs.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Bbs.getTitle()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Bbs.getIns_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Bbs.getPopup_chk_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Bbs.getPopup_start_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Bbs.getPopup_end_dt()) + "\"");

                        if (i == (ls_bbsMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                ht.put("ResultSet", sb.toString());
                ht.put("ls_bbsMasterList", ls_bbsMasterList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;          
        
    }
    
     //팝업공지사항 조회
    @Override
    //public String bbsMasterPopUpList(BbsFormBean bbsFormBean) {
      public List<Tb_Bbs>  bbsMasterPopUpList(BbsFormBean bbsFormBean) {
      /*
        Hashtable ht = new Hashtable();
        List<Tb_Bbs> ls_bbsMasterList;
        ls_bbsMasterList = null;
        StringBuilder sb = new StringBuilder();
        try {
            
                Integer total_count = bbsDAO.bbsMasterListCount(bbsFormBean);
                ls_bbsMasterList = bbsDAO.bbsMasterPopUpList(bbsFormBean);
                
                
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(bbsFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_bbsMasterList.size(); i++) {
                        Tb_Bbs tb_Bbs = ls_bbsMasterList.get(i);
                        sb.append("<tr>");
                        sb.append("<td>"+Util.nullToString(tb_Bbs.getTitle())+"</td>");
                        sb.append("<td>"+Util.nullToString(tb_Bbs.getContent())+"</td>");
                        sb.append("</tr>");
                        
                        if (i == (ls_bbsMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");   
                
            } catch (Exception e) {
                e.printStackTrace();
        }

        return sb.toString();          
        */
          List<Tb_Bbs> ls_bbsMasterList;
          ls_bbsMasterList = bbsDAO.bbsMasterPopUpList(bbsFormBean);
        return ls_bbsMasterList;
    }
    
    //공지사항 등록
    @Override
    @Transactional
    public void bbsMasterInsert(BbsFormBean bbsFormBean) {
        
        bbsDAO.bbsMasterInsert(bbsFormBean); 
        
    }    
    
    //공지사항 수정
    @Override
    @Transactional
    public void bbsMasterUpdate(BbsFormBean bbsFormBean) {
        
        bbsDAO.bbsMasterUpdate(bbsFormBean); 
        
    }        

    //공지사항 메인 조회
    @Override
    public String bbsMasterMainList(BbsFormBean bbsFormBean) {

        List<Tb_Bbs> ls_bbsMasterList;
        ls_bbsMasterList = null;
        StringBuilder sb = new StringBuilder();

        try {
            
                ls_bbsMasterList = bbsDAO.bbsMasterMainList(bbsFormBean);

                sb.append("{\"rows\" : [");
                for (int i = 0; i < ls_bbsMasterList.size(); i++) {
                        Tb_Bbs tb_Bbs = ls_bbsMasterList.get(i);
                        sb.append("<tr>");
                        sb.append("<td  height=\"20px;\" align=\"left\">"+Util.nullToString(tb_Bbs.getTitle())+"</td>");
                        sb.append("</tr>");
                        if (i == (ls_bbsMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");   

            } catch (Exception e) {
                e.printStackTrace();
        }

        return sb.toString();            
        
    }
    
    

    //공지사항 메인 조회
    @Override
    public List<Tb_Bbs> bbsMasterMainList2(BbsFormBean bbsFormBean) {

        List<Tb_Bbs> ls_bbsMasterList;
        ls_bbsMasterList = null;

        return ls_bbsMasterList = bbsDAO.bbsMasterMainList(bbsFormBean);
        
    }    
}
