/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.bbs.Dao;

import com.onoffkorea.system.bbs.Bean.BbsFormBean;
import com.onoffkorea.system.bbs.Vo.Tb_Bbs;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("bbsDAO")
public class BbsDAOImpl extends SqlSessionDaoSupport implements BbsDAO{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    //공지사항 조회 카운트
    @Override
    public Integer bbsMasterListCount(BbsFormBean bbsFormBean) {
        
        return (Integer)getSqlSession().selectOne("Bbs.bbsMasterListCount",bbsFormBean);
        
    }

    //공지사항 조회
    @Override
    public List<Tb_Bbs> bbsMasterList(BbsFormBean bbsFormBean) {
        
        return (List)getSqlSession().selectList("Bbs.bbsMasterList",bbsFormBean);
        
    }
    
    //팝업 공지사항 조회
    @Override
    public List<Tb_Bbs> bbsMasterPopUpList(BbsFormBean bbsFormBean) {
        
        return (List)getSqlSession().selectList("Bbs.bbsMasterPopUpList",bbsFormBean);
        
    }

    //공지사항 등록
    @Override
    public void bbsMasterInsert(BbsFormBean bbsFormBean) {
        
        getSqlSession().insert("Bbs.bbsMasterInsert", bbsFormBean); 
        
    }
    
    //공지사항 수정
    @Override
    public void bbsMasterUpdate(BbsFormBean bbsFormBean) {
        
        getSqlSession().update("Bbs.bbsMasterUpdate", bbsFormBean); 
        
    }    

    //공지사항 메인 조회
    @Override
    public List<Tb_Bbs> bbsMasterMainList(BbsFormBean bbsFormBean) {
        
        return (List)getSqlSession().selectList("Bbs.bbsMasterMainList",bbsFormBean);
        
    }
    
}
