/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.bbs.Dao;

import com.onoffkorea.system.bbs.Bean.BbsFormBean;
import com.onoffkorea.system.bbs.Vo.Tb_Bbs;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface BbsDAO {

    public Integer bbsMasterListCount(BbsFormBean bbsFormBean);

    public List<Tb_Bbs> bbsMasterList(BbsFormBean bbsFormBean);
    
     public List<Tb_Bbs> bbsMasterPopUpList(BbsFormBean bbsFormBean);

    public void bbsMasterInsert(BbsFormBean bbsFormBean);

    public void bbsMasterUpdate(BbsFormBean bbsFormBean);

    public List<Tb_Bbs> bbsMasterMainList(BbsFormBean bbsFormBean);
    
}
