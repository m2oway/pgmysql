/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.bbs.Vo;

/**
 *
 * @author Administrator
 */
public class Tb_Bbs {
    
    private String bbs_seq       ;
    private String title         ;
    private String content       ;
    private String popup_chk     ;
    private String popup_start_dt;
    private String popup_end_dt  ;
    private String ins_dt        ;
    private String mod_dt        ;
    private String ins_user      ;
    private String mod_user      ;
    private String rnum          ;
    private String popup_chk_nm  ;

    public String getPopup_chk_nm() {
        return popup_chk_nm;
    }

    public void setPopup_chk_nm(String popup_chk_nm) {
        this.popup_chk_nm = popup_chk_nm;
    }

    public String getBbs_seq() {
        return bbs_seq;
    }

    public void setBbs_seq(String bbs_seq) {
        this.bbs_seq = bbs_seq;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPopup_chk() {
        return popup_chk;
    }

    public void setPopup_chk(String popup_chk) {
        this.popup_chk = popup_chk;
    }

    public String getPopup_start_dt() {
        return popup_start_dt;
    }

    public void setPopup_start_dt(String popup_start_dt) {
        this.popup_start_dt = popup_start_dt;
    }

    public String getPopup_end_dt() {
        return popup_end_dt;
    }

    public void setPopup_end_dt(String popup_end_dt) {
        this.popup_end_dt = popup_end_dt;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    public String getRnum() {
        return rnum;
    }

    public void setRnum(String rnum) {
        this.rnum = rnum;
    }
    
}
