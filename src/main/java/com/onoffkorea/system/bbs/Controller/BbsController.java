/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.bbs.Controller;

import com.onoffkorea.system.bbs.Bean.BbsFormBean;
import com.onoffkorea.system.bbs.Service.BbsService;
import com.onoffkorea.system.bbs.Vo.Tb_Bbs;
import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/bbs/**")
public class BbsController {
    
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private CommonService commonService;
    
    @Autowired
    private BbsService bbsService;    
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }      
    
       //공지사항 조회
    @RequestMapping(method= RequestMethod.GET,value = "/basic_bbsMasterList")
    public String basic_bbsMasterFormList(@Valid BbsFormBean bbsFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        bbsFormBean.setSes_user_cate(siteSession.getSes_user_cate());
   
        
        Hashtable ht_bbsMasterList = bbsService.basic_bbsMasterList(bbsFormBean);
        List<Tb_Bbs> ls_bbsMasterList = (List<Tb_Bbs>) ht_bbsMasterList.get("ls_bbsMasterList");
        
        String strTpTotal_count = (String)ht_bbsMasterList.get("total_count");
        
        model.addAttribute("ls_bbsMasterList",ls_bbsMasterList);
        
        model.addAttribute("total_count",strTpTotal_count);
        
        return null;
    }    
    
    //공지사항 조회
    @RequestMapping(method= RequestMethod.POST,value = "/basic_bbsMasterList")
    //@ResponseBody
    public String basic_bbsMasterList(@Valid BbsFormBean bbsFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        bbsFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        Hashtable ht_bbsMasterList = bbsService.basic_bbsMasterList(bbsFormBean);
        List<Tb_Bbs> ls_bbsMasterList = (List<Tb_Bbs>) ht_bbsMasterList.get("ls_bbsMasterList");
        
        String strTpTotal_count = (String)ht_bbsMasterList.get("total_count");
        
        model.addAttribute("ls_bbsMasterList",ls_bbsMasterList);
        
        model.addAttribute("total_count",strTpTotal_count);
        
        return null;
        
    }     
    
    //공지사항 조회
    @RequestMapping(method= RequestMethod.GET,value = "/bbsMasterList")
    public String bbsMasterFormList(@Valid BbsFormBean bbsFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        bbsFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        Hashtable ht_bbsMasterList = bbsService.bbsMasterList(bbsFormBean);
        String ls_bbsMasterList = (String) ht_bbsMasterList.get("ResultSet");
        
        model.addAttribute("ls_bbsMasterList",ls_bbsMasterList);
        
        return null;
        
    }    
    
    //공지사항 조회
    @RequestMapping(method= RequestMethod.POST,value = "/bbsMasterList")
    @ResponseBody
    public String bbsMasterList(@Valid BbsFormBean bbsFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        bbsFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        Hashtable ht_bbsMasterList = bbsService.bbsMasterList(bbsFormBean);
        String ls_bbsMasterList = (String) ht_bbsMasterList.get("ResultSet");

        return ls_bbsMasterList;
        
    }        
    
    //공지사항 팝업 조회
    @RequestMapping(method= RequestMethod.POST,value = "/bbsMasterPopUpList")
    @ResponseBody
    //public String bbsMasterPopUpList(@Valid BbsFormBean bbsFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
    public List bbsMasterPopUpList(@Valid BbsFormBean bbsFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        logger.debug("####### bbsMasterPopUpList : 공지사항 팝업 조회");

        return bbsService.bbsMasterPopUpList(bbsFormBean);
        
    }    
    
    //공지사항 매인 조회
    @RequestMapping(method= RequestMethod.POST,value = "/bbsMasterMainList")
    @ResponseBody
    public String bbsMasterMainList(@Valid BbsFormBean bbsFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        return bbsService.bbsMasterMainList(bbsFormBean);
        
    }              
    
    //공지사항 매인 조회
    @RequestMapping(method= RequestMethod.POST,value = "/bbsMasterMainList2")
    @ResponseBody
    public List bbsMasterMainList2(@Valid BbsFormBean bbsFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        return bbsService.bbsMasterMainList2(bbsFormBean);
        
    }           
    
    //공지사항 등록
    @RequestMapping(method= RequestMethod.GET,value = "/bbsMasterInsert")
    public String bbsMasterFormInsert(@Valid BbsFormBean bbsFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("POPUP_CHK");
        Tb_Code_Main popupChkList = commonService.codeSearch(commonCodeSearchFormBean);
        
        model.addAttribute("popupChkList",popupChkList);
        
        return null;
        
    } 
    
    //공지사항 등록
    @RequestMapping(method= RequestMethod.POST,value = "/bbsMasterInsert")
    public @ResponseBody String bbsMasterInsert(@Valid BbsFormBean bbsFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        bbsFormBean.setIns_user(siteSession.getUser_seq());
        bbsService.bbsMasterInsert(bbsFormBean);
        
        return null;
         
    }             
    
    //공지사항 수정
    @RequestMapping(method= RequestMethod.GET,value = "/bbsMasterUpdate")
    public String bbsMasterFormUpdate(@Valid BbsFormBean bbsFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        bbsFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        Hashtable ht_bbsMasterList = bbsService.bbsMasterList(bbsFormBean);
        List<Tb_Bbs> ls_bbsMasterList = (List<Tb_Bbs>) ht_bbsMasterList.get("ls_bbsMasterList");
        
        model.addAttribute("ls_bbsMasterList",ls_bbsMasterList);
        
        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("POPUP_CHK");
        Tb_Code_Main popupChkList = commonService.codeSearch(commonCodeSearchFormBean);
        
        model.addAttribute("popupChkList",popupChkList);
        
        return null;
        
    }     
    
    //공지사항 수정
    @RequestMapping(method= RequestMethod.POST,value = "/bbsMasterUpdate")
    public @ResponseBody String bbsMasterUpdate(@Valid BbsFormBean bbsFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        bbsFormBean.setMod_user(siteSession.getUser_seq());
        bbsService.bbsMasterUpdate(bbsFormBean);
        
        return null;
         
    }           
    
}
