/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.merchant.Bean;

import com.onoffkorea.system.common.util.CommonSortableListPagingForm;

/**
 *
 * @author Administrator
 */
public class MerchantFormBean extends CommonSortableListPagingForm{
    
        private String onffmerch_no;
        private String comp_seq;
        private String comp_nm	;
        private String biz_no;
        private String corp_no;
        private String comp_cate;
        private String comp_cate_nm;
        private String comp_ceo_nm	;
        private String comp_tel1;
        private String comp_tel2;
        private String comp_zip_cd;
        private String comp_addr_1;
        private String comp_addr_2;
        private String merch_nm;
        private String tel_1;
        private String tel_2;
        private String email;
        private String zip_cd;
        private String addr_1;
        private String addr_2;
        private String svc_stat;
        private String svc_stat_nm;
        private String merch_cate;
        private String merch_cate_nm;
        private String biz_type;
        private String biz_type_nm;
        private String biz_cate;
        private String biz_cate_nm;
        private String tax_flag;
        private String tax_flag_nm;
        private String close_dt;
        private String close_user;
        private String close_reson;
        private String commision;
        private String bal_period;
        private String bal_period_nm;
        private String pay_dt_cd_1;
        private String pay_dt_cd_1_nm;
        private String pay_dt_cd_2;
        private String pay_dt_cd_2_nm;
        private String bank_cd;
        private String bank_cd_nm;
        private String acc_no;
        private String acc_nm;
        private String memo;
        private String agent_commission;
        private String org_seq;
        private String agent_seq;
        private String agent_nm;
        private String del_flag;
        private String ins_dt;
        private String mod_dt;
        private String ins_user	;
        private String mod_user;
        private String app_chk_amt;
        
        private String pay_dt;
        
        private String pay_chn_info;
        private String security_info;
        
        private String cncl_auth;
        private String cncl_auth_nm;
        
        private String agentsearchflag;
        
    private String appreq_chk_amt;

        public String getAppreq_chk_amt() {
            return appreq_chk_amt;
        }

        public void setAppreq_chk_amt(String appreq_chk_amt) {
            this.appreq_chk_amt = appreq_chk_amt;
        }        

    public String getAgentsearchflag() {
        return agentsearchflag;
    }

    public void setAgentsearchflag(String agentsearchflag) {
        this.agentsearchflag = agentsearchflag;
    }
        
        

    public String getCncl_auth() {
        return cncl_auth;
    }

    public void setCncl_auth(String cncl_auth) {
        this.cncl_auth = cncl_auth;
    }

    public String getCncl_auth_nm() {
        return cncl_auth_nm;
    }

    public void setCncl_auth_nm(String cncl_auth_nm) {
        this.cncl_auth_nm = cncl_auth_nm;
    }
        
        

    public String getPay_dt() {
        return pay_dt;
    }

    public void setPay_dt(String pay_dt) {
        this.pay_dt = pay_dt;
    }
        

    public String getPay_chn_info() {
        return pay_chn_info;
    }

    public void setPay_chn_info(String pay_chn_info) {
        this.pay_chn_info = pay_chn_info;
    }

    public String getSecurity_info() {
        return security_info;
    }

    public void setSecurity_info(String security_info) {
        this.security_info = security_info;
    }

    public String getApp_chk_amt() {
        return app_chk_amt;
    }

    public void setApp_chk_amt(String app_chk_amt) {
        this.app_chk_amt = app_chk_amt;
    }

    public String getOnffmerch_no() {
        return onffmerch_no;
    }

    public void setOnffmerch_no(String onffmerch_no) {
        this.onffmerch_no = onffmerch_no;
    }
        
    public String getComp_seq() {
        return comp_seq;
    }

    public void setComp_seq(String comp_seq) {
        this.comp_seq = comp_seq;
    }

    public String getComp_nm() {
        return comp_nm;
    }

    public void setComp_nm(String comp_nm) {
        this.comp_nm = comp_nm;
    }

    public String getBiz_no() {
        return biz_no;
    }

    public void setBiz_no(String biz_no) {
        this.biz_no = biz_no;
    }

    public String getCorp_no() {
        return corp_no;
    }

    public void setCorp_no(String corp_no) {
        this.corp_no = corp_no;
    }

    public String getComp_cate() {
        return comp_cate;
    }

    public void setComp_cate(String comp_cate) {
        this.comp_cate = comp_cate;
    }

    public String getComp_cate_nm() {
        return comp_cate_nm;
    }

    public void setComp_cate_nm(String comp_cate_nm) {
        this.comp_cate_nm = comp_cate_nm;
    }

    public String getComp_ceo_nm() {
        return comp_ceo_nm;
    }

    public void setComp_ceo_nm(String comp_ceo_nm) {
        this.comp_ceo_nm = comp_ceo_nm;
    }

    public String getComp_tel1() {
        return comp_tel1;
    }

    public void setComp_tel1(String comp_tel1) {
        this.comp_tel1 = comp_tel1;
    }

    public String getComp_tel2() {
        return comp_tel2;
    }

    public void setComp_tel2(String comp_tel2) {
        this.comp_tel2 = comp_tel2;
    }

    public String getComp_zip_cd() {
        return comp_zip_cd;
    }

    public void setComp_zip_cd(String comp_zip_cd) {
        this.comp_zip_cd = comp_zip_cd;
    }

    public String getComp_addr_1() {
        return comp_addr_1;
    }

    public void setComp_addr_1(String comp_addr_1) {
        this.comp_addr_1 = comp_addr_1;
    }

    public String getComp_addr_2() {
        return comp_addr_2;
    }

    public void setComp_addr_2(String comp_addr_2) {
        this.comp_addr_2 = comp_addr_2;
    }

    public String getMerch_nm() {
        return merch_nm;
    }

    public void setMerch_nm(String merch_nm) {
        this.merch_nm = merch_nm;
    }

    public String getTel_1() {
        return tel_1;
    }

    public void setTel_1(String tel_1) {
        this.tel_1 = tel_1;
    }

    public String getTel_2() {
        return tel_2;
    }

    public void setTel_2(String tel_2) {
        this.tel_2 = tel_2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getZip_cd() {
        return zip_cd;
    }

    public void setZip_cd(String zip_cd) {
        this.zip_cd = zip_cd;
    }

    public String getAddr_1() {
        return addr_1;
    }

    public void setAddr_1(String addr_1) {
        this.addr_1 = addr_1;
    }

    public String getAddr_2() {
        return addr_2;
    }

    public void setAddr_2(String addr_2) {
        this.addr_2 = addr_2;
    }

    public String getSvc_stat() {
        return svc_stat;
    }

    public void setSvc_stat(String svc_stat) {
        this.svc_stat = svc_stat;
    }

    public String getSvc_stat_nm() {
        return svc_stat_nm;
    }

    public void setSvc_stat_nm(String svc_stat_nm) {
        this.svc_stat_nm = svc_stat_nm;
    }

    public String getMerch_cate() {
        return merch_cate;
    }

    public void setMerch_cate(String merch_cate) {
        this.merch_cate = merch_cate;
    }

    public String getMerch_cate_nm() {
        return merch_cate_nm;
    }

    public void setMerch_cate_nm(String merch_cate_nm) {
        this.merch_cate_nm = merch_cate_nm;
    }

    public String getBiz_type() {
        return biz_type;
    }

    public void setBiz_type(String biz_type) {
        this.biz_type = biz_type;
    }

    public String getBiz_type_nm() {
        return biz_type_nm;
    }

    public void setBiz_type_nm(String biz_type_nm) {
        this.biz_type_nm = biz_type_nm;
    }

    public String getBiz_cate() {
        return biz_cate;
    }

    public void setBiz_cate(String biz_cate) {
        this.biz_cate = biz_cate;
    }

    public String getBiz_cate_nm() {
        return biz_cate_nm;
    }

    public void setBiz_cate_nm(String biz_cate_nm) {
        this.biz_cate_nm = biz_cate_nm;
    }

    public String getTax_flag() {
        return tax_flag;
    }

    public void setTax_flag(String tax_flag) {
        this.tax_flag = tax_flag;
    }

    public String getTax_flag_nm() {
        return tax_flag_nm;
    }

    public void setTax_flag_nm(String tax_flag_nm) {
        this.tax_flag_nm = tax_flag_nm;
    }

    public String getClose_dt() {
        return close_dt;
    }

    public void setClose_dt(String close_dt) {
        this.close_dt = close_dt;
    }

    public String getClose_user() {
        return close_user;
    }

    public void setClose_user(String close_user) {
        this.close_user = close_user;
    }

    public String getClose_reson() {
        return close_reson;
    }

    public void setClose_reson(String close_reson) {
        this.close_reson = close_reson;
    }

    public String getCommision() {
        return commision;
    }

    public void setCommision(String commision) {
        this.commision = commision;
    }

    public String getBal_period() {
        return bal_period;
    }

    public void setBal_period(String bal_period) {
        this.bal_period = bal_period;
    }

    public String getBal_period_nm() {
        return bal_period_nm;
    }

    public void setBal_period_nm(String bal_period_nm) {
        this.bal_period_nm = bal_period_nm;
    }

    public String getPay_dt_cd_1() {
        return pay_dt_cd_1;
    }

    public void setPay_dt_cd_1(String pay_dt_cd_1) {
        this.pay_dt_cd_1 = pay_dt_cd_1;
    }

    public String getPay_dt_cd_1_nm() {
        return pay_dt_cd_1_nm;
    }

    public void setPay_dt_cd_1_nm(String pay_dt_cd_1_nm) {
        this.pay_dt_cd_1_nm = pay_dt_cd_1_nm;
    }

    public String getPay_dt_cd_2() {
        return pay_dt_cd_2;
    }

    public void setPay_dt_cd_2(String pay_dt_cd_2) {
        this.pay_dt_cd_2 = pay_dt_cd_2;
    }

    public String getPay_dt_cd_2_nm() {
        return pay_dt_cd_2_nm;
    }

    public void setPay_dt_cd_2_nm(String pay_dt_cd_2_nm) {
        this.pay_dt_cd_2_nm = pay_dt_cd_2_nm;
    }

    public String getBank_cd() {
        return bank_cd;
    }

    public void setBank_cd(String bank_cd) {
        this.bank_cd = bank_cd;
    }

    public String getBank_cd_nm() {
        return bank_cd_nm;
    }

    public void setBank_cd_nm(String bank_cd_nm) {
        this.bank_cd_nm = bank_cd_nm;
    }

    public String getAcc_no() {
        return acc_no;
    }

    public void setAcc_no(String acc_no) {
        this.acc_no = acc_no;
    }

    public String getAcc_nm() {
        return acc_nm;
    }

    public void setAcc_nm(String acc_nm) {
        this.acc_nm = acc_nm;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getAgent_commission() {
        return agent_commission;
    }

    public void setAgent_commission(String agent_commission) {
        this.agent_commission = agent_commission;
    }

    public String getOrg_seq() {
        return org_seq;
    }

    public void setOrg_seq(String org_seq) {
        this.org_seq = org_seq;
    }

    public String getAgent_seq() {
        return agent_seq;
    }

    public void setAgent_seq(String agent_seq) {
        this.agent_seq = agent_seq;
    }

    public String getAgent_nm() {
        return agent_nm;
    }

    public void setAgent_nm(String agent_nm) {
        this.agent_nm = agent_nm;
    }

    public String getDel_flag() {
        return del_flag;
    }

    public void setDel_flag(String del_flag) {
        this.del_flag = del_flag;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    @Override
    public String toString() {
        return "MerchantFormBean{" + "onffmerch_no=" + onffmerch_no + ", comp_seq=" + comp_seq + ", comp_nm=" + comp_nm + ", biz_no=" + biz_no + ", corp_no=" + corp_no + ", comp_cate=" + comp_cate + ", comp_cate_nm=" + comp_cate_nm + ", comp_ceo_nm=" + comp_ceo_nm + ", comp_tel1=" + comp_tel1 + ", comp_tel2=" + comp_tel2 + ", comp_zip_cd=" + comp_zip_cd + ", comp_addr_1=" + comp_addr_1 + ", comp_addr_2=" + comp_addr_2 + ", merch_nm=" + merch_nm + ", tel_1=" + tel_1 + ", tel_2=" + tel_2 + ", email=" + email + ", zip_cd=" + zip_cd + ", addr_1=" + addr_1 + ", addr_2=" + addr_2 + ", svc_stat=" + svc_stat + ", svc_stat_nm=" + svc_stat_nm + ", merch_cate=" + merch_cate + ", merch_cate_nm=" + merch_cate_nm + ", biz_type=" + biz_type + ", biz_type_nm=" + biz_type_nm + ", biz_cate=" + biz_cate + ", biz_cate_nm=" + biz_cate_nm + ", tax_flag=" + tax_flag + ", tax_flag_nm=" + tax_flag_nm + ", close_dt=" + close_dt + ", close_user=" + close_user + ", close_reson=" + close_reson + ", commision=" + commision + ", bal_period=" + bal_period + ", bal_period_nm=" + bal_period_nm + ", pay_dt_cd_1=" + pay_dt_cd_1 + ", pay_dt_cd_1_nm=" + pay_dt_cd_1_nm + ", pay_dt_cd_2=" + pay_dt_cd_2 + ", pay_dt_cd_2_nm=" + pay_dt_cd_2_nm + ", bank_cd=" + bank_cd + ", bank_cd_nm=" + bank_cd_nm + ", acc_no=" + acc_no + ", acc_nm=" + acc_nm + ", memo=" + memo + ", agent_commission=" + agent_commission + ", org_seq=" + org_seq + ", agent_seq=" + agent_seq + ", agent_nm=" + agent_nm + ", del_flag=" + del_flag + ", ins_dt=" + ins_dt + ", mod_dt=" + mod_dt + ", ins_user=" + ins_user + ", mod_user=" + mod_user + ", app_chk_amt=" + app_chk_amt + ", pay_chn_info=" + pay_chn_info + ", security_info=" + security_info + '}';
    }
    
}
