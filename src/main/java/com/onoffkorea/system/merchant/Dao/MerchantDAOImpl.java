/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.merchant.Dao;

import com.onoffkorea.system.merchant.Bean.MerchantFormBean;
import com.onoffkorea.system.merchant.Bean.SecurityFormBean;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("merchantDAO")
public class MerchantDAOImpl extends SqlSessionDaoSupport  implements MerchantDAO {
    
    protected Logger logger = Logger.getLogger(getClass().getName());
    
    @Override
    public String GetmerchantMasterKey() {
        return (String) getSqlSession().selectOne("Merchant.GetmerchantMasterKey");
    }

    @Override
    public String merchantRecordCount(MerchantFormBean merchantFormBean) {
        return (String) getSqlSession().selectOne("Merchant.merchantRecordCount", merchantFormBean);
    }

    @Override
    public List merchantMasterList(MerchantFormBean merchantFormBean) {
        return (List) getSqlSession().selectList("Merchant.merchantMasterList", merchantFormBean);
    }

   @Override
    public Integer merchantMasterInsert(MerchantFormBean merchantFormBean) {
        return (Integer)getSqlSession().insert("Merchant.merchantMasterInsert", merchantFormBean);     
    }
    
    
    @Override
    public Integer merchantDtlInsert(SecurityFormBean securityFormBean) {
        return (Integer)getSqlSession().insert("Merchant.SecurityInsert", securityFormBean);
        
    }
    
    @Override
    public List merchantMasterInfo(MerchantFormBean merchantFormBean) {
         return (List) getSqlSession().selectList("Merchant.merchantMasterList", merchantFormBean);
    }    
    
    @Override
    public List merchantDtlList(SecurityFormBean securityFormBean) {
        return (List) getSqlSession().selectList("Merchant.SecurityList", securityFormBean);
    }
    
    @Override
    public Integer merchantMasterDelete(MerchantFormBean merchantFormBean) {
         //getSqlSession().delete("Merchant.merchantMasterDelete", merchantFormBean);        
        return (Integer)getSqlSession().update("Merchant.merchantMasterDelete", merchantFormBean);
    }

    @Override
    public Integer merchantMasterUpdate(MerchantFormBean merchantFormBean) {
        return (Integer)getSqlSession().update("Merchant.merchantMasterUpdate", merchantFormBean);
    }

    @Override
    public Integer merchantDtlDelete(SecurityFormBean securityFormBean) {
        
           int Resultx  = getSqlSession().update("Merchant.SecurityDelete", securityFormBean);

           return Resultx;
    }

    @Override
    public Integer merchantDtlUpdate(SecurityFormBean securityFormBean) {
        return (Integer)getSqlSession().update("Merchant.SecurityUpdate", securityFormBean);
    }

    @Override
    public Integer securityMasterListCount(SecurityFormBean securityFormBean) {
        return (Integer) getSqlSession().selectOne("Merchant.SecurityListCount", securityFormBean);
    }

    @Override
    public List securityMasterList(SecurityFormBean securityFormBean) {
        return (List) getSqlSession().selectList("Merchant.SecurityList", securityFormBean);
    }
    
    @Override
    public Integer saleLimitMasterListCount(MerchantFormBean merchantFormBean) {
        return (Integer) getSqlSession().selectOne("Merchant.saleLimitMasterListCount", merchantFormBean);
    }

    @Override
    public List saleLimitMasterList(MerchantFormBean merchantFormBean) {
        return (List) getSqlSession().selectList("Merchant.saleLimitMasterList", merchantFormBean);
    }    

    @Override
    public List securityMasterListExcel(MerchantFormBean merchantFormBean) {
        return (List) getSqlSession().selectList("Merchant.securityMasterListExcel", merchantFormBean);
    }

}
