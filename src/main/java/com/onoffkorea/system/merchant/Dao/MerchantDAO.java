/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.merchant.Dao;

import com.onoffkorea.system.merchant.Bean.MerchantFormBean;
import com.onoffkorea.system.merchant.Bean.SecurityFormBean;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface MerchantDAO {
    
    public String GetmerchantMasterKey();
    
    public String merchantRecordCount(MerchantFormBean merchantFormBean);

    public List merchantMasterList(MerchantFormBean merchantFormBean);
    
    public Integer merchantMasterInsert(MerchantFormBean merchantFormBean);
    
    public Integer merchantDtlInsert(SecurityFormBean securityFormBean);
    
    public List merchantMasterInfo(MerchantFormBean merchantFormBean);   
    
    public List merchantDtlList(SecurityFormBean securityFormBean);    
    
    public Integer merchantMasterDelete(MerchantFormBean merchantFormBean);

    public Integer merchantMasterUpdate(MerchantFormBean merchantFormBean);
    
    public Integer merchantDtlDelete(SecurityFormBean securityFormBean);
    
   public Integer merchantDtlUpdate(SecurityFormBean securityFormBean);

    public Integer securityMasterListCount(SecurityFormBean securityFormBean);

    public List securityMasterList(SecurityFormBean securityFormBean);

    public Integer saleLimitMasterListCount(MerchantFormBean merchantFormBean);

    public List saleLimitMasterList(MerchantFormBean merchantFormBean);

    public List securityMasterListExcel(MerchantFormBean merchantFormBean);
  
}
