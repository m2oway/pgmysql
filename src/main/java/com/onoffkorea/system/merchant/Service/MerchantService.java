/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.merchant.Service;

import com.onoffkorea.system.merchant.Bean.MerchantFormBean;
import com.onoffkorea.system.merchant.Bean.SecurityFormBean;
import java.util.Hashtable;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public interface MerchantService {
    
    public Hashtable merchantMasterList(MerchantFormBean merchantFormBean) throws Exception;

    public String merchantMasterDelete(MerchantFormBean merchantFormBean) throws Exception;
    
    public String merchantMasterInsert(MerchantFormBean merchantFormBean) throws Exception;
    
    public Hashtable merchantMasterInfo(MerchantFormBean merchantFormBean) throws Exception;
    
    public Hashtable merchantDtlList(SecurityFormBean securityFormBean) throws Exception;
   
    public String merchantMasterUpdate(MerchantFormBean merchantFormBean) throws Exception;
    
    public String merchantDtlInsert(SecurityFormBean securityFormBean) throws Exception;
        
    public String merchantDtlUpdate(SecurityFormBean securityFormBean) throws Exception;
    
    public String merchantDtlDelete(SecurityFormBean securityFormBean) throws Exception;

    public Hashtable securityMasterList(SecurityFormBean securityFormBean) throws Exception;

    public Hashtable saleLimitMasterList(MerchantFormBean merchantFormBean) throws Exception;

    public void securityMasterListExcel(MerchantFormBean merchantFormBean, String excelDownloadFilename, HttpServletResponse response);
   
}
