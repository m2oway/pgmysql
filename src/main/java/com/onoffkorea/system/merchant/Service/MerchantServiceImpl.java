/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.merchant.Service;

import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.merchant.Bean.MerchantFormBean;
import com.onoffkorea.system.merchant.Bean.SecurityFormBean;
import com.onoffkorea.system.merchant.Dao.MerchantDAO;
import com.onoffkorea.system.merchant.Vo.Tb_Onffmerch_Mst;
import com.onoffkorea.system.merchant.Vo.Tb_Security;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service("merchantService")
public class MerchantServiceImpl implements MerchantService
{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private MerchantDAO merchantDAO;
    
    @Autowired
    private CommonService commonService;    
    
    
    @Override
    public Hashtable merchantMasterList(MerchantFormBean merchantFormBean) throws Exception {
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
       
        String total_count = this.merchantDAO.merchantRecordCount(merchantFormBean);
       
        List ls_merchantMasterList = this.merchantDAO.merchantMasterList(merchantFormBean);
        
        logger.trace("-------pagesize-----------------");
        logger.trace("merchantFormBean getPage_size : " + merchantFormBean.getPage_size() );
        logger.trace("merchantFormBean getPage_no : " + merchantFormBean.getPage_no() );
        logger.trace("merchantFormBean getStart_no : " + merchantFormBean.getStart_no() );
        logger.trace("merchantFormBean getEnd_no: " + merchantFormBean.getEnd_no() );
        
        logger.trace("-------pagesize-----------------");
        
        sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(merchantFormBean.getStart_no())-1)+"\",\"rows\" : [");
        
        for (int i = 0; i < ls_merchantMasterList.size(); i++) {
                Tb_Onffmerch_Mst tb_Merchant = (Tb_Onffmerch_Mst) ls_merchantMasterList.get(i);
                
                //가맹점ID, 가맹점명, 사업자명,개인법인구분,사업자번호, 법인번호, 대표자, 연락처1, 결제한도, 대리점, 상태
                sb.append("{\"id\":" + i);
                sb.append(",\"userdata\":{");
                sb.append("\"onffmerch_no\":\""+tb_Merchant.getOnffmerch_no()+"\"");
                sb.append("}");                
                sb.append(",\"data\":[");
                sb.append(" \"" + tb_Merchant.getRnum() + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getOnffmerch_no()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getMerch_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getComp_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getComp_cate_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getBiz_no()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getCorp_no()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getComp_ceo_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getTel_1()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getApp_chk_amt()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getAgent_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getSvc_stat_nm()) + "\"");
                
                if (i == (ls_merchantMasterList.size() - 1)) {
                        sb.append("]}");
                } else {
                        sb.append("]},");
                }
        }

        sb.append("]}");    
        
        ht.put("ResultSet", sb.toString());
        
        return ht;
    }

    /*
    @Override
    @Transactional
    public String merchantMasterDelete(MerchantFormBean merchantFormBean) throws Exception {
        Integer result = null; 
        result = this.merchantDAO.merchantMasterDelete(merchantFormBean);
        
        return result.toString();
    }
    */
    @Override
    @Transactional
    public String merchantMasterInsert(MerchantFormBean merchantFormBean) throws Exception {
        Integer result = null;
        
        String strOnffmerch_no = this.merchantDAO.GetmerchantMasterKey();
        
        merchantFormBean.setOnffmerch_no(strOnffmerch_no);

        //메인정보 입력
        logger.debug("---------------merchantFormBean.getAppreq_chk_amt() : " + merchantFormBean.getAppreq_chk_amt());
        
        result = this.merchantDAO.merchantMasterInsert(merchantFormBean);
        
        String strmerchantDtlInfo = merchantFormBean.getSecurity_info();
        
        logger.trace("strmerchantDtlInfo :" + strmerchantDtlInfo);
        
        if(strmerchantDtlInfo != null && !strmerchantDtlInfo.equals(""))
        {
            String[] rows = strmerchantDtlInfo.split("]]]");
            
            for(int x=0 ; x < rows.length ; x++)
            {
                String[] cols = rows[x].split("```");
                
                //담보종류코드,담보액,적용시작일,적용종료일,MEMO
                //   0           1      2        3        4
                SecurityFormBean insdtlobj = new SecurityFormBean();
                
                insdtlobj.setOnffmerch_no(strOnffmerch_no);
                insdtlobj.setSecurity_cate(cols[0]);
                insdtlobj.setSecurity_amt(cols[1]);
                insdtlobj.setStart_dt(cols[2]);
                insdtlobj.setEnd_dt(cols[3]);
                insdtlobj.setMemo(cols[4]);
                insdtlobj.setIns_user(merchantFormBean.getIns_user());
                insdtlobj.setMod_user(merchantFormBean.getIns_user());
                
                this.merchantDAO.merchantDtlInsert(insdtlobj);
            }
        }
        
        return result.toString();
    }
    
    @Override
    public Hashtable merchantMasterInfo(MerchantFormBean merchantFormBean) throws Exception {
        
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
        
        List ls_companyMasterList = this.merchantDAO.merchantMasterInfo(merchantFormBean);
                
        ht.put("ResultSet", ls_companyMasterList);
        
        return ht;
    }

    @Override
    public Hashtable merchantDtlList(SecurityFormBean securityFormBean) throws Exception {
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
       
        List ls_merchantDtlMasterList = this.merchantDAO.merchantDtlList(securityFormBean);
        
        sb.append("{\"rows\" : [");
        
        for (int i = 0; i < ls_merchantDtlMasterList.size(); i++) {
                Tb_Security tb_MerchantDtl = (Tb_Security) ls_merchantDtlMasterList.get(i);
                ////담보종류코드,담보종류,담보액,적용시작일,적용종료일,MEMO
                sb.append("{\"id\":" + tb_MerchantDtl.getSecurity_seq());
                sb.append(",\"data\":[");
                //sb.append(" \"" + tb_MerchantDtl.getRnum() + "\"");
                //sb.append("\"" + "" + "\"");         
                sb.append("\"" + Util.nullToString(tb_MerchantDtl.getSecurity_cate()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_MerchantDtl.getSecurity_cate_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_MerchantDtl.getSecurity_amt()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_MerchantDtl.getStart_dt()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_MerchantDtl.getEnd_dt()) + "\"");
                //sb.append(",\"" + Util.nullToString(tb_MerchantDtl.getMemo()) + "\"");
                
                String strTpContents = Util.nullToString(tb_MerchantDtl.getMemo());
                strTpContents =  strTpContents.replaceAll("\\n", "<br>");
                strTpContents =  strTpContents.replaceAll("\\r", "");
                sb.append(",\"" + strTpContents + "\"");
                
                if (i == (ls_merchantDtlMasterList.size() - 1)) {
                        sb.append("]}");
                } else {
                        sb.append("]},");
                }
        }

        sb.append("]}");    
        
        ht.put("ResultSet", sb.toString());
        
        return ht;
    }
    

    @Override
    @Transactional
    public String merchantMasterUpdate(MerchantFormBean merchantFormBean) throws Exception {
        Integer result = null;    
        result = this.merchantDAO.merchantMasterUpdate(merchantFormBean);
        return result.toString();
    }
    

    @Override
    @Transactional
    public String merchantMasterDelete(MerchantFormBean merchantFormBean) throws Exception {
        Integer result = null;    
        result = this.merchantDAO.merchantMasterDelete(merchantFormBean);
        return result.toString();
    }    

    @Override
    @Transactional
    public String merchantDtlInsert(SecurityFormBean securityFormBean) throws Exception {
        //메인정보 입력
        Integer result = null;
        
        result = this.merchantDAO.merchantDtlInsert(securityFormBean);
        
        return result.toString();
    }

    @Override
    @Transactional
    public String merchantDtlUpdate(SecurityFormBean securityFormBean) throws Exception {
        Integer result = null;
        result = this.merchantDAO.merchantDtlUpdate(securityFormBean);
        return result.toString();
    }

    @Override
    @Transactional
    public String merchantDtlDelete(SecurityFormBean securityFormBean) throws Exception {
        Integer result = null;
        result = this.merchantDAO.merchantDtlDelete(securityFormBean);
        return result.toString();
    }

    @Override
    public Hashtable securityMasterList(SecurityFormBean securityFormBean) throws Exception {
        
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
       
        try {
            
            Integer total_count = this.merchantDAO.securityMasterListCount(securityFormBean);

            List ls_securityMasterList = this.merchantDAO.securityMasterList(securityFormBean);

            sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(securityFormBean.getStart_no())-1)+"\",\"rows\" : [");

            for (int i = 0; i < ls_securityMasterList.size(); i++) { 
                    Tb_Security tb_Security = (Tb_Security) ls_securityMasterList.get(i);

                    sb.append("{\"id\":" + i);
                    sb.append(",\"userdata\":{");
                    sb.append("\"security_seq\":\""+tb_Security.getSecurity_seq()+"\"");
                    sb.append("}");                
                    sb.append(",\"data\":[");
                    sb.append(" \"" + tb_Security.getRnum() + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Security.getOnffmerch_no()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Security.getMerch_nm()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Security.getSecurity_cate_nm()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Security.getSecurity_amt()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Security.getStart_dt()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Security.getEnd_dt()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Security.getRemain_cnt()) + "\"");

                    if (i == (ls_securityMasterList.size() - 1)) {
                            sb.append("]}");
                    } else {
                            sb.append("]},");
                    }
            }

            sb.append("]}");    

            ht.put("ResultSet", sb.toString());
            
        }catch(Exception e){
            
            e.printStackTrace();
            
        }
        
        return ht;        
        
    }

    //가맹점 결제한도 조회
    @Override
    public Hashtable saleLimitMasterList(MerchantFormBean merchantFormBean) throws Exception {
        
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
       
        try {
      
            Integer total_count = this.merchantDAO.saleLimitMasterListCount(merchantFormBean);

            List ls_saleLimitMasterList = this.merchantDAO.saleLimitMasterList(merchantFormBean);

            sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(merchantFormBean.getStart_no())-1)+"\",\"rows\" : [");

            for (int i = 0; i < ls_saleLimitMasterList.size(); i++) { 
                    Tb_Onffmerch_Mst tb_Onffmerch_Mst = (Tb_Onffmerch_Mst) ls_saleLimitMasterList.get(i);

                    sb.append("{\"id\":" + i);
                    sb.append(",\"userdata\":{");
                    sb.append("\"app_chk_status\":\""+Util.nullToString(tb_Onffmerch_Mst.getApp_chk_status())+"\"");
                    sb.append("}");                                    
                    sb.append(",\"data\":[");
                    sb.append(" \"" + tb_Onffmerch_Mst.getRnum() + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Onffmerch_Mst.getOnffmerch_no()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Onffmerch_Mst.getMerch_nm()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Onffmerch_Mst.getApp_chk_amt()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Onffmerch_Mst.getCur_app_chk_amt()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Onffmerch_Mst.getApp_chk_status()) + "%" + "\"");

                    if (i == (ls_saleLimitMasterList.size() - 1)) {
                            sb.append("]}");
                    } else {
                            sb.append("]},");
                    }
            }

            sb.append("]}");    

            ht.put("ResultSet", sb.toString());
            
        }catch(Exception e){
            
            e.printStackTrace();
            
        }
        
        return ht;       
    }

    //담조회 엑셀다운로드
    @Override
    public void securityMasterListExcel(MerchantFormBean merchantFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
             
                List ls_securityMasterListExcel = merchantDAO.securityMasterListExcel(merchantFormBean);
                
                StringBuffer securityMasterListExcel = Util.makeData(ls_securityMasterListExcel);
        
                Util.exceldownload(securityMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    }

}
