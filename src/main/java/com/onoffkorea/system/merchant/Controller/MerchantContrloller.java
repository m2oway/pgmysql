/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.merchant.Controller;

import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.common.vo.Tb_Code_Detail;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.merchant.Bean.MerchantFormBean;
import com.onoffkorea.system.merchant.Bean.SecurityFormBean;
import com.onoffkorea.system.merchant.Service.MerchantService;
import com.onoffkorea.system.merchant.Vo.Tb_Onffmerch_Mst;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import common.web.servlet.BaseSpringMultiActionController;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/merchant/**")
public class MerchantContrloller extends BaseSpringMultiActionController{
    
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private MerchantService merchantService;
    
    @Autowired
    private CommonService commonService;
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }    
    
    //정보 조회
    @RequestMapping(method= RequestMethod.GET,value = "/merchantMasterList")
    public String merchantMasterList(@Valid MerchantFormBean merchantFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("BIZ_TYPE");
        Tb_Code_Main FuncCateList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("Biz_typeList",FuncCateList);
        
        commonCodeSearchFormBean.setMain_code("AGENCY_FLAG");
        Tb_Code_Main AgencyFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("AgencyFlagList",AgencyFlagList);
        
        commonCodeSearchFormBean.setMain_code("SVC_STAT");
        Tb_Code_Main UseFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("UseFlagList",UseFlagList);
        
        
        Hashtable ht_merchantMasterList = merchantService.merchantMasterList(merchantFormBean);
        String ls_companyMasterList = (String) ht_merchantMasterList.get("ResultSet");

        model.addAttribute("onffmerchinfoMasterList",ls_companyMasterList);
        
        return null;
        
    }
    
    //merchant 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/merchantMasterList")
    public @ResponseBody String merchantMasterInfo(@Valid MerchantFormBean merchantFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
        
        Hashtable ht_merchantMasterList = merchantService.merchantMasterList(merchantFormBean);
        String ls_merchantMasterList = (String)ht_merchantMasterList.get("ResultSet");
     
        return ls_merchantMasterList;
       
    }

        /*
    //merchant 정보 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/merchantMasterDelete")
    public @ResponseBody String merchantMasterDelete(@Valid MerchantFormBean merchantFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
      
        String result = null;
        
        result = merchantService.merchantMasterDelete(merchantFormBean);
        
        return result;
        
    }
    */
    
    //정보 등록
    @RequestMapping(method= RequestMethod.GET,value = "/merchantMasterInsert")
    public String merchantMasterInsertForm(@Valid MerchantFormBean merchantFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("SVC_STAT");
        Tb_Code_Main SvcStatList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("SvcStatList",SvcStatList);
        
        commonCodeSearchFormBean.setMain_code("MERCH_CATE");
        Tb_Code_Main MerchCateList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("MerchCateList",MerchCateList);

        commonCodeSearchFormBean.setMain_code("TAX_FLAG");
        Tb_Code_Main TaxFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("TaxFlagList",TaxFlagList);

        commonCodeSearchFormBean.setMain_code("BAL_PERIOD");
        Tb_Code_Main BalPeriodList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("BalPeriodList",BalPeriodList);
        
        commonCodeSearchFormBean.setMain_code("PAY_DT_CD_1");
        Tb_Code_Main PayDtCdList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("PayDtCdList",PayDtCdList);
        
        commonCodeSearchFormBean.setMain_code("CNCL_AUTH");
        Tb_Code_Main Cncl_AuthList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("Cncl_AuthList",Cncl_AuthList);
        
        //코드표를 만든다.
        StringBuilder sb_day = new StringBuilder();
        StringBuilder sb_week = new StringBuilder();        
        StringBuilder sb_half = new StringBuilder();        
        StringBuilder sb_month = new StringBuilder();
        
        sb_day.append("[");
        sb_week.append("[");
        sb_half.append("[");
        sb_month.append("[");
        
        int int_day = 0;
        int int_week = 0;
        int int_half = 0;
        int int_month = 0;
        
        for (int i = 0; i < PayDtCdList.getTb_code_details().size(); i++) {
                Tb_Code_Detail detailcodeobj = (Tb_Code_Detail) PayDtCdList.getTb_code_details().get(i);
           
                logger.trace("detailcodeobj.getCode_memo() : " + detailcodeobj.getCode_memo());
                
                if("DAY".equals(detailcodeobj.getCode_memo()))
                {   
                    if(int_day == 0)
                    {
                        sb_day.append("{");
                    }
                    else
                    {
                        sb_day.append(",{");
                    }
                    sb_day.append("\"VIEW\":\""+ detailcodeobj.getCode_nm() +"\",\"ID\":\""+ detailcodeobj.getDetail_code() +"\"");
                    sb_day.append("}");
                    int_day++;
                }
                else if("WEEK".equals(detailcodeobj.getCode_memo()))
                {
                    if(int_week == 0)
                    {
                        sb_week.append("{");
                    }
                    else
                    {
                        sb_week.append(",{");
                    }
                    sb_week.append("\"VIEW\":\""+ detailcodeobj.getCode_nm() +"\",\"ID\":\""+ detailcodeobj.getDetail_code() +"\"");
                    sb_week.append("}");
                    int_week++;                    
                }
                else if("HALF".equals(detailcodeobj.getCode_memo()))
                {
                    if(int_half == 0)
                    {
                        sb_half.append("{");
                    }
                    else
                    {
                        sb_half.append(",{");
                    }
                    sb_half.append("\"VIEW\":\""+ detailcodeobj.getCode_nm() +"\",\"ID\":\""+ detailcodeobj.getDetail_code() +"\"");
                    sb_half.append("}");
                    int_half++;                    
                }
                else if("MONTH".equals(detailcodeobj.getCode_memo()))
                {
                    if(int_month == 0)
                    {
                        sb_month.append("{");
                    }
                    else
                    {
                        sb_month.append(",{");
                    }
                    sb_month.append("\"VIEW\":\""+ detailcodeobj.getCode_nm() +"\",\"ID\":\""+ detailcodeobj.getDetail_code() +"\"");
                    sb_month.append("}");
                    int_month++;                    
                }
        }
        
        sb_day.append("]");
        sb_week.append("]");
        sb_half.append("]");
        sb_month.append("]");
        
        model.addAttribute("PAY_DT_CD_DAY",sb_day.toString());       
        model.addAttribute("PAY_DT_CD_WEEK",sb_week.toString());       
        model.addAttribute("PAY_DT_CD_HALF",sb_half.toString());       
        model.addAttribute("PAY_DT_CD_MONTH",sb_month.toString());       
        
        commonCodeSearchFormBean.setMain_code("BANK_CD");
        Tb_Code_Main BankCdList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("BankCdList",BankCdList);       
        
        commonCodeSearchFormBean.setMain_code("BIZ_TYPE");
        Tb_Code_Main BizTypeList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("BizTypeList",BizTypeList);
        
        commonCodeSearchFormBean.setMain_code("BIZ_CATE");
        Tb_Code_Main BizCateList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("BizCateList",BizCateList);        
        
        commonCodeSearchFormBean.setMain_code("SECURITY_CATE");
        Tb_Code_Main SecurityCateList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("SecurityCateList",SecurityCateList);        
        
        return null;
        
    }   
 
    //merchant 정보 등록
    @RequestMapping(method= RequestMethod.POST,value = "/merchantMasterInsert")
    public @ResponseBody String merchantMasterInsert(@Valid MerchantFormBean merchantFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {
        String result = null;
        
        merchantFormBean.setIns_user(dentalHopitalSession.getUser_seq());
        merchantFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        result = merchantService.merchantMasterInsert(merchantFormBean);
        
        return result;
        
    }       

    //merchant 정보 수정 
    @RequestMapping(method= RequestMethod.GET,value = "/merchantMasterUpdate")
    public String merchantMasterUpdateForm(@Valid MerchantFormBean merchantFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

            CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

            commonCodeSearchFormBean.setMain_code("SVC_STAT");
            Tb_Code_Main SvcStatList = commonService.codeSearch(commonCodeSearchFormBean);
            model.addAttribute("SvcStatList",SvcStatList);

            commonCodeSearchFormBean.setMain_code("MERCH_CATE");
            Tb_Code_Main MerchCateList = commonService.codeSearch(commonCodeSearchFormBean);
            model.addAttribute("MerchCateList",MerchCateList);

            commonCodeSearchFormBean.setMain_code("TAX_FLAG");
            Tb_Code_Main TaxFlagList = commonService.codeSearch(commonCodeSearchFormBean);
            model.addAttribute("TaxFlagList",TaxFlagList);

            commonCodeSearchFormBean.setMain_code("BAL_PERIOD");
            Tb_Code_Main BalPeriodList = commonService.codeSearch(commonCodeSearchFormBean);
            model.addAttribute("BalPeriodList",BalPeriodList);

            commonCodeSearchFormBean.setMain_code("PAY_DT_CD_1");
            Tb_Code_Main PayDtCdList = commonService.codeSearch(commonCodeSearchFormBean);
            model.addAttribute("PayDtCdList",PayDtCdList);
            
            
            commonCodeSearchFormBean.setMain_code("CNCL_AUTH");
            Tb_Code_Main Cncl_AuthList = commonService.codeSearch(commonCodeSearchFormBean);
            model.addAttribute("Cncl_AuthList",Cncl_AuthList);            

            //코드표를 만든다.
            StringBuilder sb_day = new StringBuilder();
            StringBuilder sb_week = new StringBuilder();        
            StringBuilder sb_half = new StringBuilder();        
            StringBuilder sb_month = new StringBuilder();

            sb_day.append("[");
            sb_week.append("[");
            sb_half.append("[");
            sb_month.append("[");

            int int_day = 0;
            int int_week = 0;
            int int_half = 0;
            int int_month = 0;

            for (int i = 0; i < PayDtCdList.getTb_code_details().size(); i++) {
                    Tb_Code_Detail detailcodeobj = (Tb_Code_Detail) PayDtCdList.getTb_code_details().get(i);

                    logger.trace("detailcodeobj.getCode_memo() : " + detailcodeobj.getCode_memo());

                    if("DAY".equals(detailcodeobj.getCode_memo()))
                    {   
                        if(int_day == 0)
                        {
                            sb_day.append("{");
                        }
                        else
                        {
                            sb_day.append(",{");
                        }
                        sb_day.append("\"VIEW\":\""+ detailcodeobj.getCode_nm() +"\",\"ID\":\""+ detailcodeobj.getDetail_code() +"\"");
                        sb_day.append("}");
                        int_day++;
                    }
                    else if("WEEK".equals(detailcodeobj.getCode_memo()))
                    {
                        if(int_week == 0)
                        {
                            sb_week.append("{");
                        }
                        else
                        {
                            sb_week.append(",{");
                        }
                        sb_week.append("\"VIEW\":\""+ detailcodeobj.getCode_nm() +"\",\"ID\":\""+ detailcodeobj.getDetail_code() +"\"");
                        sb_week.append("}");
                        int_week++;                    
                    }
                    else if("HALF".equals(detailcodeobj.getCode_memo()))
                    {
                        if(int_half == 0)
                        {
                            sb_half.append("{");
                        }
                        else
                        {
                            sb_half.append(",{");
                        }
                        sb_half.append("\"VIEW\":\""+ detailcodeobj.getCode_nm() +"\",\"ID\":\""+ detailcodeobj.getDetail_code() +"\"");
                        sb_half.append("}");
                        int_half++;                    
                    }
                    else if("MONTH".equals(detailcodeobj.getCode_memo()))
                    {
                        if(int_month == 0)
                        {
                            sb_month.append("{");
                        }
                        else
                        {
                            sb_month.append(",{");
                        }
                        sb_month.append("\"VIEW\":\""+ detailcodeobj.getCode_nm() +"\",\"ID\":\""+ detailcodeobj.getDetail_code() +"\"");
                        sb_month.append("}");
                        int_month++;                    
                    }
            }

            sb_day.append("]");
            sb_week.append("]");
            sb_half.append("]");
            sb_month.append("]");

            model.addAttribute("PAY_DT_CD_DAY",sb_day.toString());       
            model.addAttribute("PAY_DT_CD_WEEK",sb_week.toString());       
            model.addAttribute("PAY_DT_CD_HALF",sb_half.toString());       
            model.addAttribute("PAY_DT_CD_MONTH",sb_month.toString());       

            commonCodeSearchFormBean.setMain_code("BANK_CD");
            Tb_Code_Main BankCdList = commonService.codeSearch(commonCodeSearchFormBean);
            model.addAttribute("BankCdList",BankCdList);       

            commonCodeSearchFormBean.setMain_code("BIZ_TYPE");
            Tb_Code_Main BizTypeList = commonService.codeSearch(commonCodeSearchFormBean);
            model.addAttribute("BizTypeList",BizTypeList);

            commonCodeSearchFormBean.setMain_code("BIZ_CATE");
            Tb_Code_Main BizCateList = commonService.codeSearch(commonCodeSearchFormBean);
            model.addAttribute("BizCateList",BizCateList);        

            commonCodeSearchFormBean.setMain_code("SECURITY_CATE");
            Tb_Code_Main SecurityCateList = commonService.codeSearch(commonCodeSearchFormBean);
            model.addAttribute("SecurityCateList",SecurityCateList);      
            
            //가맹점정보
            Hashtable ht_merchantMasterList = merchantService.merchantMasterInfo(merchantFormBean);
            
            List ls_paymtdMasterList = (List) ht_merchantMasterList.get("ResultSet");
            Tb_Onffmerch_Mst tb_Merchant = (Tb_Onffmerch_Mst) ls_paymtdMasterList.get(0);
        
            model.addAttribute("tb_Merchant",tb_Merchant);
            
            SecurityFormBean securityFormBean = new SecurityFormBean();
            
            securityFormBean.setOnffmerch_no(merchantFormBean.getOnffmerch_no());
        
            Hashtable ht_merchantDtlList = merchantService.merchantDtlList(securityFormBean);
            String ls_merchantDtlList = (String)ht_merchantDtlList.get("ResultSet");
            
            model.addAttribute("merchantDtlList",ls_merchantDtlList);
            
        return null;
        
    }       
    

    //merchantdtl 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/merchantDtlList")
    public @ResponseBody String merchantDtlInfo(@Valid SecurityFormBean securityFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
        
        Hashtable ht_merchantMasterList = merchantService.merchantDtlList(securityFormBean);
        String ls_merchantMasterList = (String)ht_merchantMasterList.get("ResultSet");
     
        return ls_merchantMasterList;
    }    
    

    //merchant 정보 수정 
    @RequestMapping(method= RequestMethod.POST,value = "/merchantMasterUpdate")
    public @ResponseBody String merchantMasterUpdate(@Valid MerchantFormBean merchantFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        merchantFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        String result = merchantService.merchantMasterUpdate(merchantFormBean);
        
        return result;
        
    }            
    
    
    //merchant 정보 수정 
    @RequestMapping(method= RequestMethod.POST,value = "/merchantMasterDelete")
    public @ResponseBody String merchantMasterDelete(@Valid MerchantFormBean merchantFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        merchantFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        String result = merchantService.merchantMasterDelete(merchantFormBean);
        
        return result;
        
    }          
    
    
    //merchantdtl 정보 등록
    @RequestMapping(method= RequestMethod.POST,value = "/merchantDtlInsert")
    public @ResponseBody String merchantDtlInsert(@Valid SecurityFormBean securityFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        securityFormBean.setIns_user(dentalHopitalSession.getUser_seq());
        securityFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        String result = merchantService.merchantDtlInsert(securityFormBean);
        
        return result;
        
    }       
    
    
    //merchantdtl 정보 수정 
    @RequestMapping(method= RequestMethod.POST,value = "/merchantDtlUpdate")
    public @ResponseBody String merchantDtlUpdate(@Valid SecurityFormBean securityFormBean, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        securityFormBean.setIns_user(dentalHopitalSession.getUser_seq());
        securityFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        String result = merchantService.merchantDtlUpdate(securityFormBean);
        
        return result;
    }        
    
    
    //merchantdtl 정보 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/merchantDtlDelete")
    //public @ResponseBody String merchantDtlDelete(@Valid MerchantDetailFormBean merchantDtlFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
    public @ResponseBody String merchantDtlDelete(@Valid SecurityFormBean securityFormBean, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {                

        securityFormBean.setIns_user(dentalHopitalSession.getUser_seq());
        securityFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        String result = merchantService.merchantDtlDelete(securityFormBean);
        
        return result;
        
    }
    
    //담보 정보 조회
    @RequestMapping(method= RequestMethod.GET,value = "/securityMasterList")
    public String securityMasterFormList(@Valid SecurityFormBean securityFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
    
        Hashtable ht_securityMasterList = merchantService.securityMasterList(securityFormBean);
        String ls_securityMasterList = (String) ht_securityMasterList.get("ResultSet");

        model.addAttribute("ls_securityMasterList",ls_securityMasterList);          
        
        return null;
        
    }
    
    //담보 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/securityMasterList")
    public @ResponseBody String securityMasterList(@Valid SecurityFormBean securityFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
        
        Hashtable ht_securityMasterList = merchantService.securityMasterList(securityFormBean);
        String ls_securityMasterList = (String)ht_securityMasterList.get("ResultSet");
     
        return ls_securityMasterList;
    }        
    
    //결제한도 정보 조회
    @RequestMapping(method= RequestMethod.GET,value = "/saleLimitMasterList")
    public String saleLimitMasterFormList(@Valid MerchantFormBean merchantFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
    
        merchantFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            merchantFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            merchantFormBean.setSes_onfftid(siteSession.getSes_onfftid());
            merchantFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("03".equals(siteSession.getSes_user_cate())){
            merchantFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                
        
        Hashtable ht_saleLimitMasterList = merchantService.saleLimitMasterList(merchantFormBean);
        String ls_saleLimitMasterList = (String) ht_saleLimitMasterList.get("ResultSet");

        model.addAttribute("ls_saleLimitMasterList",ls_saleLimitMasterList);   
        
        return null;
        
    }
    
    //결제한도 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/saleLimitMasterList")
    public @ResponseBody String saleLimitMasterFormListForm(@Valid MerchantFormBean merchantFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
    
        merchantFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            merchantFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            merchantFormBean.setSes_onfftid(siteSession.getSes_onfftid());
            merchantFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("03".equals(siteSession.getSes_user_cate())){
            merchantFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                
        
        Hashtable ht_saleLimitMasterList = merchantService.saleLimitMasterList(merchantFormBean);
        String ls_saleLimitMasterList = (String) ht_saleLimitMasterList.get("ResultSet");

        //model.addAttribute("ls_saleLimitMasterList",ls_saleLimitMasterList);   
        
        return ls_saleLimitMasterList;
        
    }    
    
    //결제한도 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/securityMasterListExcel")
    public void securityMasterListExcel(@Valid MerchantFormBean merchantFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletResponse response
            , HttpServletRequest request, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {             
        
        String excelDownloadFilename = new String(getMessage("merchant.securityMasterListExcel.fileName"));
        
        merchantService.securityMasterListExcel(merchantFormBean, excelDownloadFilename, response);    
        
    }            
    
}
