/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.merchant.Vo;

/**
 *
 * @author MoonbongChoi
 */
public class Tb_Security {
    
    private String rnum;
    private String security_seq	;
    private String onffmerch_no	;
    private String security_cate	;
    private String security_cate_nm	;        
    private String security_amt	;
    private String start_dt			;
    private String end_dt			;
    private String memo			;
    private String del_flag		;
    private String del_flag_nm		;
    private String ins_user		;
    private String mod_user		;
    private String ins_dt			;
    private String mod_dt			;
    private String merch_nm;
    private String remain_cnt;

    public String getRemain_cnt() {
        return remain_cnt;
    }

    public void setRemain_cnt(String remain_cnt) {
        this.remain_cnt = remain_cnt;
    }

    public String getMerch_nm() {
        return merch_nm;
    }

    public void setMerch_nm(String merch_nm) {
        this.merch_nm = merch_nm;
    }

    public String getRnum() {
        return rnum;
    }

    public void setRnum(String rnum) {
        this.rnum = rnum;
    }

    public String getSecurity_seq() {
        return security_seq;
    }

    public void setSecurity_seq(String security_seq) {
        this.security_seq = security_seq;
    }

    public String getOnffmerch_no() {
        return onffmerch_no;
    }

    public void setOnffmerch_no(String onffmerch_no) {
        this.onffmerch_no = onffmerch_no;
    }

    public String getSecurity_cate() {
        return security_cate;
    }

    public void setSecurity_cate(String security_cate) {
        this.security_cate = security_cate;
    }

    public String getSecurity_cate_nm() {
        return security_cate_nm;
    }

    public void setSecurity_cate_nm(String security_cate_nm) {
        this.security_cate_nm = security_cate_nm;
    }

    public String getSecurity_amt() {
        return security_amt;
    }

    public void setSecurity_amt(String security_amt) {
        this.security_amt = security_amt;
    }

    public String getStart_dt() {
        return start_dt;
    }

    public void setStart_dt(String start_dt) {
        this.start_dt = start_dt;
    }

    public String getEnd_dt() {
        return end_dt;
    }

    public void setEnd_dt(String end_dt) {
        this.end_dt = end_dt;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getDel_flag() {
        return del_flag;
    }

    public void setDel_flag(String del_flag) {
        this.del_flag = del_flag;
    }

    public String getDel_flag_nm() {
        return del_flag_nm;
    }

    public void setDel_flag_nm(String del_flag_nm) {
        this.del_flag_nm = del_flag_nm;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }
    
}
