/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.cardbin.Dao;

import com.onoffkorea.system.cardbin.Bean.CardbinFormBean;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("cardbinDAO")
public class CardbinDAOImpl extends SqlSessionDaoSupport  implements CardbinDAO {
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Override
    public String cardbinRecordCount(CardbinFormBean cardbinFormBean) {
        return (String) getSqlSession().selectOne("Cardbin.cardbinRecordCount", cardbinFormBean);
    }

    @Override
    public List cardbinMasterList(CardbinFormBean cardbinFormBean) {
        return (List) getSqlSession().selectList("Cardbin.cardbinMasterList", cardbinFormBean);
    }

    @Override
    public List cardbinMasterInfo(CardbinFormBean cardbinFormBean) {
         return (List) getSqlSession().selectList("Cardbin.cardbinMasterList", cardbinFormBean);
    }

    @Override
    public void cardbinMasterDelete(CardbinFormBean cardbinFormBean) {
         getSqlSession().delete("Cardbin.cardbinMasterDelete", cardbinFormBean);
    }

    @Override
    public String cardbinMasterInsert(CardbinFormBean cardbinFormBean) {
        Integer result = getSqlSession().insert("Cardbin.cardbinMasterInsert", cardbinFormBean);
        return result.toString();
    }

    @Override
    public void cardbinMasterUpdate(CardbinFormBean cardbinFormBean) {
        getSqlSession().update("Cardbin.cardbinMasterUpdate", cardbinFormBean);
    }
    
}
