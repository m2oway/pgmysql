/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.cardbin.Dao;

import com.onoffkorea.system.cardbin.Bean.CardbinFormBean;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public interface CardbinDAO {
    
    public String cardbinRecordCount(CardbinFormBean cardbinFormBean);

    public List cardbinMasterList(CardbinFormBean cardbinFormBean);

    public List cardbinMasterInfo(CardbinFormBean cardbinFormBean);
    
    public void cardbinMasterDelete(CardbinFormBean cardbinFormBean);

    public String cardbinMasterInsert(CardbinFormBean cardbinFormBean);

    public void cardbinMasterUpdate(CardbinFormBean midFormBean);
}
