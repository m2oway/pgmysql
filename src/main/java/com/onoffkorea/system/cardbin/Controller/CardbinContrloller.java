/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.cardbin.Controller;

import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.cardbin.Bean.CardbinFormBean;
import com.onoffkorea.system.cardbin.Service.CardbinService;
import com.onoffkorea.system.cardbin.Vo.Tb_Cardbin_Info;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/cardbin/**")
public class CardbinContrloller {
    
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private CardbinService cardbinService;
    
    @Autowired
    private CommonService commonService;
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }    
    
    //cardbin 정보 조회
    @RequestMapping(method= RequestMethod.GET,value = "/cardbinMasterList")
    public String cardbinMasterList(@Valid CardbinFormBean cardbinFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("ISS_CD");
        Tb_Code_Main IssCdList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("IssCdList",IssCdList);
       
        
        Hashtable ht_cardbinMasterList = cardbinService.cardbinMasterList(cardbinFormBean);
        String ls_companyMasterList = (String) ht_cardbinMasterList.get("ResultSet");

        model.addAttribute("cardbinMasterList",ls_companyMasterList);
        
        return null;
        
    }
    
    //cardbin 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/cardbinMasterList")
    public @ResponseBody String cardbinMasterList(@Valid CardbinFormBean cardbinFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
        
        Hashtable ht_cardbinMasterList = cardbinService.cardbinMasterList(cardbinFormBean);
        String ls_cardbinMasterList = (String)ht_cardbinMasterList.get("ResultSet");
     
        return ls_cardbinMasterList;
       
    }
    
    //cardbin 정보 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/cardbinMasterDelete")
    public @ResponseBody String cardbinMasterDelete(@Valid CardbinFormBean cardbinFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {

        cardbinService.cardbinMasterDelete(cardbinFormBean);
        
        return null;
        
    }
    
    //사업체 정보 등록
    @RequestMapping(method= RequestMethod.GET,value = "/cardbinMasterInsert")
    public String cardbinMasterInsertForm(@Valid CardbinFormBean cardbinFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("ISS_CD");
        Tb_Code_Main IssCdList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("IssCdList",IssCdList);
        
        return null;
        
    }   
    
    //cardbin 정보 등록
    @RequestMapping(method= RequestMethod.POST,value = "/cardbinMasterInsert")
    public @ResponseBody String cardbinMasterInsert(@Valid CardbinFormBean cardbinFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        cardbinFormBean.setIns_user(dentalHopitalSession.getUser_seq());
        cardbinFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        String merchant_seq = cardbinService.cardbinMasterInsert(cardbinFormBean);
        
        return merchant_seq;
        
    }       
    
    //cardbin 정보 수정 
    @RequestMapping(method= RequestMethod.GET,value = "/cardbinMasterUpdate")
    public String cardbinMasterUpdateForm(@Valid CardbinFormBean cardbinFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_cardbinMasterList = cardbinService.cardbinMasterInfo(cardbinFormBean);
        List ls_cardbinMasterList = (List) ht_cardbinMasterList.get("ResultSet");
        Tb_Cardbin_Info tb_Cardbin = (Tb_Cardbin_Info) ls_cardbinMasterList.get(0);
        
        model.addAttribute("tb_Cardbin",tb_Cardbin);
        
        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("ISS_CD");
        Tb_Code_Main IssCdList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("IssCdList",IssCdList);
        
        return null;
        
    }       
    
    //cardbin 정보 수정 
    @RequestMapping(method= RequestMethod.POST,value = "/cardbinMasterUpdate")
    public String cardbinMasterUpdate(@Valid CardbinFormBean cardbinFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        cardbinFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        cardbinService.cardbinMasterUpdate(cardbinFormBean);
        
        return null;
        
    }            
}
