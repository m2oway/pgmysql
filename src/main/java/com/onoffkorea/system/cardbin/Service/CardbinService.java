/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.cardbin.Service;

import com.onoffkorea.system.cardbin.Bean.CardbinFormBean;
import java.util.Hashtable;

/**
 *
 * @author Administrator
 */
public interface CardbinService {
    
    public Hashtable cardbinMasterList(CardbinFormBean cardbinFormBean) throws Exception;

    public void cardbinMasterDelete(CardbinFormBean cardbinFormBean) throws Exception;

    public String cardbinMasterInsert(CardbinFormBean cardbinFormBean) throws Exception;

    public void cardbinMasterUpdate(CardbinFormBean cardbinFormBean) throws Exception;
    
    public Hashtable cardbinMasterInfo(CardbinFormBean cardbinFormBean) throws Exception;
    
}
