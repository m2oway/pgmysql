/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.cardbin.Service;

import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.cardbin.Bean.CardbinFormBean;
import com.onoffkorea.system.cardbin.Dao.CardbinDAO;
import com.onoffkorea.system.cardbin.Vo.Tb_Cardbin_Info;
import java.util.Hashtable;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service("cardbinService")
public class CardbinServiceImpl implements CardbinService
{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private CardbinDAO cardbinDAO;
    
    
    @Override
    public Hashtable cardbinMasterList(CardbinFormBean cardbinFormBean) throws Exception {
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
       
        String total_count = this.cardbinDAO.cardbinRecordCount(cardbinFormBean);
       
        List ls_cardbinMasterList = this.cardbinDAO.cardbinMasterList(cardbinFormBean);
        
        sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(cardbinFormBean.getStart_no())-1)+"\",\"rows\" : [");
        
        for (int i = 0; i < ls_cardbinMasterList.size(); i++) {
                Tb_Cardbin_Info tb_Cardbin = (Tb_Cardbin_Info) ls_cardbinMasterList.get(i);

                
                
                sb.append("{\"id\":" + tb_Cardbin.getCardbin_seq());
                sb.append(",\"data\":[");
                sb.append(" \"" + tb_Cardbin.getRnum() + "\"");
                sb.append(",\"" + "" + "\"");                
                
                sb.append(",\"" + Util.nullToString(tb_Cardbin.getIss_cd_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Cardbin.getCardbin()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Cardbin.getIns_dt()) + "\"");
                                
                if (i == (ls_cardbinMasterList.size() - 1)) {
                        sb.append("]}");
                } else {
                        sb.append("]},");
                }
        }

        sb.append("]}");    
        
        ht.put("ResultSet", sb.toString());
        
        return ht;
    }

    @Override
    @Transactional
    public void cardbinMasterDelete(CardbinFormBean cardbinFormBean) throws Exception {
         this.cardbinDAO.cardbinMasterDelete(cardbinFormBean);
    }

    @Override
    @Transactional
    public String cardbinMasterInsert(CardbinFormBean cardbinFormBean) throws Exception {
        String result = null;
        result = this.cardbinDAO.cardbinMasterInsert(cardbinFormBean);
        return result;
    }

    @Override
    @Transactional
    public void cardbinMasterUpdate(CardbinFormBean cardbinFormBean) throws Exception {
        this.cardbinDAO.cardbinMasterUpdate(cardbinFormBean);
    }

    @Override
    public Hashtable cardbinMasterInfo(CardbinFormBean cardbinFormBean) throws Exception {
        
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
        
        List ls_companyMasterList = this.cardbinDAO.cardbinMasterInfo(cardbinFormBean);
                
        ht.put("ResultSet", ls_companyMasterList);
        
        return ht;
    }
    
}
