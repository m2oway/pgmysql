/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.cardbin.Bean;

import com.onoffkorea.system.common.util.CommonSortableListPagingForm;

/**
 *
 * @author Administrator
 */
public class CardbinFormBean extends CommonSortableListPagingForm{
    
    private String cardbin_seq;	//카드빈일련번호
    private String iss_cd;		//카드사
    private String cardbin;		//카드빈
    private String memo;		//memo
    private String ins_dt;		//입력일
    private String mod_dt;		//수정일
    private String ins_user;	//입력자
    private String mod_user;	//수정자
    
    private String[] cardbin_seqs;

    public String getCardbin_seq() {
        return cardbin_seq;
    }

    public void setCardbin_seq(String cardbin_seq) {
        this.cardbin_seq = cardbin_seq;
    }

    public String getIss_cd() {
        return iss_cd;
    }

    public void setIss_cd(String iss_cd) {
        this.iss_cd = iss_cd;
    }

    public String getCardbin() {
        return cardbin;
    }

    public void setCardbin(String cardbin) {
        this.cardbin = cardbin;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    public String[] getCardbin_seqs() {
        return cardbin_seqs;
    }

    public void setCardbin_seqs(String[] cardbin_seqs) {
        this.cardbin_seqs = cardbin_seqs;
    }

    
    
}
