/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.mid.Bean;

import com.onoffkorea.system.common.util.CommonSortableListPagingForm;

/**
 *
 * @author Administrator
 */
public class MidFormBean extends CommonSortableListPagingForm{
    
    private String pay_mtd;			//가맹점구분   
    private String merch_no;		//가맹점 번호  
    private String merchant_seq;	//일련번호     
    private String mid_nm;			//MID명        
    private String func_cate;		//기능분류     
    private String agency_flag;		//자체/대행구분
    private String installment;		//할부         
    private String point_cate;		//포인트구분   
    
    private String commision;		//수수료       
    private String commision2;		//첵크수수료   
    private String point_commsion;		//포인트수수료 
    
    /*
    private Float commision;		//수수료       
    private Float commision2;		//첵크수수료   
    private Float point_commsion;		//포인트수수료     
    */
    
    private String memo;			//MEMO         
    private String start_dt;		//적용일       
    private String end_dt;			//적용종료일   
    private String bank_cd;			//은행코드     
    private String acc_no;			//입금계좌     
    private String use_flag;		//사용여부     
    private String del_flag;		//삭제여부     
    private String ins_dt;			//입력일       
    private String mod_dt;			//수정일       
    private String ins_user;		//입력자       
    private String mod_user;		//수정자     
    
    private String[] merchant_seqs;

    public String getPay_mtd() {
        return pay_mtd;
    }

    public void setPay_mtd(String pay_mtd) {
        this.pay_mtd = pay_mtd;
    }

    public String getMerch_no() {
        return merch_no;
    }

    public void setMerch_no(String merch_no) {
        this.merch_no = merch_no;
    }

    public String getMerchant_seq() {
        return merchant_seq;
    }

    public void setMerchant_seq(String merchant_seq) {
        this.merchant_seq = merchant_seq;
    }

    public String getMid_nm() {
        return mid_nm;
    }

    public void setMid_nm(String mid_nm) {
        this.mid_nm = mid_nm;
    }

    public String getFunc_cate() {
        return func_cate;
    }

    public void setFunc_cate(String func_cate) {
        this.func_cate = func_cate;
    }

    public String getAgency_flag() {
        return agency_flag;
    }

    public void setAgency_flag(String agency_flag) {
        this.agency_flag = agency_flag;
    }

    public String getInstallment() {
        return installment;
    }

    public void setInstallment(String installment) {
        this.installment = installment;
    }

    public String getPoint_cate() {
        return point_cate;
    }

    public void setPoint_cate(String point_cate) {
        this.point_cate = point_cate;
    }

    public String getCommision() {
        return commision;
    }

    public void setCommision(String commision) {
        this.commision = commision;
    }

    public String getCommision2() {
        return commision2;
    }

    public void setCommision2(String commision2) {
        this.commision2 = commision2;
    }

    public String getPoint_commsion() {
        return point_commsion;
    }

    public void setPoint_commsion(String point_commsion) {
        this.point_commsion = point_commsion;
    }

    /*
    public Float getCommision() {
        return commision;
    }

    public void setCommision(Float commision) {
        this.commision = commision;
    }

    public Float getCommision2() {
        return commision2;
    }

    public void setCommision2(Float commision2) {
        this.commision2 = commision2;
    }

    public Float getPoint_commsion() {
        return point_commsion;
    }

    public void setPoint_commsion(Float point_commsion) {
        this.point_commsion = point_commsion;
    }
    */
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getStart_dt() {
        return start_dt;
    }

    public void setStart_dt(String start_dt) {
        this.start_dt = start_dt;
    }

    public String getEnd_dt() {
        return end_dt;
    }

    public void setEnd_dt(String end_dt) {
        this.end_dt = end_dt;
    }

    public String getBank_cd() {
        return bank_cd;
    }

    public void setBank_cd(String bank_cd) {
        this.bank_cd = bank_cd;
    }

    public String getAcc_no() {
        return acc_no;
    }

    public void setAcc_no(String acc_no) {
        this.acc_no = acc_no;
    }

    public String getUse_flag() {
        return use_flag;
    }

    public void setUse_flag(String use_flag) {
        this.use_flag = use_flag;
    }

    public String getDel_flag() {
        return del_flag;
    }

    public void setDel_flag(String del_flag) {
        this.del_flag = del_flag;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    public String[] getMerchant_seqs() {
        return merchant_seqs;
    }

    public void setMerchant_seqs(String[] merchant_seqs) {
        this.merchant_seqs = merchant_seqs;
    }
    
    
    
}
