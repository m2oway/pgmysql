/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.mid.Dao;

import com.onoffkorea.system.mid.Bean.MidFormBean;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public interface MidDAO {
    
    public String midRecordCount(MidFormBean midFormBean);

    public List midMasterList(MidFormBean midFormBean);

    public List midMasterInfo(MidFormBean midFormBean);
    
    public Integer midMasterDelete(MidFormBean midFormBean);

    public String midMasterInsert(MidFormBean midFormBean);

    public Integer midMasterUpdate(MidFormBean midFormBean);
 
}
