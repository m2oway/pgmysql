/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.mid.Dao;

import com.onoffkorea.system.mid.Bean.MidFormBean;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("midDAO")
public class MidDAOImpl extends SqlSessionDaoSupport  implements MidDAO {
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Override
    public String midRecordCount(MidFormBean midFormBean) {
        return (String) getSqlSession().selectOne("Mid.midRecordCount", midFormBean);
    }

    @Override
    public List midMasterList(MidFormBean midFormBean) {
        return (List) getSqlSession().selectList("Mid.midMasterList", midFormBean);
    }

    @Override
    public List midMasterInfo(MidFormBean midFormBean) {
         return (List) getSqlSession().selectList("Mid.midMasterList", midFormBean);
    }

    @Override
    public Integer midMasterDelete(MidFormBean midFormBean) {
         Integer result = getSqlSession().update("Mid.midMasterDelete", midFormBean);
         return result;
    }

    @Override
    public String midMasterInsert(MidFormBean midFormBean) {
        Integer result = getSqlSession().insert("Mid.midMasterInsert", midFormBean);
        return result.toString();
    }

    @Override
    public Integer midMasterUpdate(MidFormBean midFormBean) {
        Integer result = getSqlSession().update("Mid.midMasterUpdate", midFormBean);
        return result;
    }
    
}
