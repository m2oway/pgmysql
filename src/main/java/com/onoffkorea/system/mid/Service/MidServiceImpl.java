/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.mid.Service;

import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.mid.Bean.MidFormBean;
import com.onoffkorea.system.mid.Dao.MidDAO;
import com.onoffkorea.system.mid.Vo.Tb_Mid_Info;
import java.util.Hashtable;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service("midService")
public class MidServiceImpl implements MidService
{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private MidDAO midDAO;
    
    
    @Override
    public Hashtable midMasterList(MidFormBean midFormBean) throws Exception {
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
       
        String total_count = this.midDAO.midRecordCount(midFormBean);
       
        List ls_midMasterList = this.midDAO.midMasterList(midFormBean);
        
        sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(midFormBean.getStart_no())-1)+"\",\"rows\" : [");
        
        for (int i = 0; i < ls_midMasterList.size(); i++) {
                Tb_Mid_Info tb_Mid = (Tb_Mid_Info) ls_midMasterList.get(i);

                /*
                <center>번호</center>,
                <center>일련번호</center>,
                <center>MID구분</center>,
                <center>MID명</center>,
                <center>MID</center>,
                <center>기능분류</center>,
                <center>자체/대행</center>,
                <center>수수료</center>,
                <center>첵크카드수수료</center>,
                <center>시작일</center>,
                <center>종료일</center>,
                <center>사용여부</center>
                */                
                
                sb.append("{\"id\":" + i);
                sb.append(",\"userdata\":{");
                sb.append(" \"merchant_seq\":\""+tb_Mid.getMerchant_seq()+"\"");
                sb.append("}"); 
                sb.append(",\"data\":[");
                sb.append(" \"" + tb_Mid.getRnum() + "\"");
                sb.append(",\"" + "" + "\"");                
                sb.append(",\"" + Util.nullToString(tb_Mid.getPay_mtd_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Mid.getMid_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Mid.getMerch_no()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Mid.getFunc_cate_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Mid.getAgency_flag_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Mid.getCommision()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Mid.getCommision2()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Mid.getStart_dt()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Mid.getEnd_dt()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Mid.getUse_flag_nm()) + "\"");
                
                if (i == (ls_midMasterList.size() - 1)) {
                        sb.append("]}");
                } else {
                        sb.append("]},");
                }
        }

        sb.append("]}");    
        
        ht.put("ResultSet", sb.toString());
        
        return ht;
    }

    @Override
    @Transactional
    public String midMasterDelete(MidFormBean midFormBean) throws Exception {
        
        Integer result = null;
        result = this.midDAO.midMasterDelete(midFormBean);
        return result.toString();   
    }

    @Override
    @Transactional
    public String midMasterInsert(MidFormBean midFormBean) throws Exception {
        String result = null;
        result = this.midDAO.midMasterInsert(midFormBean);
        return result;
    }

    @Override
    @Transactional
    public String midMasterUpdate(MidFormBean midFormBean) throws Exception {
        Integer result = null;
        result = this.midDAO.midMasterUpdate(midFormBean);
        return result.toString();   
    }

    @Override
    public Hashtable midMasterInfo(MidFormBean midFormBean) throws Exception {
        
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
        
        List ls_companyMasterList = this.midDAO.midMasterInfo(midFormBean);
                
        ht.put("ResultSet", ls_companyMasterList);
        
        return ht;
    }
    
}
