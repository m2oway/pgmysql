/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.mid.Service;

import com.onoffkorea.system.company.Bean.CompanyFormBean;
import com.onoffkorea.system.mid.Bean.MidFormBean;
import java.util.Hashtable;

/**
 *
 * @author Administrator
 */
public interface MidService {
    
    public Hashtable midMasterList(MidFormBean midFormBean) throws Exception;

    public String midMasterDelete(MidFormBean midFormBean) throws Exception;

    public String midMasterInsert(MidFormBean midFormBean) throws Exception;

    public String midMasterUpdate(MidFormBean midFormBean) throws Exception;
    
    public Hashtable midMasterInfo(MidFormBean midFormBean) throws Exception;
    
}
