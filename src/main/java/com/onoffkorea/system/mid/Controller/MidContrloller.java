/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.mid.Controller;

import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.mid.Bean.MidFormBean;
import com.onoffkorea.system.mid.Service.MidService;
import com.onoffkorea.system.mid.Vo.Tb_Mid_Info;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/mid/**")
public class MidContrloller {
    
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private MidService midService;
    
    @Autowired
    private CommonService commonService;
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }    
    
    //mid 정보 조회
    @RequestMapping(method= RequestMethod.GET,value = "/midMasterList")
    public String midMasterList(@Valid MidFormBean midFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("MID_PAY_MTD");
        Tb_Code_Main PayMtdList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("MidPayMtdList",PayMtdList);
        
        commonCodeSearchFormBean.setMain_code("MID_FUNC_CATE");
        Tb_Code_Main FuncCateList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("FuncCateList",FuncCateList);
        
        commonCodeSearchFormBean.setMain_code("AGENCY_FLAG");
        Tb_Code_Main AgencyFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("AgencyFlagList",AgencyFlagList);
        
        commonCodeSearchFormBean.setMain_code("USE_FLAG");
        Tb_Code_Main UseFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("UseFlagList",UseFlagList);
        
        Hashtable ht_midMasterList = midService.midMasterList(midFormBean);
        String ls_companyMasterList = (String) ht_midMasterList.get("ResultSet");

        model.addAttribute("midMasterList",ls_companyMasterList);
        
        return null;
        
    }
    
    //mid 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/midMasterList")
    public @ResponseBody String midMasterList(@Valid MidFormBean midFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
        
        Hashtable ht_midMasterList = midService.midMasterList(midFormBean);
        String ls_midMasterList = (String)ht_midMasterList.get("ResultSet");
     
        return ls_midMasterList;
       
    }
    
    
    //사업체 정보 등록
    @RequestMapping(method= RequestMethod.GET,value = "/midMasterInsert")
    public String midMasterInsertForm(@Valid MidFormBean midFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        /*
        MID_PAY_MTD		
        MID_FUNC_CATE	
        AGENCY_FLAG	
        BANK_CD		
        USE_FLAG	
        DEL_FLAG	
        */
        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("MID_PAY_MTD");
        Tb_Code_Main PayMtdList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("MidPayMtdList",PayMtdList);
        
        commonCodeSearchFormBean.setMain_code("MID_FUNC_CATE");
        Tb_Code_Main FuncCateList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("FuncCateList",FuncCateList);
        
        commonCodeSearchFormBean.setMain_code("AGENCY_FLAG");
        Tb_Code_Main AgencyFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("AgencyFlagList",AgencyFlagList);
        
        commonCodeSearchFormBean.setMain_code("BANK_CD");
        Tb_Code_Main BankCdList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("BankCdList",BankCdList);
        
        commonCodeSearchFormBean.setMain_code("USE_FLAG");
        Tb_Code_Main UseFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("UseFlagList",UseFlagList);
        
        
        commonCodeSearchFormBean.setMain_code("DEL_FLAG");
        Tb_Code_Main DelFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("DelFlagList",UseFlagList);
        
        
        return null;
        
    }   
    
    //mid 정보 등록
    @RequestMapping(method= RequestMethod.POST,value = "/midMasterInsert")
    public @ResponseBody String midMasterInsert(@Valid MidFormBean midFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        midFormBean.setIns_user(dentalHopitalSession.getUser_seq());
        midFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        String merchant_seq = midService.midMasterInsert(midFormBean);
        
        return merchant_seq;
        
    }       
    
    //mid 정보 수정 
    @RequestMapping(method= RequestMethod.GET,value = "/midMasterUpdate")
    public String midMasterUpdateForm(@Valid MidFormBean midFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_midMasterList = midService.midMasterInfo(midFormBean);
        List ls_midMasterList = (List) ht_midMasterList.get("ResultSet");
        Tb_Mid_Info tb_Mid = (Tb_Mid_Info) ls_midMasterList.get(0);
        
        model.addAttribute("tb_Mid",tb_Mid);
        
        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("MID_PAY_MTD");
        Tb_Code_Main PayMtdList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("MidPayMtdList",PayMtdList);
        
        commonCodeSearchFormBean.setMain_code("MID_FUNC_CATE");
        Tb_Code_Main FuncCateList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("FuncCateList",FuncCateList);
        
        commonCodeSearchFormBean.setMain_code("AGENCY_FLAG");
        Tb_Code_Main AgencyFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("AgencyFlagList",AgencyFlagList);
        
        commonCodeSearchFormBean.setMain_code("BANK_CD");
        Tb_Code_Main BankCdList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("BankCdList",BankCdList);
        
        commonCodeSearchFormBean.setMain_code("USE_FLAG");
        Tb_Code_Main UseFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("UseFlagList",UseFlagList);
        
        
        commonCodeSearchFormBean.setMain_code("DEL_FLAG");
        Tb_Code_Main DelFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("DelFlagList",UseFlagList);
        
        
        return null;
        
    }       
    
    //mid 정보 수정 
    @RequestMapping(method= RequestMethod.POST,value = "/midMasterUpdate")
    public @ResponseBody String  midMasterUpdate(@Valid MidFormBean midFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        midFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        String result = midService.midMasterUpdate(midFormBean);
        
        return result;
        
    }
    
    
        //mid 정보 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/midMasterDelete")
    public @ResponseBody String midMasterDelete(@Valid MidFormBean midFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {
        midFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        String result = midService.midMasterDelete(midFormBean);
        
        return result;
        
    }
}
