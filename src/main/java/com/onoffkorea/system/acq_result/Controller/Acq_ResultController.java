/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.acq_result.Controller;

import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import com.onoffkorea.system.acq_result.Bean.Acq_ResultFormBean;
import com.onoffkorea.system.acq_result.Service.Acq_ResultService;
import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import common.web.servlet.BaseSpringMultiActionController;
import java.util.Hashtable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/acq_result/**")
public class Acq_ResultController extends BaseSpringMultiActionController{
        
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private Acq_ResultService acq_resultService;

    @Autowired
    private CommonService commonService;              
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }   
    
    @RequestMapping(method= RequestMethod.GET,value = "/acq_resultMasterList")
    public String acq_resultMasterDetail(@Valid Acq_ResultFormBean acq_resultFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
        
            acq_resultFormBean.setPage_size("100");
            Hashtable ht_acq_resultMasterList = acq_resultService.acq_resultMasterDetail(acq_resultFormBean);
            String ls_acq_resultMasterList = (String) ht_acq_resultMasterList.get("acq_resultDetailMasterListJsonString");
            
            model.addAttribute("acq_resultDetailMasterListJsonString",ls_acq_resultMasterList);
        
            CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

            commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
            Tb_Code_Main PayTypeList = commonService.codeSearch(commonCodeSearchFormBean);
            model.addAttribute("PayTypeList",PayTypeList);

            commonCodeSearchFormBean.setMain_code("MID_PAY_MTD");
            Tb_Code_Main PayMtdList = commonService.codeSearch(commonCodeSearchFormBean);
            model.addAttribute("PayMtdList",PayMtdList);
            
            return null;
       
    }
    
    @RequestMapping(method= RequestMethod.POST,value = "/acq_resultMasterList")
    public @ResponseBody String acq_resultMasterDetail_Search(@Valid Acq_ResultFormBean acq_resultFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {

            acq_resultFormBean.setPage_size("100");
            Hashtable ht_acq_resultMasterList = acq_resultService.acq_resultMasterDetail(acq_resultFormBean);
            String ls_acq_resultMasterList = (String) ht_acq_resultMasterList.get("acq_resultDetailMasterListJsonString");
             
            return ls_acq_resultMasterList;
       
    }
    
    @RequestMapping(method= RequestMethod.GET,value = "/acq_depositeMasterList")
    public String acq_depositeMasterDetail(@Valid Acq_ResultFormBean acq_resultFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
        
            acq_resultFormBean.setPage_size("100");
            Hashtable ht_acq_depositeMasterList = acq_resultService.acq_depositeMasterDetail(acq_resultFormBean);
            String ls_acq_depositeMasterList = (String) ht_acq_depositeMasterList.get("acq_depositeMasterListJsonString");
            
            model.addAttribute("acq_depositeMasterListJsonString",ls_acq_depositeMasterList);
        
            
            return null;
       
    }
    
    @RequestMapping(method= RequestMethod.POST,value = "/acq_depositeMasterList")
    public @ResponseBody String acq_depositeMasterDetail_Search(@Valid Acq_ResultFormBean acq_resultFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {

            acq_resultFormBean.setPage_size("100");
            Hashtable ht_acq_depositeMasterList = acq_resultService.acq_depositeMasterDetail(acq_resultFormBean);
            String ls_acq_depositeMasterList = (String) ht_acq_depositeMasterList.get("acq_depositeMasterListJsonString");
             
            return ls_acq_depositeMasterList;
       
    }    
    
    
    @RequestMapping(method= RequestMethod.GET,value = "/acq_resultMasterDetailPop")
    public String acq_resultMasterDetailPop(@Valid Acq_ResultFormBean acq_ResultFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        
            acq_ResultFormBean.setPage_size("100");
            Hashtable ht_acq_resultMasterList = acq_resultService.acq_resultMasterDetailPop(acq_ResultFormBean);
            String ls_acq_resultMasterList = (String) ht_acq_resultMasterList.get("ResultSet");
            model.addAttribute("ls_middepositDetailList",ls_acq_resultMasterList);
            
            
            Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_acq_resultMasterList.get("payChnCateList");
            Tb_Code_Main acqCdList = (Tb_Code_Main) ht_acq_resultMasterList.get("acqCdList");
            
            model.addAttribute("payChnCateList",payChnCateList);             
            model.addAttribute("acqCdList",acqCdList);       
        
            return null;
       
    }
    
    @RequestMapping(method= RequestMethod.GET,value = "/acq_depositeDetailList")
    public String acq_resultMasterDetailFormPop3(@Valid Acq_ResultFormBean acq_ResultFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
        
            acq_ResultFormBean.setPage_size("100");
            Hashtable ht_acq_resultMasterList = acq_resultService.acq_resultMasterDetailPop(acq_ResultFormBean);
            String ls_acq_resultMasterList = (String) ht_acq_resultMasterList.get("ResultSet");
            model.addAttribute("ls_middepositDetailList",ls_acq_resultMasterList);
            
            
            Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_acq_resultMasterList.get("payChnCateList");
            Tb_Code_Main acqCdList = (Tb_Code_Main) ht_acq_resultMasterList.get("acqCdList");
            
            model.addAttribute("payChnCateList",payChnCateList);             
            model.addAttribute("acqCdList",acqCdList);      
            
            return null;
       
    }
    
    @RequestMapping(method= RequestMethod.POST,value = "/acq_depositeDetailList")
    public @ResponseBody String acq_resultMasterDetailPop3(@Valid Acq_ResultFormBean acq_resultFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
        
            acq_resultFormBean.setPage_size("100");
            Hashtable ht_acq_resultMasterList = acq_resultService.acq_resultMasterDetailPop(acq_resultFormBean);
            String ls_acq_resultMasterList = (String) ht_acq_resultMasterList.get("ResultSet");

        return ls_acq_resultMasterList;
       
    }    
    
    @RequestMapping(method= RequestMethod.POST,value = "/acq_resultMasterDetailPop")
    public @ResponseBody String acq_resultMasterDetailPop2(@Valid Acq_ResultFormBean acq_resultFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
            acq_resultFormBean.setPage_size("100");
            Hashtable ht_acq_resultMasterList = acq_resultService.acq_resultMasterDetailPop(acq_resultFormBean);
            String ls_acq_resultMasterList = (String) ht_acq_resultMasterList.get("ResultSet");
            model.addAttribute("ls_middepositDetailList",ls_acq_resultMasterList);
        
            return ls_acq_resultMasterList;
       
    }
    
    @RequestMapping(method= RequestMethod.POST,value = "/acq_resultMasterListExcel")
    public void acq_resultMasterListExcel(@Valid Acq_ResultFormBean acq_resultFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        String excelDownloadFilename = new String(getMessage("acq_result.acq_resultMasterListExcel.fileName"));
        
        acq_resultService.acq_resultMasterListExcel(acq_resultFormBean, excelDownloadFilename, response); 
       
    }    
    
    @RequestMapping(method= RequestMethod.POST,value = "/acq_resultDetailExcel")
    public void acq_resultDetailExcel(@Valid Acq_ResultFormBean acq_resultFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        String excelDownloadFilename = new String(getMessage("acq_result.acq_resultDetailExcel.fileName"));
        
        acq_resultService.acq_resultDetailExcel(acq_resultFormBean, excelDownloadFilename, response); 
       
    }                
    
    @RequestMapping(method= RequestMethod.POST,value = "/acq_depositeMasterListExcel")
    public void acq_depositeMasterListExcel(@Valid Acq_ResultFormBean acq_resultFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        String excelDownloadFilename = new String(getMessage("acq_result.acq_depositeMasterListExcel.fileName"));
        
        acq_resultService.acq_depositeMasterListExcel(acq_resultFormBean, excelDownloadFilename, response); 
       
    }    
    
    @RequestMapping(method= RequestMethod.POST,value = "/acq_resultMasterDetailPopExcel")
    public void acq_resultMasterDetailPopExcel(@Valid Acq_ResultFormBean acq_resultFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        String excelDownloadFilename = new String(getMessage("acq_result.acq_resultMasterDetailPopExcel.fileName"));
        
        acq_resultService.acq_resultMasterDetailPopExcel(acq_resultFormBean, excelDownloadFilename, response); 
       
    }       
    
    @RequestMapping(method= RequestMethod.POST,value = "/acq_depositeDetailListExcel")
    public void acq_depositeDetailListExcel(@Valid Acq_ResultFormBean acq_resultFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        String excelDownloadFilename = new String(getMessage("acq_result.acq_depositeDetailListExcel.fileName"));
        
        acq_resultService.acq_depositeDetailListExcel(acq_resultFormBean, excelDownloadFilename, response); 
       
    }        
    

    @RequestMapping(method= RequestMethod.GET,value = "/acq_resultChkMasterList")
    public String acq_resultChkMasterList(@Valid Acq_ResultFormBean acq_resultFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
        
            acq_resultFormBean.setPage_size("100");
            Hashtable ht_acq_resultChkMasterList = acq_resultService.acq_resultChkMasterList(acq_resultFormBean);
            String ls_acq_resultChkMasterList = (String) ht_acq_resultChkMasterList.get("acq_resultChkMasterList");
            
            model.addAttribute("ls_acq_resultChkMasterList",ls_acq_resultChkMasterList);
            
            return null;
       
    }
    
    @RequestMapping(method= RequestMethod.POST,value = "/acq_resultChkMasterList")
    public @ResponseBody String acq_resultChkMasterList_Search(@Valid Acq_ResultFormBean acq_resultFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {

            acq_resultFormBean.setPage_size("100");
            Hashtable ht_acq_resultChkMasterList = acq_resultService.acq_resultChkMasterList(acq_resultFormBean);
            String ls_acq_resultChkMasterList = (String) ht_acq_resultChkMasterList.get("acq_resultChkMasterList");
             
            return ls_acq_resultChkMasterList;
       
    }
    
        @RequestMapping(method= RequestMethod.POST,value = "/acq_resultChkMasterListExcel")
    public void acq_resultChkMasterListExcel(@Valid Acq_ResultFormBean acq_resultFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        String excelDownloadFilename = new String(getMessage("acq_result.acq_resultChkMasterListExcel.fileName"));
        
        acq_resultService.acq_resultChkMasterListExcel(acq_resultFormBean, excelDownloadFilename, response); 
       
    }    

    @RequestMapping(method= RequestMethod.GET,value = "/acq_resultChkDetailList")
    public String acq_resultChkDetailList(@Valid Acq_ResultFormBean acq_ResultFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
        
            acq_ResultFormBean.setPage_size("100");
            Hashtable ht_acq_resultChkDetailList = acq_resultService.acq_resultChkDetailList(acq_ResultFormBean);
            String ls_acq_resultChkDetailList = (String) ht_acq_resultChkDetailList.get("ResultSet");
            model.addAttribute("ls_acq_resultChkDetailList",ls_acq_resultChkDetailList);
            
            
            Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_acq_resultChkDetailList.get("payChnCateList");
            Tb_Code_Main acqCdList = (Tb_Code_Main) ht_acq_resultChkDetailList.get("acqCdList");
            Tb_Code_Main tran_chk_flagCdList = (Tb_Code_Main) ht_acq_resultChkDetailList.get("tran_chk_flagCdList");
            Tb_Code_Main record_cl_cdCdList = (Tb_Code_Main) ht_acq_resultChkDetailList.get("record_cl_cdCdList");
            
            model.addAttribute("payChnCateList",payChnCateList);             
            model.addAttribute("acqCdList",acqCdList);  
            model.addAttribute("tran_chk_flagCdList",tran_chk_flagCdList); 
            model.addAttribute("record_cl_cdCdList",record_cl_cdCdList); 
            
            return null;
       
    }
    
    @RequestMapping(method= RequestMethod.POST,value = "/acq_resultChkDetailList")
    public @ResponseBody String acq_resultChkDetailList_Search(@Valid Acq_ResultFormBean acq_resultFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
        
            acq_resultFormBean.setPage_size("100");
            Hashtable ht_acq_resultChkDetailList = acq_resultService.acq_resultChkDetailList(acq_resultFormBean);
            String ls_acq_resultChkDetailList = (String) ht_acq_resultChkDetailList.get("ResultSet");

        return ls_acq_resultChkDetailList;
       
    }        
    
        @RequestMapping(method= RequestMethod.POST,value = "/acq_resultChkDetailListExcel")
    public void acq_resultChkDetailListExcel(@Valid Acq_ResultFormBean acq_resultFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        String excelDownloadFilename = new String(getMessage("acq_result.acq_resultChkDetailListExcel.fileName"));
        
        acq_resultService.acq_resultChkDetailListExcel(acq_resultFormBean, excelDownloadFilename, response); 
       
    }        
}