/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.acq_result.Dao;

import com.onoffkorea.system.acq_result.Bean.Acq_ResultFormBean;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("acq_resultDAO")
public class Acq_ResultDAOImpl extends SqlSessionDaoSupport implements Acq_ResultDAO{
    
    protected Logger logger = Logger.getLogger(getClass().getName());
    
        //사업체 정보 관리
    @Override
    public List acq_resultMasterDetail(Acq_ResultFormBean acq_resultFormBean) {
        
        return (List) getSqlSession().selectList("Acq_Result.acq_resultMasterDetail", acq_resultFormBean);
        
    }
    
    @Override
    public List acq_resultMasterDetailPop(Acq_ResultFormBean acq_resultFormBean) {
        
        return (List) getSqlSession().selectList("Acq_Result.acq_resultMasterDetailPop", acq_resultFormBean);
        
    }

    //신용카드 내역 개수
    @Override
    public String acq_resultDetailMasterListCount(Acq_ResultFormBean acq_resultFormBean) throws Exception{

        return (String)getSqlSession().selectOne("Acq_Result.acq_resultDetailMasterListCount", acq_resultFormBean);

    }
    
    @Override
    public String acq_resultDetailMasterListPopCount(Acq_ResultFormBean acq_resultFormBean) throws Exception{

        return (String)getSqlSession().selectOne("Acq_Result.acq_resultDetailMasterListPopCount", acq_resultFormBean);

    }

    @Override
    public List acq_depositeMasterDetail(Acq_ResultFormBean acq_resultFormBean) {
        return (List) getSqlSession().selectList("Acq_Result.acq_depositeMasterDetail", acq_resultFormBean);
    }

    //매입내역 집계 엑셀다운로드
    @Override
    public List acq_resultMasterListExcel(Acq_ResultFormBean acq_resultFormBean) {
        
        return (List)getSqlSession().selectList("Acq_Result.acq_resultMasterListExcel",acq_resultFormBean);
        
    }
    
    //매입내역 엑셀다운로드
    @Override
    public List acq_resultDetailExcel(Acq_ResultFormBean acq_resultFormBean) {
        
        return (List)getSqlSession().selectList("Acq_Result.acq_resultDetailExcel",acq_resultFormBean);
        
    }    

    @Override
    public List acq_depositeMasterListExcel(Acq_ResultFormBean acq_resultFormBean) {
        
        return (List) getSqlSession().selectList("Acq_Result.acq_depositeMasterListExcel", acq_resultFormBean);
        
    }
    
    //입금예정액 엑셀다운로드
    @Override
    public List acq_resultMasterDetailPopExcel(Acq_ResultFormBean acq_resultFormBean) {
        
        return (List)getSqlSession().selectList("Acq_Result.acq_resultMasterDetailPopExcel",acq_resultFormBean);
        
    }        
    
    
    @Override
    public List acq_resultChkMasterList(Acq_ResultFormBean acq_resultFormBean) {
        
        return (List) getSqlSession().selectList("Acq_Result.acq_resultChkMasterList", acq_resultFormBean);
        
    }
    
    @Override
    public List acq_resultChkMasterListExcel(Acq_ResultFormBean acq_resultFormBean) {
        
        return (List) getSqlSession().selectList("Acq_Result.acq_resultChkMasterListExcel", acq_resultFormBean);
        
    }
    
        @Override
    public List acq_resultChkDetailListExcel(Acq_ResultFormBean acq_resultFormBean) {
        
        return (List) getSqlSession().selectList("Acq_Result.acq_resultChkDetailListExcel", acq_resultFormBean);
        
    }
    

    
}
