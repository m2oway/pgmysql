/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.acq_result.Dao;

import com.onoffkorea.system.acq_result.Bean.Acq_ResultFormBean;
import com.onoffkorea.system.acq_result.Vo.Tb_Acq_Result;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface Acq_ResultDAO {
    
    public List acq_resultMasterDetail(Acq_ResultFormBean acq_resultFormBean);
    
    public List acq_depositeMasterDetail(Acq_ResultFormBean acq_resultFormBean);
    
    public List acq_resultMasterDetailPop(Acq_ResultFormBean acq_resultFormBean);
    
    public String acq_resultDetailMasterListCount(Acq_ResultFormBean acq_resultFormBean) throws Exception;
    
    public String acq_resultDetailMasterListPopCount(Acq_ResultFormBean acq_resultFormBean) throws Exception;

    public List acq_resultMasterListExcel(Acq_ResultFormBean acq_resultFormBean);

    public List acq_resultDetailExcel(Acq_ResultFormBean acq_resultFormBean);

    public List acq_depositeMasterListExcel(Acq_ResultFormBean acq_resultFormBean);

    public List acq_resultMasterDetailPopExcel(Acq_ResultFormBean acq_resultFormBean);
    
    public List acq_resultChkMasterList(Acq_ResultFormBean acq_resultFormBean);      
    
    public List acq_resultChkMasterListExcel(Acq_ResultFormBean acq_resultFormBean);
        
    public List acq_resultChkDetailListExcel(Acq_ResultFormBean acq_resultFormBean);
    
}
