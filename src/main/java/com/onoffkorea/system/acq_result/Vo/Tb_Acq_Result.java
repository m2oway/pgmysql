/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.acq_result.Vo;

/**
 *
 * @author Administrator
 */
public class Tb_Acq_Result {
    
    private String file_seq           ;
    private String file_nm            ;
    private String pay_type           ;
    private String record_seq         ;
    private String record_cl_cd       ;
    private String org_record_cl_cd   ;
    private String merch_no           ;
    private String crncy_cd           ;
    private String crncy_idx          ;
    private String sal_dt             ;
    private String rcpt_dt            ;
    private String org_sal_dt         ;
    private String acq_dt             ;
    private String card_num           ;
    private String installment        ;
    private String tot_trx_amt        ;
    private String cardfm_ret_rsn_cd  ;
    private String van_ret_rsn_cd     ;
    private String iss_cd             ;
    private String org_iss_cd         ;
    private String acq_cd             ;
    private String org_acq_cd         ;
    private String filler             ;
    private String filler2            ;
    private String aprv_no            ;
    private String shop_pos_no        ;
    private String vat_amt            ;
    private String cmms_amt           ;
    private String pay_amt            ;
    private String pg_seq             ;
    private String order_seq          ;
    private String filler3            ;
    private String ins_dt             ;
    private String mod_dt             ;
    private String ins_user           ;
    private String mod_user           ;
    private String pay_dt             ;
    private String pay_type_nm;
    private String record_cl_cd_nm;
    private String acq_cd_nm;
    private String rnum;
    
    
    private String tran_chk_flag;
    private String tran_chk_flag_nm;
    private String tran_cnt_1;
    private String mid_nm;

    public String getTran_chk_flag() {
        return tran_chk_flag;
    }

    public void setTran_chk_flag(String tran_chk_flag) {
        this.tran_chk_flag = tran_chk_flag;
    }

    public String getTran_chk_flag_nm() {
        return tran_chk_flag_nm;
    }

    public void setTran_chk_flag_nm(String tran_chk_flag_nm) {
        this.tran_chk_flag_nm = tran_chk_flag_nm;
    }

    public String getTran_cnt_1() {
        return tran_cnt_1;
    }

    public void setTran_cnt_1(String tran_cnt_1) {
        this.tran_cnt_1 = tran_cnt_1;
    }

    public String getMid_nm() {
        return mid_nm;
    }

    public void setMid_nm(String mid_nm) {
        this.mid_nm = mid_nm;
    }
    
    

    public String getRnum() {
        return rnum;
    }

    public void setRnum(String rnum) {
        this.rnum = rnum;
    }

    public String getFile_seq() {
        return file_seq;
    }

    public void setFile_seq(String file_seq) {
        this.file_seq = file_seq;
    }

    public String getFile_nm() {
        return file_nm;
    }

    public void setFile_nm(String file_nm) {
        this.file_nm = file_nm;
    }

    public String getPay_type() {
        return pay_type;
    }

    public void setPay_type(String pay_type) {
        this.pay_type = pay_type;
    }

    public String getRecord_seq() {
        return record_seq;
    }

    public void setRecord_seq(String record_seq) {
        this.record_seq = record_seq;
    }

    public String getRecord_cl_cd() {
        return record_cl_cd;
    }

    public void setRecord_cl_cd(String record_cl_cd) {
        this.record_cl_cd = record_cl_cd;
    }

    public String getOrg_record_cl_cd() {
        return org_record_cl_cd;
    }

    public void setOrg_record_cl_cd(String org_record_cl_cd) {
        this.org_record_cl_cd = org_record_cl_cd;
    }

    public String getMerch_no() {
        return merch_no;
    }

    public void setMerch_no(String merch_no) {
        this.merch_no = merch_no;
    }

    public String getCrncy_cd() {
        return crncy_cd;
    }

    public void setCrncy_cd(String crncy_cd) {
        this.crncy_cd = crncy_cd;
    }

    public String getCrncy_idx() {
        return crncy_idx;
    }

    public void setCrncy_idx(String crncy_idx) {
        this.crncy_idx = crncy_idx;
    }

    public String getSal_dt() {
        return sal_dt;
    }

    public void setSal_dt(String sal_dt) {
        this.sal_dt = sal_dt;
    }

    public String getRcpt_dt() {
        return rcpt_dt;
    }

    public void setRcpt_dt(String rcpt_dt) {
        this.rcpt_dt = rcpt_dt;
    }

    public String getOrg_sal_dt() {
        return org_sal_dt;
    }

    public void setOrg_sal_dt(String org_sal_dt) {
        this.org_sal_dt = org_sal_dt;
    }

    public String getAcq_dt() {
        return acq_dt;
    }

    public void setAcq_dt(String acq_dt) {
        this.acq_dt = acq_dt;
    }

    public String getCard_num() {
        return card_num;
    }

    public void setCard_num(String card_num) {
        this.card_num = card_num;
    }

    public String getInstallment() {
        return installment;
    }

    public void setInstallment(String installment) {
        this.installment = installment;
    }

    public String getTot_trx_amt() {
        return tot_trx_amt;
    }

    public void setTot_trx_amt(String tot_trx_amt) {
        this.tot_trx_amt = tot_trx_amt;
    }

    public String getCardfm_ret_rsn_cd() {
        return cardfm_ret_rsn_cd;
    }

    public void setCardfm_ret_rsn_cd(String cardfm_ret_rsn_cd) {
        this.cardfm_ret_rsn_cd = cardfm_ret_rsn_cd;
    }

    public String getVan_ret_rsn_cd() {
        return van_ret_rsn_cd;
    }

    public void setVan_ret_rsn_cd(String van_ret_rsn_cd) {
        this.van_ret_rsn_cd = van_ret_rsn_cd;
    }

    public String getIss_cd() {
        return iss_cd;
    }

    public void setIss_cd(String iss_cd) {
        this.iss_cd = iss_cd;
    }

    public String getOrg_iss_cd() {
        return org_iss_cd;
    }

    public void setOrg_iss_cd(String org_iss_cd) {
        this.org_iss_cd = org_iss_cd;
    }

    public String getAcq_cd() {
        return acq_cd;
    }

    public void setAcq_cd(String acq_cd) {
        this.acq_cd = acq_cd;
    }

    public String getOrg_acq_cd() {
        return org_acq_cd;
    }

    public void setOrg_acq_cd(String org_acq_cd) {
        this.org_acq_cd = org_acq_cd;
    }

    public String getFiller() {
        return filler;
    }

    public void setFiller(String filler) {
        this.filler = filler;
    }

    public String getFiller2() {
        return filler2;
    }

    public void setFiller2(String filler2) {
        this.filler2 = filler2;
    }

    public String getAprv_no() {
        return aprv_no;
    }

    public void setAprv_no(String aprv_no) {
        this.aprv_no = aprv_no;
    }

    public String getShop_pos_no() {
        return shop_pos_no;
    }

    public void setShop_pos_no(String shop_pos_no) {
        this.shop_pos_no = shop_pos_no;
    }

    public String getVat_amt() {
        return vat_amt;
    }

    public void setVat_amt(String vat_amt) {
        this.vat_amt = vat_amt;
    }

    public String getCmms_amt() {
        return cmms_amt;
    }

    public void setCmms_amt(String cmms_amt) {
        this.cmms_amt = cmms_amt;
    }

    public String getPay_amt() {
        return pay_amt;
    }

    public void setPay_amt(String pay_amt) {
        this.pay_amt = pay_amt;
    }

    public String getPg_seq() {
        return pg_seq;
    }

    public void setPg_seq(String pg_seq) {
        this.pg_seq = pg_seq;
    }

    public String getOrder_seq() {
        return order_seq;
    }

    public void setOrder_seq(String order_seq) {
        this.order_seq = order_seq;
    }

    public String getFiller3() {
        return filler3;
    }

    public void setFiller3(String filler3) {
        this.filler3 = filler3;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    public String getPay_dt() {
        return pay_dt;
    }

    public void setPay_dt(String pay_dt) {
        this.pay_dt = pay_dt;
    }

    public String getPay_type_nm() {
        return pay_type_nm;
    }

    public void setPay_type_nm(String pay_type_nm) {
        this.pay_type_nm = pay_type_nm;
    }

    public String getRecord_cl_cd_nm() {
        return record_cl_cd_nm;
    }

    public void setRecord_cl_cd_nm(String record_cl_cd_nm) {
        this.record_cl_cd_nm = record_cl_cd_nm;
    }

    public String getAcq_cd_nm() {
        return acq_cd_nm;
    }

    public void setAcq_cd_nm(String acq_cd_nm) {
        this.acq_cd_nm = acq_cd_nm;
    }

}