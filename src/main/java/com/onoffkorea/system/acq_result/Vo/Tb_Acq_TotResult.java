/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.acq_result.Vo;

/**
 *
 * @author Administrator
 */
public class Tb_Acq_TotResult {
        private String rnum;
    
      private String file_seq;
      private String file_nm;
      private String pay_type;
      private String pay_type_nm;
      private String file_wrt_dt;
      private String purch_fm_bizr_no;
      private String jo_shop_bizr_no;
      private String rcpt_sta_dt;
      private String rcpt_end_dt;
      private String pay_plan_dt;
      private String merch_no;
      private String mid_nm;
      private String pay_mtd;
      private String pay_mtd_nm;
      private String merch_no_cate;
      private String svc_cl_cd;
      private String cal_cate;
      private String sal_sta_dt;
      private String sal_end_dt;
      private String inv_dt;
      private String acq_cd;
      private String rcpt_cnt;
      private String rcpt_amt;
      private String app_cnt;
      private String app_amt;
      private String cncl_cnt;
      private String cncl_amt;
      private String ret_cnt;
      private String ret_amt;
      private String dfr_cnt;
      private String dfr_amt;
      private String dfr_relse_cnt;
      private String dfr_relse_amt;
      private String tot_cnt;
      private String tot_amt;
      private String cmms_amt;
      private String vat_amt;
      private String pay_amt;
      private String bank_cd;
      private String acc_no;
      private String pay_dt;

    public String getVat_amt() {
        return vat_amt;
    }

    public void setVat_amt(String vat_amt) {
        this.vat_amt = vat_amt;
    }
   
    

    public String getPay_dt() {
        return pay_dt;
    }

    public void setPay_dt(String pay_dt) {
        this.pay_dt = pay_dt;
    }
      
      

    public String getRnum() {
        return rnum;
    }

    public void setRnum(String rnum) {
        this.rnum = rnum;
    }

    public String getFile_seq() {
        return file_seq;
    }

    public void setFile_seq(String file_seq) {
        this.file_seq = file_seq;
    }

    public String getFile_nm() {
        return file_nm;
    }

    public void setFile_nm(String file_nm) {
        this.file_nm = file_nm;
    }

    public String getPay_type() {
        return pay_type;
    }

    public void setPay_type(String pay_type) {
        this.pay_type = pay_type;
    }

    public String getPay_type_nm() {
        return pay_type_nm;
    }

    public void setPay_type_nm(String pay_type_nm) {
        this.pay_type_nm = pay_type_nm;
    }

    public String getFile_wrt_dt() {
        return file_wrt_dt;
    }

    public void setFile_wrt_dt(String file_wrt_dt) {
        this.file_wrt_dt = file_wrt_dt;
    }

    public String getPurch_fm_bizr_no() {
        return purch_fm_bizr_no;
    }

    public void setPurch_fm_bizr_no(String purch_fm_bizr_no) {
        this.purch_fm_bizr_no = purch_fm_bizr_no;
    }

    public String getJo_shop_bizr_no() {
        return jo_shop_bizr_no;
    }

    public void setJo_shop_bizr_no(String jo_shop_bizr_no) {
        this.jo_shop_bizr_no = jo_shop_bizr_no;
    }

    public String getRcpt_sta_dt() {
        return rcpt_sta_dt;
    }

    public void setRcpt_sta_dt(String rcpt_sta_dt) {
        this.rcpt_sta_dt = rcpt_sta_dt;
    }

    public String getRcpt_end_dt() {
        return rcpt_end_dt;
    }

    public void setRcpt_end_dt(String rcpt_end_dt) {
        this.rcpt_end_dt = rcpt_end_dt;
    }

    public String getPay_plan_dt() {
        return pay_plan_dt;
    }

    public void setPay_plan_dt(String pay_plan_dt) {
        this.pay_plan_dt = pay_plan_dt;
    }

    public String getMerch_no() {
        return merch_no;
    }

    public void setMerch_no(String merch_no) {
        this.merch_no = merch_no;
    }

    public String getMid_nm() {
        return mid_nm;
    }

    public void setMid_nm(String mid_nm) {
        this.mid_nm = mid_nm;
    }

    public String getPay_mtd() {
        return pay_mtd;
    }

    public void setPay_mtd(String pay_mtd) {
        this.pay_mtd = pay_mtd;
    }

    public String getPay_mtd_nm() {
        return pay_mtd_nm;
    }

    public void setPay_mtd_nm(String pay_mtd_nm) {
        this.pay_mtd_nm = pay_mtd_nm;
    }

    public String getMerch_no_cate() {
        return merch_no_cate;
    }

    public void setMerch_no_cate(String merch_no_cate) {
        this.merch_no_cate = merch_no_cate;
    }

    public String getSvc_cl_cd() {
        return svc_cl_cd;
    }

    public void setSvc_cl_cd(String svc_cl_cd) {
        this.svc_cl_cd = svc_cl_cd;
    }

    public String getCal_cate() {
        return cal_cate;
    }

    public void setCal_cate(String cal_cate) {
        this.cal_cate = cal_cate;
    }

    public String getSal_sta_dt() {
        return sal_sta_dt;
    }

    public void setSal_sta_dt(String sal_sta_dt) {
        this.sal_sta_dt = sal_sta_dt;
    }

    public String getSal_end_dt() {
        return sal_end_dt;
    }

    public void setSal_end_dt(String sal_end_dt) {
        this.sal_end_dt = sal_end_dt;
    }

    public String getInv_dt() {
        return inv_dt;
    }

    public void setInv_dt(String inv_dt) {
        this.inv_dt = inv_dt;
    }

    public String getAcq_cd() {
        return acq_cd;
    }

    public void setAcq_cd(String acq_cd) {
        this.acq_cd = acq_cd;
    }

    public String getRcpt_cnt() {
        return rcpt_cnt;
    }

    public void setRcpt_cnt(String rcpt_cnt) {
        this.rcpt_cnt = rcpt_cnt;
    }

    public String getRcpt_amt() {
        return rcpt_amt;
    }

    public void setRcpt_amt(String rcpt_amt) {
        this.rcpt_amt = rcpt_amt;
    }

    public String getApp_cnt() {
        return app_cnt;
    }

    public void setApp_cnt(String app_cnt) {
        this.app_cnt = app_cnt;
    }

    public String getApp_amt() {
        return app_amt;
    }

    public void setApp_amt(String app_amt) {
        this.app_amt = app_amt;
    }

    public String getCncl_cnt() {
        return cncl_cnt;
    }

    public void setCncl_cnt(String cncl_cnt) {
        this.cncl_cnt = cncl_cnt;
    }

    public String getCncl_amt() {
        return cncl_amt;
    }

    public void setCncl_amt(String cncl_amt) {
        this.cncl_amt = cncl_amt;
    }

    public String getRet_cnt() {
        return ret_cnt;
    }

    public void setRet_cnt(String ret_cnt) {
        this.ret_cnt = ret_cnt;
    }

    public String getRet_amt() {
        return ret_amt;
    }

    public void setRet_amt(String ret_amt) {
        this.ret_amt = ret_amt;
    }

    public String getDfr_cnt() {
        return dfr_cnt;
    }

    public void setDfr_cnt(String dfr_cnt) {
        this.dfr_cnt = dfr_cnt;
    }

    public String getDfr_amt() {
        return dfr_amt;
    }

    public void setDfr_amt(String dfr_amt) {
        this.dfr_amt = dfr_amt;
    }

    public String getDfr_relse_cnt() {
        return dfr_relse_cnt;
    }

    public void setDfr_relse_cnt(String dfr_relse_cnt) {
        this.dfr_relse_cnt = dfr_relse_cnt;
    }

    public String getDfr_relse_amt() {
        return dfr_relse_amt;
    }

    public void setDfr_relse_amt(String dfr_relse_amt) {
        this.dfr_relse_amt = dfr_relse_amt;
    }

    public String getTot_cnt() {
        return tot_cnt;
    }

    public void setTot_cnt(String tot_cnt) {
        this.tot_cnt = tot_cnt;
    }

    public String getTot_amt() {
        return tot_amt;
    }

    public void setTot_amt(String tot_amt) {
        this.tot_amt = tot_amt;
    }

    public String getCmms_amt() {
        return cmms_amt;
    }

    public void setCmms_amt(String cmms_amt) {
        this.cmms_amt = cmms_amt;
    }

    public String getPay_amt() {
        return pay_amt;
    }

    public void setPay_amt(String pay_amt) {
        this.pay_amt = pay_amt;
    }

    public String getBank_cd() {
        return bank_cd;
    }

    public void setBank_cd(String bank_cd) {
        this.bank_cd = bank_cd;
    }

    public String getAcc_no() {
        return acc_no;
    }

    public void setAcc_no(String acc_no) {
        this.acc_no = acc_no;
    }
      
      
}