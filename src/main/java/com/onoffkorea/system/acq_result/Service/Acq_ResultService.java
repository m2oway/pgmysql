/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.acq_result.Service;

import com.onoffkorea.system.acq_result.Bean.Acq_ResultFormBean;
import com.onoffkorea.system.acq_result.Vo.Tb_Acq_Result;
import java.util.ArrayList;
import java.util.Hashtable;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public interface Acq_ResultService {
 
    public Hashtable acq_resultMasterDetail(Acq_ResultFormBean acq_resultFormBean) throws Exception;
    
    public Hashtable acq_depositeMasterDetail(Acq_ResultFormBean acq_resultFormBean) throws Exception;   
    
    public Hashtable acq_resultMasterDetailPop(Acq_ResultFormBean acq_resultFormBean) throws Exception;
    
    public Hashtable acq_resultChkMasterList(Acq_ResultFormBean acq_resultFormBean) throws Exception;

    public void acq_resultMasterListExcel(Acq_ResultFormBean acq_resultFormBean, String excelDownloadFilename, HttpServletResponse response);

    public void acq_resultDetailExcel(Acq_ResultFormBean acq_resultFormBean, String excelDownloadFilename, HttpServletResponse response);

    public void acq_depositeMasterListExcel(Acq_ResultFormBean acq_resultFormBean, String excelDownloadFilename, HttpServletResponse response);

    public void acq_resultMasterDetailPopExcel(Acq_ResultFormBean acq_resultFormBean, String excelDownloadFilename, HttpServletResponse response);
    
    public void acq_depositeDetailListExcel(Acq_ResultFormBean acq_resultFormBean, String excelDownloadFilename, HttpServletResponse response);
    
    public Hashtable acq_resultChkDetailList(Acq_ResultFormBean acq_resultFormBean) throws Exception;
    
    public void acq_resultChkMasterListExcel(Acq_ResultFormBean acq_resultFormBean, String excelDownloadFilename, HttpServletResponse response);
    public void acq_resultChkDetailListExcel(Acq_ResultFormBean acq_resultFormBean, String excelDownloadFilename, HttpServletResponse response);
}