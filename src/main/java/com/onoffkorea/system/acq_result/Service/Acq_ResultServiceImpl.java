/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.acq_result.Service;

import com.onoffkorea.system.acq_result.Bean.Acq_ResultFormBean;
import com.onoffkorea.system.acq_result.Dao.Acq_ResultDAO;
import com.onoffkorea.system.acq_result.Vo.Tb_Acq_Result;
import com.onoffkorea.system.acq_result.Vo.Tb_Acq_TotResult;
import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 */
@Service("acq_resultService")
public class Acq_ResultServiceImpl  implements Acq_ResultService{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private Acq_ResultDAO acq_resultDAO;
    
    
    @Autowired
    private CommonService commonService;    
    

    //사업체 정보 관리
    @Override
    public Hashtable acq_resultMasterDetail(Acq_ResultFormBean acq_resultFormBean) throws Exception{
        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        
        if((acq_resultFormBean.getInv_dt_start() == null) || (acq_resultFormBean.getInv_dt_end() == "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            acq_resultFormBean.setInv_dt_start(start_dt);
            acq_resultFormBean.setInv_dt_end(end_dt);
        }
        
        Hashtable ht = new Hashtable();
        
         String total_count = this.acq_resultDAO.acq_resultDetailMasterListCount(acq_resultFormBean);
        List<Tb_Acq_TotResult>  acq_resultDetailMasterList = (List<Tb_Acq_TotResult>)this.acq_resultDAO.acq_resultMasterDetail(acq_resultFormBean);
        StringBuilder sb = new StringBuilder();        
        sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(acq_resultFormBean.getStart_no())-1)+"\",\"rows\" : [");
      //sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(companyFormBean.getStart_no())-1)+"\",\"rows\" : [");    

            for (int i = 0; i < acq_resultDetailMasterList.size(); i++) {
                /*
                     mheaders += "<center>매입일</center>,<center>결제채널</center>,<center>결제채널명</center>,<center>MID구분</center>,<center>MID구분명</center>,<center>MID</center>";
                    mheaders += ",<center>입금예정액</center>,<center>수수료</center>"
                    mheaders += ",<center>매입요청액</center>,<center>매입성공액</center>,<center>수수료</center>,<center>반송액</center>,<center>보류액</center>,<center>보류해제액</center>";
                    mheaders += ",<center>매입요청건</center>,<center>매입성공건</center><center>반송건</center>,<center>보류건</center>,<center>보류해제건</center>";
                */
                Tb_Acq_TotResult tb_Acq_Result = (Tb_Acq_TotResult) acq_resultDetailMasterList.get(i);
                    sb.append("{\"id\": \""+i+"\"");
                    sb.append(",\"userdata\":{");
                    sb.append("\"inv_dt\":\""+tb_Acq_Result.getInv_dt()+"\"");
                    sb.append(",\"pay_type\":\""+tb_Acq_Result.getPay_type()+"\"");
                    sb.append(",\"merch_no\":\""+tb_Acq_Result.getMerch_no()+"\"");  
                    sb.append(",\"file_seq\":\""+tb_Acq_Result.getFile_seq()+"\"");
                    sb.append(",\"file_nm\":\""+tb_Acq_Result.getFile_nm()+"\"");  
                    sb.append(",\"rcpt_sta_dt\":\""+tb_Acq_Result.getRcpt_sta_dt()+"\"");
                    sb.append(",\"rcpt_end_dt\":\""+tb_Acq_Result.getRcpt_end_dt()+"\"");  
                    sb.append("}");
                    sb.append(",\"data\": [");                    
                    sb.append("\"" + Util.nullToString(tb_Acq_Result.getInv_dt()) + "\"");
//                    sb.append("\"" + Util.nullToString(tb_Acq_Result.getFile_wrt_dt()) + "\"");
//                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getPay_type()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getPay_type_nm()) + "\"");
//                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getPay_mtd()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getPay_mtd_nm()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getMerch_no()) + "\"");
                    
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getRcpt_sta_dt()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getRcpt_end_dt()) + "\"");                    
                    
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getPay_amt())+ "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getCmms_amt())+ "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getVat_amt())+ "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getRcpt_amt())+ "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getTot_amt())+ "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getRet_amt())+ "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getDfr_amt())+ "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getDfr_relse_amt())+ "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getRcpt_cnt())+ "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getTot_cnt())+ "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getRet_cnt())+ "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getDfr_cnt())+ "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getDfr_relse_cnt())+ "\"");
                    
                    if (i == (acq_resultDetailMasterList.size() - 1)) {
                            sb.append("]}");
                    } else {
                            sb.append("]},");
                    }
            }
            sb.append("]}"); 
            //sb.append("]}");    

            SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
            Date start_date = formatter.parse(acq_resultFormBean.getInv_dt_start());
            Date end_date   = formatter.parse(acq_resultFormBean.getInv_dt_end());
            
            String start_dt = formatter2.format(start_date);
            String end_dt = formatter2.format(end_date);

            acq_resultFormBean.setInv_dt_start(start_dt);
            acq_resultFormBean.setInv_dt_end(end_dt);
            
        ht.put("acq_resultDetailMasterListJsonString", sb.toString());
        return ht;
    }
    
    @Override
    public Hashtable acq_resultMasterDetailPop(Acq_ResultFormBean acq_resultFormBean) throws Exception{
        

        Hashtable ht = new Hashtable();

        try {
            
                List<Tb_Acq_Result> ls_middepositDetailList = acq_resultDAO.acq_resultMasterDetailPop(acq_resultFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"rows\" : [");
                for (int i = 0; i < ls_middepositDetailList.size(); i++) {
                        Tb_Acq_Result tb_Acq_Result = ls_middepositDetailList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Acq_Result.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getPay_type_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getMerch_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getRecord_cl_cd_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getAcq_cd_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getSal_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getAprv_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getTot_trx_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getInstallment()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getAcq_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getPay_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getCmms_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getVat_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getPay_amt()) + "\"");

                        if (i == (ls_middepositDetailList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);
                
                commonCodeSearchFormBean.setMain_code("ACQ_CD");
                Tb_Code_Main acqCdList = commonService.codeSearch(commonCodeSearchFormBean);
                
                ht.put("payChnCateList", payChnCateList);
                ht.put("acqCdList", acqCdList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;                   
        
    }

    @Override
    public Hashtable acq_depositeMasterDetail(Acq_ResultFormBean acq_resultFormBean) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        
        if((acq_resultFormBean.getPay_dt_start()== null) || (acq_resultFormBean.getPay_dt_end() == "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            acq_resultFormBean.setPay_dt_start(start_dt);
            acq_resultFormBean.setPay_dt_end(end_dt);
        }
        
        Hashtable ht = new Hashtable();
        
        //String total_count = this.acq_resultDAO.acq_resultDetailMasterListCount(acq_resultFormBean);
        String total_count =  "0";
        List<Tb_Acq_TotResult>  acq_resultDetailMasterList = (List<Tb_Acq_TotResult>)this.acq_resultDAO.acq_depositeMasterDetail(acq_resultFormBean);
        StringBuilder sb = new StringBuilder();        
        sb.append("{\"rows\" : [");
      //sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(companyFormBean.getStart_no())-1)+"\",\"rows\" : [");    

            for (int i = 0; i < acq_resultDetailMasterList.size(); i++) {
                /*
                     mheaders += "<center>매입일</center>,<center>결제채널</center>,<center>결제채널명</center>,<center>MID구분</center>,<center>MID구분명</center>,<center>MID</center>";
                    mheaders += ",<center>입금예정액</center>,<center>수수료</center>"
                    mheaders += ",<center>매입요청액</center>,<center>매입성공액</center>,<center>수수료</center>,<center>반송액</center>,<center>보류액</center>,<center>보류해제액</center>";
                    mheaders += ",<center>매입요청건</center>,<center>매입성공건</center><center>반송건</center>,<center>보류건</center>,<center>보류해제건</center>";
                */
                Tb_Acq_TotResult tb_Acq_Result = (Tb_Acq_TotResult) acq_resultDetailMasterList.get(i);
                    sb.append("{\"id\": \"" + i+"\"");
                    sb.append(",\"userdata\":{");
                    sb.append("\"pay_dt\":\""+tb_Acq_Result.getPay_dt()+"\"");                    
                    sb.append(",\"merch_no\":\""+tb_Acq_Result.getMerch_no()+"\"");                    
                    sb.append("}");
                    sb.append(",\"data\": [");                    
                    sb.append("\"" + Util.nullToString(tb_Acq_Result.getPay_dt()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getMid_nm()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getMerch_no()) + "\"");                    
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getPay_amt())+ "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getCmms_amt())+ "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getVat_amt())+ "\"");                    
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getTot_amt())+ "\"");
                    
                    if (i == (acq_resultDetailMasterList.size() - 1)) {
                            sb.append("]}");
                    } else {
                            sb.append("]},");
                    }
            }
            sb.append("]}"); 
            //sb.append("]}");    

            SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
            Date start_date = formatter.parse(acq_resultFormBean.getPay_dt_start());
            Date end_date   = formatter.parse(acq_resultFormBean.getPay_dt_end());
            
            String start_dt = formatter2.format(start_date);
            String end_dt = formatter2.format(end_date);

            acq_resultFormBean.setPay_dt_start(start_dt);
            acq_resultFormBean.setPay_dt_end(end_dt);
            
        ht.put("acq_depositeMasterListJsonString", sb.toString());
        return ht;
    }

    //매입내역 집계 엑셀다운로드
    @Override
    public void acq_resultMasterListExcel(Acq_ResultFormBean acq_resultFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
             
                List ls_acq_resultMasterListExcel = acq_resultDAO.acq_resultMasterListExcel(acq_resultFormBean);
                
                StringBuffer acq_resultMasterListExcel = Util.makeData(ls_acq_resultMasterListExcel);
        
                Util.exceldownload(acq_resultMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    }
    
    //매입내역 엑셀다운로드
    @Override
    public void acq_resultDetailExcel(Acq_ResultFormBean acq_resultFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
             
                List ls_acq_resultDetailExcel = acq_resultDAO.acq_resultDetailExcel(acq_resultFormBean);
                
                StringBuffer acq_resultDetailExcel = Util.makeData(ls_acq_resultDetailExcel);
        
                Util.exceldownload(acq_resultDetailExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    }    

    @Override
    public void acq_depositeMasterListExcel(Acq_ResultFormBean acq_resultFormBean, String excelDownloadFilename, HttpServletResponse response) {
        
        try {
             
                List ls_acq_depositeMasterListExcel = acq_resultDAO.acq_depositeMasterListExcel(acq_resultFormBean);
                
                StringBuffer acq_depositeMasterListExcel = Util.makeData(ls_acq_depositeMasterListExcel);
        
                Util.exceldownload(acq_depositeMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    }
    
    @Override
    public void acq_resultMasterDetailPopExcel(Acq_ResultFormBean acq_resultFormBean, String excelDownloadFilename, HttpServletResponse response) {
        
        try {
             
                List ls_acq_resultMasterDetailPopExcel = acq_resultDAO.acq_resultMasterDetailPopExcel(acq_resultFormBean);
                
                StringBuffer acq_resultMasterDetailPopExcel = Util.makeData(ls_acq_resultMasterDetailPopExcel);
        
                Util.exceldownload(acq_resultMasterDetailPopExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    }    
    
    @Override
    public void acq_depositeDetailListExcel(Acq_ResultFormBean acq_resultFormBean, String excelDownloadFilename, HttpServletResponse response) {
        
        try {
             
                List ls_acq_resultMasterDetailPopExcel = acq_resultDAO.acq_resultMasterDetailPopExcel(acq_resultFormBean);
                
                StringBuffer acq_resultMasterDetailPopExcel = Util.makeData(ls_acq_resultMasterDetailPopExcel);
        
                Util.exceldownload(acq_resultMasterDetailPopExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    } 
    
    
    //사업체 정보 관리
    @Override
    public Hashtable acq_resultChkMasterList(Acq_ResultFormBean acq_resultFormBean) throws Exception{
        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        
        if((acq_resultFormBean.getPay_dt_start() == null) || (acq_resultFormBean.getPay_dt_end() == "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            //cal.add(Calendar.DAY_OF_WEEK, -3);

            Calendar cal2 = Calendar.getInstance();
            cal2.setTime(now);
            cal2.add(Calendar.DAY_OF_WEEK, +6);            
            
            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(cal2.getTime());


            acq_resultFormBean.setPay_dt_start(start_dt);
            acq_resultFormBean.setPay_dt_end(end_dt);
        }
        
        Hashtable ht = new Hashtable();
        
        List<Tb_Acq_Result>  acq_resultChkMasterList = (List<Tb_Acq_Result>)this.acq_resultDAO.acq_resultChkMasterList(acq_resultFormBean);
        StringBuilder sb = new StringBuilder();  
        sb.append("{\"rows\" : [");
        
            for (int i = 0; i < acq_resultChkMasterList.size(); i++) 
            {
                    Tb_Acq_Result tb_Acq_Result = (Tb_Acq_Result) acq_resultChkMasterList.get(i);
                    sb.append("{\"id\": \""+i+"\"");
                    sb.append(",\"userdata\":{");
                    sb.append("\"pay_dt\":\""+tb_Acq_Result.getPay_dt()+"\"");
                    sb.append(",\"merch_no\":\""+tb_Acq_Result.getMerch_no()+"\"");  
                    sb.append(",\"sal_dt\":\""+tb_Acq_Result.getSal_dt()+"\"");
                    sb.append(",\"record_cl_cd\":\""+tb_Acq_Result.getRecord_cl_cd()+"\"");  
                    sb.append(",\"tran_chk_flag\":\""+tb_Acq_Result.getTran_chk_flag()+"\"");
                    sb.append("}");
                    sb.append(",\"data\": [");                    
                    sb.append("\"" + Util.nullToString(tb_Acq_Result.getPay_dt()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getMerch_no()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getMid_nm()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getSal_dt()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getRecord_cl_cd_nm())+ "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getTran_chk_flag_nm())+ "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getTran_cnt_1())+ "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getPay_amt())+ "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getTot_trx_amt())+ "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getCmms_amt())+ "\"");
                    sb.append(",\"" + Util.nullToString(tb_Acq_Result.getVat_amt())+ "\"");
                    
                    if (i == (acq_resultChkMasterList.size() - 1)) {
                            sb.append("]}");
                    } else {
                            sb.append("]},");
                    }
            }
            sb.append("]}"); 
            //sb.append("]}");    

            SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
            Date start_date = formatter.parse(acq_resultFormBean.getPay_dt_start());
            Date end_date   = formatter.parse(acq_resultFormBean.getPay_dt_end());
            
            String start_dt = formatter2.format(start_date);
            String end_dt = formatter2.format(end_date);

            acq_resultFormBean.setPay_dt_start(start_dt);
            acq_resultFormBean.setPay_dt_end(end_dt);
            
        ht.put("acq_resultChkMasterList", sb.toString());
        return ht;
    }
    
    

    @Override
    public Hashtable acq_resultChkDetailList(Acq_ResultFormBean acq_resultFormBean) throws Exception{
        

        Hashtable ht = new Hashtable();

        try {
            
                List<Tb_Acq_Result> ls_middepositDetailList = acq_resultDAO.acq_resultMasterDetailPop(acq_resultFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"rows\" : [");
                for (int i = 0; i < ls_middepositDetailList.size(); i++) {
                        Tb_Acq_Result tb_Acq_Result = ls_middepositDetailList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Acq_Result.getRnum()) + "\"");
                        //sb.append(",\"" + Util.nullToString(tb_Acq_Result.getPay_type_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getPay_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getMerch_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getMid_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getRecord_cl_cd_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getTran_chk_flag_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getAcq_cd_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getSal_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getOrg_sal_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getAprv_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getTot_trx_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getInstallment()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getAcq_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getPay_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getCmms_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getVat_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getPay_amt()) + "\"");

                        if (i == (ls_middepositDetailList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);
                
                commonCodeSearchFormBean.setMain_code("ACQ_CD");
                Tb_Code_Main acqCdList = commonService.codeSearch(commonCodeSearchFormBean);
                
                commonCodeSearchFormBean.setMain_code("TRAD_CHK_FLAG");
                Tb_Code_Main tran_chk_flagCdList = commonService.codeSearch(commonCodeSearchFormBean);
                
                commonCodeSearchFormBean.setMain_code("RECODE_CATE");
                Tb_Code_Main record_cl_cdCdList = commonService.codeSearch(commonCodeSearchFormBean);
                
                ht.put("payChnCateList", payChnCateList);
                ht.put("acqCdList", acqCdList);
                ht.put("tran_chk_flagCdList", tran_chk_flagCdList);
                ht.put("record_cl_cdCdList", record_cl_cdCdList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;                   
        
    }    
        
    @Override
    public void acq_resultChkMasterListExcel(Acq_ResultFormBean acq_resultFormBean, String excelDownloadFilename, HttpServletResponse response) {
        
        try {
             
                List ls_acq_resultMasterDetailPopExcel = acq_resultDAO.acq_resultChkMasterListExcel(acq_resultFormBean);
                
                StringBuffer acq_resultMasterDetailPopExcel = Util.makeData(ls_acq_resultMasterDetailPopExcel);
        
                Util.exceldownload(acq_resultMasterDetailPopExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    } 
    
    @Override
    public void acq_resultChkDetailListExcel(Acq_ResultFormBean acq_resultFormBean, String excelDownloadFilename, HttpServletResponse response) {
        
        try {
             
                List ls_acq_resultMasterDetailPopExcel = acq_resultDAO.acq_resultChkDetailListExcel(acq_resultFormBean);
                
                StringBuffer acq_resultMasterDetailPopExcel = Util.makeData(ls_acq_resultMasterDetailPopExcel);
        
                Util.exceldownload(acq_resultMasterDetailPopExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    }     
}
