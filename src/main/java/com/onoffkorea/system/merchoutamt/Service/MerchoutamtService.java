/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.merchoutamt.Service;

import com.onoffkorea.system.merchoutamt.Bean.MerchoutamtFormBean;
import java.util.Hashtable;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public interface MerchoutamtService {

    public Hashtable merchoutamtMasterList(MerchoutamtFormBean merchoutamtFormBean);

    public Hashtable merchoutamtDetailList(MerchoutamtFormBean merchoutamtFormBean);

    public String merchoutamtMasterInsert(MerchoutamtFormBean merchoutamtFormBean);

    public Hashtable merchsetoffinfoList(MerchoutamtFormBean merchoutamtFormBean);

    public void merchoutamtMasterUpdate(MerchoutamtFormBean merchoutamtFormBean);

    public void merchoutamtMasterDelete(MerchoutamtFormBean merchoutamtFormBean);

    public void merchoutamtDetailInsert(MerchoutamtFormBean merchoutamtFormBean);

    public void merchoutamtSetoffDelete(MerchoutamtFormBean merchoutamtFormBean);

    public void merchoutamtMasterListExcel(MerchoutamtFormBean merchoutamtFormBean, String excelDownloadFilename, HttpServletResponse response);

    public void merchoutamtDetailListExcel(MerchoutamtFormBean merchoutamtFormBean, String excelDownloadFilename, HttpServletResponse response);
    
}
