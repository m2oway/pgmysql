/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.merchoutamt.Service;

import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.merchoutamt.Bean.MerchoutamtFormBean;
import com.onoffkorea.system.merchoutamt.Dao.MerchoutamtDAO;
import com.onoffkorea.system.merchoutamt.Vo.Tb_Merch_Outamt_Main;
import com.onoffkorea.system.merchoutamt.Vo.Tb_Merch_Setoffinfo;
import com.onoffkorea.system.mid.Dao.MidDAO;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service("merchoutamtService")
public class MerchoutamtServiceImpl implements MerchoutamtService{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private CommonService commonService;    
    
    @Autowired
    private MerchoutamtDAO merchoutamtDAO;   
    
    @Autowired
    private MidDAO midDAO;

    //가맹점 미수금 조회
    @Override
    public Hashtable merchoutamtMasterList(MerchoutamtFormBean merchoutamtFormBean) {
        
        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = merchoutamtDAO.merchoutamtMasterListCount(merchoutamtFormBean);
                List<Tb_Merch_Outamt_Main> ls_merchoutamtMasterList = merchoutamtDAO.merchoutamtMasterList(merchoutamtFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(merchoutamtFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_merchoutamtMasterList.size(); i++) {
                        Tb_Merch_Outamt_Main tb_Merch_Outamt_Main = ls_merchoutamtMasterList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"onffmerch_no\":\""+tb_Merch_Outamt_Main.getOnffmerch_no()+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Merch_Outamt_Main.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Outamt_Main.getOnffmerch_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Outamt_Main.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Outamt_Main.getOut_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Outamt_Main.getCur_outamt()) + "\"");

                        if (i == (ls_merchoutamtMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                ht.put("ResultSet", sb.toString());

        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;             
        
    }

    //가맹점 미수금 상세 조회
    @Override
    public Hashtable merchoutamtDetailList(MerchoutamtFormBean merchoutamtFormBean) {
        
        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = merchoutamtDAO.merchoutamtDetailListCount(merchoutamtFormBean);
                List<Tb_Merch_Outamt_Main> ls_merchoutamtDetailList = merchoutamtDAO.merchoutamtDetailList(merchoutamtFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"rows\" : [");
                for (int i = 0; i < ls_merchoutamtDetailList.size(); i++) {
                        Tb_Merch_Outamt_Main tb_Merch_Outamt_Main = ls_merchoutamtDetailList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"outamt_seq\":\""+tb_Merch_Outamt_Main.getOutamt_seq()+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Merch_Outamt_Main.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Outamt_Main.getGen_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Outamt_Main.getOnffmerch_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Outamt_Main.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Outamt_Main.getOut_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Outamt_Main.getCur_outamt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Outamt_Main.getIns_reson_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Merch_Outamt_Main.getOutamt_stat_nm()) + "\"");

                        if (i == (ls_merchoutamtDetailList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);           
                
                commonCodeSearchFormBean.setMain_code("OUTAMT_STAT");
                Tb_Code_Main outamtStatList = commonService.codeSearch(commonCodeSearchFormBean);
                
                ht.put("ResultSet", sb.toString());
                ht.put("payChnCateList", payChnCateList);
                ht.put("outamtStatList", outamtStatList);
                ht.put("ls_merchoutamtDetailList", ls_merchoutamtDetailList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;          
        
    }

    //미수금 등록
    @Override
    @Transactional
    public String merchoutamtMasterInsert(MerchoutamtFormBean merchoutamtFormBean) {
        
        String outamt_seq = merchoutamtDAO.merchoutamtMasterInsert(merchoutamtFormBean);
        
        return outamt_seq;
        
    }

    //가맹점 미수금 상계 조회
    @Override
    public Hashtable merchsetoffinfoList(MerchoutamtFormBean merchoutamtFormBean) {

        Hashtable ht = new Hashtable();

        try {        
        
            List<Tb_Merch_Setoffinfo> ls_merchsetoffinfoList = merchoutamtDAO.merchsetoffinfoList(merchoutamtFormBean);
            
            StringBuilder sb = new StringBuilder();
            sb.append("{\"rows\" : [");
            for (int i = 0; i < ls_merchsetoffinfoList.size(); i++) {
                    Tb_Merch_Setoffinfo tb_Merch_Setoffinfo = ls_merchsetoffinfoList.get(i);
                    sb.append("{\"id\":" + tb_Merch_Setoffinfo.getSetoff_seq());          
                    sb.append(",\"userdata\":{");
                    sb.append(" \"outamt_seq\":\""+tb_Merch_Setoffinfo.getOutamt_seq()+"\"");
                    sb.append(",\"setoff_seq\":\""+tb_Merch_Setoffinfo.getSetoff_seq()+"\"");
                    sb.append(",\"setoff_amt\":\""+tb_Merch_Setoffinfo.getSetoff_amt()+"\"");
                    sb.append(",\"setoff_date\":\""+tb_Merch_Setoffinfo.getSetoff_date()+"\"");
                    sb.append(",\"pay_sche_seq\":\""+tb_Merch_Setoffinfo.getPay_sche_seq()+"\"");
                    sb.append("}");                                      
                    sb.append(",\"data\":[");
                    sb.append(" \"" + tb_Merch_Setoffinfo.getRnum() + "\"");
                    sb.append(",\"" + "" + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Merch_Setoffinfo.getSetoff_date()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Merch_Setoffinfo.getSetoff_cate_nm()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Merch_Setoffinfo.getSetoff_amt()) + "\"");
                    sb.append(",\"" + Util.nullToString(tb_Merch_Setoffinfo.getMemo()) + "\"");

                    if (i == (ls_merchsetoffinfoList.size() - 1)) {
                            sb.append("]}");
                    } else {
                            sb.append("]},");
                    }
            }

            sb.append("]}");           

            CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

            commonCodeSearchFormBean.setMain_code("SETOFF_CATE");
            Tb_Code_Main setoffCateList = commonService.codeSearch(commonCodeSearchFormBean);  

            commonCodeSearchFormBean.setMain_code("INS_RESON");
            Tb_Code_Main insResonList = commonService.codeSearch(commonCodeSearchFormBean);  
            
            ht.put("ResultSet", sb.toString());
            ht.put("setoffCateList", setoffCateList);
            ht.put("insResonList", insResonList);
     
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;           
        
    }

    //미수금 수정
    @Override
    @Transactional
    public void merchoutamtMasterUpdate(MerchoutamtFormBean merchoutamtFormBean) {
        
        merchoutamtDAO.merchoutamtMasterUpdate(merchoutamtFormBean);
        
    }
    
    //미수금 삭제
    @Override
    @Transactional
    public void merchoutamtMasterDelete(MerchoutamtFormBean merchoutamtFormBean) {
        
        merchoutamtDAO.merchoutamtMasterDelete(merchoutamtFormBean);
        
    }    
    
    //미수금 상계 등록
    @Override
    @Transactional
    public void merchoutamtDetailInsert(MerchoutamtFormBean merchoutamtFormBean) {
        
        merchoutamtDAO.merchoutamtDetailInsert(merchoutamtFormBean);
        //미수잔액 수정
        merchoutamtDAO.merchoutamtMasterUpdate_minusoutamt(merchoutamtFormBean);
        
    }    
    
    //상계 삭제
    @Override
    @Transactional
    public void merchoutamtSetoffDelete(MerchoutamtFormBean merchoutamtFormBean) {
        
        merchoutamtDAO.merchoutamtSetoffDelete(merchoutamtFormBean);
        
        merchoutamtDAO.merchoutamtMasterUpdate_plusoutamt(merchoutamtFormBean);
        
    }    

    //미수내역 엑셀다운로드
    @Override
    public void merchoutamtMasterListExcel(MerchoutamtFormBean merchoutamtFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
             
                List ls_merchoutamtMasterListExcel = merchoutamtDAO.merchoutamtMasterListExcel(merchoutamtFormBean);
                
                StringBuffer merchoutamtMasterListExcel = Util.makeData(ls_merchoutamtMasterListExcel);
        
                Util.exceldownload(merchoutamtMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    }
    
    //미수내역 상세 엑셀다운로드
    @Override
    public void merchoutamtDetailListExcel(MerchoutamtFormBean merchoutamtFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
             
                List ls_merchoutamtDetailListExcel = merchoutamtDAO.merchoutamtDetailListExcel(merchoutamtFormBean);
                
                StringBuffer merchoutamtDetailListExcel = Util.makeData(ls_merchoutamtDetailListExcel);
        
                Util.exceldownload(merchoutamtDetailListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    }    
    
}
