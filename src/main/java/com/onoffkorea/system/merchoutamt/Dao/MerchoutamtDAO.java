/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.merchoutamt.Dao;

import com.onoffkorea.system.merchoutamt.Bean.MerchoutamtFormBean;
import com.onoffkorea.system.merchoutamt.Vo.Tb_Merch_Outamt_Main;
import com.onoffkorea.system.merchoutamt.Vo.Tb_Merch_Setoffinfo;
import com.onoffkorea.system.outamt.Bean.OutamtFormBean;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface MerchoutamtDAO {

    public List<Tb_Merch_Outamt_Main> merchoutamtMasterList(MerchoutamtFormBean merchoutamtFormBean);

    public List<Tb_Merch_Outamt_Main> merchoutamtDetailList(MerchoutamtFormBean merchoutamtFormBean);

    public String merchoutamtMasterInsert(MerchoutamtFormBean merchoutamtFormBean);

    public List<Tb_Merch_Setoffinfo> merchsetoffinfoList(MerchoutamtFormBean merchoutamtFormBean);

    public void merchoutamtMasterUpdate(MerchoutamtFormBean merchoutamtFormBean);

    public void merchoutamtMasterDelete(MerchoutamtFormBean merchoutamtFormBean);

    public void merchoutamtDetailInsert(MerchoutamtFormBean merchoutamtFormBean);

    public void merchoutamtMasterUpdate_minusoutamt(MerchoutamtFormBean merchoutamtFormBean);

    public void merchoutamtSetoffDelete(MerchoutamtFormBean merchoutamtFormBean);

    public void merchoutamtMasterUpdate_plusoutamt(MerchoutamtFormBean merchoutamtFormBean);

    public List merchoutamtMasterListExcel(MerchoutamtFormBean merchoutamtFormBean);

    public List merchoutamtDetailListExcel(MerchoutamtFormBean merchoutamtFormBean);

    public Integer merchoutamtMasterListCount(MerchoutamtFormBean merchoutamtFormBean);

    public Integer merchoutamtDetailListCount(MerchoutamtFormBean merchoutamtFormBean);
    
}
