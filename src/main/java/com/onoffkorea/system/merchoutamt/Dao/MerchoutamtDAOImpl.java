/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.merchoutamt.Dao;

import com.onoffkorea.system.merchoutamt.Bean.MerchoutamtFormBean;
import com.onoffkorea.system.merchoutamt.Vo.Tb_Merch_Outamt_Main;
import com.onoffkorea.system.merchoutamt.Vo.Tb_Merch_Setoffinfo;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("merchoutamtDAO")
public class MerchoutamtDAOImpl extends SqlSessionDaoSupport implements MerchoutamtDAO{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    //가맹점 미수금 조회
    @Override
    public List<Tb_Merch_Outamt_Main> merchoutamtMasterList(MerchoutamtFormBean merchoutamtFormBean) {
        
        return (List)getSqlSession().selectList("Merchoutamt.merchoutamtMasterList",merchoutamtFormBean);
        
    }

    //가맹점 미수금 상세 조회
    @Override
    public List<Tb_Merch_Outamt_Main> merchoutamtDetailList(MerchoutamtFormBean merchoutamtFormBean) {
        
        return (List)getSqlSession().selectList("Merchoutamt.merchoutamtDetailList",merchoutamtFormBean);
        
    }

    //미수금 등록
    @Override
    public String merchoutamtMasterInsert(MerchoutamtFormBean merchoutamtFormBean) {

        Integer result = getSqlSession().insert("Merchoutamt.merchoutamtMasterInsert", merchoutamtFormBean);
        
        return result.toString();        
        
    }

    //가맹점 미수금 상계 조회
    @Override
    public List<Tb_Merch_Setoffinfo> merchsetoffinfoList(MerchoutamtFormBean merchoutamtFormBean) {
        
        return (List)getSqlSession().selectList("Merchoutamt.merchsetoffinfoList",merchoutamtFormBean);
        
    }

    //미수금 수정
    @Override
    public void merchoutamtMasterUpdate(MerchoutamtFormBean merchoutamtFormBean) {
        
        getSqlSession().update("Merchoutamt.merchoutamtMasterUpdate", merchoutamtFormBean);
        
    }
    
    //미수금 삭제
    @Override
    public void merchoutamtMasterDelete(MerchoutamtFormBean merchoutamtFormBean) {
        
        getSqlSession().delete("Merchoutamt.merchoutamtMasterDelete", merchoutamtFormBean);
        
    }    
    
    //미수금 상계 등록
    @Override
    public void merchoutamtDetailInsert(MerchoutamtFormBean merchoutamtFormBean) {
        
        getSqlSession().insert("Merchoutamt.merchoutamtDetailInsert", merchoutamtFormBean);
        
    }

    //미수금 상계 후 미수잔액 수정
    @Override
    public void merchoutamtMasterUpdate_minusoutamt(MerchoutamtFormBean merchoutamtFormBean) {
        
        getSqlSession().update("Merchoutamt.merchoutamtMasterUpdate_minusoutamt", merchoutamtFormBean);
        
    }    
    
    //상계 삭제
    @Override
    public void merchoutamtSetoffDelete(MerchoutamtFormBean merchoutamtFormBean) {
        
        getSqlSession().delete("Merchoutamt.merchoutamtSetoffDelete", merchoutamtFormBean);
        
    }

    //상계 삭제 후 미수잔액 수정
    @Override
    public void merchoutamtMasterUpdate_plusoutamt(MerchoutamtFormBean merchoutamtFormBean) {
        
        getSqlSession().update("Merchoutamt.merchoutamtMasterUpdate_plusoutamt", merchoutamtFormBean);
        
    }    

    //미수 내역 엑셀다운로드
    @Override
    public List merchoutamtMasterListExcel(MerchoutamtFormBean merchoutamtFormBean) {
        
        return (List)getSqlSession().selectList("Merchoutamt.merchoutamtMasterListExcel",merchoutamtFormBean);
        
    }
    
    //미수 내역 상세 엑셀다운로드
    @Override
    public List merchoutamtDetailListExcel(MerchoutamtFormBean merchoutamtFormBean) {
        
        return (List)getSqlSession().selectList("Merchoutamt.merchoutamtDetailListExcel",merchoutamtFormBean);
        
    }    

    //미수금 조회 카운트
    @Override
    public Integer merchoutamtMasterListCount(MerchoutamtFormBean merchoutamtFormBean) {
        
        return (Integer)getSqlSession().selectOne("Merchoutamt.merchoutamtMasterListCount",merchoutamtFormBean);
        
    }
    
    //미수금상세 조회 카운트
    @Override
    public Integer merchoutamtDetailListCount(MerchoutamtFormBean merchoutamtFormBean) {
        
        return (Integer)getSqlSession().selectOne("Merchoutamt.merchoutamtDetailListCount",merchoutamtFormBean);
        
    }    
    
}
