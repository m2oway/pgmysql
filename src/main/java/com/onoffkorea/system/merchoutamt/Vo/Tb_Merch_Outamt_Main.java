/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.merchoutamt.Vo;

/**
 *
 * @author Administrator
 */
public class Tb_Merch_Outamt_Main {
    
    private String outamt_seq;  
    private String onfftid   ;  
    private String gen_reson ;  
    private String gen_dt    ;  
    private String out_amt   ;  
    private String cur_outamt;  
    private String proc_mtd  ;  
    private String memo      ;  
    private String ins_dt    ;  
    private String mod_dt    ;  
    private String ins_user  ;  
    private String mod_user  ;  
    private String outamt_stat;
    private String rnum;
    private String merch_nm;
    private String pay_chn_cate_nm;
    private String pay_chn_cate;
    private String ins_reson_nm;
    private String outamt_stat_nm;
    private String decision_flag;
    private String onffmerch_no;
    private String onfftid_nm;
    private String exp_pay_dt;
    private String pay_sche_seq;
    private String setoff_seq;
    private String setoff_amt;

    public String getSetoff_amt() {
        return setoff_amt;
    }

    public void setSetoff_amt(String setoff_amt) {
        this.setoff_amt = setoff_amt;
    }

    public String getSetoff_seq() {
        return setoff_seq;
    }

    public void setSetoff_seq(String setoff_seq) {
        this.setoff_seq = setoff_seq;
    }

    public String getPay_sche_seq() {
        return pay_sche_seq;
    }

    public void setPay_sche_seq(String pay_sche_seq) {
        this.pay_sche_seq = pay_sche_seq;
    }

    public String getExp_pay_dt() {
        return exp_pay_dt;
    }

    public void setExp_pay_dt(String exp_pay_dt) {
        this.exp_pay_dt = exp_pay_dt;
    }

    public String getOnfftid_nm() {
        return onfftid_nm;
    }

    public void setOnfftid_nm(String onfftid_nm) {
        this.onfftid_nm = onfftid_nm;
    }

    public String getOnffmerch_no() {
        return onffmerch_no;
    }

    public void setOnffmerch_no(String onffmerch_no) {
        this.onffmerch_no = onffmerch_no;
    }

    public String getDecision_flag() {
        return decision_flag;
    }

    public void setDecision_flag(String decision_flag) {
        this.decision_flag = decision_flag;
    }

    public String getIns_reson_nm() {
        return ins_reson_nm;
    }

    public void setIns_reson_nm(String ins_reson_nm) {
        this.ins_reson_nm = ins_reson_nm;
    }

    public String getOutamt_stat_nm() {
        return outamt_stat_nm;
    }

    public void setOutamt_stat_nm(String outamt_stat_nm) {
        this.outamt_stat_nm = outamt_stat_nm;
    }

    public String getPay_chn_cate() {
        return pay_chn_cate;
    }

    public void setPay_chn_cate(String pay_chn_cate) {
        this.pay_chn_cate = pay_chn_cate;
    }

    public String getPay_chn_cate_nm() {
        return pay_chn_cate_nm;
    }

    public void setPay_chn_cate_nm(String pay_chn_cate_nm) {
        this.pay_chn_cate_nm = pay_chn_cate_nm;
    }    

    public String getMerch_nm() {
        return merch_nm;
    }

    public void setMerch_nm(String merch_nm) {
        this.merch_nm = merch_nm;
    }

    public String getOutamt_seq() {
        return outamt_seq;
    }

    public void setOutamt_seq(String outamt_seq) {
        this.outamt_seq = outamt_seq;
    }

    public String getOnfftid() {
        return onfftid;
    }

    public void setOnfftid(String onfftid) {
        this.onfftid = onfftid;
    }

    public String getGen_reson() {
        return gen_reson;
    }

    public void setGen_reson(String gen_reson) {
        this.gen_reson = gen_reson;
    }

    public String getGen_dt() {
        return gen_dt;
    }

    public void setGen_dt(String gen_dt) {
        this.gen_dt = gen_dt;
    }

    public String getOut_amt() {
        return out_amt;
    }

    public void setOut_amt(String out_amt) {
        this.out_amt = out_amt;
    }

    public String getCur_outamt() {
        return cur_outamt;
    }

    public void setCur_outamt(String cur_outamt) {
        this.cur_outamt = cur_outamt;
    }

    public String getProc_mtd() {
        return proc_mtd;
    }

    public void setProc_mtd(String proc_mtd) {
        this.proc_mtd = proc_mtd;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    public String getOutamt_stat() {
        return outamt_stat;
    }

    public void setOutamt_stat(String outamt_stat) {
        this.outamt_stat = outamt_stat;
    }

    public String getRnum() {
        return rnum;
    }

    public void setRnum(String rnum) {
        this.rnum = rnum;
    }

}
