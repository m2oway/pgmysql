/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.merchoutamt.Vo;

/**
 *
 * @author Administrator
 */
public class Tb_Merch_Setoffinfo {
    
    private String setoff_seq   ;
    private String outamt_seq   ;
    private String setoff_cate  ;
    private String setoff_amt   ;
    private String setoff_date  ;
    private String memo         ;
    private String ins_dt       ;
    private String mod_dt       ;
    private String ins_user     ;
    private String mod_user     ;
    private String user_seq;
    private String rnum;
    private String setoff_cate_nm;
    private String pay_sche_seq;

    public String getPay_sche_seq() {
        return pay_sche_seq;
    }

    public void setPay_sche_seq(String pay_sche_seq) {
        this.pay_sche_seq = pay_sche_seq;
    }

    public String getSetoff_cate_nm() {
        return setoff_cate_nm;
    }

    public void setSetoff_cate_nm(String setoff_cate_nm) {
        this.setoff_cate_nm = setoff_cate_nm;
    }

    public String getRnum() {
        return rnum;
    }

    public void setRnum(String rnum) {
        this.rnum = rnum;
    }

    public String getUser_seq() {
        return user_seq;
    }

    public void setUser_seq(String user_seq) {
        this.user_seq = user_seq;
    }

    public String getSetoff_seq() {
        return setoff_seq;
    }

    public void setSetoff_seq(String setoff_seq) {
        this.setoff_seq = setoff_seq;
    }

    public String getOutamt_seq() {
        return outamt_seq;
    }

    public void setOutamt_seq(String outamt_seq) {
        this.outamt_seq = outamt_seq;
    }

    public String getSetoff_cate() {
        return setoff_cate;
    }

    public void setSetoff_cate(String setoff_cate) {
        this.setoff_cate = setoff_cate;
    }

    public String getSetoff_amt() {
        return setoff_amt;
    }

    public void setSetoff_amt(String setoff_amt) {
        this.setoff_amt = setoff_amt;
    }

    public String getSetoff_date() {
        return setoff_date;
    }

    public void setSetoff_date(String setoff_date) {
        this.setoff_date = setoff_date;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }
    
}
