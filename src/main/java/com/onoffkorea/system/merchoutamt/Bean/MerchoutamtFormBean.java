/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.merchoutamt.Bean;

import com.onoffkorea.system.common.util.CommonSortableListPagingForm;

/**
 *
 * @author Administrator
 */
public class MerchoutamtFormBean extends CommonSortableListPagingForm{
    
    private String outamt_seq;  
    private String onfftid   ;  
    private String gen_reson ;  
    private String gen_dt    ;  
    private String out_amt   ;  
    private String cur_outamt;  
    private String proc_mtd  ;  
    private String memo      ;  
    private String ins_dt    ;  
    private String mod_dt    ;  
    private String ins_user  ;  
    private String mod_user  ;  
    private String outamt_stat;
    private String rnum;
    private String merch_nm;
    private String pay_chn_cate_nm;
    private String pay_chn_cate;
    private String gen_start_dt;
    private String gen_end_dt;
    private String user_seq;
    private String setoff_seq   ;
    private String setoff_cate  ;
    private String setoff_amt   ;
    private String setoff_date  ;
    private String setoff_cate_nm;
    private String[] setoff_seqs  ;
    private String merchpay_popup_yn;
    private String onffmerch_no;
    private String exp_pay_dt;
    private String merchoutamt_popup_yn;
    private String pay_sche_seq;

    public String getPay_sche_seq() {
        return pay_sche_seq;
    }

    public void setPay_sche_seq(String pay_sche_seq) {
        this.pay_sche_seq = pay_sche_seq;
    }

    public String getMerchoutamt_popup_yn() {
        return merchoutamt_popup_yn;
    }

    public void setMerchoutamt_popup_yn(String merchoutamt_popup_yn) {
        this.merchoutamt_popup_yn = merchoutamt_popup_yn;
    }

    public String getExp_pay_dt() {
        return exp_pay_dt;
    }

    public void setExp_pay_dt(String exp_pay_dt) {
        this.exp_pay_dt = exp_pay_dt;
    }

    public String getOnffmerch_no() {
        return onffmerch_no;
    }

    public void setOnffmerch_no(String onffmerch_no) {
        this.onffmerch_no = onffmerch_no;
    }

    public String getMerchpay_popup_yn() {
        return merchpay_popup_yn;
    }

    public void setMerchpay_popup_yn(String merchpay_popup_yn) {
        this.merchpay_popup_yn = merchpay_popup_yn;
    }

    public String[] getSetoff_seqs() {
        return setoff_seqs;
    }

    public void setSetoff_seqs(String[] setoff_seqs) {
        this.setoff_seqs = setoff_seqs;
    }

    public String getSetoff_seq() {
        return setoff_seq;
    }

    public void setSetoff_seq(String setoff_seq) {
        this.setoff_seq = setoff_seq;
    }

    public String getSetoff_cate() {
        return setoff_cate;
    }

    public void setSetoff_cate(String setoff_cate) {
        this.setoff_cate = setoff_cate;
    }

    public String getSetoff_amt() {
        return setoff_amt;
    }

    public void setSetoff_amt(String setoff_amt) {
        this.setoff_amt = setoff_amt;
    }

    public String getSetoff_date() {
        return setoff_date;
    }

    public void setSetoff_date(String setoff_date) {
        this.setoff_date = setoff_date;
    }

    public String getSetoff_cate_nm() {
        return setoff_cate_nm;
    }

    public void setSetoff_cate_nm(String setoff_cate_nm) {
        this.setoff_cate_nm = setoff_cate_nm;
    }

    public String getUser_seq() {
        return user_seq;
    }

    public void setUser_seq(String user_seq) {
        this.user_seq = user_seq;
    }

    public String getMerch_nm() {
        return merch_nm;
    }

    public void setMerch_nm(String merch_nm) {
        this.merch_nm = merch_nm;
    }

    public String getGen_start_dt() {
        return gen_start_dt;
    }

    public void setGen_start_dt(String gen_start_dt) {
        this.gen_start_dt = gen_start_dt;
    }

    public String getGen_end_dt() {
        return gen_end_dt;
    }

    public void setGen_end_dt(String gen_end_dt) {
        this.gen_end_dt = gen_end_dt;
    }

    public String getPay_chn_cate() {
        return pay_chn_cate;
    }

    public void setPay_chn_cate(String pay_chn_cate) {
        this.pay_chn_cate = pay_chn_cate;
    }    

    public String getPay_chn_cate_nm() {
        return pay_chn_cate_nm;
    }

    public void setPay_chn_cate_nm(String pay_chn_cate_nm) {
        this.pay_chn_cate_nm = pay_chn_cate_nm;
    }

    public String getOutamt_seq() {
        return outamt_seq;
    }

    public void setOutamt_seq(String outamt_seq) {
        this.outamt_seq = outamt_seq;
    }

    public String getOnfftid() {
        return onfftid;
    }

    public void setOnfftid(String onfftid) {
        this.onfftid = onfftid;
    }

    public String getGen_reson() {
        return gen_reson;
    }

    public void setGen_reson(String gen_reson) {
        this.gen_reson = gen_reson;
    }

    public String getGen_dt() {
        return gen_dt;
    }

    public void setGen_dt(String gen_dt) {
        this.gen_dt = gen_dt;
    }

    public String getOut_amt() {
        return out_amt;
    }

    public void setOut_amt(String out_amt) {
        this.out_amt = out_amt;
    }

    public String getCur_outamt() {
        return cur_outamt;
    }

    public void setCur_outamt(String cur_outamt) {
        this.cur_outamt = cur_outamt;
    }

    public String getProc_mtd() {
        return proc_mtd;
    }

    public void setProc_mtd(String proc_mtd) {
        this.proc_mtd = proc_mtd;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    public String getOutamt_stat() {
        return outamt_stat;
    }

    public void setOutamt_stat(String outamt_stat) {
        this.outamt_stat = outamt_stat;
    }

    public String getRnum() {
        return rnum;
    }

    public void setRnum(String rnum) {
        this.rnum = rnum;
    }

    @Override
    public String toString() {
        return "MerchoutamtFormBean{" + "outamt_seq=" + outamt_seq + ", onfftid=" + onfftid + ", gen_reson=" + gen_reson + ", gen_dt=" + gen_dt + ", out_amt=" + out_amt + ", cur_outamt=" + cur_outamt + ", proc_mtd=" + proc_mtd + ", memo=" + memo + ", ins_dt=" + ins_dt + ", mod_dt=" + mod_dt + ", ins_user=" + ins_user + ", mod_user=" + mod_user + ", outamt_stat=" + outamt_stat + ", rnum=" + rnum + ", merch_nm=" + merch_nm + ", pay_chn_cate_nm=" + pay_chn_cate_nm + ", pay_chn_cate=" + pay_chn_cate + ", gen_start_dt=" + gen_start_dt + ", gen_end_dt=" + gen_end_dt + ", user_seq=" + user_seq + '}';
    }

}