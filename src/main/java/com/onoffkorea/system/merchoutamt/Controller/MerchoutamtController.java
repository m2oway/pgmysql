/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.merchoutamt.Controller;

import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.merchant.Bean.MerchantFormBean;
import com.onoffkorea.system.merchoutamt.Bean.MerchoutamtFormBean;
import com.onoffkorea.system.merchoutamt.Service.MerchoutamtService;
import com.onoffkorea.system.merchoutamt.Vo.Tb_Merch_Outamt_Main;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import com.onoffkorea.system.popup.Service.PopupService;
import common.web.servlet.BaseSpringMultiActionController;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/merchoutamt/**")
public class MerchoutamtController extends BaseSpringMultiActionController{
    
    protected Log logger = LogFactory.getLog(getClass());
    
    @Autowired
    private MerchoutamtService merchoutamtService;
    
    @Autowired
    private CommonService commonService;
    
    @Autowired
    private PopupService popupService;    
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }       
    
    //가맹점 미수금 정보 조회
    @RequestMapping(method= RequestMethod.GET,value = "/merchoutamtMasterList")
    public String merchoutamtMasterFormList(@ModelAttribute MerchoutamtFormBean merchoutamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        
        Hashtable ht_merchoutamtMasterList = merchoutamtService.merchoutamtMasterList(merchoutamtFormBean);
        String ls_merchoutamtMasterList = (String) ht_merchoutamtMasterList.get("ResultSet");
        
        model.addAttribute("merchoutamtMasterList",ls_merchoutamtMasterList);     

        return null;
        
    }
    
    //가맹점 미수금 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/merchoutamtMasterList")
    @ResponseBody
    public String merchoutamtMasterList(@ModelAttribute MerchoutamtFormBean merchoutamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {    
        
        Hashtable ht_merchoutamtMasterList = merchoutamtService.merchoutamtMasterList(merchoutamtFormBean);
        String ls_merchoutamtMasterList = (String) ht_merchoutamtMasterList.get("ResultSet");
        
        return ls_merchoutamtMasterList;
        
    }
    
    //가맹점 미수금 정보 상세조회
    @RequestMapping(method= RequestMethod.GET,value = "/merchoutamtDetailList")
    public String merchoutamtDetailFormList(@ModelAttribute MerchoutamtFormBean merchoutamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        
        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        
        /*
        if((merchoutamtFormBean.getGen_start_dt() == null) || (merchoutamtFormBean.getGen_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            merchoutamtFormBean.setGen_start_dt(start_dt);
            merchoutamtFormBean.setGen_end_dt(end_dt);
        } 
        */
        
        if(merchoutamtFormBean.getOutamt_stat() == null || merchoutamtFormBean.getOutamt_stat().equals(""))
        {
            merchoutamtFormBean.setOutamt_stat("0");
        }
        
        Hashtable ht_merchoutamtDetailList = merchoutamtService.merchoutamtDetailList(merchoutamtFormBean);
        String ls_merchoutamtDetailList = (String) ht_merchoutamtDetailList.get("ResultSet");
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_merchoutamtDetailList.get("payChnCateList");
        
        Tb_Code_Main outamtStatList = (Tb_Code_Main) ht_merchoutamtDetailList.get("outamtStatList");
        
        model.addAttribute("merchoutamtDetailList",ls_merchoutamtDetailList);     
        model.addAttribute("payChnCateList",payChnCateList);      
        model.addAttribute("outamtStatList",outamtStatList);  
        
        /*
        SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
        Date start_date = formatter.parse(merchoutamtFormBean.getGen_start_dt());
        Date end_date   = formatter.parse(merchoutamtFormBean.getGen_end_dt());
        
        String start_dt = formatter2.format(start_date);
        String end_dt = formatter2.format(end_date);

        merchoutamtFormBean.setGen_start_dt(start_dt);
        merchoutamtFormBean.setGen_end_dt(end_dt);        
        */
        merchoutamtFormBean.setOutamt_stat("0");
        
        return null;
        
    }
    
    //가맹점 미수금 정보 상세조회
    @RequestMapping(method= RequestMethod.POST,value = "/merchoutamtDetailList")
    @ResponseBody
    public String merchoutamtDetailList(@ModelAttribute MerchoutamtFormBean merchoutamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {    
        
        Hashtable ht_merchoutamtDetailList = merchoutamtService.merchoutamtDetailList(merchoutamtFormBean);
        String ls_merchoutamtDetailList = (String) ht_merchoutamtDetailList.get("ResultSet");
        
        return ls_merchoutamtDetailList;
        
    }    
    
    //가맹점 미수금 등록
    @RequestMapping(method= RequestMethod.GET,value = "/merchoutamtMasterInsert")
    public String merchoutamtMasterInsertForm(@Valid MerchoutamtFormBean merchoutamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        
        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("INS_RESON");
        Tb_Code_Main insResonList = commonService.codeSearch(commonCodeSearchFormBean);
        
        model.addAttribute("insResonList",insResonList);
        
        return null;
        
    }       
    
    //미수금 등록
    @RequestMapping(method= RequestMethod.POST,value = "/merchoutamtMasterInsert")
    public @ResponseBody String merchoutamtMasterInsert(@Valid MerchoutamtFormBean merchoutamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        merchoutamtFormBean.setUser_seq(siteSession.getUser_seq());
        String outamt_seq = merchoutamtService.merchoutamtMasterInsert(merchoutamtFormBean);
        
        return outamt_seq;
        
    }         
    
    //미수금 상계
    @RequestMapping(method= RequestMethod.GET,value = "/merchoutamtMasterUpdate")
    public String merchoutamtMasterUpdateForm(@Valid MerchoutamtFormBean merchoutamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_merchoutamtDetailList = merchoutamtService.merchoutamtDetailList(merchoutamtFormBean);
        List<Tb_Merch_Outamt_Main> ls_merchoutamtDetailList = (List<Tb_Merch_Outamt_Main>) ht_merchoutamtDetailList.get("ls_merchoutamtDetailList");
        
        Hashtable ht_merchsetoffinfoList = merchoutamtService.merchsetoffinfoList(merchoutamtFormBean);
        String ls_merchsetoffinfoList = (String) ht_merchsetoffinfoList.get("ResultSet");
        
        Tb_Code_Main setoffCateList = (Tb_Code_Main) ht_merchsetoffinfoList.get("setoffCateList");
        
        Tb_Code_Main insResonList = (Tb_Code_Main) ht_merchsetoffinfoList.get("insResonList");
        
        model.addAttribute("ls_merchoutamtDetailList",ls_merchoutamtDetailList);
        model.addAttribute("ls_merchsetoffinfoList",ls_merchsetoffinfoList);
        model.addAttribute("setoffCateList",setoffCateList);
        model.addAttribute("insResonList",insResonList);
        
        return null;
        
    }           
    
    //미수금 수정
    @RequestMapping(method= RequestMethod.POST,value = "/merchoutamtMasterUpdate")
    public @ResponseBody String merchoutamtMasterUpdate(@Valid MerchoutamtFormBean merchoutamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        merchoutamtFormBean.setUser_seq(siteSession.getUser_seq());
        merchoutamtService.merchoutamtMasterUpdate(merchoutamtFormBean);
        
        return null;
        
    }                  
    
    //미수금 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/merchoutamtMasterDelete")
    public @ResponseBody String merchoutamtMasterDelete(@Valid MerchoutamtFormBean merchoutamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        merchoutamtFormBean.setUser_seq(siteSession.getUser_seq());
        merchoutamtService.merchoutamtMasterDelete(merchoutamtFormBean);
        
        return null;
        
    }            
    
    //미수금 상계 등록
    @RequestMapping(method= RequestMethod.POST,value = "/merchoutamtDetailInsert")
    public @ResponseBody String merchoutamtDetailInsert(@Valid MerchoutamtFormBean merchoutamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        merchoutamtFormBean.setUser_seq(siteSession.getUser_seq());
        merchoutamtService.merchoutamtDetailInsert(merchoutamtFormBean);
        
        return null;
        
    }              
    
    //상계 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/merchoutamtSetoffDelete")
    public @ResponseBody String merchoutamtSetoffDelete(@Valid MerchoutamtFormBean merchoutamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        merchoutamtFormBean.setUser_seq(siteSession.getUser_seq());        
        merchoutamtService.merchoutamtSetoffDelete(merchoutamtFormBean);
        
        return null;
        
    }             
    
    //미수 내역 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/merchoutamtMasterListExcel")
    public void merchoutamtMasterListExcel(@Valid MerchoutamtFormBean merchoutamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("merchoutamt.merchoutamtMasterListExcel.fileName"));

        merchoutamtService.merchoutamtMasterListExcel(merchoutamtFormBean, excelDownloadFilename, response);  
        
    }               
    
    //미수 내역 상세 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/merchoutamtDetailListExcel")
    public void merchoutamtDetailListExcel(@Valid MerchoutamtFormBean merchoutamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("merchoutamt.merchoutamtDetailListExcel.fileName"));

        merchoutamtService.merchoutamtDetailListExcel(merchoutamtFormBean, excelDownloadFilename, response);  
        
    }             
    
}
