/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.auth.Controller;

import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.auth.Bean.AuthFormBean;
import com.onoffkorea.system.auth.Service.AuthService;
import com.onoffkorea.system.auth.Vo.Tb_Sys_Auth;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/auth/**")
public class AuthController {
        
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private AuthService authService;
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
         model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }    
    
    //레이아웃
    @RequestMapping(method= RequestMethod.GET,value = "/authLayout")
    public String authLayout(@Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        return null;
        
    }
    // 코드 정보 조회
     @RequestMapping(method= RequestMethod.GET,value = "/authMasterList")
    public String authMasterList(@Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
 
        
        Hashtable ht_authMaster = authService.authMasterList(authFormBean);
        List ls_authMaster = (List) ht_authMaster.get("ResultSet");
        StringBuilder cc = new StringBuilder();
        cc.append("{\"rows\" : [");
        for (int i = 0; i < ls_authMaster.size(); i++) {
                Tb_Sys_Auth Tb_sys_Auth = (Tb_Sys_Auth) ls_authMaster.get(i);
                cc.append("{\"id\":" + i);
                cc.append(",\"userdata\":{");
                cc.append("\"auth_seq\":\""+Util.nullToString(Tb_sys_Auth.getAuth_seq())+"\"");
//                cc.append(" ,\"use_flag\":\""+Util.nullToString(tb_Main_Auth.getUse_flag())+"\"");
                cc.append("}");
                cc.append(",\"data\":[");
                cc.append(" \"" + Util.nullToString(Tb_sys_Auth.getRnum()) + "\"");
                cc.append(",\"" + Util.nullToString(Tb_sys_Auth.getAuth_name()) + "\"");
                cc.append(",\"" + Util.nullToString(Tb_sys_Auth.getIns_dt()) + "\"");
                cc.append(",\"" + Util.nullToString(Tb_sys_Auth.getIns_user()) + "\"");
                cc.append(",\"" + Util.nullToString(Tb_sys_Auth.getUse_flag()) + "\"");

                if (i == (ls_authMaster.size() - 1)) {
                        cc.append("]}");
                } else {
                        cc.append("]},");
                }
        }

        cc.append("]}");    
        model.addAttribute("authMasterList",cc.toString());
        return null;
        


        
    }
     //상세코드 정보 조회
     @RequestMapping(method= RequestMethod.GET,value = "/authDetailMasterList")
    public String authDetailMasterList(@Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

      StringBuilder cc = new StringBuilder();
        cc.append("{rows : []}");
        cc.append("]}");    
        
        model.addAttribute("authDetailMasterList",cc.toString());
        return null;
        
    } 
    
    //코드 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/authMasterList")
    public @ResponseBody String authMasterList(@Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
        
         Hashtable ht_authMaster = authService.authMasterList(authFormBean);
        List ls_authMaster = (List) ht_authMaster.get("ResultSet");
        StringBuilder cc = new StringBuilder();
        cc.append("{\"rows\" : [");
        for (int i = 0; i < ls_authMaster.size(); i++) {
                Tb_Sys_Auth Tb_sys_Auth = (Tb_Sys_Auth) ls_authMaster.get(i);
                cc.append("{\"id\":" + i);
                cc.append(",\"userdata\":{");
                cc.append("\"auth_seq\":\""+Util.nullToString(Tb_sys_Auth.getAuth_seq())+"\"");
//                cc.append(" ,\"use_flag\":\""+Util.nullToString(tb_Main_Auth.getUse_flag())+"\"");
                cc.append("}");
                cc.append(",\"data\":[");
                cc.append(" \"" + Util.nullToString(Tb_sys_Auth.getRnum()) + "\"");
                cc.append(",\"" + Util.nullToString(Tb_sys_Auth.getAuth_name()) + "\"");
                cc.append(",\"" + Util.nullToString(Tb_sys_Auth.getIns_dt()) + "\"");
                 cc.append(",\"" + Util.nullToString(Tb_sys_Auth.getIns_user()) + "\"");
                cc.append(",\"" + Util.nullToString(Tb_sys_Auth.getUse_flag()) + "\"");

                if (i == (ls_authMaster.size() - 1)) {
                        cc.append("]}");
                } else {
                        cc.append("]},");
                }
        }

        cc.append("]}");     
     System.out.println(cc);
        return cc.toString();
       
    }
    
    //상세코드 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/authDetailMasterList")
    public @ResponseBody String authDetailMasterList(@Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
       
         request.getParameter("auth_seq");
         
        Hashtable ht_authDetailMasterList = authService.authDetailMasterList(authFormBean);
        List ls_authDetailMasterList = (List) ht_authDetailMasterList.get("ResultSet");
        StringBuilder cc = new StringBuilder();
        cc.append("{\"rows\" : [");
        for (int i = 0; i < ls_authDetailMasterList.size(); i++) {
                Tb_Sys_Auth Tb_sys_Auth = (Tb_Sys_Auth) ls_authDetailMasterList.get(i);
                cc.append("{\"id\":" + i);
                cc.append(",\"userdata\":{");
                cc.append("\"auth_seq\":\""+Util.nullToString(Tb_sys_Auth.getAuth_seq())+"\"");
                cc.append(" ,\"program_seq\":\""+Util.nullToString(Tb_sys_Auth.getProgram_seq())+"\"");
                cc.append("}");
                cc.append(",\"data\":[");
                cc.append(" \"" + Util.nullToString(Tb_sys_Auth.getRnum()) + "\"");
                cc.append(",\"" + Util.nullToString(Tb_sys_Auth.getProgram_seq()) + "\"");
                cc.append(",\"" + Util.nullToString(Tb_sys_Auth.getMenu_name()) + "\"");
                cc.append(",\"" + Util.nullToString(Tb_sys_Auth.getEnable_read()) + "\"");
                cc.append(",\"" + Util.nullToString(Tb_sys_Auth.getEnable_create()) + "\"");
                cc.append(",\"" + Util.nullToString(Tb_sys_Auth.getEnable_update()) + "\"");
                cc.append(",\"" + Util.nullToString(Tb_sys_Auth.getEnable_delete()) + "\"");
               

                if (i == (ls_authDetailMasterList.size() - 1)) {
                        cc.append("]}");
                } else {
                        cc.append("]},");
                }
        }

        
        cc.append("]}");

     System.out.println(cc);
     
        return cc.toString();
       
    }
    
     
    
    //코드 정보 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/authMasterDelete")
    public @ResponseBody String authMasterDelete(@Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("siteSessionObj") SiteSession  dentalHopitalSession) throws Exception {
             
        authFormBean.setUser_seq(dentalHopitalSession.getUser_seq());
        authService.authMasterDelete(authFormBean);
        
        return null;
        
    }
    
       //코드 정보 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/authDetailMasterDelete")
    public @ResponseBody String authDetailMasterDelete(@Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, @ModelAttribute("siteSessionObj") SiteSession  dentalHopitalSession) throws Exception {
        
        authFormBean.setUser_seq(dentalHopitalSession.getUser_seq());
        authService.authDetailMasterDelete(authFormBean);
        
        return null;
        
    }
    
    
    //코드 정보 등록
    @RequestMapping(method= RequestMethod.GET,value = "/authMasterInsert")
    public String authMasterInsertForm(@Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        
        return null;
        
    }   
    
    //코드 정보 등록
    @RequestMapping(method= RequestMethod.POST,value = "/authMasterInsert")
    public @ResponseBody String authMasterInsert(@Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession  dentalHopitalSession) throws Exception {

        authFormBean.setUser_seq(dentalHopitalSession.getUser_seq());
         authService.authMasterInsert(authFormBean);
         String auth_seq = authFormBean.getAuth_seq();
        
        return auth_seq;
        
    }  
//    
    //상세코드 정보 등록
    @RequestMapping(method= RequestMethod.GET,value = "/authDetailMasterInsert")
    public String authDetailMasterInsert(@Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
    
//                logger.debug("authFormBean ======?????***********************======???=" +authFormBean.getMain_auth());
        String auth_seq = Util.nullToString(authFormBean.getAuth_seq());
//        model.addAttribute("main_auth",authFormBean.getMain_auth());
        
        authFormBean.setAuth_seq("");
        
        Hashtable ht_authMaster = authService.authMasterList(authFormBean);
        List ls_authMaster = (List) ht_authMaster.get("ResultSet");
        
        model.addAttribute("ls_authMaster",ls_authMaster);
        model.addAttribute("auth_seq",auth_seq);
        return null;
        
    }   
    
    //상세코드 정보 등록
    @RequestMapping(method= RequestMethod.POST,value = "/authDetailMasterInsert")
    public @ResponseBody String authDetailMasterInsert(@Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession  dentalHopitalSession) throws Exception {
        authFormBean.setUser_seq(dentalHopitalSession.getUser_seq());
        
        String detail_auth = authService.authDetailMasterInsert(authFormBean);
        
//        return authFormBean.getDetail_auth();
        return null;
         
        
    }     
    
    //코드 정보 수정 
    @RequestMapping(method= RequestMethod.GET,value = "/authMasterUpdate")
    public String authMasterUpdateForm(@Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        
        Hashtable ht_authMaster = authService.authMasterList(authFormBean);
        List ls_authMaster = (List) ht_authMaster.get("ResultSet");
        Tb_Sys_Auth tb_Auth = (Tb_Sys_Auth) ls_authMaster.get(0);
        
        StringBuilder cc = new StringBuilder();
          cc.append("{rows : []}");
          
        model.addAttribute("authMasterList",cc.toString());
        model.addAttribute("tb_Auth",tb_Auth);
        
        return null;
        
    }       
    
    //코드 정보 수정 
    @RequestMapping(method= RequestMethod.POST,value = "/authMasterUpdate")
    public String authMasterUpdate(@Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession  dentalHopitalSession) throws Exception {

        authFormBean.setUser_seq(dentalHopitalSession.getUser_seq());
        authService.authMasterUpdate(authFormBean);
        
        Hashtable ht_authMaster = authService.authMasterList(authFormBean);
        List ls_authMaster = (List) ht_authMaster.get("ResultSet");
        Tb_Sys_Auth tb_Auth = (Tb_Sys_Auth) ls_authMaster.get(0);    
        
        model.addAttribute("tb_Auth",tb_Auth);
        
        return null;
        
    } 
    
    //상세코드 정보 수정 
    @RequestMapping(method= RequestMethod.GET,value = "/authDetailMasterUpdate")
    public String authDetailMasterUpdate(@Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        
        Hashtable ht_authDetailMaster = authService.authDetailMasterList(authFormBean);
        List ls_authDetailMaster = (List) ht_authDetailMaster.get("ResultSet");
        
        Hashtable ht_authMaster = authService.authMasterList(authFormBean);
        List ls_authMaster = (List) ht_authMaster.get("ResultSet");        
     
        Tb_Sys_Auth tb_Auth = (Tb_Sys_Auth) ls_authDetailMaster.get(0);
        String auth_seq = Util.nullToString(authFormBean.getAuth_seq());
   
        model.addAttribute("ls_authMaster",ls_authMaster);
         model.addAttribute("ls_authDetailMaster",ls_authDetailMaster);
        model.addAttribute("auth_seq",auth_seq);
        model.addAttribute("tb_Auth",tb_Auth);
        return null;
    }       
    
    //상세코드 정보 수정 
    @RequestMapping(method= RequestMethod.POST,value = "/authDetailMasterUpdate")
    public String authDetailMasterUpdate(@Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession  dentalHopitalSession) throws Exception {

        authFormBean.setUser_seq(dentalHopitalSession.getUser_seq());
        authService.authDetailMasterUpdate(authFormBean);
        
        Hashtable ht_authDetailMaster = authService.authDetailMasterList(authFormBean);
        List ls_authDetailMaster = (List) ht_authDetailMaster.get("ResultSet");
        Tb_Sys_Auth tb_Auth = (Tb_Sys_Auth) ls_authDetailMaster.get(0);    
        
        model.addAttribute("tb_Auth",tb_Auth);
        
        return null;
        
    } 
    
   //상세코드 정보 수정 
//    @RequestMapping(method= RequestMethod.POST,value = "/authDetailMasterDelete")
//    public String authDetailMasterDelete(@Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
//            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("dentalHopitalSession") DentalHopitalSession dentalHopitalSession) throws Exception {
//
//        authFormBean.setUser_seq(dentalHopitalSession.getUser_seq());
//        authService.authDetailMasterUpdate(authFormBean);
//        
//        Hashtable ht_authDetailMaster = authService.authDetailMasterList(authFormBean);
//        List ls_authDetailMaster = (List) ht_authDetailMaster.get("ResultSet");
//        Tb_Sys_Auth tb_Auth = (Tb_Sys_Auth) ls_authDetailMaster.get(0);    
//        
//        model.addAttribute("tb_Auth",tb_Auth);
//        
//        return null;
//        
//    }
}
