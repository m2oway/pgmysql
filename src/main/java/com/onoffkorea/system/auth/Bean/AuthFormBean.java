/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.auth.Bean;

import com.onoffkorea.system.common.util.CommonSortableListPagingForm;

/**
 *
 * @author Administrator
 */
public class AuthFormBean extends CommonSortableListPagingForm {
     private String rnum   ;  
     private String user_seq;
     private String auth_seq;
     private String auth_name;
     private String auth_depth;    
     private String ins_user;
     private String ins_dt;
     private String mod_user;
     private String mod_dt;
     private String use_flag;
    private String program_seq;
    private String menu_name;
    private String enable_create;
    private String enable_update;             
    private String enable_delete;
    private String enable_read;

    public String getProgram_seq() {
        return program_seq;
    }

    public void setProgram_seq(String program_seq) {
        this.program_seq = program_seq;
    }

    public String getMenu_name() {
        return menu_name;
    }

    public void setMenu_name(String menu_name) {
        this.menu_name = menu_name;
    }

    public String getEnable_create() {
        return enable_create;
    }

    public void setEnable_create(String enable_create) {
        this.enable_create = enable_create;
    }

    public String getEnable_update() {
        return enable_update;
    }

    public void setEnable_update(String enable_update) {
        this.enable_update = enable_update;
    }

    public String getEnable_delete() {
        return enable_delete;
    }

    public void setEnable_delete(String enable_delete) {
        this.enable_delete = enable_delete;
    }

    public String getEnable_read() {
        return enable_read;
    }

    public void setEnable_read(String enable_read) {
        this.enable_read = enable_read;
    }

    public String getUse_flag() {
        return use_flag;
    }

    public void setUse_flag(String use_flag) {
        this.use_flag = use_flag;
    }

    public String getAuth_depth() {
        return auth_depth;
    }

    public void setAuth_depth(String auth_depth) {
        this.auth_depth = auth_depth;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }
     

    public String getUser_seq() {
        return user_seq;
    }

    public void setUser_seq(String user_seq) {
        this.user_seq = user_seq;
    }
     
    public String getAuth_name() {
        return auth_name;
    }

    public void setAuth_name(String auth_name) {
        this.auth_name = auth_name;
    }

    public String getRnum() {
        return rnum;
    }

    public void setRnum(String rnum) {
        this.rnum = rnum;
    }

    public String getAuth_seq() {
        return auth_seq;
    }

    public void setAuth_seq(String auth_seq) {
        this.auth_seq = auth_seq;
    }
    
}
