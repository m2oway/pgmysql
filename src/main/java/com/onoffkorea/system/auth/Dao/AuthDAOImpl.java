/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.auth.Dao;

import com.onoffkorea.system.auth.Bean.AuthFormBean;
import com.onoffkorea.system.auth.Vo.Tb_Sys_Auth;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("authDAO")
public class AuthDAOImpl extends SqlSessionDaoSupport implements AuthDAO{
    
    protected Logger logger = Logger.getLogger(getClass().getName());
    
     public ArrayList<Tb_Sys_Auth> authSeqList()throws Exception{
        return (ArrayList)getSqlSession().selectList("Auth.authSeqList");
    }

    //코드 정보 관리
    @Override
    public List authMasterList(AuthFormBean authFormBean) {
        
        return (List) getSqlSession().selectList("Auth.authMasterList", authFormBean);
        
    }

    //상세코드 정보 관리
    @Override
    public List authDetailMasterList(AuthFormBean authFormBean) {
        
        return (List) getSqlSession().selectList("Auth.authDetailMasterList", authFormBean);
        
    }
    //코드 정보 삭제
    @Override
    public String authMasterDelete(AuthFormBean authFormBean) {

        Integer result =  getSqlSession().delete("Auth.authMasterDelete", authFormBean);
        return result.toString(); 
    }
    
        //코드 정보 삭제
    @Override
    public String authDetailMasterDelete(AuthFormBean authFormBean) {

          Integer result = getSqlSession().delete("Auth.authDetailMasterDelete", authFormBean);
           return result.toString();
        
    }

    //코드 정보 등록
    @Override
    public String authMasterInsert(AuthFormBean authFormBean) {

       Integer result = getSqlSession().insert("Auth.authMasterInsert", authFormBean);
           return result.toString();
        
    }
    
      //코드 정보 등록
    @Override
    public String authDetailMasterInsert(AuthFormBean authFormBean) {

        Integer result = getSqlSession().insert("Auth.authDetailMasterInsert", authFormBean);

        return result.toString();
        
    }

    //코드 정보 수정
    @Override
    public void authMasterUpdate(AuthFormBean authFormBean) {

        getSqlSession().update("Auth.authMasterUpdate", authFormBean);
        
    }
    
    //코드 정보 수정
    @Override
    public void authDetailMasterUpdate(AuthFormBean authFormBean) {

        getSqlSession().update("Auth.authDetailMasterUpdate", authFormBean);
        
    }
    
}
