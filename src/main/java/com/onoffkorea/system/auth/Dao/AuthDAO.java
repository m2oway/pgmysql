/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.auth.Dao;

import com.onoffkorea.system.auth.Bean.AuthFormBean;
import com.onoffkorea.system.auth.Vo.Tb_Sys_Auth;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public interface AuthDAO {
    
    public ArrayList<Tb_Sys_Auth> authSeqList()throws Exception;

    public Object authMasterList(AuthFormBean authFormBean);
    
    public Object authDetailMasterList(AuthFormBean authFormBean);

    public String authMasterDelete(AuthFormBean authFormBean);
    
    public String authDetailMasterDelete(AuthFormBean authFormBean);

    public String authDetailMasterInsert(AuthFormBean authFormBean);
    
    public String authMasterInsert(AuthFormBean authFormBean);

    public void authMasterUpdate(AuthFormBean authFormBean);
    
    public void authDetailMasterUpdate(AuthFormBean authFormBean);
    
    
}
