/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.auth.Service;

import com.onoffkorea.system.auth.Bean.AuthFormBean;
import com.onoffkorea.system.auth.Dao.AuthDAO;
import com.onoffkorea.system.auth.Vo.Tb_Sys_Auth;
import java.util.ArrayList;
import java.util.Hashtable;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service("authService")
public class AuthServiceImpl  implements AuthService{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private AuthDAO authDAO;
    
    @Override
    public ArrayList<Tb_Sys_Auth> authSeqList() throws Exception {

        return authDAO.authSeqList();

    }

    //코드 정보 관리
    @Override
    public Hashtable authMasterList(AuthFormBean authFormBean) {
        
        Hashtable ht = new Hashtable();

        try {
                ht.put("ResultSet", this.authDAO.authMasterList(authFormBean));
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;
    }
   //코드 정보 관리
    @Override
    public Hashtable authDetailMasterList(AuthFormBean authFormBean) {
        
        Hashtable ht = new Hashtable();

        try {
                ht.put("ResultSet", this.authDAO.authDetailMasterList(authFormBean));
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;
    }
    //코드 정보 삭제
    @Override
    @Transactional
    public void authMasterDelete(AuthFormBean authFormBean) {
        
        authDAO.authDetailMasterDelete(authFormBean);
        authDAO.authMasterDelete(authFormBean);
        
    }
    
    //상세코드 정보 삭제
    @Override
    @Transactional
    public String authDetailMasterDelete(AuthFormBean authFormBean) {
        
         return authDAO.authDetailMasterDelete(authFormBean);
        
    }

    //코드 정보 등록
    @Override
    public String authMasterInsert(AuthFormBean authFormBean) {
        
           return authDAO.authMasterInsert(authFormBean);
        
    }
      //코드 정보 등록
    @Override
    public String authDetailMasterInsert(AuthFormBean authFormBean) {
        
        return authDAO.authDetailMasterInsert(authFormBean);
        
    }

    //코드 정보 수정
    @Override
    public void authMasterUpdate(AuthFormBean authFormBean) {
        
        authDAO.authMasterUpdate(authFormBean);
        
    }

    //상세코드 정보 수정
    @Override
    public void authDetailMasterUpdate(AuthFormBean authFormBean) {
        
        authDAO.authDetailMasterUpdate(authFormBean);
        
    }
}
