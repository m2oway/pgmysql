/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.paydeposit.Service;

import com.onoffkorea.system.acq_result.Vo.Tb_Acq_Result;
import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.paydeposit.Bean.PaydepositFormBean;
import com.onoffkorea.system.paydeposit.Dao.PaydepositDAO;
import com.onoffkorea.system.paydeposit.Vo.Tb_Pay_Deposit;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service("paydepositService")
public class PaydepositServiceImpl implements PaydepositService{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private CommonService commonService;    
    
    @Autowired
    private PaydepositDAO paydepositDAO;       

    @Override
    public Hashtable paydepositMasterList(PaydepositFormBean paydepositFormBean) {

        Hashtable ht = new Hashtable();
        
        

        try {
            
                List<Tb_Pay_Deposit> ls_paydepositMasterList = paydepositDAO.paydepositMasterList(paydepositFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"rows\" : [");
                
                for (int i = 0; i < ls_paydepositMasterList.size(); i++) {
                        Tb_Pay_Deposit tb_Mid_Deposit = ls_paydepositMasterList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"onffmerch_no\":\""+Util.nullToString(tb_Mid_Deposit.getOnffmerch_no())+"\"");
                        sb.append(",\"merch_no\":\""+Util.nullToString(tb_Mid_Deposit.getMerch_no())+"\"");
                        sb.append(",\"pay_mtd\":\""+Util.nullToString(tb_Mid_Deposit.getPay_mtd())+"\"");
                        sb.append(",\"seq\":\""+Util.nullToString(tb_Mid_Deposit.getDeposit_seq())+"\"");
                        sb.append(",\"proc_flag\":\""+Util.nullToString(tb_Mid_Deposit.getProc_flag())+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Mid_Deposit.getGen_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getOnffmerch_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getPay_targetamt_chk()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getPay_amt_chk()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getCms_chk()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getVat_chk()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getWithhold_tax_chk()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getApp_amt_chk()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getCncl_chk()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getPay_targetamt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getPay_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getCommission()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getVat()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getWithhold_tax()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getApp_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getCncl_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getMer_pay_targetamt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getMer_pay_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getMer_commission()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getMer_vat()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getMer_withhold_tax()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getMer_app_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getMer_cncl_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getApp_start()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getApp_end()) + "\"");

                        if (i == (ls_paydepositMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
                commonCodeSearchFormBean.setMain_code("MID_PAY_MTD");
                Tb_Code_Main midPayMtdList = commonService.codeSearch(commonCodeSearchFormBean);
                
                commonCodeSearchFormBean.setMain_code("PROC_FLAG");
                Tb_Code_Main procFlagList = commonService.codeSearch(commonCodeSearchFormBean);
                
                ht.put("midPayMtdList", midPayMtdList);
                ht.put("procFlagList", procFlagList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;               
        
    }

    //입금정산 상세 조회
    @Override
    public Hashtable paydepositDetailList(PaydepositFormBean paydepositFormBean) {

        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = paydepositDAO.paydepositMasterListCount(paydepositFormBean);
                List<Tb_Acq_Result> ls_paydepositDetailList = paydepositDAO.paydepositDetailList(paydepositFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(paydepositFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_paydepositDetailList.size(); i++) {
                        Tb_Acq_Result tb_Acq_Result = ls_paydepositDetailList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Acq_Result.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getPay_type_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getMerch_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getRecord_cl_cd_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getAcq_cd_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getSal_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getAprv_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getTot_trx_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getVat_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getInstallment()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getAcq_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getPay_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getCmms_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Acq_Result.getPay_amt()) + "\"");

                        if (i == (ls_paydepositDetailList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);
                
                commonCodeSearchFormBean.setMain_code("ACQ_CD");
                Tb_Code_Main acqCdList = commonService.codeSearch(commonCodeSearchFormBean);
                
                ht.put("payChnCateList", payChnCateList);
                ht.put("acqCdList", acqCdList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;                   
        
    }

    //입금확정
    @Override
    @Transactional
    public Hashtable depositMetabolismConfirmProcess(PaydepositFormBean paydepositFormBean) {
        
        Hashtable ht = new Hashtable();
        
        Integer updateResult = 0;

        //입금확정 등록        
        List<Tb_Pay_Deposit> ls_paydepositMasterList = paydepositDAO.paydepositMasterList(paydepositFormBean);
        Tb_Pay_Deposit tb_Mid_Deposit = ls_paydepositMasterList.get(0);
        tb_Mid_Deposit.setUser_seq(paydepositFormBean.getUser_seq());

        updateResult = paydepositDAO.depositMetabolismConfirmProcess(tb_Mid_Deposit);        
        
        //입반내역 입금확정
        paydepositDAO.depositAcqDetailProcFlagYUpdate(paydepositFormBean);
        
        //통장입금내역 입금확정  
        paydepositDAO.depositBankDetailProcFlagYUpdate(paydepositFormBean);

        if(updateResult == 0){
            ht.put("Result","fail");
        }else{
            ht.put("Result","success");
        }

        return ht;        
        
    }

    //입금확정취소
    @Override
    @Transactional
    public Hashtable depositMetabolismCancelProcess(PaydepositFormBean paydepositFormBean) {
        
        Hashtable ht = new Hashtable();

        Integer updateResult = 0;
        
        //입금확정 삭제
        updateResult = paydepositDAO.depositMetabolismCancelProcess(paydepositFormBean);
        
        //입반내역 입금확정취소
        paydepositDAO.depositAcqDetailProcFlagNUpdate(paydepositFormBean);
        
        //통장입금내역 입금확정취소  
        paydepositDAO.depositBankDetailProcFlagNUpdate(paydepositFormBean);        
         
        if(updateResult == 0){
            ht.put("Result","fail");
        }else{
            ht.put("Result","success");
        }

        return ht;        
        
    }

    @Override
    public void paydepositMasterListExcel(PaydepositFormBean paydepositFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
              
                List ls_paydepositMasterListExcel = paydepositDAO.paydepositMasterListExcel(paydepositFormBean);
                
                StringBuffer paydepositMasterListExcel = Util.makeData(ls_paydepositMasterListExcel); 
        
                Util.exceldownload(paydepositMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }                
        
    }

    @Override
    public void paydepositDetailListExcel(PaydepositFormBean paydepositFormBean, String excelDownloadFilename, HttpServletResponse response) {
        
        try {
              
                List ls_paydepositDetailListExcel = paydepositDAO.paydepositDetailListExcel(paydepositFormBean);
                
                StringBuffer paydepositDetailListExcel = Util.makeData(ls_paydepositDetailListExcel); 
        
                Util.exceldownload(paydepositDetailListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }                     
        
    }
    
    
    @Override
    public Hashtable OprProfitMasterList(PaydepositFormBean paydepositFormBean) {

        Hashtable ht = new Hashtable();
        
        

        try {
            
                List<Tb_Pay_Deposit> ls_paydepositMasterList = paydepositDAO.OprProfitMasterList(paydepositFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"rows\" : [");
                
                for (int i = 0; i < ls_paydepositMasterList.size(); i++) {
                        Tb_Pay_Deposit tb_Mid_Deposit = ls_paydepositMasterList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"onffmerch_no\":\""+Util.nullToString(tb_Mid_Deposit.getOnffmerch_no())+"\"");
                        sb.append(" ,\"pay_dt\":\""+Util.nullToString(tb_Mid_Deposit.getPay_dt())+"\"");
                        sb.append(" ,\"onoffmerch_ext_pay_dt\":\""+Util.nullToString(tb_Mid_Deposit.getOnoffmerch_ext_pay_dt())+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Mid_Deposit.getOnoffmerch_ext_pay_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getPay_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getOnffmerch_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Mid_Deposit.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToInt(tb_Mid_Deposit.getTot_amt()) + "\"");
                        sb.append(",\"" + Util.nullToInt(tb_Mid_Deposit.getPay_amt()) + "\"");
                        sb.append(",\"" + Util.nullToInt(tb_Mid_Deposit.getCommision()) + "\"");
                        sb.append(",\"" + Util.nullToInt(tb_Mid_Deposit.getOnoff_pay_amt()) + "\"");
                        sb.append(",\"" + Util.nullToInt(tb_Mid_Deposit.getOnoff_cms()) + "\"");
                        sb.append(",\"" + Util.nullToInt(tb_Mid_Deposit.getOnoff_vat()) + "\"");
                        sb.append(",\"" + Util.nullToInt(tb_Mid_Deposit.getAgent_fee()) + "\"");
                        sb.append(",\"" + Util.nullToInt(tb_Mid_Deposit.getProfit()) + "\"");
                        
                        if (i == (ls_paydepositMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                ht.put("ResultSet", sb.toString());
                
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;               
        
    }    
 
    
    
    @Override
    public void OprProfitMasterListExcel(PaydepositFormBean paydepositFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
              
                List ls_paydepositMasterListExcel = paydepositDAO.OprProfitMasterListExcel(paydepositFormBean);
                
                StringBuffer paydepositMasterListExcel = Util.makeData(ls_paydepositMasterListExcel); 
        
                Util.exceldownload(paydepositMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }                
        
    }
}
