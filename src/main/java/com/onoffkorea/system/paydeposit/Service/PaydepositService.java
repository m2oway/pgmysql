/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.paydeposit.Service;

import com.onoffkorea.system.paydeposit.Bean.PaydepositFormBean;
import java.util.Hashtable;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public interface PaydepositService {

    public Hashtable paydepositMasterList(PaydepositFormBean paydepositFormBean);

    public Hashtable paydepositDetailList(PaydepositFormBean paydepositFormBean);

    public Hashtable depositMetabolismConfirmProcess(PaydepositFormBean paydepositFormBean);

    public Hashtable depositMetabolismCancelProcess(PaydepositFormBean paydepositFormBean);

    public void paydepositMasterListExcel(PaydepositFormBean paydepositFormBean, String excelDownloadFilename, HttpServletResponse response);

    public void paydepositDetailListExcel(PaydepositFormBean paydepositFormBean, String excelDownloadFilename, HttpServletResponse response);
    
    public Hashtable OprProfitMasterList(PaydepositFormBean paydepositFormBean);
    
    public void OprProfitMasterListExcel(PaydepositFormBean paydepositFormBean, String excelDownloadFilename, HttpServletResponse response);
}
