/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.paydeposit.Controller;

import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.mid.Bean.MidFormBean;
import com.onoffkorea.system.paydeposit.Bean.PaydepositFormBean;
import com.onoffkorea.system.paydeposit.Service.PaydepositService;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import common.web.servlet.BaseSpringMultiActionController;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/paydeposit/**")
public class PaydepositController extends BaseSpringMultiActionController{
        
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private CommonService commonService;
    
    @Autowired
    private PaydepositService paydepositService;    
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }    

    //입금정산 레이아웃
    @RequestMapping(method= RequestMethod.GET,value = "/paydepositLayout")
    public String paydepositLayout(@Valid MidFormBean midFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        return null;
        
    }
    
    //입금정산 조회
    @RequestMapping(method= RequestMethod.GET,value = "/paydepositMasterList")
    public String paydepositMasterFormList(@Valid PaydepositFormBean paydepositFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        
           SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        if((paydepositFormBean.getDeposit_start_dt() == null) || (paydepositFormBean.getDeposit_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DAY_OF_WEEK, 0);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            paydepositFormBean.setDeposit_start_dt(end_dt);
            paydepositFormBean.setDeposit_end_dt(end_dt);
        }
        
        if((paydepositFormBean.getApp_start_dt() == null) || (paydepositFormBean.getApp_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DATE, -1);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            paydepositFormBean.setApp_start_dt(start_dt);
            paydepositFormBean.setApp_end_dt(start_dt);
        }

        Hashtable ht_paydepositMasterList = paydepositService.paydepositMasterList(paydepositFormBean);
        String ls_paydepositMasterList = (String) ht_paydepositMasterList.get("ResultSet");
        
        Tb_Code_Main midPayMtdList = (Tb_Code_Main) ht_paydepositMasterList.get("midPayMtdList");
        Tb_Code_Main procFlagList = (Tb_Code_Main) ht_paydepositMasterList.get("procFlagList");
        
        SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
        Date start_date = formatter.parse(paydepositFormBean.getDeposit_start_dt());
        Date end_date   = formatter.parse(paydepositFormBean.getDeposit_end_dt());
        
        String start_dt = formatter2.format(start_date);
        String end_dt = formatter2.format(end_date);

        paydepositFormBean.setDeposit_start_dt(start_dt);
        paydepositFormBean.setDeposit_end_dt(end_dt);
        
        Date app_start_date = formatter.parse(paydepositFormBean.getApp_start_dt());
        Date app_end_date   = formatter.parse(paydepositFormBean.getApp_end_dt());
        
        String app_start_dt = formatter2.format(app_start_date);
        String app_end_dt = formatter2.format(app_end_date);

        paydepositFormBean.setApp_start_dt(app_start_dt);
        paydepositFormBean.setApp_end_dt(app_end_dt);
        
        model.addAttribute("ls_paydepositMasterList",ls_paydepositMasterList);     
        model.addAttribute("midPayMtdList",midPayMtdList);     
        model.addAttribute("procFlagList",procFlagList);             
        
        
        return null;
        
    }
    
    //입금정산 조회
    @RequestMapping(method= RequestMethod.POST,value = "/paydepositMasterList")
    @ResponseBody
    public String paydepositMasterList(@Valid PaydepositFormBean paydepositFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_paydepositMasterList = paydepositService.paydepositMasterList(paydepositFormBean);
        String ls_paydepositMasterList = (String) ht_paydepositMasterList.get("ResultSet");
        
        return ls_paydepositMasterList;
        
    }    
    
    //입금정산 상세 조회
    @RequestMapping(method= RequestMethod.GET,value = "/paydepositDetailList")
    public String paydepositDetailFormList(@Valid PaydepositFormBean paydepositFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_paydepositDetailList = paydepositService.paydepositDetailList(paydepositFormBean);
        String ls_paydepositDetailList = (String) ht_paydepositDetailList.get("ResultSet");
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_paydepositDetailList.get("payChnCateList");
        
        Tb_Code_Main acqCdList = (Tb_Code_Main) ht_paydepositDetailList.get("acqCdList");
        
        model.addAttribute("ls_paydepositDetailList",ls_paydepositDetailList);     
        model.addAttribute("payChnCateList",payChnCateList);             
        model.addAttribute("acqCdList",acqCdList);       
        
        return null;
        
    }
    
    //입금정산 조회
    @RequestMapping(method= RequestMethod.POST,value = "/paydepositDetailList")
    @ResponseBody
    public String paydepositDetailList(@Valid PaydepositFormBean paydepositFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_paydepositDetailList = paydepositService.paydepositDetailList(paydepositFormBean);
        String ls_paydepositDetailList = (String) ht_paydepositDetailList.get("ResultSet");
        
        return ls_paydepositDetailList;
        
    }        
    
    /*
     *  입금대사 확정 처리
     */
    @RequestMapping(value="/depositMetabolismConfirmProcess", method= RequestMethod.POST)
    @ResponseBody
    public String depositMetabolismConfirmProcess(@Valid PaydepositFormBean paydepositFormBean, Model model , HttpServletRequest request,@ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        paydepositFormBean.setUser_seq(siteSession.getUser_seq());

        Hashtable ht_depositMetabolismConfirmProcess = paydepositService.depositMetabolismConfirmProcess(paydepositFormBean);

        return (String)ht_depositMetabolismConfirmProcess.get("Result");
    }    
    
    /*
     *  입금대사 확정 취소 처리
     */
    @RequestMapping(value="/depositMetabolismCancelProcess", method= RequestMethod.POST)
    @ResponseBody
    public String depositMetabolismCancelProcess(@Valid PaydepositFormBean paydepositFormBean, Model model , HttpServletRequest request,@ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        paydepositFormBean.setUser_seq(siteSession.getUser_seq());

        Hashtable ht_depositMetabolismCancelProcess = paydepositService.depositMetabolismCancelProcess(paydepositFormBean);

        return (String)ht_depositMetabolismCancelProcess.get("Result");
    }    
    
    //입금정산 엑셀
    @RequestMapping(method= RequestMethod.POST,value = "/paydepositMasterListExcel")
    public void paydepositMasterListExcel(@Valid PaydepositFormBean paydepositFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, HttpServletResponse response) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
         if((paydepositFormBean.getDeposit_start_dt() == null) || (paydepositFormBean.getDeposit_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DAY_OF_WEEK, 0);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            paydepositFormBean.setDeposit_start_dt(end_dt);
            paydepositFormBean.setDeposit_end_dt(end_dt);
        }
        
        if((paydepositFormBean.getApp_start_dt() == null) || (paydepositFormBean.getApp_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DATE, -1);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            paydepositFormBean.setApp_start_dt(start_dt);
            paydepositFormBean.setApp_end_dt(start_dt);
        }



        String excelDownloadFilename = new String(getMessage("paydeposit.paydepositMasterListExcel.fileName"));
        
        paydepositService.paydepositMasterListExcel(paydepositFormBean, excelDownloadFilename, response); 

    }        
    
    //입금정산 상세내역 엑셀
    @RequestMapping(method= RequestMethod.POST,value = "/paydepositDetailListExcel")
    public void paydepositDetailListExcel(@Valid PaydepositFormBean paydepositFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, HttpServletResponse response) throws Exception { 

        String excelDownloadFilename = new String(getMessage("paydeposit.paydepositDetailListExcel.fileName"));
        
        paydepositService.paydepositDetailListExcel(paydepositFormBean, excelDownloadFilename, response); 

    }            

    
    
    //영업이익금 조회
    @RequestMapping(method= RequestMethod.GET,value = "/OprProfitMasterList")
    public String OprProfitMasterFormList(@Valid PaydepositFormBean paydepositFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        
        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        /*
        if((paydepositFormBean.getDeposit_start_dt() == null) || (paydepositFormBean.getDeposit_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DAY_OF_WEEK, 0);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            paydepositFormBean.setDeposit_start_dt(end_dt);
            paydepositFormBean.setDeposit_end_dt(end_dt);
        }
        
        if((paydepositFormBean.getApp_start_dt() == null) || (paydepositFormBean.getApp_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DATE, -1);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            paydepositFormBean.setApp_start_dt(start_dt);
            paydepositFormBean.setApp_end_dt(start_dt);
        }
        */
        
        if((paydepositFormBean.getOnoffmerch_ext_pay_dt_start() == null) || (paydepositFormBean.getOnoffmerch_ext_pay_dt_end()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            //cal.add(Calendar.DATE, -1);
    
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            paydepositFormBean.setOnoffmerch_ext_pay_dt_start(start_dt);
            paydepositFormBean.setOnoffmerch_ext_pay_dt_end(start_dt);
        }        

        Hashtable ht_paydepositMasterList = paydepositService.OprProfitMasterList(paydepositFormBean);
        String ls_paydepositMasterList = (String) ht_paydepositMasterList.get("ResultSet");
        
        model.addAttribute("ls_OprProfitMasterList",ls_paydepositMasterList);     
        
        return null;
        
    }
    
    //영업이익금 조회
    @RequestMapping(method= RequestMethod.POST,value = "/OprProfitMasterList")
    @ResponseBody
    public String OprProfitMasterList(@Valid PaydepositFormBean paydepositFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_paydepositMasterList = paydepositService.OprProfitMasterList(paydepositFormBean);
        String ls_paydepositMasterList = (String) ht_paydepositMasterList.get("ResultSet");
        
        return ls_paydepositMasterList;
        
    }    
    
        //영업이익금 엑셀
    @RequestMapping(method= RequestMethod.POST,value = "/OprProfitMasterListExcel")
    public void OprProfitMasterListExcel(@Valid PaydepositFormBean paydepositFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, HttpServletResponse response) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        if((paydepositFormBean.getOnoffmerch_ext_pay_dt_start() == null) || (paydepositFormBean.getOnoffmerch_ext_pay_dt_end()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DATE, -1);
    
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            paydepositFormBean.setOnoffmerch_ext_pay_dt_start(start_dt);
            paydepositFormBean.setOnoffmerch_ext_pay_dt_end(start_dt);
        }        

        String excelDownloadFilename = new String(getMessage("paydeposit.OprProfitMasterListExcel.fileName"));
        
        paydepositService.OprProfitMasterListExcel(paydepositFormBean, excelDownloadFilename, response); 

    } 
}
