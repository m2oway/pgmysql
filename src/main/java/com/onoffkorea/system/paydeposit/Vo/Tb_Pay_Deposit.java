/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.paydeposit.Vo;

/**
 *
 * @author Administrator
 */
public class Tb_Pay_Deposit {
    
    private String app_start ;
    private String app_end ;
    
    private String deposit_dt     ;
    private String merch_no       ;
    private String pay_mtd        ;
    private String deposit_seq    ;
    private String bank_cd        ;
    private String acc_no         ;
    private String tot_cnt        ;
    private String tot_amt        ;
    private String acq_cnt        ;
    private String acq_amt        ;
    private String ret_cnt        ;
    private String ret_amt        ;
    private String dfr_cnt        ;
    private String dfr_amt        ;
    private String dfr_rese_cnt   ;
    private String dfr_rese_amt   ;
    private String deposit_amt    ;
    private String in_amt         ;
    private String mis_amt        ;
    private String out_amt        ;
    private String decision_dt    ;
    private String decision_user  ;
    private String proc_flag      ;
    private String ins_dt         ;
    private String mod_dt         ;
    private String ins_user       ;
    private String mod_user       ;    
    private String rnum           ;
    private String pay_mtd_nm;
    private String bank_cd_nm;
    private String real_deposit_amt;
    private String diff_amt;
    private String commission;
    private String pay_type_nm;
    private String user_seq;
    
    private String app_dt	   ;
    private String tid         ;
    private String chkacqamt   ;
    private String t2acq_amt   ;
    private String app_amt     ;
    private String cncl_amt    ;
private String onffmerch_no                            ;
private String pay_targetamt_chk                       ;
private String pay_amt_chk                             ;
private String cms_chk                                 ;
private String vat_chk                                 ;
private String app_amt_chk                             ;
private String cncl_chk                                ;
private String pay_targetamt                           ;
private String pay_amt                                 ;
private String vat                                     ;
private String mer_pay_targetamt                           ;
private String mer_pay_amt                                 ;
private String mer_commission                              ;
private String mer_vat                                     ;
private String mer_app_amt                                 ;
private String mer_cncl_amt                                ;


private String onoffmerch_ext_pay_dt;
private String pay_dt;
private String merch_nm;
private String commision;
private String onoff_pay_amt;
private String onoff_cms;
private String onoff_vat;
private String agent_fee;
private String profit;


private String withhold_tax                                     ;
private String mer_withhold_tax                                     ;
private String withhold_tax_chk                                     ;

    public String getWithhold_tax() {
        return withhold_tax;
    }

    public void setWithhold_tax(String withhold_tax) {
        this.withhold_tax = withhold_tax;
    }

    public String getMer_withhold_tax() {
        return mer_withhold_tax;
    }

    public void setMer_withhold_tax(String mer_withhold_tax) {
        this.mer_withhold_tax = mer_withhold_tax;
    }

    public String getWithhold_tax_chk() {
        return withhold_tax_chk;
    }

    public void setWithhold_tax_chk(String withhold_tax_chk) {
        this.withhold_tax_chk = withhold_tax_chk;
    }

    public String getOnoffmerch_ext_pay_dt() {
        return onoffmerch_ext_pay_dt;
    }

    public void setOnoffmerch_ext_pay_dt(String onoffmerch_ext_pay_dt) {
        this.onoffmerch_ext_pay_dt = onoffmerch_ext_pay_dt;
    }

    public String getPay_dt() {
        return pay_dt;
    }

    public void setPay_dt(String pay_dt) {
        this.pay_dt = pay_dt;
    }

    public String getCommision() {
        return commision;
    }

    public void setCommision(String commision) {
        this.commision = commision;
    }

    public String getOnoff_pay_amt() {
        return onoff_pay_amt;
    }

    public void setOnoff_pay_amt(String onoff_pay_amt) {
        this.onoff_pay_amt = onoff_pay_amt;
    }

    public String getOnoff_cms() {
        return onoff_cms;
    }

    public void setOnoff_cms(String onoff_cms) {
        this.onoff_cms = onoff_cms;
    }

    public String getOnoff_vat() {
        return onoff_vat;
    }

    public void setOnoff_vat(String onoff_vat) {
        this.onoff_vat = onoff_vat;
    }

    public String getAgent_fee() {
        return agent_fee;
    }

    public void setAgent_fee(String agent_fee) {
        this.agent_fee = agent_fee;
    }

    public String getProfit() {
        return profit;
    }

    public void setProfit(String profit) {
        this.profit = profit;
    }




    public String getApp_start() {
        return app_start;
    }

    public void setApp_start(String app_start) {
        this.app_start = app_start;
    }

    public String getApp_end() {
        return app_end;
    }

    public void setApp_end(String app_end) {
        this.app_end = app_end;
    }


    public String getMerch_nm() {
        return merch_nm;
    }

    public void setMerch_nm(String merch_nm) {
        this.merch_nm = merch_nm;
    }



    public String getMer_pay_targetamt() {
        return mer_pay_targetamt;
    }

    public void setMer_pay_targetamt(String mer_pay_targetamt) {
        this.mer_pay_targetamt = mer_pay_targetamt;
    }

    public String getMer_pay_amt() {
        return mer_pay_amt;
    }

    public void setMer_pay_amt(String mer_pay_amt) {
        this.mer_pay_amt = mer_pay_amt;
    }

    public String getMer_commission() {
        return mer_commission;
    }

    public void setMer_commission(String mer_commission) {
        this.mer_commission = mer_commission;
    }

    public String getMer_vat() {
        return mer_vat;
    }

    public void setMer_vat(String mer_vat) {
        this.mer_vat = mer_vat;
    }

    public String getMer_app_amt() {
        return mer_app_amt;
    }

    public void setMer_app_amt(String mer_app_amt) {
        this.mer_app_amt = mer_app_amt;
    }

    public String getMer_cncl_amt() {
        return mer_cncl_amt;
    }

    public void setMer_cncl_amt(String mer_cncl_amt) {
        this.mer_cncl_amt = mer_cncl_amt;
    }



private String gen_dt;

    public String getGen_dt() {
        return gen_dt;
    }

    public void setGen_dt(String gen_dt) {
        this.gen_dt = gen_dt;
    }



    public String getOnffmerch_no() {
        return onffmerch_no;
    }

    public void setOnffmerch_no(String onffmerch_no) {
        this.onffmerch_no = onffmerch_no;
    }

    public String getPay_targetamt_chk() {
        return pay_targetamt_chk;
    }

    public void setPay_targetamt_chk(String pay_targetamt_chk) {
        this.pay_targetamt_chk = pay_targetamt_chk;
    }

    public String getPay_amt_chk() {
        return pay_amt_chk;
    }

    public void setPay_amt_chk(String pay_amt_chk) {
        this.pay_amt_chk = pay_amt_chk;
    }

    public String getCms_chk() {
        return cms_chk;
    }

    public void setCms_chk(String cms_chk) {
        this.cms_chk = cms_chk;
    }

    public String getVat_chk() {
        return vat_chk;
    }

    public void setVat_chk(String vat_chk) {
        this.vat_chk = vat_chk;
    }

    public String getApp_amt_chk() {
        return app_amt_chk;
    }

    public void setApp_amt_chk(String app_amt_chk) {
        this.app_amt_chk = app_amt_chk;
    }

    public String getCncl_chk() {
        return cncl_chk;
    }

    public void setCncl_chk(String cncl_chk) {
        this.cncl_chk = cncl_chk;
    }

    public String getPay_targetamt() {
        return pay_targetamt;
    }

    public void setPay_targetamt(String pay_targetamt) {
        this.pay_targetamt = pay_targetamt;
    }

    public String getPay_amt() {
        return pay_amt;
    }

    public void setPay_amt(String pay_amt) {
        this.pay_amt = pay_amt;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }





    public String getApp_dt() {
        return app_dt;
    }

    public void setApp_dt(String app_dt) {
        this.app_dt = app_dt;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getChkacqamt() {
        return chkacqamt;
    }

    public void setChkacqamt(String chkacqamt) {
        this.chkacqamt = chkacqamt;
    }

    public String getT2acq_amt() {
        return t2acq_amt;
    }

    public void setT2acq_amt(String t2acq_amt) {
        this.t2acq_amt = t2acq_amt;
    }

    public String getApp_amt() {
        return app_amt;
    }

    public void setApp_amt(String app_amt) {
        this.app_amt = app_amt;
    }

    public String getCncl_amt() {
        return cncl_amt;
    }

    public void setCncl_amt(String cncl_amt) {
        this.cncl_amt = cncl_amt;
    }

    
    
    
    public String getUser_seq() {
        return user_seq;
    }

    public void setUser_seq(String user_seq) {
        this.user_seq = user_seq;
    }

    public String getPay_type_nm() {
        return pay_type_nm;
    }

    public void setPay_type_nm(String pay_type_nm) {
        this.pay_type_nm = pay_type_nm;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getDiff_amt() {
        return diff_amt;
    }

    public void setDiff_amt(String diff_amt) {
        this.diff_amt = diff_amt;
    }

    public String getReal_deposit_amt() {
        return real_deposit_amt;
    }

    public void setReal_deposit_amt(String real_deposit_amt) {
        this.real_deposit_amt = real_deposit_amt;
    }

    public String getBank_cd_nm() {
        return bank_cd_nm;
    }

    public void setBank_cd_nm(String bank_cd_nm) {
        this.bank_cd_nm = bank_cd_nm;
    }

    public String getPay_mtd_nm() {
        return pay_mtd_nm;
    }

    public void setPay_mtd_nm(String pay_mtd_nm) {
        this.pay_mtd_nm = pay_mtd_nm;
    }

    public String getRnum() {
        return rnum;
    }

    public void setRnum(String rnum) {
        this.rnum = rnum;
    }

    public String getDeposit_dt() {
        return deposit_dt;
    }

    public void setDeposit_dt(String deposit_dt) {
        this.deposit_dt = deposit_dt;
    }

    public String getMerch_no() {
        return merch_no;
    }

    public void setMerch_no(String merch_no) {
        this.merch_no = merch_no;
    }

    public String getPay_mtd() {
        return pay_mtd;
    }

    public void setPay_mtd(String pay_mtd) {
        this.pay_mtd = pay_mtd;
    }

    public String getDeposit_seq() {
        return deposit_seq;
    }

    public void setDeposit_seq(String deposit_seq) {
        this.deposit_seq = deposit_seq;
    }

    public String getBank_cd() {
        return bank_cd;
    }

    public void setBank_cd(String bank_cd) {
        this.bank_cd = bank_cd;
    }

    public String getAcc_no() {
        return acc_no;
    }

    public void setAcc_no(String acc_no) {
        this.acc_no = acc_no;
    }

    public String getTot_cnt() {
        return tot_cnt;
    }

    public void setTot_cnt(String tot_cnt) {
        this.tot_cnt = tot_cnt;
    }

    public String getTot_amt() {
        return tot_amt;
    }

    public void setTot_amt(String tot_amt) {
        this.tot_amt = tot_amt;
    }

    public String getAcq_cnt() {
        return acq_cnt;
    }

    public void setAcq_cnt(String acq_cnt) {
        this.acq_cnt = acq_cnt;
    }

    public String getAcq_amt() {
        return acq_amt;
    }

    public void setAcq_amt(String acq_amt) {
        this.acq_amt = acq_amt;
    }

    public String getRet_cnt() {
        return ret_cnt;
    }

    public void setRet_cnt(String ret_cnt) {
        this.ret_cnt = ret_cnt;
    }

    public String getRet_amt() {
        return ret_amt;
    }

    public void setRet_amt(String ret_amt) {
        this.ret_amt = ret_amt;
    }

    public String getDfr_cnt() {
        return dfr_cnt;
    }

    public void setDfr_cnt(String dfr_cnt) {
        this.dfr_cnt = dfr_cnt;
    }

    public String getDfr_amt() {
        return dfr_amt;
    }

    public void setDfr_amt(String dfr_amt) {
        this.dfr_amt = dfr_amt;
    }

    public String getDfr_rese_cnt() {
        return dfr_rese_cnt;
    }

    public void setDfr_rese_cnt(String dfr_rese_cnt) {
        this.dfr_rese_cnt = dfr_rese_cnt;
    }

    public String getDfr_rese_amt() {
        return dfr_rese_amt;
    }

    public void setDfr_rese_amt(String dfr_rese_amt) {
        this.dfr_rese_amt = dfr_rese_amt;
    }

    public String getDeposit_amt() {
        return deposit_amt;
    }

    public void setDeposit_amt(String deposit_amt) {
        this.deposit_amt = deposit_amt;
    }

    public String getIn_amt() {
        return in_amt;
    }

    public void setIn_amt(String in_amt) {
        this.in_amt = in_amt;
    }

    public String getMis_amt() {
        return mis_amt;
    }

    public void setMis_amt(String mis_amt) {
        this.mis_amt = mis_amt;
    }

    public String getOut_amt() {
        return out_amt;
    }

    public void setOut_amt(String out_amt) {
        this.out_amt = out_amt;
    }

    public String getDecision_dt() {
        return decision_dt;
    }

    public void setDecision_dt(String decision_dt) {
        this.decision_dt = decision_dt;
    }

    public String getDecision_user() {
        return decision_user;
    }

    public void setDecision_user(String decision_user) {
        this.decision_user = decision_user;
    }

    public String getProc_flag() {
        return proc_flag;
    }

    public void setProc_flag(String proc_flag) {
        this.proc_flag = proc_flag;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }
    
}
