/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.paydeposit.Dao;

import com.onoffkorea.system.acq_result.Vo.Tb_Acq_Result;
import com.onoffkorea.system.paydeposit.Bean.PaydepositFormBean;
import com.onoffkorea.system.paydeposit.Vo.Tb_Pay_Deposit;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface PaydepositDAO {

    public List<Tb_Pay_Deposit> paydepositMasterList(PaydepositFormBean paydepositFormBean);
    
    public List<Tb_Pay_Deposit> OprProfitMasterList(PaydepositFormBean paydepositFormBean);

    public List<Tb_Acq_Result> paydepositDetailList(PaydepositFormBean paydepositFormBean);

    public Integer depositMetabolismConfirmProcess(Tb_Pay_Deposit tb_Mid_Deposit);

    public Integer depositMetabolismCancelProcess(PaydepositFormBean paydepositFormBean);

    public Integer paydepositMasterListCount(PaydepositFormBean paydepositFormBean);

    public void depositAcqDetailProcFlagYUpdate(PaydepositFormBean paydepositFormBean);

    public void depositBankDetailProcFlagYUpdate(PaydepositFormBean paydepositFormBean);

    public void depositAcqDetailProcFlagNUpdate(PaydepositFormBean paydepositFormBean);

    public void depositBankDetailProcFlagNUpdate(PaydepositFormBean paydepositFormBean);

    public List paydepositMasterListExcel(PaydepositFormBean paydepositFormBean);

    public List paydepositDetailListExcel(PaydepositFormBean paydepositFormBean);
    
    public List OprProfitMasterListExcel(PaydepositFormBean paydepositFormBean);
    
}
