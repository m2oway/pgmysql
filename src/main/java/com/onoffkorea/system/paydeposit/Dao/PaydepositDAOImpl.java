/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.paydeposit.Dao;

import com.onoffkorea.system.acq_result.Vo.Tb_Acq_Result;
import com.onoffkorea.system.paydeposit.Bean.PaydepositFormBean;
import com.onoffkorea.system.paydeposit.Vo.Tb_Pay_Deposit;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("paydepositDAO")
public class PaydepositDAOImpl extends SqlSessionDaoSupport implements PaydepositDAO{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    //입금정산 조회
    @Override
    public List<Tb_Pay_Deposit> paydepositMasterList(PaydepositFormBean paydepositFormBean) {
        
        return (List)getSqlSession().selectList("Paydeposit.paydepositMasterList",paydepositFormBean);
        
    }

    //이익금조회
    @Override
    public List<Tb_Pay_Deposit> OprProfitMasterList(PaydepositFormBean paydepositFormBean) {
        
        return (List)getSqlSession().selectList("Paydeposit.OprProfitMasterList",paydepositFormBean);
        
    }
    
    
    //입금정산 조회
    @Override
    public List<Tb_Acq_Result> paydepositDetailList(PaydepositFormBean paydepositFormBean) {
        
        return (List)getSqlSession().selectList("Paydeposit.paydepositDetailList",paydepositFormBean);
        
    }

    //입금확정
    @Override
    public Integer depositMetabolismConfirmProcess(Tb_Pay_Deposit tb_Mid_Deposit) {
        
        return (Integer)getSqlSession().insert("Paydeposit.depositMetabolismConfirmProcess",tb_Mid_Deposit);
        
    }

    //입금확정취소
    @Override
    public Integer depositMetabolismCancelProcess(PaydepositFormBean paydepositFormBean) {
        
        return (Integer)getSqlSession().delete("Paydeposit.depositMetabolismCancelProcess",paydepositFormBean);
        
    }

    //입금정산 상세 내역 카운트
    @Override
    public Integer paydepositMasterListCount(PaydepositFormBean paydepositFormBean) {
        
        return (Integer)getSqlSession().selectOne("Paydeposit.paydepositDetailListCount",paydepositFormBean);
        
    }

    //입반내역 입금확정 처리
    @Override
    public void depositAcqDetailProcFlagYUpdate(PaydepositFormBean paydepositFormBean) {
        
        getSqlSession().update("Paydeposit.depositAcqDetailProcFlagYUpdate",paydepositFormBean);
        
    }
    
    //통장입금내역 입금확정 처리
    @Override
    public void depositBankDetailProcFlagYUpdate(PaydepositFormBean paydepositFormBean) {
        
        getSqlSession().update("Paydeposit.depositBankDetailProcFlagYUpdate",paydepositFormBean);
        
    }     
    
    //입반내역 입금확정취소 처리
    @Override
    public void depositAcqDetailProcFlagNUpdate(PaydepositFormBean paydepositFormBean) {
        
        getSqlSession().update("Paydeposit.depositAcqDetailProcFlagNUpdate",paydepositFormBean);
        
    }
    
    //통장입금내역 입금확정취소
    @Override
    public void depositBankDetailProcFlagNUpdate(PaydepositFormBean paydepositFormBean) {
        
        getSqlSession().update("Paydeposit.depositBankDetailProcFlagNUpdate",paydepositFormBean);
        
    }         

    @Override
    public List paydepositMasterListExcel(PaydepositFormBean paydepositFormBean) {
        
        return (List)getSqlSession().selectList("Paydeposit.paydepositMasterListExcel",paydepositFormBean);
        
    }
    
    @Override
    public List paydepositDetailListExcel(PaydepositFormBean paydepositFormBean) {
        
        return (List)getSqlSession().selectList("Paydeposit.paydepositDetailListExcel",paydepositFormBean);
        
    }    
    

    @Override
    public List OprProfitMasterListExcel(PaydepositFormBean paydepositFormBean) {
        
        return (List)getSqlSession().selectList("Paydeposit.OprProfitMasterListExcel",paydepositFormBean);
        
    }    
}
