/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.holiday.Dao;

import com.onoffkorea.system.holiday.Bean.HolidayFormBean;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("holidayDAO")
public class HolidayDAOImpl extends SqlSessionDaoSupport implements HolidayDAO{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    //코드 정보 관리
    @Override
    public List holidayMasterList(HolidayFormBean holidayFormBean) {
        
        return (List) getSqlSession().selectList("Holiday.holidayMasterList", holidayFormBean);
        
    }

    //코드 정보 삭제
    @Override
    public void holidayMasterDelete(HolidayFormBean holidayFormBean) {

        getSqlSession().update("Holiday.holidayMasterDelete", holidayFormBean);
        
    }
    

    //코드 정보 등록
    @Override
    public void holidayMasterInsert(HolidayFormBean holidayFormBean) {

        getSqlSession().insert("Holiday.holidayMasterInsert", holidayFormBean);
        
    }
    
  

    //코드 정보 수정
    @Override
    public Integer holidayMasterUpdate(HolidayFormBean holidayFormBean) {

       return (Integer) getSqlSession().update("Holiday.holidayMasterUpdate", holidayFormBean);
        
    }

    @Override
    public Integer holidayMasterListCount(HolidayFormBean holidayFormBean) {
        return (Integer) getSqlSession().selectOne("Holiday.holidayMasterListCount", holidayFormBean);
    }
   
    
}
