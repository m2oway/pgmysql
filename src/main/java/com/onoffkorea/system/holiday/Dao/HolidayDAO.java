/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.holiday.Dao;

import com.onoffkorea.system.holiday.Bean.HolidayFormBean;

/**
 *
 * @author Administrator
 */
public interface HolidayDAO {

    public Object holidayMasterList(HolidayFormBean holidayFormBean);
   
    public void holidayMasterDelete(HolidayFormBean holidayFormBean);
    
    public void holidayMasterInsert(HolidayFormBean holidayFormBean);

    public Integer holidayMasterUpdate(HolidayFormBean holidayFormBean);
    
    public Integer holidayMasterListCount(HolidayFormBean holidayFormBean);
    
    
    
}
