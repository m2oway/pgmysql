/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.holiday.Controller;

import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.holiday.Bean.HolidayFormBean;
import com.onoffkorea.system.holiday.Service.HolidayService;
import com.onoffkorea.system.company.Bean.CompanyFormBean;
import com.onoffkorea.system.company.Service.CompanyService;
import com.onoffkorea.system.auth.Bean.AuthFormBean;
import com.onoffkorea.system.auth.Service.AuthService;
import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.holiday.Vo.Tb_Holiday;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/holiday/**")
public class HolidayController {
        
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private HolidayService HolidayService;
    
    @Autowired
    private CompanyService companyService;
    
    @Autowired
    private AuthService authService;
    
    @Autowired
    private CommonService commonService;    
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }    
    
     //회원 정보 조회
     @RequestMapping(method= RequestMethod.GET,value = "/holidayMasterList")
    public String holidayMasterList(@Valid HolidayFormBean holidayFormBean, @Valid CompanyFormBean companyFormBean, @Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        holidayFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            holidayFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            holidayFormBean.setSes_onfftid(siteSession.getSes_onfftid());
            holidayFormBean.setUser_seq(siteSession.getUser_seq());
        }else if("03".equals(siteSession.getSes_user_cate())){
            holidayFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
            holidayFormBean.setUser_seq(siteSession.getUser_seq());
        }     
        
        String strTpCurDateYYYY  = Util.getTodayDate().substring(0, 4);
        System.out.println("strTpCurDateYYYY : " + strTpCurDateYYYY);
        
        String strTpHoliday_date_start = Util.nullToString(holidayFormBean.getHoliday_date_start());
        if(strTpHoliday_date_start.equals(""))
        {
            holidayFormBean.setHoliday_date_start(strTpCurDateYYYY+"0101");
        }
        else
        {
             holidayFormBean.setHoliday_date_start(strTpHoliday_date_start.replaceAll("-", ""));
        }
        
        String strTpHoliday_date_end = Util.nullToString(holidayFormBean.getHoliday_date_end());
        if(strTpHoliday_date_end.equals(""))
        {
            //holidayFormBean.setHoliday_date_end(Util.lastDayOfMonth(strTpCurDateYYYY+"1231","yyyymmdd"));
            holidayFormBean.setHoliday_date_end(strTpCurDateYYYY+"1231");
        }
        else
        {
             holidayFormBean.setHoliday_date_end(strTpHoliday_date_end.replaceAll("-", ""));
        }        
        
        Hashtable ht_HolidayMasterList = HolidayService.holidayMasterList(holidayFormBean);
        List ls_HolidayMasterList = (List) ht_HolidayMasterList.get("ResultSet");
        Integer total_count = (Integer) ht_HolidayMasterList.get("totalCount");
        StringBuilder cc = new StringBuilder();
        cc.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(holidayFormBean.getStart_no())-1)+"\",\"rows\" : [");
        for (int i = 0; i < ls_HolidayMasterList.size(); i++) {
                Tb_Holiday tb_Holiday = (Tb_Holiday) ls_HolidayMasterList.get(i);
                cc.append("{\"id\":" + i);
                cc.append(",\"userdata\":{");                   
                cc.append(" \"yyyymmdd\":\""+Util.nullToString(tb_Holiday.getYyyymmdd())+"\"");
                cc.append("}");
                cc.append(",\"data\":[");
                cc.append(" \"" + Util.nullToString(tb_Holiday.getRnum()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_Holiday.getYyyymmdd()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_Holiday.getHolidayname()) + "\"");
                
                if (i == (ls_HolidayMasterList.size() - 1)) {
                        cc.append("]}");
                } else {
                        cc.append("]},");
                }
        }

        cc.append("]}");    
        model.addAttribute("HolidayMasterList",cc.toString());
        return null;
        
    }
    
    //회원 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/holidayMasterList")
    public @ResponseBody String holidayMasterList(@Valid HolidayFormBean holidayFormBean, @Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        holidayFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            holidayFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            holidayFormBean.setSes_onfftid(siteSession.getSes_onfftid());
            holidayFormBean.setUser_seq(siteSession.getUser_seq());
        }else if("03".equals(siteSession.getSes_user_cate())){
            holidayFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
            holidayFormBean.setUser_seq(siteSession.getUser_seq());
        }                 
        
        Hashtable ht_HolidayMasterList = HolidayService.holidayMasterList(holidayFormBean);
        List ls_HolidayMasterList = (List) ht_HolidayMasterList.get("ResultSet");
        Integer total_count = (Integer) ht_HolidayMasterList.get("totalCount");
        StringBuilder cc = new StringBuilder();
        cc.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(holidayFormBean.getStart_no())-1)+"\",\"rows\" : [");
        for (int i = 0; i < ls_HolidayMasterList.size(); i++) {
                Tb_Holiday tb_Holiday = (Tb_Holiday) ls_HolidayMasterList.get(i);
                cc.append("{\"id\":" + i);
                cc.append(",\"userdata\":{");                   
                cc.append(" \"yyyymmdd\":\""+Util.nullToString(tb_Holiday.getYyyymmdd())+"\"");
                cc.append("}");
                cc.append(",\"data\":[");
                cc.append(" \"" + Util.nullToString(tb_Holiday.getRnum()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_Holiday.getYyyymmdd()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_Holiday.getHolidayname()) + "\"");
 
                if (i == (ls_HolidayMasterList.size() - 1)) {
                        cc.append("]}");
                } else {
                        cc.append("]},");
                }
        }

        cc.append("]}");    
  
        return cc.toString();
        
        }
    
    
     
    
    //회원 정보 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/holidayMasterDelete")
    public @ResponseBody String holidayMasterDelete(@Valid HolidayFormBean holidayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {
             
        HolidayService.holidayMasterDelete(holidayFormBean);
        
        return null;
        
    }
    
    
    //회원 정보 등록
    @RequestMapping(method= RequestMethod.GET,value = "/holidayMasterInsert")
    public String holidayMasterInsertForm(@Valid HolidayFormBean holidayFormBean, Model model, @Valid CompanyFormBean companyFormBean,@Valid AuthFormBean authFormBean, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        StringBuilder sb = new StringBuilder();
        sb.append("{rows : []}");
                
        return null;
        
    }   
    
    
    //회원 정보 등록
    @RequestMapping(method= RequestMethod.POST,value = "/holidayMasterInsert")
    public @ResponseBody String holidayMasterInsert(@Valid HolidayFormBean holidayFormBean, @Valid CompanyFormBean companyFormBean,@Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {
        
         HolidayService.holidayMasterInsert(holidayFormBean);
         
        return null;
        
    }  
    
    
    //휴일 정보 수정 
    @RequestMapping(method= RequestMethod.GET,value = "/holidayMasterUpdate")
    public String holidayMasterUpdateForm(@Valid HolidayFormBean holidayFormBean,@Valid CompanyFormBean companyFormBean,@Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        holidayFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        
        Hashtable ht_HolidayMasterList = HolidayService.holidayMasterList(holidayFormBean);
        List ls_HolidayMasterList = (List) ht_HolidayMasterList.get("ResultSet");
        Tb_Holiday tb_Holiday = (Tb_Holiday) ls_HolidayMasterList.get(0);
        
                
        StringBuilder sb = new StringBuilder();
        sb.append("{rows : []}");

        model.addAttribute("HolidayMasterList",sb.toString());
        model.addAttribute("tb_Holiday",tb_Holiday);
        
        return null;
        
    }       

    //회원수정
    @RequestMapping(method= RequestMethod.POST,value = "/holidayMasterUpdate")
    public @ResponseBody String holidayMasterUpdate(@Valid HolidayFormBean holidayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        String strreturnval = HolidayService.holidayMasterUpdate(holidayFormBean);
        logger.trace("strreturnval : "+ strreturnval);
        return strreturnval;
        
    }  
        
   

}
