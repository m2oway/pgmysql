/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.holiday.Service;

import com.onoffkorea.system.holiday.Bean.HolidayFormBean;
import java.util.Hashtable;

/**
 *
 * @author Administrator
 */
public interface HolidayService {

    public Hashtable  holidayMasterList(HolidayFormBean holidayFormBean);
    
    public void  holidayMasterDelete(HolidayFormBean holidayFormBean);   

    public void  holidayMasterInsert(HolidayFormBean holidayFormBean);    

    public String  holidayMasterUpdate(HolidayFormBean holidayFormBean);
   
}