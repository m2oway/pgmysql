/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.holiday.Service;

import com.onoffkorea.system.holiday.Bean.HolidayFormBean;
import com.onoffkorea.system.holiday.Dao.HolidayDAO;
import java.util.Hashtable;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service("holidayService")
public class HolidayServiceImpl  implements HolidayService{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private HolidayDAO holidayDAO;

    //코드 정보 관리
    @Override
    public Hashtable holidayMasterList(HolidayFormBean holidayFormBean) {
        
        Hashtable ht = new Hashtable();

        try {
                ht.put("totalCount", this.holidayDAO.holidayMasterListCount(holidayFormBean));
                ht.put("ResultSet", this.holidayDAO.holidayMasterList(holidayFormBean));
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;
    }
     
    //코드 정보 삭제
    @Override
    @Transactional
    public void holidayMasterDelete(HolidayFormBean holidayFormBean) {
        
        holidayDAO.holidayMasterDelete(holidayFormBean);
        
    }
    
  

    //코드 정보 등록
    @Override
    @Transactional
    public void holidayMasterInsert(HolidayFormBean holidayFormBean) {
        
        holidayDAO.holidayMasterInsert(holidayFormBean);
        
    }
 

    //코드 정보 수정
    @Override
    @Transactional
    public String holidayMasterUpdate(HolidayFormBean holidayFormBean) {
         //조직코드 중복체크
        //Integer holiday_seq_cnt = holidayDAO.holidaymasterCheck(holidayFormBean);
        
        //if(holiday_seq_cnt == 1){
            //조직 등록
        Integer intReturn = holidayDAO.holidayMasterUpdate(holidayFormBean);
        //}

        String strReturnval = null;
        if(intReturn != null)
        {
            strReturnval = String.valueOf(intReturn.intValue());
        }
        else
        {
            strReturnval = "-1";
        }
        //return holiday_seq_cnt.toString(); 
        return strReturnval;
        
    } 

}

    

