/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.holiday.Bean;
import com.onoffkorea.system.common.util.CommonSortableListPagingForm;
/**
 *
 * @author Administrator
 */
public class HolidayFormBean extends CommonSortableListPagingForm {
    
     //휴일조회
    private String holiday_date_start   ;
    private String holiday_date_end   ;
    private String yyyymmdd;
    private String holidayname;
    

    public String getHoliday_date_start() {
        return holiday_date_start;
    }

    public void setHoliday_date_start(String holiday_date_start) {
        this.holiday_date_start = holiday_date_start;
    }

    public String getHoliday_date_end() {
        return holiday_date_end;
    }

    public void setHoliday_date_end(String holiday_date_end) {
        this.holiday_date_end = holiday_date_end;
    }

    public String getYyyymmdd() {
        return yyyymmdd;
    }

    public void setYyyymmdd(String yyyymmdd) {
        this.yyyymmdd = yyyymmdd;
    }

    public String getHolidayname() {
        return holidayname;
    }

    public void setHolidayname(String holidayname) {
        this.holidayname = holidayname;
    }    
                
}
