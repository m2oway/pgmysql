package com.onoffkorea.system.aop;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class LogAspect{
	private static Logger log = null;


//	@Around("execution(* com.itPlayers.*.Service..*.*(..)) || execution(* com.itPlayers.*.Dao..*.*(..))")
//	@Around("execution(* com.itPlayers.*.Dao..*.*(..))")
	public Object logTimeMethod(ProceedingJoinPoint joinPoint) throws Throwable {
			if (log == null) log = 	Logger.getLogger(joinPoint.getTarget().getClass());


			StringBuffer logMessage = new StringBuffer();
			log.debug(">>>>>>>>>>>> LogAspect Before: Method Signature <<<<<<<<<<<<<<<");
			logMessage.append(joinPoint.getTarget().getClass().getName());
			logMessage.append(".");
			logMessage.append(joinPoint.getSignature().getName());
			logMessage.append("(");
			// append args
			Object[] args = joinPoint.getArgs();
			for (int i = 0; i < args.length; i++) {
				logMessage.append(args[i]).append(",");
			}
			
			if (args.length > 0) {
				logMessage.deleteCharAt(logMessage.length() - 1);
			}
			
			logMessage.append(")");

			log.debug(logMessage.toString());
			log.debug(">>>>>>>>>>>> LogAspect End: Method Signature <<<<<<<<<<<<<<<");
			Object retVal = null;
			try
			{
				log.debug(">>>>>>>>>>>> LogAspect Begin: Call Method <<<<<<<<<<<<<<<");
				retVal = joinPoint.proceed();
				log.debug(">>>>>>>>>>>> LogAspect End: Call Method <<<<<<<<<<<<<<<");
			}
			catch(Exception ex)
			{
				log.error(">>>>>>>>>>>> LogAspect Begin: Exception <<<<<<<<<<<<<<<");
				log.error(">>>>>>>>>>>> Error Message : "+ex.getStackTrace());
				log.error(">>>>>>>>>>>> Error Message : "+ex.getMessage());
				log.error(">>>>>>>>>>>> LogAspect Begin: Exception <<<<<<<<<<<<<<<");				
			}
			finally
			{
				log.debug(">>>>>>>>>>>> LogAspect Begin: Return Value <<<<<<<<<<<<<<<");
				log.debug(retVal != null ? retVal.toString() : "No return value.");
				log.debug(">>>>>>>>>>>> LogAspect Begin: Return Value <<<<<<<<<<<<<<<");
			}
			return retVal;
	}

}
