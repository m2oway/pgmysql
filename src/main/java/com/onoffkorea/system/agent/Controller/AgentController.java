/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.agent.Controller;

import com.onoffkorea.system.agent.Bean.AgentFormBean;
import com.onoffkorea.system.agent.Service.AgentService;
import com.onoffkorea.system.agent.Vo.Tb_Agent;
import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.company.Bean.CompanyFormBean;
import com.onoffkorea.system.company.Service.CompanyService;
import com.onoffkorea.system.company.Vo.Tb_Company;
import com.onoffkorea.system.merchant.Bean.MerchantFormBean;
import com.onoffkorea.system.merchant.Service.MerchantService;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/agent/**")
public class AgentController {
    
    protected Log logger = LogFactory.getLog(getClass());
    
    @Autowired
    private CommonService commonService;
    
    @Autowired
    private AgentService agentService;  
    
    @Autowired
    private CompanyService companyService;      
    
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }   
    
    //대리점 가맹점 조회
    @RequestMapping(method= RequestMethod.GET,value = "/merchantAgentMasterList")
    public String merchantAgentMasterFormList(@Valid MerchantFormBean merchantFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        merchantFormBean.setAgentsearchflag("Y");
        
        merchantFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            merchantFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            merchantFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            merchantFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("BIZ_TYPE");
        Tb_Code_Main FuncCateList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("Biz_typeList",FuncCateList);
        
        commonCodeSearchFormBean.setMain_code("AGENCY_FLAG");
        Tb_Code_Main AgencyFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("AgencyFlagList",AgencyFlagList);
        
        commonCodeSearchFormBean.setMain_code("SVC_STAT");
        Tb_Code_Main UseFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("UseFlagList",UseFlagList);
        
        
        Hashtable ht_merchantMasterList = agentService.merchantMasterList(merchantFormBean);
        String ls_companyMasterList = (String) ht_merchantMasterList.get("ResultSet");

        model.addAttribute("onffmerchinfoMasterList",ls_companyMasterList);
        
        return null;
        
    }
    
    //대리점 가맹점 조회
    @RequestMapping(method= RequestMethod.POST,value = "/merchantAgentMasterList")
    @ResponseBody
    public String merchantAgentMasterList(@Valid MerchantFormBean merchantFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        merchantFormBean.setAgentsearchflag("Y");
        
        merchantFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            merchantFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            merchantFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            merchantFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        Hashtable ht_merchantMasterList = agentService.merchantMasterList(merchantFormBean);
        String ls_merchantMasterList = (String)ht_merchantMasterList.get("ResultSet");
     
        return ls_merchantMasterList;
        
    }            
    
    //대리점 조회
    @RequestMapping(method= RequestMethod.GET,value = "/agentMasterList")
    public String agentMasterFormList(@Valid AgentFormBean agentFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_agentMasterList = agentService.agentMasterList(agentFormBean);
        String ls_agentMasterList = (String) ht_agentMasterList.get("ResultSet");
        
        model.addAttribute("ls_agentMasterList",ls_agentMasterList);            
        
        return null;
        
    }
    
    //대리점 조회
    @RequestMapping(method= RequestMethod.POST,value = "/agentMasterList")
    @ResponseBody
    public String agentMasterList(@Valid AgentFormBean agentFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_agentMasterList = agentService.agentMasterList(agentFormBean);
        String ls_agentMasterList = (String) ht_agentMasterList.get("ResultSet");

        return ls_agentMasterList;
        
    }        
    
    //대리점 등록 화면
    @RequestMapping(method= RequestMethod.GET,value = "/agentMasterInsert")
    public String agentMasterFormInsert(@Valid AgentFormBean agentFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        commonCodeSearchFormBean.setMain_code("BANK_CD");
        Tb_Code_Main bankCdList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("bankCdList",bankCdList);        
        
        return null;
        
    }    
    
    //대리점 정보 등록
    @RequestMapping(method= RequestMethod.POST,value = "/agentMasterInsert")
    public @ResponseBody String agentMasterInsert(@Valid AgentFormBean agentFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        agentFormBean.setIns_user(siteSession.getUser_seq());
        String agent_seq = agentService.agentMasterInsert(agentFormBean);
        
        return agent_seq;
        
    }           
    
    //대리점 수정 화면
    @RequestMapping(method= RequestMethod.GET,value = "/agentMasterUpdate")
    public String agentMasterFormUpdate(@Valid AgentFormBean agentFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_agentMasterList = agentService.agentMasterList(agentFormBean);
        List<Tb_Agent> ls_agentMasterList = (List<Tb_Agent>) ht_agentMasterList.get("ls_agentMasterList");
        model.addAttribute("ls_agentMasterList",ls_agentMasterList);            
        
        CompanyFormBean companyFormBean = new CompanyFormBean();
        companyFormBean.setComp_seq(agentFormBean.getComp_seq());
        Hashtable ht_companyMasterList = companyService.companyMasterList(companyFormBean);
        List<Tb_Company> ls_companyMasterList = (List<Tb_Company>) ht_companyMasterList.get("ls_companyMasterList");
        model.addAttribute("ls_companyMasterList",ls_companyMasterList);            
        
        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        commonCodeSearchFormBean.setMain_code("BANK_CD");
        Tb_Code_Main bankCdList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("bankCdList",bankCdList);        
        
        return null;
        
    }        
    
    //대리점 정보 수정
    @RequestMapping(method= RequestMethod.POST,value = "/agentMasterUpdate")
    public @ResponseBody String agentMasterUpdate(@Valid AgentFormBean agentFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        agentFormBean.setMod_user(siteSession.getUser_seq());
        String agent_seq = agentService.agentMasterUpdate(agentFormBean);
        
        return agent_seq;
        
    }               
    
    //대리점 정보 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/agentMasterDelete")
    public @ResponseBody String agentMasterDelete(@Valid AgentFormBean agentFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        agentFormBean.setMod_user(siteSession.getUser_seq());
        String agent_seq = agentService.agentMasterDelete(agentFormBean);
        
        return agent_seq;
        
    }           
    
}
