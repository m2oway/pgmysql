/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.agent.Service;

import com.onoffkorea.system.agent.Bean.AgentFormBean;
import com.onoffkorea.system.merchant.Bean.MerchantFormBean;
import java.util.Hashtable;

/**
 *
 * @author Administrator
 */
public interface AgentService {
    
    public Hashtable merchantMasterList(MerchantFormBean merchantFormBean) throws Exception;
            
    public Hashtable agentMasterList(AgentFormBean agentFormBean);

    public String agentMasterInsert(AgentFormBean agentFormBean);

    public String agentMasterUpdate(AgentFormBean agentFormBean);

    public String agentMasterDelete(AgentFormBean agentFormBean);
    
}
