/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.agent.Service;

import com.onoffkorea.system.agent.Bean.AgentFormBean;
import com.onoffkorea.system.agent.Dao.AgentDAO;
import com.onoffkorea.system.agent.Vo.Tb_Agent;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.merchant.Bean.MerchantFormBean;
import com.onoffkorea.system.merchant.Dao.MerchantDAO;
import com.onoffkorea.system.merchant.Vo.Tb_Onffmerch_Mst;
import java.util.Hashtable;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service("agentService")
public class AgentServiceImpl implements AgentService{
    
    protected Logger logger = Logger.getLogger(getClass().getName());
    
    @Autowired
    private CommonService commonService;        

    @Autowired
    private AgentDAO agentDAO; 
    
    @Autowired
    private MerchantDAO merchantDAO;
    
    @Override
    public Hashtable merchantMasterList(MerchantFormBean merchantFormBean) throws Exception {
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
       
        String total_count = this.merchantDAO.merchantRecordCount(merchantFormBean);
       
        List ls_merchantMasterList = this.merchantDAO.merchantMasterList(merchantFormBean);
        
        logger.trace("-------pagesize-----------------");
        logger.trace("merchantFormBean getPage_size : " + merchantFormBean.getPage_size() );
        logger.trace("merchantFormBean getPage_no : " + merchantFormBean.getPage_no() );
        logger.trace("merchantFormBean getStart_no : " + merchantFormBean.getStart_no() );
        logger.trace("merchantFormBean getEnd_no: " + merchantFormBean.getEnd_no() );
        
        logger.trace("-------pagesize-----------------");
        
        sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(merchantFormBean.getStart_no())-1)+"\",\"rows\" : [");
        
        for (int i = 0; i < ls_merchantMasterList.size(); i++) {
                Tb_Onffmerch_Mst tb_Merchant = (Tb_Onffmerch_Mst) ls_merchantMasterList.get(i);
                
                //가맹점ID, 가맹점명, 사업자명,개인법인구분,사업자번호, 법인번호, 대표자, 연락처1, 결제한도, 대리점, 상태
                sb.append("{\"id\":" + i);
                sb.append(",\"userdata\":{");
                sb.append("\"onffmerch_no\":\""+tb_Merchant.getOnffmerch_no()+"\"");
                sb.append("}");                
                sb.append(",\"data\":[");
                sb.append(" \"" + tb_Merchant.getRnum() + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getAgent_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getAgent_commission()) + "\"");                
                sb.append(",\"" + Util.nullToString(tb_Merchant.getOnffmerch_no()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getMerch_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getComp_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getComp_cate_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getBiz_no()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getCorp_no()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getComp_ceo_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getTel_1()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Merchant.getSvc_stat_nm()) + "\"");
                
                if (i == (ls_merchantMasterList.size() - 1)) {
                        sb.append("]}");
                } else {
                        sb.append("]},");
                }
        }

        sb.append("]}");    
        
        ht.put("ResultSet", sb.toString());
        
        return ht;
    }    

    //대리점 조회
    @Override
    public Hashtable agentMasterList(AgentFormBean agentFormBean) {

        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = agentDAO.agentMasterListCount(agentFormBean);
                List<Tb_Agent> ls_agentMasterList = agentDAO.agentMasterList(agentFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(agentFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_agentMasterList.size(); i++) {
                        Tb_Agent tb_Agent = ls_agentMasterList.get(i);
                        sb.append("{\"id\":" + tb_Agent.getAgent_seq());
                        sb.append(",\"userdata\":{");
                        sb.append(" \"comp_seq\":\""+tb_Agent.getComp_seq()+"\"");
                        sb.append("}");                                    
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Agent.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Agent.getAgent_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Agent.getBiz_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Agent.getCorp_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Agent.getComp_ceo_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Agent.getTel_1()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Agent.getTel_2()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Agent.getUse_flag_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Agent.getIns_dt()) + "\"");

                        if (i == (ls_agentMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                ht.put("ResultSet", sb.toString());
                ht.put("ls_agentMasterList", ls_agentMasterList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;                    
        
    }

    //대리점 등록
    @Override
    @Transactional
    public String agentMasterInsert(AgentFormBean agentFormBean) {
        
        return this.agentDAO.agentMasterInsert(agentFormBean);
        
    }
    
    //대리점 수정
    @Override
    @Transactional
    public String agentMasterUpdate(AgentFormBean agentFormBean) {
        
        return this.agentDAO.agentMasterUpdate(agentFormBean);
        
    }    
    
    //대리점 삭제
    @Override
    @Transactional
    public String agentMasterDelete(AgentFormBean agentFormBean) {
        
        return this.agentDAO.agentMasterDelete(agentFormBean);
        
    }        
    
}
