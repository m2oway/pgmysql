/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.agent.Bean;

import com.onoffkorea.system.common.util.CommonSortableListPagingForm;

/**
 *
 * @author Administrator
 */
public class AgentFormBean extends CommonSortableListPagingForm{
    
    private String agent_seq   ;
    private String comp_seq    ;
    private String comp_nm    ;
    private String agent_nm    ;
    private String zip_cd      ;
    private String addr_1      ;
    private String addr_2      ;
    private String tel_1       ;
    private String tel_2       ;
    private String bal_dt      ;
    private String commision   ;
    private String bank_cd     ;
    private String bank_cd_nm     ;
    private String acc_no      ;
    private String acc_ownner  ;
    private String memo        ;
    private String del_flag    ;
    private String use_flag    ;
    private String ins_dt      ;
    private String mod_dt      ;
    private String ins_user    ;
    private String mod_user    ;    
    private String user_seq;
    private String biz_no; 
    private String corp_no;
    private String comp_biz_no;
    private String comp_comp_cate;
    private String comp_corp_no;
    private String comp_comp_ceo_nm;
    private String comp_comp_tel1;
    private String comp_comp_tel2;
    private String comp_zip_code;
    private String comp_addr_1;
    private String comp_addr_2;

    public String getComp_biz_no() {
        return comp_biz_no;
    }

    public void setComp_biz_no(String comp_biz_no) {
        this.comp_biz_no = comp_biz_no;
    }

    public String getComp_comp_cate() {
        return comp_comp_cate;
    }

    public void setComp_comp_cate(String comp_comp_cate) {
        this.comp_comp_cate = comp_comp_cate;
    }

    public String getComp_corp_no() {
        return comp_corp_no;
    }

    public void setComp_corp_no(String comp_corp_no) {
        this.comp_corp_no = comp_corp_no;
    }

    public String getComp_comp_ceo_nm() {
        return comp_comp_ceo_nm;
    }

    public void setComp_comp_ceo_nm(String comp_comp_ceo_nm) {
        this.comp_comp_ceo_nm = comp_comp_ceo_nm;
    }

    public String getComp_comp_tel1() {
        return comp_comp_tel1;
    }

    public void setComp_comp_tel1(String comp_comp_tel1) {
        this.comp_comp_tel1 = comp_comp_tel1;
    }

    public String getComp_comp_tel2() {
        return comp_comp_tel2;
    }

    public void setComp_comp_tel2(String comp_comp_tel2) {
        this.comp_comp_tel2 = comp_comp_tel2;
    }

    public String getComp_zip_code() {
        return comp_zip_code;
    }

    public void setComp_zip_code(String comp_zip_code) {
        this.comp_zip_code = comp_zip_code;
    }

    public String getComp_addr_1() {
        return comp_addr_1;
    }

    public void setComp_addr_1(String comp_addr_1) {
        this.comp_addr_1 = comp_addr_1;
    }

    public String getComp_addr_2() {
        return comp_addr_2;
    }

    public void setComp_addr_2(String comp_addr_2) {
        this.comp_addr_2 = comp_addr_2;
    }

    public String getBiz_no() {
        return biz_no;
    }

    public void setBiz_no(String biz_no) {
        this.biz_no = biz_no;
    }

    public String getCorp_no() {
        return corp_no;
    }

    public void setCorp_no(String corp_no) {
        this.corp_no = corp_no;
    }

    public String getAgent_seq() {
        return agent_seq;
    }

    public void setAgent_seq(String agent_seq) {
        this.agent_seq = agent_seq;
    }

    public String getComp_seq() {
        return comp_seq;
    }

    public void setComp_seq(String comp_seq) {
        this.comp_seq = comp_seq;
    }

    public String getComp_nm() {
        return comp_nm;
    }

    public void setComp_nm(String comp_nm) {
        this.comp_nm = comp_nm;
    }

    public String getAgent_nm() {
        return agent_nm;
    }

    public void setAgent_nm(String agent_nm) {
        this.agent_nm = agent_nm;
    }

    public String getZip_cd() {
        return zip_cd;
    }

    public void setZip_cd(String zip_cd) {
        this.zip_cd = zip_cd;
    }

    public String getAddr_1() {
        return addr_1;
    }

    public void setAddr_1(String addr_1) {
        this.addr_1 = addr_1;
    }

    public String getAddr_2() {
        return addr_2;
    }

    public void setAddr_2(String addr_2) {
        this.addr_2 = addr_2;
    }

    public String getTel_1() {
        return tel_1;
    }

    public void setTel_1(String tel_1) {
        this.tel_1 = tel_1;
    }

    public String getTel_2() {
        return tel_2;
    }

    public void setTel_2(String tel_2) {
        this.tel_2 = tel_2;
    }

    public String getBal_dt() {
        return bal_dt;
    }

    public void setBal_dt(String bal_dt) {
        this.bal_dt = bal_dt;
    }

    public String getCommision() {
        return commision;
    }

    public void setCommision(String commision) {
        this.commision = commision;
    }

    public String getBank_cd() {
        return bank_cd;
    }

    public void setBank_cd(String bank_cd) {
        this.bank_cd = bank_cd;
    }

    public String getBank_cd_nm() {
        return bank_cd_nm;
    }

    public void setBank_cd_nm(String bank_cd_nm) {
        this.bank_cd_nm = bank_cd_nm;
    }

    public String getAcc_no() {
        return acc_no;
    }

    public void setAcc_no(String acc_no) {
        this.acc_no = acc_no;
    }

    public String getAcc_ownner() {
        return acc_ownner;
    }

    public void setAcc_ownner(String acc_ownner) {
        this.acc_ownner = acc_ownner;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getDel_flag() {
        return del_flag;
    }

    public void setDel_flag(String del_flag) {
        this.del_flag = del_flag;
    }

    public String getUse_flag() {
        return use_flag;
    }

    public void setUse_flag(String use_flag) {
        this.use_flag = use_flag;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    public String getUser_seq() {
        return user_seq;
    }

    public void setUser_seq(String user_seq) {
        this.user_seq = user_seq;
    }

    @Override
    public String toString() {
        return "AgentFormBean{" + "agent_seq=" + agent_seq + ", comp_seq=" + comp_seq + ", comp_nm=" + comp_nm + ", agent_nm=" + agent_nm + ", zip_cd=" + zip_cd + ", addr_1=" + addr_1 + ", addr_2=" + addr_2 + ", tel_1=" + tel_1 + ", tel_2=" + tel_2 + ", bal_dt=" + bal_dt + ", commision=" + commision + ", bank_cd=" + bank_cd + ", bank_cd_nm=" + bank_cd_nm + ", acc_no=" + acc_no + ", acc_ownner=" + acc_ownner + ", memo=" + memo + ", del_flag=" + del_flag + ", use_flag=" + use_flag + ", ins_dt=" + ins_dt + ", mod_dt=" + mod_dt + ", ins_user=" + ins_user + ", mod_user=" + mod_user + ", user_seq=" + user_seq + ", biz_no=" + biz_no + ", corp_no=" + corp_no + '}';
    }
    
}
