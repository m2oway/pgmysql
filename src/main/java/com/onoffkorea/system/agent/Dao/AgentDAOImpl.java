/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.agent.Dao;

import com.onoffkorea.system.agent.Bean.AgentFormBean;
import com.onoffkorea.system.agent.Vo.Tb_Agent;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("agentDAO")
public class AgentDAOImpl extends SqlSessionDaoSupport implements AgentDAO{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    //대리점 조회 카운트
    @Override
    public Integer agentMasterListCount(AgentFormBean agentFormBean) {
        
        return (Integer)getSqlSession().selectOne("Agent.agentMasterListCount",agentFormBean);
        
    }

    //대리점 조회
    @Override
    public List<Tb_Agent> agentMasterList(AgentFormBean agentFormBean) {
        
        return (List)getSqlSession().selectList("Agent.agentMasterList",agentFormBean);
        
    }

    //대리점 등록
    @Override
    public String agentMasterInsert(AgentFormBean agentFormBean) {
        
        Integer result = getSqlSession().insert("Agent.agentMasterInsert", agentFormBean);
        return result.toString();
        
    }
    
    //대리점 수정
    @Override
    public String agentMasterUpdate(AgentFormBean agentFormBean) {
        
        Integer result = getSqlSession().update("Agent.agentMasterUpdate", agentFormBean);
        return result.toString();
        
    }    
    
    //대리점 삭제
    @Override
    public String agentMasterDelete(AgentFormBean agentFormBean) {
        
        Integer result = getSqlSession().update("Agent.agentMasterDelete", agentFormBean);
        return result.toString();
        
    }        
    
}
