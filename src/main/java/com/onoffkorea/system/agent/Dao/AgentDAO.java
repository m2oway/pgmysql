/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.agent.Dao;

import com.onoffkorea.system.agent.Bean.AgentFormBean;
import com.onoffkorea.system.agent.Vo.Tb_Agent;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface AgentDAO {

    public Integer agentMasterListCount(AgentFormBean agentFormBean);

    public List<Tb_Agent> agentMasterList(AgentFormBean agentFormBean);

    public String agentMasterInsert(AgentFormBean agentFormBean);

    public String agentMasterUpdate(AgentFormBean agentFormBean);

    public String agentMasterDelete(AgentFormBean agentFormBean);
    
}
