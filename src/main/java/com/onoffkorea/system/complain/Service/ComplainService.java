/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.complain.Service;

import com.onoffkorea.system.complain.Bean.ComplainFormBean;
import com.onoffkorea.system.trans.Bean.TransFormBean;
import java.util.Hashtable;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public interface ComplainService {

    public Hashtable complainMasterList(ComplainFormBean complainFormBean);
    
    public Hashtable basic_complainMasterList(ComplainFormBean complainFormBean);

    public Hashtable transMasterList(TransFormBean transFormBean);

    public void complainMasterInsert(ComplainFormBean complainFormBean);
    
    public Hashtable complainMasterInsertForm(ComplainFormBean complainFormBean);

    public void complainMasterUpdate(ComplainFormBean complainFormBean);

    public void complainMasterDelete(ComplainFormBean complainFormBean);

    public Hashtable complainDetailList(ComplainFormBean complainFormBean);

    public void complainDetailInsert(ComplainFormBean complainFormBean);

    public void complainDetailUpdate(ComplainFormBean complainFormBean);

    public void complainDetailDelete(ComplainFormBean complainFormBean);

    public void complainMasterUpdateComplete(ComplainFormBean complainFormBean);
    
    public void complainMasterUpdateUnComplete(ComplainFormBean complainFormBean);
    
    public void complainMasterListExcel(ComplainFormBean complainFormBean, String excelDownloadFilename, HttpServletResponse response); 
    
    public void basic_complainMasterListExcel(ComplainFormBean complainFormBean, String excelDownloadFilename, HttpServletResponse response); 
    
    
}
