/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.complain.Service;

import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.complain.Bean.ComplainFormBean;
import com.onoffkorea.system.complain.Dao.ComplainDAO;
import com.onoffkorea.system.complain.Vo.Tb_Complain;
import com.onoffkorea.system.trans.Bean.TransFormBean;
import com.onoffkorea.system.trans.Dao.TransDAO;
import com.onoffkorea.system.trans.Vo.Tb_Tran_Cardpg;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service("complainService")
public class ComplainServiceImpl implements ComplainService{
 
    protected Logger logger = Logger.getLogger(getClass().getName());
    
    @Autowired
    private CommonService commonService;        

    @Autowired
    private ComplainDAO complainDAO;       
    
    @Autowired
    private TransDAO transDAO;
    
    
        //민원 관리 조회
    @Override
    public Hashtable basic_complainMasterList(ComplainFormBean complainFormBean) {
        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        
        if(!"Y".equals(complainFormBean.getComplain_popup_yn())){
           
            if(!"Y".equals(complainFormBean.getUpdatepopup()))
            {
                if(complainFormBean.getComplete_flag() == null || "".equals(complainFormBean.getComplete_flag()))
                {
                     complainFormBean.setComplete_flag("N");
                }
            }
            
        }

        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = complainDAO.complainMasterListCount(complainFormBean);
                List<Tb_Complain> ls_complainMasterList = complainDAO.complainMasterList(complainFormBean);
                
                  ht.put("total_count", String.valueOf(total_count.intValue()));

                ht.put("ls_complainMasterList", ls_complainMasterList);
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
                commonCodeSearchFormBean.setMain_code("COMPLETE_FLAG");
                Tb_Code_Main completeFlagList = commonService.codeSearch(commonCodeSearchFormBean);
                
                ht.put("completeFlagList", completeFlagList);                
                
                
                
                CommonCodeSearchFormBean commonCodeSearchFormBean2 = new CommonCodeSearchFormBean();
        
                commonCodeSearchFormBean2.setMain_code("COMPLAIN_CATE");
                Tb_Code_Main complainCateList = commonService.codeSearch(commonCodeSearchFormBean2);
                
                ht.put("complainCateList", complainCateList);                                
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;             
        
    }

    //민원 관리 조회
    @Override
    public Hashtable complainMasterList(ComplainFormBean complainFormBean) {
        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        
        if(!"Y".equals(complainFormBean.getComplain_popup_yn())){
            /*
            if((complainFormBean.getIns_start_dt() == null) || (complainFormBean.getIns_end_dt() == "")){
                Date now = new Date();
                Calendar cal = Calendar.getInstance();
                cal.setTime(now);
                cal.add(Calendar.DAY_OF_WEEK, -6);


                Date currentTime = new Date ( );
                String start_dt = formatter.format ( cal.getTime());
                String end_dt = formatter.format(currentTime.getTime());


                complainFormBean.setIns_start_dt(start_dt);
                complainFormBean.setIns_end_dt(end_dt);

                complainFormBean.setComplete_flag("N");

            }    
            */
            if(!"Y".equals(complainFormBean.getUpdatepopup()))
            {
                if(complainFormBean.getComplete_flag() == null || "".equals(complainFormBean.getComplete_flag()))
                {
                     complainFormBean.setComplete_flag("N");
                }
            }
            
        }

        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = complainDAO.complainMasterListCount(complainFormBean);
                List<Tb_Complain> ls_complainMasterList = complainDAO.complainMasterList(complainFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(complainFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_complainMasterList.size(); i++) {
                        Tb_Complain tb_Complain = ls_complainMasterList.get(i);
                        sb.append("{\"id\":" + tb_Complain.getComplain_seq());
                        sb.append(",\"userdata\":{");
                        sb.append(" \"complain_seq\":\""+tb_Complain.getComplain_seq()+"\"");
                        sb.append(",\"tran_seq\":\""+tb_Complain.getTran_seq()+"\"");
                        sb.append(",\"complete_flag\":\""+tb_Complain.getComplete_flag()+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Complain.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Complain.getOnffmerch_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Complain.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Complain.getOnfftid()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Complain.getOnfftid_nm()) + "\"");
                        
                        sb.append(",\"" + Util.nullToString(tb_Complain.getTel_1()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Complain.getTel_2()) + "\"");
                        
                        //sb.append(",\"" + Util.nullToString(tb_Complain.getTitle()) + "\"");
                        //sb.append(",\"" + Util.nullToString(tb_Complain.getContent()) + "\"");
                        //String strTpContent = Util.nullToString(tb_Complain.getContent());                        
                        
                        //strTpContent = strTpContent.replaceAll("\r\n", "<br>");
                        //strTpContent = strTpContent.replaceAll("\n", "<br>");
                        //sb.append(",\"" + strTpContent + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Complain.getComplain_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Complain.getCust_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Complain.getCust_tel()) + "\"");
                        
                        //sb.append(",\"" + Util.nullToString(tb_Complain.getCard_num()) + "\"");
                        String strTpUseCate = Util.nullToString(complainFormBean.getSes_user_cate());
                        
                        
                        if(strTpUseCate.equals("01") || strTpUseCate.equals("02") || strTpUseCate.equals("03"))
                        {
                            String strTpCardNum  = Util.nullToString(tb_Complain.getCard_num()).trim();
                        
                            if(!strTpCardNum.equals(""))
                            {
                                sb.append(",\"" + strTpCardNum.substring(0, 8) + "****" + strTpCardNum.substring(12, strTpCardNum.length()) + "\"");
                            }
                            else
                            {
                                sb.append(",\"" + Util.nullToString(strTpCardNum) + "\"");
                            }
                        }
                        else
                        {
                            sb.append(",\"" + Util.nullToString(tb_Complain.getCard_num()) + "\"");
                        }
                        
                        
                        sb.append(",\"" + Util.nullToString(tb_Complain.getInstallment()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Complain.getApp_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Complain.getApp_dt())+ Util.nullToString(tb_Complain.getApp_tm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Complain.getTot_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Complain.getCncl_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Complain.getTran_status_nm()) + "\"");                        
                        sb.append(",\"" + Util.nullToString(tb_Complain.getIns_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Complain.getStatus_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Complain.getComplete_flag_nm()) + "\"");
                        

                        if (i == (ls_complainMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");     

                if(!"Y".equals(complainFormBean.getComplain_popup_yn())){
                    /*
                    SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
                    Date start_date = formatter.parse(complainFormBean.getIns_start_dt());
                    Date end_date   = formatter.parse(complainFormBean.getIns_end_dt());

                    String start_dt = formatter2.format(start_date);
                    String end_dt = formatter2.format(end_date);

                    complainFormBean.setIns_start_dt(start_dt);
                    complainFormBean.setIns_end_dt(end_dt);                
                    */
                }
            
                ht.put("ResultSet", sb.toString());
                ht.put("ls_complainMasterList", ls_complainMasterList);
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
                commonCodeSearchFormBean.setMain_code("COMPLETE_FLAG");
                Tb_Code_Main completeFlagList = commonService.codeSearch(commonCodeSearchFormBean);
                
                ht.put("completeFlagList", completeFlagList);                
                
                
                
                CommonCodeSearchFormBean commonCodeSearchFormBean2 = new CommonCodeSearchFormBean();
        
                commonCodeSearchFormBean2.setMain_code("COMPLAIN_CATE");
                Tb_Code_Main complainCateList = commonService.codeSearch(commonCodeSearchFormBean2);
                
                ht.put("complainCateList", complainCateList);                                
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;             
        
    }

    //거래내역 조회
    @Override
    public Hashtable transMasterList(TransFormBean transFormBean) {

        Hashtable ht = new Hashtable();

        try {
            
                List<Tb_Tran_Cardpg> ls_transDetailList = transDAO.transDetailList(transFormBean);
                
                String strTpUserCate = Util.nullToString(transFormBean.getSes_user_cate());
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"rows\" : [");
                for (int i = 0; i < ls_transDetailList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_transDetailList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"data\":[");
                        
                        //sb.append(" \"" + Util.nullToString(tb_Tran_Cardpg.getOnfftid()) + "\"");
                        sb.append(" \"" + Util.nullToString(tb_Tran_Cardpg.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnfftid_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMassagetype_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_chn_cate_nm()) + "\"");
                        /*
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTid()) + "\"");
                        */
                        
                        if(strTpUserCate.equals("00"))
                        {
                            sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCard_num()) + "\"");
                        }
                        else
                        {
                            String strTpCardNum = Util.nullToString(tb_Tran_Cardpg.getCard_num()).trim();
                            
                            if(!strTpCardNum.equals(""))
                            {
                                String strTpViewCardNum = strTpCardNum.substring(0, 8) + "****" + strTpCardNum.substring(12 , strTpCardNum.length());

                                sb.append(",\"" + strTpViewCardNum + "\"");
                            }
                            else
                            {
                                sb.append(",\"\"");
                            }
                        }
                       
                        
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_dt()) + Util.nullToString(tb_Tran_Cardpg.getApp_tm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTot_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getInstallment()) + "\"");
                        /*
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_no()) + "\"");
                        */
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getIss_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_status_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_phone2()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getProduct_nm()) + "\"");                        

                        if (i == (ls_transDetailList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                ht.put("ResultSet", sb.toString());
                ht.put("ls_transDetailList", ls_transDetailList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;            
        
    }

    //민원 등록
    @Override
    @Transactional
    public void complainMasterInsert(ComplainFormBean complainFormBean) {
        
        complainDAO.complainMasterInsert(complainFormBean);
        
    }
    
    //민원 등록
    @Override
    public Hashtable complainMasterInsertForm(ComplainFormBean complainFormBean) {
      
        Hashtable ht = new Hashtable();

        try {
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
                commonCodeSearchFormBean.setMain_code("COMPLETE_FLAG");
                Tb_Code_Main completeFlagList = commonService.codeSearch(commonCodeSearchFormBean);
                
                ht.put("completeFlagList", completeFlagList);                
                
                CommonCodeSearchFormBean commonCodeSearchFormBean2 = new CommonCodeSearchFormBean();
        
                commonCodeSearchFormBean2.setMain_code("COMPLAIN_CATE");
                Tb_Code_Main complainCateList = commonService.codeSearch(commonCodeSearchFormBean2);
                
                ht.put("complainCateList", complainCateList);                                
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;      
        
    }    

    //민원 수정
    @Override
    @Transactional
    public void complainMasterUpdate(ComplainFormBean complainFormBean) {
        
        complainDAO.complainMasterUpdate(complainFormBean);
        
    }
    
    //민원 삭제
    @Override
    @Transactional
    public void complainMasterDelete(ComplainFormBean complainFormBean) {

        complainDAO.complainMasterDelete(complainFormBean);
        
    }
    
    //민원 응답내역 조회
    @Override
    public Hashtable complainDetailList(ComplainFormBean complainFormBean) {

        Hashtable ht = new Hashtable();

        try {
            
                List<Tb_Complain> ls_complainDetailList = complainDAO.complainDetailList(complainFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"rows\" : [");
                for (int i = 0; i < ls_complainDetailList.size(); i++) {
                        Tb_Complain tb_Complain = ls_complainDetailList.get(i);
                        sb.append("{\"id\":" + tb_Complain.getComplain_seq());
                        sb.append(",\"userdata\":{");
                        sb.append(" \"complain_seq\":\""+tb_Complain.getComplain_seq()+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Complain.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Complain.getCust_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Complain.getIns_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Complain.getTitle()) + "\"");
                        
                        //sb.append(",\"" + Util.nullToString(tb_Complain.getContent()) + "\"");
                        String strTpContents = Util.nullToString(tb_Complain.getContent());
                        
                        strTpContents =  strTpContents.replaceAll("\\n", "<br>");
                        //System.out.println("xxxx["+ strTpContents + "]");
                        
                        strTpContents =  strTpContents.replaceAll("\\r", "");
                        //System.out.println("xxxx2["+ strTpContents + "]");
                        
                        sb.append(",\"" + strTpContents + "\"");
                        
                        if (i == (ls_complainDetailList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                ht.put("ResultSet", sb.toString());
                ht.put("ls_complainDetailList", ls_complainDetailList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;             
        
    }    
    
    //응답 등록
    @Override
    @Transactional
    public void complainDetailInsert(ComplainFormBean complainFormBean) {
        
        complainDAO.complainDetailInsert(complainFormBean);
        
        Integer complain_cnt = complainDAO.complainDetailCount(complainFormBean);
        if(complain_cnt > 0){
            complainFormBean.setStatus("1");
        }else{
            complainFormBean.setStatus("0");
        }
        
        complainDAO.complainMasterStatUpdate(complainFormBean);
        
    }    
    
    //응답 수정
    @Override
    @Transactional
    public void complainDetailUpdate(ComplainFormBean complainFormBean) {
        
        complainDAO.complainDetailUpdate(complainFormBean);
        
    }      
    
    //응답 삭제
    @Override
    @Transactional
    public void complainDetailDelete(ComplainFormBean complainFormBean) {
        
        complainDAO.complainDetailDelete(complainFormBean); 
        
        Integer complain_cnt = complainDAO.complainDetailCount(complainFormBean);
        if(complain_cnt > 0){
            complainFormBean.setStatus("1");
        }else{
            complainFormBean.setStatus("0");
        }        
        
        complainDAO.complainMasterStatUpdate(complainFormBean);
        
    }      
    
    //민원 완료
    @Override
    @Transactional
    public void complainMasterUpdateComplete(ComplainFormBean complainFormBean) {
         
        complainDAO.complainMasterUpdateComplete(complainFormBean);
        
    }    
    
    
    
    //민원 완료
    @Override
    @Transactional
    public void complainMasterUpdateUnComplete(ComplainFormBean complainFormBean) {
         
        complainDAO.complainMasterUpdateUnComplete(complainFormBean);
        
    }     
    
    @Override
    public void complainMasterListExcel(ComplainFormBean complainFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
             
                List ls_complainMasterListExcel = complainDAO.complainMasterListExcel(complainFormBean);
                
                StringBuffer complainMasterListExcel = Util.makeData(ls_complainMasterListExcel);
        
                Util.exceldownload(complainMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }
        
    }          
    
    
        
    @Override
    public void basic_complainMasterListExcel(ComplainFormBean complainFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
             
                List ls_complainMasterListExcel = complainDAO.basic_complainMasterListExcel(complainFormBean);
                
                StringBuffer complainMasterListExcel = Util.makeData(ls_complainMasterListExcel);
        
                Util.exceldownload(complainMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }
        
    }          
    
}
