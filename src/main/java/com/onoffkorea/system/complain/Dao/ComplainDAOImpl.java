/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.complain.Dao;

import com.onoffkorea.system.complain.Bean.ComplainFormBean;
import com.onoffkorea.system.complain.Vo.Tb_Complain;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("complainDAO")
public class ComplainDAOImpl extends SqlSessionDaoSupport implements ComplainDAO{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    //민원 관리 조회 카운트
    @Override
    public Integer complainMasterListCount(ComplainFormBean complainFormBean) {
        
        return (Integer)getSqlSession().selectOne("Complain.complainMasterListCount",complainFormBean);
        
    }

    //민원 관리 조회
    @Override
    public List<Tb_Complain> complainMasterList(ComplainFormBean complainFormBean) {
        
        return (List)getSqlSession().selectList("Complain.complainMasterList",complainFormBean);
        
    }

    //민원 등록
    @Override
    public void complainMasterInsert(ComplainFormBean complainFormBean) {
        
        getSqlSession().insert("Complain.complainMasterInsert", complainFormBean);
        
    }
    
    //민원 수정
    @Override
    public void complainMasterUpdate(ComplainFormBean complainFormBean) {
        
        getSqlSession().update("Complain.complainMasterUpdate", complainFormBean);
        
    }    
    
    //민원 삭제
    @Override
    public void complainMasterDelete(ComplainFormBean complainFormBean) {
        
        getSqlSession().update("Complain.complainMasterDelete", complainFormBean);
        
    }        
    
    //응답내역 조회
    @Override
    public List<Tb_Complain> complainDetailList(ComplainFormBean complainFormBean) {
        
        return (List)getSqlSession().selectList("Complain.complainDetailList",complainFormBean);
        
    }    
    
    //응답 등록
    @Override
    public void complainDetailInsert(ComplainFormBean complainFormBean) {
        
        getSqlSession().insert("Complain.complainDetailInsert", complainFormBean);
        
    }    
    
    //응답 수정
    @Override
    public void complainDetailUpdate(ComplainFormBean complainFormBean) {
        
        getSqlSession().update("Complain.complainDetailUpdate", complainFormBean);
        
    }        
    
    //응답 삭제
    @Override
    public void complainDetailDelete(ComplainFormBean complainFormBean) {
        
        getSqlSession().update("Complain.complainDetailDelete", complainFormBean); 
        
    }        
    
    //응답 조회 카운트
    @Override
    public Integer complainDetailCount(ComplainFormBean complainFormBean) {
        
        return (Integer)getSqlSession().selectOne("Complain.complainDetailCount",complainFormBean);
        
    }    
    
    //민원 상태 수정
    @Override
    public void complainMasterStatUpdate(ComplainFormBean complainFormBean) {
        
        getSqlSession().update("Complain.complainMasterStatUpdate", complainFormBean); 
        
    }          
    
    //민원 완료
    @Override
    public void complainMasterUpdateComplete(ComplainFormBean complainFormBean) {
         
        getSqlSession().update("Complain.complainMasterUpdateComplete", complainFormBean);
        
    }        
    
    
    //민원 완료
    @Override
    public void complainMasterUpdateUnComplete(ComplainFormBean complainFormBean) {
         
        getSqlSession().update("Complain.complainMasterUpdateUnComplete", complainFormBean);
        
    }        
    
    @Override
    public List complainMasterListExcel(ComplainFormBean complainFormBean)
    {
        return (List)getSqlSession().selectList("Complain.complainMasteListExcel",complainFormBean);
    }
    
    
        @Override
    public List basic_complainMasterListExcel(ComplainFormBean complainFormBean)
    {
        return (List)getSqlSession().selectList("Complain.basic_complainMasteListExcel",complainFormBean);
    }
        
        
}
