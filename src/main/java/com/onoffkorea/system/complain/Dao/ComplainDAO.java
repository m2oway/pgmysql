/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.complain.Dao;

import com.onoffkorea.system.complain.Bean.ComplainFormBean;
import com.onoffkorea.system.complain.Vo.Tb_Complain;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface ComplainDAO {

    public Integer complainMasterListCount(ComplainFormBean complainFormBean);

    public List<Tb_Complain> complainMasterList(ComplainFormBean complainFormBean);

    public void complainMasterInsert(ComplainFormBean complainFormBean);

    public void complainMasterUpdate(ComplainFormBean complainFormBean);

    public void complainMasterDelete(ComplainFormBean complainFormBean);

    public List<Tb_Complain> complainDetailList(ComplainFormBean complainFormBean);

    public void complainDetailInsert(ComplainFormBean complainFormBean);

    public void complainDetailUpdate(ComplainFormBean complainFormBean);

    public void complainDetailDelete(ComplainFormBean complainFormBean);

    public Integer complainDetailCount(ComplainFormBean complainFormBean);

    public void complainMasterStatUpdate(ComplainFormBean complainFormBean);

    public void complainMasterUpdateComplete(ComplainFormBean complainFormBean);
    
    public void complainMasterUpdateUnComplete(ComplainFormBean complainFormBean);
    
    public List complainMasterListExcel(ComplainFormBean complainFormBean);   
    
    public List basic_complainMasterListExcel(ComplainFormBean complainFormBean);   
    
}
