/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.complain.Bean;

import com.onoffkorea.system.common.util.CommonSortableListPagingForm;

/**
 *
 * @author Administrator
 */
public class ComplainFormBean extends CommonSortableListPagingForm{
    
    private String complain_seq  ;
    private String title         ;
    private String cust_nm       ;
    private String cust_tel      ;
    private String content       ;
    private String onfftid       ;
    private String pay_type      ;
    private String tran_seq      ;
    private String br_level      ;
    private String parent_seq    ;
    private String status        ;
    private String del_flag      ;
    private String ins_dt        ;
    private String mod_dt        ;
    private String ins_user      ;
    private String mod_user      ;    
    private String ins_start_dt;
    private String ins_end_dt;
    private String onffmerch_no;
    private String merch_nm;
    private String complain_popup_yn;
    private String parent_complain_seq;
    private String onfftid_nm;
    private String complete_flag;
    private String complete_flag_nm;
    
    
    private String complain_cate;
    private String complain_cate_nm;    
    private String card_num;
    private String app_dt;
    private String app_tm;
    private String app_no;
    private String org_app_no;
    private String org_app_dt;
    private String tot_amt;
    private String cncl_dt;
    private String tran_status_nm;
    private String user_nm;
    private String user_phone1;
    private String product_nm;

    private String installment;   
    
    private String updatepopup;
    
    private String grid_nm;

    public String getGrid_nm() {
        return grid_nm;
    }

    public void setGrid_nm(String grid_nm) {
        this.grid_nm = grid_nm;
    }
    
    
    
    
    public String getUpdatepopup() {
        return updatepopup;
    }

    public void setUpdatepopup(String updatepopup) {
        this.updatepopup = updatepopup;
    }

    
    

    public String getComplain_cate() {
        return complain_cate;
    }

    public void setComplain_cate(String complain_cate) {
        this.complain_cate = complain_cate;
    }

    public String getComplain_cate_nm() {
        return complain_cate_nm;
    }

    public void setComplain_cate_nm(String complain_cate_nm) {
        this.complain_cate_nm = complain_cate_nm;
    }

    public String getCard_num() {
        return card_num;
    }

    public void setCard_num(String card_num) {
        this.card_num = card_num;
    }

    public String getApp_dt() {
        return app_dt;
    }

    public void setApp_dt(String app_dt) {
        this.app_dt = app_dt;
    }

    public String getApp_tm() {
        return app_tm;
    }

    public void setApp_tm(String app_tm) {
        this.app_tm = app_tm;
    }

    public String getApp_no() {
        return app_no;
    }

    public void setApp_no(String app_no) {
        this.app_no = app_no;
    }

    public String getOrg_app_no() {
        return org_app_no;
    }

    public void setOrg_app_no(String org_app_no) {
        this.org_app_no = org_app_no;
    }

    public String getOrg_app_dt() {
        return org_app_dt;
    }

    public void setOrg_app_dt(String org_app_dt) {
        this.org_app_dt = org_app_dt;
    }

    public String getTot_amt() {
        return tot_amt;
    }

    public void setTot_amt(String tot_amt) {
        this.tot_amt = tot_amt;
    }

    public String getCncl_dt() {
        return cncl_dt;
    }

    public void setCncl_dt(String cncl_dt) {
        this.cncl_dt = cncl_dt;
    }

    public String getTran_status_nm() {
        return tran_status_nm;
    }

    public void setTran_status_nm(String tran_status_nm) {
        this.tran_status_nm = tran_status_nm;
    }

    public String getUser_nm() {
        return user_nm;
    }

    public void setUser_nm(String user_nm) {
        this.user_nm = user_nm;
    }

    public String getUser_phone1() {
        return user_phone1;
    }

    public void setUser_phone1(String user_phone1) {
        this.user_phone1 = user_phone1;
    }

    public String getProduct_nm() {
        return product_nm;
    }

    public void setProduct_nm(String product_nm) {
        this.product_nm = product_nm;
    }

    public String getInstallment() {
        return installment;
    }

    public void setInstallment(String installment) {
        this.installment = installment;
    }
    
    public String getComplete_flag() {
        return complete_flag;
    }

    public void setComplete_flag(String complete_flag) {
        this.complete_flag = complete_flag;
    }

    public String getComplete_flag_nm() {
        return complete_flag_nm;
    }

    public void setComplete_flag_nm(String complete_flag_nm) {
        this.complete_flag_nm = complete_flag_nm;
    }

    public String getOnfftid_nm() {
        return onfftid_nm;
    }

    public void setOnfftid_nm(String onfftid_nm) {
        this.onfftid_nm = onfftid_nm;
    }

    public String getParent_complain_seq() {
        return parent_complain_seq;
    }

    public void setParent_complain_seq(String parent_complain_seq) {
        this.parent_complain_seq = parent_complain_seq;
    }

    public String getComplain_popup_yn() {
        return complain_popup_yn;
    }

    public void setComplain_popup_yn(String complain_popup_yn) {
        this.complain_popup_yn = complain_popup_yn;
    }

    public String getMerch_nm() {
        return merch_nm;
    }

    public void setMerch_nm(String merch_nm) {
        this.merch_nm = merch_nm;
    }

    public String getOnffmerch_no() {
        return onffmerch_no;
    }

    public void setOnffmerch_no(String onffmerch_no) {
        this.onffmerch_no = onffmerch_no;
    }

    public String getComplain_seq() {
        return complain_seq;
    }

    public void setComplain_seq(String complain_seq) {
        this.complain_seq = complain_seq;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCust_nm() {
        return cust_nm;
    }

    public void setCust_nm(String cust_nm) {
        this.cust_nm = cust_nm;
    }

    public String getCust_tel() {
        return cust_tel;
    }

    public void setCust_tel(String cust_tel) {
        this.cust_tel = cust_tel;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getOnfftid() {
        return onfftid;
    }

    public void setOnfftid(String onfftid) {
        this.onfftid = onfftid;
    }

    public String getPay_type() {
        return pay_type;
    }

    public void setPay_type(String pay_type) {
        this.pay_type = pay_type;
    }

    public String getTran_seq() {
        return tran_seq;
    }

    public void setTran_seq(String tran_seq) {
        this.tran_seq = tran_seq;
    }

    public String getBr_level() {
        return br_level;
    }

    public void setBr_level(String br_level) {
        this.br_level = br_level;
    }

    public String getParent_seq() {
        return parent_seq;
    }

    public void setParent_seq(String parent_seq) {
        this.parent_seq = parent_seq;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDel_flag() {
        return del_flag;
    }

    public void setDel_flag(String del_flag) {
        this.del_flag = del_flag;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    public String getIns_start_dt() {
        return ins_start_dt;
    }

    public void setIns_start_dt(String ins_start_dt) {
        this.ins_start_dt = ins_start_dt;
    }

    public String getIns_end_dt() {
        return ins_end_dt;
    }

    public void setIns_end_dt(String ins_end_dt) {
        this.ins_end_dt = ins_end_dt;
    }
    
}
