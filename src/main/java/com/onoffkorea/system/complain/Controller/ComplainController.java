/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.complain.Controller;

import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.complain.Bean.ComplainFormBean;
import com.onoffkorea.system.complain.Service.ComplainService;
import com.onoffkorea.system.complain.Vo.Tb_Complain;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import com.onoffkorea.system.trans.Bean.TransFormBean;
import com.onoffkorea.system.trans.Vo.Tb_Tran_Cardpg;
import common.web.servlet.BaseSpringMultiActionController;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/complain/**")
public class ComplainController extends BaseSpringMultiActionController{
        
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private CommonService commonService;
    
    @Autowired
    private ComplainService complainService;    
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }        

    
    //민원 관리 조회
    @RequestMapping(method= RequestMethod.POST,value = "/complainListExcel")
    public void complainMasterListExcel(@Valid  ComplainFormBean complainFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {        
        
        complainFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            complainFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            complainFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            complainFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                
        
        String excelDownloadFilename = new String(getMessage("complain.complainListExcel.fileName"));
        complainService.complainMasterListExcel(complainFormBean,excelDownloadFilename, response);
    }    
    
    
    //민원 관리 조회
    @RequestMapping(method= RequestMethod.POST,value = "/basic_complainListExcel")
    public void basic_complainMasterListExcel(@Valid  ComplainFormBean complainFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {        
        
        complainFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            complainFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            complainFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            complainFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                
        
        String excelDownloadFilename = new String(getMessage("complain.complainListExcel.fileName"));
        complainService.basic_complainMasterListExcel(complainFormBean,excelDownloadFilename, response);
    }    
        
        
    //민원 관리 조회
    @RequestMapping(method= RequestMethod.GET,value = "/basic_complainMasterList")
    public String basic_complainMasterFormList(@Valid ComplainFormBean complainFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        complainFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            complainFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            complainFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            complainFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                
        
        Hashtable ht_complainMasterList = complainService.basic_complainMasterList(complainFormBean);
        List ls_complainMasterList = (List) ht_complainMasterList.get("ls_complainMasterList");
        
        String strTpTotal_count = (String)ht_complainMasterList.get("total_count");
        
        model.addAttribute("total_count",strTpTotal_count);
        
        model.addAttribute("ls_complainMasterList",ls_complainMasterList);     
        
        Tb_Code_Main complStatusList = (Tb_Code_Main) ht_complainMasterList.get("complStatusList");
        
        model.addAttribute("complStatusList",complStatusList);       
        
        Tb_Code_Main completeFlagList = (Tb_Code_Main) ht_complainMasterList.get("completeFlagList");
        
        model.addAttribute("completeFlagList",completeFlagList);           
        
        
        Tb_Code_Main complainCateList = (Tb_Code_Main) ht_complainMasterList.get("complainCateList");
        
        model.addAttribute("complainCateList",complainCateList);            
        
        return null;
        
    }
    
    //민원 관리 조회
    @RequestMapping(method= RequestMethod.POST,value = "/basic_complainMasterList")
    public String basic_transMasterList(@Valid ComplainFormBean complainFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

                complainFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            complainFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            complainFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            complainFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                
        
        Hashtable ht_complainMasterList = complainService.basic_complainMasterList(complainFormBean);
        List ls_complainMasterList = (List) ht_complainMasterList.get("ls_complainMasterList");
        
        String strTpTotal_count = (String)ht_complainMasterList.get("total_count");
        
        model.addAttribute("total_count",strTpTotal_count);
        
        model.addAttribute("ls_complainMasterList",ls_complainMasterList);     
        
        Tb_Code_Main complStatusList = (Tb_Code_Main) ht_complainMasterList.get("complStatusList");
        
        model.addAttribute("complStatusList",complStatusList);       
        
        Tb_Code_Main completeFlagList = (Tb_Code_Main) ht_complainMasterList.get("completeFlagList");
        
        model.addAttribute("completeFlagList",completeFlagList);           
        
        
        Tb_Code_Main complainCateList = (Tb_Code_Main) ht_complainMasterList.get("complainCateList");
        
        model.addAttribute("complainCateList",complainCateList);            
        
        return null;
        
    } 
    
    //민원 관리 조회
    @RequestMapping(method= RequestMethod.GET,value = "/complainMasterList")
    public String complainMasterFormList(@Valid ComplainFormBean complainFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        complainFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            complainFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            complainFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            complainFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                
        
        Hashtable ht_complainMasterList = complainService.complainMasterList(complainFormBean);
        String ls_complainMasterList = (String) ht_complainMasterList.get("ResultSet");
        
        model.addAttribute("ls_complainMasterList",ls_complainMasterList);     
        
        Tb_Code_Main complStatusList = (Tb_Code_Main) ht_complainMasterList.get("complStatusList");
        
        model.addAttribute("complStatusList",complStatusList);       
        
        Tb_Code_Main completeFlagList = (Tb_Code_Main) ht_complainMasterList.get("completeFlagList");
        
        model.addAttribute("completeFlagList",completeFlagList);           
        
        
        Tb_Code_Main complainCateList = (Tb_Code_Main) ht_complainMasterList.get("complainCateList");
        
        model.addAttribute("complainCateList",complainCateList);            
        
        return null;
        
    }
    
    //민원 관리 조회
    @RequestMapping(method= RequestMethod.POST,value = "/complainMasterList")
    @ResponseBody
    public String transMasterList(@Valid ComplainFormBean complainFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        complainFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            complainFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            complainFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            complainFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }              
        
        Hashtable ht_complainMasterList = complainService.complainMasterList(complainFormBean);
        String ls_complainMasterList = (String) ht_complainMasterList.get("ResultSet");

        return ls_complainMasterList;
        
    }        
    
    //민원 관리 등록
    @RequestMapping(method= RequestMethod.GET,value = "/complainMasterInsert")
    public String complainMasterFormInsert(@Valid ComplainFormBean complainFormBean, TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        complainFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            complainFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            complainFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            complainFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }              
        
        if(!"".equals(Util.nullToString(transFormBean.getTran_seq()))){
            Hashtable ht_transMasterList = complainService.transMasterList(transFormBean);
            String transMasterList = (String) ht_transMasterList.get("ResultSet");
            List<Tb_Tran_Cardpg> ls_transDetailList = (List<Tb_Tran_Cardpg>) ht_transMasterList.get("ls_transDetailList");

            model.addAttribute("transMasterList",transMasterList); 
            model.addAttribute("ls_transDetailList",ls_transDetailList);         
        }
        
        
        Hashtable ht_complainMasterList = complainService.complainMasterInsertForm(complainFormBean);
        
        Tb_Code_Main complStatusList = (Tb_Code_Main) ht_complainMasterList.get("complStatusList");
        
        model.addAttribute("complStatusList",complStatusList);       
        
        Tb_Code_Main completeFlagList = (Tb_Code_Main) ht_complainMasterList.get("completeFlagList");
        
        model.addAttribute("completeFlagList",completeFlagList);           
        
        
        Tb_Code_Main complainCateList = (Tb_Code_Main) ht_complainMasterList.get("complainCateList");
        
        model.addAttribute("complainCateList",complainCateList);                 
        
        return null;
        
    }    
    
    //민원 관리 등록
    @RequestMapping(method= RequestMethod.POST,value = "/complainMasterInsert")
    @ResponseBody
    public String complainMasterInsert(@Valid ComplainFormBean complainFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest
            , @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        complainFormBean.setIns_user(siteSession.getUser_seq());
        complainService.complainMasterInsert(complainFormBean);

        return null;
        
    }           
    
    //민원 관리 수정
    @RequestMapping(method= RequestMethod.GET,value = "/complainMasterUpdate")
    public String complainMasterFormUpdate(@Valid ComplainFormBean complainFormBean, TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        complainFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            complainFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            complainFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            complainFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }              
        
        Hashtable ht_complainMasterList = complainService.complainMasterList(complainFormBean);
        List<Tb_Complain> ls_complainMasterList = (List<Tb_Complain>) ht_complainMasterList.get("ls_complainMasterList");
        
        model.addAttribute("ls_complainMasterList",ls_complainMasterList);
        
        Hashtable ht_transMasterList = complainService.transMasterList(transFormBean);
        String ls_transMasterList = (String) ht_transMasterList.get("ResultSet");
        
        model.addAttribute("ls_transMasterList",ls_transMasterList);        
        
        String complain_seq = ls_complainMasterList.get(0).getComplain_seq();
        complainFormBean.setParent_complain_seq(complain_seq);
        Hashtable ht_complainDetailList = complainService.complainDetailList(complainFormBean);
        String ls_complainDetailList = (String) ht_complainDetailList.get("ResultSet");
        
        model.addAttribute("ls_complainDetailList",ls_complainDetailList);        
        
        Tb_Code_Main complStatusList = (Tb_Code_Main) ht_complainMasterList.get("complStatusList");
        
        model.addAttribute("complStatusList",complStatusList);       
        
        Tb_Code_Main completeFlagList = (Tb_Code_Main) ht_complainMasterList.get("completeFlagList");
        
        model.addAttribute("completeFlagList",completeFlagList);           
        
        
        Tb_Code_Main complainCateList = (Tb_Code_Main) ht_complainMasterList.get("complainCateList");
        
        model.addAttribute("complainCateList",complainCateList);                  
        
        
        
        return null;
        
    }        
    
    //민원 관리 수정
    @RequestMapping(method= RequestMethod.POST,value = "/complainMasterUpdate")
    @ResponseBody
    public String complainMasterUpdate(@Valid ComplainFormBean complainFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest
            , @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        complainFormBean.setMod_user(siteSession.getUser_seq());
        complainService.complainMasterUpdate(complainFormBean);

        return null;
        
    }          
    
    //민원 관리 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/complainMasterDelete")
    @ResponseBody
    public String complainMasterDelete(@Valid ComplainFormBean complainFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest
            , @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        complainFormBean.setMod_user(siteSession.getUser_seq());
        complainService.complainMasterDelete(complainFormBean);

        return null;
        
    }           
    
    //응답 등록
    @RequestMapping(method= RequestMethod.POST,value = "/complainDetailInsert")
    @ResponseBody
    public String complainDetailInsert(@Valid ComplainFormBean complainFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest
            , @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        complainFormBean.setIns_user(siteSession.getUser_seq());
        complainService.complainDetailInsert(complainFormBean);

        return null;
        
    }               
    
    //응답내역 조회
    @RequestMapping(method= RequestMethod.POST,value = "/complainDetailList")
    public @ResponseBody String complainDetailList(@Valid ComplainFormBean complainFormBean, TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        
        Hashtable ht_complainDetailList = complainService.complainDetailList(complainFormBean);
        String ls_complainDetailList = (String) ht_complainDetailList.get("ResultSet");
        
        return ls_complainDetailList;
        
    }           
    
    //응답 수정
    @RequestMapping(method= RequestMethod.POST,value = "/complainDetailUpdate")
    @ResponseBody
    public String complainDetailUpdate(@Valid ComplainFormBean complainFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest
            , @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        complainFormBean.setMod_user(siteSession.getUser_seq());
        complainService.complainDetailUpdate(complainFormBean);

        return null;
        
    }    
    
    //응답 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/complainDetailDelete")
    @ResponseBody
    public String complainDetailDelete(@Valid ComplainFormBean complainFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest
            , @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        complainFormBean.setMod_user(siteSession.getUser_seq());
        complainService.complainDetailDelete(complainFormBean);

        return null;
        
    }        
    
    //민원 완료
    @RequestMapping(method= RequestMethod.POST,value = "/complainMasterUpdateComplete")
    @ResponseBody
    public String complainMasterUpdateComplete(@Valid ComplainFormBean complainFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest
            , @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        complainFormBean.setMod_user(siteSession.getUser_seq());
        complainService.complainMasterUpdateComplete(complainFormBean);

        return null;
         
    }             
    
    
    //민원 완료
    @RequestMapping(method= RequestMethod.POST,value = "/complainMasterUpdateUnComplete")
    @ResponseBody
    public String complainMasterUpdateUnComplete(@Valid ComplainFormBean complainFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest
            , @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        complainFormBean.setMod_user(siteSession.getUser_seq());
        complainService.complainMasterUpdateUnComplete(complainFormBean);

        return null;
         
    }           
}
