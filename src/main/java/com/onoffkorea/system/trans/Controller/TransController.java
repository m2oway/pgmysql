/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.trans.Controller;

import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import com.onoffkorea.system.trans.Bean.TransFormBean;
import com.onoffkorea.system.trans.Service.TransService;
import common.web.servlet.BaseSpringMultiActionController;
import java.util.Hashtable;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/trans/**")
public class TransController extends BaseSpringMultiActionController{
        
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private CommonService commonService;
    
    @Autowired
    private TransService transService;
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
        
        
    }    

    //신용거래 레이아웃
    @RequestMapping(method= RequestMethod.GET,value = "/transLayout")
    public String transLayout(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        return null;
        
    }
    
    //신용거래내역 집계 조회
    @RequestMapping(method= RequestMethod.GET,value = "/transMasterList")
    public String transMasterFormList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        Hashtable ht_transMasterList = transService.transMasterList(transFormBean);
        String ls_transMasterList = (String) ht_transMasterList.get("ResultSet");
        
        model.addAttribute("ls_transMasterList",ls_transMasterList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_transMasterList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);              
        
        return null;
        
    }
    
    //신용거래내역 집계 조회
    @RequestMapping(method= RequestMethod.POST,value = "/transMasterList")
    @ResponseBody
    public String transMasterList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        Hashtable ht_transMasterList = transService.transMasterList(transFormBean);
        String ls_transMasterList = (String) ht_transMasterList.get("ResultSet");

        return ls_transMasterList;
        
    }    
    
    //신용거래 집계 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/transMasterListExcel")
    public void transMasterListExcel(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("trans.transMasterListExcel.fileName"));
        
        transService.transMasterListExcel(transFormBean, excelDownloadFilename, response);
        
    }       
    
    //신용거래내역 상세 조회
    @RequestMapping(method= RequestMethod.GET,value = "/transDetailList")
    public String transDetailFormList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {   
        
        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());   
        logger.trace("transDetailFormList siteSession.getSes_user_cate() : "+ siteSession.getSes_user_cate());
        
        Hashtable ht_transDetailList = transService.transDetailList(transFormBean);
        String ls_transDetailList = (String) ht_transDetailList.get("ResultSet");
        
        model.addAttribute("ls_transDetailList",ls_transDetailList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_transDetailList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);           
        
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_transDetailList.get("resultStatusList");
        
        model.addAttribute("resultStatusList",resultStatusList);                   
        
        Tb_Code_Main tranStatusList = (Tb_Code_Main) ht_transDetailList.get("tranStatusList");
        
        model.addAttribute("tranStatusList",tranStatusList);             
        
        Tb_Code_Main issCdList = (Tb_Code_Main) ht_transDetailList.get("issCdList");
        
        model.addAttribute("issCdList",issCdList);        
        
        Tb_Code_Main massagetypeCdList = (Tb_Code_Main) ht_transDetailList.get("massagetypeCdList");
        
        model.addAttribute("massagetypeCdList",massagetypeCdList);            
        
        return null;
        
    }
    
    //신용거래내역 상세 조회
    @RequestMapping(method= RequestMethod.POST,value = "/transDetailList")
    @ResponseBody
    public String transDetailList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());   
        
        Hashtable ht_transDetailList = transService.transDetailList(transFormBean);
        String ls_transDetailList = (String) ht_transDetailList.get("ResultSet");

        return ls_transDetailList;
        
    }   
    
    //신용거래 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/transDetailListExcel")
    public void transDetailListExcel(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("trans.transDetailListExcel.fileName"));
        
        transService.transDetailListExcel(transFormBean, excelDownloadFilename, response);
        
    }        
    
    //신용거래 실패내역 레이아웃
    @RequestMapping(method= RequestMethod.GET,value = "/transFailLayout")
    public String transFailLayout(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        return null;
        
    }
    
    //신용거래 실패내역 집계 조회
    @RequestMapping(method= RequestMethod.GET,value = "/transFailMasterList")
    public String transFailMasterFormList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        Hashtable ht_transFailMasterList = transService.transFailMasterList(transFormBean);
        String ls_transFailMasterList = (String) ht_transFailMasterList.get("ResultSet");
        model.addAttribute("ls_transFailMasterList",ls_transFailMasterList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_transFailMasterList.get("payChnCateList");
        model.addAttribute("payChnCateList",payChnCateList);              
        
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_transFailMasterList.get("resultStatusList");
        model.addAttribute("resultStatusList",resultStatusList);                   
        
        Tb_Code_Main tranStatusList = (Tb_Code_Main) ht_transFailMasterList.get("tranStatusList");
        model.addAttribute("tranStatusList",tranStatusList);             
        
        Tb_Code_Main issCdList = (Tb_Code_Main) ht_transFailMasterList.get("issCdList");
        model.addAttribute("issCdList",issCdList);                  
        
        return null;
        
    }
    
    //신용거래 실패내역 집계 조회
    @RequestMapping(method= RequestMethod.POST,value = "/transFailMasterList")
    @ResponseBody
    public String transFailMasterList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {  
        
        Hashtable ht_transFailMasterList = transService.transFailMasterList(transFormBean);
        String ls_transFailMasterList = (String) ht_transFailMasterList.get("ResultSet");

        return ls_transFailMasterList;
        
    }    
    
    //신용거래 실패내역 상세 조회
    @RequestMapping(method= RequestMethod.GET,value = "/transFailDetailList")
    public String transFailDetailFormList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        Hashtable ht_transFailDetailList = transService.transFailDetailList(transFormBean);
        String ls_transFailDetailList = (String) ht_transFailDetailList.get("ResultSet");
        model.addAttribute("ls_transFailDetailList",ls_transFailDetailList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_transFailDetailList.get("payChnCateList");
        model.addAttribute("payChnCateList",payChnCateList);           
        
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_transFailDetailList.get("resultStatusList");
        model.addAttribute("resultStatusList",resultStatusList);                   
        
        return null;
        
    }
    
    //신용거래 실패내역 상세 조회
    @RequestMapping(method= RequestMethod.POST,value = "/transFailDetailList")
    @ResponseBody
    public String transFailDetailList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        Hashtable ht_transFailDetailList = transService.transFailDetailList(transFormBean);
        String ls_transFailDetailList = (String) ht_transFailDetailList.get("ResultSet");

        return ls_transFailDetailList;
        
    }        
    
    //신용거래 실패내역 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/transFailDetailListExcel")
    public void transFailDetailListExcel(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("trans.transFailDetailListExcel.fileName"));   
        
        transService.transFailDetailListExcel(transFormBean, excelDownloadFilename, response); 
        
    }        
    
    //현금거래 레이아웃
    @RequestMapping(method= RequestMethod.GET,value = "/cashtransLayout")
    public String cashtransLayout(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        return null;
        
    }
    
    //현금거래내역 집계 조회
    @RequestMapping(method= RequestMethod.GET,value = "/cashtransMasterList")
    public String cashtransMasterFormList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        Hashtable ht_cashtransMasterList = transService.cashtransMasterList(transFormBean);
        String ls_cashtransMasterList = (String) ht_cashtransMasterList.get("ResultSet");
        
        model.addAttribute("ls_cashtransMasterList",ls_cashtransMasterList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_cashtransMasterList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);              
        
        return null;
        
    }
    
    //현금거래내역 집계 조회
    @RequestMapping(method= RequestMethod.POST,value = "/cashtransMasterList")
    @ResponseBody
    public String cashtransMasterList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        Hashtable ht_cashtransMasterList = transService.cashtransMasterList(transFormBean);
        String ls_cashtransMasterList = (String) ht_cashtransMasterList.get("ResultSet");

        return ls_cashtransMasterList;
        
    }   
    
    //현금거래 집계 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/cashTransMasterListExcel")
    public void cashTransMasterListExcel(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("trans.cashTransMasterListExcel.fileName"));   
        
        transService.cashTransMasterListExcel(transFormBean, excelDownloadFilename, response);
        
    }       
    
    //현금거래내역 상세 조회
    @RequestMapping(method= RequestMethod.GET,value = "/cashtransDetailList")
    public String cashtransDetailFormList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception { 
        
        Hashtable ht_cashtransDetailList = transService.cashtransDetailList(transFormBean);
        String ls_cashtransDetailList = (String) ht_cashtransDetailList.get("ResultSet");
        
        model.addAttribute("ls_cashtransDetailList",ls_cashtransDetailList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_cashtransDetailList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);           
        
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_cashtransDetailList.get("resultStatusList");
        
        model.addAttribute("resultStatusList",resultStatusList);  
        
        Tb_Code_Main tranStatusList = (Tb_Code_Main) ht_cashtransDetailList.get("tranStatusList");
        model.addAttribute("tranStatusList",tranStatusList);           
        
        
        
        return null;
        
    }
    
    //현금거래내역 상세 조회
    @RequestMapping(method= RequestMethod.POST,value = "/cashtransDetailList")
    @ResponseBody
    public String cashtransDetailList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {     
         
        Hashtable ht_cashtransDetailList = transService.cashtransDetailList(transFormBean);
        String ls_cashtransDetailList = (String) ht_cashtransDetailList.get("ResultSet");

        return ls_cashtransDetailList;
        
    }            
    
    //현금거래 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/cashTransDetailListExcel")
    public void cashTransDetailListExcel(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("trans.cashTransDetailListExcel.fileName")); 
        
        transService.cashTransDetailListExcel(transFormBean, excelDownloadFilename, response); 
        
    }               
    
    //가맹점 신용거래 레이아웃
    @RequestMapping(method= RequestMethod.GET,value = "/merchTransLayout")
    public String merchTransLayout(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        return null;
        
    }    
    
    //가맹점 신용거래내역 집계 조회
    @RequestMapping(method= RequestMethod.GET,value = "/merchTransMasterList")
    public String merchTransMasterFormList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        Hashtable ht_transMasterList = transService.merchTransMasterList(transFormBean);
        String ls_transMasterList = (String) ht_transMasterList.get("ResultSet");
        
        model.addAttribute("ls_transMasterList",ls_transMasterList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_transMasterList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);              
        
        return null;
        
    }    
    
    //가맹점 신용거래내역 집계 조회
    @RequestMapping(method= RequestMethod.POST,value = "/merchTransMasterList")
    @ResponseBody
    public String merchTransMasterList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        Hashtable ht_transMasterList = transService.merchTransMasterList(transFormBean);
        String ls_transMasterList = (String) ht_transMasterList.get("ResultSet");

        return ls_transMasterList;
        
    }        
    
    //가맹점 신용거래 집계 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/merchTransMasterListExcel")
    public void merchTransMasterListExcel(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("trans.transMasterListExcel.fileName"));
        
        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        transService.merchTransMasterListExcel(transFormBean, excelDownloadFilename, response);
        
    }            
    
    //가맹점 신용거래내역 상세 조회
    @RequestMapping(method= RequestMethod.GET,value = "/merchTransDetailList")
    public String merchTransDetailFormList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
            transFormBean.setSes_merch_cncl_auth(siteSession.getSes_merch_cncl_auth());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
            transFormBean.setSes_tid_cncl_auth(siteSession.getSes_tid_cncl_auth());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        Hashtable ht_transDetailList = transService.merchTransDetailList(transFormBean);
        String ls_transDetailList = (String) ht_transDetailList.get("ResultSet");
        
        model.addAttribute("ls_transDetailList",ls_transDetailList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_transDetailList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);           
        
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_transDetailList.get("resultStatusList");
        
        model.addAttribute("resultStatusList",resultStatusList);             
        
        Tb_Code_Main massagetypeCdList = (Tb_Code_Main) ht_transDetailList.get("massagetypeCdList");
        
        model.addAttribute("massagetypeCdList",massagetypeCdList);   
        
        Tb_Code_Main tranStatusList = (Tb_Code_Main) ht_transDetailList.get("tranStatusList");
        
        model.addAttribute("tranStatusList",tranStatusList);     
        
        Tb_Code_Main issCdList = (Tb_Code_Main) ht_transDetailList.get("issCdList");
        
        model.addAttribute("issCdList",issCdList); 
        
        return null;
        
    }    
    
    //가맹점 신용거래내역 상세 조회
    @RequestMapping(method= RequestMethod.POST,value = "/merchTransDetailList")
    @ResponseBody
    public String merchanttransDetailList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
             transFormBean.setSes_merch_cncl_auth(siteSession.getSes_merch_cncl_auth());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
             transFormBean.setSes_tid_cncl_auth(siteSession.getSes_tid_cncl_auth());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        Hashtable ht_transDetailList = transService.merchTransDetailList(transFormBean);
        String ls_transDetailList = (String) ht_transDetailList.get("ResultSet");

        return ls_transDetailList;
        
    }       
    
    //가맹점 신용거래내역 상세 조회
    @RequestMapping(method= RequestMethod.GET,value = "/basic_merchTransDetailList")
    public String basic_merchTransDetailFormList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
            transFormBean.setSes_merch_cncl_auth(siteSession.getSes_merch_cncl_auth());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
            transFormBean.setSes_tid_cncl_auth(siteSession.getSes_tid_cncl_auth());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        Hashtable ht_transDetailList = transService.basic_merchTransDetailList(transFormBean);
        List ls_transDetailList = (List) ht_transDetailList.get("ls_transDetailList");
        model.addAttribute("ls_transDetailList",ls_transDetailList);     
        
        String strTpTotal_count = (String)ht_transDetailList.get("total_count");
        model.addAttribute("total_count",strTpTotal_count);
                
        String strTpsesUserCate = (String)ht_transDetailList.get("parma_usercate");
        model.addAttribute("parma_usercate",strTpsesUserCate);   
        
        String strTpsesmerch_cncl_auth = (String)ht_transDetailList.get("parma_merch_cncl_auth");
        model.addAttribute("parma_merch_cncl_auth",strTpsesmerch_cncl_auth);   
        
        String strTpsestid_cncl_auth = (String)ht_transDetailList.get("parma_tid_cncl_auth");
        model.addAttribute("parma_tid_cncl_auth",strTpsestid_cncl_auth);   

        String strTpTotAppCnt = Util.nullToInt((String)ht_transDetailList.get("total_appcnt"));
        model.addAttribute("total_appcnt",strTpTotAppCnt);   
        String strTpTotAppAmt = Util.nullToInt((String)ht_transDetailList.get("total_appamt"));
        model.addAttribute("total_appamt",strTpTotAppAmt);   
        String strTpTotCnclCnt = Util.nullToInt((String)ht_transDetailList.get("total_cnclcnt"));
        model.addAttribute("total_cnclcnt",strTpTotCnclCnt);   
        String strTpTotCnclAmt = Util.nullToInt((String)ht_transDetailList.get("total_cnclamt"));
        model.addAttribute("total_cnclamt",strTpTotCnclAmt);   
        
        Tb_Code_Main issCdList = (Tb_Code_Main) ht_transDetailList.get("issCdList");
        
        model.addAttribute("issCdList",issCdList); 
        
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_transDetailList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);           
        
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_transDetailList.get("resultStatusList");
        
        model.addAttribute("resultStatusList",resultStatusList);             
        
        Tb_Code_Main massagetypeCdList = (Tb_Code_Main) ht_transDetailList.get("massagetypeCdList");
        
        model.addAttribute("massagetypeCdList",massagetypeCdList);   
        
        Tb_Code_Main tranStatusList = (Tb_Code_Main) ht_transDetailList.get("tranStatusList");
        
        model.addAttribute("tranStatusList",tranStatusList);      
        
        return null;
        
    }    
    
    //가맹점 신용거래내역 상세 조회
    @RequestMapping(method= RequestMethod.POST,value = "/basic_merchTransDetailList")
    public String basic_merchanttransDetailList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
            transFormBean.setSes_merch_cncl_auth(siteSession.getSes_merch_cncl_auth());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
            transFormBean.setSes_tid_cncl_auth(siteSession.getSes_tid_cncl_auth());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        Hashtable ht_transDetailList = transService.basic_merchTransDetailList(transFormBean);
        List ls_transDetailList = (List) ht_transDetailList.get("ls_transDetailList");
        model.addAttribute("ls_transDetailList",ls_transDetailList);     
        
        String strTpTotal_count = (String)ht_transDetailList.get("total_count");
        model.addAttribute("total_count",strTpTotal_count);
                
        String strTpsesUserCate = (String)ht_transDetailList.get("parma_usercate");
        model.addAttribute("parma_usercate",strTpsesUserCate);   
        
        String strTpsesmerch_cncl_auth = (String)ht_transDetailList.get("parma_merch_cncl_auth");
        model.addAttribute("parma_merch_cncl_auth",strTpsesmerch_cncl_auth);   
        
        String strTpsestid_cncl_auth = (String)ht_transDetailList.get("parma_tid_cncl_auth");
        model.addAttribute("parma_tid_cncl_auth",strTpsestid_cncl_auth);   
        

        String strTpTotAppCnt = Util.nullToInt((String)ht_transDetailList.get("total_appcnt"));
        model.addAttribute("total_appcnt",strTpTotAppCnt);   
        String strTpTotAppAmt = Util.nullToInt((String)ht_transDetailList.get("total_appamt"));
        model.addAttribute("total_appamt",strTpTotAppAmt);   
        String strTpTotCnclCnt = Util.nullToInt((String)ht_transDetailList.get("total_cnclcnt"));
        model.addAttribute("total_cnclcnt",strTpTotCnclCnt);   
        String strTpTotCnclAmt = Util.nullToInt((String)ht_transDetailList.get("total_cnclamt"));
        model.addAttribute("total_cnclamt",strTpTotCnclAmt);           
        
        Tb_Code_Main issCdList = (Tb_Code_Main) ht_transDetailList.get("issCdList");
        
        model.addAttribute("issCdList",issCdList); 
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_transDetailList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);           
        
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_transDetailList.get("resultStatusList");
        
        model.addAttribute("resultStatusList",resultStatusList);             
        
        Tb_Code_Main massagetypeCdList = (Tb_Code_Main) ht_transDetailList.get("massagetypeCdList");
        
        model.addAttribute("massagetypeCdList",massagetypeCdList);   
        
        Tb_Code_Main tranStatusList = (Tb_Code_Main) ht_transDetailList.get("tranStatusList");
        
        model.addAttribute("tranStatusList",tranStatusList);      
        
        return null;
        
    }                 
    
    //가맹점 신용거래 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/merchTransDetailListExcel")
    public void merchTransDetailListExcel(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("trans.transDetailListExcel.fileName"));
        
        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        transService.merchTransDetailListExcel(transFormBean, excelDownloadFilename, response);
        
    }           
    
        //가맹점 신용거래 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/basic_merchTransDetailListExcel")
    public void basic_merchTransDetailListExcel(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("trans.transDetailListExcel.fileName"));
        
        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        transService.basic_merchTransDetailListExcel(transFormBean, excelDownloadFilename, response);
        
    }   
    
    
    //가맹점 신용거래 실패내역 레이아웃
    @RequestMapping(method= RequestMethod.GET,value = "/merchTransFailLayout")
    public String merchTransFailLayout(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        return null;
        
    }
    
    //가맹점 신용거래 실패내역 상세 조회
    @RequestMapping(method= RequestMethod.GET,value = "/merchTransFailDetailList")
    public String merchTransFailDetailFormList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
 
        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        Hashtable ht_transFailDetailList = transService.merchTransFailDetailList(transFormBean);
        String ls_transFailDetailList = (String) ht_transFailDetailList.get("ResultSet");
        
        model.addAttribute("ls_transFailDetailList",ls_transFailDetailList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_transFailDetailList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);           
        
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_transFailDetailList.get("resultStatusList");
        
        model.addAttribute("resultStatusList",resultStatusList);     
        
        Tb_Code_Main issCdList = (Tb_Code_Main) ht_transFailDetailList.get("issCdList");
        
        model.addAttribute("issCdList",issCdList);        
        
        return null;
        
    }
    
    //가맹점 신용거래 실패내역 상세 조회
    @RequestMapping(method= RequestMethod.POST,value = "/merchTransFailDetailList")
    @ResponseBody
    public String merchTransFailDetailList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        Hashtable ht_transFailDetailList = transService.merchTransFailDetailList(transFormBean);
        String ls_transFailDetailList = (String) ht_transFailDetailList.get("ResultSet");

        return ls_transFailDetailList;
        
    }        
    
    
    //가맹점 신용거래 실패내역 상세 조회
    @RequestMapping(method= RequestMethod.GET,value = "/basic_merchTransFailDetailList")
    public String basic_merchTransFailDetailFormList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
 
        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        Hashtable ht_transFailDetailList = transService.basic_merchTransFailDetailList(transFormBean);
        List ls_transFailDetailList = (List) ht_transFailDetailList.get("ls_transFailDetailList");
        
        model.addAttribute("ls_transFailDetailList",ls_transFailDetailList); 
        
        String strTpTotal_count = (String)ht_transFailDetailList.get("total_count");
        model.addAttribute("total_count",strTpTotal_count);
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_transFailDetailList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);           
        
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_transFailDetailList.get("resultStatusList");
        
        model.addAttribute("resultStatusList",resultStatusList);                   
        
        return null;
        
    }
    
    //가맹점 신용거래 실패내역 상세 조회
    @RequestMapping(method= RequestMethod.POST,value = "/basic_merchTransFailDetailList")
    public String basic_merchTransFailDetailList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        Hashtable ht_transFailDetailList = transService.basic_merchTransFailDetailList(transFormBean);
        List ls_transFailDetailList = (List) ht_transFailDetailList.get("ls_transFailDetailList");
        
        model.addAttribute("ls_transFailDetailList",ls_transFailDetailList); 
        
        String strTpTotal_count = (String)ht_transFailDetailList.get("total_count");
        model.addAttribute("total_count",strTpTotal_count);
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_transFailDetailList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);           
        
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_transFailDetailList.get("resultStatusList");
        
        model.addAttribute("resultStatusList",resultStatusList);                   
        
        return null;
        
    }            
    
    
    //가맹점 신용거래 실패내역 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/merchTransFailDetailListExcel")
    public void merchTransFailDetailListExcel(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("trans.transFailDetailListExcel.fileName"));
        
        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        transService.merchTransFailDetailListExcel(transFormBean, excelDownloadFilename, response); 
        
    }   
    
    //가맹점 신용거래 실패내역 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/basic_merchTransFailDetailListExcel")
    public void basic_merchTransFailDetailListExcel(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("trans.transFailDetailListExcel.fileName"));
        
        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        transService.basic_merchTransFailDetailListExcel(transFormBean, excelDownloadFilename, response); 
        
    }        
    
    //가맹점 현금거래 레이아웃
    @RequestMapping(method= RequestMethod.GET,value = "/merchCashtransLayout")
    public String merchCashtransLayout(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        return null;
        
    }
    
    //가맹점 현금거래내역 집계 조회
    @RequestMapping(method= RequestMethod.GET,value = "/merchCashtransMasterList")
    public String merchCashtransMasterFormList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }             
        
        Hashtable ht_cashtransMasterList = transService.merchCashtransMasterList(transFormBean);
        String ls_cashtransMasterList = (String) ht_cashtransMasterList.get("ResultSet");
        
        model.addAttribute("ls_cashtransMasterList",ls_cashtransMasterList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_cashtransMasterList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);              
        
        return null;
        
    }
    
    //가맹점 현금거래내역 집계 조회
    @RequestMapping(method= RequestMethod.POST,value = "/merchCashtransMasterList")
    @ResponseBody
    public String merchCashtransMasterList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        Hashtable ht_cashtransMasterList = transService.merchCashtransMasterList(transFormBean);
        String ls_cashtransMasterList = (String) ht_cashtransMasterList.get("ResultSet");

        return ls_cashtransMasterList;
        
    }   
    
    //가맹점 현금거래 집계 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/merchCashTransMasterListExcel")
    public void merchCashTransMasterListExcel(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("trans.cashTransMasterListExcel.fileName"));
        
        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        transService.merchCashTransMasterListExcel(transFormBean, excelDownloadFilename, response);
        
    } 
    
    
        //가맹점 현금거래내역 상세 조회
    @RequestMapping(method= RequestMethod.GET,value = "/basic_merchCashtransDetailList")
    public String basic_merchCashtransDetailFormList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        Hashtable ht_cashtransDetailList = transService.basic_merchCashtransDetailList(transFormBean);
        
        List ls_cashtransDetailList = (List)ht_cashtransDetailList.get("ls_cashtransDetailList");
        
        model.addAttribute("ls_cashtransDetailList",ls_cashtransDetailList);     
        

        String strTpTotal_count = (String)ht_cashtransDetailList.get("total_count");
        model.addAttribute("total_count",strTpTotal_count);
                
        String strTpsesUserCate = (String)ht_cashtransDetailList.get("parma_usercate");
        model.addAttribute("parma_usercate",strTpsesUserCate);   
        
        String strTpsesmerch_cncl_auth = (String)ht_cashtransDetailList.get("parma_merch_cncl_auth");
        model.addAttribute("parma_merch_cncl_auth",strTpsesmerch_cncl_auth);   
        
        String strTpsestid_cncl_auth = (String)ht_cashtransDetailList.get("parma_tid_cncl_auth");
        model.addAttribute("parma_tid_cncl_auth",strTpsestid_cncl_auth);   
        
        
        String strTpTotAppCnt = Util.nullToInt((String)ht_cashtransDetailList.get("total_appcnt"));
        model.addAttribute("total_appcnt",strTpTotAppCnt);   
        String strTpTotAppAmt = Util.nullToInt((String)ht_cashtransDetailList.get("total_appamt"));
        model.addAttribute("total_appamt",strTpTotAppAmt);   
        String strTpTotCnclCnt = Util.nullToInt((String)ht_cashtransDetailList.get("total_cnclcnt"));
        model.addAttribute("total_cnclcnt",strTpTotCnclCnt);   
        String strTpTotCnclAmt = Util.nullToInt((String)ht_cashtransDetailList.get("total_cnclamt"));
        model.addAttribute("total_cnclamt",strTpTotCnclAmt);    
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_cashtransDetailList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);           
        
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_cashtransDetailList.get("resultStatusList");
        
        model.addAttribute("resultStatusList",resultStatusList);                   
        
        return null;
        
    }
    
    
        //현금거래내역 상세 조회
    @RequestMapping(method= RequestMethod.POST,value = "/basic_merchCashtransDetailList")
    public String basic_merchCashtransDetailList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        Hashtable ht_cashtransDetailList = transService.basic_merchCashtransDetailList(transFormBean);
        
        List ls_cashtransDetailList = (List)ht_cashtransDetailList.get("ls_cashtransDetailList");
        
        model.addAttribute("ls_cashtransDetailList",ls_cashtransDetailList);     
        

        String strTpTotal_count = (String)ht_cashtransDetailList.get("total_count");
        model.addAttribute("total_count",strTpTotal_count);
                
        String strTpsesUserCate = (String)ht_cashtransDetailList.get("parma_usercate");
        model.addAttribute("parma_usercate",strTpsesUserCate);   
        
        String strTpsesmerch_cncl_auth = (String)ht_cashtransDetailList.get("parma_merch_cncl_auth");
        model.addAttribute("parma_merch_cncl_auth",strTpsesmerch_cncl_auth);   
        
        String strTpsestid_cncl_auth = (String)ht_cashtransDetailList.get("parma_tid_cncl_auth");
        model.addAttribute("parma_tid_cncl_auth",strTpsestid_cncl_auth);   
        
        String strTpTotAppCnt = Util.nullToInt((String)ht_cashtransDetailList.get("total_appcnt"));
        model.addAttribute("total_appcnt",strTpTotAppCnt);   
        String strTpTotAppAmt = Util.nullToInt((String)ht_cashtransDetailList.get("total_appamt"));
        model.addAttribute("total_appamt",strTpTotAppAmt);   
        String strTpTotCnclCnt = Util.nullToInt((String)ht_cashtransDetailList.get("total_cnclcnt"));
        model.addAttribute("total_cnclcnt",strTpTotCnclCnt);   
        String strTpTotCnclAmt = Util.nullToInt((String)ht_cashtransDetailList.get("total_cnclamt"));
        model.addAttribute("total_cnclamt",strTpTotCnclAmt);    
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_cashtransDetailList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);           
        
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_cashtransDetailList.get("resultStatusList");
        
        model.addAttribute("resultStatusList",resultStatusList);                   
        
        return null;
        
    }       
    
    
    //가맹점 현금거래내역 상세 조회
    @RequestMapping(method= RequestMethod.GET,value = "/merchCashtransDetailList")
    public String merchCashtransDetailFormList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        Hashtable ht_cashtransDetailList = transService.merchCashtransDetailList(transFormBean);
        String ls_cashtransDetailList = (String) ht_cashtransDetailList.get("ResultSet");
        
        model.addAttribute("ls_cashtransDetailList",ls_cashtransDetailList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_cashtransDetailList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);           
        
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_cashtransDetailList.get("resultStatusList");
        
        model.addAttribute("resultStatusList",resultStatusList);                   
        
        return null;
        
    }
    
    //현금거래내역 상세 조회
    @RequestMapping(method= RequestMethod.POST,value = "/merchCashtransDetailList")
    @ResponseBody
    public String merchCashtransDetailList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
            transFormBean.setSes_merch_cncl_auth(siteSession.getSes_merch_cncl_auth());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
            transFormBean.setSes_tid_cncl_auth(siteSession.getSes_tid_cncl_auth());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
         
        Hashtable ht_cashtransDetailList = transService.merchCashtransDetailList(transFormBean);
        String ls_cashtransDetailList = (String) ht_cashtransDetailList.get("ResultSet");

        return ls_cashtransDetailList;
        
    }            
    
    //현금거래 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/merchCashTransDetailListExcel")
    public void merchCashTransDetailListExcel(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("trans.cashTransDetailListExcel.fileName"));
        
        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        transService.merchCashTransDetailListExcel(transFormBean, excelDownloadFilename, response); 
        
    }         

    
    
    //현금거래 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/basic_merchCashTransDetailListExcel")
    public void basic_merchCashTransDetailListExcel(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("trans.cashTransDetailListExcel.fileName"));
        
        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            transFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        transService.basic_merchCashTransDetailListExcel(transFormBean, excelDownloadFilename, response); 
        
    }         
    
    //신용이상거래 레이아웃
    @RequestMapping(method= RequestMethod.GET,value = "/transChkLayout")
    public String transChkLayout(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        return null;        
    }
    
    //신용이상거래 집계 조회
    @RequestMapping(method= RequestMethod.GET,value = "/transChkMasterList")
    public String transChkMasterFormList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        Hashtable ht_transMasterList = transService.transChkMasterList(transFormBean);
        String ls_transMasterList = (String) ht_transMasterList.get("ResultSet");
        
        model.addAttribute("ls_transMasterList",ls_transMasterList);     
        
        return null;
        
    }
    
    //이상거래 집계 조회
    @RequestMapping(method= RequestMethod.POST,value = "/transChkMasterList")
    @ResponseBody
    public String transChkMasterList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        Hashtable ht_transMasterList = transService.transChkMasterList(transFormBean);
        String ls_transMasterList = (String) ht_transMasterList.get("ResultSet");

        return ls_transMasterList;
        
    }    
    
  
    //신용거래내역 상세 조회
    @RequestMapping(method= RequestMethod.GET,value = "/transChkDetailList")
    public String transChkDetailFormList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {   
        
        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());   
        logger.trace("transDetailFormList siteSession.getSes_user_cate() : "+ siteSession.getSes_user_cate());
        
        Hashtable ht_transDetailList = transService.transDetailList(transFormBean);
        String ls_transDetailList = (String) ht_transDetailList.get("ResultSet");
        
        model.addAttribute("ls_transDetailList",ls_transDetailList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_transDetailList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);           
        
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_transDetailList.get("resultStatusList");
        
        model.addAttribute("resultStatusList",resultStatusList);                   
        
        Tb_Code_Main tranStatusList = (Tb_Code_Main) ht_transDetailList.get("tranStatusList");
        
        model.addAttribute("tranStatusList",tranStatusList);             
        
        Tb_Code_Main issCdList = (Tb_Code_Main) ht_transDetailList.get("issCdList");
        
        model.addAttribute("issCdList",issCdList);    
        
        Tb_Code_Main massagetypeCdList = (Tb_Code_Main) ht_transDetailList.get("massagetypeCdList");
        
        model.addAttribute("massagetypeCdList", massagetypeCdList);
        
        return null;
        
    }
    
    //신용거래내역 상세 조회
    @RequestMapping(method= RequestMethod.POST,value = "/transChkDetailList")
    @ResponseBody
    public String transChkDetailList(@Valid TransFormBean transFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        transFormBean.setSes_user_cate(siteSession.getSes_user_cate());   
        
        Hashtable ht_transDetailList = transService.transDetailList(transFormBean);
        String ls_transDetailList = (String) ht_transDetailList.get("ResultSet");

        return ls_transDetailList;
        
    }       
}
