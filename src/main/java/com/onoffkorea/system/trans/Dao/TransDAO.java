/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.trans.Dao;

import com.onoffkorea.system.trans.Bean.TransFormBean;
import com.onoffkorea.system.trans.Vo.Tb_Tran_Cardpg;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface TransDAO {

    public Integer transMasterListCount(TransFormBean transFormBean);
    
    public List<Tb_Tran_Cardpg> transMasterList(TransFormBean transFormBean);

    public Integer transDetailListCount(TransFormBean transFormBean);

    public List<Tb_Tran_Cardpg> transDetailList(TransFormBean transFormBean);

    public Integer transFailMasterListCount(TransFormBean transFormBean);

    public List<Tb_Tran_Cardpg> transFailMasterList(TransFormBean transFormBean);

    public Integer transFailDetailListCount(TransFormBean transFormBean);

    public List<Tb_Tran_Cardpg> transFailDetailList(TransFormBean transFormBean);

    public Integer cashtransMasterListCount(TransFormBean transFormBean);

    public List<Tb_Tran_Cardpg> cashtransMasterList(TransFormBean transFormBean);

    public Integer cashtransDetailListCount(TransFormBean transFormBean);

    public List<Tb_Tran_Cardpg> cashtransDetailList(TransFormBean transFormBean);

    public List transMasterListExcel(TransFormBean transFormBean);

    public List transDetailListExcel(TransFormBean transFormBean);

    public List cashTransMasterListExcel(TransFormBean transFormBean);

    public List cashTransDetailListExcel(TransFormBean transFormBean);

    public List transFailDetailListExcel(TransFormBean transFormBean);

    public Integer merchTransMasterListCount(TransFormBean transFormBean);

    public List<Tb_Tran_Cardpg> merchTransMasterList(TransFormBean transFormBean);
    
    public List<Tb_Tran_Cardpg> basic_merchTransMasterList(TransFormBean transFormBean);

    public List merchTransMasterListExcel(TransFormBean transFormBean);

    public Integer merchTransDetailListCount(TransFormBean transFormBean);

    public List<Tb_Tran_Cardpg> merchTransDetailList(TransFormBean transFormBean);

    public List merchTransDetailListExcel(TransFormBean transFormBean);
    
    public List basic_merchTransDetailListExcel(TransFormBean transFormBean);

    public Integer merchTransFailDetailListCount(TransFormBean transFormBean);

    public List<Tb_Tran_Cardpg> merchTransFailDetailList(TransFormBean transFormBean);

    public List merchTransFailDetailListExcel(TransFormBean transFormBean);
    
    public List basic_merchTransFailDetailListExcel(TransFormBean transFormBean);

    public Integer merchCashtransMasterListCount(TransFormBean transFormBean);

    public List<Tb_Tran_Cardpg> merchCashtransMasterList(TransFormBean transFormBean);
    
    public List<Tb_Tran_Cardpg> basic_merchCashtransMasterList(TransFormBean transFormBean);    

    public List merchCashTransMasterListExcel(TransFormBean transFormBean);

    public Integer merchCashtransDetailListCount(TransFormBean transFormBean);

    public List<Tb_Tran_Cardpg> merchCashtransDetailList(TransFormBean transFormBean);
    
    public List merchCashTransDetailListExcel(TransFormBean transFormBean);
    
    public List basic_merchCashTransDetailListExcel(TransFormBean transFormBean);

    public Integer transChkMasterListCount(TransFormBean transFormBean);
    
    public List<Tb_Tran_Cardpg> transChkMasterList(TransFormBean transFormBean);
}
