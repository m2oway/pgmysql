/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.trans.Dao;

import com.onoffkorea.system.trans.Bean.TransFormBean;
import com.onoffkorea.system.trans.Vo.Tb_Tran_Cardpg;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("transDAO")
public class TransDAOImpl extends SqlSessionDaoSupport implements TransDAO{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    //신용카드 집계 조회 카운트
    @Override
    public Integer transMasterListCount(TransFormBean transFormBean) {
        
        return (Integer)getSqlSession().selectOne("Trans.transMasterListCount",transFormBean);
        
    }    
    
    //신용카드 집계 조회
    @Override
    public List<Tb_Tran_Cardpg> transMasterList(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.transMasterList",transFormBean);
        
    }

    //신용카드 상세 조회 카운트
    @Override
    public Integer transDetailListCount(TransFormBean transFormBean) {
        
        return (Integer)getSqlSession().selectOne("Trans.transDetailListCount",transFormBean);
        
    }

    //신용카드 상세 조회 
    @Override
    public List<Tb_Tran_Cardpg> transDetailList(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.transDetailList",transFormBean);
        
    }
    
    //신용카드 실패내역 집계 조회 카운트
    @Override
    public Integer transFailMasterListCount(TransFormBean transFormBean) {
        
        return (Integer)getSqlSession().selectOne("Trans.transFailMasterListCount",transFormBean);
        
    }    
    
    //신용카드 실패내역 집계 조회
    @Override
    public List<Tb_Tran_Cardpg> transFailMasterList(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.transFailMasterList",transFormBean);
        
    }

    //신용카드 실패내역 상세 조회 카운트
    @Override
    public Integer transFailDetailListCount(TransFormBean transFormBean) {
        
        return (Integer)getSqlSession().selectOne("Trans.transFailDetailListCount",transFormBean);
        
    }

    //신용카드 실패내역 상세 조회 
    @Override
    public List<Tb_Tran_Cardpg> transFailDetailList(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.transFailDetailList",transFormBean);
        
    }    
    
    //현금 집계 조회 카운트
    @Override
    public Integer cashtransMasterListCount(TransFormBean transFormBean) {
        
        return (Integer)getSqlSession().selectOne("Trans.cashtransMasterListCount",transFormBean);
        
    }    
    
    //현금 집계 조회
    @Override
    public List<Tb_Tran_Cardpg> cashtransMasterList(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.cashtransMasterList",transFormBean);
        
    }

    //현금 상세 조회 카운트
    @Override
    public Integer cashtransDetailListCount(TransFormBean transFormBean) {
        
        return (Integer)getSqlSession().selectOne("Trans.cashtransDetailListCount",transFormBean);
        
    }

    //현금 상세 조회 
    @Override
    public List<Tb_Tran_Cardpg> cashtransDetailList(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.cashtransDetailList",transFormBean);
        
    }    
    
    //신용카드 엑셀다운로드 조회
    @Override
    public List transMasterListExcel(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.transMasterListExcel",transFormBean);
        
    }
    
    //신용카드 엑셀다운로드 조회
    @Override
    public List transDetailListExcel(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.transDetailListExcel",transFormBean);
        
    }    
    
    //현금거래 집계 엑셀다운로드 조회
    @Override
    public List cashTransMasterListExcel(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.cashTransMasterListExcel",transFormBean);
        
    }    
    
    //현금거래 엑셀다운로드 조회
    @Override
    public List cashTransDetailListExcel(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.cashTransDetailListExcel",transFormBean);
        
    }        
    
    //신용거래 실패내역 엑셀다운로드 조회
    @Override
    public List transFailDetailListExcel(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.transFailDetailListExcel",transFormBean);
        
    }        

    //가맹점 신용카드 집계 조회 카운트
    @Override
    public Integer merchTransMasterListCount(TransFormBean transFormBean) {
        
        return (Integer)getSqlSession().selectOne("Trans.merchTransMasterListCount",transFormBean);
        
    }

    //가맹점 신용카드 집계 조회
    @Override
    public List<Tb_Tran_Cardpg> merchTransMasterList(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.merchTransMasterList",transFormBean);
        
    }
    
    //가맹점 신용카드 집계 조회
    @Override
    public List<Tb_Tran_Cardpg> basic_merchTransMasterList(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.basic_merchTransMasterList",transFormBean);
        
    }
    
    //가맹점 신용카드 엑셀다운로드 조회
    @Override
    public List merchTransMasterListExcel(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.merchTransMasterListExcel",transFormBean);
        
    }    

    //가맹점 신용카드 상세 내역 조회 카운트
    @Override
    public Integer merchTransDetailListCount(TransFormBean transFormBean) {
        
        return (Integer)getSqlSession().selectOne("Trans.merchTransDetailListCount",transFormBean);
        
    }

    //가맹점 신용카드 상세 내역 조회 
    @Override
    public List<Tb_Tran_Cardpg> merchTransDetailList(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.merchTransDetailList",transFormBean);
        
    }

    //가맹점 신용카드 상세 내역 엑셀다운로드
    @Override
    public List merchTransDetailListExcel(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.merchTransDetailListExcel",transFormBean);
        
    }
    
        //가맹점 신용카드 상세 내역 엑셀다운로드
    @Override
    public List basic_merchTransDetailListExcel(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.basic_merchTransDetailListExcel",transFormBean);
        
    }

    //가맹점 신용카드 실패내역 조회 카운트
    @Override
    public Integer merchTransFailDetailListCount(TransFormBean transFormBean) {
        
        return (Integer)getSqlSession().selectOne("Trans.merchTransFailDetailListCount",transFormBean);
        
    }

    //가맹점 신용카드 실패내역 조회
    @Override
    public List<Tb_Tran_Cardpg> merchTransFailDetailList(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.merchTransFailDetailList",transFormBean);
        
    }

    //가맹점 신용카드 실패내역 엑셀다운로드
    @Override
    public List merchTransFailDetailListExcel(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.merchTransFailDetailListExcel",transFormBean);
        
    }
    
        //가맹점 신용카드 실패내역 엑셀다운로드
    @Override
    public List basic_merchTransFailDetailListExcel(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.basic_merchTransFailDetailListExcel",transFormBean);
        
    }

    //가맹점 현금거래 집계 조회 카운트
    @Override
    public Integer merchCashtransMasterListCount(TransFormBean transFormBean) {
        
        return (Integer)getSqlSession().selectOne("Trans.merchCashtransMasterListCount",transFormBean);
        
    }
    
    
    //가맹점 현금거래 집계 조회
    @Override
    public List<Tb_Tran_Cardpg> basic_merchCashtransMasterList(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.basic_merchCashtransMasterList",transFormBean);
        
    }

    //가맹점 현금거래 집계 조회
    @Override
    public List<Tb_Tran_Cardpg> merchCashtransMasterList(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.merchCashtransMasterList",transFormBean);
        
    }
    
    //가맹점 현금거래 집계 엑셀다운로드
    @Override
    public List merchCashTransMasterListExcel(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.merchCashTransMasterListExcel",transFormBean);
        
    }    
    
    //가맹점 현금거래 조회 카운트
    @Override
    public Integer merchCashtransDetailListCount(TransFormBean transFormBean) {
        
        return (Integer)getSqlSession().selectOne("Trans.merchCashtransDetailListCount",transFormBean);
        
    }

    //가맹점 현금거래 조회
    @Override
    public List<Tb_Tran_Cardpg> merchCashtransDetailList(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.merchCashtransDetailList",transFormBean);
        
    }
    
    //가맹점 현금거래 엑셀다운로드
    @Override
    public List merchCashTransDetailListExcel(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.merchCashTransDetailListExcel",transFormBean);
        
    }        

     //가맹점 현금거래 엑셀다운로드
    @Override
    public List basic_merchCashTransDetailListExcel(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.basic_merchCashTransDetailListExcel",transFormBean);
        
    }  
    
    
        //신용카드 이상거래집계 조회 카운트
    @Override
    public Integer transChkMasterListCount(TransFormBean transFormBean) {
        
        return (Integer)getSqlSession().selectOne("Trans.transChkMasterListCount",transFormBean);
        
    }    
    
    //신용카드 이상거래집계 조회
    @Override
    public List<Tb_Tran_Cardpg> transChkMasterList(TransFormBean transFormBean) {
        
        return (List)getSqlSession().selectList("Trans.transChkMasterList",transFormBean);
        
    }
}
