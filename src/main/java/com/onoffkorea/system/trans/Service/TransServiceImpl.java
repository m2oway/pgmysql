/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.trans.Service;

import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.trans.Bean.TransFormBean;
import com.onoffkorea.system.trans.Dao.TransDAO;
import com.onoffkorea.system.trans.Vo.Tb_Tran_Cardpg;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 */
@Service("transService")
public class TransServiceImpl implements TransService{
    
    protected Logger logger = Logger.getLogger(getClass().getName());
    
    @Autowired
    private CommonService commonService;        

    @Autowired
    private TransDAO transDAO;   

    //신용거래 집계 조회
    @Override
    public Hashtable transMasterList(TransFormBean transFormBean) {
        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        
        if((transFormBean.getApp_start_dt() == null) || (transFormBean.getApp_end_dt() == "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            //cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            transFormBean.setApp_start_dt(start_dt);
            transFormBean.setApp_end_dt(end_dt);
        }
        Hashtable ht = new Hashtable();

        try {
            
                //Integer total_count = transDAO.transMasterListCount(transFormBean);
                List<Tb_Tran_Cardpg> ls_transMasterList = transDAO.transMasterList(transFormBean);
                
                StringBuilder sb = new StringBuilder();
                //sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(transFormBean.getStart_no())-1)+"\",\"rows\" : [");
                sb.append("{\"rows\" : [");
                for (int i = 0; i < ls_transMasterList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_transMasterList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"app_dt\":\""+tb_Tran_Cardpg.getApp_dt()+"\"");
                        sb.append(",\"pay_chn_cate\":\""+tb_Tran_Cardpg.getPay_chn_cate()+"\"");
                        sb.append(",\"onffmerch_no\":\""+tb_Tran_Cardpg.getOnffmerch_no()+"\"");
                        sb.append(",\"onfftid\":\""+tb_Tran_Cardpg.getOnfftid()+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append(" \"" + Util.nullToString(tb_Tran_Cardpg.getApp_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_chn_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_tax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_tax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_cnt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_cnt()) + "\"");

                        if (i == (ls_transMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");   
                
                SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
                Date start_date = formatter.parse(transFormBean.getApp_start_dt());
                Date end_date   = formatter.parse(transFormBean.getApp_end_dt());

                String start_dt = formatter2.format(start_date);
                String end_dt = formatter2.format(end_date);

                transFormBean.setApp_start_dt(start_dt);
                transFormBean.setApp_end_dt(end_dt);
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                ht.put("payChnCateList", payChnCateList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }
        

        return ht;                   
        
    }
    
    //신용거래 엑셀다운로드 조회
    @Override
    public void transMasterListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
             
                List ls_transMasterListExcel = transDAO.transMasterListExcel(transFormBean);
                
                StringBuffer transMasterListExcel = Util.makeData(ls_transMasterListExcel);
        
                Util.exceldownload(transMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }
        
    }        

    //신용카드 거래내역 상세 조회
    @Override
    public Hashtable transDetailList(TransFormBean transFormBean) {
        
         SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        if((transFormBean.getApp_start_dt() == null) || (transFormBean.getApp_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            //cal.add(Calendar.WEEK_OF_MONTH, -1);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            transFormBean.setApp_start_dt(start_dt);
            transFormBean.setApp_end_dt(end_dt);
        }
        
        Hashtable ht = new Hashtable();

        try {

                Integer total_count = transDAO.transDetailListCount(transFormBean);
                List<Tb_Tran_Cardpg> ls_transDetailList = transDAO.transDetailList(transFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(transFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_transDetailList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_transDetailList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"tran_seq\":\""+tb_Tran_Cardpg.getTran_seq()+"\"");
                        sb.append(",\"result_cd\":\""+tb_Tran_Cardpg.getResult_cd()+"\"");
                        sb.append(",\"onfftid\":\""+tb_Tran_Cardpg.getOnfftid()+"\"");
                        sb.append(",\"offmerch_no\":\""+tb_Tran_Cardpg.getOnffmerch_no()+"\"");                        
                        sb.append("}");                          
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Tran_Cardpg.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnfftid_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_phone2()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getProduct_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMassagetype_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getIss_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCard_num()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getInstallment()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTot_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getSvc_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_dt()) + Util.nullToString(tb_Tran_Cardpg.getApp_tm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTran_status_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_status_nm()) + "\"");                        
                        
                        //민원
                        if("".equals(Util.nullToString(tb_Tran_Cardpg.getComplain_seq()))){
                            sb.append(" ,\"<button name=complain_insert tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + ">등록</button>\"");
                        }
                        else{
                            sb.append(" ,\"<button name=complain_search tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " complain_seq=" + Util.nullToString(tb_Tran_Cardpg.getComplain_seq()) + ">보기</button>\"");
                            //sb.append(",\"" + "" + "\"");
                        }

                        //영수증
                        if("00".equals(tb_Tran_Cardpg.getResult_status()))
                        {
                            sb.append(",\"<button name=bill_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " tran_result_cd="+tb_Tran_Cardpg.getResult_cd()+" >영수증</button>\"");//영수증
                        }
                        else
                        {
                            sb.append(",\"" + "" + "\"");
                        }
                        
                        //취소버튼
                        if("00".equals(tb_Tran_Cardpg.getResult_status()) && "10".equals(tb_Tran_Cardpg.getMassagetype()) && ("00".equals(tb_Tran_Cardpg.getTran_status())||"05".equals(tb_Tran_Cardpg.getTran_status())))
                        {
                            sb.append(",\"<button name=cncl_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " org_cno="+tb_Tran_Cardpg.getPg_seq()+" >거래취소</button>\"");//영수증
                        }
                        else
                        {
                            sb.append(",\"" + "" + "\"");
                        }
                        
                        
                        //sb.append(",\"" + "" + "\"");//취소
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_chn_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_cd()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_msg()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTid()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_no()) + "\"");    
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getAcq_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCommision()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_amt()) + "\"");                                
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnofftid_commision()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnofftid_commision_vat()) + "\"");  
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getWithhold_tax()) + "\"");    
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnofftid_pay_amt()) + "\"");                                                
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnoffmerch_ext_pay_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getClient_ip()) + "\"");
                        //sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_id()) + "\"");                            
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getIns_user()) + "\"");                            
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOrg_app_dd()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOrg_app_no()) + "\"");                         
                        
                        if (i == (ls_transDetailList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");              
                
            SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
            Date start_date = formatter.parse(transFormBean.getApp_start_dt());
            Date end_date   = formatter.parse(transFormBean.getApp_end_dt());
            
            String start_dt = formatter2.format(start_date);
            String end_dt = formatter2.format(end_date);

            transFormBean.setApp_start_dt(start_dt);
            transFormBean.setApp_end_dt(end_dt);
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("TRAN_STATUS");
                Tb_Code_Main tranStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("RESULT_STATUS");
                Tb_Code_Main resultStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("ISS_CD");
                Tb_Code_Main issCdList = commonService.codeSearch(commonCodeSearchFormBean);    
                
                commonCodeSearchFormBean.setMain_code("ACC_CHK_FLAG");
                Tb_Code_Main AccChkFlagList = commonService.codeSearch(commonCodeSearchFormBean);  
                
                commonCodeSearchFormBean.setMain_code("MASSAGETYPE");
                Tb_Code_Main massagetypeCdList = commonService.codeSearch(commonCodeSearchFormBean);  
                
                ht.put("payChnCateList", payChnCateList);
                ht.put("tranStatusList", tranStatusList);
                ht.put("resultStatusList", resultStatusList);
                ht.put("issCdList", issCdList);
                ht.put("massagetypeCdList", massagetypeCdList);
                ht.put("AccChkFlagList", AccChkFlagList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;             
        
    }
    
    //신용거래 엑셀다운로드 조회
    @Override
    public void transDetailListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
            
                List ls_transDetailListExcel = transDAO.transDetailListExcel(transFormBean);
                
                StringBuffer transDetailListExcel = Util.makeData(ls_transDetailListExcel);
        
                Util.exceldownload(transDetailListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }
        
    }      
    
    //신용거래 실패내역 집계 조회
    @Override
    public Hashtable transFailMasterList(TransFormBean transFormBean) {
        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = transDAO.transFailMasterListCount(transFormBean);
                List<Tb_Tran_Cardpg> ls_transFailMasterList = transDAO.transFailMasterList(transFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(transFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_transFailMasterList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_transFailMasterList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"app_dt\":\""+tb_Tran_Cardpg.getApp_dt()+"\"");
                        sb.append(",\"pay_chn_cate\":\""+tb_Tran_Cardpg.getPay_chn_cate()+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Tran_Cardpg.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_chn_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_tax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_tax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_cnt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_cnt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOrg_pg_seq()) + "\"");

                        if (i == (ls_transFailMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");   
                
                
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                ht.put("payChnCateList", payChnCateList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;                   
        
    }

    //신용카드 실패내역 상세 조회
    @Override
    public Hashtable transFailDetailList(TransFormBean transFormBean) {
        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
         
        if((transFormBean.getApp_start_dt() == null) || (transFormBean.getApp_end_dt() == "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            //cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            transFormBean.setApp_start_dt(start_dt);
            transFormBean.setApp_end_dt(end_dt);
        }
        
        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = transDAO.transFailDetailListCount(transFormBean);
                List<Tb_Tran_Cardpg> ls_transFailDetailList = transDAO.transFailDetailList(transFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(transFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_transFailDetailList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_transFailDetailList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"tran_seq\":\""+tb_Tran_Cardpg.getTran_seq()+"\"");
                        sb.append(",\"result_cd\":\""+tb_Tran_Cardpg.getResult_cd()+"\"");
                        sb.append("}");                          
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Tran_Cardpg.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnfftid_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTran_dt())+Util.nullToString(tb_Tran_Cardpg.getTran_tm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_phone2()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getProduct_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMassagetype_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getIss_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCard_num()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getInstallment()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTot_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getSvc_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_status_nm()) + "\"");                        
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_chn_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_cd()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_msg()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTid()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_no()) + "\"");    
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getAcq_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getClient_ip()) + "\"");
                        
                        if (i == (ls_transFailDetailList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");            
                
                SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
                Date start_date = formatter.parse(transFormBean.getApp_start_dt());
                Date end_date   = formatter.parse(transFormBean.getApp_end_dt());

                String start_dt = formatter2.format(start_date);
                String end_dt = formatter2.format(end_date);

                transFormBean.setApp_start_dt(start_dt);
                transFormBean.setApp_end_dt(end_dt);
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);            
                
                commonCodeSearchFormBean.setMain_code("TRAN_STATUS");
                Tb_Code_Main tranStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("RESULT_STATUS");
                Tb_Code_Main resultStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("ISS_CD");
                Tb_Code_Main issCdList = commonService.codeSearch(commonCodeSearchFormBean);                   
                
                ht.put("payChnCateList", payChnCateList);
                ht.put("tranStatusList", tranStatusList);
                ht.put("resultStatusList", resultStatusList);
                ht.put("issCdList", issCdList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;             
        
    }
    
    //현금거래 집계 조회
    @Override
    public Hashtable cashtransMasterList(TransFormBean transFormBean) {
        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        
        if((transFormBean.getApp_start_dt() == null) || (transFormBean.getApp_end_dt() == "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            //cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            transFormBean.setApp_start_dt(start_dt);
            transFormBean.setApp_end_dt(end_dt);
        }
        
        Hashtable ht = new Hashtable();

        try {
            
                //Integer total_count = transDAO.cashtransMasterListCount(transFormBean);
                List<Tb_Tran_Cardpg> ls_cashtransMasterList = transDAO.cashtransMasterList(transFormBean);
                
                StringBuilder sb = new StringBuilder();
                //sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(transFormBean.getStart_no())-1)+"\",\"rows\" : [");
                sb.append("{\"rows\" : [");
                for (int i = 0; i < ls_cashtransMasterList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_cashtransMasterList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"app_dt\":\""+tb_Tran_Cardpg.getApp_dt()+"\"");
                        sb.append(",\"pay_chn_cate\":\""+tb_Tran_Cardpg.getPay_chn_cate()+"\"");
                        sb.append(",\"onffmerch_no\":\""+tb_Tran_Cardpg.getOnffmerch_no()+"\"");
                        sb.append(",\"onfftid\":\""+tb_Tran_Cardpg.getOnfftid()+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append(" \"" + Util.nullToString(tb_Tran_Cardpg.getApp_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_chn_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_tax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_tax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_cnt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_cnt()) + "\"");

                        if (i == (ls_cashtransMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");
                
                SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
                Date start_date = formatter.parse(transFormBean.getApp_start_dt());
                Date end_date   = formatter.parse(transFormBean.getApp_end_dt());

                String start_dt = formatter2.format(start_date);
                String end_dt = formatter2.format(end_date);

                transFormBean.setApp_start_dt(start_dt);
                transFormBean.setApp_end_dt(end_dt);
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                ht.put("payChnCateList", payChnCateList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;                   
        
    }

    //현금 거래내역 상세 조회
    @Override
    public Hashtable cashtransDetailList(TransFormBean transFormBean) {
        
        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = transDAO.cashtransDetailListCount(transFormBean);
                List<Tb_Tran_Cardpg> ls_cashtransDetailList = transDAO.cashtransDetailList(transFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(transFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_cashtransDetailList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_cashtransDetailList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"tran_seq\":\""+tb_Tran_Cardpg.getTran_seq()+"\"");
                        sb.append(",\"result_cd\":\""+tb_Tran_Cardpg.getResult_cd()+"\"");
                        sb.append("}");                               
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Tran_Cardpg.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnfftid_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_phone2()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getProduct_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMassagetype_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCard_num()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getWcc_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCard_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTot_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getSvc_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_dt()) + Util.nullToString(tb_Tran_Cardpg.getApp_tm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTran_status_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_status_nm()) + "\"");
                        sb.append(",\"" + "" + "\"");//민원
                        
                        //영수증
                        if("00".equals(tb_Tran_Cardpg.getResult_status()))
                        {
                            sb.append(",\"<button name=cashbill_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " tran_result_cd="+tb_Tran_Cardpg.getResult_cd()+" >영수증</button>\"");//영수증
                        }
                        else
                        {
                            sb.append(",\"" + "" + "\"");
                        }
                        
                        //취소버튼
                        if("00".equals(tb_Tran_Cardpg.getResult_status()) && "10".equals(tb_Tran_Cardpg.getMassagetype()) && ("00".equals(tb_Tran_Cardpg.getTran_status()) ))
                        {
                            sb.append(",\"<button name=cncl_cash_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " org_cno="+tb_Tran_Cardpg.getPg_seq()+" >거래취소</button>\"");//영수증
                        }
                        else
                        {
                            sb.append(",\"" + "" + "\"");
                        }
                        
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_chn_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_cd()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_msg()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTid()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getClient_ip()) + "\"");
                        //sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_id()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getIns_user()) + "\"");
                        
                        if (i == (ls_cashtransDetailList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
                
                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("TRAN_STATUS");
                Tb_Code_Main tranStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("RESULT_STATUS");
                Tb_Code_Main resultStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("ISS_CD");
                Tb_Code_Main issCdList = commonService.codeSearch(commonCodeSearchFormBean);                                
                
                ht.put("payChnCateList", payChnCateList);
                ht.put("tranStatusList", tranStatusList);
                ht.put("resultStatusList", resultStatusList);
                ht.put("issCdList", issCdList);                
                
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;             
        
    }    
    
    //현금거래 집계 엑셀다운로드 조회
    @Override
    public void cashTransMasterListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
             
                List ls_cashTransMasterListExcel = transDAO.cashTransMasterListExcel(transFormBean);
                
                StringBuffer cashTransMasterListExcel = Util.makeData(ls_cashTransMasterListExcel);
        
                Util.exceldownload(cashTransMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }
        
    }        
    
    //현금거래 엑셀다운로드 조회
    @Override
    public void cashTransDetailListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response) {
 
        try {
             
                List ls_cashTransDetailListExcel = transDAO.cashTransDetailListExcel(transFormBean);
                
                StringBuffer cashTransMasterListExcel = Util.makeData(ls_cashTransDetailListExcel);
        
                Util.exceldownload(cashTransMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }
        
    }           
    
    //신용거래 실패내역 엑셀다운로드 조회
    @Override
    public void transFailDetailListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response) {
 
        try {
             
                List ls_transFailDetailListExcel = transDAO.transFailDetailListExcel(transFormBean);
                
                StringBuffer transFailDetailListExcel = Util.makeData(ls_transFailDetailListExcel);
        
                Util.exceldownload(transFailDetailListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }
        
    }          

    //가맹점 신용카드 집계 조회
    @Override
    public Hashtable merchTransMasterList(TransFormBean transFormBean) {

        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        
        if((transFormBean.getApp_start_dt() == null) || (transFormBean.getApp_end_dt() == "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            //cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            transFormBean.setApp_start_dt(start_dt);
            transFormBean.setApp_end_dt(end_dt);
        }
        Hashtable ht = new Hashtable();

        try {
            
                //Integer total_count = transDAO.merchTransMasterListCount(transFormBean);
                List<Tb_Tran_Cardpg> ls_transMasterList = transDAO.merchTransMasterList(transFormBean);
                
                StringBuilder sb = new StringBuilder();
                //sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(transFormBean.getStart_no())-1)+"\",\"rows\" : [");
                 sb.append("{\"rows\" : [");
                for (int i = 0; i < ls_transMasterList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_transMasterList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"app_dt\":\""+tb_Tran_Cardpg.getApp_dt()+"\"");
                        sb.append(",\"pay_chn_cate\":\""+tb_Tran_Cardpg.getPay_chn_cate()+"\"");
                        sb.append(",\"onffmerch_no\":\""+tb_Tran_Cardpg.getOnffmerch_no()+"\"");
                        sb.append(",\"onfftid\":\""+tb_Tran_Cardpg.getOnfftid()+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append(" \"" + Util.nullToString(tb_Tran_Cardpg.getApp_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_chn_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnfftid_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_tax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_tax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_cnt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_cnt()) + "\"");

                        if (i == (ls_transMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");   
                
                SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
                Date start_date = formatter.parse(transFormBean.getApp_start_dt());
                Date end_date   = formatter.parse(transFormBean.getApp_end_dt());

                String start_dt = formatter2.format(start_date);
                String end_dt = formatter2.format(end_date);

                transFormBean.setApp_start_dt(start_dt);
                transFormBean.setApp_end_dt(end_dt);
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                ht.put("payChnCateList", payChnCateList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }
        

        return ht;              
        
    }

    //가맹점 신용카드 집계 엑셀다운로드
    @Override
    public void merchTransMasterListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response) {
        
        try {
             
                List ls_transMasterListExcel = transDAO.merchTransMasterListExcel(transFormBean);
                
                StringBuffer transMasterListExcel = Util.makeData(ls_transMasterListExcel);
        
                Util.exceldownload(transMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    }
    
    //가맹점 신용카드 거래내역 상세 조회
    @Override
    public Hashtable merchTransDetailList(TransFormBean transFormBean) {
        
        String strCurDate = Util.getTodayDate();
        
         SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        if((transFormBean.getApp_start_dt() == null) || (transFormBean.getApp_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            //cal.add(Calendar.WEEK_OF_MONTH, -1);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            transFormBean.setApp_start_dt(start_dt);
            transFormBean.setApp_end_dt(end_dt);
        }
        
        Hashtable ht = new Hashtable();

        try {

                Integer total_count = transDAO.merchTransDetailListCount(transFormBean);
                List<Tb_Tran_Cardpg> ls_transDetailList = transDAO.merchTransDetailList(transFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(transFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_transDetailList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_transDetailList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"tran_seq\":\""+tb_Tran_Cardpg.getTran_seq()+"\"");
                        sb.append(",\"result_cd\":\""+tb_Tran_Cardpg.getResult_cd()+"\"");
                        sb.append("}");                          
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Tran_Cardpg.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnfftid_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_phone2()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getProduct_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMassagetype_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getIss_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCard_num()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getInstallment()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTot_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getSvc_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_dt()) + Util.nullToString(tb_Tran_Cardpg.getApp_tm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTran_status_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_status_nm()) + "\"");                        
                        
                        sb.append(",\"" + "" + "\"");//민원   
                        
                        //영수증
                        if("00".equals(tb_Tran_Cardpg.getResult_status()))
                        {
                            sb.append(",\"<button name=bill_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " tran_result_cd="+tb_Tran_Cardpg.getResult_cd()+" >영수증</button>\"");//영수증
                        }
                        else
                        {
                            sb.append(",\"" + "" + "\"");
                        }
                        
                        String strTpsesUserCate = Util.nullToString(transFormBean.getSes_user_cate());
                        String strTpsesmerch_cncl_auth = Util.nullToString(transFormBean.getSes_merch_cncl_auth());
                        String strTpsestid_cncl_auth = Util.nullToString(transFormBean.getSes_tid_cncl_auth());
                        //취소버튼
                        //가맹점 관리자
                        if(strTpsesUserCate.equals("01"))
                        {
                            //결제취소가능 상태일 경우
                            //if("00".equals(tb_Tran_Cardpg.getResult_status()) && "10".equals(tb_Tran_Cardpg.getMassagetype()) && "00".equals(tb_Tran_Cardpg.getTran_status()) )
                            if("00".equals(tb_Tran_Cardpg.getResult_status()) && "10".equals(tb_Tran_Cardpg.getMassagetype()) && ("00".equals(tb_Tran_Cardpg.getTran_status()) || "05".equals(tb_Tran_Cardpg.getTran_status()) ) )
                            {
                                if(strTpsesmerch_cncl_auth.equals("00"))
                                {
                                    sb.append(",\"<button name=cncl_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " org_cno="+tb_Tran_Cardpg.getPg_seq()+" >거래취소</button>\"");
                                }
                                else
                                {              
                                    //승인일과 취소일이 같은 경우  
                                    //당일취소의 경우
                                    if(strCurDate.equals(tb_Tran_Cardpg.getApp_dt()))
                                    {
                                        sb.append(",\"<button name=cncl_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " org_cno="+tb_Tran_Cardpg.getPg_seq()+" >거래취소</button>\"");
                                    }
                                    //익일취소의 경우
                                    else
                                    {
                                        sb.append(",\"<button name=cnclreq_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " org_cno="+tb_Tran_Cardpg.getPg_seq()+" >취소요청</button>\"");
                                    }
                                }
                            }
                            else
                            {
                                sb.append(",\"" + "" + "\"");
                            }

                        }
                        //결제관리자
                        else if(strTpsesUserCate.equals("02"))
                        {
                            //결제취소가능 상태일 경우
                            if("00".equals(tb_Tran_Cardpg.getResult_status()) && "10".equals(tb_Tran_Cardpg.getMassagetype()) && "00".equals(tb_Tran_Cardpg.getTran_status()) )
                            {

                                if(strTpsestid_cncl_auth.equals("00"))
                                {
                                    sb.append(",\"<button name=cncl_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " org_cno="+tb_Tran_Cardpg.getPg_seq()+" >거래취소</button>\"");//영수증
                                }
                                else
                                {       
                                     //승인일과 취소일이 같은 경우  
                                    //당일취소일경우
                                    if(strCurDate.equals(tb_Tran_Cardpg.getApp_dt()))
                                    {
                                        sb.append(",\"<button name=cncl_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " org_cno="+tb_Tran_Cardpg.getPg_seq()+" >거래취소</button>\"");
                                    }
                                    //익일취소일 경우
                                    else
                                    {
                                        sb.append(",\"<button name=cnclreq_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " org_cno="+tb_Tran_Cardpg.getPg_seq()+" >취소요청</button>\"");                                        
                                    }
                                }
                            }
                            else
                            {
                                sb.append(",\"" + "" + "\"");
                            }                            
                        }
                        //대리점관리자
                        else if(strTpsesUserCate.equals("03"))
                        {
                             sb.append(",\"" + "" + "\"");
                        }
                        
                        
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_chn_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOrg_app_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOrg_app_dd()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getIns_user()) + "\"");
                        
                        if (i == (ls_transDetailList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");              
                
            SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
            Date start_date = formatter.parse(transFormBean.getApp_start_dt());
            Date end_date   = formatter.parse(transFormBean.getApp_end_dt());
            
            String start_dt = formatter2.format(start_date);
            String end_dt = formatter2.format(end_date);

            transFormBean.setApp_start_dt(start_dt);
            transFormBean.setApp_end_dt(end_dt);
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("TRAN_STATUS");
                Tb_Code_Main tranStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("RESULT_STATUS");
                Tb_Code_Main resultStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("ISS_CD");
                Tb_Code_Main issCdList = commonService.codeSearch(commonCodeSearchFormBean); 
                
                commonCodeSearchFormBean.setMain_code("MASSAGETYPE");
                Tb_Code_Main massagetypeCdList = commonService.codeSearch(commonCodeSearchFormBean);  
                
                ht.put("payChnCateList", payChnCateList);
                ht.put("tranStatusList", tranStatusList);
                ht.put("resultStatusList", resultStatusList);
                ht.put("issCdList", issCdList);
                ht.put("massagetypeCdList", massagetypeCdList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;           
        
    }    
    
  
    //가맹점 신용카드 거래내역 상세 조회
    @Override
    public Hashtable basic_merchTransDetailList(TransFormBean transFormBean) {
        
        String strCurDate = Util.getTodayDate();
        
         SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        if((transFormBean.getApp_start_dt() == null) || (transFormBean.getApp_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            transFormBean.setApp_start_dt(start_dt);
            transFormBean.setApp_end_dt(end_dt);
        }
        
        Hashtable ht = new Hashtable();

        try {

                Integer total_count = transDAO.merchTransDetailListCount(transFormBean);
                List<Tb_Tran_Cardpg> ls_transDetailList = transDAO.merchTransDetailList(transFormBean);
                List<Tb_Tran_Cardpg> ls_transTotList = transDAO.basic_merchTransMasterList(transFormBean);
                
                Tb_Tran_Cardpg tb_Tot_Tran_Cardpg = ls_transTotList.get(0);
                
                String strTpTotAppCnt = Util.nullToInt(tb_Tot_Tran_Cardpg.getApp_cnt());
                String strTpTotAppAmt = Util.nullToInt(tb_Tot_Tran_Cardpg.getApp_amt());
                String strTpTotCnclCnt = Util.nullToInt(tb_Tot_Tran_Cardpg.getCncl_cnt());
                String strTpTotCnclAmt = Util.nullToInt(tb_Tot_Tran_Cardpg.getCncl_amt());
                
                ht.put("total_appcnt", strTpTotAppCnt);
                ht.put("total_appamt", strTpTotAppAmt);
                ht.put("total_cnclcnt", strTpTotCnclCnt);
                ht.put("total_cnclamt", strTpTotCnclAmt);
                        
                String strTpsesUserCate = Util.nullToString(transFormBean.getSes_user_cate());
                String strTpsesmerch_cncl_auth = Util.nullToString(transFormBean.getSes_merch_cncl_auth());
                String strTpsestid_cncl_auth = Util.nullToString(transFormBean.getSes_tid_cncl_auth());

                ht.put("total_count", String.valueOf(total_count.intValue()));
                ht.put("ls_transDetailList", ls_transDetailList);
                
                ht.put("parma_usercate", strTpsesUserCate);
                ht.put("parma_merch_cncl_auth", strTpsesmerch_cncl_auth);
                ht.put("parma_tid_cncl_auth", strTpsestid_cncl_auth);
                
                SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
                Date start_date = formatter.parse(transFormBean.getApp_start_dt().replaceAll("-", ""));
                Date end_date   = formatter.parse(transFormBean.getApp_end_dt().replaceAll("-", ""));

                String start_dt = formatter2.format(start_date);
                String end_dt = formatter2.format(end_date);

                transFormBean.setApp_start_dt(start_dt);
                transFormBean.setApp_end_dt(end_dt);
            
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("TRAN_STATUS");
                Tb_Code_Main tranStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("RESULT_STATUS");
                Tb_Code_Main resultStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("ISS_CD");
                Tb_Code_Main issCdList = commonService.codeSearch(commonCodeSearchFormBean); 
                
                commonCodeSearchFormBean.setMain_code("MASSAGETYPE");
                Tb_Code_Main massagetypeCdList = commonService.codeSearch(commonCodeSearchFormBean);  
                
                ht.put("payChnCateList", payChnCateList);
                ht.put("tranStatusList", tranStatusList);
                ht.put("resultStatusList", resultStatusList);
                ht.put("issCdList", issCdList);
                ht.put("massagetypeCdList", massagetypeCdList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;           
        
    }       

    //가맹점 신용카드 상세 내역 엑셀다운로드
    @Override
    public void merchTransDetailListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
            List ls_transDetailListExcel = transDAO.merchTransDetailListExcel(transFormBean);
            
            StringBuffer transDetailListExcel = Util.makeData(ls_transDetailListExcel);
            
            Util.exceldownload(transDetailListExcel.toString(), excelDownloadFilename, response);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(TransServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    
        //가맹점 신용카드 상세 내역 엑셀다운로드
    @Override
    public void basic_merchTransDetailListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
            List ls_transDetailListExcel = transDAO.basic_merchTransDetailListExcel(transFormBean);
            
            StringBuffer transDetailListExcel = Util.makeData(ls_transDetailListExcel);
            
            Util.exceldownload(transDetailListExcel.toString(), excelDownloadFilename, response);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(TransServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    //가맹점 신용카드 실패내역 조회
    @Override
    public Hashtable merchTransFailDetailList(TransFormBean transFormBean) {

        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
         
        if((transFormBean.getApp_start_dt() == null) || (transFormBean.getApp_end_dt() == "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            //cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            transFormBean.setApp_start_dt(start_dt);
            transFormBean.setApp_end_dt(end_dt);
        }
        
        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = transDAO.merchTransFailDetailListCount(transFormBean);
                List<Tb_Tran_Cardpg> ls_transFailDetailList = transDAO.merchTransFailDetailList(transFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(transFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_transFailDetailList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_transFailDetailList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"tran_seq\":\""+tb_Tran_Cardpg.getTran_seq()+"\"");
                        sb.append(",\"result_cd\":\""+tb_Tran_Cardpg.getResult_cd()+"\"");
                        sb.append("}");                          
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Tran_Cardpg.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnfftid_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTran_dt())+Util.nullToString(tb_Tran_Cardpg.getTran_tm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_phone2()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getProduct_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMassagetype_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getIss_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCard_num()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getInstallment()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTot_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getSvc_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_status_nm()) + "\"");                        
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_chn_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_cd()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_msg()) + "\"");
                        //sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTid()) + "\"");
                        //sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_no()) + "\"");    
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getAcq_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getClient_ip()) + "\"");                       
                        
                        if (i == (ls_transFailDetailList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");            
                
                SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
                Date start_date = formatter.parse(transFormBean.getApp_start_dt());
                Date end_date   = formatter.parse(transFormBean.getApp_end_dt());

                String start_dt = formatter2.format(start_date);
                String end_dt = formatter2.format(end_date);

                transFormBean.setApp_start_dt(start_dt);
                transFormBean.setApp_end_dt(end_dt);
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);            
                
                commonCodeSearchFormBean.setMain_code("TRAN_STATUS");
                Tb_Code_Main tranStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("RESULT_STATUS");
                Tb_Code_Main resultStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("ISS_CD");
                Tb_Code_Main issCdList = commonService.codeSearch(commonCodeSearchFormBean);                   
                
                ht.put("payChnCateList", payChnCateList);
                ht.put("tranStatusList", tranStatusList);
                ht.put("resultStatusList", resultStatusList);
                ht.put("issCdList", issCdList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;                     
        
    }
    
    

    //가맹점 신용카드 실패내역 조회
    @Override
    public Hashtable basic_merchTransFailDetailList(TransFormBean transFormBean) {

        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
         
        if((transFormBean.getApp_start_dt() == null) || (transFormBean.getApp_end_dt() == "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            //cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            transFormBean.setApp_start_dt(start_dt);
            transFormBean.setApp_end_dt(end_dt);
        }
        
        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = transDAO.merchTransFailDetailListCount(transFormBean);
                List<Tb_Tran_Cardpg> ls_transFailDetailList = transDAO.merchTransFailDetailList(transFormBean);
                          
                
                SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
                Date start_date = formatter.parse(transFormBean.getApp_start_dt().replaceAll("-", ""));
                Date end_date   = formatter.parse(transFormBean.getApp_end_dt().replaceAll("-", ""));

                String start_dt = formatter2.format(start_date);
                String end_dt = formatter2.format(end_date);

                transFormBean.setApp_start_dt(start_dt);
                transFormBean.setApp_end_dt(end_dt);
                
                ht.put("total_count", String.valueOf(total_count.intValue()));
                ht.put("ls_transFailDetailList", ls_transFailDetailList);
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);            
                
                commonCodeSearchFormBean.setMain_code("TRAN_STATUS");
                Tb_Code_Main tranStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("RESULT_STATUS");
                Tb_Code_Main resultStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("ISS_CD");
                Tb_Code_Main issCdList = commonService.codeSearch(commonCodeSearchFormBean);                   
                
                ht.put("payChnCateList", payChnCateList);
                ht.put("tranStatusList", tranStatusList);
                ht.put("resultStatusList", resultStatusList);
                ht.put("issCdList", issCdList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;                     
        
    }    

    //가맹점 신용카드 실패내역 엑셀다운로드
    @Override
    public void merchTransFailDetailListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response) {
        
        try {
            List ls_transFailDetailListExcel = transDAO.merchTransFailDetailListExcel(transFormBean);
            
            StringBuffer transFailDetailListExcel = Util.makeData(ls_transFailDetailListExcel);
            
            Util.exceldownload(transFailDetailListExcel.toString(), excelDownloadFilename, response);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(TransServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
    }
    
    //가맹점 신용카드 실패내역 엑셀다운로드
    @Override
    public void basic_merchTransFailDetailListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response) {
        
        try {
            List ls_transFailDetailListExcel = transDAO.basic_merchTransFailDetailListExcel(transFormBean);
            
            StringBuffer transFailDetailListExcel = Util.makeData(ls_transFailDetailListExcel);
            
            Util.exceldownload(transFailDetailListExcel.toString(), excelDownloadFilename, response);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(TransServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
    }    

    //가맹점 현금거래 집계 조회
    @Override
    public Hashtable merchCashtransMasterList(TransFormBean transFormBean) {

        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        
        if((transFormBean.getApp_start_dt() == null) || (transFormBean.getApp_end_dt() == "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            //cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            transFormBean.setApp_start_dt(start_dt);
            transFormBean.setApp_end_dt(end_dt);
        }
        
        Hashtable ht = new Hashtable();

        try {
            
               // Integer total_count = transDAO.merchCashtransMasterListCount(transFormBean);
                List<Tb_Tran_Cardpg> ls_cashtransMasterList = transDAO.merchCashtransMasterList(transFormBean);
                
                StringBuilder sb = new StringBuilder();
                //sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(transFormBean.getStart_no())-1)+"\",\"rows\" : [");
                sb.append("{\"rows\" : [");
                for (int i = 0; i < ls_cashtransMasterList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_cashtransMasterList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"app_dt\":\""+tb_Tran_Cardpg.getApp_dt()+"\"");
                        sb.append(",\"pay_chn_cate\":\""+tb_Tran_Cardpg.getPay_chn_cate()+"\"");
                        sb.append(",\"onffmerch_no\":\""+tb_Tran_Cardpg.getOnffmerch_no()+"\"");
                        sb.append(",\"onfftid\":\""+tb_Tran_Cardpg.getOnfftid()+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append(" \"" + Util.nullToString(tb_Tran_Cardpg.getApp_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_chn_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnfftid_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_tax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_tax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_cnt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_cnt()) + "\"");

                        if (i == (ls_cashtransMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");
                
                SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
                Date start_date = formatter.parse(transFormBean.getApp_start_dt());
                Date end_date   = formatter.parse(transFormBean.getApp_end_dt());

                String start_dt = formatter2.format(start_date);
                String end_dt = formatter2.format(end_date);

                transFormBean.setApp_start_dt(start_dt);
                transFormBean.setApp_end_dt(end_dt);
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                ht.put("payChnCateList", payChnCateList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;               
        
    }
    
    //가맹점 현금거래 집계 엑셀다운로드
    @Override
    public void merchCashTransMasterListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response) {
        
        try {
             
                List ls_cashTransDetailListExcel = transDAO.merchCashTransMasterListExcel(transFormBean);
                
                StringBuffer cashTransMasterListExcel = Util.makeData(ls_cashTransDetailListExcel);
        
                Util.exceldownload(cashTransMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }
        
    }    

    //가맹점 현금거래 상세 조회
    @Override
    public Hashtable merchCashtransDetailList(TransFormBean transFormBean) {
        
        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = transDAO.merchCashtransDetailListCount(transFormBean);
                List<Tb_Tran_Cardpg> ls_cashtransDetailList = transDAO.merchCashtransDetailList(transFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(transFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_cashtransDetailList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_cashtransDetailList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"tran_seq\":\""+tb_Tran_Cardpg.getTran_seq()+"\"");
                        sb.append(",\"result_cd\":\""+tb_Tran_Cardpg.getResult_cd()+"\"");
                        sb.append("}");                               
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Tran_Cardpg.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnfftid_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_phone2()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getProduct_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMassagetype_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCard_num()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getWcc_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCard_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTot_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getSvc_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_dt()) + Util.nullToString(tb_Tran_Cardpg.getApp_tm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTran_status_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_status_nm()) + "\"");
                        
                        //sb.append(",\"" + "" + "\"");
                        //sb.append(",\"" + "" + "\"");
                        //sb.append(",\"" + "" + "\"");
                        
                        sb.append(",\"" + "" + "\"");//민원   
                        
                        //영수증
                        if("00".equals(tb_Tran_Cardpg.getResult_status()))
                        {
                            sb.append(",\"<button name=cashbill_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " tran_result_cd="+tb_Tran_Cardpg.getResult_cd()+" >영수증</button>\"");//영수증
                        }
                        else
                        {
                            sb.append(",\"" + "" + "\"");
                        }
                        
                        String strTpsesUserCate = Util.nullToString(transFormBean.getSes_user_cate());
                        String strTpsesmerch_cncl_auth = Util.nullToString(transFormBean.getSes_merch_cncl_auth());
                        String strTpsestid_cncl_auth = Util.nullToString(transFormBean.getSes_tid_cncl_auth());
                        //취소버튼
                        //가맹점 관리자
                        if(strTpsesUserCate.equals("01"))
                        {
                            //결제취소가능 상태일 경우
                            if("00".equals(tb_Tran_Cardpg.getResult_status()) && "10".equals(tb_Tran_Cardpg.getMassagetype()) && ("00".equals(tb_Tran_Cardpg.getTran_status())||"05".equals(tb_Tran_Cardpg.getTran_status())))
                            {
                                //if(strTpsesmerch_cncl_auth.equals("00"))
                                //{
                                    sb.append(",\"<button name=cncl_cash_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " org_cno="+tb_Tran_Cardpg.getPg_seq()+" >거래취소</button>\"");//영수증
                                //}
                                //else
                                //{                                
                                //    sb.append(",\"<button name=cnclreq_cash_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " org_cno="+tb_Tran_Cardpg.getPg_seq()+" >거래취소</button>\"");//영수증
                                //}
                            }
                            else
                            {
                                sb.append(",\"" + "" + "\"");
                            }

                        }
                        //결제관리자
                        else if(strTpsesUserCate.equals("02"))
                        {
                            //결제취소가능 상태일 경우
                            if("00".equals(tb_Tran_Cardpg.getResult_status()) && "10".equals(tb_Tran_Cardpg.getMassagetype()) && ("00".equals(tb_Tran_Cardpg.getTran_status())||"05".equals(tb_Tran_Cardpg.getTran_status())))
                            {
                                //if(strTpsesmerch_cncl_auth.equals("00"))
                               // {
                                    sb.append(",\"<button name=cncl_cash_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " org_cno="+tb_Tran_Cardpg.getPg_seq()+" >거래취소</button>\"");//영수증
                              //  }
                              ///  else
                               // {                                
                              //      sb.append(",\"<button name=cnclreq_cash_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " org_cno="+tb_Tran_Cardpg.getPg_seq()+" >거래취소</button>\"");//영수증
                              //  }
                            }
                            else
                            {
                                sb.append(",\"" + "" + "\"");
                            }                            
                        }
                        //대리점관리자
                        else if(strTpsesUserCate.equals("03"))
                        {
                             sb.append(",\"" + "" + "\"");
                        }                        
                        
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_chn_cate_nm()) + "\"");
                        //sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_cd()) + "\"");
                        //sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_msg()) + "\"");
                        //sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTid()) + "\"");
                        //sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getClient_ip()) + "\"");
                        //sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_id()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getIns_user()) + "\"");
                        
                        if (i == (ls_cashtransDetailList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                ht.put("payChnCateList", payChnCateList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;            
        
    }

    @Override
    public void merchCashTransDetailListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response) {
        
        try {
             
                List ls_cashTransDetailListExcel = transDAO.merchCashTransDetailListExcel(transFormBean);
                
                StringBuffer cashTransMasterListExcel = Util.makeData(ls_cashTransDetailListExcel);
        
                Util.exceldownload(cashTransMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    }
    
    @Override
    public void basic_merchCashTransDetailListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response) {
        
        try {
             
                List ls_cashTransDetailListExcel = transDAO.basic_merchCashTransDetailListExcel(transFormBean);
                
                StringBuffer cashTransMasterListExcel = Util.makeData(ls_cashTransDetailListExcel);
        
                Util.exceldownload(cashTransMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    }    
    
    
    //가맹점 현금거래 상세 조회
    @Override
    public Hashtable basic_merchCashtransDetailList(TransFormBean transFormBean) {
        
        Hashtable ht = new Hashtable();
        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );

        try {
        
                if((transFormBean.getApp_start_dt() == null) || (transFormBean.getApp_end_dt() == "")){
                    Date now = new Date();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(now);

                    Date currentTime = new Date ( );
                    String start_dt = formatter.format ( cal.getTime());
                    String end_dt = formatter.format(currentTime.getTime());


                    transFormBean.setApp_start_dt(start_dt);
                    transFormBean.setApp_end_dt(end_dt);
                }            
            
                Integer total_count = transDAO.merchCashtransDetailListCount(transFormBean);
                
                List<Tb_Tran_Cardpg> ls_cashtransMasterList = transDAO.basic_merchCashtransMasterList(transFormBean);
                List<Tb_Tran_Cardpg> ls_cashtransDetailList = transDAO.merchCashtransDetailList(transFormBean);
                   
                Tb_Tran_Cardpg tb_Tot_Tran_Cardpg = ls_cashtransMasterList.get(0);
                
                String strTpTotAppCnt = Util.nullToInt(tb_Tot_Tran_Cardpg.getApp_cnt());
                String strTpTotAppAmt = Util.nullToInt(tb_Tot_Tran_Cardpg.getApp_amt());
                String strTpTotCnclCnt = Util.nullToInt(tb_Tot_Tran_Cardpg.getCncl_cnt());
                String strTpTotCnclAmt = Util.nullToInt(tb_Tot_Tran_Cardpg.getCncl_amt());
                
                String strTpsesUserCate = Util.nullToString(transFormBean.getSes_user_cate());
                String strTpsesmerch_cncl_auth = Util.nullToString(transFormBean.getSes_merch_cncl_auth());
                String strTpsestid_cncl_auth = Util.nullToString(transFormBean.getSes_tid_cncl_auth());

                ht.put("total_count", String.valueOf(total_count.intValue()));
                
                ht.put("total_appcnt", strTpTotAppCnt);
                ht.put("total_appamt", strTpTotAppAmt);
                ht.put("total_cnclcnt", strTpTotCnclCnt);
                ht.put("total_cnclamt", strTpTotCnclAmt);
                                
                ht.put("parma_usercate", strTpsesUserCate);
                ht.put("parma_merch_cncl_auth", strTpsesmerch_cncl_auth);
                ht.put("parma_tid_cncl_auth", strTpsestid_cncl_auth);
                
                ht.put("ls_cashtransDetailList", ls_cashtransDetailList);
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                ht.put("payChnCateList", payChnCateList);
                
                
                SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
                Date start_date = formatter.parse(transFormBean.getApp_start_dt().replaceAll("-", ""));
                Date end_date   = formatter.parse(transFormBean.getApp_end_dt().replaceAll("-", ""));

                String start_dt = formatter2.format(start_date);
                String end_dt = formatter2.format(end_date);

                transFormBean.setApp_start_dt(start_dt);
                transFormBean.setApp_end_dt(end_dt);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;            
        
    }

    //신용이상거래 집계 조회
    @Override
    public Hashtable transChkMasterList(TransFormBean transFormBean) {
        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        
        if((transFormBean.getApp_start_dt() == null) || (transFormBean.getApp_end_dt() == "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            //cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            transFormBean.setApp_start_dt(start_dt);
            transFormBean.setApp_end_dt(end_dt);
        }
        
        if((transFormBean.getChktran_cnt()== null) || (transFormBean.getChktran_cnt() == "")){
                transFormBean.setChktran_cnt("3");
        }        
        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = transDAO.transChkMasterListCount(transFormBean);
                List<Tb_Tran_Cardpg> ls_transMasterList = transDAO.transChkMasterList(transFormBean);
                
                StringBuilder sb = new StringBuilder();
                String cardNum=null;
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(transFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_transMasterList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_transMasterList.get(i);
                        
                        if(!"".equals(Util.nullToString(tb_Tran_Cardpg.getCard_num()))){
                            cardNum = tb_Tran_Cardpg.getCard_num().substring(0, 6)+"******"+tb_Tran_Cardpg.getCard_num().substring(12);
                        }else {
                            cardNum="";
                        }
                        
                        
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"app_dt\":\""+tb_Tran_Cardpg.getApp_dt()+"\"");
                        sb.append(",\"onffmerch_no\":\""+tb_Tran_Cardpg.getOnffmerch_no()+"\"");
                        sb.append(",\"onfftid\":\""+tb_Tran_Cardpg.getOnfftid()+"\"");
                        sb.append(",\"massagetype\":\""+tb_Tran_Cardpg.getMassagetype()+"\"");
//                        sb.append(",\"card_num\":\""+cardNum+"\"");
                        sb.append(",\"card_num\":\""+tb_Tran_Cardpg.getCard_num()+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append(" \"" + Util.nullToString(tb_Tran_Cardpg.getApp_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnfftid_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMassagetype_nm()) + "\"");
//                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCard_num()) + "\"");
                        sb.append(",\"" + cardNum + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getChktrancnt()) + "\"");

                        if (i == (ls_transMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");   
                
                SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
                Date start_date = formatter.parse(transFormBean.getApp_start_dt());
                Date end_date   = formatter.parse(transFormBean.getApp_end_dt());

                String start_dt = formatter2.format(start_date);
                String end_dt = formatter2.format(end_date);

                transFormBean.setApp_start_dt(start_dt);
                transFormBean.setApp_end_dt(end_dt);
            
                ht.put("ResultSet", sb.toString());
                
        } catch (Exception e) {
                e.printStackTrace();
        }
        

        return ht;                   
        
    }
    
}
