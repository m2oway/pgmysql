/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.trans.Service;

import com.onoffkorea.system.trans.Bean.TransFormBean;
import java.util.Hashtable;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public interface TransService {

    public Hashtable transMasterList(TransFormBean transFormBean);

    public Hashtable transDetailList(TransFormBean transFormBean);
    
    public Hashtable merchTransDetailList(TransFormBean transFormBean);
    
    public Hashtable basic_merchTransDetailList(TransFormBean transFormBean);

    public void basic_merchTransDetailListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response);
    
    public Hashtable transFailMasterList(TransFormBean transFormBean);

    public Hashtable transFailDetailList(TransFormBean transFormBean);

    public Hashtable cashtransMasterList(TransFormBean transFormBean);

    public Hashtable cashtransDetailList(TransFormBean transFormBean);

    public void transMasterListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response);

    public void transDetailListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response);

    public void cashTransMasterListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response);

    public void cashTransDetailListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response);

    public void transFailDetailListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response);

    public Hashtable merchTransMasterList(TransFormBean transFormBean);

    public void merchTransMasterListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response);

    public void merchTransDetailListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response);
        
    public Hashtable basic_merchTransFailDetailList(TransFormBean transFormBean);

    public Hashtable merchTransFailDetailList(TransFormBean transFormBean);

    public void merchTransFailDetailListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response);
    
    public void basic_merchTransFailDetailListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response);

    public Hashtable merchCashtransMasterList(TransFormBean transFormBean);

    public void merchCashTransMasterListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response);

    public Hashtable merchCashtransDetailList(TransFormBean transFormBean);
    
    public Hashtable basic_merchCashtransDetailList(TransFormBean transFormBean);

    public void merchCashTransDetailListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response);
    
    public void basic_merchCashTransDetailListExcel(TransFormBean transFormBean, String excelDownloadFilename, HttpServletResponse response);
    
    public Hashtable transChkMasterList(TransFormBean transFormBean) ;
    
}
