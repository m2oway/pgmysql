/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.user.Service;

import com.onoffkorea.system.user.Bean.UserFormBean;
import com.onoffkorea.system.user.Dao.UserDAO;
import java.util.Hashtable;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service("userService")
public class UserServiceImpl  implements UserService{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private UserDAO userDAO;

    //코드 정보 관리
    @Override
    public Hashtable userMasterList(UserFormBean userFormBean) {
        
        Hashtable ht = new Hashtable();

        try {
                ht.put("totalCount", this.userDAO.userMasterListCount(userFormBean));
                ht.put("ResultSet", this.userDAO.userMasterList(userFormBean));
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;
    }
     
    //코드 정보 삭제
    @Override
    @Transactional
    public void userMasterDelete(UserFormBean userFormBean) {
        
        userDAO.userMasterDelete(userFormBean);
        
    }
    
  

    //코드 정보 등록
    @Override
    @Transactional
    public void userMasterInsert(UserFormBean userFormBean) {
        
        userDAO.userMasterInsert(userFormBean);
        
    }
 

    //코드 정보 수정
    @Override
    @Transactional
    public String userMasterUpdate(UserFormBean userFormBean) {
         //조직코드 중복체크
        //Integer user_seq_cnt = userDAO.usermasterCheck(userFormBean);
        
        //if(user_seq_cnt == 1){
            //조직 등록
        Integer intReturn = userDAO.userMasterUpdate(userFormBean);
        //}

        String strReturnval = null;
        if(intReturn != null)
        {
            strReturnval = String.valueOf(intReturn.intValue());
        }
        else
        {
            strReturnval = "-1";
        }
        //return user_seq_cnt.toString(); 
        return strReturnval;
        
    } 

}

    

