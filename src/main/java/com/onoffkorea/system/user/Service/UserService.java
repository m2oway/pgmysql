/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.user.Service;

import com.onoffkorea.system.user.Bean.UserFormBean;
import java.util.Hashtable;

/**
 *
 * @author Administrator
 */
public interface UserService {

    public Hashtable  userMasterList(UserFormBean userFormBean);
    
    public void  userMasterDelete(UserFormBean userFormBean);   

    public void  userMasterInsert(UserFormBean userFormBean);    

    public String  userMasterUpdate(UserFormBean userFormBean);
   
}