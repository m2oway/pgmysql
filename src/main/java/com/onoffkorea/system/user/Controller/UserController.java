/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.user.Controller;

import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.user.Bean.UserFormBean;
import com.onoffkorea.system.user.Service.UserService;
import com.onoffkorea.system.company.Bean.CompanyFormBean;
import com.onoffkorea.system.company.Service.CompanyService;
import com.onoffkorea.system.auth.Bean.AuthFormBean;
import com.onoffkorea.system.auth.Service.AuthService;
import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.user.Vo.Tb_User;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/user/**")
public class UserController {
        
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private UserService userService;
    
    @Autowired
    private CompanyService companyService;
    
    @Autowired
    private AuthService authService;
    
    @Autowired
    private CommonService commonService;    
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }    
    
     //회원 정보 조회
     @RequestMapping(method= RequestMethod.GET,value = "/userMasterList")
    public String userMasterList(@Valid UserFormBean userFormBean, @Valid CompanyFormBean companyFormBean, @Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        userFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            userFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            userFormBean.setSes_onfftid(siteSession.getSes_onfftid());
            userFormBean.setUser_seq(siteSession.getUser_seq());
        }else if("03".equals(siteSession.getSes_user_cate())){
            userFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
            userFormBean.setUser_seq(siteSession.getUser_seq());
        }                
        
        Hashtable ht_userMasterList = userService.userMasterList(userFormBean);
        List ls_userMasterList = (List) ht_userMasterList.get("ResultSet");
        Integer total_count = (Integer) ht_userMasterList.get("totalCount");
        StringBuilder cc = new StringBuilder();
        cc.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(userFormBean.getStart_no())-1)+"\",\"rows\" : [");
        for (int i = 0; i < ls_userMasterList.size(); i++) {
                Tb_User tb_User = (Tb_User) ls_userMasterList.get(i);
                cc.append("{\"id\":" + i);
                cc.append(",\"userdata\":{");                   
                cc.append(" \"user_seq\":\""+Util.nullToString(tb_User.getUser_seq())+"\"");
                cc.append(", \"auth_seq\":\""+Util.nullToString(tb_User.getAuth_seq())+"\"");
                cc.append("}");
                cc.append(",\"data\":[");
                cc.append(" \"" + Util.nullToString(tb_User.getRnum()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_User.getUser_id()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_User.getUser_nm()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_User.getAuth_name()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_User.getCode_user_cate_nm()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_User.getAgent_nm()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_User.getMerch_nm()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_User.getOnfftid_nm()) + "\"");                  
                cc.append(",\"" + Util.nullToString(tb_User.getTel_no_1()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_User.getTel_no_2()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_User.getUser_email()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_User.getIns_dt()) + "\"");
 
                if (i == (ls_userMasterList.size() - 1)) {
                        cc.append("]}");
                } else {
                        cc.append("]},");
                }
        }

        cc.append("]}");    
        model.addAttribute("userMasterList",cc.toString());
        return null;
        
    }
    
    //회원 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/userMasterList")
    public @ResponseBody String userMasterList(@Valid UserFormBean userFormBean, @Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        userFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            userFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            userFormBean.setSes_onfftid(siteSession.getSes_onfftid());
            userFormBean.setUser_seq(siteSession.getUser_seq());
        }else if("03".equals(siteSession.getSes_user_cate())){
            userFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
            userFormBean.setUser_seq(siteSession.getUser_seq());
        }               
        
        Hashtable ht_userMasterList = userService.userMasterList(userFormBean);
        List ls_userMasterList = (List) ht_userMasterList.get("ResultSet");
        Integer total_count = (Integer) ht_userMasterList.get("totalCount");
        StringBuilder cc = new StringBuilder();
        cc.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(userFormBean.getStart_no())-1)+"\",\"rows\" : [");
        for (int i = 0; i < ls_userMasterList.size(); i++) {
                Tb_User tb_User = (Tb_User) ls_userMasterList.get(i);
                cc.append("{\"id\":" + i);
                cc.append(",\"userdata\":{");                   
                cc.append(" \"user_seq\":\""+Util.nullToString(tb_User.getUser_seq())+"\"");
                cc.append(", \"auth_seq\":\""+Util.nullToString(tb_User.getAuth_seq())+"\"");
                cc.append("}");
                cc.append(",\"data\":[");
                cc.append(" \"" + Util.nullToString(tb_User.getRnum()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_User.getUser_id()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_User.getUser_nm()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_User.getAuth_name()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_User.getCode_user_cate_nm()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_User.getAgent_nm()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_User.getMerch_nm()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_User.getOnfftid_nm()) + "\"");                
                cc.append(",\"" + Util.nullToString(tb_User.getTel_no_1()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_User.getTel_no_2()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_User.getUser_email()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_User.getIns_dt()) + "\"");
 
                if (i == (ls_userMasterList.size() - 1)) {
                        cc.append("]}");
                } else {
                        cc.append("]},");
                }
        }

        cc.append("]}");    
        
     System.out.println(cc);
        return cc.toString();
        
        }
    
    
     
    
    //회원 정보 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/userMasterDelete")
    public @ResponseBody String userMasterDelete(@Valid UserFormBean userFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {
             
        userFormBean.setUser_seq(dentalHopitalSession.getUser_seq());
        userService.userMasterDelete(userFormBean);
        
        return null;
        
    }
    
    
    //회원 정보 등록
    @RequestMapping(method= RequestMethod.GET,value = "/userMasterInsert")
    public String userMasterInsertForm(@Valid UserFormBean userFormBean, Model model, @Valid CompanyFormBean companyFormBean,@Valid AuthFormBean authFormBean, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        StringBuilder sb = new StringBuilder();
        sb.append("{rows : []}");
        
        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        commonCodeSearchFormBean.setMain_code("USER_CATE");
        Tb_Code_Main userCateList = commonService.codeSearch(commonCodeSearchFormBean);           
        
        model.addAttribute("userMasterList",sb.toString());      
        model.addAttribute("userCateList",userCateList);
        
        return null;
        
    }   
    
    
    //회원 정보 등록
    @RequestMapping(method= RequestMethod.POST,value = "/userMasterInsert")
    public @ResponseBody String userMasterInsert(@Valid UserFormBean userFormBean, @Valid CompanyFormBean companyFormBean,@Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {
        
         userFormBean.setUser_seqs(dentalHopitalSession.getUser_seq());
         userService.userMasterInsert(userFormBean);
         
        return null;
        
    }  
    
    
    //회원 정보 수정 
    @RequestMapping(method= RequestMethod.GET,value = "/userMasterUpdate")
    public String userMasterUpdateForm(@Valid UserFormBean userFormBean,@Valid CompanyFormBean companyFormBean,@Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        userFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            userFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            userFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            userFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }            
        
        Hashtable ht_userMasterList = userService.userMasterList(userFormBean);
        List ls_userMasterList = (List) ht_userMasterList.get("ResultSet");
        Tb_User tb_User = (Tb_User) ls_userMasterList.get(0);
        
        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        commonCodeSearchFormBean.setMain_code("USER_CATE");
        Tb_Code_Main userCateList = commonService.codeSearch(commonCodeSearchFormBean);        
        
        StringBuilder sb = new StringBuilder();
        sb.append("{rows : []}");

        model.addAttribute("userMasterList",sb.toString());
        model.addAttribute("tb_User",tb_User);
        model.addAttribute("userCateList",userCateList);
        
        return null;
        
    }       

    //회원수정
    @RequestMapping(method= RequestMethod.POST,value = "/userMasterUpdate")
    public @ResponseBody String companyMasterUpdate(@Valid UserFormBean userFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        userFormBean.setUser_seqs(dentalHopitalSession.getUser_seq());
        String strreturnval = userService.userMasterUpdate(userFormBean);
        logger.trace("strreturnval : "+ strreturnval);
        return strreturnval;
        
    }  
        
   

}
