/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.user.Dao;

import com.onoffkorea.system.user.Bean.UserFormBean;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("userDAO")
public class UserDAOImpl extends SqlSessionDaoSupport implements UserDAO{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    //코드 정보 관리
    @Override
    public List userMasterList(UserFormBean userFormBean) {
        
        return (List) getSqlSession().selectList("User.userMasterList", userFormBean);
        
    }

    //코드 정보 삭제
    @Override
    public void userMasterDelete(UserFormBean userFormBean) {

        getSqlSession().update("User.userMasterDelete", userFormBean);
        
    }
    

    //코드 정보 등록
    @Override
    public void userMasterInsert(UserFormBean userFormBean) {

        getSqlSession().insert("User.userMasterInsert", userFormBean);
        
    }
    
  

    //코드 정보 수정
    @Override
    public Integer userMasterUpdate(UserFormBean userFormBean) {

       return (Integer) getSqlSession().update("User.userMasterUpdate", userFormBean);
        
    }
     //조직코드 중복체크
    @Override
    public Integer usermasterCheck(UserFormBean userFormBean) {
        
        return (Integer) getSqlSession().selectOne("User.usermasterCheck", userFormBean);
        
    }

    @Override
    public Integer userMasterListCount(UserFormBean userFormBean) {
        return (Integer) getSqlSession().selectOne("User.userMasterListCount", userFormBean);
    }
   
    
}
