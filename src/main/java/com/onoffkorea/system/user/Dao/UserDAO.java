/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.user.Dao;

import com.onoffkorea.system.user.Bean.UserFormBean;

/**
 *
 * @author Administrator
 */
public interface UserDAO {

    public Object userMasterList(UserFormBean userFormBean);
    
   
    public void userMasterDelete(UserFormBean userFormBean);
    
    public void userMasterInsert(UserFormBean userFormBean);

    //public void userMasterUpdate(UserFormBean userFormBean);
    public Integer userMasterUpdate(UserFormBean userFormBean);
    
    
    public Integer usermasterCheck(UserFormBean userFormBean);

    public Integer userMasterListCount(UserFormBean userFormBean);
    
    
    
}
