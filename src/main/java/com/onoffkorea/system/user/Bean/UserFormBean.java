/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.user.Bean;
import com.onoffkorea.system.common.util.CommonSortableListPagingForm;
/**
 *
 * @author Administrator
 */
public class UserFormBean extends CommonSortableListPagingForm {
    
     //사용자조회
    private String user_seq   ;
    private String user_id   ;
    private String user_pwd   ;
    private String user_nm   ;
    private String tel_no_1   ;
    private String tel_no_2   ;
    private String user_memo   ;
    private String ins_dt   ;
    private String mod_dt   ;
    private String ins_user   ;
    private String mod_user   ;
    private String user_email   ;
    private String comp_seq   ;
    private String comp_nm   ;
    private String org_seq   ;
    private String org_nm   ;
    private String auth_seq   ;
    private String auth_nm   ;   
    private String rnum   ;  
    private String select_nm   ;  
    private String auth_name   ;  
    private String result_colum;
    private String use_flag;
    private String curlogin_dt;
    private String user_pwd2   ;
    private String user_seq_cnt ;
    private String user_seqs   ;
    private String onffmerch_no  ;
    private String onfftid       ;
    private String user_cate     ;
    private String agent_seq     ;  
     
    private String merch_nm;
    private String onfftid_nm;
    private String agent_nm ;

    public String getMerch_nm() {
        return merch_nm;
    }

    public void setMerch_nm(String merch_nm) {
        this.merch_nm = merch_nm;
    }

    public String getOnfftid_nm() {
        return onfftid_nm;
    }

    public void setOnfftid_nm(String onfftid_nm) {
        this.onfftid_nm = onfftid_nm;
    }

    public String getAgent_nm() {
        return agent_nm;
    }

    public void setAgent_nm(String agent_nm) {
        this.agent_nm = agent_nm;
    }

    
    
    
    public String getOnffmerch_no() {
        return onffmerch_no;
    }

    public void setOnffmerch_no(String onffmerch_no) {
        this.onffmerch_no = onffmerch_no;
    }

    public String getOnfftid() {
        return onfftid;
    }

    public void setOnfftid(String onfftid) {
        this.onfftid = onfftid;
    }

    public String getUser_cate() {
        return user_cate;
    }

    public void setUser_cate(String user_cate) {
        this.user_cate = user_cate;
    }

    public String getAgent_seq() {
        return agent_seq;
    }

    public void setAgent_seq(String agent_seq) {
        this.agent_seq = agent_seq;
    }

    public String getUser_seqs() {
        return user_seqs;
    }

    public void setUser_seqs(String user_seqs) {
        this.user_seqs = user_seqs;
    }

    public String getUser_seq_cnt() {
        return user_seq_cnt;
    }

    public void setUser_seq_cnt(String user_seq_cnt) {
        this.user_seq_cnt = user_seq_cnt;
    }

    public String getUser_pwd2() {
        return user_pwd2;
    }

    public void setUser_pwd2(String user_pwd2) {
        this.user_pwd2 = user_pwd2;
    }

    public String getUse_flag() {
        return use_flag;
    }

    public void setUse_flag(String use_flag) {
        this.use_flag = use_flag;
    }

    public String getCurlogin_dt() {
        return curlogin_dt;
    }

    public void setCurlogin_dt(String curlogin_dt) {
        this.curlogin_dt = curlogin_dt;
    }
    
    
    public String getResult_colum() {
        return result_colum;
    }

    public void setResult_colum(String result_colum) {
        this.result_colum = result_colum;
    }
    
    
    
    public String getAuth_name() {
        return auth_name;
    }

    public void setAuth_name(String auth_name) {
        this.auth_name = auth_name;
    }

    public String getSelect_nm() {
        return select_nm;
    }

    public void setSelect_nm(String select_nm) {
        this.select_nm = select_nm;
    }

    public String getRnum() {
        return rnum;
    }

    public void setRnum(String rnum) {
        this.rnum = rnum;
    }

    public String getUser_seq() {
        return user_seq;
    }

    public void setUser_seq(String user_seq) {
        this.user_seq = user_seq;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_pwd() {
        return user_pwd;
    }

    public void setUser_pwd(String user_pwd) {
        this.user_pwd = user_pwd;
    }

    public String getUser_nm() {
        return user_nm;
    }

    public void setUser_nm(String user_nm) {
        this.user_nm = user_nm;
    }

    public String getTel_no_1() {
        return tel_no_1;
    }

    public void setTel_no_1(String tel_no_1) {
        this.tel_no_1 = tel_no_1;
    }

    public String getTel_no_2() {
        return tel_no_2;
    }

    public void setTel_no_2(String tel_no_2) {
        this.tel_no_2 = tel_no_2;
    }

    public String getUser_memo() {
        return user_memo;
    }

    public void setUser_memo(String user_memo) {
        this.user_memo = user_memo;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getComp_seq() {
        return comp_seq;
    }

    public void setComp_seq(String comp_seq) {
        this.comp_seq = comp_seq;
    }

    public String getComp_nm() {
        return comp_nm;
    }

    public void setComp_nm(String comp_nm) {
        this.comp_nm = comp_nm;
    }

    public String getOrg_seq() {
        return org_seq;
    }

    public void setOrg_seq(String org_seq) {
        this.org_seq = org_seq;
    }

    public String getOrg_nm() {
        return org_nm;
    }

    public void setOrg_nm(String org_nm) {
        this.org_nm = org_nm;
    }

    public String getAuth_seq() {
        return auth_seq;
    }

    public void setAuth_seq(String auth_seq) {
        this.auth_seq = auth_seq;
    }

    public String getAuth_nm() {
        return auth_nm;
    }

    public void setAuth_nm(String auth_nm) {
        this.auth_nm = auth_nm;
    }
 
              
    
}
