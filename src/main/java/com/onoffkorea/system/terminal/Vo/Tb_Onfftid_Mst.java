/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.terminal.Vo;

/**
 *
 * @author MoonbongChoi
 */
public class Tb_Onfftid_Mst {
        private String rnum;
        private String pay_chn_cate;
        private String pay_chn_cate_nm;
        private String onfftid			;
        private String onfftid_nm		;
        private String onffmerch_no;
        private String merch_nm;
        private String svc_stat			;
        private String svc_stat_nm	;
        private String pay_mtd_seq;
        private String pay_mtd_nm;
        private String memo			;
        private String del_flag			;
        private String ins_dt			;
        private String mod_dt			;
        private String ins_user			;
        private String mod_user		;        
        private String cert_type;
        private String cert_type_nm;
        private String cncl_auth;
        private String cncl_auth_nm;

    public String getCncl_auth_nm() {
        return cncl_auth_nm;
    }

    public void setCncl_auth_nm(String cncl_auth_nm) {
        this.cncl_auth_nm = cncl_auth_nm;
    }

    public String getCncl_auth() {
        return cncl_auth;
    }

    public void setCncl_auth(String cncl_auth) {
        this.cncl_auth = cncl_auth;
    }
    
    public String getCert_type_nm() {
        return cert_type_nm;
    }

    public void setCert_type_nm(String cert_type_nm) {
        this.cert_type_nm = cert_type_nm;
    }

    public String getCert_type() {
        return cert_type;
    }

    public void setCert_type(String cert_type) {
        this.cert_type = cert_type;
    }
        
    public String getRnum() {
        return rnum;
    }

    public void setRnum(String rnum) {
        this.rnum = rnum;
    }

    public String getPay_chn_cate() {
        return pay_chn_cate;
    }

    public void setPay_chn_cate(String pay_chn_cate) {
        this.pay_chn_cate = pay_chn_cate;
    }

    public String getPay_chn_cate_nm() {
        return pay_chn_cate_nm;
    }

    public void setPay_chn_cate_nm(String pay_chn_cate_nm) {
        this.pay_chn_cate_nm = pay_chn_cate_nm;
    }

    public String getOnfftid() {
        return onfftid;
    }

    public void setOnfftid(String onfftid) {
        this.onfftid = onfftid;
    }

    public String getOnfftid_nm() {
        return onfftid_nm;
    }

    public void setOnfftid_nm(String onfftid_nm) {
        this.onfftid_nm = onfftid_nm;
    }

    public String getOnffmerch_no() {
        return onffmerch_no;
    }

    public void setOnffmerch_no(String onffmerch_no) {
        this.onffmerch_no = onffmerch_no;
    }

    public String getMerch_nm() {
        return merch_nm;
    }

    public void setMerch_nm(String merch_nm) {
        this.merch_nm = merch_nm;
    }

    public String getSvc_stat() {
        return svc_stat;
    }

    public void setSvc_stat(String svc_stat) {
        this.svc_stat = svc_stat;
    }

    public String getSvc_stat_nm() {
        return svc_stat_nm;
    }

    public void setSvc_stat_nm(String svc_stat_nm) {
        this.svc_stat_nm = svc_stat_nm;
    }

    public String getPay_mtd_seq() {
        return pay_mtd_seq;
    }

    public void setPay_mtd_seq(String pay_mtd_seq) {
        this.pay_mtd_seq = pay_mtd_seq;
    }

    public String getPay_mtd_nm() {
        return pay_mtd_nm;
    }

    public void setPay_mtd_nm(String pay_mtd_nm) {
        this.pay_mtd_nm = pay_mtd_nm;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getDel_flag() {
        return del_flag;
    }

    public void setDel_flag(String del_flag) {
        this.del_flag = del_flag;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }
    
}
