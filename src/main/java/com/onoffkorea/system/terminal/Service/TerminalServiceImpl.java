/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.terminal.Service;

import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.terminal.Bean.TerminalFormBean;
import com.onoffkorea.system.terminal.Bean.TmlCmsFormBean;
import com.onoffkorea.system.terminal.Dao.TerminalDAO;
import com.onoffkorea.system.terminal.Vo.Tb_Onfftid_Commission;
import com.onoffkorea.system.terminal.Vo.Tb_Onfftid_Mst;
import java.util.Hashtable;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service("terminalService")
public class TerminalServiceImpl implements TerminalService
{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private TerminalDAO terminalDAO;
    
    @Override
    public Hashtable terminalMasterInfo(TerminalFormBean terminalFormBean) throws Exception {
        
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
        
        List ls_companyMasterList = this.terminalDAO.terminalMasterList(terminalFormBean);
                
        ht.put("ResultSet", ls_companyMasterList);
        
        return ht;
    }    
    
    
    @Override
    public Hashtable terminalMasterList(TerminalFormBean terminalFormBean) throws Exception {
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
       
        String total_count = this.terminalDAO.terminalRecordCount(terminalFormBean);
       
        List ls_terminalMasterList = this.terminalDAO.terminalMasterList(terminalFormBean);
        
        
        sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(terminalFormBean.getStart_no())-1)+"\",\"rows\" : [");
        
        for (int i = 0; i < ls_terminalMasterList.size(); i++) {
                Tb_Onfftid_Mst tb_Terminal = (Tb_Onfftid_Mst) ls_terminalMasterList.get(i);
                
                //no, 결제채널, 결제TID명,결제TID,가맹점명, 가맹점번호, 결제상품명 ,상태
                sb.append("{\"id\":" + i);
                sb.append(",\"userdata\":{");
                sb.append("\"onfftid\":\""+tb_Terminal.getOnfftid()+"\"");
                sb.append("}");                
                sb.append(",\"data\":[");
                sb.append(" \"" + tb_Terminal.getRnum() + "\"");
                sb.append(",\"" + Util.nullToString(tb_Terminal.getPay_chn_cate_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Terminal.getOnfftid_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Terminal.getOnfftid()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Terminal.getMerch_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Terminal.getOnffmerch_no()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Terminal.getPay_chn_cate_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Terminal.getSvc_stat_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Terminal.getCert_type_nm()) + "\"");

                if (i == (ls_terminalMasterList.size() - 1)) {
                        sb.append("]}");
                } else {
                        sb.append("]},");
                }
        }

        sb.append("]}");    
        
        ht.put("ResultSet", sb.toString());
        
        return ht;
    }

    @Override
    @Transactional
    public String terminalMasterInsert(TerminalFormBean terminalFormBean) throws Exception {
        Integer result = null;
        
        String strOnfftid = this.terminalDAO.GetterminalMasterKey();
        
        terminalFormBean.setOnfftid(strOnfftid);

        //메인정보 입력
        result = this.terminalDAO.terminalMasterInsert(terminalFormBean);
        
        String strterminalDtlInfo = terminalFormBean.getCommissioninfo();
        
        logger.trace("strterminalDtlInfo :" + strterminalDtlInfo);
        
        if(strterminalDtlInfo != null && !strterminalDtlInfo.equals(""))
        {
            String[] rows = strterminalDtlInfo.split("]]]");
            
            for(int x=0 ; x < rows.length ; x++)
            {
                String[] cols = rows[x].split("```");
                

                TmlCmsFormBean insdtlobj = new TmlCmsFormBean();
                
                insdtlobj.setOnfftid(strOnfftid);
                insdtlobj.setCommission(cols[0]);
                insdtlobj.setStart_dt(cols[1]);
                insdtlobj.setEnd_dt(cols[2]);
                insdtlobj.setMemo(cols[3]);
                insdtlobj.setIns_user(terminalFormBean.getIns_user());
                insdtlobj.setMod_user(terminalFormBean.getIns_user());
                
                this.terminalDAO.terminalDtlInsert(insdtlobj);
            }
        }
        
        return result.toString();
    }
    
    @Override
    @Transactional
    public String terminalMasterUpdate(TerminalFormBean terminalFormBean) throws Exception {
        Integer result = null;    
        result = this.terminalDAO.terminalMasterUpdate(terminalFormBean);
        return result.toString();
    }
    
    @Override
    @Transactional
    public String terminalMasterDelete(TerminalFormBean terminalFormBean) throws Exception {
        Integer result = null;    
        result = this.terminalDAO.terminalMasterDelete(terminalFormBean);
        return result.toString();
    }        

    @Override
    public Hashtable terminalDtlList(TmlCmsFormBean tmlCmsFormBean) throws Exception {
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
       
        List ls_terminalDtlMasterList = this.terminalDAO.terminalDtlList(tmlCmsFormBean);
        
        sb.append("{\"rows\" : [");
        
        for (int i = 0; i < ls_terminalDtlMasterList.size(); i++) {
                Tb_Onfftid_Commission tb_TerminalDtl = (Tb_Onfftid_Commission) ls_terminalDtlMasterList.get(i);
                ////담보종류코드,담보종류,담보액,적용시작일,적용종료일,MEMO
                sb.append("{\"id\":" + tb_TerminalDtl.getOnfftid_cms_seq());
                sb.append(",\"data\":[");
                //sb.append(" \"" + tb_TerminalDtl.getRnum() + "\"");
                //sb.append("\"" + "" + "\"");         
                sb.append("\"" + Util.nullToString(tb_TerminalDtl.getCommission()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_TerminalDtl.getStart_dt()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_TerminalDtl.getEnd_dt()) + "\"");
                //sb.append(",\"" + Util.nullToString(tb_TerminalDtl.getMemo()) + "\"");
                
                String strTpContents = Util.nullToString(tb_TerminalDtl.getMemo());
                strTpContents =  strTpContents.replaceAll("\\n", "<br>");
                strTpContents =  strTpContents.replaceAll("\\r", "");
                sb.append(",\"" + strTpContents + "\"");
                
                if (i == (ls_terminalDtlMasterList.size() - 1)) {
                        sb.append("]}");
                } else {
                        sb.append("]},");
                }
        }

        sb.append("]}");    
        
        ht.put("ResultSet", sb.toString());
        
        return ht;
    }

    @Override
    @Transactional
    public String terminalDtlInsert(TmlCmsFormBean tmlCmsFormBean) throws Exception {
        //메인정보 입력
        Integer result = null;
        
        result = this.terminalDAO.terminalDtlInsert(tmlCmsFormBean);
        
        return result.toString();
    }

    @Override
    @Transactional
    public String terminalDtlUpdate(TmlCmsFormBean tmlCmsFormBean) throws Exception {
        Integer result = null;
        result = this.terminalDAO.terminalDtlUpdate(tmlCmsFormBean);
        return result.toString();
    }

    @Override
    @Transactional
    public String terminalDtlDelete(TmlCmsFormBean tmlCmsFormBean) throws Exception {
        Integer result = null;
        result = this.terminalDAO.terminalDtlDelete(tmlCmsFormBean);
        return result.toString();
    }

}
