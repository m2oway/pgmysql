/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.terminal.Service;

import com.onoffkorea.system.company.Bean.CompanyFormBean;
import com.onoffkorea.system.terminal.Bean.TerminalFormBean;
import com.onoffkorea.system.terminal.Bean.TmlCmsFormBean;
import java.util.Hashtable;

/**
 *
 * @author Administrator
 */
public interface TerminalService {
    
    public Hashtable terminalMasterList(TerminalFormBean terminalFormBean) throws Exception;

    public Hashtable terminalMasterInfo(TerminalFormBean terminalFormBean) throws Exception;
    
    public String terminalMasterInsert(TerminalFormBean terminalFormBean) throws Exception;
    
    public String terminalMasterUpdate(TerminalFormBean terminalFormBean) throws Exception;
    
    public String terminalMasterDelete(TerminalFormBean terminalFormBean) throws Exception;

    public Hashtable terminalDtlList(TmlCmsFormBean tmlCmsFormBean) throws Exception;
    
    public String terminalDtlInsert(TmlCmsFormBean tmlCmsFormBean) throws Exception;
        
    public String terminalDtlUpdate(TmlCmsFormBean tmlCmsFormBean) throws Exception;
    
    public String terminalDtlDelete(TmlCmsFormBean tmlCmsFormBean) throws Exception;
   
}
