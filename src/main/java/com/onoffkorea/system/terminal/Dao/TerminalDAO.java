/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.terminal.Dao;

import com.onoffkorea.system.terminal.Bean.TerminalFormBean;
import com.onoffkorea.system.terminal.Bean.TmlCmsFormBean;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public interface TerminalDAO {
    public String GetterminalMasterKey();
    
    public String terminalRecordCount(TerminalFormBean terminalFormBean);

    public List terminalMasterList(TerminalFormBean terminalFormBean);
    
    public Integer terminalMasterInsert(TerminalFormBean terminalFormBean);

    public Integer terminalMasterDelete(TerminalFormBean terminalFormBean);

    public Integer terminalMasterUpdate(TerminalFormBean terminalFormBean);

    
    public List terminalDtlList(TmlCmsFormBean tmlCmsFormBean);    

    public Integer terminalDtlInsert(TmlCmsFormBean tmlCmsFormBean);

    public Integer terminalDtlDelete(TmlCmsFormBean tmlCmsFormBean);
    
    public Integer terminalDtlUpdate(TmlCmsFormBean tmlCmsFormBean);
  

 
}
