/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.terminal.Dao;

import com.onoffkorea.system.terminal.Bean.TerminalFormBean;
import com.onoffkorea.system.terminal.Bean.TmlCmsFormBean;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("terminalDAO")
public class TerminalDAOImpl extends SqlSessionDaoSupport  implements TerminalDAO {
    
    protected Logger logger = Logger.getLogger(getClass().getName());
    
    @Override
    public String GetterminalMasterKey() {
        return (String) getSqlSession().selectOne("Terminal.GetterminalMasterKey");
    }

    @Override
    public String terminalRecordCount(TerminalFormBean terminalFormBean) {
        return (String) getSqlSession().selectOne("Terminal.terminalRecordCount", terminalFormBean);
    }

    @Override
    public List terminalMasterList(TerminalFormBean terminalFormBean) {
        return (List) getSqlSession().selectList("Terminal.terminalMasterList", terminalFormBean);
    }

   @Override
    public Integer terminalMasterInsert(TerminalFormBean terminalFormBean) {
        return (Integer)getSqlSession().insert("Terminal.terminalMasterInsert", terminalFormBean);     
    }
    
    @Override
    public Integer terminalMasterUpdate(TerminalFormBean terminalFormBean) {
        return (Integer)getSqlSession().update("Terminal.terminalMasterUpdate", terminalFormBean);
    }    

    @Override
    public Integer terminalMasterDelete(TerminalFormBean terminalFormBean) {
        return (Integer)getSqlSession().update("Terminal.terminalMasterDelete", terminalFormBean);
    }    

    
    @Override
    public List terminalDtlList(TmlCmsFormBean tmlCmsFormBean) {
        return (List) getSqlSession().selectList("Terminal.TidCmsList", tmlCmsFormBean);
    }
        
    
    @Override
    public Integer terminalDtlInsert(TmlCmsFormBean tmlCmsFormBean) {
        return (Integer)getSqlSession().insert("Terminal.TidCmsInsert", tmlCmsFormBean);
        
    }

    @Override
    public Integer terminalDtlUpdate(TmlCmsFormBean tmlCmsFormBean) {
        return (Integer)getSqlSession().update("Terminal.TidCmsUpdate", tmlCmsFormBean);
    }
    

    @Override
    public Integer terminalDtlDelete(TmlCmsFormBean tmlCmsFormBean) {
        
           int Resultx  = getSqlSession().update("Terminal.TidCmsDelete", tmlCmsFormBean);

           return Resultx;
    }




}
