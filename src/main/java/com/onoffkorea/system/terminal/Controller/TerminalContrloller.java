/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.terminal.Controller;

import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.common.vo.Tb_Code_Detail;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import com.onoffkorea.system.terminal.Bean.TerminalFormBean;
import com.onoffkorea.system.terminal.Bean.TmlCmsFormBean;
import com.onoffkorea.system.terminal.Service.TerminalService;
import com.onoffkorea.system.terminal.Vo.Tb_Onfftid_Commission;
import com.onoffkorea.system.terminal.Vo.Tb_Onfftid_Mst;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/terminal/**")
public class TerminalContrloller {
    
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private TerminalService terminalService;
    
    @Autowired
    private CommonService commonService;
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }    
    
    
    //정보 조회
    @RequestMapping(method= RequestMethod.GET,value = "/terminalMasterList")
    public String terminalMasterList(@Valid TerminalFormBean terminalFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("SVC_STAT");
        Tb_Code_Main UseFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("UseFlagList",UseFlagList);

        commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
        Tb_Code_Main PayChnCateList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("PayChnCateList",PayChnCateList);
        
        
        commonCodeSearchFormBean.setMain_code("CERT_TYPE");
        Tb_Code_Main CertTypeList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("CertTypeList",CertTypeList);
        
        
        Hashtable ht_terminalMasterList = terminalService.terminalMasterList(terminalFormBean);
        String ls_companyMasterList = (String) ht_terminalMasterList.get("ResultSet");

        model.addAttribute("ResultSet",ls_companyMasterList);
        
        return null;
        
    }
    
    
 //terminal 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/terminalMasterList")
    public @ResponseBody String terminalMasterInfo(@Valid TerminalFormBean terminalFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
        
        Hashtable ht_terminalMasterList = terminalService.terminalMasterList(terminalFormBean);
        String ls_terminalMasterList = (String)ht_terminalMasterList.get("ResultSet");
     
        return ls_terminalMasterList;
       
    }     
    
    //정보 등록
    @RequestMapping(method= RequestMethod.GET,value = "/terminalMasterInsert")
    public String terminalMasterInsertForm(@Valid TerminalFormBean terminalFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("SVC_STAT");
        Tb_Code_Main UseFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("UseFlagList",UseFlagList);

        commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
        Tb_Code_Main PayChnCateList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("PayChnCateList",PayChnCateList);
        
        
        commonCodeSearchFormBean.setMain_code("CERT_TYPE");
        Tb_Code_Main CertTypeList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("CertTypeList",CertTypeList);
        
        
        commonCodeSearchFormBean.setMain_code("CNCL_AUTH");
        Tb_Code_Main Cncl_AuthList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("Cncl_AuthList",Cncl_AuthList);
        
        
        return null;
        
    }   
    
 
    //terminal 정보 등록
    @RequestMapping(method= RequestMethod.POST,value = "/terminalMasterInsert")
    public @ResponseBody String terminalMasterInsert(@Valid TerminalFormBean terminalFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {
        String result = null;
        
        terminalFormBean.setIns_user(dentalHopitalSession.getUser_seq());
        terminalFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        result = terminalService.terminalMasterInsert(terminalFormBean);
        
        return result;
        
    }       

    //terminal 정보 수정 
    @RequestMapping(method= RequestMethod.GET,value = "/terminalMasterUpdate")
    public String terminalMasterUpdateForm(@Valid TerminalFormBean terminalFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

            CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

            commonCodeSearchFormBean.setMain_code("SVC_STAT");
            Tb_Code_Main UseFlagList = commonService.codeSearch(commonCodeSearchFormBean);
            model.addAttribute("UseFlagList",UseFlagList);

            commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
            Tb_Code_Main PayChnCateList = commonService.codeSearch(commonCodeSearchFormBean);
            model.addAttribute("PayChnCateList",PayChnCateList);
            
            
            commonCodeSearchFormBean.setMain_code("CERT_TYPE");
            Tb_Code_Main CertTypeList = commonService.codeSearch(commonCodeSearchFormBean);
            model.addAttribute("CertTypeList",CertTypeList);
            
            commonCodeSearchFormBean.setMain_code("CNCL_AUTH");
            Tb_Code_Main Cncl_AuthList = commonService.codeSearch(commonCodeSearchFormBean);
            model.addAttribute("Cncl_AuthList",Cncl_AuthList);
        
            //가맹점정보
            Hashtable ht_terminalMasterList = terminalService.terminalMasterInfo(terminalFormBean);
            
            List ls_paymtdMasterList = (List) ht_terminalMasterList.get("ResultSet");
            Tb_Onfftid_Mst tb_Terminal = (Tb_Onfftid_Mst) ls_paymtdMasterList.get(0);
        
            model.addAttribute("tb_Terminal",tb_Terminal);
            
            TmlCmsFormBean tmlCmsFormBean = new TmlCmsFormBean();
            
            tmlCmsFormBean.setOnfftid(terminalFormBean.getOnfftid());
        
            Hashtable ht_terminalDtlList = terminalService.terminalDtlList(tmlCmsFormBean);
            String ls_terminalDtlList = (String)ht_terminalDtlList.get("ResultSet");
            
            model.addAttribute("terminalDtlList",ls_terminalDtlList);
            
        return null;
        
    }       
    
    //terminal 정보 수정 
    @RequestMapping(method= RequestMethod.POST,value = "/terminalMasterUpdate")
    public @ResponseBody String terminalMasterUpdate(@Valid TerminalFormBean terminalFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        terminalFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        String result = terminalService.terminalMasterUpdate(terminalFormBean);
        
        return result;
        
    }            
        

    
    //terminal 정보 수정 
    @RequestMapping(method= RequestMethod.POST,value = "/terminalMasterDelete")
    public @ResponseBody String terminalMasterDelete(@Valid TerminalFormBean terminalFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        terminalFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        String result = terminalService.terminalMasterDelete(terminalFormBean);
        
        return result;
        
    }          
    
    
    //terminaldtl 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/terminalDtlList")
    public @ResponseBody String terminalDtlInfo(@Valid TmlCmsFormBean tmlCmsFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
        
        Hashtable ht_terminalMasterList = terminalService.terminalDtlList(tmlCmsFormBean);
        String ls_terminalMasterList = (String)ht_terminalMasterList.get("ResultSet");
     
        return ls_terminalMasterList;
    }    
    


    //terminaldtl 정보 등록
    @RequestMapping(method= RequestMethod.POST,value = "/terminalDtlInsert")
    public @ResponseBody String terminalDtlInsert(@Valid TmlCmsFormBean tmlCmsFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        tmlCmsFormBean.setIns_user(dentalHopitalSession.getUser_seq());
        tmlCmsFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        String result = terminalService.terminalDtlInsert(tmlCmsFormBean);
        
        return result;
        
    }       
    
    
    //terminaldtl 정보 수정 
    @RequestMapping(method= RequestMethod.POST,value = "/terminalDtlUpdate")
    public @ResponseBody String terminalDtlUpdate(@Valid TmlCmsFormBean tmlCmsFormBean, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        tmlCmsFormBean.setIns_user(dentalHopitalSession.getUser_seq());
        tmlCmsFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        String result = terminalService.terminalDtlUpdate(tmlCmsFormBean);
        
        return result;
    }        
    
    
    //terminaldtl 정보 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/terminalDtlDelete")
    //public @ResponseBody String terminalDtlDelete(@Valid TerminalDetailFormBean terminalDtlFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
    public @ResponseBody String terminalDtlDelete(@Valid TmlCmsFormBean tmlCmsFormBean, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {                

        tmlCmsFormBean.setIns_user(dentalHopitalSession.getUser_seq());
        tmlCmsFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        String result = terminalService.terminalDtlDelete(tmlCmsFormBean);
        
        return result;
        
    }
   
    
}
