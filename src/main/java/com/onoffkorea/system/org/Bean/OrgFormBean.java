/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.org.Bean;

/**
 *
 * @author Administrator
 */
public class OrgFormBean {
    
    private String org_seq   ;
    private String comp_seq  ;
    private String org_nm    ;
    private String parent_seq;
    private String org_level ;
    private String use_flag  ;
    private String ins_dt    ;
    private String mod_dt    ;
    private String ins_user  ;
    private String mod_user  ;
    private String tel_no_1  ;
    private String manager_nm;    
    private String parent_org_seq;
    private String memo;
    private String user_seq;
    private String v_error;

    public String getV_error() {
        return v_error;
    }

    public void setV_error(String v_error) {
        this.v_error = v_error;
    }

    public String getUser_seq() {
        return user_seq;
    }

    public void setUser_seq(String user_seq) {
        this.user_seq = user_seq;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }
    
    public String getParent_org_seq() {
        return parent_org_seq;
    }

    public void setParent_org_seq(String parent_org_seq) {
        this.parent_org_seq = parent_org_seq;
    }

    public String getOrg_seq() {
        return org_seq;
    }

    public void setOrg_seq(String org_seq) {
        this.org_seq = org_seq;
    }

    public String getComp_seq() {
        return comp_seq;
    }

    public void setComp_seq(String comp_seq) {
        this.comp_seq = comp_seq;
    }

    public String getOrg_nm() {
        return org_nm;
    }

    public void setOrg_nm(String org_nm) {
        this.org_nm = org_nm;
    }

    public String getParent_seq() {
        return parent_seq;
    }

    public void setParent_seq(String parent_seq) {
        this.parent_seq = parent_seq;
    }

    public String getOrg_level() {
        return org_level;
    }

    public void setOrg_level(String org_level) {
        this.org_level = org_level;
    }

    public String getUse_flag() {
        return use_flag;
    }

    public void setUse_flag(String use_flag) {
        this.use_flag = use_flag;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    public String getTel_no_1() {
        return tel_no_1;
    }

    public void setTel_no_1(String tel_no_1) {
        this.tel_no_1 = tel_no_1;
    }

    public String getManager_nm() {
        return manager_nm;
    }

    public void setManager_nm(String manager_nm) {
        this.manager_nm = manager_nm;
    }

}
