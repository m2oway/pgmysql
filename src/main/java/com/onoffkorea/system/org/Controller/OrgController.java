/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.org.Controller;

import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.company.Bean.CompanyFormBean;
import com.onoffkorea.system.company.Service.CompanyService;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import com.onoffkorea.system.org.Bean.OrgFormBean;
import com.onoffkorea.system.org.Service.OrgService;
import com.onoffkorea.system.org.Vo.Tb_Org;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/org/**")
public class OrgController {
    
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private OrgService orgService;
    
    @Autowired
    private CompanyService companyService;
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }
    
    //조직 정보 조회
    @RequestMapping(method= RequestMethod.GET,value = "/orgMasterList")
    public String orgMasterFormList(@Valid OrgFormBean orgFormBean, @Valid CompanyFormBean companyFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        //사업체 정보 조회
        Hashtable ht_companyMasterList = companyService.companyMasterList(companyFormBean);
        List ls_companyMasterList = (List) ht_companyMasterList.get("ResultSet");
        
        model.addAttribute("ls_companyMasterList",ls_companyMasterList);
        
        return null;
        
    }    
    
    //최상위 조직 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/orgMasterList")
    public @ResponseBody Hashtable orgMasterList(@Valid OrgFormBean orgFormBean, @Valid CompanyFormBean companyFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht = new Hashtable();
        
        List<Tb_Org> ls_orgMasterList = orgService.orgMasterList(orgFormBean);
        
        ht.put("ls_orgMasterList", ls_orgMasterList);
        
        return ht;
        
    }        
    
    //하위조직 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/orgMasterDetailList")
    public @ResponseBody Hashtable orgMasterDetailList(@Valid OrgFormBean orgFormBean, @Valid CompanyFormBean companyFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht = new Hashtable();
        
        List<Tb_Org> ls_orgMasterDetailList = orgService.orgMasterDetailList(orgFormBean);
        
        Tb_Org orgMasterDetail = orgService.orgMasterDetail(orgFormBean);
        
        ht.put("ls_orgMasterDetailList", ls_orgMasterDetailList);
        ht.put("orgMasterDetail", orgMasterDetail);
        
        return ht;
        
    }            
    
    //조직 등록 폼
    @RequestMapping(method= RequestMethod.GET,value = "/orgMasterInsert")
    public String orgMasterInsertForm(@Valid OrgFormBean orgFormBean, @Valid CompanyFormBean companyFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        
        List<Tb_Org> ls_orgMasterParentList = orgService.orgMasterParentList(orgFormBean);

        model.addAttribute("ls_orgMasterParentList",ls_orgMasterParentList);
        model.addAttribute("comp_seq", orgFormBean.getComp_seq());
        model.addAttribute("org_level", orgFormBean.getOrg_level());
        
        return null;
        
    }        
    
    //조직 정보 등록
    @RequestMapping(method= RequestMethod.POST,value = "/orgMasterInsert")
    public @ResponseBody String orgMasterInsert(@Valid OrgFormBean orgFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        orgFormBean.setUser_seq(dentalHopitalSession.getUser_seq());
        String result = orgService.orgMasterInsert(orgFormBean);
        
        return result;
        
    }          
    
    //조직 정보 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/orgMasterDelete")
    public @ResponseBody String orgMasterDelete(@Valid OrgFormBean orgFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        orgFormBean.setUser_seq(dentalHopitalSession.getUser_seq());
        orgService.orgMasterDelete(orgFormBean);
        
        return null;
        
    }           
    
    //조직 수정 폼
    @RequestMapping(method= RequestMethod.GET,value = "/orgMasterUpdate")
    public String orgMasterUpdateForm(@Valid OrgFormBean orgFormBean, @Valid CompanyFormBean companyFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        
        List<Tb_Org> ls_orgMasterParentList = orgService.orgMasterUpdateParentList(orgFormBean);
        Tb_Org orgMasterDetail = orgService.orgMasterDetail(orgFormBean);
        
        model.addAttribute("orgMasterDetail", orgMasterDetail);
        model.addAttribute("ls_orgMasterParentList",ls_orgMasterParentList);
        
        return null;
        
    }        
    
    //조직 정보 수정
    @RequestMapping(method= RequestMethod.POST,value = "/orgMasterUpdate")
    public @ResponseBody String orgMasterUpdate(@Valid OrgFormBean orgFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        orgFormBean.setUser_seq(dentalHopitalSession.getUser_seq());
        String rs = orgService.orgMasterUpdate(orgFormBean);
        
        return rs;
        
    }            
    
}
