/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.org.Dao;

import com.onoffkorea.system.org.Bean.OrgFormBean;
import com.onoffkorea.system.org.Vo.Tb_Org;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("orgDAO")
public class OrgDAOImpl extends SqlSessionDaoSupport implements OrgDAO{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    //최상위 조직 정보 조회
    @Override
    public List<Tb_Org> orgMasterList(OrgFormBean orgFormBean) {
        
        return (List)getSqlSession().selectList("Org.orgMasterList", orgFormBean);
        
    }

    //하위 조직 정보 조회
    @Override
    public List<Tb_Org> orgMasterDetailList(OrgFormBean orgFormBean) {
        
        return (List)getSqlSession().selectList("Org.orgMasterDetailList", orgFormBean);
        
    }

    //선택한 조직 정보 조회
    @Override
    public Tb_Org orgMasterDetail(OrgFormBean orgFormBean) {
        
        return (Tb_Org) getSqlSession().selectOne("Org.orgMasterDetail", orgFormBean);
        
    }

    //상위 조직 조회
    @Override
    public List<Tb_Org> orgMasterParentList(OrgFormBean orgFormBean) {
        
        return (List)getSqlSession().selectList("Org.orgMasterParentList", orgFormBean);
        
    }

    //조직 등록
    @Override
    public void orgMasterInsert(OrgFormBean orgFormBean) {

        getSqlSession().insert("Org.orgMasterInsert", orgFormBean);
        
    }

    //조직 삭제
    @Override
    public void orgMasterDelete(OrgFormBean orgFormBean) {

        getSqlSession().update("Org.orgMasterDelete", orgFormBean);
        
    }

    //수정화면 상위조직 조회
    @Override
    public List<Tb_Org> orgMasterUpdateParentList(OrgFormBean orgFormBean) {
        
        return (List)getSqlSession().selectList("Org.orgMasterUpdateParentList", orgFormBean);
        
    }

    //조직 수정
    @Override
    public String orgMasterUpdate(OrgFormBean orgFormBean) {

        getSqlSession().selectOne("Org.orgMasterUpdate", orgFormBean);
        return orgFormBean.getV_error();
        
    }

    //조직코드 중복체크
    @Override
    public Integer orgmasterCheck(OrgFormBean orgFormBean) {
        
        return (Integer) getSqlSession().selectOne("Org.orgmasterCheck", orgFormBean);
        
    }

}
