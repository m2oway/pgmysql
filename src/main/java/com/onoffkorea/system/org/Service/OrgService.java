/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.org.Service;

import com.onoffkorea.system.org.Bean.OrgFormBean;
import com.onoffkorea.system.org.Vo.Tb_Org;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface OrgService {

    public List<Tb_Org> orgMasterList(OrgFormBean orgFormBean);

    public List<Tb_Org> orgMasterDetailList(OrgFormBean orgFormBean);

    public Tb_Org orgMasterDetail(OrgFormBean orgFormBean);

    public List<Tb_Org> orgMasterParentList(OrgFormBean orgFormBean);

    public String orgMasterInsert(OrgFormBean orgFormBean);

    public void orgMasterDelete(OrgFormBean orgFormBean);

    public List<Tb_Org> orgMasterUpdateParentList(OrgFormBean orgFormBean);

    public String orgMasterUpdate(OrgFormBean orgFormBean);
    
}
