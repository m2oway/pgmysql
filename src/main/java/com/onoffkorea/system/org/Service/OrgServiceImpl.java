/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.org.Service;

import com.onoffkorea.system.org.Bean.OrgFormBean;
import com.onoffkorea.system.org.Dao.OrgDAO;
import com.onoffkorea.system.org.Vo.Tb_Org;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service("orgService")
public class OrgServiceImpl implements OrgService{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private OrgDAO orgDAO;

    //조직 정보 조회
    @Override
    public List<Tb_Org> orgMasterList(OrgFormBean orgFormBean) {
        
        List<Tb_Org> ls = new ArrayList<Tb_Org>();
        
        try {
                ls = this.orgDAO.orgMasterList(orgFormBean);
        } catch (Exception e) {
                e.printStackTrace();
        }
        
        return ls;
        
    }

    //하위조직 정보 조회
    @Override
    public List<Tb_Org> orgMasterDetailList(OrgFormBean orgFormBean) {
        
        List<Tb_Org> ls = new ArrayList<Tb_Org>();
        
        try {
                ls = this.orgDAO.orgMasterDetailList(orgFormBean);
        } catch (Exception e) {
                e.printStackTrace();
        }
        
        return ls;
        
    }

    //선택한 조직 정보 조회
    @Override
    public Tb_Org orgMasterDetail(OrgFormBean orgFormBean) {
        
        Tb_Org tb_org = new Tb_Org();
        
        tb_org = this.orgDAO.orgMasterDetail(orgFormBean);
        
        return tb_org;
        
    }

    //상위조직 조회
    @Override
    public List<Tb_Org> orgMasterParentList(OrgFormBean orgFormBean) {
        
        List<Tb_Org> ls = new ArrayList<Tb_Org>();
        
        try {
                ls = this.orgDAO.orgMasterParentList(orgFormBean);
        } catch (Exception e) {
                e.printStackTrace();
        }
        
        return ls;
        
    }

    //조직 정보 등록
    @Override
    @Transactional
    public String orgMasterInsert(OrgFormBean orgFormBean) {
        
        //조직코드 중복체크
        Integer org_seq_cnt = orgDAO.orgmasterCheck(orgFormBean);
        
        if(org_seq_cnt == 0){
            //조직 등록
            orgDAO.orgMasterInsert(orgFormBean);        
        }

        return org_seq_cnt.toString(); 
        
    }

    //조직 삭제
    @Override
    @Transactional
    public void orgMasterDelete(OrgFormBean orgFormBean) {
        
        orgDAO.orgMasterDelete(orgFormBean);
    
    }

    //수정화면 상위조직 조회
    @Override
    public List<Tb_Org> orgMasterUpdateParentList(OrgFormBean orgFormBean) {

        List<Tb_Org> ls = new ArrayList<Tb_Org>();
        
        try {
                ls = this.orgDAO.orgMasterUpdateParentList(orgFormBean);
        } catch (Exception e) {
                e.printStackTrace();
        }
        
        return ls;        
        
    }

    //조직 수정
    @Override
    @Transactional
    public String orgMasterUpdate(OrgFormBean orgFormBean) {
        
        return orgDAO.orgMasterUpdate(orgFormBean);
        
    }
    
}
