/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.taxamt.Service;

import com.onoffkorea.system.taxamt.Bean.TaxamtFormBean;
import java.util.Hashtable;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public interface TaxamtService { 

    public void taxamtMasterListExcel(TaxamtFormBean taxamtFormBean, String excelDownloadFilename, HttpServletResponse response);

    public Hashtable merchTaxamtMasterListPopup(TaxamtFormBean taxamtFormBean);

    public void merchTaxamtMasterListPopupExcel(TaxamtFormBean taxamtFormBean, String excelDownloadFilename, HttpServletResponse response);
    
    public void merchTaxamtCashbillMasterListPopupExcel(TaxamtFormBean taxamtFormBean, String excelDownloadFilename, HttpServletResponse response);
    
}
