/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.taxamt.Service;

import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.taxamt.Bean.TaxamtFormBean;
import com.onoffkorea.system.taxamt.Dao.TaxamtDAO;
import com.onoffkorea.system.trans.Vo.Tb_Tran_Cardpg;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 */
@Service("taxamtService")
public class TaxamtServiceImpl implements TaxamtService{
 
    protected Logger logger = Logger.getLogger(getClass().getName());
    
    @Autowired
    private CommonService commonService;        

    @Autowired
    private TaxamtDAO taxamtDAO;       

    //세금계산서 엑셀다운로드
    @Override
    public void taxamtMasterListExcel(TaxamtFormBean taxamtFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try { 
             
                List ls_taxamtMasterListExcel = taxamtDAO.taxamtMasterListExcel(taxamtFormBean);
                
                StringBuffer taxamtMasterListExcel = Util.makeData(ls_taxamtMasterListExcel);
        
                Util.exceldownload(taxamtMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    }

    //부가세 조회
    @Override
    public Hashtable merchTaxamtMasterListPopup(TaxamtFormBean taxamtFormBean) {

        Hashtable ht = new Hashtable();

        try {
            
                List<Tb_Tran_Cardpg> ls_merchTaxamtMasterListPopup = taxamtDAO.merchTaxamtMasterListPopup(taxamtFormBean);

                ht.put("ResultSet", ls_merchTaxamtMasterListPopup);
                
                
                List<Tb_Tran_Cardpg> ls_merchTaxamtCashbillMasterListPopup = taxamtDAO.merchTaxamtCashbillMasterListPopup(taxamtFormBean);

                ht.put("ResultSet_Cash", ls_merchTaxamtCashbillMasterListPopup);

        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;                  
        
    }

    //부가세 카드매출 상세내역 엑셀다운로드
    @Override
    public void merchTaxamtMasterListPopupExcel(TaxamtFormBean taxamtFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
             
                List ls_merchTaxamtMasterListPopupExcel = taxamtDAO.merchTaxamtMasterListPopupExcel(taxamtFormBean);
                
                StringBuffer merchTaxamtMasterListPopupExcel = Util.makeData(ls_merchTaxamtMasterListPopupExcel);
        
                Util.exceldownload(merchTaxamtMasterListPopupExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }                
        
    }
    
        //부가세 현금영수증 매출 상세내역 엑셀다운로드
    @Override
    public void merchTaxamtCashbillMasterListPopupExcel(TaxamtFormBean taxamtFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
             
                List ls_merchTaxamtCashbillMasterListPopupExcel = taxamtDAO.merchTaxamtCashbillMasterListPopupExcel(taxamtFormBean);
                
                StringBuffer merchTaxamtCashbillMasterListPopupExcel = Util.makeData(ls_merchTaxamtCashbillMasterListPopupExcel);
        
                Util.exceldownload(merchTaxamtCashbillMasterListPopupExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }                
        
    }
    
}
