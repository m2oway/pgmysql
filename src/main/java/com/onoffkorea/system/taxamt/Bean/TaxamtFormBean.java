/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.taxamt.Bean;

import com.onoffkorea.system.common.util.CommonSortableListPagingForm;

/**
 *
 * @author Administrator
 */
public class TaxamtFormBean extends CommonSortableListPagingForm{
    
    private String app_start_yyyy;
    private String app_start_mm;
    private String app_end_yyyy;
    private String app_end_mm;    
    private String pay_yyyy;
    private String pay_mm;
    
    private String pay_start_yyyy;
    private String pay_start_mm;
    private String pay_end_yyyy;
    private String pay_end_mm;       

    public String getPay_start_yyyy() {
        return pay_start_yyyy;
    }

    public void setPay_start_yyyy(String pay_start_yyyy) {
        this.pay_start_yyyy = pay_start_yyyy;
    }

    public String getPay_start_mm() {
        return pay_start_mm;
    }

    public void setPay_start_mm(String pay_start_mm) {
        this.pay_start_mm = pay_start_mm;
    }

    public String getPay_end_yyyy() {
        return pay_end_yyyy;
    }

    public void setPay_end_yyyy(String pay_end_yyyy) {
        this.pay_end_yyyy = pay_end_yyyy;
    }

    public String getPay_end_mm() {
        return pay_end_mm;
    }

    public void setPay_end_mm(String pay_end_mm) {
        this.pay_end_mm = pay_end_mm;
    }
    
    

    public String getApp_start_yyyy() {
        return app_start_yyyy;
    }

    public void setApp_start_yyyy(String app_start_yyyy) {
        this.app_start_yyyy = app_start_yyyy;
    }

    public String getApp_start_mm() {
        return app_start_mm;
    }

    public void setApp_start_mm(String app_start_mm) {
        this.app_start_mm = app_start_mm;
    }

    public String getApp_end_yyyy() {
        return app_end_yyyy;
    }

    public void setApp_end_yyyy(String app_end_yyyy) {
        this.app_end_yyyy = app_end_yyyy;
    }

    public String getApp_end_mm() {
        return app_end_mm;
    }

    public void setApp_end_mm(String app_end_mm) {
        this.app_end_mm = app_end_mm;
    }

    public String getPay_yyyy() {
        return pay_yyyy;
    }

    public void setPay_yyyy(String pay_yyyy) {
        this.pay_yyyy = pay_yyyy;
    }

    public String getPay_mm() {
        return pay_mm;
    }

    public void setPay_mm(String pay_mm) {
        this.pay_mm = pay_mm;
    }
    
}
