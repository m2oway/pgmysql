/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.taxamt.Dao;

import com.onoffkorea.system.taxamt.Bean.TaxamtFormBean;
import com.onoffkorea.system.trans.Vo.Tb_Tran_Cardpg;
import java.util.List; 

/**
 *
 * @author Administrator
 */
public interface TaxamtDAO {

    public List taxamtMasterListExcel(TaxamtFormBean taxamtFormBean);

    public List<Tb_Tran_Cardpg> merchTaxamtMasterListPopup(TaxamtFormBean taxamtFormBean);

    public List merchTaxamtMasterListPopupExcel(TaxamtFormBean taxamtFormBean);
    
    
    public List<Tb_Tran_Cardpg> merchTaxamtCashbillMasterListPopup(TaxamtFormBean taxamtFormBean);

    public List merchTaxamtCashbillMasterListPopupExcel(TaxamtFormBean taxamtFormBean);
}
