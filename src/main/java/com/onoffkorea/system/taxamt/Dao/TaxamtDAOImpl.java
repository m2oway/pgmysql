/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.taxamt.Dao;

import com.onoffkorea.system.taxamt.Bean.TaxamtFormBean;
import com.onoffkorea.system.trans.Vo.Tb_Tran_Cardpg;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("taxamtDAO")
public class TaxamtDAOImpl extends SqlSessionDaoSupport implements TaxamtDAO{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    //세금계산서 엑셀다운로드 
    @Override
    public List taxamtMasterListExcel(TaxamtFormBean taxamtFormBean) {
        
        return (List)getSqlSession().selectList("Taxamt.taxamtMasterListExcel",taxamtFormBean);
        
    }

    //부가세
    @Override
    public List<Tb_Tran_Cardpg> merchTaxamtMasterListPopup(TaxamtFormBean taxamtFormBean) {
        
        return (List)getSqlSession().selectList("Taxamt.merchTaxamtMasterListPopup",taxamtFormBean);
        
    }

    //부가세 카드매출 상세내역 엑셀다운로드
    @Override
    public List merchTaxamtMasterListPopupExcel(TaxamtFormBean taxamtFormBean) {
        
        return (List)getSqlSession().selectList("Taxamt.merchTaxamtMasterListPopupExcel",taxamtFormBean);
        
    }
    
    //현금영수증 부가세
    @Override
    public List<Tb_Tran_Cardpg> merchTaxamtCashbillMasterListPopup(TaxamtFormBean taxamtFormBean) {
        
        return (List)getSqlSession().selectList("Taxamt.merchTaxamtCashbillMasterListPopup",taxamtFormBean);
        
    }

    //현금영수증 부가세 카드매출 상세내역 엑셀다운로드
    @Override
    public List merchTaxamtCashbillMasterListPopupExcel(TaxamtFormBean taxamtFormBean) {
        
        return (List)getSqlSession().selectList("Taxamt.merchTaxamtCashbillMasterListPopupExcel",taxamtFormBean);
        
    }
}
