/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.taxamt.Controller;

import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import com.onoffkorea.system.taxamt.Bean.TaxamtFormBean;
import com.onoffkorea.system.taxamt.Service.TaxamtService;
import common.web.servlet.BaseSpringMultiActionController;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/taxamt/**")
public class TaxamtController extends BaseSpringMultiActionController{
    
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private CommonService commonService;
    
    @Autowired
    private TaxamtService taxamtService;
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));

    }  

    
    //세금계산서 조회
    @RequestMapping(method= RequestMethod.GET,value = "/taxamtMasterList")
    public String taxamtMasterList(@Valid TaxamtFormBean taxamtFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        return null;
        
    }    
    
    //세금계산서 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/taxamtMasterListExcel")
    public void taxamtMasterListExcel(@Valid TaxamtFormBean taxamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletResponse response
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        String excelDownloadFilename = new String(getMessage("taxamt.taxamtMasterListExcel.fileName"));
        
        taxamtService.taxamtMasterListExcel(taxamtFormBean, excelDownloadFilename, response);        

    }        
    
    
    
        //가맹점 세금계산서 발행
    @RequestMapping(method= RequestMethod.GET,value = "/basic_merchTaxamtMasterList")
    public String basic_merchTaxamtMasterList(@Valid TaxamtFormBean taxamtFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        return null;
        
    }         
    
    
    
    //가맹점 세금계산서 발행
    @RequestMapping(method= RequestMethod.GET,value = "/merchTaxamtMasterList")
    public String merchTaxamtMasterList(@Valid TaxamtFormBean taxamtFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        return null;
        
    }         
    
    
    
    
    //가맹점 세금계산서 발행
    @RequestMapping(method= RequestMethod.GET,value = "/merchTaxamtMasterListPopup")
    public String merchTaxamtMasterListPopup(@Valid TaxamtFormBean taxamtFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        taxamtFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            taxamtFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            taxamtFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            taxamtFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                
        
        Hashtable ht_merchTaxamtMasterListPopup = taxamtService.merchTaxamtMasterListPopup(taxamtFormBean);
        List ls_merchTaxamtMasterListPopup = (List) ht_merchTaxamtMasterListPopup.get("ResultSet");
        
        model.addAttribute("ls_merchTaxamtMasterListPopup",ls_merchTaxamtMasterListPopup);             
        
  
        List ls_merchTaxamtCashbillMasterListPopup = (List) ht_merchTaxamtMasterListPopup.get("ResultSet_Cash");
        
        model.addAttribute("ls_merchTaxamtCashbillMasterListPopup",ls_merchTaxamtCashbillMasterListPopup);                     
        
        return null;

    }           
    
    //부가세 카드매출 상세내역 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/merchTaxamtMasterListPopupExcel")
    public void merchTaxamtMasterListPopupExcel(@Valid TaxamtFormBean taxamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("taxamt.merchTaxamtMasterListPopupExcel.fileName"));
        
        taxamtFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            taxamtFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            taxamtFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            taxamtFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        taxamtService.merchTaxamtMasterListPopupExcel(taxamtFormBean, excelDownloadFilename, response); 
        
    }      
    
    //부가세 현금영수증매출 상세내역 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/merchTaxamtCashbillMasterListPopupExcel")
    public void merchTaxamtCashbillMasterListPopupExcel(@Valid TaxamtFormBean taxamtFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("taxamt.merchCashTaxamtMasterListPopupExcel.fileName"));
        
        taxamtFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            taxamtFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            taxamtFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            taxamtFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        taxamtService.merchTaxamtCashbillMasterListPopupExcel(taxamtFormBean, excelDownloadFilename, response); 
        
    }             
    
}
