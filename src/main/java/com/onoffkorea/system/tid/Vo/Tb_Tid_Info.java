/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.tid.Vo;

/**
 *
 * @author Administrator
 */
public class Tb_Tid_Info {
    private String tid_mtd;			//가맹점구분   
    private String tid_mtd_nm;
    private String terminal_no;		//가맹점 번호  
    private String tid_seq;	//일련번호     
    private String tid_nm;			//MID명        
    private String func_cate;		//기능분류     
    private String func_cate_nm;		//기능분류     
    private String agency_flag;		//자체/대행구분
    private String agency_flag_nm;
    private String installment;		//할부         
    private String point_cate;		//포인트구분   
    private String commision;		//수수료       
    private String commision2;		//첵크수수료   
    private String point_commsion;		//포인트수수료 
    private String memo;			//MEMO         
    private String start_dt;		//적용일       
    private String end_dt;			//적용종료일   
    private String bank_cd;			//은행코드     
    private String bank_cd_nm;
    private String acc_no;			//입금계좌     
    private String use_flag;		//사용여부     
    private String use_flag_nm;		//사용여부     
    private String del_flag;		//삭제여부   
    private String del_flag_nm;
    private String ins_dt;			//입력일       
    private String mod_dt;			//수정일       
    private String ins_user;		//입력자       
    private String mod_user;		//수정자       
    private String rnum;
    
    private String terminal_pwd;

    
    private String appcnclflag;//단말기결제 당일취소여부

    private String appcnclflag_nm;//단말기결제 당일취소여부

    public String getAppcnclflag_nm() {
        return appcnclflag_nm;
    }

    public void setAppcnclflag_nm(String appcnclflag_nm) {
        this.appcnclflag_nm = appcnclflag_nm;
    }
      
    
    
    public String getAppcnclflag() {
        return appcnclflag;
    }

    public void setAppcnclflag(String appcnclflag) {
        this.appcnclflag = appcnclflag;
    }
    
    
    public String getTerminal_pwd() {
        return terminal_pwd;
    }

    public void setTerminal_pwd(String terminal_pwd) {
        this.terminal_pwd = terminal_pwd;
    }
    
    

    public String getBank_cd_nm() {
        return bank_cd_nm;
    }

    public void setBank_cd_nm(String bank_cd_nm) {
        this.bank_cd_nm = bank_cd_nm;
    }

    public String getDel_flag_nm() {
        return del_flag_nm;
    }

    public void setDel_flag_nm(String del_flag_nm) {
        this.del_flag_nm = del_flag_nm;
    }

    public String getFunc_cate_nm() {
        return func_cate_nm;
    }

    public void setFunc_cate_nm(String func_cate_nm) {
        this.func_cate_nm = func_cate_nm;
    }

    public String getTid_mtd_nm() {
        return tid_mtd_nm;
    }

    public void setTid_mtd_nm(String tid_mtd_nm) {
        this.tid_mtd_nm = tid_mtd_nm;
    }

    public String getAgency_flag_nm() {
        return agency_flag_nm;
    }

    public void setAgency_flag_nm(String agency_flag_nm) {
        this.agency_flag_nm = agency_flag_nm;
    }

    public String getUse_flag_nm() {
        return use_flag_nm;
    }

    public void setUse_flag_nm(String use_flag_nm) {
        this.use_flag_nm = use_flag_nm;
    }

    public String getRnum() {
        return rnum;
    }

    public void setRnum(String rnum) {
        this.rnum = rnum;
    }
    
    

    public String getTid_mtd() {
        return tid_mtd;
    }

    public void setTid_mtd(String tid_mtd) {
        this.tid_mtd = tid_mtd;
    }

    public String getTerminal_no() {
        return terminal_no;
    }

    public void setTerminal_no(String terminal_no) {
        this.terminal_no = terminal_no;
    }

    public String getTid_seq() {
        return tid_seq;
    }

    public void setTid_seq(String tid_seq) {
        this.tid_seq = tid_seq;
    }

    public String getTid_nm() {
        return tid_nm;
    }

    public void setTid_nm(String tid_nm) {
        this.tid_nm = tid_nm;
    }

    public String getFunc_cate() {
        return func_cate;
    }

    public void setFunc_cate(String func_cate) {
        this.func_cate = func_cate;
    }

    public String getAgency_flag() {
        return agency_flag;
    }

    public void setAgency_flag(String agency_flag) {
        this.agency_flag = agency_flag;
    }

    public String getInstallment() {
        return installment;
    }

    public void setInstallment(String installment) {
        this.installment = installment;
    }

    public String getPoint_cate() {
        return point_cate;
    }

    public void setPoint_cate(String point_cate) {
        this.point_cate = point_cate;
    }

    public String getCommision() {
        return commision;
    }

    public void setCommision(String commision) {
        this.commision = commision;
    }

    public String getCommision2() {
        return commision2;
    }

    public void setCommision2(String commision2) {
        this.commision2 = commision2;
    }

    public String getPoint_commsion() {
        return point_commsion;
    }

    public void setPoint_commsion(String point_commsion) {
        this.point_commsion = point_commsion;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getStart_dt() {
        return start_dt;
    }

    public void setStart_dt(String start_dt) {
        this.start_dt = start_dt;
    }

    public String getEnd_dt() {
        return end_dt;
    }

    public void setEnd_dt(String end_dt) {
        this.end_dt = end_dt;
    }

    public String getBank_cd() {
        return bank_cd;
    }

    public void setBank_cd(String bank_cd) {
        this.bank_cd = bank_cd;
    }

    public String getAcc_no() {
        return acc_no;
    }

    public void setAcc_no(String acc_no) {
        this.acc_no = acc_no;
    }

    public String getUse_flag() {
        return use_flag;
    }

    public void setUse_flag(String use_flag) {
        this.use_flag = use_flag;
    }

    public String getDel_flag() {
        return del_flag;
    }

    public void setDel_flag(String del_flag) {
        this.del_flag = del_flag;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }
    
    

}
