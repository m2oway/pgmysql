/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.tid.Controller;

import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.tid.Bean.TidFormBean;
import com.onoffkorea.system.tid.Service.TidService;
import com.onoffkorea.system.tid.Vo.Tb_Tid_Info;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/tid/**")
public class TidContrloller {
    
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private TidService tidService;
    
    @Autowired
    private CommonService commonService;
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }    
    
    //tid 정보 조회
    @RequestMapping(method= RequestMethod.GET,value = "/tidMasterList")
    public String tidMasterList(@Valid TidFormBean tidFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("TID_MTD");
        Tb_Code_Main PayMtdList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("TidPayMtdList",PayMtdList);
        
        commonCodeSearchFormBean.setMain_code("TID_FUNC_CATE");
        Tb_Code_Main FuncCateList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("FuncCateList",FuncCateList);
        
        commonCodeSearchFormBean.setMain_code("AGENCY_FLAG");
        Tb_Code_Main AgencyFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("AgencyFlagList",AgencyFlagList);
        
        commonCodeSearchFormBean.setMain_code("USE_FLAG");
        Tb_Code_Main UseFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("UseFlagList",UseFlagList);
        
        Hashtable ht_tidMasterList = tidService.tidMasterList(tidFormBean);
        String ls_companyMasterList = (String) ht_tidMasterList.get("ResultSet");

        model.addAttribute("tidMasterList",ls_companyMasterList);
        
        return null;
        
    }
    
    //tid 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/tidMasterList")
    public @ResponseBody String tidMasterList(@Valid TidFormBean tidFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
        
        Hashtable ht_tidMasterList = tidService.tidMasterList(tidFormBean);
        String ls_tidMasterList = (String)ht_tidMasterList.get("ResultSet");
     
        return ls_tidMasterList;
       
    }
    
    //사업체 정보 등록
    @RequestMapping(method= RequestMethod.GET,value = "/tidMasterInsert")
    public String tidMasterInsertForm(@Valid TidFormBean tidFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        /*
        MID_PAY_MTD		
        MID_FUNC_CATE	
        AGENCY_FLAG	
        BANK_CD		
        USE_FLAG	
        DEL_FLAG	
        */
        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("TID_MTD");
        Tb_Code_Main PayMtdList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("TidPayMtdList",PayMtdList);
        
        commonCodeSearchFormBean.setMain_code("TID_FUNC_CATE");
        Tb_Code_Main FuncCateList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("FuncCateList",FuncCateList);
        
        commonCodeSearchFormBean.setMain_code("AGENCY_FLAG");
        Tb_Code_Main AgencyFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("AgencyFlagList",AgencyFlagList);
        
        commonCodeSearchFormBean.setMain_code("BANK_CD");
        Tb_Code_Main BankCdList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("BankCdList",BankCdList);
        
        commonCodeSearchFormBean.setMain_code("USE_FLAG");
        Tb_Code_Main UseFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("UseFlagList",UseFlagList);
        
        
        commonCodeSearchFormBean.setMain_code("DEL_FLAG");
        Tb_Code_Main DelFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("DelFlagList",DelFlagList);
        
        commonCodeSearchFormBean.setMain_code("APPCNCLFLAG");
        Tb_Code_Main AppCnclFlag = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("AppcnclflagList",AppCnclFlag);

        
        
        return null;
        
    }   
    
    //tid 정보 등록
    @RequestMapping(method= RequestMethod.POST,value = "/tidMasterInsert")
    public @ResponseBody String tidMasterInsert(@Valid TidFormBean tidFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        tidFormBean.setIns_user(dentalHopitalSession.getUser_seq());
        tidFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        String merchant_seq = tidService.tidMasterInsert(tidFormBean);
        
        return merchant_seq;
        
    }       
    
    //tid 정보 수정 
    @RequestMapping(method= RequestMethod.GET,value = "/tidMasterUpdate")
    public String tidMasterUpdateForm(@Valid TidFormBean tidFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_tidMasterList = tidService.tidMasterInfo(tidFormBean);
        List ls_tidMasterList = (List) ht_tidMasterList.get("ResultSet");
        Tb_Tid_Info tb_Tid = (Tb_Tid_Info) ls_tidMasterList.get(0);
        
        model.addAttribute("tb_Tid",tb_Tid);
        
        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("TID_MTD");
        Tb_Code_Main PayMtdList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("TidPayMtdList",PayMtdList);
        
        commonCodeSearchFormBean.setMain_code("TID_FUNC_CATE");
        Tb_Code_Main FuncCateList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("FuncCateList",FuncCateList);
        
        commonCodeSearchFormBean.setMain_code("AGENCY_FLAG");
        Tb_Code_Main AgencyFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("AgencyFlagList",AgencyFlagList);
        
        commonCodeSearchFormBean.setMain_code("BANK_CD");
        Tb_Code_Main BankCdList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("BankCdList",BankCdList);
        
        commonCodeSearchFormBean.setMain_code("USE_FLAG");
        Tb_Code_Main UseFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("UseFlagList",UseFlagList);
        
        
        commonCodeSearchFormBean.setMain_code("DEL_FLAG");
        Tb_Code_Main DelFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("DelFlagList",DelFlagList);
        
        commonCodeSearchFormBean.setMain_code("APPCNCLFLAG");
        Tb_Code_Main AppCnclFlag = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("AppcnclflagList",AppCnclFlag);
        
        
        
        return null;
        
    }       
    
    //tid 정보 수정 
    @RequestMapping(method= RequestMethod.POST,value = "/tidMasterUpdate")
    public @ResponseBody String tidMasterUpdate(@Valid TidFormBean tidFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        tidFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        String result = tidService.tidMasterUpdate(tidFormBean);
        
        return result;
        
    }            
    
    
        //tid 정보 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/tidMasterDelete")
    public @ResponseBody String tidMasterDelete(@Valid TidFormBean tidFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {
        tidFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        logger.trace("-----------------------------------------------------------------------------");
        logger.trace("control tidFormBean.getTid_seq() : " + tidFormBean.getTid_seq());
        logger.trace("controrl tidFormBean.getMod_user(): " + tidFormBean.getMod_user());
        logger.trace("-----------------------------------------------------------------------------");
        
        String result = tidService.tidMasterDelete(tidFormBean);
        
        return result;
        
    }
}
