/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.tid.Dao;

import com.onoffkorea.system.tid.Bean.TidFormBean;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public interface TidDAO {
    
    public String tidRecordCount(TidFormBean tidFormBean);

    public List tidMasterList(TidFormBean tidFormBean);

    public List tidMasterInfo(TidFormBean tidFormBean);
    
    public Integer tidMasterDelete(TidFormBean tidFormBean);

    public Integer tidMasterInsert(TidFormBean tidFormBean);

    public Integer tidMasterUpdate(TidFormBean tidFormBean);
 
}
