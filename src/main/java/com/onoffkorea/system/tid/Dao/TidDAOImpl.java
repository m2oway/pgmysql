/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.tid.Dao;

import com.onoffkorea.system.tid.Bean.TidFormBean;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("tidDAO")
public class TidDAOImpl extends SqlSessionDaoSupport  implements TidDAO {
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Override
    public String tidRecordCount(TidFormBean tidFormBean) {
        return (String) getSqlSession().selectOne("Tid.tidRecordCount", tidFormBean);
    }

    @Override
    public List tidMasterList(TidFormBean tidFormBean) {
        return (List) getSqlSession().selectList("Tid.tidMasterList", tidFormBean);
    }

    @Override
    public List tidMasterInfo(TidFormBean tidFormBean) {
         return (List) getSqlSession().selectList("Tid.tidMasterList", tidFormBean);
    }

    @Override
    public Integer tidMasterDelete(TidFormBean tidFormBean) {
        
        logger.trace("-----------------------------------------------------------------------------");
        logger.trace("dao tidFormBean.getTid_seq() : " + tidFormBean.getTid_seq());
        logger.trace("dao idFormBean.getMod_user(): " + tidFormBean.getMod_user());
        logger.trace("-----------------------------------------------------------------------------");
         Integer result = getSqlSession().update("Tid.tidMasterDelete", tidFormBean);
         return result;
    }

    @Override
    public Integer tidMasterInsert(TidFormBean tidFormBean) {
        Integer result = getSqlSession().insert("Tid.tidMasterInsert", tidFormBean);
        return result;
    }

    @Override
    public Integer tidMasterUpdate(TidFormBean tidFormBean) {
        Integer result = getSqlSession().update("Tid.tidMasterUpdate", tidFormBean);
        return result;
    }
    
}
