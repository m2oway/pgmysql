/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.tid.Service;

import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.tid.Bean.TidFormBean;
import com.onoffkorea.system.tid.Dao.TidDAO;
import com.onoffkorea.system.tid.Vo.Tb_Tid_Info;
import java.util.Hashtable;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service("tidService")
public class TidServiceImpl implements TidService
{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private TidDAO tidDAO;
    
    
    @Override
    public Hashtable tidMasterList(TidFormBean tidFormBean) throws Exception {
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
       
        String total_count = this.tidDAO.tidRecordCount(tidFormBean);
       
        List ls_tidMasterList = this.tidDAO.tidMasterList(tidFormBean);
        
        sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(tidFormBean.getStart_no())-1)+"\",\"rows\" : [");
        
        for (int i = 0; i < ls_tidMasterList.size(); i++) {
                Tb_Tid_Info tb_Tid = (Tb_Tid_Info) ls_tidMasterList.get(i);

                /*
                <center>번호</center>,
                <center>일련번호</center>,
                <center>MID구분</center>,
                <center>MID명</center>,
                <center>MID</center>,
                <center>기능분류</center>,
                <center>자체/대행</center>,
                <center>수수료</center>,
                <center>첵크카드수수료</center>,
                <center>시작일</center>,
                <center>종료일</center>,
                <center>사용여부</center>
                */                
                
                sb.append("{\"id\":" + i);
                sb.append(",\"userdata\":{");
                sb.append(" \"tid_seq\":\""+tb_Tid.getTid_seq()+"\"");
                sb.append("}");                 
                sb.append(",\"data\":[");
                sb.append(" \"" + tb_Tid.getRnum() + "\"");
                sb.append(",\"" + "" + "\"");                
                sb.append(",\"" + Util.nullToString(tb_Tid.getTid_mtd_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Tid.getTid_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Tid.getTerminal_no()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Tid.getFunc_cate_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Tid.getAgency_flag_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Tid.getCommision()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Tid.getCommision2()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Tid.getStart_dt()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Tid.getEnd_dt()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Tid.getUse_flag_nm()) + "\"");
                
                if (i == (ls_tidMasterList.size() - 1)) {
                        sb.append("]}");
                } else {
                        sb.append("]},");
                }
        }

        sb.append("]}");    
        
        ht.put("ResultSet", sb.toString());
        
        return ht;
    }

    @Override
    @Transactional
    public String tidMasterDelete(TidFormBean tidFormBean) throws Exception {
        Integer result = null;           
        
        logger.trace("-----------------------------------------------------------------------------");
        logger.trace("service tidFormBean.getTid_seq() : " + tidFormBean.getTid_seq());
        logger.trace("servide idFormBean.getMod_user(): " + tidFormBean.getMod_user());
        logger.trace("-----------------------------------------------------------------------------");
        result =this.tidDAO.tidMasterDelete(tidFormBean);
        return result.toString();         
    }

    @Override
    @Transactional
    public String tidMasterInsert(TidFormBean tidFormBean) throws Exception {
        Integer result = null;   
        result = this.tidDAO.tidMasterInsert(tidFormBean);
        return result.toString();
    }

    @Override
    @Transactional
    public String tidMasterUpdate(TidFormBean tidFormBean) throws Exception {
        Integer result = null;  
        result = this.tidDAO.tidMasterUpdate(tidFormBean);
        return result.toString();
    }

    @Override
    public Hashtable tidMasterInfo(TidFormBean tidFormBean) throws Exception {
        
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
        
        List ls_companyMasterList = this.tidDAO.tidMasterInfo(tidFormBean);
                
        ht.put("ResultSet", ls_companyMasterList);
        
        return ht;
    }
    
}
