/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.tid.Service;

import com.onoffkorea.system.company.Bean.CompanyFormBean;
import com.onoffkorea.system.tid.Bean.TidFormBean;
import java.util.Hashtable;

/**
 *
 * @author Administrator
 */
public interface TidService {
    
    public Hashtable tidMasterList(TidFormBean tidFormBean) throws Exception;

    public String tidMasterDelete(TidFormBean tidFormBean) throws Exception;

    public String tidMasterInsert(TidFormBean tidFormBean) throws Exception;

    public String tidMasterUpdate(TidFormBean tidFormBean) throws Exception;
    
    public Hashtable tidMasterInfo(TidFormBean tidFormBean) throws Exception;
    
}
