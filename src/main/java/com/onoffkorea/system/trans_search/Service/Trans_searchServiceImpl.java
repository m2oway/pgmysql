/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.trans_search.Service;

import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.trans_search.Bean.Trans_searchFormBean;
import com.onoffkorea.system.trans_search.Dao.Trans_searchDAO;
import com.onoffkorea.system.trans_search.Vo.Tb_Tran_Cardpg;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 */
@Service("trans_searchService")
public class Trans_searchServiceImpl implements Trans_searchService{
    
    protected Logger logger = Logger.getLogger(getClass().getName());
    
    @Autowired
    private CommonService commonService;        

    @Autowired
    private Trans_searchDAO trans_searchDAO;   

    //신용거래 집계 조회
    @Override
    public Hashtable trans_searchMasterList(Trans_searchFormBean trans_searchFormBean) {
        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        
        if((trans_searchFormBean.getApp_start_dt() == null) || (trans_searchFormBean.getApp_end_dt() == "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            trans_searchFormBean.setApp_start_dt(start_dt);
            trans_searchFormBean.setApp_end_dt(end_dt);
        }
        Hashtable ht = new Hashtable();

        try {
            
                //Integer total_count = trans_searchDAO.trans_searchMasterListCount(trans_searchFormBean);
                List<Tb_Tran_Cardpg> ls_trans_searchMasterList = trans_searchDAO.trans_searchMasterList(trans_searchFormBean);
                
                StringBuilder sb = new StringBuilder();
                //sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(trans_searchFormBean.getStart_no())-1)+"\",\"rows\" : [");
                sb.append("{\"rows\" : [");
                for (int i = 0; i < ls_trans_searchMasterList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_trans_searchMasterList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"app_dt\":\""+tb_Tran_Cardpg.getApp_dt()+"\"");
                        sb.append(",\"pay_chn_cate\":\""+tb_Tran_Cardpg.getPay_chn_cate()+"\"");
                        sb.append(",\"onffmerch_no\":\""+tb_Tran_Cardpg.getOnffmerch_no()+"\"");
                        sb.append(",\"onfftid\":\""+tb_Tran_Cardpg.getOnfftid()+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append(" \"" + Util.nullToString(tb_Tran_Cardpg.getApp_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_chn_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_tax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_tax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_cnt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_cnt()) + "\"");

                        if (i == (ls_trans_searchMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");   
                
                SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
                Date start_date = formatter.parse(trans_searchFormBean.getApp_start_dt());
                Date end_date   = formatter.parse(trans_searchFormBean.getApp_end_dt());

                String start_dt = formatter2.format(start_date);
                String end_dt = formatter2.format(end_date);

                trans_searchFormBean.setApp_start_dt(start_dt);
                trans_searchFormBean.setApp_end_dt(end_dt);
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                ht.put("payChnCateList", payChnCateList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }
        

        return ht;                   
        
    }
    
    //신용거래 엑셀다운로드 조회
    @Override
    public void trans_searchMasterListExcel(Trans_searchFormBean trans_searchFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
             
                List ls_trans_searchMasterListExcel = trans_searchDAO.trans_searchMasterListExcel(trans_searchFormBean);
                
                StringBuffer trans_searchMasterListExcel = Util.makeData(ls_trans_searchMasterListExcel);
        
                Util.exceldownload(trans_searchMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }
        
    }        

    //신용카드 거래내역 상세 조회
    @Override
    public Hashtable trans_searchDetailList(Trans_searchFormBean trans_searchFormBean) {
        
         SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        if((trans_searchFormBean.getApp_start_dt() == null) || (trans_searchFormBean.getApp_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            //cal.add(Calendar.WEEK_OF_MONTH, -1);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            trans_searchFormBean.setApp_start_dt(start_dt);
            trans_searchFormBean.setApp_end_dt(end_dt);
        }
        
        Hashtable ht = new Hashtable();

        try {

                Integer total_count = trans_searchDAO.trans_searchDetailListCount(trans_searchFormBean);
                List<Tb_Tran_Cardpg> ls_trans_searchDetailList = trans_searchDAO.trans_searchDetailList(trans_searchFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(trans_searchFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_trans_searchDetailList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_trans_searchDetailList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"tran_seq\":\""+tb_Tran_Cardpg.getTran_seq()+"\"");
                        sb.append(",\"result_cd\":\""+tb_Tran_Cardpg.getResult_cd()+"\"");
                        sb.append(",\"onfftid\":\""+tb_Tran_Cardpg.getOnfftid()+"\"");
                        sb.append(",\"offmerch_no\":\""+tb_Tran_Cardpg.getOnffmerch_no()+"\"");
                        sb.append("}");                          
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Tran_Cardpg.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnfftid_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_phone2()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getProduct_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMassagetype_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getIss_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCard_num()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getInstallment()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTot_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getSvc_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_dt()) + Util.nullToString(tb_Tran_Cardpg.getApp_tm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTran_status_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_status_nm()) + "\"");                        
                        
                        //민원
                        if("".equals(Util.nullToString(tb_Tran_Cardpg.getComplain_seq()))){
                            sb.append(" ,\"<button name=complain_insert tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + ">등록</button>\"");
                        }
                        else{
                            sb.append(" ,\"<button name=complain_search tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " complain_seq=" + Util.nullToString(tb_Tran_Cardpg.getComplain_seq()) + ">보기</button>\"");
                            //sb.append(",\"" + "" + "\"");
                        }

                        //영수증
                        if("00".equals(tb_Tran_Cardpg.getResult_status()))
                        {
                            sb.append(",\"<button name=bill_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " tran_result_cd="+tb_Tran_Cardpg.getResult_cd()+" >영수증</button>\"");//영수증
                        }
                        else
                        {
                            sb.append(",\"" + "" + "\"");
                        }
                        
                        //취소버튼
                        if("00".equals(tb_Tran_Cardpg.getResult_status()) && "10".equals(tb_Tran_Cardpg.getMassagetype()) && ("00".equals(tb_Tran_Cardpg.getTran_status())||"05".equals(tb_Tran_Cardpg.getTran_status())))
                        {
                            sb.append(",\"<button name=cncl_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " org_cno="+tb_Tran_Cardpg.getPg_seq()+" >거래취소</button>\"");//영수증
                        }
                        else
                        {
                            sb.append(",\"" + "" + "\"");
                        }
                        
                        
                        //sb.append(",\"" + "" + "\"");//취소
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_chn_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_cd()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_msg()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTid()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_no()) + "\"");    
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getAcq_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCommision()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_amt()) + "\"");                                
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnofftid_commision()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnofftid_commision_vat()) + "\"");  
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getWithhold_tax()) + "\""); 
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnofftid_pay_amt()) + "\"");       
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnoffmerch_ext_pay_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getClient_ip()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getIns_user()) + "\"");                            
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOrg_app_dd()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOrg_app_no()) + "\"");                         
                        
                        if (i == (ls_trans_searchDetailList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");              
                
            SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
            Date start_date = formatter.parse(trans_searchFormBean.getApp_start_dt());
            Date end_date   = formatter.parse(trans_searchFormBean.getApp_end_dt());
            
            String start_dt = formatter2.format(start_date);
            String end_dt = formatter2.format(end_date);

            trans_searchFormBean.setApp_start_dt(start_dt);
            trans_searchFormBean.setApp_end_dt(end_dt);
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("TRAN_STATUS");
                Tb_Code_Main tranStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("RESULT_STATUS");
                Tb_Code_Main resultStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("ISS_CD");
                Tb_Code_Main issCdList = commonService.codeSearch(commonCodeSearchFormBean);    
                
                commonCodeSearchFormBean.setMain_code("ACC_CHK_FLAG");
                Tb_Code_Main AccChkFlagList = commonService.codeSearch(commonCodeSearchFormBean);  
                
                commonCodeSearchFormBean.setMain_code("MASSAGETYPE");
                Tb_Code_Main massagetypeCdList = commonService.codeSearch(commonCodeSearchFormBean);  
                
                ht.put("payChnCateList", payChnCateList);
                ht.put("tranStatusList", tranStatusList);
                ht.put("resultStatusList", resultStatusList);
                ht.put("issCdList", issCdList);
                ht.put("massagetypeCdList", massagetypeCdList);
                ht.put("AccChkFlagList", AccChkFlagList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;             
        
    }
    
    //신용거래 엑셀다운로드 조회
    @Override
    public void trans_searchDetailListExcel(Trans_searchFormBean trans_searchFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
            
                List ls_trans_searchDetailListExcel = trans_searchDAO.trans_searchDetailListExcel(trans_searchFormBean);
                
                StringBuffer trans_searchDetailListExcel = Util.makeData(ls_trans_searchDetailListExcel);
        
                Util.exceldownload(trans_searchDetailListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }
        
    }      
    
    //신용거래 실패내역 집계 조회
    @Override
    public Hashtable trans_searchFailMasterList(Trans_searchFormBean trans_searchFormBean) {
        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = trans_searchDAO.trans_searchFailMasterListCount(trans_searchFormBean);
                List<Tb_Tran_Cardpg> ls_trans_searchFailMasterList = trans_searchDAO.trans_searchFailMasterList(trans_searchFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(trans_searchFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_trans_searchFailMasterList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_trans_searchFailMasterList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"app_dt\":\""+tb_Tran_Cardpg.getApp_dt()+"\"");
                        sb.append(",\"pay_chn_cate\":\""+tb_Tran_Cardpg.getPay_chn_cate()+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Tran_Cardpg.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_chn_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_tax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_tax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_cnt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_cnt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOrg_pg_seq()) + "\"");

                        if (i == (ls_trans_searchFailMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");   
                
                
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                ht.put("payChnCateList", payChnCateList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;                   
        
    }

    //신용카드 실패내역 상세 조회
    @Override
    public Hashtable trans_searchFailDetailList(Trans_searchFormBean trans_searchFormBean) {
        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
         
        if((trans_searchFormBean.getApp_start_dt() == null) || (trans_searchFormBean.getApp_end_dt() == "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            trans_searchFormBean.setApp_start_dt(start_dt);
            trans_searchFormBean.setApp_end_dt(end_dt);
        }
        
        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = trans_searchDAO.trans_searchFailDetailListCount(trans_searchFormBean);
                List<Tb_Tran_Cardpg> ls_trans_searchFailDetailList = trans_searchDAO.trans_searchFailDetailList(trans_searchFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(trans_searchFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_trans_searchFailDetailList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_trans_searchFailDetailList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"tran_seq\":\""+tb_Tran_Cardpg.getTran_seq()+"\"");
                        sb.append(",\"result_cd\":\""+tb_Tran_Cardpg.getResult_cd()+"\"");
                        sb.append("}");                          
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Tran_Cardpg.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnfftid_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTran_dt())+Util.nullToString(tb_Tran_Cardpg.getTran_tm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_phone2()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getProduct_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMassagetype_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getIss_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCard_num()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getInstallment()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTot_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getSvc_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_status_nm()) + "\"");                        
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_chn_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_cd()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_msg()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTid()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_no()) + "\"");    
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getAcq_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getClient_ip()) + "\"");
                        
                        if (i == (ls_trans_searchFailDetailList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");            
                
                SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
                Date start_date = formatter.parse(trans_searchFormBean.getApp_start_dt());
                Date end_date   = formatter.parse(trans_searchFormBean.getApp_end_dt());

                String start_dt = formatter2.format(start_date);
                String end_dt = formatter2.format(end_date);

                trans_searchFormBean.setApp_start_dt(start_dt);
                trans_searchFormBean.setApp_end_dt(end_dt);
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);            
                
                commonCodeSearchFormBean.setMain_code("TRAN_STATUS");
                Tb_Code_Main tranStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("RESULT_STATUS");
                Tb_Code_Main resultStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("ISS_CD");
                Tb_Code_Main issCdList = commonService.codeSearch(commonCodeSearchFormBean);                   
                
                ht.put("payChnCateList", payChnCateList);
                ht.put("tranStatusList", tranStatusList);
                ht.put("resultStatusList", resultStatusList);
                ht.put("issCdList", issCdList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;             
        
    }
    
    //현금거래 집계 조회
    @Override
    public Hashtable cashtrans_searchMasterList(Trans_searchFormBean trans_searchFormBean) {
        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        
        if((trans_searchFormBean.getApp_start_dt() == null) || (trans_searchFormBean.getApp_end_dt() == "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            trans_searchFormBean.setApp_start_dt(start_dt);
            trans_searchFormBean.setApp_end_dt(end_dt);
        }
        
        Hashtable ht = new Hashtable();

        try {
            
                //Integer total_count = trans_searchDAO.cashtrans_searchMasterListCount(trans_searchFormBean);
                List<Tb_Tran_Cardpg> ls_cashtrans_searchMasterList = trans_searchDAO.cashtrans_searchMasterList(trans_searchFormBean);
                
                StringBuilder sb = new StringBuilder();
                //sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(trans_searchFormBean.getStart_no())-1)+"\",\"rows\" : [");
                sb.append("{\"rows\" : [");
                for (int i = 0; i < ls_cashtrans_searchMasterList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_cashtrans_searchMasterList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"app_dt\":\""+tb_Tran_Cardpg.getApp_dt()+"\"");
                        sb.append(",\"pay_chn_cate\":\""+tb_Tran_Cardpg.getPay_chn_cate()+"\"");
                        sb.append(",\"onffmerch_no\":\""+tb_Tran_Cardpg.getOnffmerch_no()+"\"");
                        sb.append(",\"onfftid\":\""+tb_Tran_Cardpg.getOnfftid()+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append(" \"" + Util.nullToString(tb_Tran_Cardpg.getApp_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_chn_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_tax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_tax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_cnt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_cnt()) + "\"");

                        if (i == (ls_cashtrans_searchMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");
                
                SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
                Date start_date = formatter.parse(trans_searchFormBean.getApp_start_dt());
                Date end_date   = formatter.parse(trans_searchFormBean.getApp_end_dt());

                String start_dt = formatter2.format(start_date);
                String end_dt = formatter2.format(end_date);

                trans_searchFormBean.setApp_start_dt(start_dt);
                trans_searchFormBean.setApp_end_dt(end_dt);
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                ht.put("payChnCateList", payChnCateList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;                   
        
    }

    //현금 거래내역 상세 조회
    @Override
    public Hashtable cashtrans_searchDetailList(Trans_searchFormBean trans_searchFormBean) {
        
        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = trans_searchDAO.cashtrans_searchDetailListCount(trans_searchFormBean);
                List<Tb_Tran_Cardpg> ls_cashtrans_searchDetailList = trans_searchDAO.cashtrans_searchDetailList(trans_searchFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(trans_searchFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_cashtrans_searchDetailList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_cashtrans_searchDetailList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"tran_seq\":\""+tb_Tran_Cardpg.getTran_seq()+"\"");
                        sb.append(",\"result_cd\":\""+tb_Tran_Cardpg.getResult_cd()+"\"");
                        sb.append("}");                               
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Tran_Cardpg.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnfftid_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_phone2()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getProduct_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMassagetype_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCard_num()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getWcc_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCard_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTot_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getSvc_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_dt()) + Util.nullToString(tb_Tran_Cardpg.getApp_tm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTran_status_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_status_nm()) + "\"");
                        sb.append(",\"" + "" + "\"");//민원
                        
                        //영수증
                        if("00".equals(tb_Tran_Cardpg.getResult_status()))
                        {
                            sb.append(",\"<button name=cashbill_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " tran_result_cd="+tb_Tran_Cardpg.getResult_cd()+" >영수증</button>\"");//영수증
                        }
                        else
                        {
                            sb.append(",\"" + "" + "\"");
                        }
                        
                        //취소버튼
                        if("00".equals(tb_Tran_Cardpg.getResult_status()) && "10".equals(tb_Tran_Cardpg.getMassagetype()) && ("00".equals(tb_Tran_Cardpg.getTran_status()) ))
                        {
                            sb.append(",\"<button name=cncl_cash_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " org_cno="+tb_Tran_Cardpg.getPg_seq()+" >거래취소</button>\"");//영수증
                        }
                        else
                        {
                            sb.append(",\"" + "" + "\"");
                        }
                        
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_chn_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_cd()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_msg()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTid()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getClient_ip()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_id()) + "\"");
                        
                        if (i == (ls_cashtrans_searchDetailList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
                
                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("TRAN_STATUS");
                Tb_Code_Main tranStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("RESULT_STATUS");
                Tb_Code_Main resultStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("ISS_CD");
                Tb_Code_Main issCdList = commonService.codeSearch(commonCodeSearchFormBean);                                
                
                ht.put("payChnCateList", payChnCateList);
                ht.put("tranStatusList", tranStatusList);
                ht.put("resultStatusList", resultStatusList);
                ht.put("issCdList", issCdList);                
                
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;             
        
    }    
    
    //현금거래 집계 엑셀다운로드 조회
    @Override
    public void cashTrans_searchMasterListExcel(Trans_searchFormBean trans_searchFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
             
                List ls_cashTrans_searchMasterListExcel = trans_searchDAO.cashTrans_searchMasterListExcel(trans_searchFormBean);
                
                StringBuffer cashTrans_searchMasterListExcel = Util.makeData(ls_cashTrans_searchMasterListExcel);
        
                Util.exceldownload(cashTrans_searchMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }
        
    }        
    
    //현금거래 엑셀다운로드 조회
    @Override
    public void cashTrans_searchDetailListExcel(Trans_searchFormBean trans_searchFormBean, String excelDownloadFilename, HttpServletResponse response) {
 
        try {
             
                List ls_cashTrans_searchDetailListExcel = trans_searchDAO.cashTrans_searchDetailListExcel(trans_searchFormBean);
                
                StringBuffer cashTrans_searchMasterListExcel = Util.makeData(ls_cashTrans_searchDetailListExcel);
        
                Util.exceldownload(cashTrans_searchMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }
        
    }           
    
    //신용거래 실패내역 엑셀다운로드 조회
    @Override
    public void trans_searchFailDetailListExcel(Trans_searchFormBean trans_searchFormBean, String excelDownloadFilename, HttpServletResponse response) {
 
        try {
             
                List ls_trans_searchFailDetailListExcel = trans_searchDAO.trans_searchFailDetailListExcel(trans_searchFormBean);
                
                StringBuffer trans_searchFailDetailListExcel = Util.makeData(ls_trans_searchFailDetailListExcel);
        
                Util.exceldownload(trans_searchFailDetailListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }
        
    }          

    //가맹점 신용카드 집계 조회
    @Override
    public Hashtable merchTrans_searchMasterList(Trans_searchFormBean trans_searchFormBean) {

        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        
        if((trans_searchFormBean.getApp_start_dt() == null) || (trans_searchFormBean.getApp_end_dt() == "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            trans_searchFormBean.setApp_start_dt(start_dt);
            trans_searchFormBean.setApp_end_dt(end_dt);
        }
        Hashtable ht = new Hashtable();

        try {
            
                //Integer total_count = trans_searchDAO.merchTrans_searchMasterListCount(trans_searchFormBean);
                List<Tb_Tran_Cardpg> ls_trans_searchMasterList = trans_searchDAO.merchTrans_searchMasterList(trans_searchFormBean);
                
                StringBuilder sb = new StringBuilder();
                //sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(trans_searchFormBean.getStart_no())-1)+"\",\"rows\" : [");
                 sb.append("{\"rows\" : [");
                for (int i = 0; i < ls_trans_searchMasterList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_trans_searchMasterList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"app_dt\":\""+tb_Tran_Cardpg.getApp_dt()+"\"");
                        sb.append(",\"pay_chn_cate\":\""+tb_Tran_Cardpg.getPay_chn_cate()+"\"");
                        sb.append(",\"onffmerch_no\":\""+tb_Tran_Cardpg.getOnffmerch_no()+"\"");
                        sb.append(",\"onfftid\":\""+tb_Tran_Cardpg.getOnfftid()+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append(" \"" + Util.nullToString(tb_Tran_Cardpg.getApp_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_chn_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnfftid_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_tax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_tax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_cnt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_cnt()) + "\"");

                        if (i == (ls_trans_searchMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");   
                
                SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
                Date start_date = formatter.parse(trans_searchFormBean.getApp_start_dt());
                Date end_date   = formatter.parse(trans_searchFormBean.getApp_end_dt());

                String start_dt = formatter2.format(start_date);
                String end_dt = formatter2.format(end_date);

                trans_searchFormBean.setApp_start_dt(start_dt);
                trans_searchFormBean.setApp_end_dt(end_dt);
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                ht.put("payChnCateList", payChnCateList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }
        

        return ht;              
        
    }

    //가맹점 신용카드 집계 엑셀다운로드
    @Override
    public void merchTrans_searchMasterListExcel(Trans_searchFormBean trans_searchFormBean, String excelDownloadFilename, HttpServletResponse response) {
        
        try {
             
                List ls_trans_searchMasterListExcel = trans_searchDAO.merchTrans_searchMasterListExcel(trans_searchFormBean);
                
                StringBuffer trans_searchMasterListExcel = Util.makeData(ls_trans_searchMasterListExcel);
        
                Util.exceldownload(trans_searchMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    }
    
    //가맹점 신용카드 거래내역 상세 조회
    @Override
    public Hashtable merchTrans_searchDetailList(Trans_searchFormBean trans_searchFormBean) {
        
        String strCurDate = Util.getTodayDate();
        
         SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        if((trans_searchFormBean.getApp_start_dt() == null) || (trans_searchFormBean.getApp_start_dt()== "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.WEEK_OF_MONTH, -1);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            trans_searchFormBean.setApp_start_dt(start_dt);
            trans_searchFormBean.setApp_end_dt(end_dt);
        }
        
        Hashtable ht = new Hashtable();

        try {

                Integer total_count = trans_searchDAO.merchTrans_searchDetailListCount(trans_searchFormBean);
                List<Tb_Tran_Cardpg> ls_trans_searchDetailList = trans_searchDAO.merchTrans_searchDetailList(trans_searchFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(trans_searchFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_trans_searchDetailList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_trans_searchDetailList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"tran_seq\":\""+tb_Tran_Cardpg.getTran_seq()+"\"");
                        sb.append(",\"result_cd\":\""+tb_Tran_Cardpg.getResult_cd()+"\"");
                        sb.append("}");                          
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Tran_Cardpg.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnfftid_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_phone2()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getProduct_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMassagetype_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getIss_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCard_num()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getInstallment()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTot_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getSvc_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_dt()) + Util.nullToString(tb_Tran_Cardpg.getApp_tm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTran_status_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_status_nm()) + "\"");                        
                        
                        sb.append(",\"" + "" + "\"");//민원   
                        
                        //영수증
                        if("00".equals(tb_Tran_Cardpg.getResult_status()))
                        {
                            sb.append(",\"<button name=bill_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " tran_result_cd="+tb_Tran_Cardpg.getResult_cd()+" >영수증</button>\"");//영수증
                        }
                        else
                        {
                            sb.append(",\"" + "" + "\"");
                        }
                        
                        String strTpsesUserCate = Util.nullToString(trans_searchFormBean.getSes_user_cate());
                        String strTpsesmerch_cncl_auth = Util.nullToString(trans_searchFormBean.getSes_merch_cncl_auth());
                        String strTpsestid_cncl_auth = Util.nullToString(trans_searchFormBean.getSes_tid_cncl_auth());
                        //취소버튼
                        //가맹점 관리자
                        if(strTpsesUserCate.equals("01"))
                        {
                            //결제취소가능 상태일 경우
                            //if("00".equals(tb_Tran_Cardpg.getResult_status()) && "10".equals(tb_Tran_Cardpg.getMassagetype()) && "00".equals(tb_Tran_Cardpg.getTran_status()) )
                            if("00".equals(tb_Tran_Cardpg.getResult_status()) && "10".equals(tb_Tran_Cardpg.getMassagetype()) && ("00".equals(tb_Tran_Cardpg.getTran_status()) || "05".equals(tb_Tran_Cardpg.getTran_status()) ) )
                            {
                                if(strTpsesmerch_cncl_auth.equals("00"))
                                {
                                    sb.append(",\"<button name=cncl_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " org_cno="+tb_Tran_Cardpg.getPg_seq()+" >거래취소</button>\"");
                                }
                                else
                                {              
                                    //승인일과 취소일이 같은 경우  
                                    //당일취소의 경우
                                    if(strCurDate.equals(tb_Tran_Cardpg.getApp_dt()))
                                    {
                                        sb.append(",\"<button name=cncl_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " org_cno="+tb_Tran_Cardpg.getPg_seq()+" >거래취소</button>\"");
                                    }
                                    //익일취소의 경우
                                    else
                                    {
                                        sb.append(",\"<button name=cnclreq_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " org_cno="+tb_Tran_Cardpg.getPg_seq()+" >취소요청</button>\"");
                                    }
                                }
                            }
                            else
                            {
                                sb.append(",\"" + "" + "\"");
                            }

                        }
                        //결제관리자
                        else if(strTpsesUserCate.equals("02"))
                        {
                            //결제취소가능 상태일 경우
                            if("00".equals(tb_Tran_Cardpg.getResult_status()) && "10".equals(tb_Tran_Cardpg.getMassagetype()) && "00".equals(tb_Tran_Cardpg.getTran_status()) )
                            {

                                if(strTpsestid_cncl_auth.equals("00"))
                                {
                                    sb.append(",\"<button name=cncl_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " org_cno="+tb_Tran_Cardpg.getPg_seq()+" >거래취소</button>\"");//영수증
                                }
                                else
                                {       
                                     //승인일과 취소일이 같은 경우  
                                    //당일취소일경우
                                    if(strCurDate.equals(tb_Tran_Cardpg.getApp_dt()))
                                    {
                                        sb.append(",\"<button name=cncl_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " org_cno="+tb_Tran_Cardpg.getPg_seq()+" >거래취소</button>\"");
                                    }
                                    //익일취소일 경우
                                    else
                                    {
                                        sb.append(",\"<button name=cnclreq_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " org_cno="+tb_Tran_Cardpg.getPg_seq()+" >취소요청</button>\"");                                        
                                    }
                                }
                            }
                            else
                            {
                                sb.append(",\"" + "" + "\"");
                            }                            
                        }
                        //대리점관리자
                        else if(strTpsesUserCate.equals("03"))
                        {
                             sb.append(",\"" + "" + "\"");
                        }
                        
                        
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_chn_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOrg_app_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOrg_app_dd()) + "\"");
                        
                        if (i == (ls_trans_searchDetailList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");              
                
            SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
            Date start_date = formatter.parse(trans_searchFormBean.getApp_start_dt());
            Date end_date   = formatter.parse(trans_searchFormBean.getApp_end_dt());
            
            String start_dt = formatter2.format(start_date);
            String end_dt = formatter2.format(end_date);

            trans_searchFormBean.setApp_start_dt(start_dt);
            trans_searchFormBean.setApp_end_dt(end_dt);
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("TRAN_STATUS");
                Tb_Code_Main tranStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("RESULT_STATUS");
                Tb_Code_Main resultStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("ISS_CD");
                Tb_Code_Main issCdList = commonService.codeSearch(commonCodeSearchFormBean); 
                
                commonCodeSearchFormBean.setMain_code("MASSAGETYPE");
                Tb_Code_Main massagetypeCdList = commonService.codeSearch(commonCodeSearchFormBean);  
                
                ht.put("payChnCateList", payChnCateList);
                ht.put("tranStatusList", tranStatusList);
                ht.put("resultStatusList", resultStatusList);
                ht.put("issCdList", issCdList);
                ht.put("massagetypeCdList", massagetypeCdList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;           
        
    }        

    //가맹점 신용카드 상세 내역 엑셀다운로드
    @Override
    public void merchTrans_searchDetailListExcel(Trans_searchFormBean trans_searchFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
            List ls_trans_searchDetailListExcel = trans_searchDAO.merchTrans_searchDetailListExcel(trans_searchFormBean);
            
            StringBuffer trans_searchDetailListExcel = Util.makeData(ls_trans_searchDetailListExcel);
            
            Util.exceldownload(trans_searchDetailListExcel.toString(), excelDownloadFilename, response);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(Trans_searchServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    //가맹점 신용카드 실패내역 조회
    @Override
    public Hashtable merchTrans_searchFailDetailList(Trans_searchFormBean trans_searchFormBean) {

        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
         
        if((trans_searchFormBean.getApp_start_dt() == null) || (trans_searchFormBean.getApp_end_dt() == "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            trans_searchFormBean.setApp_start_dt(start_dt);
            trans_searchFormBean.setApp_end_dt(end_dt);
        }
        
        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = trans_searchDAO.merchTrans_searchFailDetailListCount(trans_searchFormBean);
                List<Tb_Tran_Cardpg> ls_trans_searchFailDetailList = trans_searchDAO.merchTrans_searchFailDetailList(trans_searchFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(trans_searchFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_trans_searchFailDetailList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_trans_searchFailDetailList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"tran_seq\":\""+tb_Tran_Cardpg.getTran_seq()+"\"");
                        sb.append(",\"result_cd\":\""+tb_Tran_Cardpg.getResult_cd()+"\"");
                        sb.append("}");                          
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Tran_Cardpg.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnfftid_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTran_dt())+Util.nullToString(tb_Tran_Cardpg.getTran_tm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_phone2()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getProduct_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMassagetype_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getIss_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCard_num()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getInstallment()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTot_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getSvc_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_status_nm()) + "\"");                        
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_chn_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_cd()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_msg()) + "\"");
                        //sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTid()) + "\"");
                        //sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_no()) + "\"");    
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getAcq_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getClient_ip()) + "\"");                       
                        
                        if (i == (ls_trans_searchFailDetailList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");            
                
                SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
                Date start_date = formatter.parse(trans_searchFormBean.getApp_start_dt());
                Date end_date   = formatter.parse(trans_searchFormBean.getApp_end_dt());

                String start_dt = formatter2.format(start_date);
                String end_dt = formatter2.format(end_date);

                trans_searchFormBean.setApp_start_dt(start_dt);
                trans_searchFormBean.setApp_end_dt(end_dt);
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);            
                
                commonCodeSearchFormBean.setMain_code("TRAN_STATUS");
                Tb_Code_Main tranStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("RESULT_STATUS");
                Tb_Code_Main resultStatusList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                commonCodeSearchFormBean.setMain_code("ISS_CD");
                Tb_Code_Main issCdList = commonService.codeSearch(commonCodeSearchFormBean);                   
                
                ht.put("payChnCateList", payChnCateList);
                ht.put("tranStatusList", tranStatusList);
                ht.put("resultStatusList", resultStatusList);
                ht.put("issCdList", issCdList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;                     
        
    }

    //가맹점 신용카드 실패내역 엑셀다운로드
    @Override
    public void merchTrans_searchFailDetailListExcel(Trans_searchFormBean trans_searchFormBean, String excelDownloadFilename, HttpServletResponse response) {
        
        try {
            List ls_trans_searchFailDetailListExcel = trans_searchDAO.merchTrans_searchFailDetailListExcel(trans_searchFormBean);
            
            StringBuffer trans_searchFailDetailListExcel = Util.makeData(ls_trans_searchFailDetailListExcel);
            
            Util.exceldownload(trans_searchFailDetailListExcel.toString(), excelDownloadFilename, response);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(Trans_searchServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
    }

    //가맹점 현금거래 집계 조회
    @Override
    public Hashtable merchCashtrans_searchMasterList(Trans_searchFormBean trans_searchFormBean) {

        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        
        if((trans_searchFormBean.getApp_start_dt() == null) || (trans_searchFormBean.getApp_end_dt() == "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            trans_searchFormBean.setApp_start_dt(start_dt);
            trans_searchFormBean.setApp_end_dt(end_dt);
        }
        
        Hashtable ht = new Hashtable();

        try {
            
               // Integer total_count = trans_searchDAO.merchCashtrans_searchMasterListCount(trans_searchFormBean);
                List<Tb_Tran_Cardpg> ls_cashtrans_searchMasterList = trans_searchDAO.merchCashtrans_searchMasterList(trans_searchFormBean);
                
                StringBuilder sb = new StringBuilder();
                //sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(trans_searchFormBean.getStart_no())-1)+"\",\"rows\" : [");
                sb.append("{\"rows\" : [");
                for (int i = 0; i < ls_cashtrans_searchMasterList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_cashtrans_searchMasterList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"app_dt\":\""+tb_Tran_Cardpg.getApp_dt()+"\"");
                        sb.append(",\"pay_chn_cate\":\""+tb_Tran_Cardpg.getPay_chn_cate()+"\"");
                        sb.append(",\"onffmerch_no\":\""+tb_Tran_Cardpg.getOnffmerch_no()+"\"");
                        sb.append(",\"onfftid\":\""+tb_Tran_Cardpg.getOnfftid()+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append(" \"" + Util.nullToString(tb_Tran_Cardpg.getApp_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_chn_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnfftid_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_tax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_tax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_cnt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_cnt()) + "\"");

                        if (i == (ls_cashtrans_searchMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");
                
                SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
                Date start_date = formatter.parse(trans_searchFormBean.getApp_start_dt());
                Date end_date   = formatter.parse(trans_searchFormBean.getApp_end_dt());

                String start_dt = formatter2.format(start_date);
                String end_dt = formatter2.format(end_date);

                trans_searchFormBean.setApp_start_dt(start_dt);
                trans_searchFormBean.setApp_end_dt(end_dt);
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                ht.put("payChnCateList", payChnCateList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;               
        
    }
    
    //가맹점 현금거래 집계 엑셀다운로드
    @Override
    public void merchCashTrans_searchMasterListExcel(Trans_searchFormBean trans_searchFormBean, String excelDownloadFilename, HttpServletResponse response) {
        
        try {
             
                List ls_cashTrans_searchDetailListExcel = trans_searchDAO.merchCashTrans_searchMasterListExcel(trans_searchFormBean);
                
                StringBuffer cashTrans_searchMasterListExcel = Util.makeData(ls_cashTrans_searchDetailListExcel);
        
                Util.exceldownload(cashTrans_searchMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }
        
    }    

    //가맹점 현금거래 상세 조회
    @Override
    public Hashtable merchCashtrans_searchDetailList(Trans_searchFormBean trans_searchFormBean) {
        
        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = trans_searchDAO.merchCashtrans_searchDetailListCount(trans_searchFormBean);
                List<Tb_Tran_Cardpg> ls_cashtrans_searchDetailList = trans_searchDAO.merchCashtrans_searchDetailList(trans_searchFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(trans_searchFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_cashtrans_searchDetailList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_cashtrans_searchDetailList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"tran_seq\":\""+tb_Tran_Cardpg.getTran_seq()+"\"");
                        sb.append(",\"result_cd\":\""+tb_Tran_Cardpg.getResult_cd()+"\"");
                        sb.append("}");                               
                        sb.append(",\"data\":[");
                        sb.append("\"" + Util.nullToString(tb_Tran_Cardpg.getRnum()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnfftid_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_phone2()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getProduct_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMassagetype_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCard_num()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getWcc_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCard_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTot_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getSvc_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTax_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_no()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getApp_dt()) + Util.nullToString(tb_Tran_Cardpg.getApp_tm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCncl_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTran_status_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_status_nm()) + "\"");
                        
                        //sb.append(",\"" + "" + "\"");
                        //sb.append(",\"" + "" + "\"");
                        //sb.append(",\"" + "" + "\"");
                        
                        sb.append(",\"" + "" + "\"");//민원   
                        
                        //영수증
                        if("00".equals(tb_Tran_Cardpg.getResult_status()))
                        {
                            sb.append(",\"<button name=cashbill_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " tran_result_cd="+tb_Tran_Cardpg.getResult_cd()+" >영수증</button>\"");//영수증
                        }
                        else
                        {
                            sb.append(",\"" + "" + "\"");
                        }
                        
                        String strTpsesUserCate = Util.nullToString(trans_searchFormBean.getSes_user_cate());
                        String strTpsesmerch_cncl_auth = Util.nullToString(trans_searchFormBean.getSes_merch_cncl_auth());
                        String strTpsestid_cncl_auth = Util.nullToString(trans_searchFormBean.getSes_tid_cncl_auth());
                        //취소버튼
                        //가맹점 관리자
                        if(strTpsesUserCate.equals("01"))
                        {
                            //결제취소가능 상태일 경우
                            if("00".equals(tb_Tran_Cardpg.getResult_status()) && "10".equals(tb_Tran_Cardpg.getMassagetype()) && ("00".equals(tb_Tran_Cardpg.getTran_status())||"05".equals(tb_Tran_Cardpg.getTran_status())))
                            {
                                //if(strTpsesmerch_cncl_auth.equals("00"))
                                //{
                                    sb.append(",\"<button name=cncl_cash_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " org_cno="+tb_Tran_Cardpg.getPg_seq()+" >거래취소</button>\"");//영수증
                                //}
                                //else
                                //{                                
                                //    sb.append(",\"<button name=cnclreq_cash_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " org_cno="+tb_Tran_Cardpg.getPg_seq()+" >거래취소</button>\"");//영수증
                                //}
                            }
                            else
                            {
                                sb.append(",\"" + "" + "\"");
                            }

                        }
                        //결제관리자
                        else if(strTpsesUserCate.equals("02"))
                        {
                            //결제취소가능 상태일 경우
                            if("00".equals(tb_Tran_Cardpg.getResult_status()) && "10".equals(tb_Tran_Cardpg.getMassagetype()) && ("00".equals(tb_Tran_Cardpg.getTran_status())||"05".equals(tb_Tran_Cardpg.getTran_status())))
                            {
                                //if(strTpsesmerch_cncl_auth.equals("00"))
                               // {
                                    sb.append(",\"<button name=cncl_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " org_cno="+tb_Tran_Cardpg.getPg_seq()+" >거래취소</button>\"");//영수증
                              //  }
                              ///  else
                               // {                                
                              //      sb.append(",\"<button name=cnclreq_cash_view_bt tran_seq=" + Util.nullToString(tb_Tran_Cardpg.getTran_seq()) + " org_cno="+tb_Tran_Cardpg.getPg_seq()+" >거래취소</button>\"");//영수증
                              //  }
                            }
                            else
                            {
                                sb.append(",\"" + "" + "\"");
                            }                            
                        }
                        //대리점관리자
                        else if(strTpsesUserCate.equals("03"))
                        {
                             sb.append(",\"" + "" + "\"");
                        }                        
                        
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getPay_chn_cate_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_cd()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getResult_msg()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getTid()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getClient_ip()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getUser_id()) + "\"");
                        
                        if (i == (ls_cashtrans_searchDetailList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");                    
            
                ht.put("ResultSet", sb.toString());
                
                CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();

                commonCodeSearchFormBean.setMain_code("PAY_CHN_CATE");
                Tb_Code_Main payChnCateList = commonService.codeSearch(commonCodeSearchFormBean);                
                
                ht.put("payChnCateList", payChnCateList);
                
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;            
        
    }

    @Override
    public void merchCashTrans_searchDetailListExcel(Trans_searchFormBean trans_searchFormBean, String excelDownloadFilename, HttpServletResponse response) {
        
        try {
             
                List ls_cashTrans_searchDetailListExcel = trans_searchDAO.merchCashTrans_searchDetailListExcel(trans_searchFormBean);
                
                StringBuffer cashTrans_searchMasterListExcel = Util.makeData(ls_cashTrans_searchDetailListExcel);
        
                Util.exceldownload(cashTrans_searchMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    }
    
    

    //신용이상거래 집계 조회
    @Override
    public Hashtable trans_searchChkMasterList(Trans_searchFormBean trans_searchFormBean) {
        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        
        if((trans_searchFormBean.getApp_start_dt() == null) || (trans_searchFormBean.getApp_end_dt() == "")){
            Date now = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(now);
            cal.add(Calendar.DAY_OF_WEEK, -6);

            
            Date currentTime = new Date ( );
            String start_dt = formatter.format ( cal.getTime());
            String end_dt = formatter.format(currentTime.getTime());


            trans_searchFormBean.setApp_start_dt(start_dt);
            trans_searchFormBean.setApp_end_dt(end_dt);
        }
        
        if((trans_searchFormBean.getChktran_cnt()== null) || (trans_searchFormBean.getChktran_cnt() == "")){
                trans_searchFormBean.setChktran_cnt("3");
        }        
        Hashtable ht = new Hashtable();

        try {
            
                Integer total_count = trans_searchDAO.trans_searchChkMasterListCount(trans_searchFormBean);
                List<Tb_Tran_Cardpg> ls_trans_searchMasterList = trans_searchDAO.trans_searchChkMasterList(trans_searchFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(trans_searchFormBean.getStart_no())-1)+"\",\"rows\" : [");
                for (int i = 0; i < ls_trans_searchMasterList.size(); i++) {
                        Tb_Tran_Cardpg tb_Tran_Cardpg = ls_trans_searchMasterList.get(i);
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"app_dt\":\""+tb_Tran_Cardpg.getApp_dt()+"\"");
                        sb.append(",\"onffmerch_no\":\""+tb_Tran_Cardpg.getOnffmerch_no()+"\"");
                        sb.append(",\"onfftid\":\""+tb_Tran_Cardpg.getOnfftid()+"\"");
                        sb.append(",\"massagetype\":\""+tb_Tran_Cardpg.getMassagetype()+"\"");
                        sb.append(",\"card_num\":\""+tb_Tran_Cardpg.getCard_num()+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append(" \"" + Util.nullToString(tb_Tran_Cardpg.getApp_dt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getOnfftid_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getMassagetype_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getCard_num()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_Tran_Cardpg.getChktrancnt()) + "\"");

                        if (i == (ls_trans_searchMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");   
                
                SimpleDateFormat formatter2 = new SimpleDateFormat ( "yyyy-MM-dd", Locale.KOREA );
                Date start_date = formatter.parse(trans_searchFormBean.getApp_start_dt());
                Date end_date   = formatter.parse(trans_searchFormBean.getApp_end_dt());

                String start_dt = formatter2.format(start_date);
                String end_dt = formatter2.format(end_date);

                trans_searchFormBean.setApp_start_dt(start_dt);
                trans_searchFormBean.setApp_end_dt(end_dt);
            
                ht.put("ResultSet", sb.toString());
                
        } catch (Exception e) {
                e.printStackTrace();
        }
        

        return ht;                   
        
    }
    
}
