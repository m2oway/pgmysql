/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.trans_search.Service;

import com.onoffkorea.system.trans_search.Bean.Trans_searchFormBean;
import java.util.Hashtable;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public interface Trans_searchService {

    public Hashtable trans_searchMasterList(Trans_searchFormBean trans_searchFormBean);

    public Hashtable trans_searchDetailList(Trans_searchFormBean trans_searchFormBean);
    
    public Hashtable merchTrans_searchDetailList(Trans_searchFormBean trans_searchFormBean);

    public Hashtable trans_searchFailMasterList(Trans_searchFormBean trans_searchFormBean);

    public Hashtable trans_searchFailDetailList(Trans_searchFormBean trans_searchFormBean);

    public Hashtable cashtrans_searchMasterList(Trans_searchFormBean trans_searchFormBean);

    public Hashtable cashtrans_searchDetailList(Trans_searchFormBean trans_searchFormBean);

    public void trans_searchMasterListExcel(Trans_searchFormBean trans_searchFormBean, String excelDownloadFilename, HttpServletResponse response);

    public void trans_searchDetailListExcel(Trans_searchFormBean trans_searchFormBean, String excelDownloadFilename, HttpServletResponse response);

    public void cashTrans_searchMasterListExcel(Trans_searchFormBean trans_searchFormBean, String excelDownloadFilename, HttpServletResponse response);

    public void cashTrans_searchDetailListExcel(Trans_searchFormBean trans_searchFormBean, String excelDownloadFilename, HttpServletResponse response);

    public void trans_searchFailDetailListExcel(Trans_searchFormBean trans_searchFormBean, String excelDownloadFilename, HttpServletResponse response);

    public Hashtable merchTrans_searchMasterList(Trans_searchFormBean trans_searchFormBean);

    public void merchTrans_searchMasterListExcel(Trans_searchFormBean trans_searchFormBean, String excelDownloadFilename, HttpServletResponse response);

    public void merchTrans_searchDetailListExcel(Trans_searchFormBean trans_searchFormBean, String excelDownloadFilename, HttpServletResponse response);

    public Hashtable merchTrans_searchFailDetailList(Trans_searchFormBean trans_searchFormBean);

    public void merchTrans_searchFailDetailListExcel(Trans_searchFormBean trans_searchFormBean, String excelDownloadFilename, HttpServletResponse response);

    public Hashtable merchCashtrans_searchMasterList(Trans_searchFormBean trans_searchFormBean);

    public void merchCashTrans_searchMasterListExcel(Trans_searchFormBean trans_searchFormBean, String excelDownloadFilename, HttpServletResponse response);

    public Hashtable merchCashtrans_searchDetailList(Trans_searchFormBean trans_searchFormBean);

    public void merchCashTrans_searchDetailListExcel(Trans_searchFormBean trans_searchFormBean, String excelDownloadFilename, HttpServletResponse response);
    
    public Hashtable trans_searchChkMasterList(Trans_searchFormBean trans_searchFormBean) ;
    
}
