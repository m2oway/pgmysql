/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.trans_search.Dao;

import com.onoffkorea.system.trans_search.Bean.Trans_searchFormBean;
import com.onoffkorea.system.trans_search.Vo.Tb_Tran_Cardpg;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("trans_searchDAO")
public class Trans_searchDAOImpl extends SqlSessionDaoSupport implements Trans_searchDAO{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    //신용카드 집계 조회 카운트
    @Override
    public Integer trans_searchMasterListCount(Trans_searchFormBean trans_searchFormBean) {
        
        return (Integer)getSqlSession().selectOne("Trans_search.trans_searchMasterListCount",trans_searchFormBean);
        
    }    
    
    //신용카드 집계 조회
    @Override
    public List<Tb_Tran_Cardpg> trans_searchMasterList(Trans_searchFormBean trans_searchFormBean) {
        
        return (List)getSqlSession().selectList("Trans_search.trans_searchMasterList",trans_searchFormBean);
        
    }

    //신용카드 상세 조회 카운트
    @Override
    public Integer trans_searchDetailListCount(Trans_searchFormBean trans_searchFormBean) {
        
        return (Integer)getSqlSession().selectOne("Trans_search.trans_searchDetailListCount",trans_searchFormBean);
        
    }

    //신용카드 상세 조회 
    @Override
    public List<Tb_Tran_Cardpg> trans_searchDetailList(Trans_searchFormBean trans_searchFormBean) {
        
        return (List)getSqlSession().selectList("Trans_search.trans_searchDetailList",trans_searchFormBean);
        
    }
    
    //신용카드 실패내역 집계 조회 카운트
    @Override
    public Integer trans_searchFailMasterListCount(Trans_searchFormBean trans_searchFormBean) {
        
        return (Integer)getSqlSession().selectOne("Trans_search.trans_searchFailMasterListCount",trans_searchFormBean);
        
    }    
    
    //신용카드 실패내역 집계 조회
    @Override
    public List<Tb_Tran_Cardpg> trans_searchFailMasterList(Trans_searchFormBean trans_searchFormBean) {
        
        return (List)getSqlSession().selectList("Trans_search.trans_searchFailMasterList",trans_searchFormBean);
        
    }

    //신용카드 실패내역 상세 조회 카운트
    @Override
    public Integer trans_searchFailDetailListCount(Trans_searchFormBean trans_searchFormBean) {
        
        return (Integer)getSqlSession().selectOne("Trans_search.trans_searchFailDetailListCount",trans_searchFormBean);
        
    }

    //신용카드 실패내역 상세 조회 
    @Override
    public List<Tb_Tran_Cardpg> trans_searchFailDetailList(Trans_searchFormBean trans_searchFormBean) {
        
        return (List)getSqlSession().selectList("Trans_search.trans_searchFailDetailList",trans_searchFormBean);
        
    }    
    
    //현금 집계 조회 카운트
    @Override
    public Integer cashtrans_searchMasterListCount(Trans_searchFormBean trans_searchFormBean) {
        
        return (Integer)getSqlSession().selectOne("Trans_search.cashtrans_searchMasterListCount",trans_searchFormBean);
        
    }    
    
    //현금 집계 조회
    @Override
    public List<Tb_Tran_Cardpg> cashtrans_searchMasterList(Trans_searchFormBean trans_searchFormBean) {
        
        return (List)getSqlSession().selectList("Trans_search.cashtrans_searchMasterList",trans_searchFormBean);
        
    }

    //현금 상세 조회 카운트
    @Override
    public Integer cashtrans_searchDetailListCount(Trans_searchFormBean trans_searchFormBean) {
        
        return (Integer)getSqlSession().selectOne("Trans_search.cashtrans_searchDetailListCount",trans_searchFormBean);
        
    }

    //현금 상세 조회 
    @Override
    public List<Tb_Tran_Cardpg> cashtrans_searchDetailList(Trans_searchFormBean trans_searchFormBean) {
        
        return (List)getSqlSession().selectList("Trans_search.cashtrans_searchDetailList",trans_searchFormBean);
        
    }    
    
    //신용카드 엑셀다운로드 조회
    @Override
    public List trans_searchMasterListExcel(Trans_searchFormBean trans_searchFormBean) {
        
        return (List)getSqlSession().selectList("Trans_search.trans_searchMasterListExcel",trans_searchFormBean);
        
    }
    
    //신용카드 엑셀다운로드 조회
    @Override
    public List trans_searchDetailListExcel(Trans_searchFormBean trans_searchFormBean) {
        
        return (List)getSqlSession().selectList("Trans_search.trans_searchDetailListExcel",trans_searchFormBean);
        
    }    
    
    //현금거래 집계 엑셀다운로드 조회
    @Override
    public List cashTrans_searchMasterListExcel(Trans_searchFormBean trans_searchFormBean) {
        
        return (List)getSqlSession().selectList("Trans_search.cashTrans_searchMasterListExcel",trans_searchFormBean);
        
    }    
    
    //현금거래 엑셀다운로드 조회
    @Override
    public List cashTrans_searchDetailListExcel(Trans_searchFormBean trans_searchFormBean) {
        
        return (List)getSqlSession().selectList("Trans_search.cashTrans_searchDetailListExcel",trans_searchFormBean);
        
    }        
    
    //신용거래 실패내역 엑셀다운로드 조회
    @Override
    public List trans_searchFailDetailListExcel(Trans_searchFormBean trans_searchFormBean) {
        
        return (List)getSqlSession().selectList("Trans_search.trans_searchFailDetailListExcel",trans_searchFormBean);
        
    }        

    //가맹점 신용카드 집계 조회 카운트
    @Override
    public Integer merchTrans_searchMasterListCount(Trans_searchFormBean trans_searchFormBean) {
        
        return (Integer)getSqlSession().selectOne("Trans_search.merchTrans_searchMasterListCount",trans_searchFormBean);
        
    }

    //가맹점 신용카드 집계 조회
    @Override
    public List<Tb_Tran_Cardpg> merchTrans_searchMasterList(Trans_searchFormBean trans_searchFormBean) {
        
        return (List)getSqlSession().selectList("Trans_search.merchTrans_searchMasterList",trans_searchFormBean);
        
    }
    
    //가맹점 신용카드 엑셀다운로드 조회
    @Override
    public List merchTrans_searchMasterListExcel(Trans_searchFormBean trans_searchFormBean) {
        
        return (List)getSqlSession().selectList("Trans_search.merchTrans_searchMasterListExcel",trans_searchFormBean);
        
    }    

    //가맹점 신용카드 상세 내역 조회 카운트
    @Override
    public Integer merchTrans_searchDetailListCount(Trans_searchFormBean trans_searchFormBean) {
        
        return (Integer)getSqlSession().selectOne("Trans_search.merchTrans_searchDetailListCount",trans_searchFormBean);
        
    }

    //가맹점 신용카드 상세 내역 조회 
    @Override
    public List<Tb_Tran_Cardpg> merchTrans_searchDetailList(Trans_searchFormBean trans_searchFormBean) {
        
        return (List)getSqlSession().selectList("Trans_search.merchTrans_searchDetailList",trans_searchFormBean);
        
    }

    //가맹점 신용카드 상세 내역 엑셀다운로드
    @Override
    public List merchTrans_searchDetailListExcel(Trans_searchFormBean trans_searchFormBean) {
        
        return (List)getSqlSession().selectList("Trans_search.merchTrans_searchDetailListExcel",trans_searchFormBean);
        
    }

    //가맹점 신용카드 실패내역 조회 카운트
    @Override
    public Integer merchTrans_searchFailDetailListCount(Trans_searchFormBean trans_searchFormBean) {
        
        return (Integer)getSqlSession().selectOne("Trans_search.merchTrans_searchFailDetailListCount",trans_searchFormBean);
        
    }

    //가맹점 신용카드 실패내역 조회
    @Override
    public List<Tb_Tran_Cardpg> merchTrans_searchFailDetailList(Trans_searchFormBean trans_searchFormBean) {
        
        return (List)getSqlSession().selectList("Trans_search.merchTrans_searchFailDetailList",trans_searchFormBean);
        
    }

    //가맹점 신용카드 실패내역 엑셀다운로드
    @Override
    public List merchTrans_searchFailDetailListExcel(Trans_searchFormBean trans_searchFormBean) {
        
        return (List)getSqlSession().selectList("Trans_search.merchTrans_searchFailDetailListExcel",trans_searchFormBean);
        
    }

    //가맹점 현금거래 집계 조회 카운트
    @Override
    public Integer merchCashtrans_searchMasterListCount(Trans_searchFormBean trans_searchFormBean) {
        
        return (Integer)getSqlSession().selectOne("Trans_search.merchCashtrans_searchMasterListCount",trans_searchFormBean);
        
    }

    //가맹점 현금거래 집계 조회
    @Override
    public List<Tb_Tran_Cardpg> merchCashtrans_searchMasterList(Trans_searchFormBean trans_searchFormBean) {
        
        return (List)getSqlSession().selectList("Trans_search.merchCashtrans_searchMasterList",trans_searchFormBean);
        
    }
    
    //가맹점 현금거래 집계 엑셀다운로드
    @Override
    public List merchCashTrans_searchMasterListExcel(Trans_searchFormBean trans_searchFormBean) {
        
        return (List)getSqlSession().selectList("Trans_search.merchCashTrans_searchMasterListExcel",trans_searchFormBean);
        
    }    
    
    //가맹점 현금거래 조회 카운트
    @Override
    public Integer merchCashtrans_searchDetailListCount(Trans_searchFormBean trans_searchFormBean) {
        
        return (Integer)getSqlSession().selectOne("Trans_search.merchCashtrans_searchDetailListCount",trans_searchFormBean);
        
    }

    //가맹점 현금거래 조회
    @Override
    public List<Tb_Tran_Cardpg> merchCashtrans_searchDetailList(Trans_searchFormBean trans_searchFormBean) {
        
        return (List)getSqlSession().selectList("Trans_search.merchCashtrans_searchDetailList",trans_searchFormBean);
        
    }
    
    //가맹점 현금거래 엑셀다운로드
    @Override
    public List merchCashTrans_searchDetailListExcel(Trans_searchFormBean trans_searchFormBean) {
        
        return (List)getSqlSession().selectList("Trans_search.merchCashTrans_searchDetailListExcel",trans_searchFormBean);
        
    }        

    
    
        //신용카드 이상거래집계 조회 카운트
    @Override
    public Integer trans_searchChkMasterListCount(Trans_searchFormBean trans_searchFormBean) {
        
        return (Integer)getSqlSession().selectOne("Trans_search.trans_searchChkMasterListCount",trans_searchFormBean);
        
    }    
    
    //신용카드 이상거래집계 조회
    @Override
    public List<Tb_Tran_Cardpg> trans_searchChkMasterList(Trans_searchFormBean trans_searchFormBean) {
        
        return (List)getSqlSession().selectList("Trans_search.trans_searchChkMasterList",trans_searchFormBean);
        
    }
}
