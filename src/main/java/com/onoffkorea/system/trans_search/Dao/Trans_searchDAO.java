/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.trans_search.Dao;

import com.onoffkorea.system.trans_search.Bean.Trans_searchFormBean;
import com.onoffkorea.system.trans_search.Vo.Tb_Tran_Cardpg;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface Trans_searchDAO {

    public Integer trans_searchMasterListCount(Trans_searchFormBean trans_searchFormBean);
    
    public List<Tb_Tran_Cardpg> trans_searchMasterList(Trans_searchFormBean trans_searchFormBean);

    public Integer trans_searchDetailListCount(Trans_searchFormBean trans_searchFormBean);

    public List<Tb_Tran_Cardpg> trans_searchDetailList(Trans_searchFormBean trans_searchFormBean);

    public Integer trans_searchFailMasterListCount(Trans_searchFormBean trans_searchFormBean);

    public List<Tb_Tran_Cardpg> trans_searchFailMasterList(Trans_searchFormBean trans_searchFormBean);

    public Integer trans_searchFailDetailListCount(Trans_searchFormBean trans_searchFormBean);

    public List<Tb_Tran_Cardpg> trans_searchFailDetailList(Trans_searchFormBean trans_searchFormBean);

    public Integer cashtrans_searchMasterListCount(Trans_searchFormBean trans_searchFormBean);

    public List<Tb_Tran_Cardpg> cashtrans_searchMasterList(Trans_searchFormBean trans_searchFormBean);

    public Integer cashtrans_searchDetailListCount(Trans_searchFormBean trans_searchFormBean);

    public List<Tb_Tran_Cardpg> cashtrans_searchDetailList(Trans_searchFormBean trans_searchFormBean);

    public List trans_searchMasterListExcel(Trans_searchFormBean trans_searchFormBean);

    public List trans_searchDetailListExcel(Trans_searchFormBean trans_searchFormBean);

    public List cashTrans_searchMasterListExcel(Trans_searchFormBean trans_searchFormBean);

    public List cashTrans_searchDetailListExcel(Trans_searchFormBean trans_searchFormBean);

    public List trans_searchFailDetailListExcel(Trans_searchFormBean trans_searchFormBean);

    public Integer merchTrans_searchMasterListCount(Trans_searchFormBean trans_searchFormBean);

    public List<Tb_Tran_Cardpg> merchTrans_searchMasterList(Trans_searchFormBean trans_searchFormBean);

    public List merchTrans_searchMasterListExcel(Trans_searchFormBean trans_searchFormBean);

    public Integer merchTrans_searchDetailListCount(Trans_searchFormBean trans_searchFormBean);

    public List<Tb_Tran_Cardpg> merchTrans_searchDetailList(Trans_searchFormBean trans_searchFormBean);

    public List merchTrans_searchDetailListExcel(Trans_searchFormBean trans_searchFormBean);

    public Integer merchTrans_searchFailDetailListCount(Trans_searchFormBean trans_searchFormBean);

    public List<Tb_Tran_Cardpg> merchTrans_searchFailDetailList(Trans_searchFormBean trans_searchFormBean);

    public List merchTrans_searchFailDetailListExcel(Trans_searchFormBean trans_searchFormBean);

    public Integer merchCashtrans_searchMasterListCount(Trans_searchFormBean trans_searchFormBean);

    public List<Tb_Tran_Cardpg> merchCashtrans_searchMasterList(Trans_searchFormBean trans_searchFormBean);

    public List merchCashTrans_searchMasterListExcel(Trans_searchFormBean trans_searchFormBean);

    public Integer merchCashtrans_searchDetailListCount(Trans_searchFormBean trans_searchFormBean);

    public List<Tb_Tran_Cardpg> merchCashtrans_searchDetailList(Trans_searchFormBean trans_searchFormBean);

    public List merchCashTrans_searchDetailListExcel(Trans_searchFormBean trans_searchFormBean);

    
    public Integer trans_searchChkMasterListCount(Trans_searchFormBean trans_searchFormBean);
    
    public List<Tb_Tran_Cardpg> trans_searchChkMasterList(Trans_searchFormBean trans_searchFormBean);
}
