/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.trans_search.Controller;

import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import com.onoffkorea.system.trans_search.Bean.Trans_searchFormBean;
import com.onoffkorea.system.trans_search.Service.Trans_searchService;
import common.web.servlet.BaseSpringMultiActionController;
import java.util.Hashtable;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/trans_search/**")
public class Trans_searchController extends BaseSpringMultiActionController{
        
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private CommonService commonService;
    
    @Autowired
    private Trans_searchService trans_searchService;
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
        
        
    }    

    //신용거래 레이아웃
    @RequestMapping(method= RequestMethod.GET,value = "/trans_searchLayout")
    public String trans_searchLayout(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        return null;
        
    }
    
    //신용거래내역 집계 조회
    @RequestMapping(method= RequestMethod.GET,value = "/trans_searchMasterList")
    public String trans_searchMasterFormList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        Hashtable ht_trans_searchMasterList = trans_searchService.trans_searchMasterList(trans_searchFormBean);
        String ls_trans_searchMasterList = (String) ht_trans_searchMasterList.get("ResultSet");
        
        model.addAttribute("ls_trans_searchMasterList",ls_trans_searchMasterList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_trans_searchMasterList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);              
        
        return null;
        
    }
    
    //신용거래내역 집계 조회
    @RequestMapping(method= RequestMethod.POST,value = "/trans_searchMasterList")
    @ResponseBody
    public String trans_searchMasterList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        Hashtable ht_trans_searchMasterList = trans_searchService.trans_searchMasterList(trans_searchFormBean);
        String ls_trans_searchMasterList = (String) ht_trans_searchMasterList.get("ResultSet");

        return ls_trans_searchMasterList;
        
    }    
    
    //신용거래 집계 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/trans_searchMasterListExcel")
    public void trans_searchMasterListExcel(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("trans_search.trans_searchMasterListExcel.fileName"));
        
        trans_searchService.trans_searchMasterListExcel(trans_searchFormBean, excelDownloadFilename, response);
        
    }       
    
    //신용거래내역 상세 조회
    @RequestMapping(method= RequestMethod.GET,value = "/trans_searchDetailList")
    public String trans_searchDetailFormList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {   
        
        trans_searchFormBean.setSes_user_cate(siteSession.getSes_user_cate());   
        logger.trace("trans_searchDetailFormList siteSession.getSes_user_cate() : "+ siteSession.getSes_user_cate());
        
        Hashtable ht_trans_searchDetailList = trans_searchService.trans_searchDetailList(trans_searchFormBean);
        String ls_trans_searchDetailList = (String) ht_trans_searchDetailList.get("ResultSet");
        
        model.addAttribute("ls_trans_searchDetailList",ls_trans_searchDetailList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_trans_searchDetailList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);           
        
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_trans_searchDetailList.get("resultStatusList");
        
        model.addAttribute("resultStatusList",resultStatusList);                   
        
        Tb_Code_Main tranStatusList = (Tb_Code_Main) ht_trans_searchDetailList.get("tranStatusList");
        
        model.addAttribute("tranStatusList",tranStatusList);             
        
        Tb_Code_Main issCdList = (Tb_Code_Main) ht_trans_searchDetailList.get("issCdList");
        
        model.addAttribute("issCdList",issCdList);        
        
        Tb_Code_Main massagetypeCdList = (Tb_Code_Main) ht_trans_searchDetailList.get("massagetypeCdList");
        
        model.addAttribute("massagetypeCdList",massagetypeCdList);            
        
        return null;
        
    }
    
    //신용거래내역 상세 조회
    @RequestMapping(method= RequestMethod.POST,value = "/trans_searchDetailList")
    @ResponseBody
    public String trans_searchDetailList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        trans_searchFormBean.setSes_user_cate(siteSession.getSes_user_cate());   
        
        Hashtable ht_trans_searchDetailList = trans_searchService.trans_searchDetailList(trans_searchFormBean);
        String ls_trans_searchDetailList = (String) ht_trans_searchDetailList.get("ResultSet");

        return ls_trans_searchDetailList;
        
    }   
    
    //신용거래 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/trans_searchDetailListExcel")
    public void trans_searchDetailListExcel(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("trans_search.trans_searchDetailListExcel.fileName"));
        
        trans_searchService.trans_searchDetailListExcel(trans_searchFormBean, excelDownloadFilename, response);
        
    }        
    
    //신용거래 실패내역 레이아웃
    @RequestMapping(method= RequestMethod.GET,value = "/trans_searchFailLayout")
    public String trans_searchFailLayout(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        return null;
        
    }
    
    //신용거래 실패내역 집계 조회
    @RequestMapping(method= RequestMethod.GET,value = "/trans_searchFailMasterList")
    public String trans_searchFailMasterFormList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        Hashtable ht_trans_searchFailMasterList = trans_searchService.trans_searchFailMasterList(trans_searchFormBean);
        String ls_trans_searchFailMasterList = (String) ht_trans_searchFailMasterList.get("ResultSet");
        model.addAttribute("ls_trans_searchFailMasterList",ls_trans_searchFailMasterList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_trans_searchFailMasterList.get("payChnCateList");
        model.addAttribute("payChnCateList",payChnCateList);              
        
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_trans_searchFailMasterList.get("resultStatusList");
        model.addAttribute("resultStatusList",resultStatusList);                   
        
        Tb_Code_Main tranStatusList = (Tb_Code_Main) ht_trans_searchFailMasterList.get("tranStatusList");
        model.addAttribute("tranStatusList",tranStatusList);             
        
        Tb_Code_Main issCdList = (Tb_Code_Main) ht_trans_searchFailMasterList.get("issCdList");
        model.addAttribute("issCdList",issCdList);                  
        
        return null;
        
    }
    
    //신용거래 실패내역 집계 조회
    @RequestMapping(method= RequestMethod.POST,value = "/trans_searchFailMasterList")
    @ResponseBody
    public String trans_searchFailMasterList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {  
        
        Hashtable ht_trans_searchFailMasterList = trans_searchService.trans_searchFailMasterList(trans_searchFormBean);
        String ls_trans_searchFailMasterList = (String) ht_trans_searchFailMasterList.get("ResultSet");

        return ls_trans_searchFailMasterList;
        
    }    
    
    //신용거래 실패내역 상세 조회
    @RequestMapping(method= RequestMethod.GET,value = "/trans_searchFailDetailList")
    public String trans_searchFailDetailFormList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        Hashtable ht_trans_searchFailDetailList = trans_searchService.trans_searchFailDetailList(trans_searchFormBean);
        String ls_trans_searchFailDetailList = (String) ht_trans_searchFailDetailList.get("ResultSet");
        model.addAttribute("ls_trans_searchFailDetailList",ls_trans_searchFailDetailList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_trans_searchFailDetailList.get("payChnCateList");
        model.addAttribute("payChnCateList",payChnCateList);           
        
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_trans_searchFailDetailList.get("resultStatusList");
        model.addAttribute("resultStatusList",resultStatusList);                   
        
        return null;
        
    }
    
    //신용거래 실패내역 상세 조회
    @RequestMapping(method= RequestMethod.POST,value = "/trans_searchFailDetailList")
    @ResponseBody
    public String trans_searchFailDetailList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        Hashtable ht_trans_searchFailDetailList = trans_searchService.trans_searchFailDetailList(trans_searchFormBean);
        String ls_trans_searchFailDetailList = (String) ht_trans_searchFailDetailList.get("ResultSet");

        return ls_trans_searchFailDetailList;
        
    }        
    
    //신용거래 실패내역 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/trans_searchFailDetailListExcel")
    public void trans_searchFailDetailListExcel(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("trans_search.trans_searchFailDetailListExcel.fileName"));   
        
        trans_searchService.trans_searchFailDetailListExcel(trans_searchFormBean, excelDownloadFilename, response); 
        
    }        
    
    //현금거래 레이아웃
    @RequestMapping(method= RequestMethod.GET,value = "/cashtrans_searchLayout")
    public String cashtrans_searchLayout(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        return null;
        
    }
    
    //현금거래내역 집계 조회
    @RequestMapping(method= RequestMethod.GET,value = "/cashtrans_searchMasterList")
    public String cashtrans_searchMasterFormList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        Hashtable ht_cashtrans_searchMasterList = trans_searchService.cashtrans_searchMasterList(trans_searchFormBean);
        String ls_cashtrans_searchMasterList = (String) ht_cashtrans_searchMasterList.get("ResultSet");
        
        model.addAttribute("ls_cashtrans_searchMasterList",ls_cashtrans_searchMasterList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_cashtrans_searchMasterList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);              
        
        return null;
        
    }
    
    //현금거래내역 집계 조회
    @RequestMapping(method= RequestMethod.POST,value = "/cashtrans_searchMasterList")
    @ResponseBody
    public String cashtrans_searchMasterList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        Hashtable ht_cashtrans_searchMasterList = trans_searchService.cashtrans_searchMasterList(trans_searchFormBean);
        String ls_cashtrans_searchMasterList = (String) ht_cashtrans_searchMasterList.get("ResultSet");

        return ls_cashtrans_searchMasterList;
        
    }   
    
    //현금거래 집계 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/cashTrans_searchMasterListExcel")
    public void cashTrans_searchMasterListExcel(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("trans_search.cashTrans_searchMasterListExcel.fileName"));   
        
        trans_searchService.cashTrans_searchMasterListExcel(trans_searchFormBean, excelDownloadFilename, response);
        
    }       
    
    //현금거래내역 상세 조회
    @RequestMapping(method= RequestMethod.GET,value = "/cashtrans_searchDetailList")
    public String cashtrans_searchDetailFormList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception { 
        
        Hashtable ht_cashtrans_searchDetailList = trans_searchService.cashtrans_searchDetailList(trans_searchFormBean);
        String ls_cashtrans_searchDetailList = (String) ht_cashtrans_searchDetailList.get("ResultSet");
        
        model.addAttribute("ls_cashtrans_searchDetailList",ls_cashtrans_searchDetailList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_cashtrans_searchDetailList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);           
        
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_cashtrans_searchDetailList.get("resultStatusList");
        
        model.addAttribute("resultStatusList",resultStatusList);  
        
        Tb_Code_Main tranStatusList = (Tb_Code_Main) ht_cashtrans_searchDetailList.get("tranStatusList");
        model.addAttribute("tranStatusList",tranStatusList);           
        
        
        
        return null;
        
    }
    
    //현금거래내역 상세 조회
    @RequestMapping(method= RequestMethod.POST,value = "/cashtrans_searchDetailList")
    @ResponseBody
    public String cashtrans_searchDetailList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {     
         
        Hashtable ht_cashtrans_searchDetailList = trans_searchService.cashtrans_searchDetailList(trans_searchFormBean);
        String ls_cashtrans_searchDetailList = (String) ht_cashtrans_searchDetailList.get("ResultSet");

        return ls_cashtrans_searchDetailList;
        
    }            
    
    //현금거래 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/cashTrans_searchDetailListExcel")
    public void cashTrans_searchDetailListExcel(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("trans_search.cashTrans_searchDetailListExcel.fileName")); 
        
        trans_searchService.cashTrans_searchDetailListExcel(trans_searchFormBean, excelDownloadFilename, response); 
        
    }               
    
    //가맹점 신용거래 레이아웃
    @RequestMapping(method= RequestMethod.GET,value = "/merchTrans_searchLayout")
    public String merchTrans_searchLayout(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        return null;
        
    }    
    
    //가맹점 신용거래내역 집계 조회
    @RequestMapping(method= RequestMethod.GET,value = "/merchTrans_searchMasterList")
    public String merchTrans_searchMasterFormList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        trans_searchFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        Hashtable ht_trans_searchMasterList = trans_searchService.merchTrans_searchMasterList(trans_searchFormBean);
        String ls_trans_searchMasterList = (String) ht_trans_searchMasterList.get("ResultSet");
        
        model.addAttribute("ls_trans_searchMasterList",ls_trans_searchMasterList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_trans_searchMasterList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);              
        
        return null;
        
    }    
    
    //가맹점 신용거래내역 집계 조회
    @RequestMapping(method= RequestMethod.POST,value = "/merchTrans_searchMasterList")
    @ResponseBody
    public String merchTrans_searchMasterList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        trans_searchFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        Hashtable ht_trans_searchMasterList = trans_searchService.merchTrans_searchMasterList(trans_searchFormBean);
        String ls_trans_searchMasterList = (String) ht_trans_searchMasterList.get("ResultSet");

        return ls_trans_searchMasterList;
        
    }        
    
    //가맹점 신용거래 집계 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/merchTrans_searchMasterListExcel")
    public void merchTrans_searchMasterListExcel(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("trans_search.trans_searchMasterListExcel.fileName"));
        
        trans_searchFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        trans_searchService.merchTrans_searchMasterListExcel(trans_searchFormBean, excelDownloadFilename, response);
        
    }            
    
    //가맹점 신용거래내역 상세 조회
    @RequestMapping(method= RequestMethod.GET,value = "/merchTrans_searchDetailList")
    public String merchTrans_searchDetailFormList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        trans_searchFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
            trans_searchFormBean.setSes_merch_cncl_auth(siteSession.getSes_merch_cncl_auth());
        }else if("02".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onfftid(siteSession.getSes_onfftid());
            trans_searchFormBean.setSes_tid_cncl_auth(siteSession.getSes_tid_cncl_auth());
        }else if("03".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        Hashtable ht_trans_searchDetailList = trans_searchService.merchTrans_searchDetailList(trans_searchFormBean);
        String ls_trans_searchDetailList = (String) ht_trans_searchDetailList.get("ResultSet");
        
        model.addAttribute("ls_trans_searchDetailList",ls_trans_searchDetailList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_trans_searchDetailList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);           
        
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_trans_searchDetailList.get("resultStatusList");
        
        model.addAttribute("resultStatusList",resultStatusList);             
        
        Tb_Code_Main massagetypeCdList = (Tb_Code_Main) ht_trans_searchDetailList.get("massagetypeCdList");
        
        model.addAttribute("massagetypeCdList",massagetypeCdList);       
        
        return null;
        
    }    
    
    //가맹점 신용거래내역 상세 조회
    @RequestMapping(method= RequestMethod.POST,value = "/merchTrans_searchDetailList")
    @ResponseBody
    public String merchanttrans_searchDetailList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        trans_searchFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
             trans_searchFormBean.setSes_merch_cncl_auth(siteSession.getSes_merch_cncl_auth());
        }else if("02".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onfftid(siteSession.getSes_onfftid());
             trans_searchFormBean.setSes_tid_cncl_auth(siteSession.getSes_tid_cncl_auth());
        }else if("03".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        Hashtable ht_trans_searchDetailList = trans_searchService.merchTrans_searchDetailList(trans_searchFormBean);
        String ls_trans_searchDetailList = (String) ht_trans_searchDetailList.get("ResultSet");

        return ls_trans_searchDetailList;
        
    }             
    
    //가맹점 신용거래 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/merchTrans_searchDetailListExcel")
    public void merchTrans_searchDetailListExcel(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("trans_search.trans_searchDetailListExcel.fileName"));
        
        trans_searchFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        trans_searchService.merchTrans_searchDetailListExcel(trans_searchFormBean, excelDownloadFilename, response);
        
    }           
    
    //가맹점 신용거래 실패내역 레이아웃
    @RequestMapping(method= RequestMethod.GET,value = "/merchTrans_searchFailLayout")
    public String merchTrans_searchFailLayout(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        return null;
        
    }
    
    //가맹점 신용거래 실패내역 상세 조회
    @RequestMapping(method= RequestMethod.GET,value = "/merchTrans_searchFailDetailList")
    public String merchTrans_searchFailDetailFormList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
 
        trans_searchFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        Hashtable ht_trans_searchFailDetailList = trans_searchService.merchTrans_searchFailDetailList(trans_searchFormBean);
        String ls_trans_searchFailDetailList = (String) ht_trans_searchFailDetailList.get("ResultSet");
        
        model.addAttribute("ls_trans_searchFailDetailList",ls_trans_searchFailDetailList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_trans_searchFailDetailList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);           
        
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_trans_searchFailDetailList.get("resultStatusList");
        
        model.addAttribute("resultStatusList",resultStatusList);                   
        
        
        
        return null;
        
    }
    
    //가맹점 신용거래 실패내역 상세 조회
    @RequestMapping(method= RequestMethod.POST,value = "/merchTrans_searchFailDetailList")
    @ResponseBody
    public String merchTrans_searchFailDetailList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        trans_searchFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        Hashtable ht_trans_searchFailDetailList = trans_searchService.merchTrans_searchFailDetailList(trans_searchFormBean);
        String ls_trans_searchFailDetailList = (String) ht_trans_searchFailDetailList.get("ResultSet");

        return ls_trans_searchFailDetailList;
        
    }        
    
    //가맹점 신용거래 실패내역 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/merchTrans_searchFailDetailListExcel")
    public void merchTrans_searchFailDetailListExcel(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("trans_search.trans_searchFailDetailListExcel.fileName"));
        
        trans_searchFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        trans_searchService.merchTrans_searchFailDetailListExcel(trans_searchFormBean, excelDownloadFilename, response); 
        
    }        
    
    //가맹점 현금거래 레이아웃
    @RequestMapping(method= RequestMethod.GET,value = "/merchCashtrans_searchLayout")
    public String merchCashtrans_searchLayout(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        return null;
        
    }
    
    //가맹점 현금거래내역 집계 조회
    @RequestMapping(method= RequestMethod.GET,value = "/merchCashtrans_searchMasterList")
    public String merchCashtrans_searchMasterFormList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        trans_searchFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }             
        
        Hashtable ht_cashtrans_searchMasterList = trans_searchService.merchCashtrans_searchMasterList(trans_searchFormBean);
        String ls_cashtrans_searchMasterList = (String) ht_cashtrans_searchMasterList.get("ResultSet");
        
        model.addAttribute("ls_cashtrans_searchMasterList",ls_cashtrans_searchMasterList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_cashtrans_searchMasterList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);              
        
        return null;
        
    }
    
    //가맹점 현금거래내역 집계 조회
    @RequestMapping(method= RequestMethod.POST,value = "/merchCashtrans_searchMasterList")
    @ResponseBody
    public String merchCashtrans_searchMasterList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        trans_searchFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        Hashtable ht_cashtrans_searchMasterList = trans_searchService.merchCashtrans_searchMasterList(trans_searchFormBean);
        String ls_cashtrans_searchMasterList = (String) ht_cashtrans_searchMasterList.get("ResultSet");

        return ls_cashtrans_searchMasterList;
        
    }   
    
    //가맹점 현금거래 집계 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/merchCashTrans_searchMasterListExcel")
    public void merchCashTrans_searchMasterListExcel(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("trans_search.cashTrans_searchMasterListExcel.fileName"));
        
        trans_searchFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        trans_searchService.merchCashTrans_searchMasterListExcel(trans_searchFormBean, excelDownloadFilename, response);
        
    }       
    
    //가맹점 현금거래내역 상세 조회
    @RequestMapping(method= RequestMethod.GET,value = "/merchCashtrans_searchDetailList")
    public String merchCashtrans_searchDetailFormList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        trans_searchFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        Hashtable ht_cashtrans_searchDetailList = trans_searchService.merchCashtrans_searchDetailList(trans_searchFormBean);
        String ls_cashtrans_searchDetailList = (String) ht_cashtrans_searchDetailList.get("ResultSet");
        
        model.addAttribute("ls_cashtrans_searchDetailList",ls_cashtrans_searchDetailList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_cashtrans_searchDetailList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);           
        
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_cashtrans_searchDetailList.get("resultStatusList");
        
        model.addAttribute("resultStatusList",resultStatusList);                   
        
        return null;
        
    }
    
    //현금거래내역 상세 조회
    @RequestMapping(method= RequestMethod.POST,value = "/merchCashtrans_searchDetailList")
    @ResponseBody
    public String merchCashtrans_searchDetailList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        trans_searchFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
            trans_searchFormBean.setSes_merch_cncl_auth(siteSession.getSes_merch_cncl_auth());
        }else if("02".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onfftid(siteSession.getSes_onfftid());
            trans_searchFormBean.setSes_tid_cncl_auth(siteSession.getSes_tid_cncl_auth());
        }else if("03".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
         
        Hashtable ht_cashtrans_searchDetailList = trans_searchService.merchCashtrans_searchDetailList(trans_searchFormBean);
        String ls_cashtrans_searchDetailList = (String) ht_cashtrans_searchDetailList.get("ResultSet");

        return ls_cashtrans_searchDetailList;
        
    }            
    
    //현금거래 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/merchCashTrans_searchDetailListExcel")
    public void merchCashTrans_searchDetailListExcel(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("trans_search.cashTrans_searchDetailListExcel.fileName"));
        
        trans_searchFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            trans_searchFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }        
        
        trans_searchService.merchCashTrans_searchDetailListExcel(trans_searchFormBean, excelDownloadFilename, response); 
        
    }         

    
    //신용이상거래 레이아웃
    @RequestMapping(method= RequestMethod.GET,value = "/trans_searchChkLayout")
    public String trans_searchChkLayout(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        return null;        
    }
    
    //신용이상거래 집계 조회
    @RequestMapping(method= RequestMethod.GET,value = "/trans_searchChkMasterList")
    public String trans_searchChkMasterFormList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        Hashtable ht_trans_searchMasterList = trans_searchService.trans_searchChkMasterList(trans_searchFormBean);
        String ls_trans_searchMasterList = (String) ht_trans_searchMasterList.get("ResultSet");
        
        model.addAttribute("ls_trans_searchMasterList",ls_trans_searchMasterList);     
        
        return null;
        
    }
    
    //이상거래 집계 조회
    @RequestMapping(method= RequestMethod.POST,value = "/trans_searchChkMasterList")
    @ResponseBody
    public String trans_searchChkMasterList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {
        
        Hashtable ht_trans_searchMasterList = trans_searchService.trans_searchChkMasterList(trans_searchFormBean);
        String ls_trans_searchMasterList = (String) ht_trans_searchMasterList.get("ResultSet");

        return ls_trans_searchMasterList;
        
    }    
    
  
    //신용거래내역 상세 조회
    @RequestMapping(method= RequestMethod.GET,value = "/trans_searchChkDetailList")
    public String trans_searchChkDetailFormList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {   
        
        trans_searchFormBean.setSes_user_cate(siteSession.getSes_user_cate());   
        logger.trace("trans_searchDetailFormList siteSession.getSes_user_cate() : "+ siteSession.getSes_user_cate());
        
        Hashtable ht_trans_searchDetailList = trans_searchService.trans_searchDetailList(trans_searchFormBean);
        String ls_trans_searchDetailList = (String) ht_trans_searchDetailList.get("ResultSet");
        
        model.addAttribute("ls_trans_searchDetailList",ls_trans_searchDetailList);     
        
        Tb_Code_Main payChnCateList = (Tb_Code_Main) ht_trans_searchDetailList.get("payChnCateList");
        
        model.addAttribute("payChnCateList",payChnCateList);           
        
        Tb_Code_Main resultStatusList = (Tb_Code_Main) ht_trans_searchDetailList.get("resultStatusList");
        
        model.addAttribute("resultStatusList",resultStatusList);                   
        
        Tb_Code_Main tranStatusList = (Tb_Code_Main) ht_trans_searchDetailList.get("tranStatusList");
        
        model.addAttribute("tranStatusList",tranStatusList);             
        
        Tb_Code_Main issCdList = (Tb_Code_Main) ht_trans_searchDetailList.get("issCdList");
        
        model.addAttribute("issCdList",issCdList);    
        
        Tb_Code_Main massagetypeCdList = (Tb_Code_Main) ht_trans_searchDetailList.get("massagetypeCdList");
        
        model.addAttribute("massagetypeCdList", massagetypeCdList);
        
        return null;
        
    }
    
    //신용거래내역 상세 조회
    @RequestMapping(method= RequestMethod.POST,value = "/trans_searchChkDetailList")
    @ResponseBody
    public String trans_searchChkDetailList(@Valid Trans_searchFormBean trans_searchFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        trans_searchFormBean.setSes_user_cate(siteSession.getSes_user_cate());   
        
        Hashtable ht_trans_searchDetailList = trans_searchService.trans_searchDetailList(trans_searchFormBean);
        String ls_trans_searchDetailList = (String) ht_trans_searchDetailList.get("ResultSet");

        return ls_trans_searchDetailList;
        
    }       
}
