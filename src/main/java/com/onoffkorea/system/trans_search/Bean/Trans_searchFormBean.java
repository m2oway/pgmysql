/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.trans_search.Bean;

import com.onoffkorea.system.common.util.CommonSortableListPagingForm;

/**
 *
 * @author Administrator
 */
public class Trans_searchFormBean  extends CommonSortableListPagingForm{
    
    private String tran_seq            ;
    private String massagetype         ;
    private String card_num            ;
    private String app_dt              ;
    private String app_tm              ;
    private String app_no              ;
    private String tot_amt             ;
    private String pg_seq              ;
    private String pay_chn_cate        ;
    private String tid                 ;
    private String onofftid            ;
    private String wcc                 ;
    private String expire_dt           ;
    private String card_cate           ;
    private String order_seq           ;
    private String tran_dt             ;
    private String tran_tm             ;
    private String tran_cate           ;
    private String free_inst_flag      ;
    private String tax_amt             ;
    private String svc_amt             ;
    private String currency            ;
    private String installment         ;
    private String iss_cd              ;
    private String iss_nm              ;
    private String app_iss_cd          ;
    private String app_iss_nm          ;
    private String acq_cd              ;
    private String acq_nm              ;
    private String app_acq_cd          ;
    private String app_acq_nm          ;
    private String merch_no            ;
    private String result_cd           ;
    private String result_msg          ;
    private String org_app_dd          ;
    private String org_app_no          ;
    private String cncl_reason         ;
    private String tran_status         ;
    private String result_status       ;
    private String tran_step           ;
    private String cncl_dt             ;
    private String acq_dt              ;
    private String acq_result_cd       ;
    private String holdoff_dt          ;
    private String holdon_dt           ;
    private String pay_dt              ;
    private String pay_amt             ;
    private String commision           ;
    private String onofftid_pay_amt    ;
    private String onofftid_commision  ;
    private String client_ip           ;
    private String user_type           ;
    private String memb_user_no        ;
    private String user_id             ;
    private String user_nm             ;
    private String user_mail           ;
    private String user_phone1         ;
    private String user_phone2         ;
    private String user_addr           ;
    private String product_type        ;
    private String product_nm          ;
    private String filler              ;
    private String filler2             ;
    private String term_filler1        ;
    private String term_filler2        ;
    private String term_filler3        ;
    private String term_filler4        ;
    private String term_filler5        ;
    private String term_filler6        ;
    private String term_filler7        ;
    private String term_filler8        ;
    private String term_filler9        ;
    private String term_filler10       ;
    private String trad_chk_flag       ;
    private String acc_chk_flag        ;
    private String ins_dt              ;
    private String mod_dt              ;
    private String ins_user            ;
    private String mod_user			;
    private String user_seq;
    private String rnum;    
    private String app_start_dt;
    private String app_end_dt;
    private String onfftid;
    private String[] merch_nos;
    private String org_pg_seq;
    private String onffmerch_no;
    private String complain_seq;
    private String merch_nm;
    private String onfftid_nm;
    private String app_dt_start;
    private String app_dt_end;
    private String pay_start_dt;
    private String pay_end_dt;
    private String adj_start_dt;
    private String adj_end_dt;
    
    private String chktran_cnt;

    public String getChktran_cnt() {
        return chktran_cnt;
    }

    public void setChktran_cnt(String chktran_cnt) {
        this.chktran_cnt = chktran_cnt;
    }
    
    

    public String getAdj_start_dt() {
        return adj_start_dt;
    }

    public void setAdj_start_dt(String adj_start_dt) {
        this.adj_start_dt = adj_start_dt;
    }

    public String getAdj_end_dt() {
        return adj_end_dt;
    }

    public void setAdj_end_dt(String adj_end_dt) {
        this.adj_end_dt = adj_end_dt;
    }

    public String getPay_start_dt() {
        return pay_start_dt;
    }

    public void setPay_start_dt(String pay_start_dt) {
        this.pay_start_dt = pay_start_dt;
    }

    public String getPay_end_dt() {
        return pay_end_dt;
    }

    public void setPay_end_dt(String pay_end_dt) {
        this.pay_end_dt = pay_end_dt;
    }

    public String getApp_dt_start() {
        return app_dt_start;
    }

    public void setApp_dt_start(String app_dt_start) {
        this.app_dt_start = app_dt_start;
    }

    public String getApp_dt_end() {
        return app_dt_end;
    }

    public void setApp_dt_end(String app_dt_end) {
        this.app_dt_end = app_dt_end;
    }

    public String getOnfftid_nm() {
        return onfftid_nm;
    }

    public void setOnfftid_nm(String onfftid_nm) {
        this.onfftid_nm = onfftid_nm;
    }

    public String getMerch_nm() {
        return merch_nm;
    }

    public void setMerch_nm(String merch_nm) {
        this.merch_nm = merch_nm;
    }

    public String getComplain_seq() {
        return complain_seq;
    }

    public void setComplain_seq(String complain_seq) {
        this.complain_seq = complain_seq;
    }

    public String getOnffmerch_no() {
        return onffmerch_no;
    }

    public void setOnffmerch_no(String onffmerch_no) {
        this.onffmerch_no = onffmerch_no;
    }

    public String getOrg_pg_seq() {
        return org_pg_seq;
    }

    public void setOrg_pg_seq(String org_pg_seq) {
        this.org_pg_seq = org_pg_seq;
    }

    public String[] getMerch_nos() {
        return merch_nos;
    }

    public void setMerch_nos(String[] merch_nos) {
        this.merch_nos = merch_nos;
    }

    public String getOnfftid() {
        return onfftid;
    }

    public void setOnfftid(String onfftid) {
        this.onfftid = onfftid;
    }

    public String getApp_start_dt() {
        return app_start_dt;
    }

    public void setApp_start_dt(String app_start_dt) {
        this.app_start_dt = app_start_dt;
    }

    public String getApp_end_dt() {
        return app_end_dt;
    }

    public void setApp_end_dt(String app_end_dt) {
        this.app_end_dt = app_end_dt;
    }

    public String getTran_seq() {
        return tran_seq;
    }

    public void setTran_seq(String tran_seq) {
        this.tran_seq = tran_seq;
    }

    public String getMassagetype() {
        return massagetype;
    }

    public void setMassagetype(String massagetype) {
        this.massagetype = massagetype;
    }

    public String getCard_num() {
        return card_num;
    }

    public void setCard_num(String card_num) {
        this.card_num = card_num;
    }

    public String getApp_dt() {
        return app_dt;
    }

    public void setApp_dt(String app_dt) {
        this.app_dt = app_dt;
    }

    public String getApp_tm() {
        return app_tm;
    }

    public void setApp_tm(String app_tm) {
        this.app_tm = app_tm;
    }

    public String getApp_no() {
        return app_no;
    }

    public void setApp_no(String app_no) {
        this.app_no = app_no;
    }

    public String getTot_amt() {
        return tot_amt;
    }

    public void setTot_amt(String tot_amt) {
        this.tot_amt = tot_amt;
    }

    public String getPg_seq() {
        return pg_seq;
    }

    public void setPg_seq(String pg_seq) {
        this.pg_seq = pg_seq;
    }

    public String getPay_chn_cate() {
        return pay_chn_cate;
    }

    public void setPay_chn_cate(String pay_chn_cate) {
        this.pay_chn_cate = pay_chn_cate;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getOnofftid() {
        return onofftid;
    }

    public void setOnofftid(String onofftid) {
        this.onofftid = onofftid;
    }

    public String getWcc() {
        return wcc;
    }

    public void setWcc(String wcc) {
        this.wcc = wcc;
    }

    public String getExpire_dt() {
        return expire_dt;
    }

    public void setExpire_dt(String expire_dt) {
        this.expire_dt = expire_dt;
    }

    public String getCard_cate() {
        return card_cate;
    }

    public void setCard_cate(String card_cate) {
        this.card_cate = card_cate;
    }

    public String getOrder_seq() {
        return order_seq;
    }

    public void setOrder_seq(String order_seq) {
        this.order_seq = order_seq;
    }

    public String getTran_dt() {
        return tran_dt;
    }

    public void setTran_dt(String tran_dt) {
        this.tran_dt = tran_dt;
    }

    public String getTran_tm() {
        return tran_tm;
    }

    public void setTran_tm(String tran_tm) {
        this.tran_tm = tran_tm;
    }

    public String getTran_cate() {
        return tran_cate;
    }

    public void setTran_cate(String tran_cate) {
        this.tran_cate = tran_cate;
    }

    public String getFree_inst_flag() {
        return free_inst_flag;
    }

    public void setFree_inst_flag(String free_inst_flag) {
        this.free_inst_flag = free_inst_flag;
    }

    public String getTax_amt() {
        return tax_amt;
    }

    public void setTax_amt(String tax_amt) {
        this.tax_amt = tax_amt;
    }

    public String getSvc_amt() {
        return svc_amt;
    }

    public void setSvc_amt(String svc_amt) {
        this.svc_amt = svc_amt;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getInstallment() {
        return installment;
    }

    public void setInstallment(String installment) {
        this.installment = installment;
    }

    public String getIss_cd() {
        return iss_cd;
    }

    public void setIss_cd(String iss_cd) {
        this.iss_cd = iss_cd;
    }

    public String getIss_nm() {
        return iss_nm;
    }

    public void setIss_nm(String iss_nm) {
        this.iss_nm = iss_nm;
    }

    public String getApp_iss_cd() {
        return app_iss_cd;
    }

    public void setApp_iss_cd(String app_iss_cd) {
        this.app_iss_cd = app_iss_cd;
    }

    public String getApp_iss_nm() {
        return app_iss_nm;
    }

    public void setApp_iss_nm(String app_iss_nm) {
        this.app_iss_nm = app_iss_nm;
    }

    public String getAcq_cd() {
        return acq_cd;
    }

    public void setAcq_cd(String acq_cd) {
        this.acq_cd = acq_cd;
    }

    public String getAcq_nm() {
        return acq_nm;
    }

    public void setAcq_nm(String acq_nm) {
        this.acq_nm = acq_nm;
    }

    public String getApp_acq_cd() {
        return app_acq_cd;
    }

    public void setApp_acq_cd(String app_acq_cd) {
        this.app_acq_cd = app_acq_cd;
    }

    public String getApp_acq_nm() {
        return app_acq_nm;
    }

    public void setApp_acq_nm(String app_acq_nm) {
        this.app_acq_nm = app_acq_nm;
    }

    public String getMerch_no() {
        return merch_no;
    }

    public void setMerch_no(String merch_no) {
        this.merch_no = merch_no;
    }

    public String getResult_cd() {
        return result_cd;
    }

    public void setResult_cd(String result_cd) {
        this.result_cd = result_cd;
    }

    public String getResult_msg() {
        return result_msg;
    }

    public void setResult_msg(String result_msg) {
        this.result_msg = result_msg;
    }

    public String getOrg_app_dd() {
        return org_app_dd;
    }

    public void setOrg_app_dd(String org_app_dd) {
        this.org_app_dd = org_app_dd;
    }

    public String getOrg_app_no() {
        return org_app_no;
    }

    public void setOrg_app_no(String org_app_no) {
        this.org_app_no = org_app_no;
    }

    public String getCncl_reason() {
        return cncl_reason;
    }

    public void setCncl_reason(String cncl_reason) {
        this.cncl_reason = cncl_reason;
    }

    public String getTran_status() {
        return tran_status;
    }

    public void setTran_status(String tran_status) {
        this.tran_status = tran_status;
    }

    public String getResult_status() {
        return result_status;
    }

    public void setResult_status(String result_status) {
        this.result_status = result_status;
    }

    public String getTran_step() {
        return tran_step;
    }

    public void setTran_step(String tran_step) {
        this.tran_step = tran_step;
    }

    public String getCncl_dt() {
        return cncl_dt;
    }

    public void setCncl_dt(String cncl_dt) {
        this.cncl_dt = cncl_dt;
    }

    public String getAcq_dt() {
        return acq_dt;
    }

    public void setAcq_dt(String acq_dt) {
        this.acq_dt = acq_dt;
    }

    public String getAcq_result_cd() {
        return acq_result_cd;
    }

    public void setAcq_result_cd(String acq_result_cd) {
        this.acq_result_cd = acq_result_cd;
    }

    public String getHoldoff_dt() {
        return holdoff_dt;
    }

    public void setHoldoff_dt(String holdoff_dt) {
        this.holdoff_dt = holdoff_dt;
    }

    public String getHoldon_dt() {
        return holdon_dt;
    }

    public void setHoldon_dt(String holdon_dt) {
        this.holdon_dt = holdon_dt;
    }

    public String getPay_dt() {
        return pay_dt;
    }

    public void setPay_dt(String pay_dt) {
        this.pay_dt = pay_dt;
    }

    public String getPay_amt() {
        return pay_amt;
    }

    public void setPay_amt(String pay_amt) {
        this.pay_amt = pay_amt;
    }

    public String getCommision() {
        return commision;
    }

    public void setCommision(String commision) {
        this.commision = commision;
    }

    public String getOnofftid_pay_amt() {
        return onofftid_pay_amt;
    }

    public void setOnofftid_pay_amt(String onofftid_pay_amt) {
        this.onofftid_pay_amt = onofftid_pay_amt;
    }

    public String getOnofftid_commision() {
        return onofftid_commision;
    }

    public void setOnofftid_commision(String onofftid_commision) {
        this.onofftid_commision = onofftid_commision;
    }

    public String getClient_ip() {
        return client_ip;
    }

    public void setClient_ip(String client_ip) {
        this.client_ip = client_ip;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getMemb_user_no() {
        return memb_user_no;
    }

    public void setMemb_user_no(String memb_user_no) {
        this.memb_user_no = memb_user_no;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_nm() {
        return user_nm;
    }

    public void setUser_nm(String user_nm) {
        this.user_nm = user_nm;
    }

    public String getUser_mail() {
        return user_mail;
    }

    public void setUser_mail(String user_mail) {
        this.user_mail = user_mail;
    }

    public String getUser_phone1() {
        return user_phone1;
    }

    public void setUser_phone1(String user_phone1) {
        this.user_phone1 = user_phone1;
    }

    public String getUser_phone2() {
        return user_phone2;
    }

    public void setUser_phone2(String user_phone2) {
        this.user_phone2 = user_phone2;
    }

    public String getUser_addr() {
        return user_addr;
    }

    public void setUser_addr(String user_addr) {
        this.user_addr = user_addr;
    }

    public String getProduct_type() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public String getProduct_nm() {
        return product_nm;
    }

    public void setProduct_nm(String product_nm) {
        this.product_nm = product_nm;
    }

    public String getFiller() {
        return filler;
    }

    public void setFiller(String filler) {
        this.filler = filler;
    }

    public String getFiller2() {
        return filler2;
    }

    public void setFiller2(String filler2) {
        this.filler2 = filler2;
    }

    public String getTerm_filler1() {
        return term_filler1;
    }

    public void setTerm_filler1(String term_filler1) {
        this.term_filler1 = term_filler1;
    }

    public String getTerm_filler2() {
        return term_filler2;
    }

    public void setTerm_filler2(String term_filler2) {
        this.term_filler2 = term_filler2;
    }

    public String getTerm_filler3() {
        return term_filler3;
    }

    public void setTerm_filler3(String term_filler3) {
        this.term_filler3 = term_filler3;
    }

    public String getTerm_filler4() {
        return term_filler4;
    }

    public void setTerm_filler4(String term_filler4) {
        this.term_filler4 = term_filler4;
    }

    public String getTerm_filler5() {
        return term_filler5;
    }

    public void setTerm_filler5(String term_filler5) {
        this.term_filler5 = term_filler5;
    }

    public String getTerm_filler6() {
        return term_filler6;
    }

    public void setTerm_filler6(String term_filler6) {
        this.term_filler6 = term_filler6;
    }

    public String getTerm_filler7() {
        return term_filler7;
    }

    public void setTerm_filler7(String term_filler7) {
        this.term_filler7 = term_filler7;
    }

    public String getTerm_filler8() {
        return term_filler8;
    }

    public void setTerm_filler8(String term_filler8) {
        this.term_filler8 = term_filler8;
    }

    public String getTerm_filler9() {
        return term_filler9;
    }

    public void setTerm_filler9(String term_filler9) {
        this.term_filler9 = term_filler9;
    }

    public String getTerm_filler10() {
        return term_filler10;
    }

    public void setTerm_filler10(String term_filler10) {
        this.term_filler10 = term_filler10;
    }

    public String getTrad_chk_flag() {
        return trad_chk_flag;
    }

    public void setTrad_chk_flag(String trad_chk_flag) {
        this.trad_chk_flag = trad_chk_flag;
    }

    public String getAcc_chk_flag() {
        return acc_chk_flag;
    }

    public void setAcc_chk_flag(String acc_chk_flag) {
        this.acc_chk_flag = acc_chk_flag;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    public String getUser_seq() {
        return user_seq;
    }

    public void setUser_seq(String user_seq) {
        this.user_seq = user_seq;
    }

    public String getRnum() {
        return rnum;
    }

    public void setRnum(String rnum) {
        this.rnum = rnum;
    }
    
}
