/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.agentpay.Vo;

/**
 *
 * @author Administrator
 */
public class Tb_AgentPay { 
    
    private String pay_mm           ;
    private String agent_seq        ;
    private String agent_nm         ;
    private String bank_cd          ;
    private String acc_no           ;
    private String bank_cd_acc_no   ;
    private String acc_ownner       ;
    private String pay_amt          ;
    private String decision_pay_amt ;
    private String app_amt          ;
    private String cncl_amt			;     
    private String pay_yyyymm;
    private String pay_yyyy;
    private String onffmerch_no;
    private String merch_nm;    
    private String app_cnt          ;
    private String cncl_cnt			;         
    private String vat_amt;
    private String commission;
    private String vat;

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }
    
    

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getVat_amt() {
        return vat_amt;
    }

    public void setVat_amt(String vat_amt) {
        this.vat_amt = vat_amt;
    }

    public String getApp_cnt() {
        return app_cnt;
    }

    public void setApp_cnt(String app_cnt) {
        this.app_cnt = app_cnt;
    }

    public String getCncl_cnt() {
        return cncl_cnt;
    }

    public void setCncl_cnt(String cncl_cnt) {
        this.cncl_cnt = cncl_cnt;
    }

    public String getOnffmerch_no() {
        return onffmerch_no;
    }

    public void setOnffmerch_no(String onffmerch_no) {
        this.onffmerch_no = onffmerch_no;
    }

    public String getMerch_nm() {
        return merch_nm;
    }

    public void setMerch_nm(String merch_nm) {
        this.merch_nm = merch_nm;
    }

    public String getPay_yyyymm() {
        return pay_yyyymm;
    }

    public void setPay_yyyymm(String pay_yyyymm) {
        this.pay_yyyymm = pay_yyyymm;
    }

    public String getPay_yyyy() {
        return pay_yyyy;
    }

    public void setPay_yyyy(String pay_yyyy) {
        this.pay_yyyy = pay_yyyy;
    }

    public String getPay_mm() {
        return pay_mm;
    }

    public void setPay_mm(String pay_mm) {
        this.pay_mm = pay_mm;
    }

    public String getAgent_seq() {
        return agent_seq;
    }

    public void setAgent_seq(String agent_seq) {
        this.agent_seq = agent_seq;
    }

    public String getAgent_nm() {
        return agent_nm;
    }

    public void setAgent_nm(String agent_nm) {
        this.agent_nm = agent_nm;
    }

    public String getBank_cd() {
        return bank_cd;
    }

    public void setBank_cd(String bank_cd) {
        this.bank_cd = bank_cd;
    }

    public String getAcc_no() {
        return acc_no;
    }

    public void setAcc_no(String acc_no) {
        this.acc_no = acc_no;
    }

    public String getBank_cd_acc_no() {
        return bank_cd_acc_no;
    }

    public void setBank_cd_acc_no(String bank_cd_acc_no) {
        this.bank_cd_acc_no = bank_cd_acc_no;
    }

    public String getAcc_ownner() {
        return acc_ownner;
    }

    public void setAcc_ownner(String acc_ownner) {
        this.acc_ownner = acc_ownner;
    }

    public String getPay_amt() {
        return pay_amt;
    }

    public void setPay_amt(String pay_amt) {
        this.pay_amt = pay_amt;
    }

    public String getDecision_pay_amt() {
        return decision_pay_amt;
    }

    public void setDecision_pay_amt(String decision_pay_amt) {
        this.decision_pay_amt = decision_pay_amt;
    }

    public String getApp_amt() {
        return app_amt;
    }

    public void setApp_amt(String app_amt) {
        this.app_amt = app_amt;
    }

    public String getCncl_amt() {
        return cncl_amt;
    }

    public void setCncl_amt(String cncl_amt) {
        this.cncl_amt = cncl_amt;
    }
    
}
