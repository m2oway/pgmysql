/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.agentpay.Service;

import com.onoffkorea.system.agentpay.Bean.AgentPayFormBean;
import java.util.Hashtable;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public interface AgentpayService {

    public Hashtable agentpayMasterList(AgentPayFormBean agentPayFormBean);

    public void agentpayMasterListExcel(AgentPayFormBean agentPayFormBean, String excelDownloadFilename, HttpServletResponse response);

    public Hashtable agentpayDetailList(AgentPayFormBean agentPayFormBean);

    public void agentpayDetailListExcel(AgentPayFormBean agentPayFormBean, String excelDownloadFilename, HttpServletResponse response);
    
}
