/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.agentpay.Service;

import com.onoffkorea.system.agentpay.Bean.AgentPayFormBean;
import com.onoffkorea.system.agentpay.Dao.AgentpayDAO;
import com.onoffkorea.system.agentpay.Vo.Tb_AgentPay;
import com.onoffkorea.system.common.util.Util;
import java.text.SimpleDateFormat;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Administrator
 */
@Service("agentpayService")
public class AgentpayServiceImpl implements AgentpayService{

    protected Logger logger = Logger.getLogger(getClass().getName());
    
    @Autowired
    private AgentpayDAO agentpayDAO;    

    @Override
    public Hashtable agentpayMasterList(AgentPayFormBean agentPayFormBean) {
        
        String bankNmAccNo = null;
        int idx = 0;
        String bankNm = null;
        String accNo = null;

        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        
        if((agentPayFormBean.getPay_yyyy() == null) || (agentPayFormBean.getPay_yyyy() == "")){
            
            String yyyy  = new java.text.SimpleDateFormat ("yyyy").format(new java.util.Date());
            String mm = new java.text.SimpleDateFormat ("MM").format(new java.util.Date());

            agentPayFormBean.setPay_yyyy(yyyy);
            agentPayFormBean.setPay_mm(mm);
        }        
        
        Hashtable ht = new Hashtable();

        try {
            
                List<Tb_AgentPay> ls_agentpayMasterList = agentpayDAO.agentpayMasterList(agentPayFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"rows\" : [");
                for (int i = 0; i < ls_agentpayMasterList.size(); i++) {
                    Tb_AgentPay tb_AgentPay = ls_agentpayMasterList.get(i);
                        
                       bankNmAccNo = tb_AgentPay.getBank_cd_acc_no();
                       idx = bankNmAccNo.indexOf("/");
                       bankNm = bankNmAccNo.substring(0, idx);
                       
                         if(!"".equals(Util.nullToString(tb_AgentPay.getAcc_no()))){
                            accNo = tb_AgentPay.getAcc_no().substring(0, 4)+"******"+tb_AgentPay.getAcc_no().substring(tb_AgentPay.getAcc_no().length()-4, tb_AgentPay.getAcc_no().length());
                        }else {
                            accNo="";
                        }
                                
                        
                        sb.append("{\"id\":" + i);
                        sb.append(",\"userdata\":{");
                        sb.append(" \"pay_yyyymm\":\""+tb_AgentPay.getPay_yyyymm()+"\"");
                        sb.append(",\"pay_yyyy\":\""+tb_AgentPay.getPay_yyyymm().substring(0, 4)+"\"");
                        sb.append(",\"pay_mm\":\""+tb_AgentPay.getPay_yyyymm().substring(4, 6)+"\"");
                        sb.append(",\"agent_seq\":\""+tb_AgentPay.getAgent_seq()+"\"");
                        sb.append("}");                  
                        sb.append(",\"data\":[");
                        sb.append(" \"" + Util.nullToString(tb_AgentPay.getPay_yyyymm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_AgentPay.getAgent_nm()) + "\"");
                        sb.append(",\"" + bankNm + " / " + accNo + "\"");
                        sb.append(",\"" + Util.nullToString(tb_AgentPay.getAcc_ownner()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_AgentPay.getPay_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_AgentPay.getVat_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_AgentPay.getDecision_pay_amt()) + "\"");
                        //sb.append(",\"" + Util.nullToString(tb_AgentPay.getCommission()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_AgentPay.getApp_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_AgentPay.getCncl_amt()) + "\"");

                        if (i == (ls_agentpayMasterList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");   

                ht.put("ResultSet", sb.toString());

        } catch (Exception e) {
                e.printStackTrace();
        }
        

        return ht;           
        
    }

    //대리점 지급 엑셀다운로드
    @Override
    public void agentpayMasterListExcel(AgentPayFormBean agentPayFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
              
                List ls_agentpayMasterListExcel = agentpayDAO.agentpayMasterListExcel(agentPayFormBean);
                
                StringBuffer agentpayMasterListExcel = Util.makeData(ls_agentpayMasterListExcel);
        
                Util.exceldownload(agentpayMasterListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    }

    //대리점 지급 상세 조회
    @Override
    public Hashtable agentpayDetailList(AgentPayFormBean agentPayFormBean) {

        SimpleDateFormat formatter = new SimpleDateFormat ( "yyyyMMdd", Locale.KOREA );
        
        if((agentPayFormBean.getPay_yyyy() == null) || (agentPayFormBean.getPay_yyyy() == "")){
            
            String yyyy  = new java.text.SimpleDateFormat ("yyyy").format(new java.util.Date());
            String mm = new java.text.SimpleDateFormat ("MM").format(new java.util.Date());

            agentPayFormBean.setPay_yyyy(yyyy);
            agentPayFormBean.setPay_mm(mm);
        }        
        
        Hashtable ht = new Hashtable();

        try {
            
                List<Tb_AgentPay> ls_agentpayDetailList = agentpayDAO.agentpayDetailList(agentPayFormBean);
                
                StringBuilder sb = new StringBuilder();
                sb.append("{\"rows\" : [");
                for (int i = 0; i < ls_agentpayDetailList.size(); i++) {
                        Tb_AgentPay tb_AgentPay = ls_agentpayDetailList.get(i);
                        sb.append("{\"id\":" + i);               
                        sb.append(",\"data\":[");
                        sb.append(" \"" + Util.nullToString(tb_AgentPay.getPay_yyyymm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_AgentPay.getAgent_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_AgentPay.getMerch_nm()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_AgentPay.getPay_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_AgentPay.getVat_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_AgentPay.getDecision_pay_amt()) + "\"");
                        //sb.append(",\"" + Util.nullToString(tb_AgentPay.getCommission()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_AgentPay.getApp_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_AgentPay.getCncl_amt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_AgentPay.getApp_cnt()) + "\"");
                        sb.append(",\"" + Util.nullToString(tb_AgentPay.getCncl_cnt()) + "\"");                        

                        if (i == (ls_agentpayDetailList.size() - 1)) {
                                sb.append("]}");
                        } else {
                                sb.append("]},");
                        }
                }

                sb.append("]}");   

                ht.put("ResultSet", sb.toString());

        } catch (Exception e) {
                e.printStackTrace();
        }
        

        return ht;          
        
    }
    
    //대리점 지급 상세 엑셀다운로드
    @Override
    public void agentpayDetailListExcel(AgentPayFormBean agentPayFormBean, String excelDownloadFilename, HttpServletResponse response) {

        try {
              
                List ls_agentpayDetailListExcel = agentpayDAO.agentpayDetailListExcel(agentPayFormBean);
                
                StringBuffer agentpayDetailListExcel = Util.makeData(ls_agentpayDetailListExcel);
        
                Util.exceldownload(agentpayDetailListExcel.toString(), excelDownloadFilename, response);
                
        } catch (Exception e) {
                e.printStackTrace();
        }        
        
    }    
    
}
