/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.agentpay.Dao;

import com.onoffkorea.system.agentpay.Bean.AgentPayFormBean;
import com.onoffkorea.system.agentpay.Vo.Tb_AgentPay;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("agentpayDAO")
public class AgentpayDAOImpl extends SqlSessionDaoSupport implements AgentpayDAO{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    //대리점 지급 조회
    @Override
    public List<Tb_AgentPay> agentpayMasterList(AgentPayFormBean agentPayFormBean) {
        
        return (List)getSqlSession().selectList("Agentpay.agentpayMasterList",agentPayFormBean);
        
    }

    //대리점 지급 엑셀다운로드
    @Override
    public List agentpayMasterListExcel(AgentPayFormBean agentPayFormBean) {
        
        return (List)getSqlSession().selectList("Agentpay.agentpayMasterListExcel",agentPayFormBean);
        
    }

    //대리점 지급 상세 조회
    @Override
    public List<Tb_AgentPay> agentpayDetailList(AgentPayFormBean agentPayFormBean) {
        
        return (List)getSqlSession().selectList("Agentpay.agentpayDetailList",agentPayFormBean);
        
    }
    
    //대리점 지급 상세 엑셀다운로드
    @Override
    public List agentpayDetailListExcel(AgentPayFormBean agentPayFormBean) {
        
        return (List)getSqlSession().selectList("Agentpay.agentpayDetailListExcel",agentPayFormBean);
        
    }    
    
}
