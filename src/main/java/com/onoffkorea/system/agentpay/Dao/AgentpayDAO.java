/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.agentpay.Dao;

import com.onoffkorea.system.agentpay.Bean.AgentPayFormBean;
import com.onoffkorea.system.agentpay.Vo.Tb_AgentPay;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface AgentpayDAO {

    public List<Tb_AgentPay> agentpayMasterList(AgentPayFormBean agentPayFormBean);

    public List agentpayMasterListExcel(AgentPayFormBean agentPayFormBean);

    public List<Tb_AgentPay> agentpayDetailList(AgentPayFormBean agentPayFormBean);

    public List agentpayDetailListExcel(AgentPayFormBean agentPayFormBean);
    
}
