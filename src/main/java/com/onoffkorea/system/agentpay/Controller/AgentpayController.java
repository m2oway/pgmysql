/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.agentpay.Controller;

import com.onoffkorea.system.agentpay.Bean.AgentPayFormBean;
import com.onoffkorea.system.agentpay.Service.AgentpayService;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import common.web.servlet.BaseSpringMultiActionController;
import java.util.Hashtable;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/agentpay/**")
public class AgentpayController extends BaseSpringMultiActionController{
        
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private CommonService commonService;
    
    @Autowired
    private AgentpayService agentpayService;    
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }    

    //레이아웃
    @RequestMapping(method= RequestMethod.GET,value = "/agentpayLayout")
    public String agentpayLayout(@Valid AgentPayFormBean agentPayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        return null;
        
    } 
    
    //대리점 지급 조회
    @RequestMapping(method= RequestMethod.GET,value = "/agentpayMasterList")
    public String agentpayMasterFormList(@Valid AgentPayFormBean agentPayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        agentPayFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            agentPayFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            agentPayFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            agentPayFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }           
        
        Hashtable ht_agentpayMasterList = agentpayService.agentpayMasterList(agentPayFormBean);
        String ls_agentpayMasterList = (String) ht_agentpayMasterList.get("ResultSet");
        
        model.addAttribute("ls_agentpayMasterList",ls_agentpayMasterList);             
        
        return null;
        
    }
    
    //대리점 지급 조회
    @RequestMapping(method= RequestMethod.POST,value = "/agentpayMasterList")
    @ResponseBody
    public String agentpayMasterList(@Valid AgentPayFormBean agentPayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        agentPayFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            agentPayFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            agentPayFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            agentPayFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                
        
        Hashtable ht_agentpayMasterList = agentpayService.agentpayMasterList(agentPayFormBean);
        String ls_agentpayMasterList = (String) ht_agentpayMasterList.get("ResultSet");

        return ls_agentpayMasterList;
        
    }        
    
    //대리점 지급 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/agentpayMasterListExcel")
    public void agentpayMasterListExcel(@Valid AgentPayFormBean agentPayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("agentpay.agentpayMasterListExcel.fileName"));

        agentPayFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            agentPayFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            agentPayFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            agentPayFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                
        
        agentpayService.agentpayMasterListExcel(agentPayFormBean, excelDownloadFilename, response); 
        
    }           
    
    //대리점 지급 상세 조회
    @RequestMapping(method= RequestMethod.GET,value = "/agentpayDetailList")
    public String agentpayDetailFormList(@Valid AgentPayFormBean agentPayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        agentPayFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            agentPayFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            agentPayFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            agentPayFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                
        
        Hashtable ht_agentpayDetailList = agentpayService.agentpayDetailList(agentPayFormBean);
        String ls_agentpayDetailList = (String) ht_agentpayDetailList.get("ResultSet");
        
        model.addAttribute("ls_agentpayDetailList",ls_agentpayDetailList);         
        
        return null;
        
    }
    
    //대리점 지급 상세 조회
    @RequestMapping(method= RequestMethod.POST,value = "/agentpayDetailList")
    @ResponseBody
    public String agentpayDetailList(@Valid AgentPayFormBean agentPayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        agentPayFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            agentPayFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            agentPayFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            agentPayFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                
        
        Hashtable ht_agentpayDetailList = agentpayService.agentpayDetailList(agentPayFormBean);
        String ls_agentpayDetailList = (String) ht_agentpayDetailList.get("ResultSet");

        return ls_agentpayDetailList;
        
    }           
    
    //대리점 지급 상세 엑셀다운로드
    @RequestMapping(method= RequestMethod.POST,value = "/agentpayDetailListExcel")
    public void agentpayDetailListExcel(@Valid AgentPayFormBean agentPayFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request, HttpServletResponse response
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

        String excelDownloadFilename = new String(getMessage("agentpay.agentpayDetailListExcel.fileName"));

        agentPayFormBean.setSes_user_cate(siteSession.getSes_user_cate());
        
        if("01".equals(siteSession.getSes_user_cate())){
            agentPayFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
        }else if("02".equals(siteSession.getSes_user_cate())){
            agentPayFormBean.setSes_onfftid(siteSession.getSes_onfftid());
        }else if("03".equals(siteSession.getSes_user_cate())){
            agentPayFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
        }                
        
        agentpayService.agentpayDetailListExcel(agentPayFormBean, excelDownloadFilename, response); 
        
    }        
    

}
