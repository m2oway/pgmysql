/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.paymtd.Dao;

import com.onoffkorea.system.paymtd.Bean.PaymtdDetailFormBean;
import com.onoffkorea.system.paymtd.Bean.PaymtdFormBean;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("paymtdDAO")
public class PaymtdDAOImpl extends SqlSessionDaoSupport  implements PaymtdDAO {
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Override
    public String paymtdRecordCount(PaymtdFormBean paymtdFormBean) {
        return (String) getSqlSession().selectOne("Paymtd.paymtdRecordCount", paymtdFormBean);
    }

    @Override
    public List paymtdMasterList(PaymtdFormBean paymtdFormBean) {
        return (List) getSqlSession().selectList("Paymtd.paymtdMasterList", paymtdFormBean);
    }

    @Override
    public List paymtdMasterInfo(PaymtdFormBean paymtdFormBean) {
         return (List) getSqlSession().selectList("Paymtd.paymtdMasterList", paymtdFormBean);
    }

    @Override
    public Integer paymtdMasterDelete(PaymtdFormBean paymtdFormBean) {
         //getSqlSession().delete("Paymtd.paymtdMasterDelete", paymtdFormBean);        
        return (Integer)getSqlSession().update("Paymtd.paymtdMasterDelete", paymtdFormBean);
    }

    @Override
    public Integer paymtdMasterInsert(PaymtdFormBean paymtdFormBean) {
        return (Integer)getSqlSession().insert("Paymtd.paymtdMasterInsert", paymtdFormBean);        
    }

    @Override
    public Integer paymtdMasterUpdate(PaymtdFormBean paymtdFormBean) {
        return (Integer)getSqlSession().update("Paymtd.paymtdMasterUpdate", paymtdFormBean);
    }

    @Override
    public List paymtdDtlList(PaymtdDetailFormBean paymtdDtlFormBean) {
        return (List) getSqlSession().selectList("Paymtd.paymtdDetailList", paymtdDtlFormBean);
    }

    @Override
    public Integer paymtdDtlInsert(PaymtdDetailFormBean paymtdDtlFormBean) {
        return (Integer)getSqlSession().insert("Paymtd.paymtdDetailInsert", paymtdDtlFormBean);
        
    }

    @Override
    public Integer paymtdDtlDelete(PaymtdDetailFormBean paymtdDtlFormBean) {
        
           int Resultx  = getSqlSession().update("Paymtd.paymtdDetailDel", paymtdDtlFormBean);
           logger.trace("xxxxxxxxx : " + Resultx);
           
        return Resultx;
    }

    @Override
    public String GetpaymtdMasterKey() {
        return (String) getSqlSession().selectOne("Paymtd.GetpaymtdMasterKey");
    }

    @Override
    public Integer paymtdDtlUpdate(PaymtdDetailFormBean paymtdDtlFormBean) {
        return (Integer)getSqlSession().update("Paymtd.paymtdDetailUpdate", paymtdDtlFormBean);
    }

    
}
