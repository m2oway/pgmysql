/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.paymtd.Dao;

import com.onoffkorea.system.paymtd.Bean.PaymtdDetailFormBean;
import com.onoffkorea.system.paymtd.Bean.PaymtdFormBean;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public interface PaymtdDAO {
    
    public String paymtdRecordCount(PaymtdFormBean paymtdFormBean);

    public List paymtdMasterList(PaymtdFormBean paymtdFormBean);

    public List paymtdMasterInfo(PaymtdFormBean paymtdFormBean);
    
    public Integer paymtdMasterDelete(PaymtdFormBean paymtdFormBean);

    public Integer paymtdMasterInsert(PaymtdFormBean paymtdFormBean);

    public Integer paymtdMasterUpdate(PaymtdFormBean paymtdFormBean);
    
    public List paymtdDtlList(PaymtdDetailFormBean paymtdDtlFormBean);
    
    public Integer paymtdDtlInsert(PaymtdDetailFormBean paymtdDtlFormBean);
    
    public Integer paymtdDtlDelete(PaymtdDetailFormBean paymtdDtlFormBean);
    
    public Integer paymtdDtlUpdate(PaymtdDetailFormBean paymtdDtlFormBean);
    
    public String GetpaymtdMasterKey();
 
}
