/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.paymtd.Bean;

import com.onoffkorea.system.common.util.CommonSortableListPagingForm;

/**
 *
 * @author Administrator
 */
public class PaymtdDetailFormBean extends CommonSortableListPagingForm{ 
    private String pay_mtd_dtl_seq	;
    private String pay_mtd_seq		;
    private String app_iss_cd		;
    private String app_iss_cd_nm        ;
    private String tid_mtd		;
    private String tid_mtd_nm		;
    private String tid_nm               ;
    private String terminal_no		;
    private String pay_mtd              ;
    private String pay_mtd_nm              ;
    private String merch_no             ;
    private String mid_nm               ;    
    private String use_flag		;
    private String del_flag		;
    private String ins_dt		;
    private String mod_dt		;
    private String ins_user		;
    private String mod_user		;

    public String getPay_mtd() {
        return pay_mtd;
    }

    public void setPay_mtd(String pay_mtd) {
        this.pay_mtd = pay_mtd;
    }

    public String getPay_mtd_nm() {
        return pay_mtd_nm;
    }

    public void setPay_mtd_nm(String pay_mtd_nm) {
        this.pay_mtd_nm = pay_mtd_nm;
    }

    public String getMerch_no() {
        return merch_no;
    }

    public void setMerch_no(String merch_no) {
        this.merch_no = merch_no;
    }

    public String getMid_nm() {
        return mid_nm;
    }

    public void setMid_nm(String mid_nm) {
        this.mid_nm = mid_nm;
    }

    public String getPay_mtd_dtl_seq() {
        return pay_mtd_dtl_seq;
    }

    public void setPay_mtd_dtl_seq(String pay_mtd_dtl_seq) {
        this.pay_mtd_dtl_seq = pay_mtd_dtl_seq;
    }

    public String getPay_mtd_seq() {
        return pay_mtd_seq;
    }

    public void setPay_mtd_seq(String pay_mtd_seq) {
        this.pay_mtd_seq = pay_mtd_seq;
    }

    public String getApp_iss_cd() {
        return app_iss_cd;
    }

    public void setApp_iss_cd(String app_iss_cd) {
        this.app_iss_cd = app_iss_cd;
    }

    public String getApp_iss_cd_nm() {
        return app_iss_cd_nm;
    }

    public void setApp_iss_cd_nm(String app_iss_cd_nm) {
        this.app_iss_cd_nm = app_iss_cd_nm;
    }

    public String getTid_mtd() {
        return tid_mtd;
    }

    public void setTid_mtd(String tid_mtd) {
        this.tid_mtd = tid_mtd;
    }

    public String getTid_mtd_nm() {
        return tid_mtd_nm;
    }

    public void setTid_mtd_nm(String tid_mtd_nm) {
        this.tid_mtd_nm = tid_mtd_nm;
    }

    public String getTid_nm() {
        return tid_nm;
    }

    public void setTid_nm(String tid_nm) {
        this.tid_nm = tid_nm;
    }

    public String getTerminal_no() {
        return terminal_no;
    }

    public void setTerminal_no(String terminal_no) {
        this.terminal_no = terminal_no;
    }

    public String getUse_flag() {
        return use_flag;
    }

    public void setUse_flag(String use_flag) {
        this.use_flag = use_flag;
    }

    public String getDel_flag() {
        return del_flag;
    }

    public void setDel_flag(String del_flag) {
        this.del_flag = del_flag;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    
    
    
}
