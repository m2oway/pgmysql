/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.paymtd.Bean;

import com.onoffkorea.system.common.util.CommonSortableListPagingForm;

/**
 *
 * @author Administrator
 */
public class PaymtdFormBean extends CommonSortableListPagingForm{
    
    private String[] pay_mtd_seqs;

    private String pay_mtd_seq;	//seq
    private String pay_mtd_nm;	//결제수단명
    private String func_cate;	//유이자무이자구분
    private String agent_flag;	//자체대행구분
    private String commission;
    private String commision2;	//무이자수수료
    private String commision3;	//기타수수료
    private String start_dt;	//시작일
    private String end_dt;		//종료일
    private String memo;		//memo
    private String use_flag;	//사용여부
    private String del_flag;	//삭제여부
    private String ins_dt;		//입력일
    private String mod_dt;		//수정일
    private String ins_user;	//입력자
    private String mod_user;	//수정자
    private String paymtd_detailInfo; //결제수단 상세입력 정보
    

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getPaymtd_detailInfo() {
        return paymtd_detailInfo;
    }

    public void setPaymtd_detailInfo(String paymtd_detailInfo) {
        this.paymtd_detailInfo = paymtd_detailInfo;
    }
    
    public String[] getPay_mtd_seqs() {
        return pay_mtd_seqs;
    }

    public void setPay_mtd_seqs(String[] pay_mtd_seqs) {
        this.pay_mtd_seqs = pay_mtd_seqs;
    }

    public String getPay_mtd_seq() {
        return pay_mtd_seq;
    }

    public void setPay_mtd_seq(String pay_mtd_seq) {
        this.pay_mtd_seq = pay_mtd_seq;
    }

    public String getPay_mtd_nm() {
        return pay_mtd_nm;
    }

    public void setPay_mtd_nm(String pay_mtd_nm) {
        this.pay_mtd_nm = pay_mtd_nm;
    }

    public String getFunc_cate() {
        return func_cate;
    }

    public void setFunc_cate(String func_cate) {
        this.func_cate = func_cate;
    }

    public String getAgent_flag() {
        return agent_flag;
    }

    public void setAgent_flag(String agent_flag) {
        this.agent_flag = agent_flag;
    }

    public String getCommision2() {
        return commision2;
    }

    public void setCommision2(String commision2) {
        this.commision2 = commision2;
    }

    public String getCommision3() {
        return commision3;
    }

    public void setCommision3(String commision3) {
        this.commision3 = commision3;
    }

    public String getStart_dt() {
        return start_dt;
    }

    public void setStart_dt(String start_dt) {
        this.start_dt = start_dt;
    }

    public String getEnd_dt() {
        return end_dt;
    }

    public void setEnd_dt(String end_dt) {
        this.end_dt = end_dt;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getUse_flag() {
        return use_flag;
    }

    public void setUse_flag(String use_flag) {
        this.use_flag = use_flag;
    }

    public String getDel_flag() {
        return del_flag;
    }

    public void setDel_flag(String del_flag) {
        this.del_flag = del_flag;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }
    
    

    
}
