/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.paymtd.Service;

import com.onoffkorea.system.company.Bean.CompanyFormBean;
import com.onoffkorea.system.paymtd.Bean.PaymtdDetailFormBean;
import com.onoffkorea.system.paymtd.Bean.PaymtdFormBean;
import java.util.Hashtable;

/**
 *
 * @author Administrator
 */
public interface PaymtdService {
    
    public Hashtable paymtdMasterList(PaymtdFormBean paymtdFormBean) throws Exception;

    public String paymtdMasterDelete(PaymtdFormBean paymtdFormBean) throws Exception;

    public String paymtdMasterInsert(PaymtdFormBean paymtdFormBean) throws Exception;

    public String paymtdMasterUpdate(PaymtdFormBean paymtdFormBean) throws Exception;
    
    public Hashtable paymtdMasterInfo(PaymtdFormBean paymtdFormBean) throws Exception;
    
    public Hashtable paymtdDtlList(PaymtdDetailFormBean paymtdtlFormBean) throws Exception;
    
    public String paymtdDtlInsert(PaymtdDetailFormBean paymtdtlFormBean) throws Exception;
        
    public String paymtdDtlUpdate(PaymtdDetailFormBean paymtdtlFormBean) throws Exception;
    
    public String paymtdDtlDelete(PaymtdDetailFormBean paymtdtlFormBean) throws Exception;
    
}
