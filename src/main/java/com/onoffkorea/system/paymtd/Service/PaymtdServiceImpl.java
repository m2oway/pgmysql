/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.paymtd.Service;

import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.paymtd.Bean.PaymtdDetailFormBean;
import com.onoffkorea.system.paymtd.Bean.PaymtdFormBean;
import com.onoffkorea.system.paymtd.Dao.PaymtdDAO;
import com.onoffkorea.system.paymtd.Vo.Tb_Pay_Mtd;
import com.onoffkorea.system.paymtd.Vo.Tb_Pay_Mtd_Detail;
import java.util.Hashtable;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service("paymtdService")
public class PaymtdServiceImpl implements PaymtdService
{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private PaymtdDAO paymtdDAO;
    
    
    @Override
    public Hashtable paymtdMasterList(PaymtdFormBean paymtdFormBean) throws Exception {
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
       
        String total_count = this.paymtdDAO.paymtdRecordCount(paymtdFormBean);
       
        List ls_paymtdMasterList = this.paymtdDAO.paymtdMasterList(paymtdFormBean);
        
        sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(paymtdFormBean.getStart_no())-1)+"\",\"rows\" : [");
        
        for (int i = 0; i < ls_paymtdMasterList.size(); i++) {
                Tb_Pay_Mtd tb_Paymtd = (Tb_Pay_Mtd) ls_paymtdMasterList.get(i);
                //NO,선택,결제수단명,일반/무이자,자체/대행,시작일,종료일,사용여부
                sb.append("{\"id\":" + i);
                sb.append(",\"userdata\":{");
                sb.append(" \"pay_mtd_seq\":\""+tb_Paymtd.getPay_mtd_seq()+"\"");
                sb.append("}");                  
                sb.append(",\"data\":[");
                sb.append(" \"" + tb_Paymtd.getRnum() + "\"");
                //sb.append(",\"" + "" + "\"");         
                sb.append(",\"" + Util.nullToString(tb_Paymtd.getPay_mtd_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Paymtd.getFunc_cate_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Paymtd.getAgent_flag_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Paymtd.getStart_dt()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Paymtd.getEnd_dt()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Paymtd.getUse_flag_nm()) + "\"");
                
                if (i == (ls_paymtdMasterList.size() - 1)) {
                        sb.append("]}");
                } else {
                        sb.append("]},");
                }
        }

        sb.append("]}");    
        
        ht.put("ResultSet", sb.toString());
        
        return ht;
    }

    @Override
    @Transactional
    public String paymtdMasterDelete(PaymtdFormBean paymtdFormBean) throws Exception {
        Integer result = null; 
        result = this.paymtdDAO.paymtdMasterDelete(paymtdFormBean);
        
        return result.toString();
    }

    @Override
    @Transactional
    public String paymtdMasterInsert(PaymtdFormBean paymtdFormBean) throws Exception {
        Integer result = null;
        
        String strpay_mtd_seq = this.paymtdDAO.GetpaymtdMasterKey();
        
        paymtdFormBean.setPay_mtd_seq(strpay_mtd_seq);

        //메인정보 입력
        result = this.paymtdDAO.paymtdMasterInsert(paymtdFormBean);
        
        String strpaymtdDtlInfo = paymtdFormBean.getPaymtd_detailInfo();
        
        logger.trace("strpaymtdDtlInfo :" + strpaymtdDtlInfo);
        
        if(strpaymtdDtlInfo != null && !strpaymtdDtlInfo.equals(""))
        {
            String[] rows = strpaymtdDtlInfo.split("]");
            
            for(int x=0 ; x < rows.length ; x++)
            {
                String[] cols = rows[x].split(",");
                
                ////결제카드코드,결제카드명,결제사명,결제사코드,TID명,TID,가맹점종류코드,가맹점번호명,가맹점번호
                //      0         1         2          3      4     5       6            7            8
                ////  결제카드코드, 결제사코드, TID,가맹점종류코드,가맹점번호명,가맹점번호
                PaymtdDetailFormBean insdtlobj = new PaymtdDetailFormBean();
                
                insdtlobj.setPay_mtd_seq(strpay_mtd_seq);
                insdtlobj.setApp_iss_cd(cols[0]);
                insdtlobj.setTid_mtd(cols[1]);
                insdtlobj.setTerminal_no(cols[2]);
                insdtlobj.setPay_mtd(cols[3]);
                insdtlobj.setMerch_no(cols[4]);
                insdtlobj.setUse_flag("Y");
                insdtlobj.setIns_user(paymtdFormBean.getIns_user());
                insdtlobj.setMod_user(paymtdFormBean.getIns_user());
                
                this.paymtdDAO.paymtdDtlInsert(insdtlobj);
            }
        }
        
        return result.toString();
    }

    @Override
    @Transactional
    public String paymtdMasterUpdate(PaymtdFormBean paymtdFormBean) throws Exception {
        Integer result = null;    
        result = this.paymtdDAO.paymtdMasterUpdate(paymtdFormBean);
        return result.toString();
    }

    @Override
    public Hashtable paymtdMasterInfo(PaymtdFormBean paymtdFormBean) throws Exception {
        
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
        
        List ls_companyMasterList = this.paymtdDAO.paymtdMasterInfo(paymtdFormBean);
                
        ht.put("ResultSet", ls_companyMasterList);
        
        return ht;
    }

    @Override
    public Hashtable paymtdDtlList(PaymtdDetailFormBean paymtdtlFormBean) throws Exception {
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
       
        List ls_paymtdDtlMasterList = this.paymtdDAO.paymtdDtlList(paymtdtlFormBean);
        
        sb.append("{\"rows\" : [");
        
        for (int i = 0; i < ls_paymtdDtlMasterList.size(); i++) {
                Tb_Pay_Mtd_Detail tb_PaymtdDtl = (Tb_Pay_Mtd_Detail) ls_paymtdDtlMasterList.get(i);
                //결제수단seq,결제카드사코드,결제카드사명,결제사명,결제사코드,tid,사용여부코드,사용여부명
                sb.append("{\"id\":" + tb_PaymtdDtl.getPay_mtd_dtl_seq());
                sb.append(",\"data\":[");
                //sb.append(" \"" + tb_PaymtdDtl.getRnum() + "\"");
                //sb.append("\"" + "" + "\"");         
                sb.append("\"" + Util.nullToString(tb_PaymtdDtl.getPay_mtd_seq()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_PaymtdDtl.getApp_iss_cd()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_PaymtdDtl.getApp_iss_cd_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_PaymtdDtl.getTid_mtd_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_PaymtdDtl.getTid_mtd()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_PaymtdDtl.getTid_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_PaymtdDtl.getTerminal_no()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_PaymtdDtl.getPay_mtd()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_PaymtdDtl.getMid_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_PaymtdDtl.getMerch_no()) + "\"");                
                sb.append(",\"" + Util.nullToString(tb_PaymtdDtl.getUse_flag()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_PaymtdDtl.getUse_flag_nm()) + "\"");
                
                if (i == (ls_paymtdDtlMasterList.size() - 1)) {
                        sb.append("]}");
                } else {
                        sb.append("]},");
                }
        }

        sb.append("]}");    
        
        ht.put("ResultSet", sb.toString());
        
        return ht;
    }

    @Override
    @Transactional
    public String paymtdDtlInsert(PaymtdDetailFormBean paymtdtlFormBean) throws Exception {
        //메인정보 입력
        Integer result = null;
        
        result = this.paymtdDAO.paymtdDtlInsert(paymtdtlFormBean);
        
        return result.toString();
    }

    @Override
    @Transactional
    public String paymtdDtlUpdate(PaymtdDetailFormBean paymtdtlFormBean) throws Exception {
        Integer result = null;
        result = this.paymtdDAO.paymtdDtlUpdate(paymtdtlFormBean);
        logger.trace("-----------------service returnval start!!----------------" );
        logger.trace(result);
        logger.trace("-----------------service returnval end!!----------------" );
        return result.toString();
    }

    @Override
    @Transactional
    public String paymtdDtlDelete(PaymtdDetailFormBean paymtdtlFormBean) throws Exception {
        Integer result = null;
        result = this.paymtdDAO.paymtdDtlDelete(paymtdtlFormBean);
        return result.toString();
    }
    
}
