/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.paymtd.Controller;

import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.paymtd.Bean.PaymtdFormBean;
import com.onoffkorea.system.paymtd.Service.PaymtdService;
import com.onoffkorea.system.paymtd.Vo.Tb_Pay_Mtd;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import com.onoffkorea.system.paymtd.Bean.PaymtdDetailFormBean;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/paymtd/**")
public class PaymtdContrloller {
    
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private PaymtdService paymtdService;
    
    @Autowired
    private CommonService commonService;
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }    
    
    //정보 조회
    @RequestMapping(method= RequestMethod.GET,value = "/paymtdMasterList")
    public String paymtdMasterList(@Valid PaymtdFormBean paymtdFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("FUNC_CATE");
        Tb_Code_Main FuncCateList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("FuncCateList",FuncCateList);
        
        commonCodeSearchFormBean.setMain_code("AGENCY_FLAG");
        Tb_Code_Main AgencyFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("AgencyFlagList",AgencyFlagList);
        
        commonCodeSearchFormBean.setMain_code("USE_FLAG");
        Tb_Code_Main UseFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("UseFlagList",UseFlagList);
        
        
        Hashtable ht_paymtdMasterList = paymtdService.paymtdMasterList(paymtdFormBean);
        String ls_companyMasterList = (String) ht_paymtdMasterList.get("ResultSet");

        model.addAttribute("paymtdMasterList",ls_companyMasterList);
        
        return null;
        
    }
    
    //paymtd 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/paymtdMasterList")
    public @ResponseBody String paymtdMasterInfo(@Valid PaymtdFormBean paymtdFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
        
        Hashtable ht_paymtdMasterList = paymtdService.paymtdMasterList(paymtdFormBean);
        String ls_paymtdMasterList = (String)ht_paymtdMasterList.get("ResultSet");
     
        return ls_paymtdMasterList;
       
    }
    
    //paymtd 정보 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/paymtdMasterDelete")
    public @ResponseBody String paymtdMasterDelete(@Valid PaymtdFormBean paymtdFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
      
        String result = null;
        
        result = paymtdService.paymtdMasterDelete(paymtdFormBean);
        
        return result;
        
    }
    
    //정보 등록
    @RequestMapping(method= RequestMethod.GET,value = "/paymtdMasterInsert")
    public String paymtdMasterInsertForm(@Valid PaymtdFormBean paymtdFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        /*
        FUNC_CATE	
        AGENCY_FLAG	
        USE_FLAG	
        */
        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("FUNC_CATE");
        Tb_Code_Main FuncCateList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("FuncCateList",FuncCateList);
        
        commonCodeSearchFormBean.setMain_code("AGENCY_FLAG");
        Tb_Code_Main AgencyFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("AgencyFlagList",AgencyFlagList);
        
        commonCodeSearchFormBean.setMain_code("USE_FLAG");
        Tb_Code_Main UseFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("UseFlagList",UseFlagList);
        
        
        commonCodeSearchFormBean.setMain_code("APP_ISS_CD");
        Tb_Code_Main IssCdList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("IssCdList",IssCdList);
        
        
        return null;
        
    }   
    
    //paymtd 정보 등록
    @RequestMapping(method= RequestMethod.POST,value = "/paymtdMasterInsert")
    public @ResponseBody String paymtdMasterInsert(@Valid PaymtdFormBean paymtdFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {
        String result = null;
        
        paymtdFormBean.setIns_user(dentalHopitalSession.getUser_seq());
        paymtdFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        result = paymtdService.paymtdMasterInsert(paymtdFormBean);
        
        return result;
        
    }       
    
    //paymtd 정보 수정 
    @RequestMapping(method= RequestMethod.GET,value = "/paymtdMasterUpdate")
    public String paymtdMasterUpdateForm(@Valid PaymtdFormBean paymtdFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_paymtdMasterList = paymtdService.paymtdMasterInfo(paymtdFormBean);
        List ls_paymtdMasterList = (List) ht_paymtdMasterList.get("ResultSet");
        Tb_Pay_Mtd tb_Paymtd = (Tb_Pay_Mtd) ls_paymtdMasterList.get(0);
        
        model.addAttribute("tb_Paymtd",tb_Paymtd);
        
        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("FUNC_CATE");
        Tb_Code_Main FuncCateList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("FuncCateList",FuncCateList);
        
        commonCodeSearchFormBean.setMain_code("AGENCY_FLAG");
        Tb_Code_Main AgencyFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("AgencyFlagList",AgencyFlagList);
        
        commonCodeSearchFormBean.setMain_code("USE_FLAG");
        Tb_Code_Main UseFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("UseFlagList",UseFlagList);
        
        commonCodeSearchFormBean.setMain_code("APP_ISS_CD");
        Tb_Code_Main IssCdList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("IssCdList",IssCdList);
        
        PaymtdDetailFormBean paymtdtlFormBean = new PaymtdDetailFormBean();
        
        paymtdtlFormBean.setPay_mtd_seq(paymtdFormBean.getPay_mtd_seq());
        
        Hashtable ht_paymtdDtList = paymtdService.paymtdDtlList(paymtdtlFormBean);
        String strpaymtdDtList = (String) ht_paymtdDtList.get("ResultSet");
        
        model.addAttribute("PaymtdDtlList",strpaymtdDtList);
        
        return null;
        
    }       
    
    //paymtd 정보 수정 
    @RequestMapping(method= RequestMethod.POST,value = "/paymtdMasterUpdate")
    public @ResponseBody String paymtdMasterUpdate(@Valid PaymtdFormBean paymtdFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        paymtdFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        String result = paymtdService.paymtdMasterUpdate(paymtdFormBean);
        
        return result;
        
    }            
    
    //paymtddtl 정보 등록
    @RequestMapping(method= RequestMethod.POST,value = "/paymtdDtlInsert")
    public @ResponseBody String paymtdDtlInsert(@Valid PaymtdDetailFormBean paymtdDtlFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        paymtdDtlFormBean.setIns_user(dentalHopitalSession.getUser_seq());
        paymtdDtlFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        String result = paymtdService.paymtdDtlInsert(paymtdDtlFormBean);
        
        return result;
        
    }       
    
    
    //paymtddtl 정보 수정 
    @RequestMapping(method= RequestMethod.POST,value = "/paymtdDtlUpdate")
    public @ResponseBody String paymtdDtlUpdate(@Valid PaymtdDetailFormBean paymtdDtlFormBean, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        paymtdDtlFormBean.setIns_user(dentalHopitalSession.getUser_seq());
        paymtdDtlFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        String result = paymtdService.paymtdDtlUpdate(paymtdDtlFormBean);
        
        return result;
    }        
    
    
    //paymtddtl 정보 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/paymtdDtlDelete")
    //public @ResponseBody String paymtdDtlDelete(@Valid PaymtdDetailFormBean paymtdDtlFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
    public @ResponseBody String paymtdDtlDelete(@Valid PaymtdDetailFormBean paymtdDtlFormBean, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {                

        paymtdDtlFormBean.setIns_user(dentalHopitalSession.getUser_seq());
        paymtdDtlFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        String result = paymtdService.paymtdDtlDelete(paymtdDtlFormBean);
        
        return result;
        
    }
    
    //paymtddtl 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/paymtdDtlList")
    public @ResponseBody String paymtdDtlInfo(@Valid PaymtdDetailFormBean paymtdDtlFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
        
        Hashtable ht_paymtdMasterList = paymtdService.paymtdDtlList(paymtdDtlFormBean);
        String ls_paymtdMasterList = (String)ht_paymtdMasterList.get("ResultSet");
     
        return ls_paymtdMasterList;
    }
    
    
}
