/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.onoffkorea.system.paymtd.Vo;

/**
 *
 * @author Administrator
 */
public class Tb_Pay_Mtd {
    private String pay_mtd_seq;	//seq
    private String pay_mtd_nm;	//결제수단명
    private String func_cate;	//유이자무이자구분
    private String func_cate_nm;	//유이자무이자구분
    private String agent_flag;	//자체대행구분
    private String agent_flag_nm;//자체대행구분
    private String commission;	//수수료
    private String commision2;	//무이자수수료
    private String commision3;	//기타수수료
    private String start_dt;	//시작일
    private String end_dt;		//종료일
    private String memo;		//memo
    private String use_flag;	//사용여부
    private String use_flag_nm;	//사용여부
    private String del_flag;	//삭제여부
    private String del_flag_nm;	//삭제여부
    private String ins_dt;		//입력일
    private String mod_dt;		//수정일
    private String ins_user;	//입력자
    private String mod_user;	//수정자
    private String rnum;

    public String getAgent_flag_nm() {
        return agent_flag_nm;
    }

    public void setAgent_flag_nm(String agent_flag_nm) {
        this.agent_flag_nm = agent_flag_nm;
    }

    public String getRnum() {
        return rnum;
    }

    public void setRnum(String rnum) {
        this.rnum = rnum;
    }

    public String getPay_mtd_seq() {
        return pay_mtd_seq;
    }

    public void setPay_mtd_seq(String pay_mtd_seq) {
        this.pay_mtd_seq = pay_mtd_seq;
    }

    public String getPay_mtd_nm() {
        return pay_mtd_nm;
    }

    public void setPay_mtd_nm(String pay_mtd_nm) {
        this.pay_mtd_nm = pay_mtd_nm;
    }

    public String getFunc_cate() {
        return func_cate;
    }

    public void setFunc_cate(String func_cate) {
        this.func_cate = func_cate;
    }

    public String getFunc_cate_nm() {
        return func_cate_nm;
    }

    public void setFunc_cate_nm(String func_cate_nm) {
        this.func_cate_nm = func_cate_nm;
    }

    public String getAgent_flag() {
        return agent_flag;
    }

    public void setAgent_flag(String agent_flag) {
        this.agent_flag = agent_flag;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getCommision2() {
        return commision2;
    }

    public void setCommision2(String commision2) {
        this.commision2 = commision2;
    }

    public String getCommision3() {
        return commision3;
    }

    public void setCommision3(String commision3) {
        this.commision3 = commision3;
    }

    public String getStart_dt() {
        return start_dt;
    }

    public void setStart_dt(String start_dt) {
        this.start_dt = start_dt;
    }

    public String getEnd_dt() {
        return end_dt;
    }

    public void setEnd_dt(String end_dt) {
        this.end_dt = end_dt;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getUse_flag() {
        return use_flag;
    }

    public void setUse_flag(String use_flag) {
        this.use_flag = use_flag;
    }

    public String getUse_flag_nm() {
        return use_flag_nm;
    }

    public void setUse_flag_nm(String use_flag_nm) {
        this.use_flag_nm = use_flag_nm;
    }

    public String getDel_flag() {
        return del_flag;
    }

    public void setDel_flag(String del_flag) {
        this.del_flag = del_flag;
    }

    public String getDel_flag_nm() {
        return del_flag_nm;
    }

    public void setDel_flag_nm(String del_flag_nm) {
        this.del_flag_nm = del_flag_nm;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    
    
}
