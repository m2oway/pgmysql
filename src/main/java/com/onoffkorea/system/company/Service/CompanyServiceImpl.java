/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.company.Service;

import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.company.Bean.CompanyFormBean;
import com.onoffkorea.system.company.Dao.CompanyDAO;
import com.onoffkorea.system.company.Vo.Tb_Company;
import java.util.Hashtable;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service("companyService")
public class CompanyServiceImpl  implements CompanyService{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private CompanyDAO companyDAO;

    //사업체 정보 관리
    @Override    
    public Hashtable companyMasterList(CompanyFormBean companyFormBean) throws Exception {
        
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
        
        List ls_companyMasterList = this.companyDAO.companyMasterList(companyFormBean);
        
        String total_count = this.companyDAO.companyRecordCount(companyFormBean);
        
        sb.append("{\"total_count\":\""+total_count+"\",\"pos\":\""+(Integer.parseInt(companyFormBean.getStart_no())-1)+"\",\"rows\" : [");
        
        for (int i = 0; i < ls_companyMasterList.size(); i++) {
                Tb_Company tb_Company = (Tb_Company) ls_companyMasterList.get(i);

                //sb.append("{\"id\":" + tb_Company.getComp_seq());
                sb.append("{\"id\":" + i);
                sb.append(",\"userdata\":{");                
                sb.append("\"comp_seq\":\""+Util.nullToString(tb_Company.getComp_seq())+"\"");
                sb.append("}");
                sb.append(",\"data\":[");
                sb.append(" \"" + tb_Company.getRnum() + "\"");
                sb.append(",\"" + Util.nullToString(tb_Company.getComp_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Company.getBiz_no()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Company.getCorp_no()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Company.getComp_ceo_nm()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Company.getComp_tel1()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Company.getComp_tel2()) + "\"");
                sb.append(",\"" + Util.nullToString(tb_Company.getBiz_type()) + "\"");//업태
                sb.append(",\"" + Util.nullToString(tb_Company.getBiz_cate()) + "\"");//업종
                sb.append(",\"" + Util.nullToString(tb_Company.getTax_flag_nm()) + "\"");//과세구분
                sb.append(",\"" + Util.nullToString(tb_Company.getIns_dt()) + "\"");

                if (i == (ls_companyMasterList.size() - 1)) {
                        sb.append("]}");
                } else {
                        sb.append("]},");
                }
        }

        sb.append("]}");    
        
        ht.put("ResultSet", sb.toString());
        ht.put("ls_companyMasterList", ls_companyMasterList);
        
        return ht;
    }

    //사업체 정보 삭제
    @Override
    @Transactional
    public String companyMasterDelete(CompanyFormBean companyFormBean) throws Exception{
        
      Integer result = null;
      result = companyDAO.companyMasterDelete(companyFormBean);
      return result.toString();
        
    }

    //사업체 정보 등록
    @Override
    @Transactional
    public String companyMasterInsert(CompanyFormBean companyFormBean)  throws Exception {

        Integer result = null;
        result = this.companyDAO.companyMasterInsert(companyFormBean);
        return result.toString();
        
    }

    //사업체 정보 수정
    @Override
    @Transactional
    public String companyMasterUpdate(CompanyFormBean companyFormBean)  throws Exception {
        Integer result = null;
        result = companyDAO.companyMasterUpdate(companyFormBean);
        return result.toString();
    }

    @Override
    public Hashtable companyMasterInfo(CompanyFormBean companyFormBean) throws Exception {
      
        Hashtable ht = new Hashtable();
        
        StringBuilder sb = new StringBuilder();
        
        List ls_companyMasterList = this.companyDAO.companyMasterList(companyFormBean);
        
                
        ht.put("ResultSet", ls_companyMasterList);
        
        return ht;

    }

}
