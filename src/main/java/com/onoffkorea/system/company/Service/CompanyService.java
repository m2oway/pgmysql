/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.company.Service;

import com.onoffkorea.system.company.Bean.CompanyFormBean;
import java.util.Hashtable;

/**
 *
 * @author Administrator
 */
public interface CompanyService {

    public Hashtable companyMasterList(CompanyFormBean companyFormBean) throws Exception;

    public String companyMasterDelete(CompanyFormBean companyFormBean) throws Exception;

    public String companyMasterInsert(CompanyFormBean companyFormBean) throws Exception;

    public String companyMasterUpdate(CompanyFormBean companyFormBean) throws Exception;
    
    public Hashtable companyMasterInfo(CompanyFormBean companyFormBean) throws Exception;
    
}