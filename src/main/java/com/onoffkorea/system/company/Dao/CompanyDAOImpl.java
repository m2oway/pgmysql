/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.company.Dao;

import com.onoffkorea.system.company.Bean.CompanyFormBean;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("companyDAO")
public class CompanyDAOImpl extends SqlSessionDaoSupport implements CompanyDAO{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    
    //사업체 정보 관리
    @Override
    public List companyMasterList(CompanyFormBean companyFormBean) {

        return (List) getSqlSession().selectList("Company.companyMasterList", companyFormBean);

    }

    //사업체 정보 삭제
    @Override
    public Integer companyMasterDelete(CompanyFormBean companyFormBean) {

        //getSqlSession().delete("Company.companyMasterDelete", companyFormBean);
        Integer result = getSqlSession().update("Company.companyMasterDelete", companyFormBean);
        return result;        
    }

    //사업체 정보 등록
    @Override
    public Integer companyMasterInsert(CompanyFormBean companyFormBean) {
        Integer result = getSqlSession().insert("Company.companyMasterInsert", companyFormBean);
        return result;
        
    }
    
    //사업체 정보 수정
    @Override
    public Integer companyMasterUpdate(CompanyFormBean companyFormBean) {

        Integer result =  getSqlSession().update("Company.companyMasterUpdate", companyFormBean);
        return result;
    }
    
    //total record count
    @Override
    public String companyRecordCount(CompanyFormBean companyFormBean) {
       return (String) getSqlSession().selectOne("companyRecordCount", companyFormBean);
    }

    @Override
    public List companyMasterInfo(CompanyFormBean companyFormBean) {
       return (List) getSqlSession().selectList("Company.companyMasterList", companyFormBean);
    }
    
}
