/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.company.Dao;

import com.onoffkorea.system.company.Bean.CompanyFormBean;
import java.util.List;

/**
 *
 * @author Administrator
 */
public interface CompanyDAO {
    
    public String companyRecordCount(CompanyFormBean companyFormBean);

    public List companyMasterList(CompanyFormBean companyFormBean);

    public List companyMasterInfo(CompanyFormBean companyFormBean);
    
    public Integer companyMasterDelete(CompanyFormBean companyFormBean);

    public Integer companyMasterInsert(CompanyFormBean companyFormBean);

    public Integer companyMasterUpdate(CompanyFormBean companyFormBean);
    
}
