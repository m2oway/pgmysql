/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.company.Vo;

/**
 *
 * @author Administrator
 */
public class Tb_Company {
    private String rnum;
    private String comp_seq   ;
    private String comp_nm    ;
    private String biz_no     ;
    private String corp_no ;
    private String comp_ceo_nm;
    private String comp_tel1  ;
    private String comp_tel2  ;
    private String use_flag;    
    private String use_flag_nm;    
    private String del_flag;
    private String ins_dt     ;
    private String mod_dt     ;
    private String ins_user   ;
    private String mod_user   ;
    private String comp_cate  ;
    private String cate_comp_nm;
    private String zip_cd     ;
    private String addr_1     ;
    private String addr_2;
    private String memo;
    
    private String biz_cate;  
    private String biz_type;   
    private String tax_flag;
    private String tax_flag_nm;
    private String comp_email;

    public String getComp_email() {
        return comp_email;
    }

    public void setComp_email(String comp_email) {
        this.comp_email = comp_email;
    }
    
    

    public String getTax_flag_nm() {
        return tax_flag_nm;
    }

    public void setTax_flag_nm(String tax_flag_nm) {
        this.tax_flag_nm = tax_flag_nm;
    }

    public String getBiz_cate() {
        return biz_cate;
    }

    public void setBiz_cate(String biz_cate) {
        this.biz_cate = biz_cate;
    }

    public String getBiz_type() {
        return biz_type;
    }

    public void setBiz_type(String biz_type) {
        this.biz_type = biz_type;
    }

    public String getTax_flag() {
        return tax_flag;
    }

    public void setTax_flag(String tax_flag) {
        this.tax_flag = tax_flag;
    }
    
    

    public String getUse_flag_nm() {
        return use_flag_nm;
    }

    public void setUse_flag_nm(String use_flag_nm) {
        this.use_flag_nm = use_flag_nm;
    }

    
    
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getDel_flag() {
        return del_flag;
    }

    public void setDel_flag(String del_flag) {
        this.del_flag = del_flag;
    }    
    
    public String getRnum() {
        return rnum;
    }

    public void setRnum(String rnum) {
        this.rnum = rnum;
    }

    public String getComp_seq() {
        return comp_seq;
    }

    public void setComp_seq(String comp_seq) {
        this.comp_seq = comp_seq;
    }

    public String getComp_nm() {
        return comp_nm;
    }

    public void setComp_nm(String comp_nm) {
        this.comp_nm = comp_nm;
    }

    public String getBiz_no() {
        return biz_no;
    }

    public void setBiz_no(String biz_no) {
        this.biz_no = biz_no;
    }

    public String getCorp_no() {
        return corp_no;
    }

    public void setCorp_no(String corp_no) {
        this.corp_no = corp_no;
    }

    public String getComp_ceo_nm() {
        return comp_ceo_nm;
    }

    public void setComp_ceo_nm(String comp_ceo_nm) {
        this.comp_ceo_nm = comp_ceo_nm;
    }

    public String getComp_tel1() {
        return comp_tel1;
    }

    public void setComp_tel1(String comp_tel1) {
        this.comp_tel1 = comp_tel1;
    }

    public String getComp_tel2() {
        return comp_tel2;
    }

    public void setComp_tel2(String comp_tel2) {
        this.comp_tel2 = comp_tel2;
    }

    public String getUse_flag() {
        return use_flag;
    }

    public void setUse_flag(String use_flag) {
        this.use_flag = use_flag;
    }

    public String getIns_dt() {
        return ins_dt;
    }

    public void setIns_dt(String ins_dt) {
        this.ins_dt = ins_dt;
    }

    public String getMod_dt() {
        return mod_dt;
    }

    public void setMod_dt(String mod_dt) {
        this.mod_dt = mod_dt;
    }

    public String getIns_user() {
        return ins_user;
    }

    public void setIns_user(String ins_user) {
        this.ins_user = ins_user;
    }

    public String getMod_user() {
        return mod_user;
    }

    public void setMod_user(String mod_user) {
        this.mod_user = mod_user;
    }

    public String getComp_cate() {
        return comp_cate;
    }

    public void setComp_cate(String comp_cate) {
        this.comp_cate = comp_cate;
    }

    public String getCate_comp_nm() {
        return cate_comp_nm;
    }

    public void setCate_comp_nm(String cate_comp_nm) {
        this.cate_comp_nm = cate_comp_nm;
    }

    public String getZip_cd() {
        return zip_cd;
    }

    public void setZip_cd(String zip_cd) {
        this.zip_cd = zip_cd;
    }

    public String getAddr_1() {
        return addr_1;
    }

    public void setAddr_1(String addr_1) {
        this.addr_1 = addr_1;
    }

    public String getAddr_2() {
        return addr_2;
    }

    public void setAddr_2(String addr_2) {
        this.addr_2 = addr_2;
    }
    
    
}
