/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.company.Controller;

import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.company.Bean.CompanyFormBean;
import com.onoffkorea.system.company.Service.CompanyService;
import com.onoffkorea.system.company.Vo.Tb_Company;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/company/**")
public class CompanyController {
        
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private CompanyService companyService;
    
    @Autowired
    private CommonService commonService;
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }    
    
    //사업체 정보 조회
    @RequestMapping(method= RequestMethod.GET,value = "/companyMasterList")
    public String companyMasterList(@Valid CompanyFormBean companyFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_companyMasterList = companyService.companyMasterList(companyFormBean);
        String ls_companyMasterList = (String) ht_companyMasterList.get("ResultSet");

        model.addAttribute("companyMasterList",ls_companyMasterList);
        
        return null;
        
    }
    
    //사업체 정보 조회
    @RequestMapping(method= RequestMethod.POST,value = "/companyMasterList")
    public @ResponseBody String companyMasterList(@Valid CompanyFormBean companyFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {
        
        Hashtable ht_companyMasterList = companyService.companyMasterList(companyFormBean);
        String ls_companyMasterList = (String)ht_companyMasterList.get("ResultSet");
     
        return ls_companyMasterList;
       
    }
    
    //사업체 정보 삭제
    @RequestMapping(method= RequestMethod.POST,value = "/companyMasterDelete")
    public @ResponseBody String companyMasterDelete(@Valid CompanyFormBean companyFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request) throws Exception {

        String result = null;
        result = companyService.companyMasterDelete(companyFormBean);
        
        return result;
        
    }
    
    //사업체 정보 등록
    @RequestMapping(method= RequestMethod.GET,value = "/companyMasterInsert")
    public String companyMasterInsertForm(@Valid CompanyFormBean companyFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {
        
        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        commonCodeSearchFormBean.setMain_code("COMP_CATE");
        Tb_Code_Main compCateList = commonService.codeSearch(commonCodeSearchFormBean);
        
        model.addAttribute("compCateList",compCateList);
        
        
        commonCodeSearchFormBean.setMain_code("TAX_FLAG");
        Tb_Code_Main TaxFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("TaxFlagList",TaxFlagList);
        
        return null;
        
    }   
    
    //사업체 정보 등록
    @RequestMapping(method= RequestMethod.POST,value = "/companyMasterInsert")
    public @ResponseBody String companyMasterInsert(@Valid CompanyFormBean companyFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        String result = null;
        companyFormBean.setUser_seq(dentalHopitalSession.getUser_seq());
        companyFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        result = companyService.companyMasterInsert(companyFormBean);
        
        return result;
        
    }       
    
    //사업체 정보 수정 
    @RequestMapping(method= RequestMethod.GET,value = "/companyMasterUpdate")
    public String companyMasterUpdateForm(@Valid CompanyFormBean companyFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest) throws Exception {

        Hashtable ht_companyMasterList = companyService.companyMasterInfo(companyFormBean);
        List ls_companyMasterList = (List) ht_companyMasterList.get("ResultSet");
        Tb_Company tb_Company = (Tb_Company) ls_companyMasterList.get(0);
        
        CommonCodeSearchFormBean commonCodeSearchFormBean = new CommonCodeSearchFormBean();
        
        commonCodeSearchFormBean.setMain_code("COMP_CATE");
        Tb_Code_Main compCateList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("compCateList",compCateList);
        
        commonCodeSearchFormBean.setMain_code("TAX_FLAG");
        Tb_Code_Main TaxFlagList = commonService.codeSearch(commonCodeSearchFormBean);
        model.addAttribute("TaxFlagList",TaxFlagList);
        
        model.addAttribute("tb_Company",tb_Company);

        
        return null;
        
    }       
    
    //사업체 정보 수정 
    @RequestMapping(method= RequestMethod.POST,value = "/companyMasterUpdate")
    public @ResponseBody String companyMasterUpdate(@Valid CompanyFormBean companyFormBean, Model model, RedirectAttributes redirectAttrs, HttpServletRequest request
            ,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession dentalHopitalSession) throws Exception {

        String result = null;
        companyFormBean.setMod_user(dentalHopitalSession.getUser_seq());
        result = companyService.companyMasterUpdate(companyFormBean);
        
        return result;
        
    }           
    
   

}
