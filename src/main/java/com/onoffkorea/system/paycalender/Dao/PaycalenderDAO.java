/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.paycalender.Dao;

import com.onoffkorea.system.paycalender.Bean.PaycalenderFormBean;
import java.util.List;


/**
 *
 * @author Administrator
 */
public interface PaycalenderDAO {

    public List payCalenderList(PaycalenderFormBean paycalenderFormBean);
    
    public List merchpayCalenderList(PaycalenderFormBean paycalenderFormBean);
    
    
}
