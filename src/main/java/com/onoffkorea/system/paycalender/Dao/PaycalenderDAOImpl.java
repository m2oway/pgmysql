/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.paycalender.Dao;

import com.onoffkorea.system.paycalender.Bean.PaycalenderFormBean;
import java.util.List;
import org.apache.log4j.Logger;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Administrator
 */
@Repository("paycalenderDAO")
public class PaycalenderDAOImpl extends SqlSessionDaoSupport implements PaycalenderDAO{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    //코드 정보 관리
    @Override
    public List payCalenderList(PaycalenderFormBean paycalenderFormBean) {
               
        return (List) getSqlSession().selectList("Paycalender.payCalenderList", paycalenderFormBean);
        
    }

    //코드 정보 관리
    @Override
    public List merchpayCalenderList(PaycalenderFormBean paycalenderFormBean) {
               
        return (List) getSqlSession().selectList("Paycalender.merchpayCalenderList", paycalenderFormBean);
        
    }
    
}
