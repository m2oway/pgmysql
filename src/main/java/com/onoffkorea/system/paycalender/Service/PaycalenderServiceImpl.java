/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.paycalender.Service;


import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.paycalender.Bean.PaycalenderFormBean;
import com.onoffkorea.system.paycalender.Dao.PaycalenderDAO;
import com.onoffkorea.system.paycalender.Vo.Tb_PayCalender;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Administrator
 */
@Service("paycalenderService")
public class PaycalenderServiceImpl  implements PaycalenderService{
    
    protected Logger logger = Logger.getLogger(getClass().getName());

    @Autowired
    private PaycalenderDAO paycalenderDAO;

    //코드 정보 관리
    @Override
    public Hashtable payCalenderList(PaycalenderFormBean paycalenderFormBean) {
        
        Hashtable ht = new Hashtable();

        try             
        {
            //List ls_paycalenderList = this.paycalenderDAO.payCalenderList(paycalenderFormBean);
            List ls_paycalenderList = null;
            
            Calendar cal = Calendar.getInstance();
            
            String strTpYYYYMM = paycalenderFormBean.getPaycalyyyymm();
            
            ls_paycalenderList = this.paycalenderDAO.payCalenderList(paycalenderFormBean);
            
            
            StringBuilder cc = new StringBuilder();
            cc.append("{\"rows\" : [");
            for (int i = 0; i < ls_paycalenderList.size(); i++) 
            {
                Tb_PayCalender tb_paydateinfo = (Tb_PayCalender) ls_paycalenderList.get(i);
                cc.append("{\"id\":" + tb_paydateinfo.getYmd());
                cc.append(",\"data\":[");                
                cc.append(" \"" + Util.nullToString(tb_paydateinfo.getYmd()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_paydateinfo.getCard_deposit_amt()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_paydateinfo.getKicc_deposit_amt()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_paydateinfo.getExp_pay_amt()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_paydateinfo.getDecision_pay_amt()) + "\"");
                
                if (i == (ls_paycalenderList.size() - 1)) {
                        cc.append("]}");
                } else {
                        cc.append("]},");
                }
            }
            cc.append("]}");   
            ht.put("payCalenderList", cc.toString());
        
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;
    }
    
    //코드 정보 관리
    @Override
    public Hashtable merchpayCalenderList(PaycalenderFormBean paycalenderFormBean) {
        
        Hashtable ht = new Hashtable();

        try             
        {
            List ls_paycalenderList = null;
            
            Calendar cal = Calendar.getInstance();
            
            String strTpYYYYMM = paycalenderFormBean.getPaycalyyyymm();
            
            ls_paycalenderList = this.paycalenderDAO.merchpayCalenderList(paycalenderFormBean);
            
            
            StringBuilder cc = new StringBuilder();
            cc.append("{\"rows\" : [");
            for (int i = 0; i < ls_paycalenderList.size(); i++) 
            {
                Tb_PayCalender tb_paydateinfo = (Tb_PayCalender) ls_paycalenderList.get(i);
                cc.append("{\"id\":" + tb_paydateinfo.getYmd());
                cc.append(",\"data\":[");                
                cc.append(" \"" + Util.nullToString(tb_paydateinfo.getYmd()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_paydateinfo.getExp_pay_amt()) + "\"");
                cc.append(",\"" + Util.nullToString(tb_paydateinfo.getDecision_pay_amt()) + "\"");
                
                if (i == (ls_paycalenderList.size() - 1)) {
                        cc.append("]}");
                } else {
                        cc.append("]},");
                }
            }
            cc.append("]}");   
            
            System.out.println("["+cc.toString()+"]");
            ht.put("payCalenderList", cc.toString());
        
        } catch (Exception e) {
                e.printStackTrace();
        }

        return ht;
    }
         
     
}

    

