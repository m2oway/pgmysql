/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.paycalender.Service;

import com.onoffkorea.system.paycalender.Bean.PaycalenderFormBean;
import java.util.Hashtable;

/**
 *
 * @author Administrator
 */
public interface PaycalenderService {

    
    public Hashtable  payCalenderList(PaycalenderFormBean paycalenderFormBean);
    
    public Hashtable  merchpayCalenderList(PaycalenderFormBean paycalenderFormBean);
   
}