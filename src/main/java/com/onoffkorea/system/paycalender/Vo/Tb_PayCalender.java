/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.paycalender.Vo;

/**
 *
 * @author Administrator
 */
public class Tb_PayCalender {
    private String rnum;
    private String ymd;
    private String card_deposit_amt;
    private String kicc_deposit_amt;
    private String exp_pay_amt;
    private String decision_pay_amt;

    public String getRnum() {
        return rnum;
    }

    public void setRnum(String rnum) {
        this.rnum = rnum;
    }

    public String getYmd() {
        return ymd;
    }

    public void setYmd(String ymd) {
        this.ymd = ymd;
    }

    public String getCard_deposit_amt() {
        return card_deposit_amt;
    }

    public void setCard_deposit_amt(String card_deposit_amt) {
        this.card_deposit_amt = card_deposit_amt;
    }

    public String getKicc_deposit_amt() {
        return kicc_deposit_amt;
    }

    public void setKicc_deposit_amt(String kicc_deposit_amt) {
        this.kicc_deposit_amt = kicc_deposit_amt;
    }

    public String getExp_pay_amt() {
        return exp_pay_amt;
    }

    public void setExp_pay_amt(String exp_pay_amt) {
        this.exp_pay_amt = exp_pay_amt;
    }

    public String getDecision_pay_amt() {
        return decision_pay_amt;
    }

    public void setDecision_pay_amt(String decision_pay_amt) {
        this.decision_pay_amt = decision_pay_amt;
    }
   
    
    
}
