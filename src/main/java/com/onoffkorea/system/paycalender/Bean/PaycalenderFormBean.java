/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.paycalender.Bean;
import com.onoffkorea.system.holiday.Bean.*;
import com.onoffkorea.system.common.util.CommonSortableListPagingForm;
/**
 *
 * @author Administrator
 */
public class PaycalenderFormBean extends CommonSortableListPagingForm {
    
     //휴일조회
    private String paycalyyyymm;
    private String paycalyyyymmdd;
    
    private String paycalstartdate;
    private String paycalenddate;
    
    public String getPaycalyyyymm() {
        return paycalyyyymm;
    }

    public void setPaycalyyyymm(String paycalyyyymm) {
        this.paycalyyyymm = paycalyyyymm;
    }

    public String getPaycalyyyymmdd() {
        return paycalyyyymmdd;
    }

    public void setPaycalyyyymmdd(String paycalyyyymmdd) {
        this.paycalyyyymmdd = paycalyyyymmdd;
    }

    public String getPaycalstartdate() {
        return paycalstartdate;
    }

    public void setPaycalstartdate(String paycalstartdate) {
        this.paycalstartdate = paycalstartdate;
    }

    public String getPaycalenddate() {
        return paycalenddate;
    }

    public void setPaycalenddate(String paycalenddate) {
        this.paycalenddate = paycalenddate;
    }
    
    

    
}
