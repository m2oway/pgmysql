/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onoffkorea.system.paycalender.Controller;

import com.onoffkorea.system.holiday.Controller.*;
import com.onoffkorea.system.common.vo.SiteSession;
import com.onoffkorea.system.company.Bean.CompanyFormBean;
import com.onoffkorea.system.company.Service.CompanyService;
import com.onoffkorea.system.auth.Bean.AuthFormBean;
import com.onoffkorea.system.auth.Service.AuthService;
import com.onoffkorea.system.common.Bean.CommonCodeSearchFormBean;
import com.onoffkorea.system.common.Service.CommonService;
import com.onoffkorea.system.common.util.Util;
import com.onoffkorea.system.common.vo.Tb_Code_Main;
import com.onoffkorea.system.mvc.extensions.ajax.AjaxUtils;
import com.onoffkorea.system.paycalender.Bean.PaycalenderFormBean;
import com.onoffkorea.system.paycalender.Service.PaycalenderService;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Administrator
 */
@Controller
@RequestMapping("/paycalender/**")
public class PaycalenderController {
        
    protected Log logger = LogFactory.getLog(getClass());

    @Autowired
    private PaycalenderService paycalenderService;
    
    @Autowired
    private CompanyService companyService;
    
    @Autowired
    private AuthService authService;
    
    @Autowired
    private CommonService commonService;    
    
    @ModelAttribute
    public void ajaxAttribute(WebRequest request, Model model) {
        
        model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
        
    }
    
    @ModelAttribute
    public void SiteSession(HttpServletRequest request, Model model) {

        HttpSession session = request.getSession();
        
        model.addAttribute("siteSessionObj",session.getAttribute("siteSessionObj"));
        
    }    
    
     //paycalender
     @RequestMapping(method= RequestMethod.GET,value = "/payCalenderList")
    public String payCalenderList(@Valid PaycalenderFormBean paycalenderFormBean, @Valid CompanyFormBean companyFormBean, @Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

                paycalenderFormBean.setSes_user_cate(siteSession.getSes_user_cate());

                if("01".equals(siteSession.getSes_user_cate())){
                    paycalenderFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
                }else if("02".equals(siteSession.getSes_user_cate())){
                    paycalenderFormBean.setSes_onfftid(siteSession.getSes_onfftid());
                    paycalenderFormBean.setUser_seq(siteSession.getUser_seq());
                }else if("03".equals(siteSession.getSes_user_cate())){
                    paycalenderFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
                    paycalenderFormBean.setUser_seq(siteSession.getUser_seq());
                }     

                String strTpCurDateYYYYMM  = Util.getTodayDate().substring(0, 6);

                Calendar calendar = Calendar.getInstance();

                String strTpPaycalender_yyyymm = Util.nullToString(paycalenderFormBean.getPaycalyyyymm());


                if(strTpPaycalender_yyyymm.equals(""))
                {
                    paycalenderFormBean.setPaycalyyyymm(strTpCurDateYYYYMM);
                    paycalenderFormBean.setPaycalstartdate(strTpCurDateYYYYMM+"01");

                    int lastDay = calendar.getActualMaximum(Calendar.DATE);

                    if(lastDay <10)
                    {
                        paycalenderFormBean.setPaycalenddate(strTpCurDateYYYYMM+"0"+String.valueOf(lastDay));
                    }
                    else
                    {
                        paycalenderFormBean.setPaycalenddate(strTpCurDateYYYYMM+String.valueOf(lastDay));
                    }
                }
                else
                {
                    paycalenderFormBean.setPaycalyyyymm(strTpPaycalender_yyyymm);
                    calendar.set(Integer.parseInt(strTpPaycalender_yyyymm.substring(0,4)), Integer.parseInt(strTpPaycalender_yyyymm.substring(4,6))+1, Integer.parseInt("01"));
                    int lastDay = calendar.getActualMaximum(Calendar.DATE);

                    paycalenderFormBean.setPaycalstartdate(strTpPaycalender_yyyymm+"01");

                    if(lastDay <10)
                    {
                        paycalenderFormBean.setPaycalenddate(strTpPaycalender_yyyymm+"0"+String.valueOf(lastDay));
                    }
                    else
                    {
                        paycalenderFormBean.setPaycalenddate(strTpPaycalender_yyyymm+String.valueOf(lastDay));
                    }
                }

                //String str_info =  paycalenderService.payCalenderList(paycalenderFormBean)
                Hashtable ht_info =  paycalenderService.payCalenderList(paycalenderFormBean);

                String str_info =  Util.nullToString((String)ht_info.get("payCalenderList"));

                model.addAttribute("payCalenderList",str_info);
                return null;

            }

            //회원 정보 조회
            @RequestMapping(method= RequestMethod.POST,value = "/payCalenderList")
            public @ResponseBody String payCalenderList(@Valid PaycalenderFormBean paycalenderFormBean, @Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs
                    , HttpServletRequest request, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

                paycalenderFormBean.setSes_user_cate(siteSession.getSes_user_cate());

                if("01".equals(siteSession.getSes_user_cate())){
                    paycalenderFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
                }else if("02".equals(siteSession.getSes_user_cate())){
                    paycalenderFormBean.setSes_onfftid(siteSession.getSes_onfftid());
                    paycalenderFormBean.setUser_seq(siteSession.getUser_seq());
                }else if("03".equals(siteSession.getSes_user_cate())){
                    paycalenderFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
                    paycalenderFormBean.setUser_seq(siteSession.getUser_seq());
                }                 
                        String strTpCurDateYYYYMM  = Util.getTodayDate().substring(0, 6);

                Calendar calendar = Calendar.getInstance();

                String strTpPaycalender_yyyymm = Util.nullToString(paycalenderFormBean.getPaycalyyyymm());


                if(strTpPaycalender_yyyymm.equals(""))
                {
                    paycalenderFormBean.setPaycalyyyymm(strTpCurDateYYYYMM);
                    paycalenderFormBean.setPaycalstartdate(strTpCurDateYYYYMM+"01");

                    int lastDay = calendar.getActualMaximum(Calendar.DATE);

                    if(lastDay <10)
                    {
                        paycalenderFormBean.setPaycalenddate(strTpCurDateYYYYMM+"0"+String.valueOf(lastDay));
                    }
                    else
                    {
                        paycalenderFormBean.setPaycalenddate(strTpCurDateYYYYMM+String.valueOf(lastDay));
                    }
                }
                else
                {
                    paycalenderFormBean.setPaycalyyyymm(strTpPaycalender_yyyymm);
                    calendar.set(Integer.parseInt(strTpPaycalender_yyyymm.substring(0,4)), Integer.parseInt(strTpPaycalender_yyyymm.substring(4,6))+1, Integer.parseInt("01"));
                    int lastDay = calendar.getActualMaximum(Calendar.DATE);

                    paycalenderFormBean.setPaycalstartdate(strTpPaycalender_yyyymm+"01");

                    if(lastDay <10)
                    {
                        paycalenderFormBean.setPaycalenddate(strTpPaycalender_yyyymm+"0"+String.valueOf(lastDay));
                    }
                    else
                    {
                        paycalenderFormBean.setPaycalenddate(strTpPaycalender_yyyymm+String.valueOf(lastDay));
                    }
                }

                //String str_info =  paycalenderService.payCalenderList(paycalenderFormBean)
                Hashtable ht_info =  paycalenderService.payCalenderList(paycalenderFormBean);

                String str_info =  Util.nullToString((String)ht_info.get("payCalenderList"));
                
                return str_info;
        
        }
    
    
     //paycalender
     @RequestMapping(method= RequestMethod.GET,value = "/merchpayCalenderList")
    public String merchpayCalenderList(@Valid PaycalenderFormBean paycalenderFormBean, @Valid CompanyFormBean companyFormBean, @Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

                paycalenderFormBean.setSes_user_cate(siteSession.getSes_user_cate());

                if("01".equals(siteSession.getSes_user_cate())){
                    paycalenderFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
                }else if("02".equals(siteSession.getSes_user_cate())){
                    paycalenderFormBean.setSes_onfftid(siteSession.getSes_onfftid());
                    paycalenderFormBean.setUser_seq(siteSession.getUser_seq());
                }else if("03".equals(siteSession.getSes_user_cate())){
                    paycalenderFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
                    paycalenderFormBean.setUser_seq(siteSession.getUser_seq());
                }     

                String strTpCurDateYYYYMM  = Util.getTodayDate().substring(0, 6);

                Calendar calendar = Calendar.getInstance();

                String strTpPaycalender_yyyymm = Util.nullToString(paycalenderFormBean.getPaycalyyyymm());


                if(strTpPaycalender_yyyymm.equals(""))
                {
                    paycalenderFormBean.setPaycalyyyymm(strTpCurDateYYYYMM);
                    paycalenderFormBean.setPaycalstartdate(strTpCurDateYYYYMM+"01");

                    int lastDay = calendar.getActualMaximum(Calendar.DATE);

                    if(lastDay <10)
                    {
                        paycalenderFormBean.setPaycalenddate(strTpCurDateYYYYMM+"0"+String.valueOf(lastDay));
                    }
                    else
                    {
                        paycalenderFormBean.setPaycalenddate(strTpCurDateYYYYMM+String.valueOf(lastDay));
                    }
                }
                else
                {
                    paycalenderFormBean.setPaycalyyyymm(strTpPaycalender_yyyymm);
                    calendar.set(Integer.parseInt(strTpPaycalender_yyyymm.substring(0,4)), Integer.parseInt(strTpPaycalender_yyyymm.substring(4,6))+1, Integer.parseInt("01"));
                    int lastDay = calendar.getActualMaximum(Calendar.DATE);

                    paycalenderFormBean.setPaycalstartdate(strTpPaycalender_yyyymm+"01");

                    if(lastDay <10)
                    {
                        paycalenderFormBean.setPaycalenddate(strTpPaycalender_yyyymm+"0"+String.valueOf(lastDay));
                    }
                    else
                    {
                        paycalenderFormBean.setPaycalenddate(strTpPaycalender_yyyymm+String.valueOf(lastDay));
                    }
                }

                //String str_info =  paycalenderService.payCalenderList(paycalenderFormBean)
                Hashtable ht_info =  paycalenderService.merchpayCalenderList(paycalenderFormBean);

                String str_info =  Util.nullToString((String)ht_info.get("payCalenderList"));

                model.addAttribute("payCalenderList",str_info);
                return null;

            }
            
    
            //회원 정보 조회
            @RequestMapping(method= RequestMethod.POST,value = "/merchpayCalenderList")
            public @ResponseBody String merchpayCalenderList(@Valid PaycalenderFormBean paycalenderFormBean, @Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs
                    , HttpServletRequest request, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

                paycalenderFormBean.setSes_user_cate(siteSession.getSes_user_cate());

                if("01".equals(siteSession.getSes_user_cate())){
                    paycalenderFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
                }else if("02".equals(siteSession.getSes_user_cate())){
                    paycalenderFormBean.setSes_onfftid(siteSession.getSes_onfftid());
                    paycalenderFormBean.setUser_seq(siteSession.getUser_seq());
                }else if("03".equals(siteSession.getSes_user_cate())){
                    paycalenderFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
                    paycalenderFormBean.setUser_seq(siteSession.getUser_seq());
                }                 
                        String strTpCurDateYYYYMM  = Util.getTodayDate().substring(0, 6);

                Calendar calendar = Calendar.getInstance();

                String strTpPaycalender_yyyymm = Util.nullToString(paycalenderFormBean.getPaycalyyyymm());


                if(strTpPaycalender_yyyymm.equals(""))
                {
                    paycalenderFormBean.setPaycalyyyymm(strTpCurDateYYYYMM);
                    paycalenderFormBean.setPaycalstartdate(strTpCurDateYYYYMM+"01");

                    int lastDay = calendar.getActualMaximum(Calendar.DATE);

                    if(lastDay <10)
                    {
                        paycalenderFormBean.setPaycalenddate(strTpCurDateYYYYMM+"0"+String.valueOf(lastDay));
                    }
                    else
                    {
                        paycalenderFormBean.setPaycalenddate(strTpCurDateYYYYMM+String.valueOf(lastDay));
                    }
                }
                else
                {
                    paycalenderFormBean.setPaycalyyyymm(strTpPaycalender_yyyymm);
                    calendar.set(Integer.parseInt(strTpPaycalender_yyyymm.substring(0,4)), Integer.parseInt(strTpPaycalender_yyyymm.substring(4,6))+1, Integer.parseInt("01"));
                    int lastDay = calendar.getActualMaximum(Calendar.DATE);

                    paycalenderFormBean.setPaycalstartdate(strTpPaycalender_yyyymm+"01");

                    if(lastDay <10)
                    {
                        paycalenderFormBean.setPaycalenddate(strTpPaycalender_yyyymm+"0"+String.valueOf(lastDay));
                    }
                    else
                    {
                        paycalenderFormBean.setPaycalenddate(strTpPaycalender_yyyymm+String.valueOf(lastDay));
                    }
                }

                //String str_info =  paycalenderService.payCalenderList(paycalenderFormBean)
                Hashtable ht_info =  paycalenderService.merchpayCalenderList(paycalenderFormBean);

                String str_info =  Util.nullToString((String)ht_info.get("payCalenderList"));
                
                return str_info;
        
        }
        
            
    
     //paycalender
     @RequestMapping(method= RequestMethod.GET,value = "/basic_merchpayCalenderList")
    public String basic_merchpayCalenderList(@Valid PaycalenderFormBean paycalenderFormBean, @Valid CompanyFormBean companyFormBean, @Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs
            , HttpServletRequest request,@ModelAttribute("ajaxRequest") boolean ajaxRequest, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

                paycalenderFormBean.setSes_user_cate(siteSession.getSes_user_cate());

                if("01".equals(siteSession.getSes_user_cate())){
                    paycalenderFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
                }else if("02".equals(siteSession.getSes_user_cate())){
                    paycalenderFormBean.setSes_onfftid(siteSession.getSes_onfftid());
                    paycalenderFormBean.setUser_seq(siteSession.getUser_seq());
                }else if("03".equals(siteSession.getSes_user_cate())){
                    paycalenderFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
                    paycalenderFormBean.setUser_seq(siteSession.getUser_seq());
                }     

                String strTpCurDateYYYYMM  = Util.getTodayDate().substring(0, 6);

                Calendar calendar = Calendar.getInstance();

                String strTpPaycalender_yyyymm = Util.nullToString(paycalenderFormBean.getPaycalyyyymm());


                if(strTpPaycalender_yyyymm.equals(""))
                {
                    paycalenderFormBean.setPaycalyyyymm(strTpCurDateYYYYMM);
                    paycalenderFormBean.setPaycalstartdate(strTpCurDateYYYYMM+"01");

                    int lastDay = calendar.getActualMaximum(Calendar.DATE);

                    if(lastDay <10)
                    {
                        paycalenderFormBean.setPaycalenddate(strTpCurDateYYYYMM+"0"+String.valueOf(lastDay));
                    }
                    else
                    {
                        paycalenderFormBean.setPaycalenddate(strTpCurDateYYYYMM+String.valueOf(lastDay));
                    }
                }
                else
                {
                    paycalenderFormBean.setPaycalyyyymm(strTpPaycalender_yyyymm);
                    calendar.set(Integer.parseInt(strTpPaycalender_yyyymm.substring(0,4)), Integer.parseInt(strTpPaycalender_yyyymm.substring(4,6))+1, Integer.parseInt("01"));
                    int lastDay = calendar.getActualMaximum(Calendar.DATE);

                    paycalenderFormBean.setPaycalstartdate(strTpPaycalender_yyyymm+"01");

                    if(lastDay <10)
                    {
                        paycalenderFormBean.setPaycalenddate(strTpPaycalender_yyyymm+"0"+String.valueOf(lastDay));
                    }
                    else
                    {
                        paycalenderFormBean.setPaycalenddate(strTpPaycalender_yyyymm+String.valueOf(lastDay));
                    }
                }

                //String str_info =  paycalenderService.payCalenderList(paycalenderFormBean)
                Hashtable ht_info =  paycalenderService.merchpayCalenderList(paycalenderFormBean);

                String str_info =  Util.nullToString((String)ht_info.get("payCalenderList"));

                model.addAttribute("payCalenderList",str_info);
                return null;

            }
            
    
            //회원 정보 조회
            @RequestMapping(method= RequestMethod.POST,value = "/basic_merchpayCalenderList")
            public @ResponseBody String basic_merchpayCalenderList(@Valid PaycalenderFormBean paycalenderFormBean, @Valid AuthFormBean authFormBean, Model model, RedirectAttributes redirectAttrs
                    , HttpServletRequest request, @ModelAttribute("siteSessionObj") SiteSession siteSession) throws Exception {

                paycalenderFormBean.setSes_user_cate(siteSession.getSes_user_cate());

                if("01".equals(siteSession.getSes_user_cate())){
                    paycalenderFormBean.setSes_onffmerch_no(siteSession.getSes_onffmerch_no());
                }else if("02".equals(siteSession.getSes_user_cate())){
                    paycalenderFormBean.setSes_onfftid(siteSession.getSes_onfftid());
                    paycalenderFormBean.setUser_seq(siteSession.getUser_seq());
                }else if("03".equals(siteSession.getSes_user_cate())){
                    paycalenderFormBean.setSes_agent_seq(siteSession.getSes_agent_seq());
                    paycalenderFormBean.setUser_seq(siteSession.getUser_seq());
                }                 
                        String strTpCurDateYYYYMM  = Util.getTodayDate().substring(0, 6);

                Calendar calendar = Calendar.getInstance();

                String strTpPaycalender_yyyymm = Util.nullToString(paycalenderFormBean.getPaycalyyyymm());


                if(strTpPaycalender_yyyymm.equals(""))
                {
                    paycalenderFormBean.setPaycalyyyymm(strTpCurDateYYYYMM);
                    paycalenderFormBean.setPaycalstartdate(strTpCurDateYYYYMM+"01");

                    int lastDay = calendar.getActualMaximum(Calendar.DATE);

                    if(lastDay <10)
                    {
                        paycalenderFormBean.setPaycalenddate(strTpCurDateYYYYMM+"0"+String.valueOf(lastDay));
                    }
                    else
                    {
                        paycalenderFormBean.setPaycalenddate(strTpCurDateYYYYMM+String.valueOf(lastDay));
                    }
                }
                else
                {
                    paycalenderFormBean.setPaycalyyyymm(strTpPaycalender_yyyymm);
                    calendar.set(Integer.parseInt(strTpPaycalender_yyyymm.substring(0,4)), Integer.parseInt(strTpPaycalender_yyyymm.substring(4,6))+1, Integer.parseInt("01"));
                    int lastDay = calendar.getActualMaximum(Calendar.DATE);

                    paycalenderFormBean.setPaycalstartdate(strTpPaycalender_yyyymm+"01");

                    if(lastDay <10)
                    {
                        paycalenderFormBean.setPaycalenddate(strTpPaycalender_yyyymm+"0"+String.valueOf(lastDay));
                    }
                    else
                    {
                        paycalenderFormBean.setPaycalenddate(strTpPaycalender_yyyymm+String.valueOf(lastDay));
                    }
                }

                //String str_info =  paycalenderService.payCalenderList(paycalenderFormBean)
                Hashtable ht_info =  paycalenderService.merchpayCalenderList(paycalenderFormBean);

                String str_info =  Util.nullToString((String)ht_info.get("payCalenderList"));
                
                return str_info;
        
        }            
            
}
