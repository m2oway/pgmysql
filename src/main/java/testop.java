
import com.onoffkorea.system.common.util.Util;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author MoonBong
 */
public class testop {
        
   /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        String strReturnval = null;
        
        FileInputStream fis = null;
        
        BufferedReader br = null;
        
        try
        {
            fis = new FileInputStream("C:/filedownload/매입내역_20170526_132933_onoff_B_1.csv");
            String sLine = "";
            
            int readCount =0 ;
            
            br = new BufferedReader(new InputStreamReader(fis));
            while( (sLine = br.readLine()) != null )
            {
                readCount++;
                System.out.println("SLINE : ["+sLine+"]");
                
                if(readCount > 1)
                {
                    String[] arrStrLineInfo = sLine.split(",", -1);
                    
                    System.out.println("arr length : " + arrStrLineInfo.length);
                    for(int ix=0 ; ix < arrStrLineInfo.length ; ix++)
                    {
                        System.out.println("ix["+ix+"] : ["+ (Util.nullToString(arrStrLineInfo[ix])).replaceAll("=\"", "").replace("\"", "") +"]");
                    }
                }
            }
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            try
            {
                if (br != null)
                {
                    br.close();
                }
            }
            catch(Exception ex){}
            
            try
            {
                if (fis != null)
                {
                    fis.close();
                }
            }
            catch(Exception ex){}
        }
    }
}
