package common.web.servlet;

import org.springframework.web.servlet.mvc.multiaction.MultiActionController;



public class BaseSpringMultiActionController extends MultiActionController {
	
	public BaseSpringMultiActionController(){

	}

	protected String getMessage(String msg) {

		return super.getMessageSourceAccessor().getMessage(msg);
	}

	protected String getMessage(String msg, Object[] obj) {

		return super.getMessageSourceAccessor().getMessage(msg, obj);
	}
	
}